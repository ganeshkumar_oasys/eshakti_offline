package com.oasys.eshakti.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.Adapter.BankTransactionReportAdapter;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.RecyclerItemClickListener;
import com.oasys.eshakti.R;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.SHGTable;

import java.util.ArrayList;
import java.util.Arrays;


public class BankTransactionReportList extends Fragment {
    private View view;
    private TextView mGroupname, mCashinHand, mCashatBank, mFragmentHeader;
    private RecyclerView recyclerView;
    private ListOfShg shgDto;
    private LinearLayoutManager linearLayoutManager;
    ArrayList<String> banktransactionlist = new ArrayList<>(Arrays.asList("BANK TRANSACTION SUMMARY", "FIXED DEPOSIT REPORT", "RECURRING DEPOSIT REPORT", "ACCOUNT TO ACCOUNT TRANSFER REPORT"));
    private BankTransactionReportAdapter bankTransactionReportAdapter;
    private Bundle bundle;
    FixedDepositReport fixedDepositReport;

    public BankTransactionReportList() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_bank_transaction_report_list, container, false);
        inIt(view);
        return view;
    }

    private void inIt(View view) {

        mGroupname = view.findViewById(R.id.groupname);
        mCashinHand = view.findViewById(R.id.ch);
        mCashatBank = view.findViewById(R.id.cb);
        mFragmentHeader = view.findViewById(R.id.fragmentHeader);
        recyclerView = view.findViewById(R.id.recyclerViewbankreport);

        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bundle = new Bundle();
        fixedDepositReport = new FixedDepositReport();

        mGroupname.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
        mGroupname.setTypeface(LoginActivity.sTypeface);

        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);

        mCashatBank = (TextView) view.findViewById(R.id.cb);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashatBank.setTypeface(LoginActivity.sTypeface);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));


        bankTransactionReportAdapter = new BankTransactionReportAdapter(getActivity(), banktransactionlist);
        recyclerView.setAdapter(bankTransactionReportAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                switch (position) {
                    case 0:
                        NewDrawerScreen.showFragment(new Reports_BankTransactionSummary());
                        break;
                    case 1:
                        bundle.putString("FixedDeposit", "FIXEDDEPOSITREPORTS");
                        fixedDepositReport.setArguments(bundle);
                        NewDrawerScreen.showFragment(fixedDepositReport);
                        break;
                    case 2:
                        bundle.putString("FixedDeposit", "RECURRINGDEPOSITREPORTS");
                        fixedDepositReport.setArguments(bundle);
                        NewDrawerScreen.showFragment(fixedDepositReport);
                        break;
                    case 3:
                        bundle.putString("FixedDeposit", "ACCOUNTTOACCOUNTREPORTS");
                        fixedDepositReport.setArguments(bundle);
                        NewDrawerScreen.showFragment(fixedDepositReport);
                        break;
                }
            }
        }));


    }


}

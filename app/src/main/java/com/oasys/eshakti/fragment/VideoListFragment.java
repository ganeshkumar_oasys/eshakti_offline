package com.oasys.eshakti.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.R;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.VideoActivity;
import com.oasys.eshakti.database.SHGTable;


public class VideoListFragment extends Fragment {
    View rootView;
    private TextView mGroupName, mCashinHand, mCashatBank, mHeader;
    private ListOfShg shgDto;
    String[] title = {"Eshakti Mobile Application","Eshakti(Hindi)","Eshakti(English)"};
    ListView listView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        rootView =  inflater.inflate(R.layout.fragment_video_list, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        init();
        return rootView;
    }

    @SuppressLint("ResourceType")
    private void init() {

        try{
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);
            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);
            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);
            listView = (ListView)rootView.findViewById(R.id.mobile_list);


            ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.videoslistview,R.id.textview, title);
            listView.setAdapter(adapter);


 listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
     @Override
     public void onItemClick(AdapterView<?> parent, View view,int position, long id) {

         VideoActivity Vi =new VideoActivity();
         Intent i = new Intent(getActivity(), VideoActivity.class);
         if(position == 0) {
             i.putExtra("eshakti",String.valueOf(position));
             startActivity(i);
         } else if(position == 1) {
             i.putExtra("eshakti",String.valueOf(position));
             startActivity(i);
         } else if(position == 2) {
             i.putExtra("eshakti",String.valueOf(position));
             startActivity(i);
         }

     }
 });
        }catch (Exception e)
        { e.printStackTrace();
        }
    }
}
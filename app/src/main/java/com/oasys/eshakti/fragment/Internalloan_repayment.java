package com.oasys.eshakti.fragment;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.Dialogue.Dialog_New_TransactionDate;
import com.oasys.eshakti.Dto.CashOfGroup;
import com.oasys.eshakti.Dto.ExistingLoan;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.Dto.OfflineDto;
import com.oasys.eshakti.Dto.RequestDto.InternalLoanRepaymentRequestDto;
import com.oasys.eshakti.Dto.RequestDto.RepaymentDetails;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.LoanTable;
import com.oasys.eshakti.database.MemberTable;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.database.TransactionTable;
import com.oasys.eshakti.views.CustomHorizontalScrollView;
import com.oasys.eshakti.views.Get_EdiText_Filter;
import com.oasys.eshakti.views.TextviewUtils;
import com.tutorialsee.lib.TastyToast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Internalloan_repayment extends Fragment implements View.OnClickListener, NewTaskListener {
    View view;
    private String loan_id = "";
    private String accountnumber = "";
    private String loantype = "";
    private String bankname = "";
    private TextView mloantype, mMemberName;
    private LinearLayout mMemberNameLayout;
    private TableLayout mLeftHeaderTable;
    private int mSize;
    private int responsesize;
    private TableLayout mRightHeaderTable;
    private TableLayout mLeftContentTable;
    private TableLayout mRightContentTable;
    private EditText mamount_values;
    private EditText mintrest_values;
//    private TextView memberName_Text;
    private TextView moutstanding;
    private TextView mcurrentDue;
    private List<TextView> soutstandingFields;
    private List<EditText> samountFields;
    private List<EditText> sintrestFields;
    public String[] sAmountSavingsAmounts;
    public String[] sintrestSavingsAmount;
    private CustomHorizontalScrollView mHSRightHeader;
    private CustomHorizontalScrollView mHSRightContent;
    private Button mSubmit_Raised_Button;
    private Button mEdit_RaisedButton;
    private Button mOk_RaisedButton;
    private Button mPerviousButton;
    private Button mNextButton;
    private ArrayList<MemberList> arrMem;
    private ListOfShg shgDto;
    private Dialog mProgressDilaog;
    private NetworkConnection networkConnection;
    private ArrayList<OfflineDto> offlineDBData = new ArrayList<>();
    // private List<MemberList> memList;
    private ArrayList<RepaymentDetails> mlrlist;
    String width[] = {AppStrings.memberName, AppStrings.outstanding, AppStrings.savings, AppStrings.interest};
    int[] rightHeaderWidth = new int[width.length];
    int[] rightContentWidth = new int[width.length];
    ResponseDto responseDto;
    ArrayList<MemberList> outstandingamt, dummyList;
    String nullAmount = "0";
    public static int sAmount_Total, sIntrest_Total;
    public static String sSendToServer_Amount, sSendToServer_Intrest;
    String nullVlaue = "0";
    String[] confirmArr;
    Dialog confirmationDialog;
    String shgId = "";
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private String flag = "0";
    private ArrayList<CashOfGroup> csGrp;
    private int sum = 0;
    public static int sum_of_internalloan_repayment;
    //  private List<ExistingLoan> memloanDetails;
    private ArrayList<MemberList> memloanDetails;
    private List<MemberList> memList;
    private List<ExistingLoan> grploanDetails;
    boolean resume_value=false;
    int statusCode;
    String message;
    private  int total_outstanding = 0;
    ResponseDto resdto;

    public Internalloan_repayment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.newinternalloan_repayment, container, false);
//        Attendance.flag = "0";
        /*Bundle bundle = getArguments();
        if (bundle != null) {
            EShaktiApplication.flag = bundle.getString("stepwise");
            Log.d("LOANID", EShaktiApplication.flag);
        }
*/

       /* Log.d("flagvalue",EShaktiApplication.flag);

        if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
            NewDrawerScreen.item1.setVisible(false);
            NewDrawerScreen.item2.setVisible(false);
            NewDrawerScreen.item.setVisible(false);
            NewDrawerScreen.logOutItem.setVisible(false);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.INVISIBLE);
        }
        else
        {
            NewDrawerScreen.item1.setVisible(true);
            NewDrawerScreen.item2.setVisible(true);
            NewDrawerScreen.item.setVisible(true);
            NewDrawerScreen.logOutItem.setVisible(true);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.VISIBLE);
        }
*/
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
            menu.findItem(R.id.action_home).setVisible(false);
            menu.findItem(R.id.action_logout).setVisible(false);
            menu.findItem(R.id.menu_logout).setVisible(false);
            menu.findItem(R.id.action_grouplist).setVisible(false);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.INVISIBLE);
        }
        else
        {
            menu.findItem(R.id.action_home).setVisible(true);
            menu.findItem(R.id.action_logout).setVisible(true);
            menu.findItem(R.id.menu_logout).setVisible(true);
            menu.findItem(R.id.action_grouplist).setVisible(true);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.VISIBLE);
        }
        super.onPrepareOptionsMenu(menu);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        grploanDetails = LoanTable.getGrpLoanDetails(shgDto.getShgId());
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        if (MySharedPreference.readInteger(getActivity(), MySharedPreference.MR_IL_COUNT, 0) <= 0)
            MySharedPreference.writeInteger(getActivity(), MySharedPreference.MR_IL_COUNT, 0);


//        if (EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1")) {
        if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
            if (networkConnection.isNetworkAvailable()) {            //  onTaskStarted();
                RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.NAV_DETAILS + shgDto.getId(), getActivity(), ServiceType.NAV_DETAILS);

            }
        }

        memloanDetails = LoanTable.getILDetails(shgDto.getShgId());
        shgId=shgDto.getShgId();
        dummyList = new ArrayList<>();


        if (memloanDetails.size() <= 0) {
            for (MemberList ml : memList) {
                MemberList mem = new MemberList();
                mem.setMemberId(ml.getMemberId());
                mem.setMemberName(ml.getMemberName());
                mem.setLoanOutstanding("0.0");
                // mem.setLoanId(loan_id);
                dummyList.add(mem);
            }
            memloanDetails = dummyList;
        }


        outstandingamt = memloanDetails;
        init();

        if (networkConnection.isNetworkAvailable()) {
         /*   shgId = shgDto.getShgId();
            String url = Constants.BASE_URL + Constants.GETINTERNALOUTSTANDING + shgId;
            RestClient.getRestClient(this).callWebServiceForGetMethod(url, getActivity(), ServiceType.GETINTERNALOUTSTANDINGAMOUNT);*/
        }
        stepWiseLoanType();
        //  init();
    }

    public void init() {
        try {
            mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());

            mCashinHand = (TextView) view.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());

            mCashatBank = (TextView) view.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());

            samountFields = new ArrayList<EditText>();
            sintrestFields = new ArrayList<EditText>();
            mloantype = (TextView) view.findViewById(R.id.ilr_loantype);
            mloantype.setText(AppStrings.InternalLoan);
            mloantype.setTypeface(LoginActivity.sTypeface);

            // mloannumber = (TextView) view.findViewById(R.id.ilr_loanAccNo);
            mMemberNameLayout = (LinearLayout) view.findViewById(R.id.ilr_member_name_layout);
            mMemberName = (TextView) view.findViewById(R.id.ilr_member_name);
            Log.d("memberdata", String.valueOf(mSize));
            mLeftHeaderTable = (TableLayout) view.findViewById(R.id.ilr_LeftHeaderTable);
            mRightHeaderTable = (TableLayout) view.findViewById(R.id.ilr_RightHeaderTable);
            mLeftContentTable = (TableLayout) view.findViewById(R.id.ilr_LeftContentTable);
            mRightContentTable = (TableLayout) view.findViewById(R.id.ilr_RightContentTable);

            mHSRightHeader = (CustomHorizontalScrollView) view.findViewById(R.id.ilr_rightHeaderHScrollView);
            mHSRightContent = (CustomHorizontalScrollView) view.findViewById(R.id.ilr_rightContentHScrollView);

            mHSRightHeader.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {

                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub

                    mHSRightContent.scrollTo(l, 0);

                }
            });

            mHSRightContent.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
                @Override
                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub
                    mHSRightHeader.scrollTo(l, 0);
                }
            });
            // mloantype.setText(loantype + " - " + bankname);
            //mloannumber.setText("LOAN ACCOUNT NUMBER :" + accountnumber);

            TableRow leftHeaderRow = new TableRow(getActivity());

            TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

            TextView mMemberName_headerText = new TextView(getActivity());
            mMemberName_headerText
                    .setText("" + String.valueOf(AppStrings.memberName));
            mMemberName_headerText.setTextColor(Color.WHITE);
            mMemberName_headerText.setPadding(10, 5, 10, 5);
            mMemberName_headerText.setLayoutParams(lHeaderParams);
            leftHeaderRow.addView(mMemberName_headerText);

            mLeftHeaderTable.addView(leftHeaderRow);

            TableRow rightHeaderRow = new TableRow(getActivity());
            TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(10, 0, 20, 0);
            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 0, 10, 0);

            TextView moutstandingAmount_HeaderText = new TextView(getActivity());
            moutstandingAmount_HeaderText
                    .setText("" + String.valueOf(AppStrings.outstanding));
            moutstandingAmount_HeaderText.setTextColor(Color.WHITE);
            moutstandingAmount_HeaderText.setPadding(5, 5, 5, 5);
            moutstandingAmount_HeaderText.setLayoutParams(contentParams);
            moutstandingAmount_HeaderText.setGravity(Gravity.CENTER);
            moutstandingAmount_HeaderText.setSingleLine(true);
            rightHeaderRow.addView(moutstandingAmount_HeaderText);

            TextView mVAmount_HeaderText = new TextView(getActivity());
            mVAmount_HeaderText
                    .setText("" + String.valueOf(AppStrings.amount));
            mVAmount_HeaderText.setTextColor(Color.WHITE);

            mVAmount_HeaderText.setLayoutParams(rHeaderParams);
            mVAmount_HeaderText.setSingleLine(true);
            rightHeaderRow.addView(mVAmount_HeaderText);

            TextView mVIntrestAmount_HeaderText = new TextView(getActivity());
            mVIntrestAmount_HeaderText
                    .setText("" + String.valueOf(AppStrings.interest));
            mVIntrestAmount_HeaderText.setTextColor(Color.WHITE);

            mVIntrestAmount_HeaderText.setLayoutParams(rHeaderParams);
            mVIntrestAmount_HeaderText.setSingleLine(true);
            rightHeaderRow.addView(mVIntrestAmount_HeaderText);

           /* TextView mVDue_HeaderText = new TextView(getActivity());
            mVDue_HeaderText
                    .setText("" + String.valueOf(AppStrings.currentdue));
            mVDue_HeaderText.setTextColor(Color.WHITE);

            mVDue_HeaderText.setLayoutParams(rHeaderParams);
            mVDue_HeaderText.setSingleLine(true);
            rightHeaderRow.addView(mVDue_HeaderText);*/

            mRightHeaderTable.addView(rightHeaderRow);
            //getTableRowHeaderCellWidth();

            //  outstandingamt.size();

            //code
            responsesize = outstandingamt.size();
            for (int i = 0; i < outstandingamt.size(); i++) {

                TableRow leftContentRow = new TableRow(getActivity());

                TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 60, 1f);
                leftContentParams.setMargins(5, 5, 5, 5);

                final TextView memberName_Text = new TextView(getActivity());
                memberName_Text.setText(outstandingamt.get(i).getMemberName());
//                memberName_Text.setText(memList.get(i).getMemberName());
                memberName_Text.setTextColor(R.color.black);
                memberName_Text.setPadding(5, 5, 5, 5);
                memberName_Text.setLayoutParams(leftContentParams);
                memberName_Text.setWidth(200);
                memberName_Text.setSingleLine(true);
                memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
                leftContentRow.addView(memberName_Text);

                mLeftContentTable.addView(leftContentRow);

                TableRow rightContentRow = new TableRow(getActivity());

                TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);// (150,60,1f);
                rightContentParams.setMargins(10, 5, 10, 5);

                TableRow.LayoutParams contentRow_Params = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 60,
                        1f);

                contentRow_Params.setMargins(10, 5, 10, 5);


                moutstanding = new TextView(getActivity());
                String out = "";
                out = outstandingamt.get(i).getLoanOutstanding();
                int con_outstanding = (int)Double.parseDouble(out);
                total_outstanding = total_outstanding + con_outstanding;

                if ((out == ("null")) || (out == ("")) || (out == null)) {
                    moutstanding.setText("0");
                } else {
                    String outstandamount;
                    outstandamount = outstandingamt.get(i).getLoanOutstanding();
                    int oam = (int) Double.parseDouble(outstandamount);
                    moutstanding.setText(oam + "");
                }
                moutstanding.setTextColor(R.color.black);
                moutstanding.setGravity(Gravity.CENTER);
                moutstanding.setLayoutParams(contentRow_Params);
                moutstanding.setPadding(10, 0, 10, 5);
                rightContentRow.addView(moutstanding);


                mamount_values = new EditText(getActivity());
                mamount_values.setId(i);
                samountFields.add(mamount_values);
                mamount_values.setPadding(5, 5, 5, 5);
                mamount_values.setBackgroundResource(R.drawable.edittext_background);
                mamount_values.setLayoutParams(rightContentParams);
                mamount_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
                mamount_values.setFilters(Get_EdiText_Filter.editText_filter());
                mamount_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                mamount_values.setTextColor(R.color.black);
                // mSavings_values.setWidth(150);
                mamount_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);

                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            Log.d("name",""+mMemberName);
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

                        } else {

                            ((EditText) v).setGravity(Gravity.RIGHT);
                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }

                    }
                });
                rightContentRow.addView(mamount_values);

                mintrest_values = new EditText(getActivity());
                mintrest_values.setId(i);
                sintrestFields.add(mintrest_values);
                mintrest_values.setPadding(5, 5, 5, 5);
                mintrest_values.setBackgroundResource(R.drawable.edittext_background);
                mintrest_values.setLayoutParams(rightContentParams);
                mintrest_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
                mintrest_values.setFilters(Get_EdiText_Filter.editText_filter());
                mintrest_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                mintrest_values.setTextColor(R.color.black);
                // mVSavings_values.setWidth(150);
                mintrest_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);

                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

                        } else {

                            ((EditText) v).setGravity(Gravity.RIGHT);
                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }
                    }
                });
                rightContentRow.addView(mintrest_values);


              /*  mcurrentDue = new TextView(getActivity());
                mcurrentDue.setText("0");
                mcurrentDue.setTextColor(R.color.black);
                mcurrentDue.setGravity(Gravity.CENTER);
                mcurrentDue.setLayoutParams(contentRow_Params);
                mcurrentDue.setPadding(10, 0, 10, 5);
                rightContentRow.addView(mcurrentDue);*/


                mRightContentTable.addView(rightContentRow);

            }


            mSubmit_Raised_Button = (Button) view.findViewById(R.id.ilr_fragment_Submit_button);
            mSubmit_Raised_Button.setText(AppStrings.submit);
            mSubmit_Raised_Button.setOnClickListener(this);

            mPerviousButton = (Button) view.findViewById(R.id.ilr_fragment_Previousbutton);
            //  mPerviousButton.setText("Savings" + AppStrings.mPervious);
            mPerviousButton.setOnClickListener(this);

            mNextButton = (Button) view.findViewById(R.id.ilr_fragment_skipbutton);
            //     mNextButton.setText("Savings" + AppStrings.mNext);
            mNextButton.setOnClickListener(this);

//            if (EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1"))
            if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
                if(total_outstanding ==0)
                {
                    mNextButton.setVisibility(View.VISIBLE);
                }

            }
            else
            {
                mNextButton.setVisibility(View.INVISIBLE);
            }

            mPerviousButton.setVisibility(View.INVISIBLE);


            resizeMemberNameWidth();
            resizeRightSideTable();
            resizeBodyTableRowHeight();


        } catch (Exception e) {
            Log.e("error", e.toString());
        }
    }

    public void stepWiseLoanType() {
        try {
            if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
                String url = Constants.BASE_URL + Constants.GETGROUPLOANTYPES + shgId;
                RestClient.getRestClient(Internalloan_repayment.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.GETLOANTYPES_STEPWISES);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resizeRightSideTable() {
        // TODO Auto-generated method stub
        int rightHeaderCount = (((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount());
        System.out.println("------------header count---------" + rightHeaderCount);
        for (int i = 0; i < rightHeaderCount; i++) {
            rightHeaderWidth[i] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i));
            rightContentWidth[i] = viewWidth(((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i));
        }
        for (int i = 0; i < rightHeaderCount; i++) {
            if (rightHeaderWidth[i] < rightContentWidth[i]) {
                ((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i)
                        .getLayoutParams().width = rightContentWidth[i];
            } else {
                ((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i)
                        .getLayoutParams().width = rightHeaderWidth[i];
            }
        }
    }

    private void getTableRowHeaderCellWidth() {
        int lefHeaderChildCount = ((TableRow) mLeftHeaderTable.getChildAt(0)).getChildCount();
        int rightHeaderChildCount = ((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount();

        for (int x = 0; x < (lefHeaderChildCount + rightHeaderChildCount); x++) {
            if (x == 0) {
                rightHeaderWidth[x] = viewWidth(((TableRow) mLeftHeaderTable.getChildAt(0)).getChildAt(x));
            } else {
                rightHeaderWidth[x] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(x - 1));
            }
        }
    }

    // read a view's height
    private int viewHeight(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredHeight();
    }

    private int viewWidth(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredWidth();
    }


    private void resizeMemberNameWidth() {
        // TODO Auto-generated method stub
        int leftHeadertWidth = viewWidth(mLeftHeaderTable);
        int leftContentWidth = viewWidth(mLeftContentTable);

        if (leftHeadertWidth < leftContentWidth) {
            mLeftHeaderTable.getLayoutParams().width = leftContentWidth;
        } else {
            mLeftContentTable.getLayoutParams().width = leftHeadertWidth;
        }
    }

    private void resizeBodyTableRowHeight() {

        int leftContentTable_ChildCount = mLeftContentTable.getChildCount();

        for (int x = 0; x < leftContentTable_ChildCount; x++) {

            TableRow leftContentTableRow = (TableRow) mLeftContentTable.getChildAt(x);
            TableRow rightContentTableRow = (TableRow) mRightContentTable.getChildAt(x);

            int rowLeftHeight = viewHeight(leftContentTableRow);
            int rowRightHeight = viewHeight(rightContentTableRow);

            TableRow tableRow = rowLeftHeight < rowRightHeight ? leftContentTableRow : rightContentTableRow;
            int finalHeight = rowLeftHeight > rowRightHeight ? rowLeftHeight : rowRightHeight;

            this.matchLayoutHeight(tableRow, finalHeight);
        }

    }

    private void matchLayoutHeight(TableRow tableRow, int height) {

        int tableRowChildCount = tableRow.getChildCount();

        // if a TableRow has only 1 child
        if (tableRow.getChildCount() == 1) {

            View view = tableRow.getChildAt(0);
            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
            params.height = height - (params.bottomMargin + params.topMargin);

            return;
        }

        // if a TableRow has more than 1 child
        for (int x = 0; x < tableRowChildCount; x++) {

            View view = tableRow.getChildAt(x);

            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

            if (!isTheHeighestLayout(tableRow, x)) {
                params.height = height - (params.bottomMargin + params.topMargin);
                return;
            }
        }

    }

    // check if the view has the highest height in a TableRow
    private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

        int tableRowChildCount = tableRow.getChildCount();
        int heighestViewPosition = -1;
        int viewHeight = 0;

        for (int x = 0; x < tableRowChildCount; x++) {
            View view = tableRow.getChildAt(x);
            int height = this.viewHeight(view);

            if (viewHeight < height) {
                heighestViewPosition = x;
                viewHeight = height;
            }
        }
        return heighestViewPosition == layoutPosition;
    }



    @Override
    public void onClick(View view) {

        sAmountSavingsAmounts = new String[samountFields.size()];
        sintrestSavingsAmount = new String[sintrestFields.size()];

        Log.d("Saving", "sSavingsAmounts size : " + samountFields.size() + "");

        switch (view.getId()) {

            case R.id.ilr_fragment_Submit_button:
                try {
                    mlrlist = new ArrayList<>();
                    sAmount_Total = 0;
                    sIntrest_Total = 0;
                    sSendToServer_Amount = "";
                    sSendToServer_Intrest = "";
                    sum_of_internalloan_repayment=0;

                    confirmArr = new String[mSize];

                    // Do edit values here
                    int Outstandingwarning = 0;

                    for (int i = 0; i < outstandingamt.size(); i++) {

                        // sAmountSavingsAmounts[i] = samountFields.get(i).getText().toString();

                        if (samountFields.get(i).getText().toString().equals("")) {
                            sAmountSavingsAmounts[i] = "0";
                        } else {
                            sAmountSavingsAmounts[i] = samountFields.get(i).getText().toString();
                        }

                        if (sintrestFields.get(i).getText().toString().equals("")) {

                            sintrestSavingsAmount[i] = "0";
                        } else {

                            sintrestSavingsAmount[i] = sintrestFields.get(i).getText().toString();
                        }


                        String outstnd = outstandingamt.get(i).getLoanOutstanding();
                        int out = 0;

                        if ((outstnd == "") || (outstnd == null) || (outstnd == "null")) {
                            out = 0;
                        } else {
                            out = (int) Double.parseDouble(outstnd);
                        }

                        try {
                            OfflineDto offline = new OfflineDto();

                            offline.setAnimatorId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                            offline.setShgId(shgDto.getShgId());
                            // offline.setLoanId(loan_id);
//                            offline.setLastTransactionDateTime(shgDto.getLastTransactionDate());
                            offline.setLastTransactionDateTime(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                            offline.setModifiedDateTime(System.currentTimeMillis() + "");
                            offline.setModeOCash("0");
                            offline.setIs_transaction_tdy("1.0");
                            offline.setTxType(NewDrawerScreen.MEMBER_LOAN_REPAYMENT);
                            offline.setTxSubtype(AppStrings.InternalLoan);
                            offline.setMemberName(outstandingamt.get(i).getMemberName());
                            offline.setMemberId(outstandingamt.get(i).getMemberId() + "");
                            offline.setMem_amount(sAmountSavingsAmounts[i] + "");
                            offline.setMemInterest(sintrestSavingsAmount[i] + "");
                            int out1 = (int) Double.parseDouble(outstandingamt.get(i).getLoanOutstanding()) - (int) Double.parseDouble(sAmountSavingsAmounts[i]);
                            offline.setMem_os(out1 + "");
                            //  offline.setMem_os(out + "");

                            offlineDBData.add(offline);
                            //   offline.add(offline);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        if (out >= Integer.parseInt(sAmountSavingsAmounts[i].trim())) {

                            sAmount_Total += Integer.parseInt((!samountFields.get(i).getText().toString().equals("") && samountFields.get(i).getText().toString().length() > 0) ? samountFields.get(i).getText().toString() : "0");
                            // sintrestSavingsAmount[i] = sintrestFields.get(i).getText().toString();
                            sIntrest_Total += Integer.parseInt((!sintrestFields.get(i).getText().toString().equals("") && sintrestFields.get(i).getText().toString().length() > 0) ? sintrestFields.get(i).getText().toString() : "0");
                            RepaymentDetails rpl = new RepaymentDetails();
                            rpl.setMemberId(outstandingamt.get(i).getMemberId());
                            rpl.setRepaymentAmount(sAmountSavingsAmounts[i]);
                            rpl.setInterest(sintrestSavingsAmount[i]);
                            //sAmountSavingsAmounts[i] = samountFields.get(i).getText().toString();


                            mlrlist.add(rpl);

//                            Utils.sum_of_internalloan_repayment = sAmount_Total + sIntrest_Total;
                            sum_of_internalloan_repayment = sAmount_Total + sIntrest_Total;
                            Log.d("sum_of_saving", "" + sum_of_internalloan_repayment);
                        } else {
                            Outstandingwarning = 1;

                            break;

                        }

                    }

                   /* for (int i = 0; i < sAmountSavingsAmounts.length; i++) {

                        // Principle Amount
                        sAmountSavingsAmounts[i] = String.valueOf(samountFields.get(i).getText().toString());
                        if ((sAmountSavingsAmounts[i].equals("")) || (sAmountSavingsAmounts[i] == null)) {
                            sAmountSavingsAmounts[i] = nullAmount;
                        }

                        // Interest
                        sintrestSavingsAmount[i] = String.valueOf(sintrestFields.get(i).getText().toString());
                        if ((sintrestSavingsAmount[i].equals("")) || (sintrestSavingsAmount[i] == null)) {
                            sintrestSavingsAmount[i] = nullAmount;
                        }

                        if (sAmountSavingsAmounts[i].length() > 0) { // match
                            // a
                            // decimal
                            // number

                            int amount = (int) Math.round(Double.parseDouble(sAmountSavingsAmounts[i]));
                            sAmountSavingsAmounts[i] = String.valueOf(amount);
                        }

                        if (sintrestSavingsAmount[i].length() > 0) { // match
                            // a
                            // decimal
                            // number

                            int amount = (int) Math.round(Double.parseDouble(sintrestSavingsAmount[i]));
                            sintrestSavingsAmount[i] = String.valueOf(amount);
                        }

                        if (Integer.parseInt(outstandingamt.get(i).getLoanOutstanding()) >= Integer.parseInt(sAmountSavingsAmounts[i].trim())) {


                        } else {
                            TastyToast.makeText(getActivity(), com.yesteam.eshakti.appConstants.AppStrings.groupRepaidAlert, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);


                        }
                    }*/

                    if (Outstandingwarning == 0) {
//                        if (Utils.sum_of_internalloan_repayment < Integer.parseInt(shgDto.getCashInHand())) {
                            if ((sAmount_Total != 0) || (sIntrest_Total != 0)) {

                                confirmationDialog = new Dialog(getActivity());

                                LayoutInflater inflater = getActivity().getLayoutInflater();
                                View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);

                                ViewGroup.LayoutParams lParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                dialogView.setLayoutParams(lParams);

                                TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                                confirmationHeader.setText("" + AppStrings.confirmation);

                                TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                                DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
                                Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
                                String dateStr = simple.format(d);
                                TextView transactdate = (TextView)dialogView.findViewById(R.id.transactdate);
                                transactdate.setText(dateStr);

                                for (int i = 0; i < outstandingamt.size(); i++) {

                                    TableRow indv_SavingsRow = new TableRow(getActivity());

                                    TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                                    contentParams.setMargins(10, 5, 10, 5);

                                    TextView memberName_Text = new TextView(getActivity());
                                    memberName_Text.setText(com.oasys.eshakti.OasysUtils.GetSpanText.getSpanString(getActivity(),
                                            outstandingamt.get(i).getMemberName()));
                                    memberName_Text.setTextColor(R.color.black);
                                    memberName_Text.setPadding(5, 5, 5, 5);
                                    memberName_Text.setSingleLine(true);
                                    memberName_Text.setLayoutParams(contentParams);
                                    indv_SavingsRow.addView(memberName_Text);

                                    TextView confirm_values = new TextView(getActivity());
                                    confirm_values
                                            .setText(com.oasys.eshakti.OasysUtils.GetSpanText.getSpanString(getActivity(), String.valueOf(sAmountSavingsAmounts[i])));
                                    confirm_values.setTextColor(R.color.black);
                                    confirm_values.setPadding(5, 5, 5, 5);
                                    confirm_values.setGravity(Gravity.RIGHT);
                                    confirm_values.setLayoutParams(contentParams);
                                    indv_SavingsRow.addView(confirm_values);

                                    TextView confirm_VSvalues = new TextView(getActivity());
                                    confirm_VSvalues
                                            .setText(com.oasys.eshakti.OasysUtils.GetSpanText.getSpanString(getActivity(), String.valueOf(sintrestSavingsAmount[i])));
                                    confirm_VSvalues.setTextColor(R.color.black);
                                    confirm_VSvalues.setPadding(5, 5, 5, 5);
                                    confirm_VSvalues.setGravity(Gravity.RIGHT);
                                    confirm_VSvalues.setLayoutParams(contentParams);
                                    indv_SavingsRow.addView(confirm_VSvalues);

                                    confirmationTable.addView(indv_SavingsRow,
                                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                }
                                View rullerView = new View(getActivity());
                                rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                                rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                                // 229,
                                // 242));
                                confirmationTable.addView(rullerView);

                                TableRow totalRow = new TableRow(getActivity());

                                TableRow.LayoutParams totalParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                                totalParams.setMargins(10, 5, 10, 5);

                                TextView totalText = new TextView(getActivity());
                                totalText.setText(com.oasys.eshakti.OasysUtils.GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
                                totalText.setTextColor(R.color.black);
                                totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
                                totalText.setLayoutParams(totalParams);
                                totalRow.addView(totalText);

                                TextView totalAmount = new TextView(getActivity());
                                totalAmount.setText(com.oasys.eshakti.OasysUtils.GetSpanText.getSpanString(getActivity(), String.valueOf(sAmount_Total)));
                                totalAmount.setTextColor(R.color.black);
                                totalAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                                totalAmount.setGravity(Gravity.RIGHT);
                                totalAmount.setLayoutParams(totalParams);
                                totalRow.addView(totalAmount);

                                TextView totalVSAmount = new TextView(getActivity());
                                totalVSAmount.setText(com.oasys.eshakti.OasysUtils.GetSpanText.getSpanString(getActivity(), String.valueOf(sIntrest_Total)));
                                totalVSAmount.setTextColor(R.color.black);
                                totalVSAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                                totalVSAmount.setGravity(Gravity.RIGHT);
                                totalVSAmount.setLayoutParams(totalParams);
                                totalRow.addView(totalVSAmount);

                                confirmationTable.addView(totalRow,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                                mEdit_RaisedButton.setText(AppStrings.edit);
                                mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                                // 205,
                                // 0));
                                mEdit_RaisedButton.setOnClickListener(this);

                                mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                                mOk_RaisedButton.setText(AppStrings.yes);
                                mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                                mOk_RaisedButton.setOnClickListener(this);

                                confirmationDialog.getWindow()
                                        .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                confirmationDialog.setCanceledOnTouchOutside(false);
                                confirmationDialog.setContentView(dialogView);
                                confirmationDialog.setCancelable(true);
                                confirmationDialog.show();

                                ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                                margin.leftMargin = 10;
                                margin.rightMargin = 10;
                                margin.topMargin = 10;
                                margin.bottomMargin = 10;
                                margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

                            } else {

                                TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT,
                                        TastyToast.WARNING);

                                sAmount_Total = 0;
                                sIntrest_Total = 0;
                                sSendToServer_Amount = "";
                                sSendToServer_Intrest = "";
                            }
//                        } else {
//                            TastyToast.makeText(getActivity(), AppStrings.cashinHandAlert, TastyToast.LENGTH_SHORT,
//                                    TastyToast.WARNING);
//                        }
                    } else {
                        TastyToast.makeText(getActivity(), AppStrings.groupRepaidAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.fragment_Edit:
                sAmount_Total = 0;
                sIntrest_Total = 0;
                sSendToServer_Amount = "";
                sSendToServer_Intrest = "";
                mSubmit_Raised_Button.setClickable(true);

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();

                break;
            case R.id.frag_Ok:
                try {

                    if (confirmationDialog.isShowing())
                        confirmationDialog.dismiss();


                    if (networkConnection.isNetworkAvailable()) {
                        if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 1) {
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            return;
                        }
                        // NOTHING TO DO::
                    } else {

                        if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 2) {
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            return;
                        }

                    }


                    Calendar calender = Calendar.getInstance();

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String formattedDate = df.format(calender.getTime());

                    DateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
                    Date d = new Date(Long.parseLong(shgDto.getLastTransactionDate()));
                    String dateStr = simple.format(d);
//                    Log.d("a",Dialog_New_TransactionDate.cg.getLastTransactionDate());
                    InternalLoanRepaymentRequestDto mlr = new InternalLoanRepaymentRequestDto();
                    mlr.setShgId(shgId);
                    mlr.setModeOfCash("0");
//                    mlr.setTransactionDate(dateStr);
                    mlr.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                    // mlr.setTransactionDate(shgDto.getLastTransactionDate());
                    mlr.setMobileDate(formattedDate);
                    mlr.setRepaymentDetails(mlrlist);

                    String sreqString = new Gson().toJson(mlr);

//                    if(EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1"))
//                    {
                    if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
                        if (networkConnection.isNetworkAvailable()) {
                            onTaskStarted();
                            RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.INTERNALLOANREPAYMENT, sreqString, getActivity(), ServiceType.INTERNALLOANREPAY);
                        } else {
                            if (TransactionTable.getLoginFlag(AppStrings.InternalLoan).size() <= 0 || (!TransactionTable.getLoginFlag(AppStrings.InternalLoan).get(TransactionTable.getLoginFlag(AppStrings.InternalLoan).size() - 1).getLoginFlag().equals("1"))) {
                                if(grploanDetails.size()>0)
                                {
                                    MemberLoanRepayment memberLoanRepayment = new MemberLoanRepayment();
                                    Bundle bundles = new Bundle();
//                                    bundles.putString("stepwise", EShaktiApplication.flag);
                                    bundles.putString("loan_id", grploanDetails.get(0).getLoanId());
                                    bundles.putString("loan_type", grploanDetails.get(0).getLoanTypeName());
                                    bundles.putString("account_number", grploanDetails.get(0).getAccountNumber());
                                    bundles.putString("bank_name", grploanDetails.get(0).getBankName());
                                    memberLoanRepayment.setArguments(bundles);

//                                    updateILOS();
//                                    updateCIH();
                                    insertMR();
                                    NewDrawerScreen.showFragment(memberLoanRepayment);
                                    confirmationDialog.dismiss();

                                }

                            } else
                                TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                                        TastyToast.WARNING);
                        }

                    }
                    else {
                        if (networkConnection.isNetworkAvailable()) {
                            onTaskStarted();
                            RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.INTERNALLOANREPAYMENT, sreqString, getActivity(), ServiceType.INTERNALLOANREPAY);
                        } else {
                            if (TransactionTable.getLoginFlag(AppStrings.InternalLoan).size() <= 0 || (!TransactionTable.getLoginFlag(AppStrings.InternalLoan).get(TransactionTable.getLoginFlag(AppStrings.InternalLoan).size() - 1).getLoginFlag().equals("1"))) {
                                insertMR();
                            } else
                                TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                                        TastyToast.WARNING);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.fragment_Previousbutton:
                break;
            case R.id.ilr_fragment_skipbutton:

               /* try {
                    Calendar calender = Calendar.getInstance();

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String formattedDate = df.format(calender.getTime());

                    DateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
                    Date d = new Date(Long.parseLong(shgDto.getLastTransactionDate()));
                    String dateStr = simple.format(d);
                    InternalLoanRepaymentRequestDto mlr = new InternalLoanRepaymentRequestDto();
                    mlr.setShgId(shgId);
                    mlr.setModeOfCash("0");
                    mlr.setTransactionDate(dateStr);
                    // mlr.setTransactionDate(shgDto.getLastTransactionDate());
                    mlr.setMobileDate(formattedDate);
                    mlr.setRepaymentDetails(mlrlist);

                    String sreqString = new Gson().toJson(mlr);
                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();
                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.INTERNALLOANREPAYMENT, sreqString, getActivity(), ServiceType.INTERNALLOANREPAY);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
                resume_value=true;
               Log.d("size",""+Utils.loanTypes.size());

//                if (EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1")) {

                if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
                    if (Utils.loanTypes.size() > 0) {


                        MemberLoanRepayment memberLoanRepayment = new MemberLoanRepayment();
                        Bundle bundles = new Bundle();
//                        bundles.putString("stepwise", EShaktiApplication.flag);
                        bundles.putString("loan_id", Utils.loanTypes.get(0).getLoanId());
                        bundles.putString("loan_type", Utils.loanTypes.get(0).getLoanTypeName());
                        bundles.putString("account_number", Utils.loanTypes.get(0).getAccountNumber());
                        bundles.putString("bank_name", Utils.loanTypes.get(0).getBankName());
                        memberLoanRepayment.setArguments(bundles);

//                        updateILOS();
//                        updateCIH();

                        NewDrawerScreen.showFragment(memberLoanRepayment);
//                        confirmationDialog.dismiss();

                    } else {
                        LD_IL_Entry pl_DisbursementFragment = new LD_IL_Entry();
//                        MainFragment mainFragment =new MainFragment();
                        Bundle bundles = new Bundle();
//                        bundles.putString("stepwise", EShaktiApplication.flag);
//                        pl_DisbursementFragment.setArguments(bundles);
                        Toast.makeText(getActivity(),"There is no Loan Available",Toast.LENGTH_LONG).show();
                        NewDrawerScreen.showFragment(pl_DisbursementFragment);
                    }
                }

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(resume_value==true)
        {
//            if (EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1")) {

            if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
                if (Utils.loanTypes.size() > 0) {


                    MemberLoanRepayment memberLoanRepayment = new MemberLoanRepayment();
                    Bundle bundles = new Bundle();
//                    bundles.putString("stepwise", EShaktiApplication.flag);
                    bundles.putString("loan_id", Utils.loanTypes.get(0).getLoanId());
                    bundles.putString("loan_type", Utils.loanTypes.get(0).getLoanTypeName());
                    bundles.putString("account_number", Utils.loanTypes.get(0).getAccountNumber());
                    bundles.putString("bank_name", Utils.loanTypes.get(0).getBankName());
                    memberLoanRepayment.setArguments(bundles);

                    updateILOS();
                    updateCIH();

                    NewDrawerScreen.showFragment(memberLoanRepayment);
//                    confirmationDialog.dismiss();

                } else {
                    LD_IL_Entry pl_DisbursementFragment = new LD_IL_Entry();
//                                    MainFragment mainFragment =new MainFragment();
//                    Bundle bundles = new Bundle();
//                    bundles.putString("stepwise", EShaktiApplication.flag);
//                    pl_DisbursementFragment.setArguments(bundles);
                    NewDrawerScreen.showFragment(pl_DisbursementFragment);
                }
            } else if (resdto != null) {

                if (resdto.getStatusCode() == Utils.Success_Code) {
                    resume_value=true;
                    if (confirmationDialog.isShowing()) {
                        confirmationDialog.dismiss();
                    }

                    Utils.showToast(getActivity(), resdto.getMessage());

                    if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                        SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                    }

                    updateILOS();
                    updateCIH();

                    FragmentManager fm = getFragmentManager();
                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    MainFragment mainFragment = new MainFragment();
                    Bundle bundles = new Bundle();
                    bundles.putString("Transaction", MainFragment.Flag_Transaction);
                    mainFragment.setArguments(bundles);
                    NewDrawerScreen.showFragment(mainFragment);
                } else {
//                                    insertMR();

                    if (resdto.getStatusCode() == 401) {

                        Log.e("Group Logout", "Logout Sucessfully");
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                            mProgressDilaog.dismiss();
                            mProgressDilaog = null;
                        }
                    }
                    if (confirmationDialog.isShowing()) {
                        confirmationDialog.dismiss();
                    }
                    Utils.showToast(getActivity(), resdto.getMessage());
                }
            }
        } else {
//                            insertMR();
        }

        }

    private void insertTransaction_tdy()
    {
        OfflineDto offline = new OfflineDto();
        offline.setIs_transaction_tdy("1.0");
        offline.setShgId(shgDto.getShgId());
        SHGTable.updateIstransaction(offline);

    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        try {
            switch (serviceType) {
                case GETINTERNALOUTSTANDINGAMOUNT:
                    responseDto = new Gson().fromJson(result, ResponseDto.class);

                    if (responseDto != null) {
                        if (responseDto.getStatusCode() == Utils.Success_Code) {
                            resume_value = true;
                            outstandingamt = responseDto.getResponseContent().getMemberloanRepaymentList();
                            init();
                        } else {
                            if (responseDto.getStatusCode() == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                    mProgressDilaog.dismiss();
                                    mProgressDilaog = null;
                                }
                            }
                        }

//                    }

//                        if (EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1")) {

                        if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
                            if (responseDto != null) {
                                resume_value=true;
                                for (int i = 0; i < responseDto.getResponseContent().getMemberloanRepaymentList().size(); i++) {
                                    String s = responseDto.getResponseContent().getMemberloanRepaymentList().get(i).getLoanOutstanding();
                                    int store = (int) Double.parseDouble(s);
                                    sum = sum + store;

                                }

                                if (sum == 0) {
                                    mNextButton.setVisibility(View.VISIBLE);
                                }

                            }
                        }
                    }
                    break;
                case INTERNALLOANREPAY:

                    try {
                        if (result != null) {
                             resdto = new Gson().fromJson(result, ResponseDto.class);
                            resume_value=true;
//                            if (EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1")) {

                            if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
                                if (Utils.loanTypes.size() > 0) {


                                    MemberLoanRepayment memberLoanRepayment = new MemberLoanRepayment();
                                    Bundle bundles = new Bundle();
//                                    bundles.putString("stepwise", EShaktiApplication.flag);
                                    bundles.putString("loan_id", Utils.loanTypes.get(0).getLoanId());
                                    bundles.putString("loan_type", Utils.loanTypes.get(0).getLoanTypeName());
                                    bundles.putString("account_number", Utils.loanTypes.get(0).getAccountNumber());
                                    bundles.putString("bank_name", Utils.loanTypes.get(0).getBankName());
                                    memberLoanRepayment.setArguments(bundles);

                                    updateCIH();
                                    updateILOS();

                                    NewDrawerScreen.showFragment(memberLoanRepayment);
                                    confirmationDialog.dismiss();

                                } else {
                                   LD_IL_Entry pl_DisbursementFragment = new LD_IL_Entry();
//                                    MainFragment mainFragment =new MainFragment();
//                                    Bundle bundles = new Bundle();
//                                    bundles.putString("stepwise", EShaktiApplication.flag);
//                                    pl_DisbursementFragment.setArguments(bundles);
                                    NewDrawerScreen.showFragment(pl_DisbursementFragment);
                                }
                            } else if (resdto != null) {

                                if (resdto.getStatusCode() == Utils.Success_Code) {
                                    resume_value=true;
                                    if (confirmationDialog.isShowing()) {
                                        confirmationDialog.dismiss();
                                    }


                                    Utils.showToast(getActivity(), resdto.getMessage());

                                    if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                        SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                                    }
                                    insertTransaction_tdy();
//                                    insertMR();
                                    updateCIH();
                                    updateILOS();

                                    FragmentManager fm = getFragmentManager();
                                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                    MainFragment mainFragment = new MainFragment();
                                    Bundle bundles = new Bundle();
                                    bundles.putString("Transaction", MainFragment.Flag_Transaction);
                                    mainFragment.setArguments(bundles);
                                    NewDrawerScreen.showFragment(mainFragment);
                                } else {
//                                    insertMR();

                                    if (resdto.getStatusCode() == 401) {

                                        Log.e("Group Logout", "Logout Sucessfully");
                                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                            mProgressDilaog.dismiss();
                                            mProgressDilaog = null;
                                        }
                                    }
                                    if (confirmationDialog.isShowing()) {
                                        confirmationDialog.dismiss();
                                    }
                                    Utils.showToast(getActivity(), resdto.getMessage());
                                }
                            }
                        } else {
//                            insertMR();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        insertMR();
                    }

                    break;
                case GETLOANTYPES_STEPWISES:
                    responseDto = new Gson().fromJson(result, ResponseDto.class);
                    Log.d("GETLOANTYPE", responseDto.toString());
                    if (responseDto.getStatusCode() == Utils.Success_Code) {
                        resume_value=true;
                        Utils.loanTypes = responseDto.getResponseContent().getLoansList();
                    } else {
                        if (responseDto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                    }
                    break;
                case NAV_DETAILS:
                    try {
                        if (result != null && result.length() > 0) {
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            Gson gson = gsonBuilder.create();
                            ResponseDto mrDto = gson.fromJson(result, ResponseDto.class);
                             statusCode = mrDto.getStatusCode();
                            Log.d("Main Frag response ", " " + statusCode);
                            if (statusCode == 400 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                                // showMessage(statusCode);

                            } else if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                    mProgressDilaog.dismiss();
                                    mProgressDilaog = null;
                                }
                            } else if (statusCode == Utils.Success_Code) {
                                resume_value=true;
                                csGrp = mrDto.getResponseContent().getCashOfGroup();
                                String cashand = "";
                                String cashbank = "";
                                for (int i = 0; i <csGrp.size() ; i++) {

                                    cashand =csGrp.get(i).getCashInHand();
                                    cashbank=csGrp.get(i).getCashAtBank();

                                }
                               int csahhand_bal = (int) Double.parseDouble(cashand);
                               int csahbank_bal = (int) Double.parseDouble(cashbank);
                                mCashinHand.setText(AppStrings.cashinhand +  csahhand_bal);
                                mCashatBank.setText(AppStrings.cashatBank +  csahbank_bal);
                                SHGTable.updateSHGDetails(csGrp.get(0), shgDto.getId());
                                shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void insertMR() {
        try {
            int cih = 0;
            cih = (int) Double.parseDouble(shgDto.getCashInHand()) + (sAmount_Total + sIntrest_Total);

            int value = (MySharedPreference.readInteger(getActivity(), MySharedPreference.MR_IL_COUNT, 0) + 1);
            if (value > 0)
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.MR_IL_COUNT, value);

            for (OfflineDto ofdto : offlineDBData) {
                ofdto.setCashAtBank(shgDto.getCashAtBank());
                ofdto.setCashInhand(cih + "");
                ofdto.setMrCount(value + "");
                TransactionTable.insertTransILData(ofdto);
            }

            updateILOS();

            //     TransactionTable.insertTransMRData(offlineDBData);   //TODO::   CIH,CAB & LT / Dont credit twice offline on same loan / Credit the same loan after logout

            if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
            }


            updateCIH();


            FragmentManager fm = getFragmentManager();
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            NewDrawerScreen.showFragment(new MainFragment());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateILOS() {
        for (OfflineDto ofdto : offlineDBData) {
            LoanTable.updateILOSDetails(ofdto);
        }
    }

    private void updateCIH() {
        //TODO:::: CIH & CAB
        try {


            int cih = 0;
            cih = (int) Double.parseDouble(shgDto.getCashInHand()) + (sAmount_Total + sIntrest_Total);
            String cihstr = "", cabstr = "", lastTranstr = "";
            cihstr = cih + "";
            cabstr = shgDto.getCashAtBank();
            lastTranstr = offlineDBData.get(0).getLastTransactionDateTime();
            CashOfGroup csg = new CashOfGroup();
            csg.setCashInHand(cihstr);
            csg.setCashAtBank(cabstr);
            csg.setLastTransactionDate(lastTranstr);
            SHGTable.updateSHGDetails(csg, shgDto.getId());
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}

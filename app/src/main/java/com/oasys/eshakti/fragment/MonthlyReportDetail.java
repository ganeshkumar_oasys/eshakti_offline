package com.oasys.eshakti.fragment;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Adapter.LoanreportAdapter;
import com.oasys.eshakti.Adapter.MonthlyReportAdapter;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.RequestDto.MonthYearDto;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.database.SHGTable;


public class MonthlyReportDetail extends Fragment implements NewTaskListener {
    TextView Name, savingsamount;
    private String mYear, memid;
    private int mMonth;
    private View view;
    ResponseDto responseDto;
    private ListOfShg shgDto;

    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private MonthlyReportAdapter monthlyReportAdapter;
    private LoanreportAdapter loanreportAdapter;
    private RecyclerView monthlyrecyclerview,loanreportsrecyclerview;
    private LinearLayoutManager linearLayoutManager,linearLayoutManager1;

    public MonthlyReportDetail() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_monthly_report_detail, container, false);

        Bundle bundle = getArguments();
        mMonth = bundle.getInt("month_key");
        mYear = bundle.getString("year_key");
        memid = bundle.getString("member_id");
        mMonthYearSubmit(mMonth, mYear);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        inIt();
    }

    private void inIt() {
        try {
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));


            mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) view.findViewById(R.id.cashinhand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) view.findViewById(R.id.cashatbank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            loanreportsrecyclerview = (RecyclerView)view.findViewById(R.id.  loanreports);
            linearLayoutManager1 = new LinearLayoutManager(getActivity());
            loanreportsrecyclerview.setLayoutManager(linearLayoutManager1);
            loanreportsrecyclerview.setHasFixedSize(true);
            loanreportsrecyclerview.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));


            monthlyrecyclerview = (RecyclerView)view.findViewById(R.id.list);
            linearLayoutManager = new LinearLayoutManager(getActivity());
            monthlyrecyclerview.setLayoutManager(linearLayoutManager);
            monthlyrecyclerview.setHasFixedSize(true);
            monthlyrecyclerview.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));


            Name = (TextView) view.findViewById(R.id.Name);
            Name.setText(MySharedPreference.readString(getActivity(), MySharedPreference.MEM_NAME_SUMMARY, ""));

            savingsamount = (TextView) view.findViewById(R.id.savingsamount);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void mMonthYearSubmit(int mMonth, String mYear) {
        MonthYearDto monthYearDto = new MonthYearDto();
        monthYearDto.setMonth(mMonth);
        monthYearDto.setYear(mYear);
        String nMonthYearSubmit = new Gson().toJson(monthYearDto);
        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
            RestClient.getRestClient(MonthlyReportDetail.this).callRestWebService(Constants.BASE_URL + Constants.MONTH_REPORT + memid, nMonthYearSubmit, getActivity(), ServiceType.MONTH_YEAR_REPORT);
        } else {
            Utils.showToast(getActivity(), "Network Not Available");
        }
    }



    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        switch (serviceType) {
            case MONTH_YEAR_REPORT:
                try {
                    responseDto = new Gson().fromJson(result, ResponseDto.class);
                    if (responseDto.getStatusCode() == (Utils.Success_Code)) {
                        Utils.showToast(getActivity(), responseDto.getMessage());

                        savingsamount.setText(responseDto.getResponseContent().getTotalSavingsAmount());

                        if(responseDto.getResponseContent().getMembermonthlyReport()!=null)
                        {
                            monthlyReportAdapter =new MonthlyReportAdapter(getActivity(),responseDto.getResponseContent().getMembermonthlyReport());
                            monthlyrecyclerview.setAdapter(monthlyReportAdapter);
                        }

                        if(responseDto.getResponseContent().getMemberreportloansummary()!=null)
                        {
                            loanreportAdapter = new LoanreportAdapter(getActivity(),responseDto.getResponseContent().getMemberinternallaon());
                            loanreportsrecyclerview.setAdapter(loanreportAdapter);
                        }


                    } else {
                        if (responseDto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), "Network Error");
//                        Utils.showToastMethod(LoginActivity.this, message);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("excep", "" + e);
                }
                break;
        }
    }
}

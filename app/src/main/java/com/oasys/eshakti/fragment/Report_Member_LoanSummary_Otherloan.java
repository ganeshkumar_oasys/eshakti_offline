package com.oasys.eshakti.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Adapter.Report_MemberReport_LoanSummary_otherLoanAdapter;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberLoanSummary;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.database.MemberTable;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.Service.NewTaskListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Report_Member_LoanSummary_Otherloan extends Fragment implements NewTaskListener {
    private View view;
    private String loan_id = "";
    private String mem_id = "";
    private String loantype = "";
    private String bankname = "";
    String shgId = "";
    private NetworkConnection networkConnection;
    private ListOfShg shgDto;
    private ArrayList<MemberLoanSummary> memberLoanSummaries;
    private int mSize;
    ResponseDto responseDto;
    private Report_MemberReport_LoanSummary_otherLoanAdapter report_memberReport_loanSummary_otherLoanAdapter;
    private RecyclerView reportothermemberloantype_expandablelistview;
    private LinearLayoutManager linearLayoutManager;
    TextView text_outstanding, os, tlt, tlt_lbl;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView mHeader;
    private TextView Name, date, inrt, amt;


    public Report_Member_LoanSummary_Otherloan() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_report__member__loan_summary__otherloan, container, false);

        try {
            mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
            Bundle bundle = getArguments();
            loan_id = bundle.getString("loan_id");
            Log.d("loan", loan_id);
            mem_id = bundle.getString("memid");
            Log.d("Mem4", mem_id);
            loantype = bundle.getString("loan_type");
            bankname = bundle.getString("bank_name");

            mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) view.findViewById(R.id.ch);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) view.findViewById(R.id.cb);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            Name = (TextView) view.findViewById(R.id.Name);
            Name.setText(MySharedPreference.readString(getActivity(), MySharedPreference.MEM_NAME_SUMMARY, ""));
        //    Name.setTypeface(LoginActivity.sTypeface);

            mHeader = (TextView) view.findViewById(R.id.fragmentHeader);
            mHeader.setText(loantype + " " + AppStrings.loanSummary);
            mHeader.setTypeface(LoginActivity.sTypeface);

            date = (TextView) view.findViewById(R.id.date);
            date.setText(AppStrings.date);
            date.setTypeface(LoginActivity.sTypeface);
            amt = (TextView) view.findViewById(R.id.amt);
            amt.setText(AppStrings.amount);
            amt.setTypeface(LoginActivity.sTypeface);
            inrt = (TextView) view.findViewById(R.id.inrt);
            inrt.setText(AppStrings.interest);
            inrt.setTypeface(LoginActivity.sTypeface);

            Log.d("LOANID", loan_id);

            reportothermemberloantype_expandablelistview = (RecyclerView) view.findViewById(R.id.reportothermemberloantype_expandablelistview);
            linearLayoutManager = new LinearLayoutManager(getActivity());
            reportothermemberloantype_expandablelistview.setLayoutManager(linearLayoutManager);
            reportothermemberloantype_expandablelistview.setHasFixedSize(true);
            reportothermemberloantype_expandablelistview.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            text_outstanding = (TextView) view.findViewById(R.id.member_otherLoan_values);
            os = (TextView) view.findViewById(R.id.os_lbl);
            os.setTypeface(LoginActivity.sTypeface);
            os.setText(AppStrings.outstanding + " :");
            tlt_lbl = (TextView) view.findViewById(R.id.tlt_lbl);
            tlt_lbl.setTypeface(LoginActivity.sTypeface);
            tlt_lbl.setText("TOTAL AVAILED" + " :");
            tlt = (TextView) view.findViewById(R.id.tlt);
            tlt.setTypeface(LoginActivity.sTypeface);

            text_outstanding.setTypeface(LoginActivity.sTypeface);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();


        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        memberLoanSummaries = new ArrayList<>();

        if (networkConnection.isNetworkAvailable()) {
            shgId = shgDto.getShgId();
//            String url ="http://192.168.3.253:8631/api/eshakti/apkreports/memberloansummary/member/loanid/?memberId=84dec57a-5a80-46eb-aca2-7bb5472682f0&loanId=74a5ec57-a64b-4822-bc16-a760a1ccd187";
            String url = Constants.BASE_URL + Constants.MEMBERREPORT_LOANSUMMARY_OTHERLOANS + "?memberId=" + mem_id + "&loanId=" + loan_id;
            RestClient.getRestClient(this).callWebServiceForGetMethod(url, getActivity(), ServiceType.REPORT_MEMBERREPORT_LOANSUMMARY_OTHERLOANS);
        }

    }


    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        try {
            switch (serviceType) {
                case REPORT_MEMBERREPORT_LOANSUMMARY_OTHERLOANS:
                    if (result != null && result.length() > 0) {
                        responseDto = new Gson().fromJson(result, ResponseDto.class);
                        if (responseDto.getStatusCode() == Utils.Success_Code) {
                            memberLoanSummaries = responseDto.getResponseContent().getMemberLoanSummary();
                            Log.d("check", "" + responseDto.getResponseContent().getMemberLoanSummary());

                            report_memberReport_loanSummary_otherLoanAdapter = new Report_MemberReport_LoanSummary_otherLoanAdapter(getActivity(), responseDto.getResponseContent().getMemberLoanSummary());
                            reportothermemberloantype_expandablelistview.setAdapter(report_memberReport_loanSummary_otherLoanAdapter);
                            if (responseDto.getResponseContent().getOtherloanOutstanding() != null)
                                text_outstanding.setText("₹" + responseDto.getResponseContent().getOtherloanOutstanding());
                            if (responseDto.getResponseContent().getTotalAmount() != null)
                                tlt.setText("₹" + responseDto.getResponseContent().getTotalAmount());
                        }else {
                            if (responseDto.getStatusCode() == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                            }
                        }
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}

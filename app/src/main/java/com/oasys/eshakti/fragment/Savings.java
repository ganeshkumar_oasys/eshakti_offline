package com.oasys.eshakti.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Dialogue.Dialog_New_TransactionDate;
import com.oasys.eshakti.Dto.CashOfGroup;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.Dto.OfflineDto;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.Dto.SavingRequest;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.GetSpanText;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.MemberTable;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.database.TransactionTable;
import com.oasys.eshakti.views.CustomHorizontalScrollView;
import com.oasys.eshakti.views.Get_EdiText_Filter;
import com.oasys.eshakti.views.TextviewUtils;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Savings extends Fragment implements View.OnClickListener, NewTaskListener {

    private View rootView;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView mHeader, mAutoFilllabel, mMemberName;
    private LinearLayout mMemberNameLayout;
    private TableLayout mLeftHeaderTable;
    private int mSize;
    private TableLayout mRightHeaderTable;
    private TableLayout mLeftContentTable;
    private TableLayout mRightContentTable;
    private CustomHorizontalScrollView mHSRightHeader;
    private CustomHorizontalScrollView mHSRightContent;
    private Button mSubmit_Raised_Button;
    private Button mPerviousButton;
    private Button mNextButton;
    String width[] = {AppStrings.memberName, AppStrings.savingsAmount, AppStrings.voluntarySavings};
    int[] rightHeaderWidth = new int[width.length];
    int[] rightContentWidth = new int[width.length];
    private EditText mSavings_values;
    private EditText mVSavings_values;
    private List<EditText> sSavingsFields;
    private List<EditText> sVSavingsFields;
    public String[] sSavingsAmounts;
    public String[] sVSavingsAmount;
    private Button mEdit_RaisedButton;
    private Button mOk_RaisedButton;
    private List<MemberList> memList;
    private int sSavings = 0;
    private int vSavings = 0;
    private ArrayList<MemberList> arrMem;
    private ArrayList<OfflineDto> offlineData = new ArrayList<>();
    private ListOfShg shgDto;
    private Dialog mProgressDilaog;
    private NetworkConnection networkConnection;
    private CheckBox mAutoFill;
//    private String flag = "0";
    public static int sum_of_savings;
    boolean resume_value=false;
    int statusCode;
    String message;
    String channel;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.saving_fragment, container, false);

//        Attendance.flag = "0";
//        Bundle bundle = getArguments();
//        if (bundle != null) {
//            EShaktiApplication.flag = bundle.getString("stepwise");
//            Log.d("LOANID", EShaktiApplication.flag);
//        }

//        SharedPreferences shared = getActivity().getSharedPreferences("stepwise", MODE_PRIVATE);
//        channel = shared.getString("key","");
//        Log.d("TAG",channel);

        return rootView;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
            menu.findItem(R.id.action_home).setVisible(false);
            menu.findItem(R.id.action_logout).setVisible(false);
            menu.findItem(R.id.menu_logout).setVisible(false);
            menu.findItem(R.id.action_grouplist).setVisible(false);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.INVISIBLE);
        }
        else
        {
            menu.findItem(R.id.action_home).setVisible(true);
            menu.findItem(R.id.action_logout).setVisible(true);
            menu.findItem(R.id.menu_logout).setVisible(true);
            menu.findItem(R.id.action_grouplist).setVisible(true);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.VISIBLE);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));


        if (MySharedPreference.readInteger(getActivity(), MySharedPreference.SAVING_COUNT, 0) <= 0)
            MySharedPreference.writeInteger(getActivity(), MySharedPreference.SAVING_COUNT, 0);

//        Log.d("date",Dialog_New_TransactionDate.cg.getLastTransactionDate());
        arrMem = new ArrayList<>();
        offlineData = new ArrayList<>();

        init();


      /*  Log.d("flagvalue",EShaktiApplication.flag);

        if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
            NewDrawerScreen.item1.setVisible(false);
            NewDrawerScreen.item2.setVisible(false);
            NewDrawerScreen.item.setVisible(false);
            NewDrawerScreen.logOutItem.setVisible(false);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.INVISIBLE);
        }
        else
        {
            NewDrawerScreen.item1.setVisible(true);
            NewDrawerScreen.item2.setVisible(true);
            NewDrawerScreen.item.setVisible(true);
            NewDrawerScreen.logOutItem.setVisible(true);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.VISIBLE);
        }*/
    }

    private void init() {

        sSavingsFields = new ArrayList<EditText>();
        sVSavingsFields = new ArrayList<EditText>();
        try {
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);
            /** UI Mapping **/
            mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);
            mHeader.setText(AppStrings.savings);
            mHeader.setTypeface(LoginActivity.sTypeface);

            mAutoFilllabel = (TextView) rootView.findViewById(R.id.autofillLabel);
            mAutoFilllabel.setText(AppStrings.autoFill);
            mAutoFilllabel.setTypeface(LoginActivity.sTypeface);
            mAutoFilllabel.setVisibility(View.VISIBLE);

            mAutoFill = (CheckBox) rootView.findViewById(R.id.autoFill);
            mAutoFill.setVisibility(View.VISIBLE);
            mAutoFill.setOnClickListener(this);

            mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
            mMemberName = (TextView) rootView.findViewById(R.id.member_name);
            // mMemberName.setTypeface(LoginActivity.sTypeface);

            Log.d("Savings", String.valueOf(mSize));

            mLeftHeaderTable = (TableLayout) rootView.findViewById(R.id.LeftHeaderTable);
            mRightHeaderTable = (TableLayout) rootView.findViewById(R.id.RightHeaderTable);
            mLeftContentTable = (TableLayout) rootView.findViewById(R.id.LeftContentTable);
            mRightContentTable = (TableLayout) rootView.findViewById(R.id.RightContentTable);

            mHSRightHeader = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightHeaderHScrollView);
            mHSRightContent = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightContentHScrollView);


            mHSRightHeader.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {

                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub

                    mHSRightContent.scrollTo(l, 0);

                }
            });

            mHSRightContent.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
                @Override
                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub
                    mHSRightHeader.scrollTo(l, 0);
                }
            });

            TableRow leftHeaderRow = new TableRow(getActivity());

            TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

            TextView mMemberName_headerText = new TextView(getActivity());
            mMemberName_headerText
                    .setText("" + String.valueOf(AppStrings.memberName));
            mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
            mMemberName_headerText.setTextColor(Color.WHITE);
            mMemberName_headerText.setPadding(10, 5, 10, 5);
            mMemberName_headerText.setLayoutParams(lHeaderParams);
            leftHeaderRow.addView(mMemberName_headerText);
            mLeftHeaderTable.addView(leftHeaderRow);

            TableRow rightHeaderRow = new TableRow(getActivity());
            TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(10, 0, 20, 0);
            TextView mSavingsAmount_HeaderText = new TextView(getActivity());
            mSavingsAmount_HeaderText
                    .setText("" + String.valueOf(AppStrings.savingsAmount));
            mSavingsAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
            mSavingsAmount_HeaderText.setTextColor(Color.WHITE);
            mSavingsAmount_HeaderText.setPadding(5, 5, 5, 5);
            mSavingsAmount_HeaderText.setLayoutParams(rHeaderParams);
            mSavingsAmount_HeaderText.setGravity(Gravity.CENTER);
            mSavingsAmount_HeaderText.setSingleLine(true);
            rightHeaderRow.addView(mSavingsAmount_HeaderText);

            TextView mVSavingsAmount_HeaderText = new TextView(getActivity());
            mVSavingsAmount_HeaderText
                    .setText("" + String.valueOf(AppStrings.voluntarySavings));
            mVSavingsAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
            mVSavingsAmount_HeaderText.setTextColor(Color.WHITE);

            mVSavingsAmount_HeaderText.setLayoutParams(rHeaderParams);
            mVSavingsAmount_HeaderText.setSingleLine(true);
            rightHeaderRow.addView(mVSavingsAmount_HeaderText);

            mRightHeaderTable.addView(rightHeaderRow);

            getTableRowHeaderCellWidth();

            for (int i = 0; i < mSize; i++) {
                TableRow leftContentRow = new TableRow(getActivity());

                TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 60, 1f);
                leftContentParams.setMargins(5, 5, 5, 5);

                final TextView memberName_Text = new TextView(getActivity());
                memberName_Text.setText(memList.get(i).getMemberName());
                memberName_Text.setTextColor(R.color.black);
                memberName_Text.setPadding(5, 5, 5, 5);
                memberName_Text.setLayoutParams(leftContentParams);
                memberName_Text.setWidth(200);
                memberName_Text.setSingleLine(true);
                memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
                leftContentRow.addView(memberName_Text);

                mLeftContentTable.addView(leftContentRow);

                TableRow rightContentRow = new TableRow(getActivity());

                TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(rightHeaderWidth[1],
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                rightContentParams.setMargins(20, 5, 20, 5);

                mSavings_values = new EditText(getActivity());
                mSavings_values.setId(i);
                sSavingsFields.add(mSavings_values);
                mSavings_values.setPadding(5, 5, 5, 5);
                mSavings_values.setBackgroundResource(R.drawable.edittext_background);
                mSavings_values.setLayoutParams(rightContentParams);
                mSavings_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
                mSavings_values.setFilters(Get_EdiText_Filter.editText_filter());
                mSavings_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                mSavings_values.setTextColor(R.color.black);
                // mSavings_values.setWidth(150);
                mSavings_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);

                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            Log.d("name",""+mMemberName);
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

                        } else {

                            ((EditText) v).setGravity(Gravity.RIGHT);
                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }

                    }
                });
                rightContentRow.addView(mSavings_values);

                mVSavings_values = new EditText(getActivity());
                mVSavings_values.setId(i);
                sVSavingsFields.add(mVSavings_values);
                mVSavings_values.setPadding(5, 5, 5, 5);
                mVSavings_values.setBackgroundResource(R.drawable.edittext_background);
                mVSavings_values.setLayoutParams(rightContentParams);
                mVSavings_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
                mVSavings_values.setFilters(Get_EdiText_Filter.editText_filter());
                mVSavings_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                mVSavings_values.setTextColor(R.color.black);
                // mVSavings_values.setWidth(150);
                mVSavings_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);

                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            mMemberName.setTypeface(LoginActivity.sTypeface);
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

                        } else {

                            ((EditText) v).setGravity(Gravity.RIGHT);
                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }
                    }
                });
                rightContentRow.addView(mVSavings_values);

                mRightContentTable.addView(rightContentRow);

            }

            resizeMemberNameWidth();
            // resizeRightSideTable();

            resizeBodyTableRowHeight();

            mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Submit_button);
            mSubmit_Raised_Button.setText(AppStrings.submit);
            mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
            mSubmit_Raised_Button.setOnClickListener(this);

            mPerviousButton = (Button) rootView.findViewById(R.id.fragment_Previousbutton);
            //  mPerviousButton.setText("Savings" + AppStrings.mPervious);
            mPerviousButton.setOnClickListener(this);

            mNextButton = (Button) rootView.findViewById(R.id.fragment_Nextbutton);
            //     mNextButton.setText("Savings" + AppStrings.mNext);
            mNextButton.setOnClickListener(this);


            mPerviousButton.setVisibility(View.INVISIBLE);
            mNextButton.setVisibility(View.INVISIBLE);

        } catch (Exception e) {
            e.printStackTrace();

          /*  TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);

            Intent intent = new Intent(getActivity(), ExitActivity.class);
            startActivity(intent);*/
        }
    }


    private void getTableRowHeaderCellWidth() {
        int lefHeaderChildCount = ((TableRow) mLeftHeaderTable.getChildAt(0)).getChildCount();
        int rightHeaderChildCount = ((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount();

        for (int x = 0; x < (lefHeaderChildCount + rightHeaderChildCount); x++) {
            if (x == 0) {
                rightHeaderWidth[x] = viewWidth(((TableRow) mLeftHeaderTable.getChildAt(0)).getChildAt(x));
            } else {
                rightHeaderWidth[x] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(x - 1));
            }
        }
    }

    private void resizeMemberNameWidth() {
        // TODO Auto-generated method stub
        int leftHeadertWidth = viewWidth(mLeftHeaderTable);
        int leftContentWidth = viewWidth(mLeftContentTable);

        if (leftHeadertWidth < leftContentWidth) {
            mLeftHeaderTable.getLayoutParams().width = leftContentWidth;
        } else {
            mLeftContentTable.getLayoutParams().width = leftHeadertWidth;
        }
    }

    private void resizeBodyTableRowHeight() {

        int leftContentTable_ChildCount = mLeftContentTable.getChildCount();

        for (int x = 0; x < leftContentTable_ChildCount; x++) {

            TableRow leftContentTableRow = (TableRow) mLeftContentTable.getChildAt(x);
            TableRow rightContentTableRow = (TableRow) mRightContentTable.getChildAt(x);

            int rowLeftHeight = viewHeight(leftContentTableRow);
            int rowRightHeight = viewHeight(rightContentTableRow);

            TableRow tableRow = rowLeftHeight < rowRightHeight ? leftContentTableRow : rightContentTableRow;
            int finalHeight = rowLeftHeight > rowRightHeight ? rowLeftHeight : rowRightHeight;

            this.matchLayoutHeight(tableRow, finalHeight);
        }

    }

    private void matchLayoutHeight(TableRow tableRow, int height) {

        int tableRowChildCount = tableRow.getChildCount();

        // if a TableRow has only 1 child
        if (tableRow.getChildCount() == 1) {

            View view = tableRow.getChildAt(0);
            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
            params.height = height - (params.bottomMargin + params.topMargin);

            return;
        }

        // if a TableRow has more than 1 child
        for (int x = 0; x < tableRowChildCount; x++) {

            View view = tableRow.getChildAt(x);

            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

            if (!isTheHeighestLayout(tableRow, x)) {
                params.height = height - (params.bottomMargin + params.topMargin);
                return;
            }
        }

    }

    // check if the view has the highest height in a TableRow
    private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

        int tableRowChildCount = tableRow.getChildCount();
        int heighestViewPosition = -1;
        int viewHeight = 0;

        for (int x = 0; x < tableRowChildCount; x++) {
            View view = tableRow.getChildAt(x);
            int height = this.viewHeight(view);

            if (viewHeight < height) {
                heighestViewPosition = x;
                viewHeight = height;
            }
        }
        return heighestViewPosition == layoutPosition;
    }

    // read a view's height
    private int viewHeight(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredHeight();
    }

    // read a view's width
    private int viewWidth(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredWidth();
    }

    public static int sSavings_Total, sVSavings_Total;
    public static String sSendToServer_Savings, sSendToServer_VSavings;
    String nullVlaue = "0";
    String[] confirmArr;
    Dialog confirmationDialog;


    @Override
    public void onClick(View view) {
        sSavingsAmounts = new String[sSavingsFields.size()];
        sVSavingsAmount = new String[sVSavingsFields.size()];

        Log.d("Saving", "sSavingsAmounts size : " + sSavingsFields.size() + "");

        switch (view.getId()) {

            case R.id.fragment_Submit_button:
                try {
                    sSavings_Total = 0;
                    sVSavings_Total = 0;
                    sum_of_savings =0;
                    sSendToServer_Savings = "";
                    sSendToServer_VSavings = "";

                    confirmArr = new String[mSize];
                    // Do edit values here

                    if (arrMem != null && arrMem.size() > 0) {
                        arrMem.clear();
                    }

                    if (offlineData != null && offlineData.size() > 0) {
                        offlineData.clear();
                    }


                    for (int i = 0; i < memList.size(); i++) {
                        MemberList ml = new MemberList();
                        ml.setMemberId(memList.get(i).getMemberId());
                        ml.setSavingsAmount(sSavingsFields.get(i).getText().toString());
                        ml.setVoluntarySavingsAmount(sVSavingsFields.get(i).getText().toString());

                        sSavingsAmounts[i] = sSavingsFields.get(i).getText().toString();
                        sSavings_Total += Integer.parseInt((!sSavingsFields.get(i).getText().toString().equals("") && sSavingsFields.get(i).getText().toString().length() > 0) ? sSavingsFields.get(i).getText().toString() : "0");
                        sVSavingsAmount[i] = sVSavingsFields.get(i).getText().toString();
                        sVSavings_Total += Integer.parseInt((!sVSavingsFields.get(i).getText().toString().equals("") && sVSavingsFields.get(i).getText().toString().length() > 0) ? sVSavingsFields.get(i).getText().toString() : "0");
                        try {
                            OfflineDto offline = new OfflineDto();
                            offline.setMemberId(memList.get(i).getMemberId());
                            offline.setMemberName(memList.get(i).getMemberName());
                            offline.setAnimatorId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                            offline.setShgId(shgDto.getShgId());
//                            offline.setLastTransactionDateTime(shgDto.getLastTransactionDate());

                            offline.setLastTransactionDateTime(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                            offline.setModifiedDateTime(System.currentTimeMillis() + "");
                            offline.setModeOCash("2");
                            offline.setTxType(NewDrawerScreen.SAVINGS);
                            offline.setTxSubtype(AppStrings.savings);
                            offline.setSavAmount(sSavingsAmounts[i]);
                            offline.setVSavAmount(sVSavingsAmount[i]);
                            offline.setTotalSavAmount((sSavings_Total + sVSavings_Total) + "");
                            offline.setIs_transaction_tdy("1.0");
                            offlineData.add(offline);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        arrMem.add(ml);
                    }
                    Utils.sum_of_savings = sSavings_Total + sVSavings_Total;
                    Log.d("sum_of_saving", "" + Utils.sum_of_savings);

                    sum_of_savings = sSavings_Total + sVSavings_Total;
                    Log.d("sum_of_saving", "" + sum_of_savings);

                    //  if (Utils.sum_of_savings > Integer.parseInt(shgDto.getCashInHand())) {
                    if ((sSavings_Total != 0) || (sVSavings_Total != 0)) {

                        confirmationDialog = new Dialog(getActivity());

                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);

                        ViewGroup.LayoutParams lParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        dialogView.setLayoutParams(lParams);

                        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                        confirmationHeader.setText("" + AppStrings.confirmation);
                        confirmationHeader.setTypeface(LoginActivity.sTypeface);
                        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);


                        DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
                        Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
                        String dateStr = simple.format(d);
                        TextView transactdate = (TextView)dialogView.findViewById(R.id.transactdate);
                        transactdate.setText(dateStr);


                        for (int i = 0; i < memList.size(); i++) {

                            TableRow indv_SavingsRow = new TableRow(getActivity());

                            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                            contentParams.setMargins(10, 5, 10, 5);

                            TextView memberName_Text = new TextView(getActivity());
                            memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                                    memList.get(i).getMemberName()));
                            memberName_Text.setTextColor(R.color.black);
                            memberName_Text.setPadding(5, 5, 5, 5);
                            memberName_Text.setSingleLine(true);
                            memberName_Text.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(memberName_Text);

                            TextView confirm_values = new TextView(getActivity());
                            confirm_values
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf((sSavingsAmounts[i] != null && sSavingsAmounts[i].length() > 0) ? sSavingsAmounts[i] : "0")));
                            confirm_values.setTextColor(R.color.black);
                            confirm_values.setPadding(5, 5, 5, 5);
                            confirm_values.setGravity(Gravity.RIGHT);
                            confirm_values.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(confirm_values);

                            TextView confirm_VSvalues = new TextView(getActivity());
                            confirm_VSvalues
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf((sVSavingsAmount[i] != null && sVSavingsAmount[i].length() > 0) ? sVSavingsAmount[i] : "0")));
                            confirm_VSvalues.setTextColor(R.color.black);
                            confirm_VSvalues.setPadding(5, 5, 5, 5);
                            confirm_VSvalues.setGravity(Gravity.RIGHT);
                            confirm_VSvalues.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(confirm_VSvalues);

                            confirmationTable.addView(indv_SavingsRow,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        }

                        View rullerView = new View(getActivity());
                        rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                        rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                        // 229,
                        // 242));
                        confirmationTable.addView(rullerView);

                        TableRow totalRow = new TableRow(getActivity());

                        TableRow.LayoutParams totalParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                        totalParams.setMargins(10, 5, 10, 5);

                        TextView totalText = new TextView(getActivity());
                        totalText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
                        totalText.setTextColor(R.color.black);
                        totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
                        totalText.setLayoutParams(totalParams);
                        totalRow.addView(totalText);

                        TextView totalAmount = new TextView(getActivity());
                        totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sSavings_Total)));
                        totalAmount.setTextColor(R.color.black);
                        totalAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                        totalAmount.setGravity(Gravity.RIGHT);
                        totalAmount.setLayoutParams(totalParams);
                        totalRow.addView(totalAmount);

                        TextView totalVSAmount = new TextView(getActivity());
                        totalVSAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sVSavings_Total)));
                        totalVSAmount.setTextColor(R.color.black);
                        totalVSAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                        totalVSAmount.setGravity(Gravity.RIGHT);
                        totalVSAmount.setLayoutParams(totalParams);
                        totalRow.addView(totalVSAmount);

                        confirmationTable.addView(totalRow,
                                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                        mEdit_RaisedButton.setText(AppStrings.edit);
                        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                        // 205,
                        // 0));
                        mEdit_RaisedButton.setOnClickListener(this);

                        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                        mOk_RaisedButton.setText(AppStrings.yes);
                        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                        mOk_RaisedButton.setOnClickListener(this);

                        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        confirmationDialog.setCanceledOnTouchOutside(false);
                        confirmationDialog.setContentView(dialogView);
                        confirmationDialog.setCancelable(true);
                        confirmationDialog.show();

                        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                        margin.leftMargin = 10;
                        margin.rightMargin = 10;
                        margin.topMargin = 10;
                        margin.bottomMargin = 10;
                        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

                    } else {

                        TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);

                        sSendToServer_Savings = "";
                        sSendToServer_VSavings = "";
                        sSavings_Total = 0;
                        sVSavings_Total = 0;
                    }
                  /*  }else{
                        TastyToast.makeText(getActivity(), AppStrings.cashinHandAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }*/


                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.fragment_Edit:
                sSendToServer_Savings = "";
                sSendToServer_VSavings = "";
                sSavings_Total = 0;
                sVSavings_Total = 0;
                mSubmit_Raised_Button.setClickable(true);

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();

                break;
            case R.id.frag_Ok:

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();

                if (networkConnection.isNetworkAvailable()) {
                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 1) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }
                    // NOTHING TO DO::
                } else {

                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 2) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }

                }


                SavingRequest sr = new SavingRequest();
                sr.setSavingsAmount(arrMem);
                sr.setShgId(shgDto.getShgId());
                sr.setModeOfCash("2");
                sr.setMobileDate(System.currentTimeMillis() + "");
                sr.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
//                sr.setTransactionDate(shgDto.getLastTransactionDate());
                String sreqString = new Gson().toJson(sr);

               /* if (Attendance.flag == "1") {
                    confirmationDialog.dismiss();
                    Internalloan_repayment internalloanRepayment = new Internalloan_repayment();
                    Bundle bundles = new Bundle();
                    bundles.putString("stepwise", Attendance.flag);
                    internalloanRepayment.setArguments(bundles);
                    FragmentManager fm = getFragmentManager();
                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    insertSavings();
                    NewDrawerScreen.showFragment(internalloanRepayment);

                }
                else {
*/
//               if(EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1")) {
                Log.i("print","Value : "+EShaktiApplication.flag);
                if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {


                   if (networkConnection.isNetworkAvailable()) {
                       onTaskStarted();
                       RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.SAVINGS, sreqString, getActivity(), ServiceType.SAVINGS);
                   } else {
                       if (TransactionTable.getLoginFlag(AppStrings.savings).size() <= 0 || (!TransactionTable.getLoginFlag(AppStrings.savings).get(TransactionTable.getLoginFlag(AppStrings.savings).size() - 1).getLoginFlag().equals("1")))
                       {
                           confirmationDialog.dismiss();
                           Internalloan_repayment internalloanRepayment = new Internalloan_repayment();
                           FragmentManager fm = getFragmentManager();
                           fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                           insertSavings();
                           NewDrawerScreen.showFragment(internalloanRepayment);
                       }

                       else
                           TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                                   TastyToast.WARNING);
                   }
               }
               else
               {
               if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();
                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.SAVINGS, sreqString, getActivity(), ServiceType.SAVINGS);
                    } else {
                        if (TransactionTable.getLoginFlag(AppStrings.savings).size() <= 0 || (!TransactionTable.getLoginFlag(AppStrings.savings).get(TransactionTable.getLoginFlag(AppStrings.savings).size() - 1).getLoginFlag().equals("1")))
                            insertSavings();
                        else
                            TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);
                    }
                }
                break;
            case R.id.fragment_Previousbutton:
                break;
            case R.id.fragment_Nextbutton:
                break;

            case R.id.autoFill:

                String similiar_Savings;

                if (mAutoFill.isChecked()) {

                    try {

                        // Makes all edit fields holds the same savings
                        if (!String.valueOf(sSavingsFields.get(0).getText()).equals("")) {


                            similiar_Savings = sSavingsFields.get(0).getText().toString();
                            // send_To_Server_SavingsType = AppStrings.savings;
                            System.out.println("CB Amount : " + similiar_Savings);
                            for (int i = 0; i < sSavingsAmounts.length; i++) {
                                sSavingsFields.get(i).setText(similiar_Savings);
                                sSavingsFields.get(i).setGravity(Gravity.RIGHT);
                                sSavingsFields.get(i).clearFocus();
                                sSavingsAmounts[i] = similiar_Savings;
                            }

                        }

                        /** To clear the values of EditFields in case of uncheck **/

                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();

                        TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);

                    }
                } else {
                    // Do check box unCheck
                }

                break;
            default:
                break;
        }
    }

    private void insertTransaction_tdy()
    {
        for (OfflineDto ofdto : offlineData) {
            ofdto.setIs_transaction_tdy("1.0");
            ofdto.setShgId(shgDto.getShgId());
            SHGTable.updateIstransaction(ofdto);
        }

    }

    private void insertSavings() {
        try {
            //Offline fundflow:
            int cih = 0;
            cih = (int) Double.parseDouble(shgDto.getCashInHand()) + (sSavings_Total + sVSavings_Total);
            String cihstr = "", cabstr = "", lastTranstr = "";
            int value = (MySharedPreference.readInteger(getActivity(), MySharedPreference.SAVING_COUNT, 0) + 1);
            if (value > 0)
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.SAVING_COUNT, value);
            for (OfflineDto ofdto : offlineData) {
                ofdto.setTotalSavAmount((sSavings_Total + sVSavings_Total) + "");
                ofdto.setCashAtBank(shgDto.getCashAtBank());
                ofdto.setCashInhand(cih + "");
                ofdto.setSCount(value + "");
                TransactionTable.insertTransSavingData(ofdto);
            }

            Log.i("print","FFlag Value : "+shgDto.getFFlag());
            if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
            }
            cihstr = cih + "";
            cabstr = shgDto.getCashAtBank();
            lastTranstr = offlineData.get(0).getLastTransactionDateTime();
            CashOfGroup csg = new CashOfGroup();
            csg.setCashInHand(cihstr);
            csg.setCashAtBank(cabstr);
            csg.setLastTransactionDate(lastTranstr);
            SHGTable.updateSHGDetails(csg, shgDto.getId());

            FragmentManager fm = getFragmentManager();
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            NewDrawerScreen.showFragment(new MainFragment());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(resume_value==true)

        {
//            if (EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1")) {
                if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
//            if (channel!=null && channel.trim().equals("STEPWISE") ) {
                confirmationDialog.dismiss();
                Internalloan_repayment internalloanRepayment = new Internalloan_repayment();

                FragmentManager fm = getFragmentManager();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                TastyToast.makeText(getActivity(), message, TastyToast.LENGTH_SHORT,
                        TastyToast.SUCCESS);
                NewDrawerScreen.showFragment(internalloanRepayment);

            } else {
                if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                    SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                }

                FragmentManager fm = getFragmentManager();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                MainFragment mainFragment = new MainFragment();
                Bundle bundles = new Bundle();
                bundles.putString("Transaction", MainFragment.Flag_Transaction);
                mainFragment.setArguments(bundles);
                TastyToast.makeText(getActivity(), message, TastyToast.LENGTH_SHORT,
                        TastyToast.SUCCESS);
                NewDrawerScreen.showFragment(mainFragment);
            }


        } else {
            if (statusCode == 401) {

                Log.e("Group Logout", "Logout Sucessfully");
                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                    mProgressDilaog.dismiss();
                    mProgressDilaog = null;
                }
            }

//            Utils.showToast(getActivity(), message);
//                                insertSavings();
        }


        }



    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
            }
            switch (serviceType) {
                case SAVINGS:
                    try {
                        if (result != null) {
                            ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                             message = cdto.getMessage();
                             statusCode = cdto.getStatusCode();
                            if (statusCode == Utils.Success_Code) {
                                resume_value =true;
//                                Utils.showToast(getActivity(), message);

                                insertTransaction_tdy();

//                                if (EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1")) {
                                Log.i("print","case Value : "+EShaktiApplication.flag);
                                if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {

                                    confirmationDialog.dismiss();
                                    Internalloan_repayment internalloanRepayment = new Internalloan_repayment();
                                    FragmentManager fm = getFragmentManager();
                                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                    TastyToast.makeText(getActivity(), message, TastyToast.LENGTH_SHORT,
                                            TastyToast.SUCCESS);
                                    NewDrawerScreen.showFragment(internalloanRepayment);

                                } else {
                                    if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                        Log.i("print","getFFlag : "+shgDto.getFFlag());
                                        SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                                    }

                                    FragmentManager fm = getFragmentManager();
                                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                    MainFragment mainFragment = new MainFragment();
                                    Bundle bundles = new Bundle();
                                    bundles.putString("Transaction", MainFragment.Flag_Transaction);
                                    mainFragment.setArguments(bundles);
                                    TastyToast.makeText(getActivity(), message, TastyToast.LENGTH_SHORT,
                                            TastyToast.SUCCESS);
                                    NewDrawerScreen.showFragment(mainFragment);
                               }


                            } else {
                                if (statusCode == 401) {

                                    Log.e("Group Logout", "Logout Sucessfully");
                                    AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                    if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                        mProgressDilaog.dismiss();
                                        mProgressDilaog = null;
                                    }
                                }

                                Utils.showToast(getActivity(), message);
//                                insertSavings();
                            }
                        } else {
//                            insertSavings();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }

        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

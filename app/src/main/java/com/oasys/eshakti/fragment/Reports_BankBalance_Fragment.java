package com.oasys.eshakti.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Adapter.BankBalanceAdapter;
import com.oasys.eshakti.Dto.BankBalanceDTOList;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.Service.NewTaskListener;

import java.util.ArrayList;

public class Reports_BankBalance_Fragment extends Fragment implements NewTaskListener {
    private View rootView;
    private RecyclerView loanoutstandingrecyclerview;
    ResponseDto bankBalanceResponseDto;
    private BankBalanceAdapter bankBalanceAdapter;
    private LinearLayoutManager linearLayoutManager;
    private ListOfShg shgDto;
    private String shgId;
    ArrayList<BankBalanceDTOList> bankBalanceDTOLists;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView accountbook, mHeader;

    public Reports_BankBalance_Fragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_bankbalance, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgId = shgDto.getShgId();
        return rootView;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mGroupName = (TextView) rootView.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);

        mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);


        mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashatBank.setTypeface(LoginActivity.sTypeface);

        mHeader = (TextView) rootView.findViewById(R.id.fragHeader);
        mHeader.setTypeface(LoginActivity.sTypeface);



        init();
        String url = Constants.BASE_URL + Constants.REPORT_BANKBALANCE + shgId;

        // String url = Constants.BASE_URL + Constants.REPORT_BANKBALANCE + "00f23ab7-42b6-4223-9ad5-a35f43caaa6b";
        RestClient.getRestClient(Reports_BankBalance_Fragment.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.BANKBALANCE);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        loanoutstandingrecyclerview.setLayoutManager(linearLayoutManager);
        loanoutstandingrecyclerview.setHasFixedSize(true);
        loanoutstandingrecyclerview.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        bankBalanceDTOLists = new ArrayList<>();
    }

    public void init() {

        loanoutstandingrecyclerview = (RecyclerView) rootView.findViewById(R.id.bb_recyclerview);
        accountbook = (TextView) rootView.findViewById(R.id.bb_savingsaccount);
        accountbook.setText(shgDto.getCashAtBank());
    }

    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        switch (serviceType) {
            case BANKBALANCE:
                if (result != null && result.length() > 0) {
                    Log.d("getDetails", " " + result.toString());
                    bankBalanceResponseDto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    if (bankBalanceResponseDto.getStatusCode() == Utils.Success_Code) {
                        Log.d("NumbersDTOLists", " " + bankBalanceResponseDto.getResponseContent().getBankBalanceDTOList());

                        //Utils.showToast(Profile_MobileNumber_Updation.this, profileNumberDto.getMessage());
                        if ((bankBalanceResponseDto.getResponseContent().getBankBalanceDTOList() != null)) {
                            bankBalanceAdapter = new BankBalanceAdapter(getActivity(),
                                    bankBalanceResponseDto.getResponseContent().getBankBalanceDTOList());
                            loanoutstandingrecyclerview.setAdapter(bankBalanceAdapter);


                        } else {
                            if (bankBalanceResponseDto.getStatusCode() == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                            }
                            Utils.showToast(getActivity(), "Null Value");
                        }
                    }

                }

                break;

        }
    }
}
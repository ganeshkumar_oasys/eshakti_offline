package com.oasys.eshakti.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Dialogue.Dialog_New_TransactionDate;
import com.oasys.eshakti.Dto.CashOfGroup;
import com.oasys.eshakti.Dto.ExistingLoan;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.LoanDto;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.Dto.OfflineDto;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.Dto.ShgBankDetails;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.GetSpanText;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.BankTable;
import com.oasys.eshakti.database.LoanTable;
import com.oasys.eshakti.database.MemberTable;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.views.Get_EdiText_Filter;
import com.oasys.eshakti.views.RaisedButton;
import com.oasys.eshakti.views.TextviewUtils;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class LD_EL_Mem_Disburse extends Fragment implements View.OnClickListener, NewTaskListener {


    private View rootView;
    private TextView mGroupName, mCashinHand, mCashatBank, mHeader, mAutoFill_label;
    private CheckBox mAutoFill;
    private Button mSubmit_Raised_Button;
    private TableLayout mIncomeTable;
    private EditText mIncome_values;
    private static List<EditText> sIncomeFields = new ArrayList<EditText>();
    private static String sIncomeAmounts[];
    String[] confirmArr;
    String nullVlaue = "0";
    public static String sSendToServer_InternalLoanDisbursement;
    public static int sIncome_Total;

    Dialog confirmationDialog;
    private RaisedButton mEdit_RaisedButton, mOk_RaisedButton;
    private Dialog mProgressDialog;

    boolean isNaviMain = false;
    boolean isServiceCall = false;

    LinearLayout mMemberNameLayout;
    TextView mMemberName;

    private int mSize;
    private List<MemberList> memList;
    ArrayList<MemberList> memLoanDisbursement = new ArrayList<>();
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<LoanDto> arrLoanType;
    private LoanDto ldto = new LoanDto();
    private ArrayList<ShgBankDetails> bankdetails;
    private Dialog mProgressDilaog;
    int dismountadd =0;
    int outstaningamount=0;
    OfflineDto offline=new OfflineDto();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_internalloan_disbursement, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        arrLoanType = new ArrayList<>();
        sIncomeFields = new ArrayList<EditText>();
        init();
    }

    private void init() {
        try {
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
            mMemberName = (TextView) rootView.findViewById(R.id.member_name);
            mMemberName.setTypeface(LoginActivity.sTypeface);

            /** UI Mapping **/

            mHeader = (TextView) rootView.findViewById(R.id.internal_fragmentHeader);
            // mHeader.setText(AppStrings.mInternalTypeLoan));
            mHeader.setText(LD_ExternalLoan.sExistingLoagSelection.getLoanTypeName());
            mHeader.setTypeface(LoginActivity.sTypeface);

            mAutoFill_label = (TextView) rootView.findViewById(R.id.internal_autofillLabel);
            mAutoFill_label.setText(AppStrings.autoFill);
            mAutoFill_label.setTypeface(LoginActivity.sTypeface);
            mAutoFill_label.setVisibility(View.VISIBLE);

            mAutoFill = (CheckBox) rootView.findViewById(R.id.autoFillCheck);
            mAutoFill.setVisibility(View.VISIBLE);
            mAutoFill.setOnClickListener(this);


            TableLayout headerTable = (TableLayout) rootView.findViewById(R.id.internal_savingsTable);

            mIncomeTable = (TableLayout) rootView.findViewById(R.id.internal_fragment_contentTable);

            TableRow savingsHeader = new TableRow(getActivity());
            savingsHeader.setBackgroundResource(R.color.tableHeader);

            ViewGroup.LayoutParams headerParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,
                    1f);

            TextView mMemberName_headerText = new TextView(getActivity());
            mMemberName_headerText
                    .setText(String.valueOf(AppStrings.memberName));
            mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
            mMemberName_headerText.setTextColor(Color.WHITE);
            mMemberName_headerText.setPadding(20, 5, 10, 5);
            mMemberName_headerText.setLayoutParams(headerParams);
            savingsHeader.addView(mMemberName_headerText);

            TextView mIncomeAmount_HeaderText = new TextView(getActivity());
            mIncomeAmount_HeaderText
                    .setText(String.valueOf(AppStrings.amount));
            mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
            mIncomeAmount_HeaderText.setTextColor(Color.WHITE);
            mIncomeAmount_HeaderText.setPadding(10, 5, 40, 5);
            mIncomeAmount_HeaderText.setLayoutParams(headerParams);
            mIncomeAmount_HeaderText.setBackgroundResource(R.color.tableHeader);
            savingsHeader.addView(mIncomeAmount_HeaderText);

            headerTable.addView(savingsHeader,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            for (int i = 0; i < mSize; i++) {

                TableRow indv_IncomeRow = new TableRow(getActivity());

                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams.setMargins(10, 5, 10, 5);

                final TextView memberName_Text = new TextView(getActivity());
                memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                        String.valueOf(memList.get(i).getMemberName())));
                memberName_Text.setTextColor(R.color.black);
                memberName_Text.setPadding(10, 0, 10, 5);
                memberName_Text.setLayoutParams(contentParams);
                memberName_Text.setWidth(200);
                memberName_Text.setSingleLine(true);
                memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
                indv_IncomeRow.addView(memberName_Text);

                TableRow.LayoutParams contentEditParams = new TableRow.LayoutParams(150, ViewGroup.LayoutParams.WRAP_CONTENT);
                contentEditParams.setMargins(30, 5, 100, 5);

                mIncome_values = new EditText(getActivity());

                mIncome_values.setId(i);
                sIncomeFields.add(mIncome_values);
                mIncome_values.setGravity(Gravity.END);
                mIncome_values.setTextColor(Color.BLACK);
                mIncome_values.setPadding(5, 5, 5, 5);
                mIncome_values.setBackgroundResource(R.drawable.edittext_background);
                mIncome_values.setLayoutParams(contentEditParams);// contentParams
                // lParams
                mIncome_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
                mIncome_values.setFilters(Get_EdiText_Filter.editText_filter());
                mIncome_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                mIncome_values.setTextColor(R.color.black);
                mIncome_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);

                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
                        } else {
                            ((EditText) v).setGravity(Gravity.RIGHT);

                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }

                    }
                });
                indv_IncomeRow.addView(mIncome_values);

                mIncomeTable.addView(indv_IncomeRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            }

            mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.internal_fragment_Submit_button);
            mSubmit_Raised_Button.setText(AppStrings.submit);
            mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
            mSubmit_Raised_Button.setOnClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void insertTransaction_tdy()
    {

            offline.setIs_transaction_tdy("1.0");
            offline.setShgId(shgDto.getShgId());
            SHGTable.updateIstransaction(offline);

    }

    @Override
    public void onClick(View view) {
        sIncomeAmounts = new String[mSize];
        switch (view.getId()) {
            case R.id.internal_fragment_Submit_button:

                try {
                    // isIncome = true;
                    offline.setIs_transaction_tdy("1.0");
                    sSendToServer_InternalLoanDisbursement = "";
                    sIncome_Total = 0;


                    confirmArr = new String[mSize];

                    StringBuilder builder = new StringBuilder();

                    if (memLoanDisbursement != null && memLoanDisbursement.size() > 0) {
                        memLoanDisbursement.clear();
                    }

                    for (int i = 0; i < sIncomeAmounts.length; i++) {

                        sIncomeAmounts[i] = String.valueOf(sIncomeFields.get(i).getText());

                        if ((sIncomeAmounts[i].equals("")) || (sIncomeAmounts[i] == null)) {
                            sIncomeAmounts[i] = nullVlaue;
                        }

                        if (sIncomeAmounts[i].matches("\\d*\\.?\\d+")) { // match
                            // a
                            // decimal
                            // number

                            int amount = (int) Math.round(Double.parseDouble(sIncomeAmounts[i]));
                            sIncomeAmounts[i] = String.valueOf(amount);
                        }

                      /*  sSendToServer_InternalLoanDisbursement = sSendToServer_InternalLoanDisbursement
                                + String.valueOf(memList.get(i).getMemberId()) + "~" + sIncomeAmounts[i] + "~";

                        confirmArr[i] = String.valueOf(memList.get(i).getMemberName()) + " "
                                + sIncomeAmounts[i];*/
                        MemberList memeber = new MemberList();
                        memeber.setMemberId(memList.get(i).getMemberId());
                        memeber.setLoanAmount(sIncomeAmounts[i]);
                        memLoanDisbursement.add(memeber);
                        sIncome_Total = sIncome_Total + Integer.parseInt(sIncomeAmounts[i]);

                        builder.append(sIncomeAmounts[i]).append(",");
                    }

                    int mCheckValues = 0;
                    //    InternalBankMFIModel.setValues(sSendToServer_InternalLoanDisbursement);
                    if (LD_EL_SBLoanMenu.sSelectedIncomeMenu.toUpperCase().equals(AppStrings.mLoanDisbursementFromLoanAcc)) {
                        mCheckValues = (int) (Double.parseDouble(LD_EL_LD_LoanAcc.lEntry.getDisbursementAmount()));
                        if (sIncome_Total <= ((int) Double.parseDouble(LD_EL_LD_LoanAcc.disbursementAmount))) {

                            confirmationDialog = new Dialog(getActivity());

                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                            dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT));

                            TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                            confirmationHeader.setText(AppStrings.confirmation);
                            confirmationHeader.setTypeface(LoginActivity.sTypeface);

                            TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                            DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
                            Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
                            String dateStr = simple.format(d);
                            TextView transactdate = (TextView)dialogView.findViewById(R.id.transactdate);
                            transactdate.setText(dateStr);

                            for (int i = 0; i < confirmArr.length; i++) {

                                TableRow indv_SavingsRow = new TableRow(getActivity());

                                @SuppressWarnings("deprecation")
                                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                                contentParams.setMargins(10, 5, 10, 5);

                                TextView memberName_Text = new TextView(getActivity());
                                memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                                        String.valueOf(memList.get(i).getMemberName())));
                                memberName_Text.setTextColor(R.color.black);
                                memberName_Text.setPadding(5, 5, 5, 5);
                                memberName_Text.setLayoutParams(contentParams);
                                indv_SavingsRow.addView(memberName_Text);

                                TextView confirm_values = new TextView(getActivity());
                                confirm_values
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncomeAmounts[i])));
                                confirm_values.setTextColor(R.color.black);
                                confirm_values.setPadding(5, 5, 5, 5);
                                confirm_values.setGravity(Gravity.RIGHT);
                                confirm_values.setLayoutParams(contentParams);
                                indv_SavingsRow.addView(confirm_values);

                                confirmationTable.addView(indv_SavingsRow,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            }


                            View rullerView1 = new View(getActivity());
                            rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                            rullerView1.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                            // 229,
                            // 242));
                            confirmationTable.addView(rullerView1);

                            TableRow totalRow = new TableRow(getActivity());

                            @SuppressWarnings("deprecation")
                            TableRow.LayoutParams totalParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                            totalParams.setMargins(10, 5, 10, 5);

                            TextView totalText = new TextView(getActivity());
                            totalText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
                            totalText.setTypeface(LoginActivity.sTypeface);
                            totalText.setTextColor(R.color.black);
                            totalText.setPadding(5, 5, 5, 5);// (5, 10, 5,
                            // 10);
                            totalText.setLayoutParams(totalParams);
                            totalRow.addView(totalText);

                            TextView totalAmount = new TextView(getActivity());
                            totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncome_Total)));// SavingsFragment.sSavings_Total
                            totalAmount.setTextColor(R.color.black);
                            totalAmount.setPadding(5, 5, 5, 5);// (5, 10,
                            // 100,
                            // 10);
                            totalAmount.setGravity(Gravity.RIGHT);
                            totalAmount.setLayoutParams(totalParams);
                            totalRow.addView(totalAmount);

                            confirmationTable.addView(totalRow,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                            mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit);
                            mEdit_RaisedButton.setText(AppStrings.edit);
                            mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                            mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                            // 205,
                            // 0));
                            mEdit_RaisedButton.setOnClickListener(this);

                            mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.frag_Ok);
                            mOk_RaisedButton.setText(AppStrings.yes);
                            mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                            mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                            mOk_RaisedButton.setOnClickListener(this);

                            confirmationDialog.getWindow()
                                    .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            confirmationDialog.setCanceledOnTouchOutside(false);
                            confirmationDialog.setContentView(dialogView);
                            confirmationDialog.setCancelable(true);
                            confirmationDialog.show();

                            ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                            margin.leftMargin = 10;
                            margin.rightMargin = 10;
                            margin.topMargin = 10;
                            margin.bottomMargin = 10;
                            margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

                        } else {
                            TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);

                            sSendToServer_InternalLoanDisbursement = "0";
                            sIncome_Total = 0;

                        }
                    } else if (LD_EL_SBLoanMenu.sSelectedIncomeMenu.toUpperCase().equals(AppStrings.mLoanDisbursementFromSbAcc)) {

                        mCheckValues = (int) (Double.parseDouble(LD_EL_Sb_Loan_acc_disburse.lEntry.getDisbursementAmount()));
                        if (sIncome_Total <= ((int) Double.parseDouble(LD_EL_Sb_Loan_acc_disburse.sbAcc_balanceAmount))) {

                            confirmationDialog = new Dialog(getActivity());

                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                            dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT));

                            TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                            confirmationHeader.setText(AppStrings.confirmation);
                            confirmationHeader.setTypeface(LoginActivity.sTypeface);

                            TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                            DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
                            Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
                            String dateStr = simple.format(d);
                            TextView transactdate = (TextView)dialogView.findViewById(R.id.transactdate);
                            transactdate.setText(dateStr);

                            for (int i = 0; i < confirmArr.length; i++) {

                                TableRow indv_SavingsRow = new TableRow(getActivity());

                                @SuppressWarnings("deprecation")
                                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                                contentParams.setMargins(10, 5, 10, 5);

                                TextView memberName_Text = new TextView(getActivity());
                                memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                                        String.valueOf(memList.get(i).getMemberName())));
                                memberName_Text.setTextColor(R.color.black);
                                memberName_Text.setPadding(5, 5, 5, 5);
                                memberName_Text.setLayoutParams(contentParams);
                                indv_SavingsRow.addView(memberName_Text);

                                TextView confirm_values = new TextView(getActivity());
                                confirm_values
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncomeAmounts[i])));
                                confirm_values.setTextColor(R.color.black);
                                confirm_values.setPadding(5, 5, 5, 5);
                                confirm_values.setGravity(Gravity.RIGHT);
                                confirm_values.setLayoutParams(contentParams);
                                indv_SavingsRow.addView(confirm_values);

                                confirmationTable.addView(indv_SavingsRow,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            }


                            View rullerView1 = new View(getActivity());
                            rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                            rullerView1.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                            // 229,
                            // 242));
                            confirmationTable.addView(rullerView1);

                            TableRow totalRow = new TableRow(getActivity());

                            @SuppressWarnings("deprecation")
                            TableRow.LayoutParams totalParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                            totalParams.setMargins(10, 5, 10, 5);

                            TextView totalText = new TextView(getActivity());
                            totalText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
                            totalText.setTypeface(LoginActivity.sTypeface);
                            totalText.setTextColor(R.color.black);
                            totalText.setPadding(5, 5, 5, 5);// (5, 10, 5,
                            // 10);
                            totalText.setLayoutParams(totalParams);
                            totalRow.addView(totalText);

                            TextView totalAmount = new TextView(getActivity());
                            totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncome_Total)));// SavingsFragment.sSavings_Total
                            totalAmount.setTextColor(R.color.black);
                            totalAmount.setPadding(5, 5, 5, 5);// (5, 10,
                            // 100,
                            // 10);
                            totalAmount.setGravity(Gravity.RIGHT);
                            totalAmount.setLayoutParams(totalParams);
                            totalRow.addView(totalAmount);

                            confirmationTable.addView(totalRow,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                            mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit);
                            mEdit_RaisedButton.setText(AppStrings.edit);
                            mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                            mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                            // 205,
                            // 0));
                            mEdit_RaisedButton.setOnClickListener(this);

                            mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.frag_Ok);
                            mOk_RaisedButton.setText(AppStrings.yes);
                            mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                            mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                            mOk_RaisedButton.setOnClickListener(this);

                            confirmationDialog.getWindow()
                                    .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            confirmationDialog.setCanceledOnTouchOutside(false);
                            confirmationDialog.setContentView(dialogView);
                            confirmationDialog.setCancelable(true);
                            confirmationDialog.show();

                            ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                            margin.leftMargin = 10;
                            margin.rightMargin = 10;
                            margin.topMargin = 10;
                            margin.bottomMargin = 10;
                            margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);


                        } else {
                            TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);

                            sSendToServer_InternalLoanDisbursement = "0";
                            sIncome_Total = 0;
                        }
                        // Do the SP insertion

                    } else if (LD_EL_SBLoanMenu.sSelectedIncomeMenu.toUpperCase().equals(AppStrings.mLoanDisbursementFromRepaid)) {

                        mCheckValues = (int) (Double.parseDouble(LD_EL_LD_Repaid.lEntry.getDisbursementAmount()));
                        if (sIncome_Total <= ((int) Double.parseDouble(LD_EL_LD_Repaid.disbursementAmount))) {

                            confirmationDialog = new Dialog(getActivity());

                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                            dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT));

                            DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
                            Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
                            String dateStr = simple.format(d);
                            TextView transactdate = (TextView)dialogView.findViewById(R.id.transactdate);
                            transactdate.setText(dateStr);

                            TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                            confirmationHeader.setText(AppStrings.confirmation);
                            confirmationHeader.setTypeface(LoginActivity.sTypeface);

                            TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);



                            for (int i = 0; i < confirmArr.length; i++) {

                                TableRow indv_SavingsRow = new TableRow(getActivity());

                                @SuppressWarnings("deprecation")
                                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                                contentParams.setMargins(10, 5, 10, 5);

                                TextView memberName_Text = new TextView(getActivity());
                                memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                                        String.valueOf(memList.get(i).getMemberName())));
                                memberName_Text.setTextColor(R.color.black);
                                memberName_Text.setPadding(5, 5, 5, 5);
                                memberName_Text.setLayoutParams(contentParams);
                                indv_SavingsRow.addView(memberName_Text);

                                TextView confirm_values = new TextView(getActivity());
                                confirm_values
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncomeAmounts[i])));
                                confirm_values.setTextColor(R.color.black);
                                confirm_values.setPadding(5, 5, 5, 5);
                                confirm_values.setGravity(Gravity.RIGHT);
                                confirm_values.setLayoutParams(contentParams);
                                indv_SavingsRow.addView(confirm_values);

                                confirmationTable.addView(indv_SavingsRow,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            }


                            View rullerView1 = new View(getActivity());
                            rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                            rullerView1.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                            // 229,
                            // 242));
                            confirmationTable.addView(rullerView1);

                            TableRow totalRow = new TableRow(getActivity());

                            @SuppressWarnings("deprecation")
                            TableRow.LayoutParams totalParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                            totalParams.setMargins(10, 5, 10, 5);

                            TextView totalText = new TextView(getActivity());
                            totalText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
                            totalText.setTypeface(LoginActivity.sTypeface);
                            totalText.setTextColor(R.color.black);
                            totalText.setPadding(5, 5, 5, 5);// (5, 10, 5,
                            // 10);
                            totalText.setLayoutParams(totalParams);
                            totalRow.addView(totalText);

                            TextView totalAmount = new TextView(getActivity());
                            totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncome_Total)));// SavingsFragment.sSavings_Total
                            totalAmount.setTextColor(R.color.black);
                            totalAmount.setPadding(5, 5, 5, 5);// (5, 10,
                            // 100,
                            // 10);
                            totalAmount.setGravity(Gravity.RIGHT);
                            totalAmount.setLayoutParams(totalParams);
                            totalRow.addView(totalAmount);

                            confirmationTable.addView(totalRow,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                            mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit);
                            mEdit_RaisedButton.setText(AppStrings.edit);
                            mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                            mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                            // 205,
                            // 0));
                            mEdit_RaisedButton.setOnClickListener(this);

                            mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.frag_Ok);
                            mOk_RaisedButton.setText(AppStrings.yes);
                            mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                            mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                            mOk_RaisedButton.setOnClickListener(this);

                            confirmationDialog.getWindow()
                                    .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            confirmationDialog.setCanceledOnTouchOutside(false);
                            confirmationDialog.setContentView(dialogView);
                            confirmationDialog.setCancelable(true);
                            confirmationDialog.show();

                            ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                            margin.leftMargin = 10;
                            margin.rightMargin = 10;
                            margin.topMargin = 10;
                            margin.bottomMargin = 10;
                            margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);


                        } else {
                            TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);

                            sSendToServer_InternalLoanDisbursement = "0";
                            sIncome_Total = 0;
                        }
                        // Do the SP insertion

                    }/*else{
                        TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);

                        sSendToServer_InternalLoanDisbursement = "0";
                        sIncome_Total = 0;
                    }*/
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

                break;

            case R.id.autoFillCheck:

                String similiar_Income;
                // isValues = true;

                if (mAutoFill.isChecked()) {

                    try {
                        // Makes all edit fields holds the same savings
                        similiar_Income = sIncomeFields.get(0).getText().toString();

                        for (int i = 0; i < sIncomeAmounts.length; i++) {
                            sIncomeFields.get(i).setText(similiar_Income);
                            sIncomeFields.get(i).setGravity(Gravity.RIGHT);
                            sIncomeFields.get(i).clearFocus();
                            sIncomeAmounts[i] = similiar_Income;
                        }
                        /** To clear the values of EditFields in case of uncheck **/
                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();

                        TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }
                }
                break;

            case R.id.fragment_Edit:


                confirmationDialog.dismiss();

                break;
            case R.id.frag_Ok:

                confirmationDialog.dismiss();

                if (networkConnection.isNetworkAvailable()) {
                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 1) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }

                } else {

                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 2) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }

                }

             /*   if (LD_EL_SBLoanMenu.sSelectedIncomeMenu.toUpperCase().equals(AppStrings.mIncreaseLimit)) {

                    LD_EL_LD_LoanAcc.lEntry.setLoanDisbursmentDetails(memLoanDisbursement);

                    String sreqString = new Gson().toJson(LD_EL_LD_LoanAcc.lEntry);
                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();
                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.LD_EL_ILIM_DIS, sreqString, getActivity(), ServiceType.LD_EL_ILIM_DIS);
                    }

                } else*/


                if (LD_EL_SBLoanMenu.sSelectedIncomeMenu.toUpperCase().equals(AppStrings.mLoanDisbursementFromLoanAcc)) {
                    LD_EL_LD_LoanAcc.lEntry.setLoanDisbursmentDetails(memLoanDisbursement);

                    String sreqString = new Gson().toJson(LD_EL_LD_LoanAcc.lEntry);

                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();
                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.LD_EL_LFLAC_DIS, sreqString, getActivity(), ServiceType.LD_EL_ILIM_DIS);
                    }

                } else if (LD_EL_SBLoanMenu.sSelectedIncomeMenu.toUpperCase().equals(AppStrings.mLoanDisbursementFromSbAcc)) {
                    LD_EL_Sb_Loan_acc_disburse.lEntry.setLoanDisbursmentDetails(memLoanDisbursement);
                    LD_EL_LD_LoanAcc.lEntry.setDisbursementAmount(sIncome_Total + "");

                    String sreqString = new Gson().toJson(LD_EL_Sb_Loan_acc_disburse.lEntry);

                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();
                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.LD_EL_LFSBAC_DIS, sreqString, getActivity(), ServiceType.LD_EL_ILIM_DIS);
                    }
                } else if (LD_EL_SBLoanMenu.sSelectedIncomeMenu.toUpperCase().equals(AppStrings.mLoanDisbursementFromRepaid)) {
                    LD_EL_LD_Repaid.lEntry.setLoanDisbursmentDetails(memLoanDisbursement);


                    String sreqString = new Gson().toJson(LD_EL_LD_Repaid.lEntry);
                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();
                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.LD_EL_CCLW_DIS, sreqString, getActivity(), ServiceType.LD_EL_ILIM_DIS);
                    }
                }

                break;
        }

    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }


    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
            }
        }

        switch (serviceType) {
            case LD_EL_ILIM_DIS:
                try {
                    ResponseDto cdto = new Gson().fromJson(result, ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        insertTransaction_tdy();
                        if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                            SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                        }
                        if(!LD_EL_LD_Repaid.outstanding.isEmpty())
                        {
                            outstaningamount=(int) (Double.parseDouble(LD_EL_LD_Repaid.outstanding))+(int) (Double.parseDouble(LD_EL_LD_Repaid.disbursementAmount));

                        }

                        int cih = 0, cab = 0, disAmount = 0, os = 0;
                        if (LD_EL_SBLoanMenu.sSelectedIncomeMenu.toUpperCase().equals(AppStrings.mLoanDisbursementFromRepaid)) {
                            if (LD_EL_LD_Repaid.lEntry.getModeOfCash().equals("1")) {
                                if (LD_EL_LD_Repaid.lEntry.getDisbursementAmount() != null && LD_EL_LD_Repaid.lEntry.getDisbursementAmount().length() > 0)
                                    disAmount = (int) Double.parseDouble(LD_EL_LD_Repaid.lEntry.getDisbursementAmount()) + (int) Double.parseDouble(shgDto.getCashAtBank());
                                try {
                                    cab = disAmount;
                                    String cihstr = "", cabstr = "", lastTranstr = "";
                                    cihstr = shgDto.getCashInHand();
                                    cabstr = cab + "";
                                    lastTranstr = shgDto.getLastTransactionDate();

                                    CashOfGroup csg = new CashOfGroup();
                                    csg.setCashInHand(cihstr);
                                    csg.setCashAtBank(cabstr);
                                    csg.setLastTransactionDate(lastTranstr);
                                    SHGTable.updateSHGDetails(csg, shgDto.getShgId());

                                    if (LD_EL_LD_Repaid.lEntry.getDisbursementAmount() != null && LD_EL_LD_Repaid.lEntry.getDisbursementAmount().length() > 0)
                                        os = disAmount + (int) Double.parseDouble(MySharedPreference.readString(getActivity(), MySharedPreference.DB_AMT, ""));

                                    if (os > 0) {
                                        OfflineDto off = new OfflineDto();
                                        off.setLoanId(LD_EL_LD_Repaid.lEntry.getLoanId());
                                        off.setModeOCash(LD_EL_LD_Repaid.lEntry.getModeOfCash());
                                        off.setShgSavingsAccountId(LD_EL_LD_Repaid.lEntry.getSbAccountId());
                                        int cb = 0;

                                        dismountadd = Integer.parseInt(LD_EL_LD_Repaid.disbursementAmount);
                                        if(sIncome_Total>0)
                                            cb = (((int) Double.parseDouble(LD_EL_LD_Repaid.mSelectedBank.getCurrentBalance()))+(dismountadd-sIncome_Total));
                                        else
                                            cb = (((int) Double.parseDouble(LD_EL_LD_Repaid.mSelectedBank.getCurrentBalance()))+(dismountadd-sIncome_Total));


//                                        cb = (((int) Double.parseDouble(LD_EL_LD_Repaid.mSelectedBank.getCurrentBalance()))+sIncome_Total);
                                        off.setCurr_balance(cb+"");
                                        off.setGrouploan_Outstanding(String.valueOf(outstaningamount));
                                        off.setAccountNumber(LD_EL_LD_Repaid.mSelectedBank.getShgSavingsAccountId());
                                        updateGRPLOS(off);
                                        updateBKCB(off);
                                    }

                                    if (sIncome_Total > 0) {

                                        for (MemberList md : memLoanDisbursement) {
                                            OfflineDto dto = new OfflineDto();
                                            dto.setLoanId(LD_EL_LD_Repaid.lEntry.getLoanId());
                                            //  dto.setMem_os(md.getLoanAmount());
                                            dto.setMemberId(md.getMemberId());
                                            if (LoanTable.getLD_MemDetails(shgDto.getShgId(), dto) != null) {
                                                ExistingLoan el = LoanTable.getLD_MemDetails(shgDto.getShgId(), dto);
                                                int os1 = (int) Double.parseDouble((el.getMemberLoanOutstanding() != null && el.getMemberLoanOutstanding().length() > 0) ? el.getMemberLoanOutstanding() : "0") + (int) Double.parseDouble(md.getLoanAmount());
                                                dto.setMem_os(os1 + "");
                                                LoanTable.updateMROSDetails(dto);
                                            }
                                        }
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            } else if (LD_EL_LD_Repaid.lEntry.getModeOfCash().equals("2")) {
                                if (LD_EL_LD_Repaid.lEntry.getDisbursementAmount() != null && LD_EL_LD_Repaid.lEntry.getDisbursementAmount().length() > 0)
                                    disAmount = (int) Double.parseDouble(LD_EL_LD_Repaid.lEntry.getDisbursementAmount()) + (int) Double.parseDouble(shgDto.getCashInHand());
                                try {
                                    cih = disAmount;
                                    String cihstr = "", cabstr = "", lastTranstr = "";
                                    cabstr = shgDto.getCashAtBank();
                                    cihstr = cih + "";
                                    lastTranstr = shgDto.getLastTransactionDate();

                                    CashOfGroup csg = new CashOfGroup();
                                    csg.setCashInHand(cihstr);
                                    csg.setCashAtBank(cabstr);
                                    csg.setLastTransactionDate(lastTranstr);
                                    SHGTable.updateSHGDetails(csg, shgDto.getShgId());

                                    if (LD_EL_LD_Repaid.lEntry.getDisbursementAmount() != null && LD_EL_LD_Repaid.lEntry.getDisbursementAmount().length() > 0)
                                        os = disAmount + (int) Double.parseDouble(MySharedPreference.readString(getActivity(), MySharedPreference.DB_AMT, ""));

                                    if (os > 0) {
                                        OfflineDto off = new OfflineDto();
                                        off.setLoanId(LD_EL_LD_Repaid.lEntry.getLoanId());
                                        off.setModeOCash(LD_EL_LD_Repaid.lEntry.getModeOfCash());
                                        //   off.setShgSavingsAccountId(LD_EL_LD_Repaid.lEntry.getSbAccountId());
                                        off.setGrouploan_Outstanding(String.valueOf(outstaningamount));
                                        updateGRPLOS(off);
                                    }

                                    if (sIncome_Total > 0) {

                                        for (MemberList md : memLoanDisbursement) {
                                            OfflineDto dto = new OfflineDto();
                                            dto.setLoanId(LD_EL_LD_Repaid.lEntry.getLoanId());
                                            //  dto.setMem_os(md.getLoanAmount());
                                            dto.setMemberId(md.getMemberId());
                                            if (LoanTable.getLD_MemDetails(shgDto.getShgId(), dto) != null) {
                                                ExistingLoan el = LoanTable.getLD_MemDetails(shgDto.getShgId(), dto);
                                                int os1 = (int) Double.parseDouble((el.getMemberLoanOutstanding() != null && el.getMemberLoanOutstanding().length() > 0) ? el.getMemberLoanOutstanding() : "0") + (int) Double.parseDouble(md.getLoanAmount());
                                                dto.setMem_os(os1 + "");
                                                LoanTable.updateMROSDetails(dto);
                                            }
                                        }

                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else if (LD_EL_SBLoanMenu.sSelectedIncomeMenu.toUpperCase().equals(AppStrings.mLoanDisbursementFromLoanAcc)) {
                            if (LD_EL_LD_LoanAcc.lEntry.getModeOfCash().equals("1")) {

                                if (LD_EL_LD_LoanAcc.lEntry.getDisbursementAmount() != null && LD_EL_LD_LoanAcc.lEntry.getDisbursementAmount().length() > 0)
                                    disAmount = (int) Double.parseDouble(LD_EL_LD_LoanAcc.lEntry.getDisbursementAmount()) + (int) Double.parseDouble(shgDto.getCashAtBank());
                                try {
                                    cab = disAmount;
                                    String cihstr = "", cabstr = "", lastTranstr = "";
                                    cihstr = shgDto.getCashInHand();
                                    cabstr = cab + "";
                                    lastTranstr = shgDto.getLastTransactionDate();

                                    CashOfGroup csg = new CashOfGroup();
                                    csg.setCashInHand(cihstr);
                                    csg.setCashAtBank(cabstr);
                                    csg.setLastTransactionDate(lastTranstr);
                                    SHGTable.updateSHGDetails(csg, shgDto.getShgId());

                                    if (LD_EL_LD_LoanAcc.lEntry.getDisbursementAmount() != null && LD_EL_LD_LoanAcc.lEntry.getDisbursementAmount().length() > 0)
                                        os = disAmount + (int) Double.parseDouble(MySharedPreference.readString(getActivity(), MySharedPreference.DB_AMT, ""));

                                    if (os > 0) {
                                        OfflineDto off = new OfflineDto();
                                        off.setLoanId(LD_EL_LD_LoanAcc.lEntry.getLoanId());
                                        off.setModeOCash(LD_EL_LD_LoanAcc.lEntry.getModeOfCash());
                                        off.setShgSavingsAccountId(LD_EL_LD_LoanAcc.lEntry.getSbAccountId());
                                        int cb = 0;
                                        dismountadd = Integer.parseInt(LD_EL_LD_LoanAcc.disbursementAmount);
                                        if(sIncome_Total>0)
                                            cb = (((int) Double.parseDouble(LD_EL_LD_LoanAcc.mSelectedBank.getCurrentBalance()))+(dismountadd-sIncome_Total));
                                        else

                                            cb = (((int) Double.parseDouble(LD_EL_LD_LoanAcc.mSelectedBank.getCurrentBalance()))+(dismountadd-sIncome_Total));

                                        off.setCurr_balance(cb+"");
                                        off.setAccountNumber(LD_EL_LD_LoanAcc.mSelectedBank.getShgSavingsAccountId());
                                        updateGRPLOS(off);
                                        updateBKCB(off);
                                    }

                                    if (sIncome_Total > 0) {

                                        for (MemberList md : memLoanDisbursement) {
                                            OfflineDto dto = new OfflineDto();
                                            dto.setLoanId(LD_EL_LD_LoanAcc.lEntry.getLoanId());
                                            //   dto.setMem_os(md.getLoanAmount());
                                            dto.setMemberId(md.getMemberId());
                                            if (LoanTable.getLD_MemDetails(shgDto.getShgId(), dto) != null) {
                                                ExistingLoan el = LoanTable.getLD_MemDetails(shgDto.getShgId(), dto);
                                                int os1 = (int) Double.parseDouble((el.getMemberLoanOutstanding() != null && el.getMemberLoanOutstanding().length() > 0) ? el.getMemberLoanOutstanding() : "0") + (int) Double.parseDouble(md.getLoanAmount());
                                                dto.setMem_os(os1 + "");
                                                LoanTable.updateMROSDetails(dto);
                                            }
                                        }

                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            } else if (LD_EL_LD_LoanAcc.lEntry.getModeOfCash().equals("2")) {

                                if (LD_EL_LD_LoanAcc.lEntry.getDisbursementAmount() != null && LD_EL_LD_LoanAcc.lEntry.getDisbursementAmount().length() > 0)
                                    disAmount = (int) Double.parseDouble(LD_EL_LD_LoanAcc.lEntry.getDisbursementAmount()) + (int) Double.parseDouble(shgDto.getCashInHand());
                                try {
                                    cih = disAmount;
                                    String cihstr = "", cabstr = "", lastTranstr = "";
                                    cabstr = shgDto.getCashAtBank();
                                    cihstr = cih + "";
                                    lastTranstr = shgDto.getLastTransactionDate();

                                    CashOfGroup csg = new CashOfGroup();
                                    csg.setCashInHand(cihstr);
                                    csg.setCashAtBank(cabstr);
                                    csg.setLastTransactionDate(lastTranstr);
                                    SHGTable.updateSHGDetails(csg, shgDto.getShgId());

                                    if (LD_EL_LD_LoanAcc.lEntry.getDisbursementAmount() != null && LD_EL_LD_LoanAcc.lEntry.getDisbursementAmount().length() > 0)
                                        os = disAmount + (int) Double.parseDouble(MySharedPreference.readString(getActivity(), MySharedPreference.DB_AMT, ""));

                                    if (os > 0) {
                                        OfflineDto off = new OfflineDto();
                                        off.setLoanId(LD_EL_LD_LoanAcc.lEntry.getLoanId());
                                        off.setModeOCash(LD_EL_LD_LoanAcc.lEntry.getModeOfCash());
                                        updateGRPLOS(off);
                                    }

                                    if (sIncome_Total > 0) {

                                        for (MemberList md : memLoanDisbursement) {
                                            OfflineDto dto = new OfflineDto();
                                            dto.setLoanId(LD_EL_LD_LoanAcc.lEntry.getLoanId());
                                            //    dto.setMem_os(md.getLoanAmount());
                                            dto.setMemberId(md.getMemberId());
                                            //    LoanTable.updateMROSDetails(dto);
                                            if (LoanTable.getLD_MemDetails(shgDto.getShgId(), dto) != null) {
                                                ExistingLoan el = LoanTable.getLD_MemDetails(shgDto.getShgId(), dto);
                                                int os1 = (int) Double.parseDouble((el.getMemberLoanOutstanding() != null && el.getMemberLoanOutstanding().length() > 0) ? el.getMemberLoanOutstanding() : "0") + (int) Double.parseDouble(md.getLoanAmount());
                                                dto.setMem_os(os1 + "");
                                                LoanTable.updateMROSDetails(dto);
                                            }
                                        }

                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        } else if (LD_EL_SBLoanMenu.sSelectedIncomeMenu.toUpperCase().equals(AppStrings.mLoanDisbursementFromSbAcc)) {
                            if (LD_EL_Sb_Loan_acc_disburse.lEntry.getModeOfCash().equals("1")) {
                                if (LD_EL_Sb_Loan_acc_disburse.lEntry.getDisbursementAmount() != null && LD_EL_Sb_Loan_acc_disburse.lEntry.getDisbursementAmount().length() > 0)
                                    disAmount = (int) Double.parseDouble(LD_EL_Sb_Loan_acc_disburse.lEntry.getDisbursementAmount()) + (int) Double.parseDouble(shgDto.getCashAtBank());
                                try {
                                    cab = disAmount;
                                    String cihstr = "", cabstr = "", lastTranstr = "";
                                    cihstr = shgDto.getCashInHand();
                                    cabstr = cab + "";
                                    lastTranstr = shgDto.getLastTransactionDate();

                                    CashOfGroup csg = new CashOfGroup();
                                    csg.setCashInHand(cihstr);
                                    csg.setCashAtBank(cabstr);
                                    csg.setLastTransactionDate(lastTranstr);
                                    SHGTable.updateSHGDetails(csg, shgDto.getShgId());

                                    if (LD_EL_Sb_Loan_acc_disburse.lEntry.getDisbursementAmount() != null && LD_EL_Sb_Loan_acc_disburse.lEntry.getDisbursementAmount().length() > 0)
                                        os = disAmount + (int) Double.parseDouble(MySharedPreference.readString(getActivity(), MySharedPreference.DB_AMT, ""));

                                    if (os > 0) {
                                        OfflineDto off = new OfflineDto();
                                        off.setLoanId(LD_EL_Sb_Loan_acc_disburse.lEntry.getLoanId());
                                        off.setModeOCash(LD_EL_Sb_Loan_acc_disburse.lEntry.getModeOfCash());
                                        off.setShgSavingsAccountId(LD_EL_Sb_Loan_acc_disburse.lEntry.getSbAccountId());
                                        int cb = 0;
                                        cb = (((int) Double.parseDouble(LD_EL_Sb_Loan_acc_disburse.mSelectedBank.getCurrentBalance()))-sIncome_Total);
                                        off.setCurr_balance(cb+"");
                                        off.setAccountNumber(LD_EL_Sb_Loan_acc_disburse.mSelectedBank.getShgSavingsAccountId());
                                        updateGRPLOS(off);
                                        updateBKCB(off);
                                    }

                                    if (sIncome_Total > 0) {

                                        for (MemberList md : memLoanDisbursement) {
                                            OfflineDto dto = new OfflineDto();
                                            dto.setLoanId(LD_EL_Sb_Loan_acc_disburse.lEntry.getLoanId());
                                            //  dto.setMem_os(md.getLoanAmount());
                                            dto.setMemberId(md.getMemberId());

                                            if (LoanTable.getLD_MemDetails(shgDto.getShgId(), dto) != null) {
                                                ExistingLoan el = LoanTable.getLD_MemDetails(shgDto.getShgId(), dto);
                                                int os1 = (int) Double.parseDouble((el.getMemberLoanOutstanding() != null && el.getMemberLoanOutstanding().length() > 0) ? el.getMemberLoanOutstanding() : "0") + (int) Double.parseDouble(md.getLoanAmount());
                                                dto.setMem_os(os1 + "");
                                                LoanTable.updateMROSDetails(dto);
                                            }
                                        }

                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } else if (LD_EL_Sb_Loan_acc_disburse.lEntry.getModeOfCash().equals("2")) {

                                if (LD_EL_Sb_Loan_acc_disburse.lEntry.getDisbursementAmount() != null && LD_EL_Sb_Loan_acc_disburse.lEntry.getDisbursementAmount().length() > 0)
                                    disAmount = (int) Double.parseDouble(LD_EL_Sb_Loan_acc_disburse.lEntry.getDisbursementAmount()) + (int) Double.parseDouble(shgDto.getCashInHand());
                                try {
                                    cih = disAmount;
                                    String cihstr = "", cabstr = "", lastTranstr = "";
                                    cabstr = shgDto.getCashAtBank();
                                    cihstr = cih + "";
                                    lastTranstr = shgDto.getLastTransactionDate();

                                    CashOfGroup csg = new CashOfGroup();
                                    csg.setCashInHand(cihstr);
                                    csg.setCashAtBank(cabstr);
                                    csg.setLastTransactionDate(lastTranstr);
                                    SHGTable.updateSHGDetails(csg, shgDto.getShgId());

                                    if (LD_EL_Sb_Loan_acc_disburse.lEntry.getDisbursementAmount() != null && LD_EL_Sb_Loan_acc_disburse.lEntry.getDisbursementAmount().length() > 0)
                                        os = disAmount + (int) Double.parseDouble(MySharedPreference.readString(getActivity(), MySharedPreference.DB_AMT, ""));

                                    if (os > 0) {
                                        OfflineDto off = new OfflineDto();
                                        off.setLoanId(LD_EL_Sb_Loan_acc_disburse.lEntry.getLoanId());
                                        off.setModeOCash(LD_EL_Sb_Loan_acc_disburse.lEntry.getModeOfCash());
                                        updateGRPLOS(off);
                                    }

                                    if (sIncome_Total > 0) {
                                        for (MemberList md : memLoanDisbursement) {
                                            OfflineDto dto = new OfflineDto();
                                            dto.setLoanId(LD_EL_Sb_Loan_acc_disburse.lEntry.getLoanId());
                                            dto.setMemberId(md.getMemberId());
                                            if (LoanTable.getLD_MemDetails(shgDto.getShgId(), dto) != null) {
                                                ExistingLoan el = LoanTable.getLD_MemDetails(shgDto.getShgId(), dto);
                                                int os1 = (int) Double.parseDouble((el.getMemberLoanOutstanding() != null && el.getMemberLoanOutstanding().length() > 0) ? el.getMemberLoanOutstanding() : "0") + (int) Double.parseDouble(md.getLoanAmount());
                                                dto.setMem_os(os1 + "");
                                                LoanTable.updateMROSDetails(dto);
                                            }else{

                                            }

                                        }

                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }


                        FragmentManager fm = getFragmentManager();
                        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        MainFragment mainFragment = new MainFragment();
                        Bundle bundles = new Bundle();
                        bundles.putString("Transaction", MainFragment.Flag_Transaction);
                        mainFragment.setArguments(bundles);
                        NewDrawerScreen.showFragment(mainFragment);

                    } else {
                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        }
                        Utils.showToast(getActivity(), message);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }


    }


    private void updateGRPLOS(OfflineDto dto) {
        String grouploan_outstanding = "";
        grouploan_outstanding = dto.getGrouploan_Outstanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, dto.getLoanId());


        if (!dto.getModeOCash().equals("2"))
            SHGTable.updateSelectedBankCurrentBalanceDetails(dto, dto.getShgSavingsAccountId());
    }

    private void updateBKCB(OfflineDto dto) {
        if (dto.getCurr_balance() != null || dto.getCurr_balance().length() > 0) {
            SHGTable.updateBankCurrentBalanceDetails(dto);
        }
    }

}

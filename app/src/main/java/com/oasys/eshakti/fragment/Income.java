package com.oasys.eshakti.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Adapter.CustomListAdapter;
import com.oasys.eshakti.Dto.IncomeType;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.RecyclerViewListener;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.DbHelper;
import com.oasys.eshakti.database.MemberTable;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.model.ListItem;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;


import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.List;

public class Income extends Fragment implements RecyclerViewListener, AdapterView.OnItemClickListener, NewTaskListener {

    public static String TAG = Income.class.getSimpleName();
    private TextView mGroupName, mCashinHand, mCashatBank;
    public static IncomeType sSelectedIncomeMenu = null;
    private NetworkConnection networkConnection;
    ArrayList<IncomeType> incomeMenu;/*
     * = { AppStrings.subscriptioncharges,
     * AppStrings.penalty,AppStrings.otherincome,
     * AppStrings.donation, AppStrings.mSeedFund};
     */

    private ListView mListView;
    private List<ListItem> listItems;
    private CustomListAdapter mAdapter;
    int listImage;
    private TextView mHeader;
    private Dialog mProgressDilaog;
    private View rootView;
    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;

    public Income() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        init();
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        incomeMenu = DbHelper.getInstance(getActivity()).getIncExpData("Income Type");

        if (incomeMenu.size() > 0) {

            for (IncomeType it : incomeMenu) {
                    ListItem rowItem = new ListItem();
                    rowItem.setImageId(listImage);
                    rowItem.setId(it.getId());
                    rowItem.setTitle(it.getName());
                    listItems.add(rowItem);
            }

            mAdapter = new CustomListAdapter(getActivity(), listItems);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(this);
        }
       /* if (networkConnection.isNetworkAvailable()) {
            onTaskStarted();
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.INCOME, getActivity(), ServiceType.INCOME);
        }*/

    }

    private void init() {

        try {
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);


            mHeader = (TextView) rootView.findViewById(R.id.submenuHeaderTextview);
            mHeader.setVisibility(View.VISIBLE);
            mHeader.setText("" + AppStrings.income);
            mHeader.setTypeface(LoginActivity.sTypeface);

            listItems = new ArrayList<ListItem>();
            mListView = (ListView) rootView.findViewById(R.id.fragment_List);
            listImage = R.drawable.ic_navigate_next_white_24dp;


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);


       /* incomeMenu = new String[]{AppStrings.subscriptioncharges, AppStrings.penalty, AppStrings.otherincome,
                AppStrings.donation, AppStrings.mSeedFund};
*/
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        rootView = inflater.inflate(R.layout.fragment_new_menulist, container, false);


        return rootView;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
    }

    @Override
    public void recyclerViewListClicked(View view, int position) throws InterruptedIOException {
        // TODO Auto-generated method stub

        sSelectedIncomeMenu = incomeMenu.get(position);

        if ((position == 0) || (position == 1) || (position == 2)) {

            OI_S_P_Fragment fragment = new OI_S_P_Fragment();
            NewDrawerScreen.showFragment(fragment);

        } else if ((position == 3)) {

            Donation_Income fragment = new Donation_Income();
            NewDrawerScreen.showFragment(fragment);
        } else if (position == 5) {
            SeedFund fragment = new SeedFund();
            NewDrawerScreen.showFragment(fragment);
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO Auto-generated method stub

        TextView textColor_Change = (TextView) view.findViewById(R.id.dynamicText);
        textColor_Change.setText(String.valueOf(incomeMenu.get(position).getName()));
        textColor_Change.setTextColor(Color.rgb(251, 161, 108));

        sSelectedIncomeMenu = incomeMenu.get(position);
        if (MySharedPreference.readInteger(getActivity(), MySharedPreference.INCOME_COUNT, 0) <= 0)
            MySharedPreference.writeInteger(getActivity(), MySharedPreference.INCOME_COUNT, 0);

        if ((position == 0) || (position == 1) || (position == 2)) {
            OI_S_P_Fragment fragment = new OI_S_P_Fragment();
            NewDrawerScreen.showFragment(fragment);
        } else if (incomeMenu.get(position).getName().equals("Donation")) {
            Donation_Income fragment = new Donation_Income();
            NewDrawerScreen.showFragment(fragment);
        } else if (incomeMenu.get(position).getName().equals("Seed Fund")) {
            SeedFund fragment = new SeedFund();
            NewDrawerScreen.showFragment(fragment);
        }

    }


    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;

            }
            switch (serviceType) {
                case INCOME:
                    try {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            incomeMenu = cdto.getResponseContent().getIncomeType();

                            for (IncomeType it : incomeMenu) {
                                ListItem rowItem = new ListItem();
                                rowItem.setImageId(listImage);
                                rowItem.setId(it.getId());
                                rowItem.setTitle(it.getIncomeType());
                                listItems.add(rowItem);
                            }

                            mAdapter = new CustomListAdapter(getActivity(), listItems);
                            mListView.setAdapter(mAdapter);
                            mListView.setOnItemClickListener(this);

                        } else {
                            Utils.showToast(getActivity(), message);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }

        }
    }
}

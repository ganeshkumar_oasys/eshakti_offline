package com.oasys.eshakti.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.Dto.MemberMobileNumbersDTOList;
import com.oasys.eshakti.Dto.MobileNumberRequestDto;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.GetSpanText;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.RegionalConversion;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.MemberTable;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.views.Get_EdiText_Filter;
import com.oasys.eshakti.views.TextviewUtils;
import com.tutorialsee.lib.TastyToast;

import java.util.ArrayList;
import java.util.List;


public class Pipnoupdate extends Fragment implements View.OnClickListener, NewTaskListener {
    private View rootView;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView mHeader, mAutoFill_label, mMemberName;
    private CheckBox mAutoFill;
    private ListOfShg shgDto;
    private LinearLayout mMemberNameLayout;
    private int mSize;
    private TableLayout mMobileNumberTable;
    private List<MemberList> memList;
    private EditText mMobileNumber_values;
    private  List<EditText> sMobilenumberFields;
    private Button mSubmit_Raised_Button;
    private  String sPipnumberfieds[];
    Dialog confirmationDialog;
    private Button  mEdit_RaisedButton, mOk_RaisedButton;
    ArrayList<MemberMobileNumbersDTOList> memberMobileNumbersDTOLists;
    private NetworkConnection networkConnection;
    private Dialog mProgressDilaog;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.pip_layout, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());

        memberMobileNumbersDTOLists = new ArrayList<>();
        sMobilenumberFields = new ArrayList<EditText>();
        init();
        return rootView;
    }

    private void init() {
        mGroupName = (TextView) rootView.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);

        mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);

        mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashatBank.setTypeface(LoginActivity.sTypeface);
        /** UI Mapping **/

        mHeader = (TextView) rootView.findViewById(R.id.internal_fragmentHeader);
        // mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.mInternalTypeLoan));
        mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.mPipNo));
        mHeader.setTypeface(LoginActivity.sTypeface);

        mAutoFill_label = (TextView) rootView.findViewById(R.id.internal_autofillLabel);
        mAutoFill_label.setText(RegionalConversion.getRegionalConversion(AppStrings.autoFill));
        mAutoFill_label.setTypeface(LoginActivity.sTypeface);


        mAutoFill = (CheckBox) rootView.findViewById(R.id.internal_autoFill);
        mAutoFill.setOnClickListener(this);

        mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
        mMemberName = (TextView) rootView.findViewById(R.id.member_name);

        TableLayout headerTable = (TableLayout) rootView.findViewById(R.id.internal_savingsTable);

        mMobileNumberTable = (TableLayout) rootView.findViewById(R.id.internal_fragment_contentTable);

        TableRow savingsHeader = new TableRow(getActivity());
        savingsHeader.setBackgroundResource(R.color.tableHeader);
        ViewGroup.LayoutParams headerParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,
                1f);
        TextView mMemberName_headerText = new TextView(getActivity());
        mMemberName_headerText
                .setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
        mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
        mMemberName_headerText.setTextColor(Color.WHITE);
        mMemberName_headerText.setPadding(20, 5, 10, 5);
        mMemberName_headerText.setLayoutParams(headerParams);
        savingsHeader.addView(mMemberName_headerText);

        TextView mIncomeAmount_HeaderText = new TextView(getActivity());
        mIncomeAmount_HeaderText
                .setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mPipNo)));
        mIncomeAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
        mIncomeAmount_HeaderText.setTextColor(Color.WHITE);
        mIncomeAmount_HeaderText.setPadding(0, 5, 0, 0);
        mIncomeAmount_HeaderText.setLayoutParams(headerParams);
        mIncomeAmount_HeaderText.setBackgroundResource(R.color.tableHeader);
        savingsHeader.addView(mIncomeAmount_HeaderText);

        headerTable.addView(savingsHeader,new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        for (int i = 0; i < mSize; i++) {

            TableRow indv_IncomeRow = new TableRow(getActivity());

            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 5, 10, 5);

            final TextView memberName_Text = new TextView(getActivity());
            memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                    String.valueOf(memList.get(i).getMemberName())));
            memberName_Text.setTypeface(LoginActivity.sTypeface);
            memberName_Text.setTextColor(R.color.black);
            memberName_Text.setPadding(10, 0, 10, 5);
            memberName_Text.setLayoutParams(contentParams);
            memberName_Text.setWidth(200);
            memberName_Text.setSingleLine(true);
            memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
            indv_IncomeRow.addView(memberName_Text);

            TableRow.LayoutParams contentEditParams = new TableRow.LayoutParams(150, ViewGroup.LayoutParams.WRAP_CONTENT);
            contentEditParams.setMargins(30, 5, 100, 5);

            mMobileNumber_values = new EditText(getActivity());

            mMobileNumber_values.setId(i);
            sMobilenumberFields.add(mMobileNumber_values);
            mMobileNumber_values.setGravity(Gravity.END);
            mMobileNumber_values.setTextColor(Color.BLACK);
            mMobileNumber_values.setPadding(5, 5, 5, 5);
            mMobileNumber_values.setBackgroundResource(R.drawable.edittext_background);
            mMobileNumber_values.setLayoutParams(contentEditParams);// contentParams
            // lParams
            mMobileNumber_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
            mMobileNumber_values.setFilters(Get_EdiText_Filter.editText_pip_number_filter());
            mMobileNumber_values.setInputType(InputType.TYPE_CLASS_NUMBER);
            mMobileNumber_values.setTextColor(R.color.black);
            mMobileNumber_values.setText(memList.get(i).getPipNumber());
            mMobileNumber_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    // TODO Auto-generated method stub
                    if (hasFocus) {
                        ((EditText) v).setGravity(Gravity.LEFT);

                        mMemberNameLayout.setVisibility(View.VISIBLE);
                        mMemberName.setText(memberName_Text.getText().toString().trim());
                        mMemberName.setTypeface(LoginActivity.sTypeface);
                        TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
                    } else {
                        ((EditText) v).setGravity(Gravity.RIGHT);
                        mMemberNameLayout.setVisibility(View.GONE);
                        mMemberName.setText("");
                    }

                }
            });
            indv_IncomeRow.addView(mMobileNumber_values);

            mMobileNumberTable.addView(indv_IncomeRow,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }

        mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.internal_fragment_Submit_button);
        mSubmit_Raised_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
        mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
        mSubmit_Raised_Button.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        sPipnumberfieds = new String[sMobilenumberFields.size()];

        switch (view.getId())
        {
            case R.id.internal_autoFill:

                String similiar_Income;
                // isValues = true;

                if (mAutoFill.isChecked()) {

                    try {

                        // Makes all edit fields holds the same savings
                        similiar_Income = sMobilenumberFields.get(0).getText().toString();

                        for (int i = 0; i < sPipnumberfieds.length; i++) {
                            sMobilenumberFields.get(i).setText(similiar_Income);
                            sMobilenumberFields.get(i).setGravity(Gravity.RIGHT);
                            sMobilenumberFields.get(i).clearFocus();
                            sPipnumberfieds[i] = similiar_Income;

                        }

                        /** To clear the values of EditFields in case of uncheck **/

                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();

                        TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }
                } else {

                }

                break;

            case R.id.internal_fragment_Submit_button:

                for (int i = 0; i < memList.size() ; i++) {

                    sPipnumberfieds[i] = sMobilenumberFields.get(i).getText().toString();

                }
                showDialog();
                break;

            case R.id.frag_Ok:
                pipNumberpostApiCall();
                break;
            case R.id.fragment_Edit:
                confirmationDialog.dismiss();
                break;
        }

    }

    private void pipNumberpostApiCall() {

        confirmationDialog.dismiss();

        for (int i = 0; i < memList.size() ; i++) {

            MemberMobileNumbersDTOList memberMobileNumbersDTOList = new MemberMobileNumbersDTOList();
//            memberMobileNumbersDTOList.setPipNumber(memList.get(i).getPhoneNumber());
            if(sMobilenumberFields.get(i).getText().toString().isEmpty())
            {
                memberMobileNumbersDTOList.setPipNumber("0");
            }
            else {
                memberMobileNumbersDTOList.setPipNumber(sMobilenumberFields.get(i).getText().toString());
            }
            memberMobileNumbersDTOList.setName(memList.get(i).getMemberName());
            memberMobileNumbersDTOList.setUserId(memList.get(i).getMemberUserId());
            memberMobileNumbersDTOList.setShgId(memList.get(i).getShgId());
            memberMobileNumbersDTOList.setMemberId(memList.get(i).getMemberId());
            memberMobileNumbersDTOLists.add(memberMobileNumbersDTOList);
            Log.d("NumbersDTOLists", " " + memberMobileNumbersDTOLists.size());

        }

        MobileNumberRequestDto mobileNumberRequestDto = new MobileNumberRequestDto();
        mobileNumberRequestDto.setMemberMobileNumbersDTOList(memberMobileNumbersDTOLists);
        String checkData = new Gson().toJson(mobileNumberRequestDto);
        Log.d("NumbersDTOLists", " " + checkData);

        if (networkConnection.isNetworkAvailable()) {
            onTaskStarted();
            RestClient.getRestClient(this).callRestWebServiceForPutMethod(Constants.BASE_URL + Constants.PROFILE_UPDATE_PIP_NUMBER, checkData, getActivity(), ServiceType.PIP_NUMBER_UPDATE);
        }


    }
    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
            }
            switch (serviceType) {
                case PIP_NUMBER_UPDATE:
                    try {
                        ResponseDto cdto = new Gson().fromJson(result, ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {

//                            insertTransaction_tdy();
                            if (memberMobileNumbersDTOLists != null && memberMobileNumbersDTOLists.size() > 0) {
                                for (MemberMobileNumbersDTOList mem : memberMobileNumbersDTOLists) {
                                    MemberTable.updateMemberPipNo(mem);
                                }
                            }
                            TastyToast.makeText(getActivity(),message,TastyToast.LENGTH_LONG,TastyToast.SUCCESS);
                            FragmentManager fm = getFragmentManager();
                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            MainFragment mainFragment = new MainFragment();
                            Bundle bundles = new Bundle();
                            bundles.putString("Profile",MainFragment.Flag_Profile);
                            mainFragment.setArguments(bundles);
                            NewDrawerScreen.showFragment(mainFragment);

                        } else {

                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                    mProgressDilaog.dismiss();
                                    mProgressDilaog = null;
                                }
                            }
                            Utils.showToast(getActivity(), message);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }

        }

    }



    private void showDialog() {

        confirmationDialog = new Dialog(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
        dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText(AppStrings.confirmation);
        confirmationHeader.setTypeface(LoginActivity.sTypeface);

        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

        TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        contentParams.setMargins(10, 5, 10, 5);


        TableRow header_row = new TableRow(getActivity());

        TableRow.LayoutParams headerParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        headerParams.setMargins(10, 5, 10, 5);

        TextView bankName_header = new TextView(getActivity());
        bankName_header.setText(AppStrings.memberName);
        bankName_header.setTypeface(LoginActivity.sTypeface);
        bankName_header.setTextColor(R.color.black_original);
        bankName_header.setPadding(5, 5, 5, 5);
        bankName_header.setLayoutParams(headerParams);
        header_row.addView(bankName_header);

       /* TextView branchName_header = new TextView(getActivity());
        branchName_header.setText(AppStrings.mPmsby);
        branchName_header.setTypeface(LoginActivity.sTypeface);
        branchName_header.setTextColor(R.color.black_original);
        branchName_header.setPadding(5, 5, 5, 5);
        branchName_header.setLayoutParams(headerParams);
        header_row.addView(branchName_header);

        TextView accNo_header = new TextView(getActivity());
        accNo_header.setText(AppStrings.mPmsbyNumber);
        accNo_header.setTypeface(LoginActivity.sTypeface);
        accNo_header.setTextColor(R.color.black_original);
        accNo_header.setPadding(5, 5, 5, 5);
        accNo_header.setLayoutParams(headerParams);
        header_row.addView(accNo_header);

        TextView mYear = new TextView(getActivity());
        mYear.setText(AppStrings.mYear);
        mYear.setTypeface(LoginActivity.sTypeface);
        mYear.setTextColor(R.color.black_original);
        mYear.setPadding(5, 5, 5, 5);
        mYear.setLayoutParams(headerParams);
        header_row.addView(mYear);*/

        TextView mDateenroll = new TextView(getActivity());
        mDateenroll.setText(AppStrings.mPipNo);
        mDateenroll.setTypeface(LoginActivity.sTypeface);
        mDateenroll.setTextColor(R.color.black_original);
        mDateenroll.setPadding(5, 5, 5, 5);
        mDateenroll.setLayoutParams(headerParams);
        header_row.addView(mDateenroll);

        confirmationTable.addView(header_row,
                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        for (int i = 0; i < memList.size(); i++) {

            TableRow indv_SavingsRow = new TableRow(getActivity());

            TextView memberName_Text = new TextView(getActivity());
            memberName_Text.setText(GetSpanText.getSpanString(getActivity(), memList.get(i).getMemberName()));
            memberName_Text.setTextColor(R.color.black);
            memberName_Text.setPadding(5, 5, 5, 5);
            memberName_Text.setLayoutParams(contentParams);
            indv_SavingsRow.addView(memberName_Text);

            TextView confirm_values = new TextView(getActivity());
            confirm_values.setText(GetSpanText.getSpanString(getActivity(),sPipnumberfieds[i]));
            confirm_values.setTextColor(R.color.black);
            confirm_values.setPadding(5, 5, 5, 5);
            confirm_values.setGravity(Gravity.RIGHT);
            confirm_values.setLayoutParams(contentParams);
            indv_SavingsRow.addView(confirm_values);
            confirmationTable.addView(indv_SavingsRow,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }
        View rullerView = new View(getActivity());
        rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
        rullerView.setBackgroundColor(Color.rgb(0, 199, 140));
        confirmationTable.addView(rullerView);

        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
        mEdit_RaisedButton.setText("" + AppStrings.edit);
        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mEdit_RaisedButton.setOnClickListener(this);

        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
        mOk_RaisedButton.setText("" + AppStrings.yes);
        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mOk_RaisedButton.setOnClickListener(this);


        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialogView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();

        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
        margin.leftMargin = 10;
        margin.rightMargin = 10;
        margin.topMargin = 10;
        margin.bottomMargin = 10;
        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
    }


}
package com.oasys.eshakti.fragment;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.Adapter.CustomItemAdapter;
import com.oasys.eshakti.Dialogue.Dialog_New_TransactionDate;
import com.oasys.eshakti.Dto.InternalLoanEntry;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.Dto.OfflineDto;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.Dto.ShgBankDetails;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.BankTable;
import com.oasys.eshakti.database.MemberTable;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.model.RowItem;
import com.oasys.eshakti.views.MaterialSpinner;
import com.oasys.eshakti.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by Dell on 15 Dec, 2018.
 */

public class LD_EL_Sb_Loan_acc_disburse extends Fragment implements View.OnClickListener, NewTaskListener {

    private TextView mGroupName, mCashInHand, mCashAtBank, mHeader;
    private TextView mDisbursementAmountText, mBalanceAmountText, mMemberDisbursementAmountText,
            mDisbursementAmount_value, mBalanceAmount_value, mMemberDisbursementAmount_value;
    private RaisedButton mSubmitButton;
    RadioButton mCashRadio, mBankRadio;
    public static String selectedType, selectedItemBank;

    MaterialSpinner materialSpinner_Bank;
    CustomItemAdapter bankNameAdapter;
    private List<RowItem> bankNameItems;
    public static String mBankNameValue = null;
    LinearLayout mSpinnerLayout;
    ArrayList<String> mBanknames_Array = new ArrayList<String>();
    ArrayList<String> mBanknamesId_Array = new ArrayList<String>();
    ArrayList<String> mEngSendtoServerBank_Array = new ArrayList<String>();
    ArrayList<String> mEngSendtoServerBankId_Array = new ArrayList<String>();
    Dialog confirmationDialog;
    View rootView;
    Date date_dashboard, date_loanDisb;
    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<ShgBankDetails> bankdetails;
    private Dialog mProgressDilaog;
    public static InternalLoanEntry lEntry = new InternalLoanEntry();
    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    public static ShgBankDetails mSelectedBank;
    public static String sbAcc_balanceAmount;
    OfflineDto offline =new OfflineDto();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_sb_acc_loandisburse, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));

        init();

        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        if (networkConnection.isNetworkAvailable()) {            //  onTaskStarted();
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.LD_FROM_SBAC + LD_ExternalLoan.sExistingLoagSelection.getLoanId(), getActivity(), ServiceType.LD_FROM_SBAC);

        }


    }

    private void init() {
        try {

            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);

            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);

            mHeader = (TextView) rootView.findViewById(R.id.sbAccDisbursementheader);
            mHeader.setTypeface(LoginActivity.sTypeface);
            mDisbursementAmountText = (TextView) rootView.findViewById(R.id.sbAcc_disbursementAmountTextView);
            mDisbursementAmountText.setTypeface(LoginActivity.sTypeface);
            mDisbursementAmount_value = (TextView) rootView.findViewById(R.id.sbAcc_disbursementAmount_values);
            mDisbursementAmount_value.setTypeface(LoginActivity.sTypeface);
            mBalanceAmountText = (TextView) rootView.findViewById(R.id.sbAcc_balanceAmountTextView);
            mBalanceAmountText.setTypeface(LoginActivity.sTypeface);
            mBalanceAmount_value = (TextView) rootView.findViewById(R.id.sbAcc_balanceAmount_values);
            mBalanceAmount_value.setTypeface(LoginActivity.sTypeface);
            mMemberDisbursementAmountText = (TextView) rootView.findViewById(R.id.sbAcc_memberDisbursementAmountTextView);
            mMemberDisbursementAmountText.setTypeface(LoginActivity.sTypeface);
            mMemberDisbursementAmount_value = (TextView) rootView.findViewById(R.id.sbAcc_memberDisbursementAmount_values);
            mMemberDisbursementAmount_value.setTypeface(LoginActivity.sTypeface);

            mCashRadio = (RadioButton) rootView.findViewById(R.id.radioDisbursementLimitCash_sb);
            mCashRadio.setTypeface(LoginActivity.sTypeface);
            mBankRadio = (RadioButton) rootView.findViewById(R.id.radioDisbursementLimitBank_sb);
            mBankRadio.setTypeface(LoginActivity.sTypeface);

            mHeader.setText(AppStrings.mLoanDisbursementFromSbAcc);
            mHeader.setTypeface(LoginActivity.sTypeface);
            mDisbursementAmountText.setText(AppStrings.mDisbursementAmount);
            mDisbursementAmountText.setTypeface(LoginActivity.sTypeface);
            mBalanceAmountText.setText(AppStrings.mBalanceAmount);
            mBalanceAmountText.setTypeface(LoginActivity.sTypeface);
            mMemberDisbursementAmountText
                    .setText(AppStrings.mMemberDisbursementAmount);
            mMemberDisbursementAmountText.setTypeface(LoginActivity.sTypeface);


            mSubmitButton = (RaisedButton) rootView.findViewById(R.id.sbAccDisbursement_submit);
            mSubmitButton.setText(AppStrings.next);
            mSubmitButton.setTypeface(LoginActivity.sTypeface);
            mSubmitButton.setOnClickListener(this);

            mSpinnerLayout = (LinearLayout) rootView.findViewById(R.id.loan_dis_bankSpinnerlayout_sb);
            mSpinnerLayout.setVisibility(View.GONE);

            materialSpinner_Bank = (MaterialSpinner) rootView.findViewById(R.id.loan_dis_bankspinner_sb);
            RadioGroup radioGroup = (RadioGroup) rootView.findViewById(R.id.radioDisbursementLimit_sb);
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    // checkedId is the RadioButton selected
                    switch (checkedId) {
                        case R.id.radioDisbursementLimitCash_sb:
                            selectedType = "Cash";
                            mSpinnerLayout.setVisibility(View.GONE);
                            break;

                        case R.id.radioDisbursementLimitBank_sb:
                            selectedType = "Bank";
                            mSpinnerLayout.setVisibility(View.VISIBLE);

                            break;
                    }
                }
            });
            for (int i = 0; i < bankdetails.size(); i++) {
                mBanknames_Array.add(bankdetails.get(i).getBankName().toString());
                mBanknamesId_Array.add(String.valueOf(i));
            }

            for (int i = 0; i < bankdetails.size(); i++) {
                mEngSendtoServerBank_Array.add(bankdetails.get(i).getBankName().toString());
                mEngSendtoServerBankId_Array.add(String.valueOf(i));
            }

            materialSpinner_Bank.setBaseColor(R.color.grey_400);

            materialSpinner_Bank.setFloatingLabelText(AppStrings.bankName);

            materialSpinner_Bank.setPaddingSafe(10, 0, 10, 0);

            final String[] bankNames = new String[bankdetails.size() + 1];

            final String[] bankNames_BankID = new String[bankdetails.size() + 1];

            final String[] bankAmount = new String[bankdetails.size() + 1];

            bankNames[0] = String.valueOf((AppStrings.bankName));
            for (int i = 0; i < bankdetails.size(); i++) {
                bankNames[i + 1] = bankdetails.get(i).getBankName().toString();
            }

            bankNames_BankID[0] = String.valueOf((AppStrings.bankName));
            for (int i = 0; i < bankdetails.size(); i++) {
                bankNames_BankID[i + 1] = bankdetails.get(i).getBankId().toString();
            }


            for (int i = 0; i < bankdetails.size(); i++) {
                mBanknames_Array.add(bankdetails.get(i).getBankName().toString());
                mBanknamesId_Array.add(String.valueOf(i));
            }

            for (int i = 0; i < bankdetails.size(); i++) {
                mEngSendtoServerBank_Array.add(bankdetails.get(i).getBankName().toString());
                mEngSendtoServerBankId_Array.add(bankdetails.get(i).getBankId());
            }

            bankAmount[0] = String.valueOf("Bank Amount");


            int size = bankNames.length;

            bankNameItems = new ArrayList<RowItem>();
            for (int i = 0; i < size; i++) {
                RowItem rowItem = new RowItem(bankNames[i]);// SelectedGroupsTask.sBankNames.elementAt(i).toString());
                bankNameItems.add(rowItem);
            }
            bankNameAdapter = new CustomItemAdapter(getActivity(), bankNameItems);
            materialSpinner_Bank.setAdapter(bankNameAdapter);

            materialSpinner_Bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub

                    if (position == 0) {
                        selectedItemBank = bankNames_BankID[0];
                        mBankNameValue = "0";
                        // EShaktiApplication.setSelectedBankAmount("");
                    } else {
                        selectedItemBank = bankNames_BankID[position];
                        System.out.println("SELECTED BANK NAME : " + selectedItemBank);
                        mBankNameValue = selectedItemBank;
                        mSelectedBank = bankdetails.get(position - 1);
                        String mBankname = null;

                        for (int i = 0; i < bankdetails.size(); i++) {
                            if (selectedItemBank.equals(mEngSendtoServerBankId_Array.get(i))) {
                                mBankname = mEngSendtoServerBank_Array.get(i);
                            }
                        }

                        ShgBankDetails details=BankTable.getBankTransaction(mSelectedBank.getShgSavingsAccountId());
                        mSelectedBank.setCurrentBalance(details.getCurrentBalance());
                        mBankNameValue = mBankname;

                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }
    private void insertTransaction_tdy()
    {

            offline.setIs_transaction_tdy("1.0");
            offline.setShgId(shgDto.getShgId());
            SHGTable.updateIstransaction(offline);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sbAccDisbursement_submit:

                offline.setIs_transaction_tdy("1.0");
                mBanknames_Array.clear();
                sbAcc_balanceAmount = mBalanceAmount_value.getText().toString();

                Log.e("SB ACCOUNT AMOUNT", sbAcc_balanceAmount + "");
                if (((int) Double.parseDouble(sbAcc_balanceAmount)) != 0) {

                    if (mBankRadio.isChecked() || mCashRadio.isChecked()) {
                        //	String dashBoardDate = DatePickerDialog.sDashboardDate;


                        if (mBankRadio.isChecked()) {

                            lEntry.setMobileDate(System.currentTimeMillis() + "");
//                            lEntry.setTransactionDate(shgDto.getLastTransactionDate());
                            lEntry.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                            lEntry.setShgId(shgDto.getShgId());
                            lEntry.setDisbursementAmount(mDisbursementAmount_value.getText().toString().trim());

                            lEntry.setLoanId(LD_ExternalLoan.sExistingLoagSelection.getLoanId());
                            lEntry.setModeOfCash("1");
                            //   lEntry.setSbAccountId(selectedItemBank);
                            lEntry.setSbAccountId(mSelectedBank.getShgSavingsAccountId());

                            LD_EL_Mem_Disburse loan_SB_disbursementFragment = new LD_EL_Mem_Disburse();
                            NewDrawerScreen.showFragment(loan_SB_disbursementFragment);


                        } else {
                            lEntry.setMobileDate(System.currentTimeMillis() + "");
//                            lEntry.setTransactionDate(shgDto.getLastTransactionDate());
                            lEntry.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                            lEntry.setShgId(shgDto.getShgId());
                            lEntry.setDisbursementAmount(mDisbursementAmount_value.getText().toString().trim());
                            lEntry.setLoanId(LD_ExternalLoan.sExistingLoagSelection.getLoanId());
                            lEntry.setModeOfCash("2");

                            LD_EL_Mem_Disburse loan_SB_disbursementFragment = new LD_EL_Mem_Disburse();
                            NewDrawerScreen.showFragment(loan_SB_disbursementFragment);

                        }


                    } else {
                        TastyToast.makeText(getActivity(), AppStrings.mLoanaccCash_BankToast, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }

                } else {
                    TastyToast.makeText(getActivity(), AppStrings.mCheckbalanceAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                }
                break;

        }
    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;
        }

        switch (serviceType) {
            case LD_FROM_SBAC:
                try {
                    if (result != null && result.length() > 0) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        ResponseDto mrDto = gson.fromJson(result, ResponseDto.class);
                        int statusCode = mrDto.getStatusCode();
                        String message = mrDto.getMessage();
                        Log.d("Main Frag response ", " " + statusCode);
                        if (statusCode == 400 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                            // showMessage(statusCode);

                        } else if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            Utils.showToast(getActivity(), message);
                        } else if (statusCode == Utils.Success_Code) {

                            insertTransaction_tdy();
                            String sMember_disburse_amnt = mrDto.getResponseContent().getMemberDisbursementAmount();
                            String sDisburse_amnt = mrDto.getResponseContent().getDisbursementAmount();
                            String sBal_amnt = mrDto.getResponseContent().getBalanceAmount();

                            int iMember_disburse_value = (int) Double.parseDouble(sMember_disburse_amnt);
                            int iDisburse_amnt_value = (int) Double.parseDouble(sDisburse_amnt);
                            int iBal_amnt_value = (int) Double.parseDouble(sBal_amnt);

                            String sSanctionAmount = String.valueOf(iMember_disburse_value);
                            String sDisbursementAmount = String.valueOf(iDisburse_amnt_value);
                            String sBalanceAmount = String.valueOf(iBal_amnt_value);


                            mMemberDisbursementAmount_value.setText((sSanctionAmount != null && sSanctionAmount.length() > 0) ? sSanctionAmount : "0");
                            mDisbursementAmount_value.setText((sDisbursementAmount != null && sDisbursementAmount.length() > 0) ? sDisbursementAmount : "0");
                            if (mDisbursementAmount_value.getText().toString() != null && mDisbursementAmount_value.getText().toString().length() > 0)
                                MySharedPreference.writeString(getActivity(), MySharedPreference.DB_AMT, mDisbursementAmount_value.getText().toString());
                            else
                                MySharedPreference.writeString(getActivity(), MySharedPreference.DB_AMT, "0");

                            mBalanceAmount_value.setText((sBalanceAmount != null && sBalanceAmount.length() > 0) ? sBalanceAmount : "0");

                        }
                       /* mMemberDisbursementAmount_value.setText("800");
                        mDisbursementAmount_value.setText("1000");
                        mBalanceAmount_value.setText("200");*/

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}

package com.oasys.eshakti.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Adapter.ShgAccountNumberAdapter;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.RequestDto.Profile_SHGAccountnumberRequestDto;
import com.oasys.eshakti.Dto.RequestDto.ShgAccountNumbersUpdateDTOList;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.Service.NewTaskListener;

import java.util.ArrayList;

public class Profile_SHGAccountnumber_updation extends Fragment implements NewTaskListener, View.OnClickListener {

    TextView bankname;
    TextView branchname;
    RecyclerView recyclerView;

    EditText accountnumber;
    EditText accountnumber2;
    Button submit;
    View view;
    ResponseDto shgbankupdationDto;
    ArrayList<ShgAccountNumbersUpdateDTOList> shgAccountNumbersUpdateDTOLists;
    LinearLayout linearLayout,linearLayout1;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView shgaccountnumberheader,shgaccount_bank,shg_name,shg_branchname,shg_accountno,
            shg_secondbank,shg_secondbankname,shg_secondbranchname,shg_secondaccountno;
    private ListOfShg shgDto;
    private LinearLayoutManager linearLayoutManager;
    private ShgAccountNumberAdapter shgAccountNumberAdapter;


    public Profile_SHGAccountnumber_updation() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile__shgaccountnumber_updation, container, false);
        // Inflate the layout for this fragment
        return view;
    }

    public void init() {
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        mGroupName = (TextView) view.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName()+" / "+shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);

        mCashinHand = (TextView) view.findViewById(R.id.cashinHand);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);

        mCashatBank = (TextView) view.findViewById(R.id.cashatBank);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashatBank.setTypeface(LoginActivity.sTypeface);
        
        linearLayout1 = (LinearLayout) view.findViewById(R.id.numberlayouts);
        submit = (Button) view.findViewById(R.id.shgau_submit);
        submit.setTypeface(LoginActivity.sTypeface);
        shgaccountnumberheader = (TextView) view.findViewById(R.id.shgaccountnumberheader);
        shgaccountnumberheader.setTypeface(LoginActivity.sTypeface);
        recyclerView = (RecyclerView)view.findViewById(R.id.bankdetails);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
           // String url = Constants.BASE_URL + Constants.PROFILE_GETSHGACCOUNTNUMBERUPDATION + "8bfab23f-4c58-42ff-9484-417c0838987c";
            String url = Constants.BASE_URL + Constants.PROFILE_GETSHGACCOUNTNUMBERUPDATION + shgDto.getShgId().toString();

            RestClient.getRestClient(Profile_SHGAccountnumber_updation.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.SHGACCOUNTNUMBER);
        } else {
            NewDrawerScreen.showFragment(new MainFragment());
            Utils.showToast(getActivity(), "No network Available");
        }
        shgAccountNumbersUpdateDTOLists = new ArrayList<>();
        submit.setOnClickListener(this);
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        try {
            switch (serviceType) {
                case SHGACCOUNTNUMBER:
                    Log.d("getDetails", " " + result.toString());
                    shgbankupdationDto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    if (shgbankupdationDto.getStatusCode() == Utils.Success_Code) {
                        Log.d("NumbersDTOLists", " " + shgbankupdationDto.getResponseContent().getShgBankDTOList().get(0).toString());

                        Utils.showToast(getActivity(), shgbankupdationDto.getMessage());
                        if ((shgbankupdationDto.getResponseContent().getShgBankDTOList() != null)) {

                            shgAccountNumberAdapter = new ShgAccountNumberAdapter(getActivity(),shgbankupdationDto.getResponseContent().getShgBankDTOList());
                            recyclerView.setAdapter(shgAccountNumberAdapter);
                         /*   if (shgbankupdationDto.getResponseContent().getShgBankDTOList().size() == 1) {

                                bankname.setText(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(0).getBankName());
                                branchname.setText(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(0).getBranchName());
                                accountnumber.setText(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(0).getAccountNumber());
                            } else {
                                linearLayout.setVisibility(View.VISIBLE);
                                bankname.setText(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(0).getBankName());
                                branchname.setText(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(0).getBranchName());
                                accountnumber.setText(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(0).getAccountNumber());

                                bankname2.setText(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(1).getBankName());
                                branchname2.setText(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(1).getBranchName());
                                accountnumber2.setText(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(1).getAccountNumber());
                            }
*/
//                            for (int i = 0; i <shgbankupdationDto.getResponseContent().getShgBankDTOList().size() ; i++) {
//
//                                bankname.setText(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(i).getBankName());
//                                branchname.setText(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(i).getBranchName());
//                                accountnumber.setText(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(i).getAccountNumber());
//                            }

                            /*for (int i = 0; i < shgbankupdationDto.getResponseContent().getShgBankDTOList().size(); i++) {
                                ShgAccountNumbersUpdateDTOList shgAccountNumbersUpdateDTOList = new ShgAccountNumbersUpdateDTOList();
                                shgAccountNumbersUpdateDTOList.setBankId(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(i).getBankId());
                                shgAccountNumbersUpdateDTOList.setAccountNumber(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(i).getAccountNumber());

                              //  shgAccountNumbersUpdateDTOList.setAccountNumber(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(i).getAccountNumber());
                                shgAccountNumbersUpdateDTOLists.add(shgAccountNumbersUpdateDTOList);
                                Log.d("shgAccountNumbers", " " + shgAccountNumbersUpdateDTOLists.size());
                            }*/
                            //String first = shgbankupdationDto.getResponseContent().getShgBankDTOList().get(0).toString();
                        } else {
                            if (shgbankupdationDto.getStatusCode() == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                            }
                            Utils.showToast(getActivity(), "Null Value");
                        }

                    }
                    break;
                case SHGACCOUNTNUMBERUPDATE:
                    shgbankupdationDto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    if (shgbankupdationDto.getStatusCode() == Utils.Success_Code) {
                        Utils.showToast(getActivity(), shgbankupdationDto.getMessage());
                        FragmentManager fm = getFragmentManager();
                        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        MainFragment mainFragment = new MainFragment();
                        Bundle bundles = new Bundle();
                        bundles.putString("Profile",MainFragment.Flag_Profile);
                        mainFragment.setArguments(bundles);
                        NewDrawerScreen.showFragment(mainFragment);
//                        NewDrawerScreen.showFragment(new MainFragment());
                    } else {
                        if (shgbankupdationDto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), shgbankupdationDto.getMessage());
                    }
                    break;
            }
        } catch (Exception e) {
            Log.e("BAnktransaction", e.toString());
        }
    }

    @Override
    public void onClick(View view) {

        for (int i = 0; i < shgbankupdationDto.getResponseContent().getShgBankDTOList().size(); i++) {

            ShgAccountNumbersUpdateDTOList shgAccountNumbersUpdateDTOList = new ShgAccountNumbersUpdateDTOList();
            shgAccountNumbersUpdateDTOList.setBankId(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(i).getBankId());
            shgAccountNumbersUpdateDTOList.setShgId(shgDto.getShgId());
            shgAccountNumbersUpdateDTOList.setAccountNumber(shgbankupdationDto.getResponseContent().getShgBankDTOList().get(i).getAccountNumber());

//            if (i == 0) {
//                shgAccountNumbersUpdateDTOList.setAccountNumber(accountnumber.getText().toString());
//            } else {
//                shgAccountNumbersUpdateDTOList.setAccountNumber(accountnumber2.getText().toString());
//            }

            shgAccountNumbersUpdateDTOLists.add(shgAccountNumbersUpdateDTOList);
            Log.d("shgAccountNumbers", " " + shgAccountNumbersUpdateDTOLists.size());
        }

        Profile_SHGAccountnumberRequestDto profile_shgAccountnumberRequestDto = new Profile_SHGAccountnumberRequestDto();
        profile_shgAccountnumberRequestDto.setShgAccountNumbersUpdateDTOList(shgAccountNumbersUpdateDTOLists);
        String checkData = new Gson().toJson(profile_shgAccountnumberRequestDto);

        Log.d("NumbersDTOLists", " " + checkData);
        RestClient.getRestClient(Profile_SHGAccountnumber_updation.this).callRestWebServiceForPutMethod(Constants.BASE_URL + Constants.PROFILE_SHGACCOUNTNUMBERUPDATION, checkData, getActivity(), ServiceType.SHGACCOUNTNUMBERUPDATE);

    }
}

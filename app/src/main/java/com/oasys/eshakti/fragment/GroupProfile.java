package com.oasys.eshakti.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.Dto.TableData;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.R;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.database.SHGTable;

import java.text.SimpleDateFormat;
import java.util.Date;

public class GroupProfile extends Fragment {
    private TextView shgtype;
    private TextView shgcode;
    private TextView shgname;
    private TextView shgcreationdate;
    private TextView shgverifieddate;
    private TextView telephonenumber;
    private TextView totalmembers;
    private TextView presidentname;
    private TextView secretaryname;
    private TextView treasurername;
    private TextView blockname;
    private TextView panchayatname;
    private TextView villagename;
    ResponseDto groupProfileResponseDto;
    private View rootView;
    private ListOfShg shgDto;
    private String shgId;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView groupprofile,shgtypes,shgcodes,shgnames,shgcreationdates,shgverifieddates,shgtelephonenumber,totalmemberss,
            presidentnames,secretarynames,treasurernames,shgblockname,panchayatnames,shgvillagename;
    private TableData tableData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgId = shgDto.getShgId();
        tableData = SHGTable.getGroupProfile(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        Log.d("groupdetails",""+tableData);
        rootView = inflater.inflate(R.layout.fragment_new_group_profile, container, false);
        return rootView;
    }

    public void init() {
        mGroupName = (TextView) rootView.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName()+" / "+shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);

        mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);

        mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashatBank.setTypeface(LoginActivity.sTypeface);

        shgtype = (TextView) rootView.findViewById(R.id.gp_shgtype);
        shgtype.setText(tableData.getShgType());
        shgtype.setTypeface(LoginActivity.sTypeface);
        shgcode = (TextView) rootView.findViewById(R.id.gp_shgcode);
        shgcode.setText(tableData.getCode());
        shgcode.setTypeface(LoginActivity.sTypeface);
        shgname = (TextView) rootView.findViewById(R.id.gp_shgname);
        shgname.setText(tableData.getName());
        shgname.setTypeface(LoginActivity.sTypeface);
        shgcreationdate = (TextView) rootView.findViewById(R.id.gp_shgdate);
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date d = new Date(tableData.getShgCreatedDate());
        Log.d("bankaccount",String.valueOf(tableData.getShgCreatedDate()));
        String dateStr = df.format(d);
        shgcreationdate.setText(dateStr);
        shgcreationdate.setTypeface(LoginActivity.sTypeface);
        shgverifieddate = (TextView) rootView.findViewById(R.id.gp_shgverifieddate);
        Date verifiedDate = new Date(tableData.getShgVerifiedDate());
        String verified_Date = df.format(verifiedDate);
        shgverifieddate.setText(verified_Date);
        shgverifieddate.setTypeface(LoginActivity.sTypeface);
        telephonenumber = (TextView) rootView.findViewById(R.id.gp_telephonenumber);
        String phoneNum= String.valueOf(tableData.getMobileNumber());
        telephonenumber.setText(phoneNum);
        telephonenumber.setTypeface(LoginActivity.sTypeface);
        totalmembers = (TextView) rootView.findViewById(R.id.gp_totalmembers);
        int totalgrp=(int) Double.parseDouble(tableData.getTotalMembers());
        totalmembers.setText(String.valueOf(totalgrp));
        totalmembers.setTypeface(LoginActivity.sTypeface);
        presidentname = (TextView) rootView.findViewById(R.id.gp_presidentname);
        presidentname.setText(tableData.getPresidentName());
        presidentname.setTypeface(LoginActivity.sTypeface);
        secretaryname = (TextView) rootView.findViewById(R.id.gp_secretaryname);
        secretaryname.setText(tableData.getSecretaryName());
        secretaryname.setTypeface(LoginActivity.sTypeface);
        treasurername = (TextView) rootView.findViewById(R.id.gp_treasurername);
        treasurername.setText(tableData.getTreasuereName());
        treasurername.setTypeface(LoginActivity.sTypeface);
        blockname = (TextView) rootView.findViewById(R.id.gp_blockname);
        blockname.setText(tableData.getBlockName());
        blockname.setTypeface(LoginActivity.sTypeface);
        panchayatname = (TextView) rootView.findViewById(R.id.gp_panchayatname);
        panchayatname.setText(tableData.getPanchayatName());
        panchayatname.setTypeface(LoginActivity.sTypeface);
        villagename = (TextView) rootView.findViewById(R.id.gp_villagename);
        villagename.setText(tableData.getVillageName());
        villagename.setTypeface(LoginActivity.sTypeface);

        groupprofile = (TextView) rootView.findViewById(R.id.groupprofile);
        groupprofile.setText(AppStrings.group_profile);
        groupprofile.setTypeface(LoginActivity.sTypeface);

        shgtypes = (TextView) rootView.findViewById(R.id.shgtypes);
        shgtypes.setText(AppStrings.Shg_type);
        shgtypes.setTypeface(LoginActivity.sTypeface);

        shgcodes = (TextView) rootView.findViewById(R.id.shgcodes);
        shgcodes.setText(AppStrings.Shg_code);
        shgcodes.setTypeface(LoginActivity.sTypeface);

        shgnames = (TextView) rootView.findViewById(R.id.shgnames);
        shgnames.setText(AppStrings.Shg_name);
        shgnames.setTypeface(LoginActivity.sTypeface);

        shgcreationdates = (TextView) rootView.findViewById(R.id.shgcreationdates);
        shgcreationdates.setText(AppStrings.Shg_creation_date);
        shgcreationdates.setTypeface(LoginActivity.sTypeface);

        shgverifieddates = (TextView) rootView.findViewById(R.id.shgverifieddates);
        shgverifieddates.setText(AppStrings.Shg_verified_date);
        shgverifieddates.setTypeface(LoginActivity.sTypeface);

        shgtelephonenumber = (TextView) rootView.findViewById(R.id.shgtelephonenumber);
        shgtelephonenumber.setText(AppStrings.telephonenumber);
        shgtelephonenumber.setTypeface(LoginActivity.sTypeface);

        totalmemberss = (TextView) rootView.findViewById(R.id.totalmemberss);
        totalmemberss.setText(AppStrings.Total_members);
        totalmemberss.setTypeface(LoginActivity.sTypeface);

        presidentnames = (TextView) rootView.findViewById(R.id.presidentnames);
        presidentnames.setText(AppStrings.President_name);
        presidentnames.setTypeface(LoginActivity.sTypeface);

        secretarynames = (TextView) rootView.findViewById(R.id.secretarynames);
        secretarynames.setText(AppStrings.Secretary_name);
        secretarynames.setTypeface(LoginActivity.sTypeface);

        treasurernames = (TextView) rootView.findViewById(R.id.treasurernames);
        treasurernames.setText(AppStrings.Treasurer_name);
        treasurernames.setTypeface(LoginActivity.sTypeface);


        shgblockname = (TextView) rootView.findViewById(R.id.shgblockname);
        shgblockname.setText(AppStrings.Block_name);
        shgblockname.setTypeface(LoginActivity.sTypeface);

        panchayatnames = (TextView) rootView.findViewById(R.id.panchayatnames);
        panchayatnames.setText(AppStrings.Panchayat_name);
        panchayatnames.setTypeface(LoginActivity.sTypeface);

        shgvillagename = (TextView) rootView.findViewById(R.id.shgvillagename);
        shgvillagename.setText(AppStrings.village_name);
        shgvillagename.setTypeface(LoginActivity.sTypeface);



    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
       /* String url = Constants.BASE_URL + Constants.PROFILE_GROUP_PROFILE + shgId;
               // "b6ae8d4b-76a8-456b-81a6-f759f579f946";
        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
            RestClient.getRestClient(GroupProfile.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.GROUPPROFILE);

        } else {
            Utils.showToast(getActivity(), "Server Connection Error");
        }*/
    }

   /* @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        switch (serviceType) {
            case GROUPPROFILE:
                groupProfileResponseDto = new Gson().fromJson(result.toString(), ResponseDto.class);
                if (groupProfileResponseDto.getStatusCode() == Utils.Success_Code) {
                    if ((groupProfileResponseDto.getResponseContent() != null)) {
                        groupProfileResponseDto.getResponseContent().getShgProfile();
                        if ((groupProfileResponseDto.getResponseContent().getShgProfile() != null)) {

                            shgtype.setText(groupProfileResponseDto.getResponseContent().getShgProfile().getShgType());
                            shgcode.setText(groupProfileResponseDto.getResponseContent().getShgProfile().getShgCode());
                            shgname.setText(groupProfileResponseDto.getResponseContent().getShgProfile().getShgName());
                            DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
                            String datevalue = groupProfileResponseDto.getResponseContent().getShgProfile().getShgCreatedDate();

                            if(datevalue.equals("-"))
                            {
                                shgcreationdate.setText("NA");
                            }
                            else
                            {
                                Date d = new Date(Long.parseLong(datevalue));
                                String dateStr = simple.format(d);
                                shgcreationdate.setText(dateStr);
                            }

                            telephonenumber.setText(groupProfileResponseDto.getResponseContent().getShgProfile().getMobileNumber());
                            totalmembers.setText(groupProfileResponseDto.getResponseContent().getShgProfile().getTotalMembers());
                            presidentname.setText(groupProfileResponseDto.getResponseContent().getShgProfile().getPresidentName());
                            secretaryname.setText(groupProfileResponseDto.getResponseContent().getShgProfile().getSecretaryName());
                            treasurername.setText(groupProfileResponseDto.getResponseContent().getShgProfile().getTreasuereName());
                            blockname.setText(groupProfileResponseDto.getResponseContent().getShgProfile().getBlockName());
                            panchayatname.setText(groupProfileResponseDto.getResponseContent().getShgProfile().getPanchayatName());
                            villagename.setText(groupProfileResponseDto.getResponseContent().getShgProfile().getVillageName());

                        } else {

                        }
                    } else {
                        Utils.showToast(getActivity(), "Null Value");
                    }
                } else {

                    if (groupProfileResponseDto.getStatusCode() == 401) {

                        Log.e("Group Logout", "Logout Sucessfully");
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                    }

                }
                break;
        }
    }*/
}

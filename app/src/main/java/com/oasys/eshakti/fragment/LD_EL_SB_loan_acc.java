package com.oasys.eshakti.fragment;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.oasys.eshakti.Adapter.CustomItemAdapter;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.Dto.ShgBankDetails;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.R;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.BankTable;
import com.oasys.eshakti.database.MemberTable;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.model.RowItem;
import com.oasys.eshakti.views.MaterialSpinner;
import com.oasys.eshakti.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Dell on 15 Dec, 2018.
 */

public class LD_EL_SB_loan_acc extends Fragment implements View.OnClickListener {

    private TextView mGroupName, mCashInHand, mCashAtBank, mHeader;
    private TextView mDisbursementAmountText, mBalanceAmountText, mMemberDisbursementAmountText,
            mDisbursementAmount_value, mBalanceAmount_value, mMemberDisbursementAmount_value;
    private RaisedButton mSubmitButton;
    RadioButton mCashRadio, mBankRadio;
    public static String selectedType, selectedItemBank;

    MaterialSpinner materialSpinner_Bank;
    CustomItemAdapter bankNameAdapter;
    private List<RowItem> bankNameItems;
    public static String mBankNameValue = null;
    LinearLayout mSpinnerLayout;
    ArrayList<String> mBanknames_Array = new ArrayList<String>();
    ArrayList<String> mBanknamesId_Array = new ArrayList<String>();
    ArrayList<String> mEngSendtoServerBank_Array = new ArrayList<String>();
    ArrayList<String> mEngSendtoServerBankId_Array = new ArrayList<String>();
    Dialog confirmationDialog;
    View rootView;
    Date date_dashboard, date_loanDisb;


    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<ShgBankDetails> bankdetails;
    public static ShgBankDetails mSelectedBank;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_sb_acc_loandisburse, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        init();
    }

    private void init() {

        try {

            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName()+" / "+shgDto.getPresidentName());

            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());

            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());


            mHeader = (TextView) rootView.findViewById(R.id.sbAccDisbursementheader);
            mDisbursementAmountText = (TextView) rootView.findViewById(R.id.sbAcc_disbursementAmountTextView);
            mDisbursementAmount_value = (TextView) rootView.findViewById(R.id.sbAcc_disbursementAmount_values);
            mBalanceAmountText = (TextView) rootView.findViewById(R.id.sbAcc_balanceAmountTextView);
            mBalanceAmount_value = (TextView) rootView.findViewById(R.id.sbAcc_balanceAmount_values);
            mMemberDisbursementAmountText = (TextView) rootView.findViewById(R.id.sbAcc_memberDisbursementAmountTextView);
            mMemberDisbursementAmount_value = (TextView) rootView.findViewById(R.id.sbAcc_memberDisbursementAmount_values);

            mCashRadio = (RadioButton) rootView.findViewById(R.id.radioDisbursementLimitCash_sb);
            mBankRadio = (RadioButton) rootView.findViewById(R.id.radioDisbursementLimitBank_sb);

            mHeader.setText(AppStrings.mLoanDisbursementFromSbAcc);
            mDisbursementAmountText.setText(AppStrings.mDisbursementAmount);
            mBalanceAmountText.setText(AppStrings.mBalanceAmount);
            mMemberDisbursementAmountText
                    .setText(AppStrings.mMemberDisbursementAmount);


            mSubmitButton = (RaisedButton) rootView.findViewById(R.id.sbAccDisbursement_submit);
            mSubmitButton.setText(AppStrings.next);
            mSubmitButton.setOnClickListener(this);

            mSpinnerLayout = (LinearLayout) rootView.findViewById(R.id.loan_dis_bankSpinnerlayout_sb);
            mSpinnerLayout.setVisibility(View.GONE);

            materialSpinner_Bank = (MaterialSpinner) rootView.findViewById(R.id.loan_dis_bankspinner_sb);
            RadioGroup radioGroup = (RadioGroup) rootView.findViewById(R.id.radioDisbursementLimit_sb);
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    // checkedId is the RadioButton selected
                    switch (checkedId) {
                        case R.id.radioDisbursementLimitCash_sb:
                            selectedType = "Cash";
                            mSpinnerLayout.setVisibility(View.GONE);
                            break;

                        case R.id.radioDisbursementLimitBank_sb:
                            selectedType = "Bank";
                            mSpinnerLayout.setVisibility(View.VISIBLE);

                            break;
                    }
                }
            });

            //init();
            for (int i = 0; i < bankdetails.size(); i++) {
                mBanknames_Array.add(bankdetails.get(i).getBankName().toString());
                mBanknamesId_Array.add(String.valueOf(i));
            }

            for (int i = 0; i < bankdetails.size(); i++) {
                mEngSendtoServerBank_Array.add(bankdetails.get(i).getBankName().toString());
                mEngSendtoServerBankId_Array.add(String.valueOf(i));
            }

            materialSpinner_Bank.setBaseColor(R.color.grey_400);

            materialSpinner_Bank.setFloatingLabelText(AppStrings.bankName);

            materialSpinner_Bank.setPaddingSafe(10, 0, 10, 0);

            final String[] bankNames = new String[bankdetails.size() + 1];

            final String[] bankNames_BankID = new String[bankdetails.size() + 1];

            final String[] bankAmount = new String[bankdetails.size() + 1];

            bankNames[0] = String.valueOf(AppStrings.bankName);
            for (int i = 0; i < bankdetails.size(); i++) {
                bankNames[i + 1] = bankdetails.get(i).getBankName().toString();
            }

            bankNames_BankID[0] = String.valueOf(AppStrings.bankName);
            for (int i = 0; i < bankdetails.size(); i++) {
                bankNames_BankID[i + 1] = bankdetails.get(i).getBankName().toString();
            }

            bankAmount[0] = String.valueOf("Bank Amount");


            int size = bankNames.length;

            bankNameItems = new ArrayList<RowItem>();
            for (int i = 0; i < size; i++) {
                RowItem rowItem = new RowItem(bankNames[i]);// .sBankNames.elementAt(i).toString());
                bankNameItems.add(rowItem);
            }
            bankNameAdapter = new CustomItemAdapter(getActivity(), bankNameItems);
            materialSpinner_Bank.setAdapter(bankNameAdapter);

            materialSpinner_Bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub

                    if (position == 0) {
                        selectedItemBank = bankNames_BankID[0];
                        mBankNameValue = "0";
                     //   EShaktiApplication.setSelectedBankAmount("");
                    } else {
                        selectedItemBank = bankNames_BankID[position];
                        System.out.println("SELECTED BANK NAME : " + selectedItemBank);
                        mBankNameValue = selectedItemBank;
                        mSelectedBank = bankdetails.get(position - 1);
                        String mBankname = null;
                        for (int i = 0; i < bankdetails.size(); i++) {
                            if (selectedItemBank.equals(mEngSendtoServerBank_Array.get(i))) {
                                mBankname = mEngSendtoServerBank_Array.get(i);
                              //  EShaktiApplication.setSelectedBankAmount(bankAmount[i + 1]);
                            }
                        }
                        ShgBankDetails details=BankTable.getBankTransaction(mSelectedBank.getShgSavingsAccountId());
                        mSelectedBank.setCurrentBalance(details.getCurrentBalance());

                        mBankNameValue = mBankname;










                    }
                    Log.e("Selected Bank value", mBankNameValue.toString() + "");
                 //   Log.e("Selected Bank Amount", EShaktiApplication.getSelectedBankAmount().toString() + "");

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sbAccDisbursement_submit:

                mBanknames_Array.clear();
                String sbAcc_balanceAmount = mBalanceAmount_value.getText().toString();

                Log.e("SB ACCOUNT AMOUNT", sbAcc_balanceAmount + "");
                if (Integer.parseInt(sbAcc_balanceAmount) != 0) {

                    if (mBankRadio.isChecked() || mCashRadio.isChecked()) {
                        //	String dashBoardDate = DatePickerDialog.sDashboardDate;
                        String dashBoardDate = null;


                        String loanDisbArr[] = EShaktiApplication.getLoanAcc_LoanDisbursementDate().split("/");
                        String loanDisbDate = loanDisbArr[1] + "-" + loanDisbArr[0] + "-" + loanDisbArr[2];

                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                        try {
                            date_dashboard = sdf.parse(dashBoardDate);
                            date_loanDisb = sdf.parse(loanDisbDate);
                        } catch (ParseException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }

                        if (date_dashboard.compareTo(date_loanDisb) >= 0) {

                            if (mBankRadio.isChecked()) {

                                if (!mBankNameValue.equals("0")) {
                                    EShaktiApplication.setSelectedType("BANK");
                                    EShaktiApplication.setSBAccBalanceAmount(sbAcc_balanceAmount);
                                    LD_EL_Loan_SB_disbursement disbursementFragment = new LD_EL_Loan_SB_disbursement();
                                    NewDrawerScreen.showFragment(disbursementFragment);

                                } else {
                                    TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert,
                                            TastyToast.LENGTH_SHORT,
                                            TastyToast.WARNING);
                                }
                            } else {
                                EShaktiApplication.setSelectedType("CASH");
                                EShaktiApplication
                                        .setSBAccBalanceAmount(sbAcc_balanceAmount);
                                LD_EL_Loan_SB_disbursement disbursementFragment = new LD_EL_Loan_SB_disbursement();
                                NewDrawerScreen.showFragment(disbursementFragment);

                            }

                        } else {
                            TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementDate, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                        }

                    } else {
                        TastyToast.makeText(getActivity(), AppStrings.mLoanaccCash_BankToast, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }

                } else {
                    TastyToast.makeText(getActivity(), AppStrings.mCheckbalanceAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                }
                break;

        }
    }
}

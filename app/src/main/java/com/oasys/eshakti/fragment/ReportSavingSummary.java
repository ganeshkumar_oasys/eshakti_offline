package com.oasys.eshakti.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Adapter.Report__MemberReport_SavingSummary_Adapter;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberSavingsSummary;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.database.SHGTable;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportSavingSummary extends Fragment implements NewTaskListener {
    private Report__MemberReport_SavingSummary_Adapter report__memberReport_savingSummary_adapter, report__memberReport_savingSummary_adapter1;
    private RecyclerView recyclerViewmemberreportsavingsummary;
    private LinearLayoutManager linearLayoutManager, linearLayoutManager1;
    ResponseDto responseDto;
    ArrayList<MemberSavingsSummary> memberSavingsSummaries;
    TextView totalamount, Total,mtotalAmount;
    private View view;
    private TextView mGroupName,msavingdisbursement;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView mHeader, Name,date,amount,totalDisburse;
    private ListOfShg shgDto;
    private String memberid;
    private RecyclerView recyclerViewmemberreportsavingDissummary;


    public ReportSavingSummary() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_report_saving_summary, container, false);
        init();
        Bundle bundle2 = getArguments();
        memberid = bundle2.getString("Memberid");
        Log.d("Mem3", memberid);
        return view;
    }

    private void init() {
        try {
            recyclerViewmemberreportsavingsummary = (RecyclerView) view.findViewById(R.id.recyclerViewmemberreportsavingsummary);
            recyclerViewmemberreportsavingDissummary = (RecyclerView) view.findViewById(R.id.recyclerViewmemberreportsavingDissummary);
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
            mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) view.findViewById(R.id.ch);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) view.findViewById(R.id.cb);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            Name = (TextView) view.findViewById(R.id.Name);
            Name.setText(MySharedPreference.readString(getActivity(), MySharedPreference.MEM_NAME_SUMMARY, ""));
         //   Name.setTypeface(LoginActivity.sTypeface);

            mHeader = (TextView) view.findViewById(R.id.fragmentHeader);
            mHeader.setText(AppStrings.savingsSummary);
            mHeader.setTypeface(LoginActivity.sTypeface);

            date = (TextView) view.findViewById(R.id.date);
            date.setText(AppStrings.date);
            date.setTypeface(LoginActivity.sTypeface);
            amount = (TextView) view.findViewById(R.id.amount);
            amount.setText(AppStrings.amount);
            amount.setTypeface(LoginActivity.sTypeface);
            totalamount = (TextView) view.findViewById(R.id.totalAmount);
            msavingdisbursement = (TextView) view.findViewById(R.id.savingdisbursement);
            Total = (TextView) view.findViewById(R.id.Total);
            mtotalAmount = (TextView) view.findViewById(R.id.totalAmount);
//            totalDisburse = (TextView) view.findViewById(R.id.totaldisamount);
            Total.setText(AppStrings.total);
            Total.setTypeface(LoginActivity.sTypeface);
            totalamount.setTypeface(LoginActivity.sTypeface);
            linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager1 = new LinearLayoutManager(getActivity());
            recyclerViewmemberreportsavingsummary.setLayoutManager(linearLayoutManager);
            recyclerViewmemberreportsavingDissummary.setLayoutManager(linearLayoutManager1);
            recyclerViewmemberreportsavingsummary.setHasFixedSize(true);
            recyclerViewmemberreportsavingDissummary.setHasFixedSize(true);
            recyclerViewmemberreportsavingsummary.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            recyclerViewmemberreportsavingDissummary.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

            RestClient.getRestClient(ReportSavingSummary.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.MEMBERREPORT_SAVINGSUMMARY + memberid, getActivity(), ServiceType.REPORT_MEMBERREPORT_SAVINGSUMMARY);
        } else {
            Utils.showToast(getActivity(), "Network Not Available");
        }
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        switch (serviceType) {
            case REPORT_MEMBERREPORT_SAVINGSUMMARY:
                if (result != null && result.length() > 0) {
                    Log.d("getDetails", " " + result.toString());
                    responseDto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    if (responseDto.getStatusCode() == (Utils.Success_Code)) {
                        Utils.showToast(getActivity(), responseDto.getMessage());
                        responseDto.getResponseContent().getMemberSavingsSummary();
                        Log.d("check", "" + responseDto.getResponseContent().getMemberSavingsSummary());

                        report__memberReport_savingSummary_adapter = new Report__MemberReport_SavingSummary_Adapter(getActivity(), responseDto.getResponseContent().getMemberSavingsSummary(), responseDto.getResponseContent().getTotalMemberSavingsSummary(), null);
                        report__memberReport_savingSummary_adapter1 = new Report__MemberReport_SavingSummary_Adapter(getActivity(), responseDto.getResponseContent().getMemberSavingsDisbursment(), null, responseDto.getResponseContent().getTotalMemberSavingsDisbursment());
                        recyclerViewmemberreportsavingsummary.setAdapter(report__memberReport_savingSummary_adapter);
                        recyclerViewmemberreportsavingDissummary.setAdapter(report__memberReport_savingSummary_adapter1);

                        if (responseDto.getResponseContent().getTotalMemberSavingsSummary() == null || responseDto.getResponseContent().getTotalMemberSavingsSummary().equals("0.0")) {
                            recyclerViewmemberreportsavingsummary.setVisibility(View.GONE);
                        } else {
                            recyclerViewmemberreportsavingsummary.setVisibility(View.VISIBLE);
                        }

                        if (responseDto.getResponseContent().getTotalMemberSavingsDisbursment() == null || responseDto.getResponseContent().getTotalMemberSavingsDisbursment().equals("0.0")) {
                            recyclerViewmemberreportsavingDissummary.setVisibility(View.GONE);
                        } else {
                            recyclerViewmemberreportsavingDissummary.setVisibility(View.VISIBLE);
                        }

                        totalamount.setText(responseDto.getResponseContent().getTotalMemberSavings());

//                        totalDisburse.setText(responseDto.getResponseContent().getTotalMemberSavingsDisbursment());

                    }else{

                        Utils.showToast(getActivity(),responseDto.getMessage());
                        msavingdisbursement.setVisibility(View.GONE);
                        Total.setVisibility(View.GONE);
                        mtotalAmount.setVisibility(View.GONE);
                        if (responseDto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                    }
                }
                break;
        }

    }
}

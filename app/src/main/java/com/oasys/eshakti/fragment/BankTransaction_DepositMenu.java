package com.oasys.eshakti.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.oasys.eshakti.Adapter.CustomListAdapter;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.Dto.ShgBankDetails;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.RecyclerViewListener;
import com.oasys.eshakti.R;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.BankTable;
import com.oasys.eshakti.database.MemberTable;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.model.ListItem;

import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BankTransaction_DepositMenu extends Fragment implements RecyclerViewListener, AdapterView.OnItemClickListener {

    public static String TAG = BankTransaction_DepositMenu.class.getSimpleName();
    private TextView mGroupName, mCashinHand, mCashatBank;
    public static String sSelectedDepositMenu = null;
    private NetworkConnection networkConnection;
    List<String> incomeMenu;/*
     * = { AppStrings.subscriptioncharges,
     * AppStrings.penalty,AppStrings.otherincome,
     * AppStrings.donation, AppStrings.mSeedFund};
     *
     */

    private ListView mListView;
    private List<ListItem> listItems;
    private CustomListAdapter mAdapter;
    int listImage;
    private TextView mHeader;
    private Dialog mProgressDilaog;
    private View rootView;
    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private ArrayList<ShgBankDetails> bankdetails;
    private String[] menuStr;

    public BankTransaction_DepositMenu() {
        // TODO Auto-generated constructor stub
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        init();

    }



    private void init() {

        try {
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);


            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);


            mHeader = (TextView) rootView.findViewById(R.id.submenuHeaderTextview);
            mHeader.setVisibility(View.VISIBLE);
            mHeader.setText("" + AppStrings.mSavings_Banktransaction);
            mHeader.setTypeface(LoginActivity.sTypeface);

            listItems = new ArrayList<ListItem>();
            mListView = (ListView) rootView.findViewById(R.id.fragment_List);
            listImage = R.drawable.ic_navigate_next_white_24dp;

            menuStr = new String[]{AppStrings.bankTransaction,
                    AppStrings.fixedDeposit, AppStrings.recurringDeposit,
                    AppStrings.mAccountToAccountTransfer};

            incomeMenu = Arrays.asList(menuStr);

            for (String it : incomeMenu) {
                ListItem rowItem = new ListItem();
                rowItem.setImageId(listImage);
                rowItem.setTitle(it);
                listItems.add(rowItem);
            }

            mAdapter = new CustomListAdapter(getActivity(), listItems);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(this);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);


       /* incomeMenu = new String[]{AppStrings.subscriptioncharges, AppStrings.penalty, AppStrings.otherincome,
                AppStrings.donation, AppStrings.mSeedFund};
*/
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        rootView = inflater.inflate(R.layout.fragment_new_menulist, container, false);
        return rootView;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
    }

    @Override
    public void recyclerViewListClicked(View view, int position) throws InterruptedIOException {
        // TODO Auto-generated method stub


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO Auto-generated method stub

        TextView textColor_Change = (TextView) view.findViewById(R.id.dynamicText);
        textColor_Change.setText(String.valueOf(incomeMenu.get(position)));
        textColor_Change.setTextColor(Color.rgb(251, 161, 108));

        sSelectedDepositMenu = incomeMenu.get(position);
        if (MySharedPreference.readInteger(getActivity(), MySharedPreference.BT_COUNT, 0) <= 0)
            MySharedPreference.writeInteger(getActivity(), MySharedPreference.BT_COUNT, 0);

        if ((position == 0)) {
            BankTransactionEntry fragment = new BankTransactionEntry();
            NewDrawerScreen.showFragment(fragment);
        } else if ((position == 1)) {
            BTFixeddepositEntry fragment = new BTFixeddepositEntry();
            NewDrawerScreen.showFragment(fragment);
        } else if (position == 2) {
//            BT_RD_Entry fragment = new BT_RD_Entry();
            Recurring_Deposit fragment =new Recurring_Deposit();
            NewDrawerScreen.showFragment(fragment);
        }else if (position == 3) {
            BTActoAcTransfer fragment = new BTActoAcTransfer();
            NewDrawerScreen.showFragment(fragment);
        }
    }

}

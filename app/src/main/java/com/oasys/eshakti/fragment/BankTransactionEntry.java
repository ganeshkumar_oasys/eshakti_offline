package com.oasys.eshakti.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Dialogue.Dialog_New_TransactionDate;
import com.oasys.eshakti.Dto.AddBTDto;
import com.oasys.eshakti.Dto.CashOfGroup;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.Dto.OfflineDto;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.Dto.ShgBankDetails;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.GetSpanText;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.BankTable;
import com.oasys.eshakti.database.MemberTable;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.database.TransactionTable;
import com.oasys.eshakti.views.Get_EdiText_Filter;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BankTransactionEntry extends Fragment implements View.OnClickListener, NewTaskListener {

    private View rootView;
    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<MemberList> arrMem;
    public static final String TAG = BankTransactionEntry.class.getSimpleName();

    private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
    private Button mRaised_Submit_Button, mEdit_RaisedButton, mOk_RaisedButton;
    public static EditText mEditAmount;

    private Dialog mProgressDilaog;
    public static String sSend_To_Server_Deposit;
    public static String[] sDepositItems;
    public static String[] sDepositAmount;
    int size;
    public static List<EditText> sEditTextFields = new ArrayList<EditText>();

    public static int sBank_deposit, sBank_withdrawl, sBank_expenses, sBank_Interest, sBank_IntrSubvention,
            cashAtWithdrawl;

    String toBeEditArr[];
    public static int sDepositTotal;
    String mDeposit_offline, mInterest_offline, mWithdrawl_offline, mBankExpenses_offline, mInterestSubvention_Offline;
    public static String mCashatBank_individual;
    String mLastTrDate = null, mLastTr_ID = null;

    Dialog confirmationDialog;
    String mOfflineBankname;
    boolean isGetTrid = false;
    boolean isServiceCall = false;
    String mSqliteDBStoredValues_BankTransactionValues = null;
    private ArrayList<ShgBankDetails> bankdetails;
    OfflineDto offlineDBData = new OfflineDto();
    private String amount = "0.0";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_deposit_entry, container, false);
        return rootView;
    }


    @Override
    public void onStart() {
        super.onStart();
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        amount = BankTransaction.sSelectedBank.getCurrentBalance();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));

        /*Log.i("bankaccount","Balance : "+bankdetails.get(0).getBankName());

        Log.i("bankaccount","Size : "+bankdetails.size());*/

        arrMem = new ArrayList<>();
        sEditTextFields.clear();

        sSend_To_Server_Deposit = "0";
        sDepositTotal = 0;

        sDepositItems = new String[]{AppStrings.mCashDeposit, // (AppStrings.bankDeposit),
                AppStrings.bankInterest, // (AppStrings.mCashDeposit),
                // //
                // (AppStrings.bankInterest),
                AppStrings.interestSubvention,
                AppStrings.withdrawl,
                AppStrings.mBankCharges};

        init();
       /* if (networkConnection.isNetworkAvailable()) {
            onTaskStarted();
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.BT_CB_FD_VALUE + BankTransaction.sSelectedBank.getShgSavingsAccountId(), getActivity(), ServiceType.FD_VALUE);

        }*/


    }

    private void init() {
        try {
            for (int i=0; i<bankdetails.size(); i++){
                if (BankTransaction.sSelectedBank.getBankName().equals(bankdetails.get(i).getBankName())){
                    Log.d("bankaccount","For Loop if condition : "+bankdetails.get(i).getBankName());
                    Log.d("bankaccount","account id : "+bankdetails.get(i).getShgSavingsAccountId());
                    /*BankTransaction.sSelectedBank.getBankId();
                    BankTransaction.sSelectedBank.getBranchId();*/
                    Log.d("bankaccount","bank id : "+BankTransaction.sSelectedBank.getBankId());
                    Log.d("bankaccount","branch id : "+BankTransaction.sSelectedBank.getBranchId());
                    Log.i("bankaccount","savings account id : "+BankTransaction.sSelectedBank.getShgSavingsAccountId());
                }
            }
            Log.d("bankaccount","bankName : "+BankTransaction.sSelectedBank.getBankName());
            Log.d("bankaccount","size : "+bankdetails.size());
            //Log.d("bankaccount","account id : "+bankdetails.get(0).getShgSavingsAccountId());
            Log.d("bankaccount","account id : "+shgDto.getShgSavingsAccountId());
            System.out.println("LENGTH " + sDepositItems.length);
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);
            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);
            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);
            mHeadertext = (TextView) rootView.findViewById(R.id.fragment_deposit_entry_headertext);
            mHeadertext.setText(AppStrings.mSavings_Banktransaction);//(AppStrings.mSavingsTransaction));// (AppStrings.bankTransaction));
            mHeadertext.setTypeface(LoginActivity.sTypeface);
            mRaised_Submit_Button = (Button) rootView.findViewById(R.id.fragment_deposit_entry_Submitbutton);
            mRaised_Submit_Button.setText(AppStrings.submit);
            mRaised_Submit_Button.setTypeface(LoginActivity.sTypeface);
            mRaised_Submit_Button.setOnClickListener(this);

            size = sDepositItems.length;
            System.out.println("Size of values:>>>" + size);

            try {
                TableLayout tableLayout = (TableLayout) rootView.findViewById(R.id.fragmentDeposit_contentTable);
                TableRow outerRow = new TableRow(getActivity());
                TableRow.LayoutParams header_ContentParams = new TableRow.LayoutParams(250, ViewGroup.LayoutParams.WRAP_CONTENT,
                        1f);
                header_ContentParams.setMargins(10, 5, 10, 5);

                TextView bankName = new TextView(getActivity());
                bankName.setText(GetSpanText.getSpanString(getActivity(),
                        String.valueOf(BankTransaction.sSelectedBank.getBankName())));
                bankName.setPadding(15, 5, 5, 5);
                bankName.setLayoutParams(header_ContentParams);
                bankName.setTextColor(R.color.black);
                outerRow.addView(bankName);

                TableRow.LayoutParams amountParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                amountParams.setMargins(5, 5, 20, 5);

                TextView bankDeposit_Amount = new TextView(getActivity());
                Log.d("bankaccount",BankTransaction.sSelectedBank.getCurrentBalance());
                bankDeposit_Amount.setText(GetSpanText.getSpanString(getActivity(),
                        BankTransaction.sSelectedBank.getCurrentBalance()));
                bankDeposit_Amount.setTextColor(R.color.black);
                bankDeposit_Amount.setGravity(Gravity.RIGHT);
                bankDeposit_Amount.setLayoutParams(amountParams);
                bankDeposit_Amount.setPadding(5, 5, 5, 5);
                outerRow.addView(bankDeposit_Amount);

                tableLayout.addView(outerRow, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                for (int i = 0; i < size; i++) {
                    TableRow innerRow = new TableRow(getActivity());

                    TextView memberName = new TextView(getActivity());
                    memberName.setText(GetSpanText.getSpanString(getActivity(), sDepositItems[i]));
                    memberName.setTypeface(LoginActivity.sTypeface);
                    memberName.setPadding(15, 0, 5, 5);
                    memberName.setTextColor(R.color.black);

                    memberName.setLayoutParams(header_ContentParams);
                    innerRow.addView(memberName);

                    mEditAmount = new EditText(getActivity());
                    sEditTextFields.add(mEditAmount);
                    mEditAmount.setId(i);
                    mEditAmount.setInputType(InputType.TYPE_CLASS_NUMBER);
                    mEditAmount.setPadding(5, 5, 5, 5);
                    mEditAmount.setFilters(Get_EdiText_Filter.editText_filter());
                    mEditAmount.setLayoutParams(amountParams);
                    mEditAmount.setBackgroundResource(R.drawable.edittext_background);
                    mEditAmount.setWidth(150);
                    mEditAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            // TODO Auto-generated method stub
                            if (hasFocus) {
                                ((EditText) v).setGravity(Gravity.LEFT);
                            } else {
                                ((EditText) v).setGravity(Gravity.RIGHT);
                            }
                        }
                    });
                    innerRow.addView(mEditAmount);

                    tableLayout.addView(innerRow,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;

        }
        switch (serviceType) {
            case BT_ENTRY:
                try {
                    if (result != null) {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            Utils.showToast(getActivity(), message);
                            insertTransaction_tdy();
                            Log.i("print","shgDto.getFFlag() : "+shgDto.getFFlag());
                            if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                                Log.d("bankaccount","if block");
                            }

                            updateBKCB();
                            updateCIH();

                            FragmentManager fm = getFragmentManager();
                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            NewDrawerScreen.showFragment(new MainFragment());

                        } else {
                            Utils.showToast(getActivity(), message);
                          //  insertBT();
                        }
                    } else {
                        insertBT();
                    }
                } catch (Exception e) {

                }
                break;
            case FD_VALUE:

                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                    } else {
                        Utils.showToast(getActivity(), message);

                    }
                } catch (Exception e) {

                }
                break;

        }


    }

    private void insertTransaction_tdy()
    {

        Log.d("bankaccount","insertTransaction_tdy Function");
        offlineDBData.setIs_transaction_tdy("1.0");
        offlineDBData.setShgId(shgDto.getShgId());
        SHGTable.updateIstransaction(offlineDBData);


    }

    private void insertBT() {
        try {

            int value = (MySharedPreference.readInteger(getActivity(), MySharedPreference.BT_COUNT, 0) + 1);
            if (value > 0)
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.BT_COUNT, value);

            offlineDBData.setBtCount(value + "");
            TransactionTable.insertTransBTData(offlineDBData);

            if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
            }

            updateBKCB();
            updateCIH();

            FragmentManager fm = getFragmentManager();
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            NewDrawerScreen.showFragment(new MainFragment());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateCIH() {
        String cihstr = "", cabstr = "", lastTranstr = "";
        cihstr = offlineDBData.getCashInhand();
        cabstr = offlineDBData.getCashAtBank();
        lastTranstr = offlineDBData.getLastTransactionDateTime();
        CashOfGroup csg = new CashOfGroup();
        csg.setCashInHand(cihstr);
        csg.setCashAtBank(cabstr);
        csg.setLastTransactionDate(lastTranstr);
        SHGTable.updateSHGDetails(csg, shgDto.getId());
    }

    private void updateBKCB() {
        Log.i("print","offlineDBData.getCurr_balance() : "+offlineDBData.getCurr_balance());
        if (offlineDBData.getCurr_balance() != null || offlineDBData.getCurr_balance().length() > 0) {
            /*for (int i=0; i<bankdetails.size(); i++){
                if (BankTransaction.sSelectedBank.getBankName().equals(bankdetails.get(i).getBankName())){
                    Log.d("bankaccount","For Loop if condition : "+bankdetails.get(i).getBankName());
                    offlineDBData.setShgSavingsAccountId(bankdetails.get(i).getShgSavingsAccountId());
                }
            }*/
            offlineDBData.setShgSavingsAccountId(BankTransaction.sSelectedBank.getShgSavingsAccountId());
            //offlineDBData.setShgSavingsAccountId(bankdetails.get(0).getShgSavingsAccountId());
            SHGTable.updateBankCurrentBalanceDetails(offlineDBData);
            Log.d("bankaccount","Balance : "+offlineDBData.getCurr_balance());
            Log.d("bankaccount","account id : "+shgDto.getShgSavingsAccountId());
            Log.d("bankaccount","updateBKCB");
        }
    }


    @Override
    public void onClick(View view) {
        sDepositAmount = new String[sEditTextFields.size()];
        switch (view.getId()) {
            case R.id.fragment_deposit_entry_Submitbutton:

                sDepositTotal = Integer.valueOf("0");
                sSend_To_Server_Deposit = "0";

                try {
                    OfflineDto offline = new OfflineDto();
                    for (int i = 0; i < sDepositAmount.length; i++) {

                        sDepositAmount[i] = String.valueOf(sEditTextFields.get(i).getText());

                        if ((sDepositAmount[i].equals("")) || (sDepositAmount[i] == null)) {
                            sDepositAmount[i] = "0";
                        }

                        if (sDepositAmount[i].matches("\\d*\\.?\\d+")) { // match a
                            // decimal
                            // number

                            int amount = (int) Math.round(Double.parseDouble(sDepositAmount[i]));
                            sDepositAmount[i] = String.valueOf(amount);
                        }

                        sDepositTotal = sDepositTotal + Integer.parseInt(sDepositAmount[i]);

                        sSend_To_Server_Deposit = sSend_To_Server_Deposit + sDepositAmount[i] + "~";

                    }


                    sBank_deposit = Integer.parseInt(sDepositAmount[0]);
                    sBank_Interest = Integer.parseInt(sDepositAmount[1]);
                    sBank_withdrawl = Integer.parseInt(sDepositAmount[3]);
                    sBank_expenses = Integer.parseInt(sDepositAmount[4]);
                    sBank_IntrSubvention = Integer.parseInt(sDepositAmount[2]);
                    int total_val = sBank_deposit+sBank_Interest+sBank_withdrawl+sBank_expenses+sBank_IntrSubvention;

                    sSend_To_Server_Deposit = sBank_deposit + "~" + sBank_Interest + "~" + sBank_withdrawl + "~"
                            + sBank_expenses + "~" + sBank_IntrSubvention + "~";



               /* double mValues = Double.parseDouble(Transaction_BankDepositFragment.sSelected_BankDepositAmount);
                double mRoundedValues = Math.round(mValues);
                Transaction_BankDepositFragment.sSelected_BankDepositAmount = String.valueOf(mRoundedValues);

                Double mDoubleValues = Double.parseDouble(Transaction_BankDepositFragment.sSelected_BankDepositAmount);
                String mFinalDepositAmount = mDoubleValues.longValue() == mDoubleValues ? "" + mDoubleValues.longValue()
                        : "" + mDoubleValues;

                Transaction_BankDepositFragment.sSelected_BankDepositAmount = mFinalDepositAmount;
*/
                    cashAtWithdrawl = ((int) Double.parseDouble(amount.trim()))
                            + Integer.parseInt(sDepositAmount[0]) + Integer.parseInt(sDepositAmount[1])
                            + Integer.parseInt(sDepositAmount[2]) - Integer.parseInt(sDepositAmount[4]);


                    //  if (sDepositTotal != 0 && sBank_withdrawl <= cashAtWithdrawl) {
                    if (total_val!=0) {
                        if (sBank_withdrawl <= cashAtWithdrawl && sBank_expenses <= ((int) Double.parseDouble(amount.trim()))) {

                            try {
                                offline.setAnimatorId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                                offline.setShgId(shgDto.getShgId());
//                            offline.setLastTransactionDateTime(shgDto.getLastTransactionDate());
                                offline.setLastTransactionDateTime(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                                offline.setModifiedDateTime(System.currentTimeMillis() + "");
                                offline.setModeOCash("2");
                                offline.setTxType(NewDrawerScreen.BANK_TRANSACTION);
                                offline.setTxSubtype(AppStrings.bankTransaction);
                                offline.setBtFromSavingAcId(BankTransaction.sSelectedBank.getShgSavingsAccountId());
                                offline.setBtDeposit(sBank_deposit + "");
                                offline.setBtWithdraw(sBank_withdrawl + "");
                                offline.setBtCharge(sBank_expenses + "");
                                offline.setBtInterest(sBank_Interest + "");
                                offline.setBt_intSubventionRecieved(sBank_IntrSubvention + "");
                                //offline fundflow
                                int cih = 0, cab = 0, cb = 0;
//                            cab = (((int) Double.parseDouble(shgDto.getCashAtBank())) + sBank_deposit + sBank_Interest + sBank_IntrSubvention) - sBank_withdrawl;
                                cab = (((int) Double.parseDouble(shgDto.getCashAtBank())) + sBank_deposit + sBank_Interest + sBank_IntrSubvention) - (sBank_withdrawl + sBank_expenses);
//                            cih = (((int) Double.parseDouble(shgDto.getCashInHand())) + sBank_withdrawl) - (sBank_deposit + sBank_expenses);
                                cih = (((int) Double.parseDouble(shgDto.getCashInHand())) + sBank_withdrawl) - sBank_deposit;
                                cb = (((int) Double.parseDouble(BankTransaction.sSelectedBank.getCurrentBalance())) + sBank_deposit + sBank_Interest + sBank_IntrSubvention) - (sBank_withdrawl + sBank_expenses);
                                offline.setCashAtBank(cab + "");
                                offline.setCashInhand(cih + "");
                                offline.setCurr_balance(cb + "");
                                offline.setAccountNumber(BankTransaction.sSelectedBank.getShgSavingsAccountId());
                                offline.setIs_transaction_tdy("1.0");
                                offlineDBData = offline;
                                //   offline.add(offline);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            mDeposit_offline = sDepositAmount[0];
                            mInterest_offline = sDepositAmount[1];
                            mWithdrawl_offline = sDepositAmount[3];
                            mBankExpenses_offline = sDepositAmount[4];
                            mInterestSubvention_Offline = sDepositAmount[2];

                            confirmationDialog = new Dialog(getActivity());

                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
                            dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT));

                            TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                            confirmationHeader.setText(AppStrings.confirmation);
                            confirmationHeader.setTypeface(LoginActivity.sTypeface);
                            TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                            DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
                            Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
                            String dateStr = simple.format(d);
                            TextView transactdate = (TextView)dialogView.findViewById(R.id.transactdate);
                            transactdate.setText(dateStr);

                            for (int i = 0; i < size; i++) {

                                TableRow indv_DepositEntryRow = new TableRow(getActivity());

                                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                                contentParams.setMargins(10, 5, 10, 5);

                                TextView memberName_Text = new TextView(getActivity());
                                memberName_Text
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sDepositItems[i])));
                                memberName_Text.setTextColor(R.color.black);
                                memberName_Text.setPadding(5, 5, 5, 5);
                                memberName_Text.setLayoutParams(contentParams);
                                indv_DepositEntryRow.addView(memberName_Text);

                                TextView confirm_values = new TextView(getActivity());
                                confirm_values
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sDepositAmount[i])));
                                confirm_values.setTextColor(R.color.black);
                                confirm_values.setPadding(5, 5, 5, 5);
                                confirm_values.setGravity(Gravity.RIGHT);
                                confirm_values.setLayoutParams(contentParams);
                                indv_DepositEntryRow.addView(confirm_values);

                                confirmationTable.addView(indv_DepositEntryRow,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            }
                            mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
                            mEdit_RaisedButton.setText(AppStrings.edit);
                            mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                            mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                            // 205,
                            // 0));
                            mEdit_RaisedButton.setOnClickListener(this);

                            mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
                            mOk_RaisedButton.setText(AppStrings.yes);
                            mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                            mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                            mOk_RaisedButton.setOnClickListener(this);

                            confirmationDialog.getWindow()
                                    .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            confirmationDialog.setCanceledOnTouchOutside(false);
                            confirmationDialog.setContentView(dialogView);
                            confirmationDialog.setCancelable(true);
                            confirmationDialog.show();

                            ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                            margin.leftMargin = 10;
                            margin.rightMargin = 10;
                            margin.topMargin = 10;
                            margin.bottomMargin = 10;
                            margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

                        } else if (sBank_deposit > ((int) Double.parseDouble(amount.trim()))) {

                            TastyToast.makeText(getActivity(), AppStrings.cashinHandAlert, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);

                            sSend_To_Server_Deposit = "0";
                            sDepositTotal = Integer.valueOf("0");

                        } else if (sBank_withdrawl > cashAtWithdrawl || sBank_expenses > ((int) Double.parseDouble(amount.trim()))) {

                            TastyToast.makeText(getActivity(), AppStrings.cashatBankAlert, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);

                            sSend_To_Server_Deposit = "0";
                            sDepositTotal = Integer.valueOf("0");

                        } else {

                            TastyToast.makeText(getActivity(), AppStrings.DepositnullAlert, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);

                            sSend_To_Server_Deposit = "0";
                            sDepositTotal = Integer.valueOf("0");

                        }
                    }
                    else
                    {
                        TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case R.id.fragment_Edit_button:
                sSend_To_Server_Deposit = "0";
                sDepositTotal = Integer.valueOf("0");
                mRaised_Submit_Button.setClickable(true);
                // alertDialog.dismiss();
                confirmationDialog.dismiss();
                break;
            case R.id.fragment_Ok_button:
                confirmationDialog.dismiss();
                if (networkConnection.isNetworkAvailable()) {
                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 1) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }
                    // NOTHING TO DO::
                } else {

                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 2) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }

                }
                AddBTDto bt = new AddBTDto();
                bt.setBankId(BankTransaction.sSelectedBank.getShgSavingsAccountId());
                bt.setBankInterest(sBank_Interest + "");
                bt.setBankCharges(sBank_expenses + "");
                bt.setShgId(shgDto.getShgId());
                bt.setWithdrawal(sBank_withdrawl + "");
                bt.setCashDeposit(sBank_deposit + "");
                bt.setInterestSubventionReceived(sBank_IntrSubvention + "");
//                bt.setTransactionDate(shgDto.getLastTransactionDate());
                bt.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                bt.setMobileDate(System.currentTimeMillis() + "");
                String sreqString = new Gson().toJson(bt);
                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();
                    RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.BT_ENTRY, sreqString, getActivity(), ServiceType.BT_ENTRY);
                } else {
                    if (TransactionTable.getLoginFlag(AppStrings.bankTransaction).size() <= 0 || (!TransactionTable.getLoginFlag(AppStrings.bankTransaction).get(TransactionTable.getLoginFlag(AppStrings.bankTransaction).size() - 1).getLoginFlag().equals("1")))
                        insertBT();
                    else
                        TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                }
                break;
        }
    }
}

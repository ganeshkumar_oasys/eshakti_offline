package com.oasys.eshakti.fragment;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.oasys.eshakti.Dialogue.Dialog_New_TransactionDate;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.OfflineDto;
import com.oasys.eshakti.Dto.RequestDto.TrainingListId;
import com.oasys.eshakti.Dto.RequestDto.TrainingRequestDto;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.Dto.TableData;
import com.oasys.eshakti.Dto.TrainingsList;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.database.TransactionTable;
import com.tutorialsee.lib.TastyToast;





import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class Meetings_training extends Fragment implements View.OnClickListener, NewTaskListener, DatePickerDialog.OnDateSetListener {

    View view;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank,training_headertext,trainingdate;
    private LinearLayout mLayout;
    private Button mRaised_Submit_Button, mEdit_RaisedButton, mOk_RaisedButton;
    public static TextView mTrainingDate_editText;
    ResponseDto traininglistdto;
    ArrayList<TrainingsList> trslist;
    public static CheckBox sCheckBox[];
    private String shgformationMillis;
    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    private Date lastTransaDate, systemDate, openingDate;
    String trainingEditTextValue;
    Dialog confirmationDialog;
    public static String checkedTrainingType[], checkedTrainingTypeid[];
    public static String trainingDate = "", trainingType = "", trainingTypeId = "";
    private ArrayList<TrainingListId> arrtraininglistType;
    TrainingRequestDto trainingRequestDto = new TrainingRequestDto();

    private ArrayList<TableData> tableData;
    String dateStr;
    public static ArrayList<OfflineDto> offlineDBData = new ArrayList<>();

    public Meetings_training() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_meetings_training, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        tableData = SHGTable.getShgTrainingData();

        init();
        return view;
    }

    public void init() {
        try {
            checkedTrainingType = new String[]{""};
            mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName()+" / "+shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) view.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) view.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            training_headertext = (TextView) view.findViewById(R.id.training_headertext);
            training_headertext.setText(AppStrings.training);

            trainingdate = (TextView) view.findViewById(R.id.trainingdate);
            trainingdate.setText(AppStrings.training_Date);
            mLayout = (LinearLayout) view.findViewById(R.id.training_linear1);

            mRaised_Submit_Button = (Button) view.findViewById(R.id.training_Submitbutton);
            mRaised_Submit_Button.setText(AppStrings.submit);
            mRaised_Submit_Button.setTypeface(LoginActivity.sTypeface);
            mRaised_Submit_Button.setOnClickListener(this);

            mTrainingDate_editText = (TextView) view.findViewById(R.id.training_editText);
            //  mTrainingDate_editText.setText(Meeting_TrainingFragment.trainingDate);
            mTrainingDate_editText.setGravity(Gravity.CENTER);
            mTrainingDate_editText.setOnClickListener(this);

if(tableData!=null) {
    sCheckBox = new CheckBox[tableData.size()];
    for (int i = 0; i < tableData.size(); i++) {

        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        sCheckBox[i] = new CheckBox(getActivity());
        sCheckBox[i].setChecked(false);
        sCheckBox[i].setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
        linearLayout.addView(sCheckBox[i]);

        ViewGroup.LayoutParams lParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView trainingTypes = new TextView(getActivity());
        trainingTypes.setTypeface(LoginActivity.sTypeface);
        trainingTypes.setText(tableData.get(i).getName());
        trainingTypes.setPadding(0, 5, 5, 5);
        trainingTypes.setLayoutParams(lParams);
        linearLayout.addView(trainingTypes);

        mLayout.addView(linearLayout);
    }
}
        } catch (Exception e) {

        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        if (networkConnection.isNetworkAvailable()) {
////            String url = Constants.BASE_URL + Constants.GETTRAININGLIST;
//            String url = Constants.BASE_URL + Constants.GETTRAININGLIST+MySharedPreference.readString(getActivity(), MySharedPreference.LANG_ID,"");
//
//            RestClient.getRestClient(this).callWebServiceForGetMethod(url, getActivity(), ServiceType.GETTRAININGLIST);
//        }


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.training_editText) {
            try {
                if (shgDto != null)
                    shgformationMillis = shgDto.getGroupFormationDate();
                Date opDate = new Date(Long.parseLong(shgformationMillis));
                String formattedDate1 = df.format(opDate.getTime());
                openingDate = df.parse(formattedDate1);

                Calendar calender = Calendar.getInstance();

                String formattedDate = df.format(calender.getTime());
                Log.e("Device Date  =  ", formattedDate + "");

                String mBalanceSheetDate = shgDto.getLastTransactionDate(); // dd/MM/yyyy
                // String mBalanceSheetDate = "1512844200000";
                Date d = new Date(Long.parseLong(mBalanceSheetDate));
                String dateStr = df.format(d);
                String formatted_balancesheetDate = dateStr;
                Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

                lastTransaDate = df.parse(formatted_balancesheetDate);
                systemDate = df.parse(formattedDate);


                if (openingDate.compareTo(lastTransaDate) < 0) {
                    if (lastTransaDate.compareTo(systemDate) < 0 || lastTransaDate.compareTo(systemDate) == 0) {

                        calendarDialogShow();

                    } else {
                        TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY",
                                TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                    }
                } else {
                    Toast.makeText(getActivity(), "Check Group opening date", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else if (view.getId() == R.id.training_Submitbutton) {
            trainingType = "";
            trainingTypeId = "";
            arrtraininglistType = new ArrayList<>();


            trainingEditTextValue = mTrainingDate_editText.getText().toString();
            if (trainingEditTextValue.equals("")) {
                trainingEditTextValue = null;
            }
            for (int i = 0; i < tableData.size(); i++) {
                if (sCheckBox[i].isChecked()) {
                    trainingType = trainingType + tableData.get(i).getName() + "~";
//                    trainingTypeId = trainingTypeId + trslist.get(i).getId() + "~";
                    trainingTypeId = trainingTypeId + tableData.get(i).getUuid() + "~";
                }
            }
            if (trainingEditTextValue != null && (!trainingType.equals(""))) {

                confirmationDialog = new Dialog(getActivity());

                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
                dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                confirmationHeader.setText(AppStrings.confirmation);
                confirmationHeader.setTypeface(LoginActivity.sTypeface);

                TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
                Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
                String dateStr = simple.format(d);
                TextView transactdate = (TextView)dialogView.findViewById(R.id.transactdate);
                transactdate.setText(dateStr);

                TableRow trainingRow = new TableRow(getActivity());

                TableRow.LayoutParams contentTrainingParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentTrainingParams.setMargins(10, 5, 10, 5);

                TextView trainingDateText = new TextView(getActivity());
                trainingDateText.setText(AppStrings.training_Date);
                trainingDateText.setTypeface(LoginActivity.sTypeface);
                trainingDateText.setTextColor(R.color.black);
                trainingDateText.setLayoutParams(contentTrainingParams);
                trainingRow.addView(trainingDateText);

                TextView trainingDate = new TextView(getActivity());
                trainingDate.setText(mTrainingDate_editText.getText());
                trainingDate.setTextColor(R.color.black);
                trainingDate.setPadding(10, 5, 5, 5);
                trainingDate.setLayoutParams(contentTrainingParams);
                trainingRow.addView(trainingDate);

                confirmationTable.addView(trainingRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                try {
                    checkedTrainingType = trainingType.split("~");
                    checkedTrainingTypeid = trainingTypeId.split("~");
                    CheckBox checkBox[] = new CheckBox[checkedTrainingType.length];
                    for (int j = 0; j < checkedTrainingType.length; j++) {
                        TableRow indv_memberNameRow = new TableRow(getActivity());

                        TableRow.LayoutParams contentParams = new TableRow.LayoutParams(0,
                                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                        contentParams.setMargins(10, 5, 10, 5);

                        checkBox[j] = new CheckBox(getActivity());
                        checkBox[j].setText(String.valueOf(checkedTrainingType[j]));
                        checkBox[j].setChecked(true);
                        checkBox[j].setClickable(false);
                        checkBox[j].setTextColor(R.color.black);
                        indv_memberNameRow.addView(checkBox[j]);
                        TrainingListId triddto = new TrainingListId();
                        triddto.setId(String.valueOf(checkedTrainingTypeid[j]));

                        arrtraininglistType.add(triddto);
                        confirmationTable.addView(indv_memberNameRow,new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        OfflineDto offline = new OfflineDto();
                        offline.setId(String.valueOf(checkedTrainingTypeid[j]));
                        offline.setTrainingDate(mTrainingDate_editText.getText().toString());
                        offline.setIs_transaction_tdy("1.0");
                        offline.setLastTransactionDateTime(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                        offlineDBData.add(offline);
                        Log.d("offdata",""+offlineDBData);
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    System.out.println("checkbox layout error:" + e.toString());
                }
                mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
                mEdit_RaisedButton.setText(AppStrings.edit);
                mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                // 205,
                // 0));
                mEdit_RaisedButton.setOnClickListener(this);

                mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
                mOk_RaisedButton.setText(AppStrings.yes);
                mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                mOk_RaisedButton.setOnClickListener(this);

                confirmationDialog.getWindow()
                        .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                confirmationDialog.setCanceledOnTouchOutside(false);
                confirmationDialog.setContentView(dialogView);
                confirmationDialog.setCancelable(true);
                confirmationDialog.show();

                ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                margin.leftMargin = 10;
                margin.rightMargin = 10;
                margin.topMargin = 10;
                margin.bottomMargin = 10;
                margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
            } else {
                if (trainingEditTextValue == null) {

                    TastyToast.makeText(getActivity(), AppStrings.trainingDateAlert, TastyToast.LENGTH_SHORT,
                            TastyToast.WARNING);

                    // trainingType = "";

                } else if (trainingType != null) {

                    TastyToast.makeText(getActivity(), AppStrings.trainingTypeAlert, TastyToast.LENGTH_SHORT,
                            TastyToast.WARNING);
                }
            }

        } else if (view.getId() == R.id.fragment_Edit_button) {
            trainingType = "";
            confirmationDialog.dismiss();

        } else if (view.getId() == R.id.fragment_Ok_button) {
            String shgId = shgDto.getShgId();
            DateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
            Date d = new Date(mTrainingDate_editText.getText().toString());
            dateStr = simple.format(d);
            trainingRequestDto.setShgId(shgId);
            trainingRequestDto.setTrainingDate(dateStr);
            trainingRequestDto.setTrainingListId(arrtraininglistType);
            String sreqString = new Gson().toJson(trainingRequestDto);

            if (networkConnection.isNetworkAvailable()) {

                RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.SHGTRAININGPOST, sreqString, getActivity(), ServiceType.SHGTRAININGPOST);
            }
            else
            {
                if (TransactionTable.getLoginFlag(AppStrings.training).size() <= 0 || (!TransactionTable.getLoginFlag(AppStrings.training).get(TransactionTable.getLoginFlag(AppStrings.training).size() - 1).getLoginFlag().equals("1"))) {
                    insertTraining();
                    confirmationDialog.dismiss();
                    TastyToast.makeText(getActivity(), "Transaction Completed", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                    FragmentManager fm = getFragmentManager();
                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    NewDrawerScreen.showFragment(new MainFragment());
                }
                else
                {
                    TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                            TastyToast.WARNING);
                }
//                insertTransaction_tdy();
//                okTraining();
            }

        }
    }

    private void insertTraining() {

            int value = (MySharedPreference.readInteger(getActivity(), MySharedPreference.TRAINING_COUNT, 0) + 1);
            if (value > 0)
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.TRAINING_COUNT, value);

            for (OfflineDto ofdto : offlineDBData) {
            ofdto.setTrainingcount(value + "");
            TransactionTable.insertTrainingData(ofdto);
        }
    }



  /*  private void okTraining()
    {
        String cabstr = "",lastTranstr = "",cihstr="";
        cabstr = shgDto.getCashAtBank();
        cihstr = shgDto.getCashInHand();
        lastTranstr = offline.getLastTransactionDateTime();
        CashOfGroup csg = new CashOfGroup();
        csg.setCashInHand(cihstr);
        csg.setCashAtBank(cabstr);
        csg.setLastTransactionDate(lastTranstr);
        SHGTable.updateSHGDetails(csg, shgDto.getId());

        FragmentManager fm = getFragmentManager();
        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        NewDrawerScreen.showFragment(new MainFragment());

    }*/
   /* private void insertTransaction_tdy()
    {
            offline.setIs_transaction_tdy("1.0");
            offline.setShgId(shgDto.getShgId());
            SHGTable.updateIstransaction(offline);




    }*/
    @Override
    public void onTaskStarted() {

    }

    private void calendarDialogShow() {

        Calendar now = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), this, now.get(Calendar.YEAR), now.get(Calendar.DATE), now.get(Calendar.DAY_OF_MONTH));
        Calendar min_Cal = Calendar.getInstance();
        Calendar lastDate = Calendar.getInstance();

        Calendar fcal = Calendar.getInstance();
        fcal.setTimeInMillis(openingDate.getTime());
        int fyear = fcal.get(Calendar.YEAR); // this is deprecated
        int fmonth = fcal.get(Calendar.MONTH); // this is deprecated
        int fday = fcal.get(Calendar.DATE);

        Calendar lcal = Calendar.getInstance();
        lcal.setTimeInMillis(lastTransaDate.getTime());
        int lyear = lcal.get(Calendar.YEAR); // this is deprecated
        int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
        int lday = lcal.get(Calendar.DATE);

        Calendar ccal1 = Calendar.getInstance();
        ccal1.setTimeInMillis(systemDate.getTime());
        int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
        int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
        int cday = ccal1.get(Calendar.DATE);

        if (fday == lday && fmonth == lmonth && fyear == lyear) {
            min_Cal.set(lyear, lmonth, lday + 1);
            lastDate.set(cyear, cmonth, cday);
        } else if (fyear <= lyear) {

            if (cmonth == lmonth && cyear == lyear) {
                min_Cal.set(lyear, lmonth, lday);
                lastDate.set(cyear, cmonth, cday);

            } else if (cmonth <= lmonth && cyear > lyear) {
                min_Cal.set(lyear, lmonth, lday);
                Calendar c = Calendar.getInstance();
                c.setTime(lastTransaDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(cyear, cmonth, cday);
                // lastDate.set(cyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));

            } else if (cmonth > lmonth && cyear == lyear) {
                min_Cal.set(lyear, lmonth, lday);
                if ((cmonth - lmonth) == 1) {
                    lastDate.set(cyear, cmonth, cday);
                } else if ((cmonth - lmonth) > 1) {
                    Calendar c = Calendar.getInstance();
                    c.setTime(lastTransaDate);
                    c.add(Calendar.MONTH, 1);
                    c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
//                    lastDate.set(cyear, cmonth, cday);
//                    lastDate.set(cyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
                    lastDate.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                }


            } else if (cmonth > lmonth && cyear > lyear) {
                min_Cal.set(lyear, lmonth, lday);
                Calendar c = Calendar.getInstance();
                c.setTime(lastTransaDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(cyear, cmonth, cday);
                //lastDate.set(lyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
            }

        }


        datePickerDialog.getDatePicker().setMinDate(min_Cal.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(lastDate.getTimeInMillis());

        datePickerDialog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        switch (serviceType) {
            /*case GETTRAININGLIST:
                traininglistdto = new Gson().fromJson(result, ResponseDto.class);
                if (result != null) {
                    if (traininglistdto.getStatusCode() == Utils.Success_Code) {
                        trslist = traininglistdto.getResponseContent().getTrainingsList();
                        init();
                    }else{
                        if (traininglistdto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                    }
                }
                break;*/

            case SHGTRAININGPOST:
                traininglistdto = new Gson().fromJson(result, ResponseDto.class);
                if (result != null) {
                    if (traininglistdto.getStatusCode() == Utils.Success_Code) {
                        if (confirmationDialog.isShowing()) {
                            confirmationDialog.dismiss();
                        }
//                        insertTransaction_tdy();
                        Utils.showToast(getActivity(), traininglistdto.getMessage());
                        if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                            SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                        }
                        FragmentManager fm = getFragmentManager();
                        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        NewDrawerScreen.showFragment(new MainFragment());
                    }else {
                        if (traininglistdto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        confirmationDialog.dismiss();
                        Utils.showToast(getActivity(), traininglistdto.getMessage());
                    }
                }
                break;
        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = dayOfMonth + "-" + month + "-" + year;
        try {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(0);
            cal.set(year, month, dayOfMonth, 0, 0, 0);
            Date chosenDate = cal.getTime();
            DateFormat df_medium_uk = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.UK);
            String df_medium_uk_str = df_medium_uk.format(chosenDate);
            mTrainingDate_editText.setText(df_medium_uk_str);
            // Display the formatted date
            // tv.setText(tv.getText() + df_medium_uk_str + " (DateFormat.MEDIUM, Locale.UK)\n");
            lastTransaDate = df.parse(date);

            // MySharedPreference.writeString(getActivity(), MySharedPreference.LAST_TRANSACTION, lastTransaDate.getTime() + "");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}

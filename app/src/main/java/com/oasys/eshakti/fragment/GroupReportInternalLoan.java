package com.oasys.eshakti.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Adapter.GroupReportInternalLoanAdapter;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberInternalLoan;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.database.SHGTable;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupReportInternalLoan extends Fragment implements NewTaskListener {

    private GroupReportInternalLoanAdapter groupReportInternalLoanAdapter;
    private RecyclerView MemberReportloansummary_ExpandableListView;
    private LinearLayoutManager linearLayoutManager;
    ResponseDto responseDto;
    ArrayList<MemberInternalLoan> memberInternalLoans;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView mHeader;
    private TextView Name,date,amt,inrt;
    TextView text_outstanding, os, groupreportloan_total, total;
    private ListOfShg shgDto;
    private String mem_id;


    public GroupReportInternalLoan() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_group_report_internal_loan, container, false);
        Bundle bundle = getArguments();
        mem_id = bundle.getString("memid");
        init(rootview);

        return rootview;
    }

    private void init(View view) {
        try {
            MemberReportloansummary_ExpandableListView = (RecyclerView) view.findViewById(R.id.memberloansummary_expandablelistview);
            text_outstanding = (TextView) view.findViewById(R.id.groupreportloan_outstanding);
            linearLayoutManager = new LinearLayoutManager(getActivity());
            MemberReportloansummary_ExpandableListView.setLayoutManager(linearLayoutManager);
            MemberReportloansummary_ExpandableListView.setHasFixedSize(true);
            MemberReportloansummary_ExpandableListView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));


            mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) view.findViewById(R.id.cashinhand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) view.findViewById(R.id.cashatbank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            Name = (TextView) view.findViewById(R.id.Name);
            Name.setText(MySharedPreference.readString(getActivity(), MySharedPreference.MEM_NAME_SUMMARY, ""));
            Name.setTypeface(LoginActivity.sTypeface);

            mHeader = (TextView) view.findViewById(R.id.fragmentHeader);
            mHeader.setText(AppStrings.InternalLoan);
            mHeader.setTypeface(LoginActivity.sTypeface);
            text_outstanding.setTypeface(LoginActivity.sTypeface);

            date = (TextView) view.findViewById(R.id.date);
            date.setText(AppStrings.date);
            date.setTypeface(LoginActivity.sTypeface);
            amt = (TextView) view.findViewById(R.id.amt);
            amt.setText(AppStrings.amount);
            amt.setTypeface(LoginActivity.sTypeface);
            inrt = (TextView) view.findViewById(R.id.inrt);
            inrt.setText(AppStrings.interest);
            inrt.setTypeface(LoginActivity.sTypeface);

            os = (TextView) view.findViewById(R.id.os);
            os.setTypeface(LoginActivity.sTypeface);
            os.setText(AppStrings.outstanding + " :");
            groupreportloan_total = (TextView) view.findViewById(R.id.groupreportloan_total);
            groupreportloan_total.setTypeface(LoginActivity.sTypeface);
            total = (TextView) view.findViewById(R.id.total);
            total.setTypeface(LoginActivity.sTypeface);
            total.setText(AppStrings.total + " :");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
            RestClient.getRestClient(GroupReportInternalLoan.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.MEM_LOAN_SUMMARY + mem_id, getActivity(), ServiceType.GROUP_SUMMARY_LOAN);
        } else {
            Utils.showToast(getActivity(), "Network Not Available");
        }
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        switch (serviceType) {
            case GROUP_SUMMARY_LOAN:

                if (result != null && result.length() > 0) {
                    Log.d("getDetails", " " + result.toString());
                    responseDto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    if (responseDto.getStatusCode() == (Utils.Success_Code)) {
                        Utils.showToast(getActivity(), responseDto.getMessage());
                        responseDto.getResponseContent().getMemberInternalLoan();
                        Log.d("check", "" + responseDto.getResponseContent().getMemberInternalLoan());

                        groupReportInternalLoanAdapter = new GroupReportInternalLoanAdapter(getActivity(), responseDto.getResponseContent().getMemberInternalLoan());
                        MemberReportloansummary_ExpandableListView.setAdapter(groupReportInternalLoanAdapter);
                        text_outstanding.setText(" ₹" + responseDto.getResponseContent().getLoanOutstanding());
                        groupreportloan_total.setText(" ₹" + responseDto.getResponseContent().getTotal());
                    } else {
                        if (responseDto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                    }
                }
                break;
        }

    }
}

package com.oasys.eshakti.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Dialogue.Dialog_New_TransactionDate;
import com.oasys.eshakti.Dto.CashOfGroup;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.Dto.OfflineDto;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.Dto.SavingRequest;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.GetSpanText;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.MemberTable;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.database.TransactionTable;
import com.oasys.eshakti.views.Get_EdiText_Filter;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Donation_Income extends Fragment implements View.OnClickListener, NewTaskListener {
    private View rootView;
    private TextView mGroupName, mCashinHand, mCashatBank, mHeader;
    private Button mSubmit_Raised_Button, mEdit_RaisedButton, mOk_RaisedButton;
    EditText amountEdit;
    public static String sSend_To_Server_Donation_FIncome;
    private Dialog mProgressDilaog;

    Dialog confirmationDialog;

    String mLastTrDate = null, mLastTr_ID = null;
    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<MemberList> arrMem;
    OfflineDto offlineDBData = new OfflineDto();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_doantion_fincome, container, false);
        return rootView;

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        arrMem = new ArrayList<>();
        init();
    }


    private void init() {
        try {
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);
            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);
            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);
            mHeader = (TextView) rootView.findViewById(R.id.fragment_header);
//            mHeader.setText(Income.sSelectedIncomeMenu.getName());
            mHeader.setText(AppStrings.mDonation);
            mHeader.setTypeface(LoginActivity.sTypeface);
            TextView labelText = (TextView) rootView.findViewById(R.id.fragment_label_text);
//            labelText.setText(Income.sSelectedIncomeMenu.getName());
            labelText.setText(AppStrings.mDonation);
            labelText.setTextColor(R.color.black);

            amountEdit = (EditText) rootView.findViewById(R.id.fragment_amount_editText);
            amountEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
            amountEdit.setPadding(5, 5, 5, 5);
            amountEdit.setWidth(150);
            amountEdit.setHeight(60);
            amountEdit.setFilters(Get_EdiText_Filter.editText_filter());
            amountEdit.setGravity(Gravity.RIGHT);

            mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Submit);
            mSubmit_Raised_Button.setText(AppStrings.submit);
            mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
            mSubmit_Raised_Button.setOnClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fragment_Submit:
                sSend_To_Server_Donation_FIncome = String.valueOf(amountEdit.getText());
                if ((sSend_To_Server_Donation_FIncome.equals("")) || (sSend_To_Server_Donation_FIncome == null)) {
                    sSend_To_Server_Donation_FIncome = "0";
                }
                if ((!sSend_To_Server_Donation_FIncome.equals("0"))) {

                    confirmationDialog = new Dialog(getActivity());

                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                    dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));

                    TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                    confirmationHeader.setText(AppStrings.confirmation);
                    confirmationHeader.setTypeface(LoginActivity.sTypeface);

                    TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                    DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
                    Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
                    String dateStr = simple.format(d);
                    TextView transactdate = (TextView)dialogView.findViewById(R.id.transactdate);
                    transactdate.setText(dateStr);
                    TableRow indv_SavingsRow = new TableRow(getActivity());

                    TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    contentParams.setMargins(10, 5, 10, 5);

                    TextView lableItem = new TextView(getActivity());
//                    lableItem.setText(GetSpanText.getSpanString(getActivity(), Income.sSelectedIncomeMenu.getName()));
                    lableItem.setText(GetSpanText.getSpanString(getActivity(), AppStrings.mDonation));
                    lableItem.setTextColor(R.color.black);
                    lableItem.setPadding(5, 5, 5, 5);
                    lableItem.setLayoutParams(contentParams);
                    indv_SavingsRow.addView(lableItem);

                    TextView confirm_values = new TextView(getActivity());
                    confirm_values.setText(
                            GetSpanText.getSpanString(getActivity(), String.valueOf(sSend_To_Server_Donation_FIncome)));
                    confirm_values.setTextColor(R.color.black);
                    confirm_values.setPadding(5, 5, 15, 5);
                    confirm_values.setGravity(Gravity.RIGHT);
                    confirm_values.setLayoutParams(contentParams);
                    indv_SavingsRow.addView(confirm_values);

                    confirmationTable.addView(indv_SavingsRow,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                    mEdit_RaisedButton.setText(AppStrings.edit);
                    mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                    mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                    // 205,
                    // 0));
                    mEdit_RaisedButton.setOnClickListener(this);

                    mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                    mOk_RaisedButton.setText(AppStrings.yes);
                    mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                    mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                    mOk_RaisedButton.setOnClickListener(this);

                    confirmationDialog.getWindow()
                            .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    confirmationDialog.setCanceledOnTouchOutside(false);
                    confirmationDialog.setContentView(dialogView);
                    confirmationDialog.setCancelable(true);
                    confirmationDialog.show();

                    ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                    margin.leftMargin = 10;
                    margin.rightMargin = 10;
                    margin.topMargin = 10;
                    margin.bottomMargin = 10;
                    margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

                } else {
                    sSend_To_Server_Donation_FIncome = "0";
                    TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                }
                break;

            case R.id.frag_Ok:
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();

                if (networkConnection.isNetworkAvailable()) {
                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 1) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }
                    // NOTHING TO DO::
                } else {

                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 2) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }

                }


                SavingRequest sr = new SavingRequest();
                OfflineDto offline = new OfflineDto();

                sr.setIncomeTypeId(Income.sSelectedIncomeMenu.getId());
                sr.setModeOfCash("2");
                sr.setShgId(shgDto.getShgId());
//                sr.setTransactionDate(shgDto.getLastTransactionDate());
                sr.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                sr.setMobileDate(System.currentTimeMillis() + "");
                sr.setAmount(sSend_To_Server_Donation_FIncome);

                try {
                    offline.setAnimatorId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                    offline.setShgId(shgDto.getShgId());
//                    offline.setLastTransactionDateTime(shgDto.getLastTransactionDate());
                    offline.setLastTransactionDateTime(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                    offline.setModifiedDateTime(System.currentTimeMillis() + "");
                    offline.setModeOCash("2");
                    offline.setTxType(NewDrawerScreen.INCOME);
                    offline.setTxSubtype(AppStrings.donation);
                    offline.setIncomeTypeId(Income.sSelectedIncomeMenu.getId());
                    offline.setTotalIncomeAmount(sSend_To_Server_Donation_FIncome);
                    //offline fund flow
                    int cih = 0;
                    cih = (int) Double.parseDouble(shgDto.getCashInHand()) + (int) Double.parseDouble(sSend_To_Server_Donation_FIncome);
                    offline.setCashInhand(cih + "");
                    offline.setCashAtBank(shgDto.getCashAtBank());
                    offline.setIs_transaction_tdy("1.0");
                    offlineDBData = offline;


                } catch (Exception e) {
                    e.printStackTrace();
                }


                String sreqString = new Gson().toJson(sr);
                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();
                    RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.DONATION, sreqString, getActivity(), ServiceType.DONATION_INCOME);
                } else {
                    if (TransactionTable.getLoginFlag(AppStrings.donation).size() <= 0 || (!TransactionTable.getLoginFlag(AppStrings.donation).get(TransactionTable.getLoginFlag(AppStrings.donation).size()-1).getLoginFlag().equals("1")))
                        insertIncome();
                    else
                        TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);

                }


                break;
            case R.id.fragment_Edit:
                sSend_To_Server_Donation_FIncome = "" + 0;
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
                break;
        }
    }

    private void insertTransaction_tdy()
    {
        offlineDBData.setIs_transaction_tdy("1.0");
        offlineDBData.setShgId(shgDto.getShgId());
        SHGTable.updateIstransaction(offlineDBData);
    }

    private void insertIncome() {
        try {
            int value = (MySharedPreference.readInteger(getActivity(), MySharedPreference.INCOME_COUNT, 0) + 1);
            if (value > 0)
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.INCOME_COUNT, value);

            offlineDBData.setICount(value + "");
            TransactionTable.insertTransIncomeData(offlineDBData);

            Log.i("print","getFFlag Value : "+shgDto.getFFlag());
            if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
            }
            String cihstr = "", cabstr = "", lastTranstr = "";
            cihstr = offlineDBData.getCashInhand();
            cabstr = offlineDBData.getCashAtBank();
            lastTranstr = offlineDBData.getLastTransactionDateTime();
            CashOfGroup csg = new CashOfGroup();
            csg.setCashInHand(cihstr);
            csg.setCashAtBank(cabstr);
            csg.setLastTransactionDate(lastTranstr);
            SHGTable.updateSHGDetails(csg, shgDto.getId());

            FragmentManager fm = getFragmentManager();
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            NewDrawerScreen.showFragment(new MainFragment());
        } catch (Exception e) {

        }
    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
            }

            switch (serviceType) {
                case DONATION_INCOME:
                    try {
                        if (result != null) {
                            ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                            String message = cdto.getMessage();
                            int statusCode = cdto.getStatusCode();
                            if (statusCode == Utils.Success_Code) {
                                Utils.showToast(getActivity(), message);
                                insertTransaction_tdy();
                                Log.i("print","getFFlag Value : "+shgDto.getFFlag());
                                if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                    SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                                }
                                FragmentManager fm = getFragmentManager();
                                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                MainFragment mainFragment = new MainFragment();
                                Bundle bundles = new Bundle();
                                bundles.putString("Transaction", MainFragment.Flag_Transaction);
                                mainFragment.setArguments(bundles);
                                NewDrawerScreen.showFragment(mainFragment);
                            } else {
                                if (statusCode == 401) {

                                    Log.e("Group Logout", "Logout Sucessfully");
                                    AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                    if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                        mProgressDilaog.dismiss();
                                        mProgressDilaog = null;
                                    }
                                }
                                Utils.showToast(getActivity(), message);
                                insertIncome();

                            }
                        } else {
                            insertIncome();
                        }
                    } catch (Exception e) {

                    }
                    break;
            }


        }
    }
}

package com.oasys.eshakti.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Adapter.CustomListAdapter;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.LoanBankDto;
import com.oasys.eshakti.Dto.LoanDisbursementDto;
import com.oasys.eshakti.Dto.LoanDto;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.Dto.ShgBankDetails;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.BankTable;
import com.oasys.eshakti.database.LoanDisbursementTable;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.model.ListItem;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;


import java.util.ArrayList;
import java.util.List;


/**
 * Created by Dell on 15 Dec, 2018.
 */

public class LD_InternalLoan extends Fragment implements AdapterView.OnItemClickListener, NewTaskListener {


    private TextView mGroupName, mCashinHand, mCashatBank;

    private List<ListItem> listItems;
    private ListView mListView;
    private CustomListAdapter mAdapter;
    int listImage;

    String memberDeatilsMenu[];

    public static String mServiceResponse;
    private Dialog mProgressDialog;
    boolean isNavi = false;
    boolean isFedLoan = false;
    boolean isFedName = false;
    private TextView mHeader;

    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<MemberList> arrMem;
    private ArrayList<ShgBankDetails> bankdetails;
    private View rootView;
    private Dialog mProgressDilaog;
    public static String sMenuSelection;
    private ArrayList<LoanDto> internalBLtype;
    public static LoanDto sSelectedLoanMenu;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_menulist, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
            bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
            networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());

            if (networkConnection.isNetworkAvailable()) {
                onTaskStarted();
                RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.LD_BANK_LOAN, getActivity(), ServiceType.BANKLOANTYPE);

            } else {
                memberDeatilsMenu = new String[]{AppStrings.mInternalInterloanMenu};
                int size = memberDeatilsMenu.length;

                listItems = new ArrayList<ListItem>();

                for (int i = 0; i < size; i++) {
                    ListItem item = new ListItem();
                    item.setTitle(memberDeatilsMenu[i]);
                    item.setImageId(listImage);
                    listItems.add(item);
                }

                if (listItems != null && listItems.size() > 0) {
                    mAdapter = new CustomListAdapter(getActivity(), listItems);
                    mListView.setAdapter(mAdapter);
                }
            }

            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {
        //   callMasterData();
        try {

            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            if (networkConnection.isNetworkAvailable()) {

                mHeader = (TextView) rootView.findViewById(R.id.submenuHeaderTextview);
                mHeader.setVisibility(View.VISIBLE);
                mHeader.setText(
                        "" + ("" + AppStrings.mNewLoan));
                mHeader.setTypeface(LoginActivity.sTypeface);
            } else {

                mHeader = (TextView) rootView.findViewById(R.id.submenuHeaderTextview);
                mHeader.setVisibility(View.VISIBLE);
                mHeader.setText(
                        "" + ("" + AppStrings.InternalLoanDisbursement));
                mHeader.setTypeface(LoginActivity.sTypeface);
            }
            mListView = (ListView) rootView.findViewById(R.id.fragment_List);
            listImage = R.drawable.ic_navigate_next_white_24dp;

            int mSize = 0;
            if (networkConnection.isNetworkAvailable()) {
                mSize = 4;
            } else {
                mSize = 1;
            }


            mListView.setOnItemClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        TextView textColor_Change = (TextView) view.findViewById(R.id.dynamicText);
        textColor_Change.setText(String.valueOf(memberDeatilsMenu[position]));
        textColor_Change.setTextColor(Color.rgb(251, 161, 108));

        sMenuSelection = memberDeatilsMenu[position];

        sSelectedLoanMenu = internalBLtype.get(position);

        Log.d("MENU", "Menu Item " + memberDeatilsMenu[position]);
        switch (position) {
            case 0:
                LD_IL_Entry pl_DisbursementFragment = new LD_IL_Entry();
                NewDrawerScreen.showFragment(pl_DisbursementFragment);
                break;
            case 1:

                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();
                    RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.LD_BANK_DETAILS + shgDto.getDistrictId(), getActivity(), ServiceType.LD_BANK_DETAILS);
                }
                // TODO::      RestClient.getRestClient(LD_InternalLoan.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.BANKLOANTYPE + shgDto.getShgId(), getActivity(), ServiceType.ANIMATOR_PROFILE);
                break;

            case 2:

                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();
                    RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.LD_MFI_LTYPE, getActivity(), ServiceType.MFILOANTYPE);
                }


                //TODO::      RestClient.getRestClient(LD_InternalLoan.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.MFILOANTYPE + shgDto.getShgId(), getActivity(), ServiceType.MFILOANTYPE);
                break;

            case 3:

                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();
                    RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.LD_FED_LTYPE, getActivity(), ServiceType.FEDLOANTYPE);
                }
                //TODO::     RestClient.getRestClient(LD_InternalLoan.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.FEDLOANTYPE + shgDto.getShgId(), getActivity(), ServiceType.FEDLOANTYPE);
                break;
        }
    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
            }
        }
        switch (serviceType) {
            case BANKLOANTYPE:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        for (int i = 0; i < cdto.getResponseContent().getNewLoanTypes().size(); i++) {
                            LoanDto loanDto = cdto.getResponseContent().getNewLoanTypes().get(i);
                            LoanDisbursementTable.insertLD_LTYPE(loanDto);
                        }

                        internalBLtype = cdto.getResponseContent().getNewLoanTypes();

                        memberDeatilsMenu = new String[internalBLtype.size()];

                        final String[] bankNames_BankID = new String[internalBLtype.size()];

                        //  bankNames[0] = String.valueOf("" + AppStrings.bankName);
                        for (int i = 0; i < internalBLtype.size(); i++) {
                            memberDeatilsMenu[i] = internalBLtype.get(i).getLoanTypeName().toString();
                        }

                        //   bankNames_BankID[0] = String.valueOf("" + AppStrings.bankName);
                        for (int i = 0; i < internalBLtype.size(); i++) {
                            bankNames_BankID[i] = internalBLtype.get(i).getLoanTypeId().toString();
                        }

                        int size = memberDeatilsMenu.length;

                        listItems = new ArrayList<ListItem>();


                        for (int i = 0; i < size; i++) {
                            ListItem item = new ListItem();
                            item.setTitle(memberDeatilsMenu[i]);
                            item.setImageId(listImage);
                            listItems.add(item);
                        }

                        mAdapter = new CustomListAdapter(getActivity(), listItems);
                        mListView.setAdapter(mAdapter);


                    /*
                        memberDeatilsMenu = new String[]{AppStrings.mInternalInterloanMenu, AppStrings.mInternalBankLoanMenu,
                                AppStrings.mInternalMFILoanMenu, AppStrings.mInternalFederationMenu};*/

                    } else {
                        Utils.showToast(getActivity(), message);

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                break;
            case MFILOANTYPE:
                try {

                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);

                        for (int i = 0; i < cdto.getResponseContent().getFinanceBanksList().size(); i++) {
                            LoanDisbursementDto loanDto = cdto.getResponseContent().getFinanceBanksList().get(i);
                            LoanDisbursementTable.insertLD_MFI_SETTING(loanDto);
                        }
                        LD_IL_Bank_MFI_Fed_Entry pl_1DisbursementFragment = new LD_IL_Bank_MFI_Fed_Entry();
                        NewDrawerScreen.showFragment(pl_1DisbursementFragment);
                    } else {
                        Utils.showToast(getActivity(), message);

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }

                    }
                } catch (Exception e) {

                }
                break;
            case FEDLOANTYPE:
                try {

                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        for (int i = 0; i < cdto.getResponseContent().getLoanSettingList().size(); i++) {
                            LoanDisbursementDto loanDto = cdto.getResponseContent().getLoanSettingList().get(i);
                            LoanDisbursementTable.insertLD_FED_SETTING(loanDto);
                        }

                        LD_IL_Bank_MFI_Fed_Entry pl_DisbursementFragment = new LD_IL_Bank_MFI_Fed_Entry();
                        NewDrawerScreen.showFragment(pl_DisbursementFragment);
                    } else {
                        Utils.showToast(getActivity(), message);

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }

                    }
                } catch (Exception e) {

                }
                break;

            case LD_BANK_DETAILS:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        for (int i = 0; i < cdto.getResponseContent().getBankNamesList().size(); i++) {
                            LoanBankDto loanDto = cdto.getResponseContent().getBankNamesList().get(i);
                            LoanDisbursementTable.insertLD_BANK_DETAILS(loanDto);
                        }

                        LD_IL_Bank_MFI_Fed_Entry pl_DisbursementFragment = new LD_IL_Bank_MFI_Fed_Entry();
                        NewDrawerScreen.showFragment(pl_DisbursementFragment);
                    } else {
                        Utils.showToast(getActivity(), message);

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }


    }
}

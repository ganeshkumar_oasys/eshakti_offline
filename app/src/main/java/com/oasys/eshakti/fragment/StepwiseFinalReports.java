package com.oasys.eshakti.fragment;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.R;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.SHGTable;

/**
 * A simple {@link Fragment} subclass.
 */
public class StepwiseFinalReports extends Fragment implements View.OnClickListener {
    private View view;
    private TextView total_collection;
    private TextView total_disbursement;
    private TextView stepwise_cashin_hand;
    private TextView stepwise_cashat_bank;
    private TextView stepwise_interest;
    private TextView stepwise_charges;
    private TextView stepwise_totalrepayment;
    private TextView stepwise_subventionreceived;
    private TextView stepwise_bankcharges,done;
    private TextView stepwise_total_savings;
    private TextView stepwise_total_internalloanrepayment,step_wise_total_memberloanrepayment;
    private TextView headers,totalsaving,totalvoluntaysavings,totalinternalloan,totalmemberloan,totaldisbursement,cashinHand,cashatBank,
    grouploan,totaloaninterest,totalcharges,totalrepayment,totlbankchages;
    private ListOfShg shgDto;
//    private Button done;
    private int sum_of_savings_Internalloan_Memberloan =0;
    String sav_tSavings,sav_tVolSavings,Internalloanrepayment,memberloan_repayment;


    public StepwiseFinalReports() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_stepwise_final_reports, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));

//        sum_of_savings_Internalloan_Memberloan = Savings.sum_of_savings+Internalloan_repayment.sum_of_internalloan_repayment+ MemberLoanRepayment.finalsum_of_memberloan_repayment;
        initView(view);
//        Attendance.flag="0";

        /*Log.d("flagvalue",EShaktiApplication.flag);

        if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
            NewDrawerScreen.item1.setVisible(false);
            NewDrawerScreen.item2.setVisible(false);
            NewDrawerScreen.item.setVisible(false);
            NewDrawerScreen.logOutItem.setVisible(false);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.INVISIBLE);
        }
        else
        {
            NewDrawerScreen.item1.setVisible(true);
            NewDrawerScreen.item2.setVisible(true);
            NewDrawerScreen.item.setVisible(true);
            NewDrawerScreen.logOutItem.setVisible(true);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.VISIBLE);
        }*/
        return view;
    }




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
            menu.findItem(R.id.action_home).setVisible(false);
            menu.findItem(R.id.action_logout).setVisible(false);
            menu.findItem(R.id.menu_logout).setVisible(false);
            menu.findItem(R.id.action_grouplist).setVisible(false);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.INVISIBLE);
        }
        else
        {
            menu.findItem(R.id.action_home).setVisible(true);
            menu.findItem(R.id.action_logout).setVisible(true);
            menu.findItem(R.id.menu_logout).setVisible(true);
            menu.findItem(R.id.action_grouplist).setVisible(true);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.VISIBLE);
        }
        super.onPrepareOptionsMenu(menu);

    }

    private void initView(View view) {

        headers = (TextView)view.findViewById(R.id.headers);
        headers.setText(AppStrings.reports);
        headers.setTypeface(LoginActivity.sTypeface);

        totalsaving = (TextView)view.findViewById(R.id.totalsaving);
        totalsaving.setText(AppStrings.totalsaving);
        totalsaving.setTypeface(LoginActivity.sTypeface);

        totalvoluntaysavings = (TextView)view.findViewById(R.id.totalvoluntaysavings);
        totalvoluntaysavings.setText(AppStrings.totalvoluntarysaving);
        totalvoluntaysavings.setTypeface(LoginActivity.sTypeface);

        totalinternalloan = (TextView)view.findViewById(R.id.totalinternalloanrepayment);
        totalinternalloan.setText(AppStrings.totalinternalloan);
        totalinternalloan.setTypeface(LoginActivity.sTypeface);

        totalmemberloan = (TextView)view.findViewById(R.id.totalmemberloan);
        totalmemberloan.setText(AppStrings.totalmemberloan);
        totalmemberloan.setTypeface(LoginActivity.sTypeface);

        totaldisbursement = (TextView)view.findViewById(R.id.totaldisbursement);
        totaldisbursement.setText(AppStrings.totaldisbursement);
        totaldisbursement.setTypeface(LoginActivity.sTypeface);

        cashinHand = (TextView)view.findViewById(R.id.cashinHand);
        cashinHand.setText(AppStrings.cashinhand);
        cashinHand.setTypeface(LoginActivity.sTypeface);

        cashatBank = (TextView)view.findViewById(R.id.cashatBank);
        cashatBank.setText(AppStrings.cashatBank);
        cashatBank.setTypeface(LoginActivity.sTypeface);

        grouploan = (TextView)view.findViewById(R.id.grouploan);
        grouploan.setText(AppStrings.grouploan);
        grouploan.setTypeface(LoginActivity.sTypeface);

        totaloaninterest = (TextView)view.findViewById(R.id.totaloaninterest);
        totaloaninterest.setText(AppStrings.totaloaninterest);
        totaloaninterest.setTypeface(LoginActivity.sTypeface);


        totalcharges = (TextView)view.findViewById(R.id.totalcharges);
        totalcharges.setText(AppStrings.totalcharges);
        totalcharges.setTypeface(LoginActivity.sTypeface);

        totalrepayment = (TextView)view.findViewById(R.id.totalrepayment);
        totalrepayment.setText(AppStrings.totalrepayment);
        totalrepayment.setTypeface(LoginActivity.sTypeface);

        totlbankchages = (TextView)view.findViewById(R.id.totlbankchages);
        totlbankchages.setText(AppStrings.totlbankchages);
        totlbankchages.setTypeface(LoginActivity.sTypeface);


        sav_tSavings = String.valueOf(Savings.sSavings_Total);
        sav_tVolSavings = String.valueOf(Savings.sVSavings_Total);
        Internalloanrepayment =String.valueOf(Internalloan_repayment.sum_of_internalloan_repayment);
        memberloan_repayment = String.valueOf(MemberLoanRepayment.sum_of_memberloan_repayment);


        stepwise_total_savings = (TextView) view.findViewById(R.id.total_savings);
        stepwise_total_savings.setText(sav_tSavings);

        total_collection = (TextView) view.findViewById(R.id.total_collection);
//        String con_sum_of_savings_Internalloan_Memberloan = String.valueOf(sum_of_savings_Internalloan_Memberloan);
//        Log.d("Lsuminternalloan", con_sum_of_savings_Internalloan_Memberloan);
//        total_collection.setText(con_sum_of_savings_Internalloan_Memberloan);
        total_collection.setText(sav_tVolSavings);



        stepwise_total_internalloanrepayment = (TextView)view.findViewById(R.id.total_internalloanrepayment);
        stepwise_total_internalloanrepayment.setText(Internalloanrepayment);

        step_wise_total_memberloanrepayment = (TextView)view.findViewById(R.id.total_memberloanrepayment);
        step_wise_total_memberloanrepayment.setText(memberloan_repayment);

        stepwise_cashin_hand = (TextView) view.findViewById(R.id.stepwise_cashin_hand);
        stepwise_cashin_hand.setText(shgDto.getCashInHand());

        stepwise_cashat_bank = (TextView) view.findViewById(R.id.stepwise_cashat_bank);
        stepwise_cashat_bank.setText(shgDto.getCashAtBank());

        total_disbursement = (TextView) view.findViewById(R.id.total_disbursement);
        total_disbursement.setText(String.valueOf(LD_IL_Entry.mTotalDisbursement));

        stepwise_interest = (TextView) view.findViewById(R.id.stepwise_interest);
        stepwise_interest.setText(String.valueOf(Transaction_LoanType_Details.store_interest));

        stepwise_charges = (TextView) view.findViewById(R.id.stepwise_charges);
        stepwise_charges.setText(String.valueOf(Transaction_LoanType_Details.store_charges));

        stepwise_totalrepayment = (TextView) view.findViewById(R.id.stepwise_totalrepayment);
        stepwise_totalrepayment.setText(String.valueOf(Transaction_LoanType_Details.store_repayment));

        stepwise_subventionreceived = (TextView) view.findViewById(R.id.stepwise_subventionreceived);
        stepwise_subventionreceived.setText(String.valueOf(Transaction_LoanType_Details.store_interest_subvention));

        stepwise_bankcharges = (TextView) view.findViewById(R.id.stepwise_bankcharges);
        stepwise_bankcharges.setText(String.valueOf(Transaction_LoanType_Details.store_bank_charges));



        done = (TextView) view.findViewById(R.id.done);
        done.setText(AppStrings.mDone);
        done.setTypeface(LoginActivity.sTypeface);
        done.setOnClickListener(this);
    }

    @Override
   public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done:
                MainFragment mainFragment = new MainFragment();
//                Savings.sum_of_savings = 0;
//                Attendance.flag="0";
//                EShaktiApplication.flag="0";
//                Savings.sSavings_Total=0;
//                Savings.sVSavings_Total =0;
//                Internalloan_repayment.sum_of_internalloan_repayment = 0;
////                MemberLoanRepayment.finalsum_of_memberloan_repayment =0;
////                Utils.sum_of_internalloan_repayment=0;
////                Utils.sum_of_memberloan_repayment=0;
//                LD_IL_Entry.mTotalDisbursement =0;


//                    EShaktiApplication.setFlag("0");
//                    EShaktiApplication.flag = "0";
                    MemberLoanRepayment.sum_of_memberloan_repayment = 0;
                    Transaction_LoanType_Details.store_interest = 0;
                    Transaction_LoanType_Details.store_charges = 0;
                    Transaction_LoanType_Details.store_repayment = 0;
                    Transaction_LoanType_Details.store_interest_subvention = 0;
                    Transaction_LoanType_Details.store_bank_charges = 0;
//                sum_of_savings_Internalloan_Memberloan = 0;

//                    NewDrawerScreen.showFragment(mainFragment);
//                FragmentManager fm = getFragmentManager();
//                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                NewDrawerScreen.showFragment(mainFragment);

               break;
        }
    }
}

package com.oasys.eshakti.fragment;


import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.Adapter.CustomItemAdapter;
import com.oasys.eshakti.Adapter.Grouploan_Adapter;
import com.oasys.eshakti.Dialogue.Dialog_New_TransactionDate;
import com.oasys.eshakti.Dto.CashOfGroup;
import com.oasys.eshakti.Dto.ExistingLoan;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.OfflineDto;
import com.oasys.eshakti.Dto.RequestDto.GroupLoanRepaymentDto;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.Dto.SavingsAccounts;
import com.oasys.eshakti.Dto.SavingsBalance;
import com.oasys.eshakti.Dto.ShgBankDetails;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.BankTable;
import com.oasys.eshakti.database.LoanTable;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.database.TransactionTable;
import com.oasys.eshakti.model.RowItem;
import com.oasys.eshakti.views.MaterialSpinner;
import com.tutorialsee.lib.TastyToast;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Transaction_LoanType_Details extends Fragment implements NewTaskListener {
    private RadioButton mSavingsAccRadio, mLoanAccRadio;
    private RadioGroup mRadioGroup;
    private View view;
    private LinearLayout materialSpinner_layout, bankcharge_layout, spinnerlayoutamount;
    private MaterialSpinner materialSpinner;
    private Button transaction_submit_button, transaction_skip_button;
    ResponseDto bankitemdto;
    private EditText editText_interest, editText_charges, editText_repayment, editText_interest_subvention, edittext_bank_charges;
    private int selectedId;
    String type_of_radiovalue = AppStrings.mcash;
    private static FragmentManager fm;
    private ListOfShg shgDto;
    private String shgId;
    private TextView grouploanRnrepayment_outstanding, amount;
    private TextView mGroupreportName_loantype;
    private TextView textoutstanding,interest1,charges1,repayment,subvention,bankcharge;
    private String str_editText_interest, str_editText_charges, str_editText_repayment, str_editText_interest_subvention, str_materialSpinner = "SELECT BANK NAME";
    private String str_edittext_bank_charges = "";
    private String loan_id = "", loan_ac_id;
    private String accountnumber = "";
    private String loantype = "";
    private String bankname = "";
    private String outstandings = "";
    private String modeofcash = "";
    private String bankaccountid = "";
    private TextView loantypess, bankloan, accountno, cashhand, cashbank;
    private String outstanding;
    private int outstanding_convert;
    private int str_editText_repayment_convert;
    private String apioutstanding_value;
    //    private double mApiOutStandingValue  ;
    private int convert_apioutstanding_value;
    private int convert_cashin_hand;
    private Dialog dialog;
    private GroupLoanRepaymentDto groupLoanRepaymentDto;
    private ArrayList<ShgBankDetails> bankdetails;
    //    private int con_str_editText_interest,con_str_editText_charges,con_str_editText_repayment,con_str_editText_bankcharge,con_str_editText_interest_subvention;
//    private int add_oustanding_interst;
    private String flag = "0";
    private ArrayList<CashOfGroup> csGrp;
    private ArrayList<SavingsAccounts> spn_label_Bankbranch;
    ArrayList<String> mBanknames_Array = new ArrayList<String>();
    ArrayList<String> mBanknamesId_Array = new ArrayList<String>();
    private List<RowItem> bankBranchItems;
    CustomItemAdapter mBankBranchAdapter;
    private ShgBankDetails sSelectedSavingbank;
    private List<SavingsAccounts> internalBranchList = new ArrayList<>();
    SavingsAccounts[] mBankBranchArray;
    private String[] mBankBranchArr;
    private String[] branchNameArr;
    private String mBankBranchValue = "";
    private String mSelectionBranchId;
    private SavingsAccounts accounts;
    private String currentbalance = "";
    private int current_bal = 0;
    private int con_cashinhand;
    private int con_cashatbank;
    private SavingsBalance savingsBalance;
    private String bank_id;
    private String bank_bal;
    public static int store_interest ;
    public static int store_charges;
    public static int store_repayment;
    public static int store_interest_subvention;
    public static int store_bank_charges;
    private String spl_cashbank = "";
    private String spl = "";
    private int con_cash_in_hand;
    private int con_cash_at_bank;
    private ArrayList<RowItem> stateNameItems;
    private CustomItemAdapter bankNameAdapter;
    ArrayList<String> mEngSendtoServerBankId_Array = new ArrayList<String>();
    ArrayList<String> mEngSendtoServerBank_Array = new ArrayList<String>();
    private int convert__str_editText_interest = 0;
    private int convert__str_editText_charges = 0;
    private int convert_editText_repayment = 0;
    private int convert_editText_interest_subvention = 0;
    private int offline_repayment_subvention;
    private int offline_cashatbankvalution = 0;
    private int offline_cashatbank = 0;
    private int offline_outstanding;
    private int bankcurrent_balance = 0;
    private int convert__edittext_bank_charges = 0;
    private int reducebankbalance = 0;
    OfflineDto offlineDBata = new OfflineDto();
    private List<ExistingLoan> grploanDetails;
    boolean resume_value=false;
    int statusCode;
    String message;


    public Transaction_LoanType_Details() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_transaction__loan_type__details, container, false);
        dialog = new Dialog(getActivity());
        fm = getActivity().getSupportFragmentManager();
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        grploanDetails = LoanTable.getGrpLoanDetails(shgDto.getShgId());
        shgId = shgDto.getShgId();
//       Log.d("shgid",shgId);
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        inIt(view);
        mGroupreportName_loantype.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
//        for (int i=0;i<bankdetails.size();i++)
//        {
//            bankaccountid = bankdetails.get(i).getShgSavingsAccountId();
//
//
//        }
//        Log.d("id1",bankaccountid);


      /*  Log.d("flagvalue",EShaktiApplication.flag);

        if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
            NewDrawerScreen.item1.setVisible(false);
            NewDrawerScreen.item2.setVisible(false);
            NewDrawerScreen.item.setVisible(false);
            NewDrawerScreen.logOutItem.setVisible(false);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.INVISIBLE);
        }
        else
        {
            NewDrawerScreen.item1.setVisible(true);
            NewDrawerScreen.item2.setVisible(true);
            NewDrawerScreen.item.setVisible(true);
            NewDrawerScreen.logOutItem.setVisible(true);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.VISIBLE);
        }*/


        try {
//            Attendance.flag ="0";
            Bundle bundle = getArguments();
            loan_id = bundle.getString("loan_id");
            loan_ac_id = bundle.getString("loan_ac_id");
            accountnumber = bundle.getString("account_number");
            loantype = bundle.getString("loan_type");
            bankname = bundle.getString("bank_name");
            outstandings = bundle.getString("outstanding");
            Log.d("LOANID", loan_id);

            if (bundle != null) {
//                EShaktiApplication.flag = bundle.getString("stepwise");
//                Log.d("LOANID", Attendance.flag);

//                if (EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1"))
                if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") )
                {
                    loan_id = bundle.getString("loan_id");
                    accountnumber = bundle.getString("account_number");
                    loantype = bundle.getString("loan_type");
                    bankname = bundle.getString("bank_name");
                    outstandings = bundle.getString("outstanding");
                    transaction_skip_button.setVisibility(View.VISIBLE);

                    if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
                        //  onTaskStarted();
                        RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.NAV_DETAILS + shgDto.getId(), getActivity(), ServiceType.NAV_DETAILS);

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (outstandings != null) {
            convert_apioutstanding_value = (int) Double.parseDouble(outstandings);
        }

        String convertint_apioutstanding_value = String.valueOf(convert_apioutstanding_value);
        grouploanRnrepayment_outstanding.setText(convertint_apioutstanding_value);
        loantypess.setText(loantype + " - " + bankname);
//        bankloan.setText(bankname);
        accountno.setText(AppStrings.mloanaccountnumber+":" + accountnumber);

        cashhand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        cashbank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());

        try {
            String cashhand = shgDto.getCashInHand();
            spl = cashhand.substring(0, cashhand.length());
            if (spl != null) {
                con_cashinhand = Integer.parseInt(spl);
            }


            String cashbank = shgDto.getCashAtBank();
            spl_cashbank = cashbank.substring(0, cashbank.length());

            if (spl_cashbank != null) {
                con_cashatbank = Integer.parseInt(spl_cashbank);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
            menu.findItem(R.id.action_home).setVisible(false);
            menu.findItem(R.id.action_logout).setVisible(false);
            menu.findItem(R.id.menu_logout).setVisible(false);
            menu.findItem(R.id.action_grouplist).setVisible(false);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.INVISIBLE);
        }
        else
        {
            menu.findItem(R.id.action_home).setVisible(true);
            menu.findItem(R.id.action_logout).setVisible(true);
            menu.findItem(R.id.menu_logout).setVisible(true);
            menu.findItem(R.id.action_grouplist).setVisible(true);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.VISIBLE);
        }
        super.onPrepareOptionsMenu(menu);

    }
    public void inIt(View view) {

        mRadioGroup = (RadioGroup) view.findViewById(R.id.radio_loanAccTransaction);
        mSavingsAccRadio = (RadioButton) view.findViewById(R.id.radioloanaccTransaction_SavingsAcc);
        mSavingsAccRadio.setText(AppStrings.mcash);
        mLoanAccRadio = (RadioButton) view.findViewById(R.id.radioloanaccTransaction_LoanAcc);
        mLoanAccRadio.setText(AppStrings.mbank);
        materialSpinner_layout = (LinearLayout) view.findViewById(R.id.spinnerlayout);
        spinnerlayoutamount = (LinearLayout) view.findViewById(R.id.spinnerlayoutamount);
        bankcharge_layout = (LinearLayout) view.findViewById(R.id.bankcharge_layout);
        materialSpinner = (MaterialSpinner) view.findViewById(R.id.loanAccTransactionspinner);

//        materialSpinner.setHint("SELECT BANK NAME");
        materialSpinner.setHintColor(Color.BLACK);

        grouploanRnrepayment_outstanding = (TextView) view.findViewById(R.id.grouploanRnrepayment_outstanding);
        transaction_submit_button = (Button) view.findViewById(R.id.transaction_submit_button);
        transaction_skip_button = (Button) view.findViewById(R.id.transaction_skip_button);
        editText_interest = (EditText) view.findViewById(R.id.interest);
        editText_charges = (EditText) view.findViewById(R.id.charges);
        editText_repayment = (EditText) view.findViewById(R.id.Repayment);
        editText_interest_subvention = (EditText) view.findViewById(R.id.Interest_Subvention);
        edittext_bank_charges = (EditText) view.findViewById(R.id.edittext_bank_charges);
        loantypess = (TextView) view.findViewById(R.id.loantypess);
        amount = (TextView) view.findViewById(R.id.amount);
        textoutstanding = (TextView) view.findViewById(R.id.textoutstanding);
        interest1 = (TextView) view.findViewById(R.id.interest1);
        charges1 = (TextView) view.findViewById(R.id.charges1);
        repayment = (TextView) view.findViewById(R.id.repayment);
        subvention = (TextView) view.findViewById(R.id.subvention);
        bankcharge = (TextView) view.findViewById(R.id.bankcharge);

        textoutstanding.setText(AppStrings.outstanding);
        interest1.setText(AppStrings.interest);
        charges1.setText(AppStrings.charges);
        repayment.setText(AppStrings.repayment);
        subvention.setText(AppStrings.subvention);
        bankcharge.setText(AppStrings.bankcharge);
        transaction_submit_button.setText(AppStrings.submit);
//        bankloan=(TextView)view.findViewById(R.id.bankloan);
        accountno = (TextView) view.findViewById(R.id.account_no);
        bankitemdto = new ResponseDto();
        cashhand = (TextView) view.findViewById(R.id.cashhand);
        cashbank = (TextView) view.findViewById(R.id.cashbank);
        mGroupreportName_loantype = (TextView) view.findViewById(R.id.mGroupreportName_loantype);


        materialSpinner.setBaseColor(R.color.grey_400);

        materialSpinner.setFloatingLabelText("LOAN ACCOUNT BANK");

        materialSpinner.setPaddingSafe(10, 0, 10, 0);

        spn_label_Bankbranch = new ArrayList<>();

//  material Spinner

        final String[] bankNames = new String[bankdetails.size() + 1];

        final String[] bankNames_BankID = new String[bankdetails.size() + 1];

        final String[] bankAmount = new String[bankdetails.size() + 1];

        bankNames[0] = String.valueOf("Select Bank Name");
        for (int i = 0; i < bankdetails.size(); i++) {
            bankNames[i + 1] = bankdetails.get(i).getBankName();
        }

        bankNames_BankID[0] = String.valueOf("Select Bank Name");
        for (int i = 0; i < bankdetails.size(); i++) {
            bankNames_BankID[i + 1] = bankdetails.get(i).getBankId();
        }

        bankAmount[0] = String.valueOf("Bank Amount");
        for (int i = 0; i < bankdetails.size(); i++) {
            bankAmount[i + 1] = bankdetails.get(i).toString();
        }
        int size = bankNames.length;

        stateNameItems = new ArrayList<RowItem>();
        for (int i = 0; i < size; i++) {
            RowItem rowItem = new RowItem(bankNames[i]);// SelectedGroupsTask.sBankNames.elementAt(i).toString());
            stateNameItems.add(rowItem);
        }

        bankNameAdapter = new CustomItemAdapter(getActivity(), stateNameItems);
        materialSpinner.setAdapter(bankNameAdapter);

        materialSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if (position == 0) {
                    SeedFund.selectedItemBank = bankNames_BankID[0];
                    SeedFund.mBankNameValue = "0";
                    spinnerlayoutamount.setVisibility(View.GONE);

                } else {
                    SeedFund.selectedItemBank = bankNames_BankID[position];
                    SeedFund.selectedSBAcId = bankdetails.get(position - 1).getShgSavingsAccountId();
                    Log.d("selectedSBAcId", SeedFund.selectedSBAcId);
                    str_materialSpinner = bankNames[position];
                    System.out.println("SELECTED BANK NAME : " + SeedFund.selectedItemBank);
                    SeedFund.mBankNameValue = SeedFund.selectedItemBank;
                    spinnerlayoutamount.setVisibility(View.VISIBLE);
                    String mBankname = null;
                    for (int i = 0; i < mBanknames_Array.size(); i++) {
                        if (SeedFund.selectedItemBank.equals(mEngSendtoServerBankId_Array.get(i))) {
                            mBankname = mEngSendtoServerBank_Array.get(i);
                        }
                    }
                    SeedFund.mBankNameValue = str_materialSpinner;

                    if (SeedFund.selectedSBAcId != null) {
                        ShgBankDetails details = BankTable.getBankTransaction(SeedFund.selectedSBAcId);
                        bankcurrent_balance = (int) Double.parseDouble(details.getCurrentBalance());
                        amount.setText("Available Balance :" + bankcurrent_balance);
                    }
                }




//                if (SeedFund.selectedSBAcId != null) {
//                    if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
////           String shgid_spinner = "45c8f633-22ac-43f4-8f5b-ae307594f1ef";
//                        RestClient.getRestClient(Transaction_LoanType_Details.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.SPINNER_BANK_ITEM_AMOUNT + SeedFund.selectedSBAcId, getActivity(), ServiceType.SPINNER_BANK_ITEM_AMOUNT_TYPE);
//                    } else {
//                        Utils.showToast(getActivity(), "Network Not Available");
//                    }
//
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                selectedId = radioGroup.getCheckedRadioButtonId();

                if (selectedId == R.id.radioloanaccTransaction_SavingsAcc) {
                    type_of_radiovalue = AppStrings.mcash;
                    materialSpinner_layout.setVisibility(View.GONE);
                    bankcharge_layout.setVisibility(View.GONE);
                    spinnerlayoutamount.setVisibility(View.GONE);
                } else if (selectedId == R.id.radioloanaccTransaction_LoanAcc) {
                    type_of_radiovalue = AppStrings.mbank;
                    materialSpinner_layout.setVisibility(View.VISIBLE);
                    bankcharge_layout.setVisibility(View.VISIBLE);
                    spinnerlayoutamount.setVisibility(View.VISIBLE);
                }

            }
        });

        transaction_submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    /*store_interest=0;
                    store_charges=0;
                    store_repayment=0;
                    store_interest_subvention=0;
                    store_bank_charges=0;*/

                    str_editText_interest = editText_interest.getText().toString();
                    str_editText_charges = editText_charges.getText().toString();
                    str_editText_repayment = editText_repayment.getText().toString();
                    str_editText_interest_subvention = editText_interest_subvention.getText().toString();
                    str_edittext_bank_charges = edittext_bank_charges.getText().toString();


                    if (!str_edittext_bank_charges.isEmpty()) {
                        convert__edittext_bank_charges = Integer.parseInt(str_edittext_bank_charges);
                    }

                    if (!str_editText_repayment.isEmpty()) {
                        convert_editText_repayment = Integer.parseInt(str_editText_repayment);
                    }

                    if (!str_editText_interest_subvention.isEmpty()) {
                        convert_editText_interest_subvention = Integer.parseInt(str_editText_interest_subvention);
                    }


                    if (!str_editText_interest.isEmpty()) {
                        convert__str_editText_interest = Integer.parseInt(str_editText_interest);
                    }


                    if (!str_editText_charges.isEmpty()) {
                        convert__str_editText_charges = Integer.parseInt(str_editText_charges);
                    }
                    int addvalue = convert_editText_repayment + convert_editText_interest_subvention;
                    int repayment_Bankcharge = convert_editText_repayment + convert__edittext_bank_charges;
                    int totaledit_value = convert__edittext_bank_charges+convert_editText_repayment+convert_editText_interest_subvention+convert__str_editText_interest+convert__str_editText_charges;

                    if (type_of_radiovalue.equals(AppStrings.mcash)) {

                        if (str_editText_interest.isEmpty() &&
                                str_editText_charges.isEmpty() &&
                                str_editText_repayment.isEmpty() &&
                                str_editText_interest_subvention.isEmpty() &&
                                str_edittext_bank_charges.isEmpty()) {
                            TastyToast.makeText(getActivity(), "PROVIDE THE CASH DETAILS", TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                        }
                        else if(totaledit_value==0)
                        {
                            TastyToast.makeText(getActivity(), "PROVIDE THE CASH DETAILS", TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                        }

                        else if (convert_apioutstanding_value < addvalue) {
                            TastyToast.makeText(getActivity(), "CHECK YOUR OUTSTANDING AMOUNT", TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                        } else if (con_cashinhand < convert_editText_repayment) {
                            TastyToast.makeText(getActivity(), "CHECK YOUR CASH IN HAND", TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                        } else {
                            displayDialogWindow();
                        }

                    } else if (type_of_radiovalue.equals(AppStrings.mbank)) {

                        if (SeedFund.mBankNameValue == "0") {

                            TastyToast.makeText(getActivity(), "PLEASE SELECT A BANK NAME", TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                        } else if (str_editText_interest.isEmpty() &&
                                str_editText_charges.isEmpty() &&
                                str_editText_repayment.isEmpty() &&
                                str_editText_interest_subvention.isEmpty() &&
                                str_edittext_bank_charges.isEmpty()) {
                            TastyToast.makeText(getActivity(), "PROVIDE THE CASH DETAILS", TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                        } else if (convert_apioutstanding_value < addvalue) {
                            TastyToast.makeText(getActivity(), "CHECK YOUR OUTSTANDING AMOUNT", TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                        } else if (bankcurrent_balance < convert_editText_repayment || bankcurrent_balance < repayment_Bankcharge || bankcurrent_balance < convert__edittext_bank_charges) {
                            TastyToast.makeText(getActivity(), "CHECK YOUR BANK AMOUNT", TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                        } else if (con_cashatbank < convert__edittext_bank_charges) {
                            TastyToast.makeText(getActivity(), "CHECK YOUR CASH AT BANK", TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                        } else {
                            displayDialogWindow();
                        }
                    }

                    store_interest += convert__str_editText_interest;
                    store_charges += convert__str_editText_charges;
                    store_repayment += convert_editText_repayment;
                    store_interest_subvention += convert_editText_interest_subvention;
                    store_bank_charges += convert__edittext_bank_charges;

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        transaction_skip_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Transaction_LoanType_Details.showFragment(new MainFragment());
//                dialog.dismiss();
             /*   Calendar calender = Calendar.getInstance();

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(calender.getTime());

                DateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
                Date d = new Date(Long.parseLong(shgDto.getLastTransactionDate()));
                String dateStr = simple.format(d);
                groupLoanRepaymentDto = new GroupLoanRepaymentDto();

                if (modeofcash == "1") {

                    groupLoanRepaymentDto.setShgId(shgId);
                    groupLoanRepaymentDto.setLoanId(loan_id);
                    groupLoanRepaymentDto.setMobileDate(formattedDate);
                    groupLoanRepaymentDto.setTransactionDate(dateStr);
                    groupLoanRepaymentDto.setInterest(str_editText_interest);
                    groupLoanRepaymentDto.setCharges(str_editText_charges);
                    groupLoanRepaymentDto.setRepaymentAmount(str_editText_repayment);
                    groupLoanRepaymentDto.setInterestSubvensionRecevied(str_editText_interest_subvention);
                    groupLoanRepaymentDto.setBankCharges(str_edittext_bank_charges);
                    groupLoanRepaymentDto.setModeOfCash(modeofcash);
                    groupLoanRepaymentDto.setSbAccountId(bankaccountid);

//                    conversion
//                    con_str_editText_interest = Integer.parseInt(str_editText_interest);
//                    con_str_editText_charges = Integer.parseInt(str_editText_charges);
//                    con_str_editText_repayment = Integer.parseInt(str_editText_repayment);
//                    con_str_editText_bankcharge = Integer.parseInt(str_edittext_bank_charges);
//                    con_str_editText_interest_subvention = Integer.parseInt(str_editText_interest_subvention);
//
//                     add_oustanding_interst = con_str_editText_interest+convert_apioutstanding_value;

                } else if (modeofcash == "2") {

                    groupLoanRepaymentDto.setShgId(shgId);
                    groupLoanRepaymentDto.setLoanId(loan_id);
                    groupLoanRepaymentDto.setMobileDate(formattedDate);
                    groupLoanRepaymentDto.setTransactionDate(dateStr);
                    groupLoanRepaymentDto.setInterest(str_editText_interest);
                    groupLoanRepaymentDto.setCharges(str_editText_charges);
                    groupLoanRepaymentDto.setRepaymentAmount(str_editText_repayment);
                    groupLoanRepaymentDto.setInterestSubvensionRecevied(str_editText_interest_subvention);
                    groupLoanRepaymentDto.setModeOfCash(modeofcash);

                }
                String transaction_loan_end = new Gson().toJson(groupLoanRepaymentDto);
                Log.d("trans", transaction_loan_end);
                if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

                    RestClient.getRestClient(Transaction_LoanType_Details.this).callRestWebService(Constants.BASE_URL + Constants.GROUPLOANREPAYMENT_END, transaction_loan_end, getActivity(), ServiceType.GROUP_LOAN_REPAYMENT_END);
                } else {
                    Utils.showToast(getActivity(), "Network Not Available");
                }
*/
                resume_value=true;
//                if (EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1"))
                if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") )
                {
                    Utils.loanTypes.remove(0);

                    if (Utils.loanTypes.size() > 0) {

                        Transaction_LoanType_Details transaction_loanType_details = new Transaction_LoanType_Details();
                        Bundle bundles = new Bundle();
//                        bundles.putString("stepwise", EShaktiApplication.flag);
                        bundles.putString("loan_id", Utils.loanTypes.get(0).getLoanId());
                        bundles.putString("loan_type", Utils.loanTypes.get(0).getLoanTypeName());
                        bundles.putString("account_number", Utils.loanTypes.get(0).getAccountNumber());
                        bundles.putString("bank_name", Utils.loanTypes.get(0).getBankName());
                        bundles.putString("outstanding", Utils.loanTypes.get(0).getLoanOutstanding());
                        transaction_loanType_details.setArguments(bundles);
                        NewDrawerScreen.showFragment(transaction_loanType_details);
                        dialog.dismiss();

                    } else {
                        MinutesOFMeeting minutesOFMeeting = new MinutesOFMeeting();
//                        Bundle bundles = new Bundle();
//                        bundles.putString("stepwise", EShaktiApplication.flag);
//                        minutesOFMeeting.setArguments(bundles);
                        NewDrawerScreen.showFragment(minutesOFMeeting);
                        dialog.dismiss();
                    }
                }
            }
        });


    }

    public void displayDialogWindow() {
        dialog.setContentView(R.layout.confirmation_dialog);

        TextView type = (TextView) dialog.findViewById(R.id.transaction_loantypevalue);
        TextView interest = (TextView) dialog.findViewById(R.id.transaction_loan_interestvalue);
        TextView charges = (TextView) dialog.findViewById(R.id.transaction_loanchargesvalue);
        TextView repayment = (TextView) dialog.findViewById(R.id.transaction_loanrepaymentvalue);
        TextView interest_subvention = (TextView) dialog.findViewById(R.id.transaction_loan_interest_subventionvalue);
        TextView bankname = (TextView) dialog.findViewById(R.id.transaction_loanbanknamevalue);
        TextView bankcharges = (TextView) dialog.findViewById(R.id.transaction_loanbankchargesvalue);
        TextView transactiondate = (TextView) dialog.findViewById(R.id.transaction_date);
        DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
        Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
        String dateStr = simple.format(d);
        transactiondate.setText(dateStr);
        Button edit = (Button) dialog.findViewById(R.id.edit);
        Button ok = (Button) dialog.findViewById(R.id.ok);
        LinearLayout dialog_bank_charge_layout = (LinearLayout) dialog.findViewById(R.id.dialog_bank_charge_layout);
        LinearLayout dialog_bankname_layout = (LinearLayout) dialog.findViewById(R.id.dialog_bankname_layout);

        if (str_editText_interest.equals("")) {
            str_editText_interest = "0";
        }
        if (str_editText_charges.equals("")) {
            str_editText_charges = "0";
        }
        if (str_editText_repayment.equals("")) {
            str_editText_repayment = "0";
        }
        if (str_editText_interest_subvention.equals("")) {
            str_editText_interest_subvention = "0";
        }
        if (str_edittext_bank_charges.equals("")) {
            str_edittext_bank_charges = "0";
        }

        if (type_of_radiovalue.equals(AppStrings.mcash)) {
            modeofcash = "2";

            dialog_bankname_layout.setVisibility(View.GONE);
            type.setText(type_of_radiovalue);
            interest.setText(str_editText_interest);
            charges.setText(str_editText_charges);
            repayment.setText(str_editText_repayment);
            interest_subvention.setText(str_editText_interest_subvention);


        } else if (type_of_radiovalue.equals(AppStrings.mbank)) {
            modeofcash = "1";
            dialog_bank_charge_layout.setVisibility(View.VISIBLE);
            dialog_bankname_layout.setVisibility(View.VISIBLE);
            type.setText(type_of_radiovalue);
            interest.setText(str_editText_interest);
            bankname.setText(str_materialSpinner);
            bankcharges.setText(str_edittext_bank_charges);
            charges.setText(str_editText_charges);
            repayment.setText(str_editText_repayment);
            interest_subvention.setText(str_editText_interest_subvention);

        }
        dialog.show();
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Transaction_LoanType_Details.showFragment(new MainFragment());
//                dialog.dismiss();

                if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 1) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }
                    // NOTHING TO DO::
                } else {

                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 2) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }

                }


                Calendar calender = Calendar.getInstance();

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(calender.getTime());

                DateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
                Date d = new Date(Long.parseLong(shgDto.getLastTransactionDate()));
                String dateStr = simple.format(d);
                groupLoanRepaymentDto = new GroupLoanRepaymentDto();
                OfflineDto offline = new OfflineDto();

                if (modeofcash == "1") {

                    groupLoanRepaymentDto.setShgId(shgId);
                    groupLoanRepaymentDto.setLoanId(loan_id);
                    groupLoanRepaymentDto.setMobileDate(formattedDate);
//                    groupLoanRepaymentDto.setTransactionDate(dateStr);
                    groupLoanRepaymentDto.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                    groupLoanRepaymentDto.setInterest(str_editText_interest);
                    groupLoanRepaymentDto.setCharges(str_editText_charges);
                    groupLoanRepaymentDto.setRepaymentAmount(str_editText_repayment);
                    groupLoanRepaymentDto.setInterestSubvensionRecevied(str_editText_interest_subvention);
                    groupLoanRepaymentDto.setBankCharges(str_edittext_bank_charges);
                    groupLoanRepaymentDto.setModeOfCash(modeofcash);
                    groupLoanRepaymentDto.setSbAccountId(SeedFund.selectedSBAcId);


//                    conversion
//                    con_str_editText_interest = Integer.parseInt(str_editText_interest);
//                    con_str_editText_charges = Integer.parseInt(str_editText_charges);
//                    con_str_editText_repayment = Integer.parseInt(str_editText_repayment);
//                    con_str_editText_bankcharge = Integer.parseInt(str_edittext_bank_charges);
//                    con_str_editText_interest_subvention = Integer.parseInt(str_editText_interest_subvention);
//
//                     add_oustanding_interst = con_str_editText_interest+convert_apioutstanding_value;
                } else if (modeofcash == "2") {

                    groupLoanRepaymentDto.setShgId(shgId);
                    groupLoanRepaymentDto.setLoanId(loan_id);
                    groupLoanRepaymentDto.setMobileDate(formattedDate);
//                    groupLoanRepaymentDto.setTransactionDate(dateStr);
                    groupLoanRepaymentDto.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                    groupLoanRepaymentDto.setInterest(str_editText_interest);
                    groupLoanRepaymentDto.setCharges(str_editText_charges);
                    groupLoanRepaymentDto.setRepaymentAmount(str_editText_repayment);
                    groupLoanRepaymentDto.setInterestSubvensionRecevied(str_editText_interest_subvention);
                    groupLoanRepaymentDto.setModeOfCash(modeofcash);

                }


                try {
                    offline.setAnimatorId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                    offline.setShgId(shgDto.getShgId());
                    offline.setLoanId(loan_id);
//                    offline.setLastTransactionDateTime(shgDto.getLastTransactionDate());
                    offline.setLastTransactionDateTime(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                    offline.setModifiedDateTime(System.currentTimeMillis() + "");
                    offline.setMobileDate(formattedDate);
                    offline.setTransactionDate(dateStr);
                    offline.setTxType(NewDrawerScreen.GROUP_LOAN_REPAYMENT);
                    offline.setMLoanName(loantype);
                    offline.setAccountNumber(accountnumber);
                    offline.setTxSubtype(AppStrings.grouploanrepayment);
                    offline.setGrp_interest(str_editText_interest);
                    offline.setGrp_charge(str_editText_charges);
                    offline.setGrp_repayment(str_editText_repayment);
                    offline.setGrp_intSubventionRecieved(str_editText_interest_subvention);
                    offline.setIs_transaction_tdy("1.0");

                    if (type_of_radiovalue.equals(AppStrings.mcash)) {

                        offline.setModeOCash("2");
                    } else {
                        offline.setModeOCash("1");
                        offline.setBankCharges(str_edittext_bank_charges);
                        offline.setShgSavingsAccountId(SeedFund.selectedSBAcId);
//                        int cb = 0;
//                        int lessamount = ((int) Double.parseDouble(str_editText_repayment)+ (int) Double.parseDouble(str_edittext_bank_charges));
////                        int addamount = ((int) Double.parseDouble(str_editText_interest)+ (int) Double.parseDouble(str_editText_charges));
//                        cb = bankcurrent_balance - lessamount;
//                        offline.setCurr_balance(cb+"");
//                        offline.setAccountNumber(SeedFund.selectedSBAcId);
                        offline.setMToBk(SeedFund.mBankNameValue);
//                        updateBKCB(offline);
                    }

                    //Offline fundflow
                    if (type_of_radiovalue.equals(AppStrings.mcash)) {
                        offline_repayment_subvention = convert_editText_repayment + convert_editText_interest_subvention;
                        offline_outstanding = convert_apioutstanding_value - offline_repayment_subvention + (convert__str_editText_interest + convert__str_editText_charges);

                        int cih = 0;
                        cih = (int) Double.parseDouble(shgDto.getCashInHand()) - (int) Double.parseDouble(str_editText_repayment);
                        offline.setCashInhand(cih + "");
                        offline.setCashAtBank(shgDto.getCashAtBank() + "");
                        offline.setGrouploan_Outstanding(offline_outstanding + "");

                    } else {

                        offline_repayment_subvention = convert_editText_repayment + convert_editText_interest_subvention;
                        offline_outstanding = convert_apioutstanding_value - offline_repayment_subvention + (convert__str_editText_interest + convert__str_editText_charges);
                        offline_cashatbank = convert_editText_repayment + convert__edittext_bank_charges;
//                        reducebankbalance = bankcurrent_balance - offline_repayment_subvention;
                        reducebankbalance = bankcurrent_balance - (convert_editText_repayment+convert__edittext_bank_charges);
                        int cab = 0;
                        cab = (int) Double.parseDouble(shgDto.getCashAtBank()) - (int) Double.parseDouble(offline_cashatbank + "");
                        offline.setCashAtBank(cab + "");
                        offline.setCashInhand(shgDto.getCashInHand() + "");
                        offline.setGrouploan_Outstanding(offline_outstanding + "");
                        offline.setCurr_balance1(reducebankbalance + "");
                    }

//                    offline.setCurr_balance();
                    offline.setGrp_os(offline_outstanding + "");
                    offlineDBata = offline;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String transaction_loan_end = new Gson().toJson(groupLoanRepaymentDto);
//                Log.d("trans", transaction_loan_end);

                Log.i("print","EShaktiApplication.getFlag() : "+EShaktiApplication.getFlag());
                if(EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1"))
                {
                    if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

                        RestClient.getRestClient(Transaction_LoanType_Details.this).callRestWebService(Constants.BASE_URL + Constants.GROUPLOANREPAYMENT_END, transaction_loan_end, getActivity(), ServiceType.GROUP_LOAN_REPAYMENT_END);
                    } else {
                        if (TransactionTable.getLoginFlag(AppStrings.grouploanrepayment).size() <= 0 || (!TransactionTable.getLoginFlag(AppStrings.grouploanrepayment).get(TransactionTable.getLoginFlag(AppStrings.grouploanrepayment).size() - 1).getLoginFlag().equals("1"))) {

                            grploanDetails.remove(0);

                            if (grploanDetails.size() > 0) {

                                Transaction_LoanType_Details transaction_loanType_details = new Transaction_LoanType_Details();
                                Bundle bundles = new Bundle();
//                                bundles.putString("stepwise", EShaktiApplication.flag);
                                bundles.putString("loan_id", grploanDetails.get(0).getLoanId());
                                bundles.putString("loan_type", grploanDetails.get(0).getLoanTypeName());
                                bundles.putString("account_number", grploanDetails.get(0).getAccountNumber());
                                bundles.putString("bank_name", grploanDetails.get(0).getBankName());
                                bundles.putString("outstanding", grploanDetails.get(0).getLoanOutstanding());
                                transaction_loanType_details.setArguments(bundles);
                                insertGroupLoanData();
                                NewDrawerScreen.showFragment(transaction_loanType_details);
                                dialog.dismiss();

                            } else {
                                MinutesOFMeeting minutesOFMeeting = new MinutesOFMeeting();
//                                Bundle bundles = new Bundle();
//                                bundles.putString("stepwise", EShaktiApplication.flag);
//                                minutesOFMeeting.setArguments(bundles);
                                insertGroupLoanData();
                                NewDrawerScreen.showFragment(minutesOFMeeting);
                                dialog.dismiss();
                            }
                        }
                        else{
                            MinutesOFMeeting minutesOFMeeting = new MinutesOFMeeting();
//                                Bundle bundles = new Bundle();
//                                bundles.putString("stepwise", EShaktiApplication.flag);
//                                minutesOFMeeting.setArguments(bundles);
                                insertGroupLoanData();
                                NewDrawerScreen.showFragment(minutesOFMeeting);
                                dialog.dismiss();
                            //TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                                    //TastyToast.WARNING);
                        }
                    }
                }
                else {

                    if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

                        RestClient.getRestClient(Transaction_LoanType_Details.this).callRestWebService(Constants.BASE_URL + Constants.GROUPLOANREPAYMENT_END, transaction_loan_end, getActivity(), ServiceType.GROUP_LOAN_REPAYMENT_END);
                    } else {
                        if (TransactionTable.getLoginFlag(AppStrings.grouploanrepayment).size() <= 0 || (!TransactionTable.getLoginFlag(AppStrings.grouploanrepayment).get(TransactionTable.getLoginFlag(AppStrings.grouploanrepayment).size() - 1).getLoginFlag().equals("1")))
                            insertGroupLoanData();
                        else
                            TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);
                    }
                }

            }
        });

    }

    private void insertGroupLoanData() {

        try {

            Log.i("print","insertGroupLoanData() Function Called");
            int value = (MySharedPreference.readInteger(getActivity(), MySharedPreference.GRP_COUNT, 0) + 1);
            if (value > 0)
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.GRP_COUNT, value);

            offlineDBata.setGrpCount(value + "");
            TransactionTable.insertTransGRData(offlineDBata);


            Log.i("print","shgDto.getFFlag() : "+shgDto.getFFlag());
            if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
            }


            updateCIH();
            updateGRPLOS();

            FragmentManager fm = getFragmentManager();
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            dialog.dismiss();
            TastyToast.makeText(getActivity(), "Transaction Completed",
                    TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
            NewDrawerScreen.showFragment(new MainFragment());


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void insertTransaction_tdy()
    {
        offlineDBata.setIs_transaction_tdy("1.0");
        offlineDBata.setShgId(shgDto.getShgId());
        SHGTable.updateIstransaction(offlineDBata);

    }

    private void updateCIH() {
        Log.i("print","updateCIH() Function Called");
        String cihstr = "", cabstr = "", lastTranstr = "";
        cihstr = offlineDBata.getCashInhand();
        cabstr = offlineDBata.getCashAtBank();
        lastTranstr = offlineDBata.getLastTransactionDateTime();
        CashOfGroup csg = new CashOfGroup();
        csg.setCashInHand(cihstr);
        csg.setCashAtBank(cabstr);
        csg.setLastTransactionDate(lastTranstr);
        SHGTable.updateSHGDetails(csg, shgDto.getId());
    }

//    private void updateBKCB(OfflineDto dto) {
//        if (dto.getCurr_balance() != null || dto.getCurr_balance().length() > 0) {
//            SHGTable.updateBankCurrentBalanceDetails(dto);
//        }
//    }
    private void updateGRPLOS() {
        Log.i("print","updateGRPLOS() Function Called");
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBata.getGrouploan_Outstanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, loan_id);


        Log.i("print","ModeofCash : "+offlineDBata.getModeOCash());
        if (!offlineDBata.getModeOCash().equals("2"))
            SHGTable.updateSelectedBankCurrentBalanceDetails(offlineDBata, SeedFund.selectedSBAcId);
    }

    public static void showFragment(Fragment fragment) {

        FragmentTransaction trans = fm.beginTransaction();
        trans.replace(R.id.static_frame, fragment);
        trans.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out);
        trans.show(fragment).commit();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        materialSpinner.setOnItemSelectedListener(this);

//        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
//            String shgid_spinner = "45c8f633-22ac-43f4-8f5b-ae307594f1ef";
//            RestClient.getRestClient(Transaction_LoanType_Details.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.TRANSACTION_LOANBANKSPINNERITEM +shgId, getActivity(), ServiceType.TRANSACTION_LOAN_BANK_SPINNER);
//        } else {
//            Utils.showToast(getActivity(), "Network Not Available");
//        }

    }

//    @Override
//    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//        try {
//
//            SavingsAccounts sb = (SavingsAccounts) adapterView.getItemAtPosition(i);
//            str_materialSpinner = sb.getBankName();
//            bank_id =  sb.getId();
//
//
////               currentbalance = bankitemdto.getResponseContent().getSavingsAccounts().get(i).getCurrentBalance();
//            //  bank_id = bankitemdto.getResponseContent().getSavingsAccounts().get(i).getId();
//
//            if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
////            String shgid_spinner = "45c8f633-22ac-43f4-8f5b-ae307594f1ef";
//                RestClient.getRestClient(Transaction_LoanType_Details.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.SPINNER_BANK_ITEM_AMOUNT +bank_id, getActivity(), ServiceType.SPINNER_BANK_ITEM_AMOUNT_TYPE);
//            } else {
//                Utils.showToast(getActivity(), "Network Not Available");
//            }
////               current_bal =(int)Double.parseDouble(currentbalance);
//
//            Log.d("bank_id",""+bank_id);
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//
//    }

//    @Override
//    public void onNothingSelected(AdapterView<?> adapterView) {
//
//    }


    @Override
    public void onResume() {
        super.onResume();

        if(resume_value==true)
        {
            if (bankitemdto.getStatusCode() == (Utils.Success_Code)) {
//                if (EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1"))
                insertTransaction_tdy();

                if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") )
                {
                    Utils.loanTypes.remove(0);

                    if (Utils.loanTypes.size() > 0) {

                        Transaction_LoanType_Details transaction_loanType_details = new Transaction_LoanType_Details();
                        Bundle bundles = new Bundle();
//                        bundles.putString("stepwise", EShaktiApplication.flag);
                        bundles.putString("loan_id", Utils.loanTypes.get(0).getLoanId());
                        bundles.putString("loan_type", Utils.loanTypes.get(0).getLoanTypeName());
                        bundles.putString("account_number", Utils.loanTypes.get(0).getAccountNumber());
                        bundles.putString("bank_name", Utils.loanTypes.get(0).getBankName());
                        bundles.putString("outstanding", Utils.loanTypes.get(0).getLoanOutstanding());
                        transaction_loanType_details.setArguments(bundles);
                        NewDrawerScreen.showFragment(transaction_loanType_details);
                        updateGRPLOS();
                        dialog.dismiss();

                    } else {
                        MinutesOFMeeting minutesOFMeeting = new MinutesOFMeeting();
//                        Bundle bundles = new Bundle();
//                        bundles.putString("stepwise", EShaktiApplication.flag);
//                        minutesOFMeeting.setArguments(bundles);
                        NewDrawerScreen.showFragment(minutesOFMeeting);
                        updateGRPLOS();
                        dialog.dismiss();
                    }
                } else {
                    updateCIH();
                    updateGRPLOS();

                    MainFragment mainFragment = new MainFragment();
                    Bundle bundles = new Bundle();
                    bundles.putString("Transaction", MainFragment.Flag_Transaction);
                    mainFragment.setArguments(bundles);
                    NewDrawerScreen.showFragment(mainFragment);
                    Transaction_LoanType_Details.showFragment(mainFragment);
                    dialog.dismiss();
                    TastyToast.makeText(getActivity(), "Transaction Completed",
                            TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                }
            }
            else {
                Utils.showToast(getActivity(), bankitemdto.getMessage());
            }


        }

    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        switch (serviceType) {

            case TRANSACTION_LOAN_BANK_SPINNER:
                try {
                    Log.d("getDetails", " " + result.toString());
                    bankitemdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    if (bankitemdto.getStatusCode() == (Utils.Success_Code)) {
                        Utils.showToast(getActivity(), bankitemdto.getMessage());

                        ArrayList<SavingsAccounts> mAccountsArrayList = new ArrayList<>();
                        for (int i = 0; i < bankitemdto.getResponseContent().getSavingsAccounts().size(); i++) {
                            accounts = new SavingsAccounts();
                            accounts.setId(bankitemdto.getResponseContent().getSavingsAccounts().get(i).getId());
                            accounts.setBankName(bankitemdto.getResponseContent().getSavingsAccounts().get(i).getBankName());
                            // accounts.setCurrentBalance(bankitemdto.getResponseContent().getSavingsAccounts().get(i).getId());
                            mAccountsArrayList.add(accounts);
                            materialSpinner.setAdapter(new Grouploan_Adapter(getActivity(), mAccountsArrayList));
                        }

//                       if(bankitemdto!=null && bankitemdto.getResponseContent()!=null && bankitemdto.getResponseContent().getSavingsAccounts()!=null && bankitemdto.getResponseContent().getSavingsAccounts().size()>0) {
//                           materialSpinner.setAdapter(new Grouploan_Adapter(getActivity(), bankitemdto.getResponseContent().getSavingsAccounts()));
//                       }

                    } else {
                        if (bankitemdto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case GROUP_LOAN_REPAYMENT_END:
                try {
                    Log.d("getDetails", " " + result);
                    bankitemdto = new Gson().fromJson(result, ResponseDto.class);

                    if (bankitemdto.getStatusCode() == (Utils.Success_Code)) {
                        resume_value=true;
//                        if (EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1"))
                        Log.i("print","EShaktiApplication.flag : "+EShaktiApplication.flag);
                        if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") )
                        {
                            Utils.loanTypes.remove(0);

                            if (Utils.loanTypes.size() > 0) {

                                Transaction_LoanType_Details transaction_loanType_details = new Transaction_LoanType_Details();
                                Bundle bundles = new Bundle();
//                                bundles.putString("stepwise", EShaktiApplication.flag);
                                bundles.putString("loan_id", Utils.loanTypes.get(0).getLoanId());
                                bundles.putString("loan_type", Utils.loanTypes.get(0).getLoanTypeName());
                                bundles.putString("account_number", Utils.loanTypes.get(0).getAccountNumber());
                                bundles.putString("bank_name", Utils.loanTypes.get(0).getBankName());
                                bundles.putString("outstanding", Utils.loanTypes.get(0).getLoanOutstanding());
                                transaction_loanType_details.setArguments(bundles);
                                NewDrawerScreen.showFragment(transaction_loanType_details);
                                updateGRPLOS();
                                dialog.dismiss();

                            } else {
                                MinutesOFMeeting minutesOFMeeting = new MinutesOFMeeting();
//                                Bundle bundles = new Bundle();
//                                bundles.putString("stepwise", EShaktiApplication.flag);
//                                minutesOFMeeting.setArguments(bundles);
                                NewDrawerScreen.showFragment(minutesOFMeeting);
                                updateGRPLOS();
                                dialog.dismiss();
                            }
                        } else {
                            updateCIH();
                            updateGRPLOS();

                            MainFragment mainFragment = new MainFragment();
                            Bundle bundles = new Bundle();
                            bundles.putString("Transaction", MainFragment.Flag_Transaction);
                            mainFragment.setArguments(bundles);
                            NewDrawerScreen.showFragment(mainFragment);
                            Transaction_LoanType_Details.showFragment(mainFragment);
                            dialog.dismiss();
                            TastyToast.makeText(getActivity(), "Transaction Completed",
                                    TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                            Log.d("TastyToast", " " + result);
                        }
                    }
                    else {
                        Utils.showToast(getActivity(), bankitemdto.getMessage());
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case NAV_DETAILS:
                try {
                    if (result != null && result.length() > 0) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        ResponseDto mrDto = gson.fromJson(result, ResponseDto.class);
                        int statusCode = mrDto.getStatusCode();
                        Log.d("Main Frag response ", " " + statusCode);
                        if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                            // showMessage(statusCode);

                        } else if (statusCode == Utils.Success_Code) {

                            csGrp = mrDto.getResponseContent().getCashOfGroup();
                           /* String cash_in_hand = mrDto.getResponseContent().getCashOfGroup().get(0).cashInHand;
                            String cash_at_bank = mrDto.getResponseContent().getCashOfGroup().get(0).cashAtBank;
                            con_cash_in_hand = (int) Double.parseDouble(cash_in_hand);
                            con_cash_at_bank = (int) Double.parseDouble(cash_at_bank);
                            cashhand.setText(String.valueOf(con_cash_in_hand));
                            cashbank.setText(String.valueOf(con_cash_at_bank));*/
                            String cashand = "";
                            String cashbanks = "";
                            for (int i = 0; i <csGrp.size() ; i++) {

                                cashand =csGrp.get(i).getCashInHand();
                                cashbanks=csGrp.get(i).getCashAtBank();

                            }
                            int csahhand_bal = (int) Double.parseDouble(cashand);
                            int csahbank_bal = (int) Double.parseDouble(cashbanks);
//                            cashhand.setText(AppStrings.cashinhand +  csahhand_bal);
//                            cashbank.setText("CASH AT BANK: "+csahbank_bal);
                            cashhand.setText(AppStrings.cashinhand + csahhand_bal);
                            cashbank.setText(AppStrings.cashatBank + csahbank_bal);

                            SHGTable.updateSHGDetails(csGrp.get(0), shgDto.getId());
                            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case SPINNER_BANK_ITEM_AMOUNT_TYPE:
                bankitemdto = new Gson().fromJson(result, ResponseDto.class);
                if (bankitemdto.getStatusCode() == (Utils.Success_Code)) {
                    Utils.showToast(getActivity(), bankitemdto.getMessage());
                    bank_bal = bankitemdto.getResponseContent().getSavingsBalance().getCurrentBalance();
                    Toast.makeText(getActivity(), bank_bal, Toast.LENGTH_LONG).show();
                    current_bal = (int) Double.parseDouble(bank_bal);
                    amount.setText("Available Balance:" + String.valueOf(current_bal));
                    Log.d("amount", String.valueOf(current_bal));
                }
                break;


        }
    }

}

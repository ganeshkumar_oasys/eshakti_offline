package com.oasys.eshakti.fragment;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.gson.Gson;
import com.oasys.eshakti.Adapter.ProfileNumberAdapter;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.Dto.MemberMobileNumbersDTOList;
import com.oasys.eshakti.Dto.MobileNumberRequestDto;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.MemberTable;
import com.oasys.eshakti.Service.NewTaskListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Profile_MobileNumber_Updation extends Fragment implements NewTaskListener{

    private RecyclerView mProfileList;
    private ProfileNumberAdapter profileNumberAdapter;
    private LinearLayoutManager linearLayoutManager;
    private Button button_click;

    static List<MemberList> profileNumberDto;
    ArrayList<MemberMobileNumbersDTOList> memberMobileNumbersDTOLists;
    private View rootView;
    private List<MemberList> finalMemberNoUpdateList;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile__mobile_number__updation, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        //  RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.PROFILE_GET_MOBILE_NUMBER, getActivity(), ServiceType.PROFILE_GET_NUMBER);

        profileNumberDto = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        mProfileList = (RecyclerView) rootView.findViewById(R.id.mProfileNumberUpdaterecyclerview);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        mProfileList.setLayoutManager(linearLayoutManager);
        mProfileList.setHasFixedSize(true);
        memberMobileNumbersDTOLists = new ArrayList<>();
        button_click = (Button) rootView.findViewById(R.id.button_click);

        button_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int i = 0; i < profileNumberDto.size(); i++) {
                    MemberMobileNumbersDTOList memberMobileNumbersDTOList = new MemberMobileNumbersDTOList();
                    memberMobileNumbersDTOList.setMobileNumber(profileNumberDto.get(i).getPhoneNumber());
                    memberMobileNumbersDTOList.setName(profileNumberDto.get(i).getMemberName());
                    memberMobileNumbersDTOList.setUserId(profileNumberDto.get(i).getMemberUserId());
                    memberMobileNumbersDTOList.setShgId(profileNumberDto.get(i).getShgId());
                    memberMobileNumbersDTOList.setMemberId(profileNumberDto.get(i).getMemberId());
                    memberMobileNumbersDTOLists.add(memberMobileNumbersDTOList);
                    Log.d("NumbersDTOLists", " " + memberMobileNumbersDTOLists.size());
                }
                MobileNumberRequestDto mobileNumberRequestDto = new MobileNumberRequestDto();
                mobileNumberRequestDto.setMemberMobileNumbersDTOList(memberMobileNumbersDTOLists);
                String checkData = new Gson().toJson(mobileNumberRequestDto);
                Log.d("NumbersDTOLists", " " + checkData);
                RestClient.getRestClient(Profile_MobileNumber_Updation.this).callRestWebServiceForPutMethod(Constants.BASE_URL + Constants.PROFILE_UPDATE_MOBILE_NUMBER, checkData, getActivity(), ServiceType.PROFILE_NUMBER_UPDATE);

            }
        });

        profileNumberAdapter = new ProfileNumberAdapter(getActivity(), profileNumberDto);
        mProfileList.setAdapter(profileNumberAdapter);

    }


    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        switch (serviceType) {

            case PROFILE_NUMBER_UPDATE:
                Log.d("getDetails", " " + result.toString());
                try {
                    JSONObject jsonObject = new JSONObject(result.toString());
                    String statusCode = jsonObject.getString("statusCode");
                    String message = jsonObject.getString("message");
                    if (statusCode.equals("200")) {



                        Utils.showToast(getActivity(), message);
                        FragmentManager fm = getFragmentManager();
                        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        NewDrawerScreen.showFragment(new MainFragment());
                        //  getActivity().finish();
                    } else {
                        if (statusCode.equals(401)) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }


}

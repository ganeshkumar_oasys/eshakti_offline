package com.oasys.eshakti.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Dialogue.Dialog_New_TransactionDate;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.Dto.MemberMobileNumbersDTOList;
import com.oasys.eshakti.Dto.MobileNumberRequestDto;
import com.oasys.eshakti.Dto.OfflineDto;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.GetSpanText;
import com.oasys.eshakti.OasysUtils.MobilenumberUtils;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.RegionalConversion;
import com.oasys.eshakti.OasysUtils.Reset;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.MemberTable;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.views.Get_EdiText_Filter;
import com.oasys.eshakti.views.RaisedButton;
import com.oasys.eshakti.views.TextviewUtils;
import com.tutorialsee.lib.TastyToast;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProfileMobileUpdate extends Fragment implements View.OnClickListener, NewTaskListener {

    private View rootView;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView mHeader, mAutoFill_label, mMemberName;
    private CheckBox mAutoFill;
    private NetworkConnection networkConnection;
    private List<MemberList> memList;
    private ArrayList<MemberList> arrMem;
    private ListOfShg shgDto;
    private int mSize;
    private EditText mMobileNumber_values;
    private static List<EditText> sMobilenumberFields;
    private LinearLayout mMemberNameLayout;
    private TableLayout mMobileNumberTable;
    private Button mSubmit_Raised_Button;
    private RaisedButton mEdit_RaisedButton;
    private RaisedButton mOk_RaisedButton;
    private Dialog confirmationDialog;
    private Dialog mProgressDilaog;
    public static String sSendToServer_MemberMobileNumber;
    public static int sIncome_Total;
    ArrayList<MemberMobileNumbersDTOList> memberMobileNumbersDTOLists;
    OfflineDto offline =new OfflineDto();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sSendToServer_MemberMobileNumber = Reset.reset(sSendToServer_MemberMobileNumber);
        sIncome_Total = 0;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_internalloan_disbursement, container, false);
        return rootView;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        arrMem = new ArrayList<>();
        memberMobileNumbersDTOLists = new ArrayList<>();
        sMobilenumberFields = new ArrayList<EditText>();
        init();
    }

    private void init() {
        mGroupName = (TextView) rootView.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);

        mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);

        mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashatBank.setTypeface(LoginActivity.sTypeface);
        /** UI Mapping **/

        mHeader = (TextView) rootView.findViewById(R.id.internal_fragmentHeader);
        // mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.mInternalTypeLoan));
        mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.mMobileNoUpdation));
        mHeader.setTypeface(LoginActivity.sTypeface);

        mAutoFill_label = (TextView) rootView.findViewById(R.id.internal_autofillLabel);
        mAutoFill_label.setText(RegionalConversion.getRegionalConversion(AppStrings.autoFill));
        mAutoFill_label.setTypeface(LoginActivity.sTypeface);
        mAutoFill_label.setVisibility(View.GONE);

        mAutoFill = (CheckBox) rootView.findViewById(R.id.internal_autoFill);
        mAutoFill.setVisibility(View.GONE);
        mAutoFill.setOnClickListener(this);

        mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
        mMemberName = (TextView) rootView.findViewById(R.id.member_name);

        TableLayout headerTable = (TableLayout) rootView.findViewById(R.id.internal_savingsTable);

        mMobileNumberTable = (TableLayout) rootView.findViewById(R.id.internal_fragment_contentTable);

        TableRow savingsHeader = new TableRow(getActivity());
        savingsHeader.setBackgroundResource(R.color.tableHeader);
        ViewGroup.LayoutParams headerParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,
                1f);
        TextView mMemberName_headerText = new TextView(getActivity());
        mMemberName_headerText
                .setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
        mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
        mMemberName_headerText.setTextColor(Color.WHITE);
        mMemberName_headerText.setPadding(20, 5, 10, 5);
        mMemberName_headerText.setLayoutParams(headerParams);
        savingsHeader.addView(mMemberName_headerText);

        TextView mIncomeAmount_HeaderText = new TextView(getActivity());
        mIncomeAmount_HeaderText
                .setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mMobileNo)));
        mIncomeAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
        mIncomeAmount_HeaderText.setTextColor(Color.WHITE);
        mIncomeAmount_HeaderText.setPadding(10, 5, 40, 5);
        mIncomeAmount_HeaderText.setLayoutParams(headerParams);
        mIncomeAmount_HeaderText.setBackgroundResource(R.color.tableHeader);
        savingsHeader.addView(mIncomeAmount_HeaderText);

        headerTable.addView(savingsHeader,new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        for (int i = 0; i < mSize; i++) {

            TableRow indv_IncomeRow = new TableRow(getActivity());

            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 5, 10, 5);

            final TextView memberName_Text = new TextView(getActivity());
            memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                    String.valueOf(memList.get(i).getMemberName())));
            memberName_Text.setTypeface(LoginActivity.sTypeface);
            memberName_Text.setTextColor(R.color.black);
            memberName_Text.setPadding(10, 0, 10, 5);
            memberName_Text.setLayoutParams(contentParams);
            memberName_Text.setWidth(200);
            memberName_Text.setSingleLine(true);
            memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
            indv_IncomeRow.addView(memberName_Text);

            TableRow.LayoutParams contentEditParams = new TableRow.LayoutParams(150, ViewGroup.LayoutParams.WRAP_CONTENT);
            contentEditParams.setMargins(30, 5, 100, 5);

            mMobileNumber_values = new EditText(getActivity());

            mMobileNumber_values.setId(i);
            sMobilenumberFields.add(mMobileNumber_values);
            mMobileNumber_values.setGravity(Gravity.END);
            mMobileNumber_values.setTextColor(Color.BLACK);
            mMobileNumber_values.setPadding(5, 5, 5, 5);
            mMobileNumber_values.setBackgroundResource(R.drawable.edittext_background);
            mMobileNumber_values.setLayoutParams(contentEditParams);// contentParams
            // lParams
            mMobileNumber_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
            mMobileNumber_values.setFilters(Get_EdiText_Filter.editText_mobile_number_filter());
            mMobileNumber_values.setInputType(InputType.TYPE_CLASS_NUMBER);
            mMobileNumber_values.setTextColor(R.color.black);
            if(memList.get(i).getPhoneNumber().equals("0"))
            {
                mMobileNumber_values.setText("-");
            }
            else {
                mMobileNumber_values.setText(memList.get(i).getPhoneNumber());

            }
            mMobileNumber_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    // TODO Auto-generated method stub
                    if (hasFocus) {
                        ((EditText) v).setGravity(Gravity.LEFT);

                        mMemberNameLayout.setVisibility(View.VISIBLE);
                        mMemberName.setText(memberName_Text.getText().toString().trim());
                        mMemberName.setTypeface(LoginActivity.sTypeface);
                        TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
                    } else {
                        ((EditText) v).setGravity(Gravity.RIGHT);
                        mMemberNameLayout.setVisibility(View.GONE);
                        mMemberName.setText("");
                    }

                }
            });
            indv_IncomeRow.addView(mMobileNumber_values);

            mMobileNumberTable.addView(indv_IncomeRow,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }

        mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.internal_fragment_Submit_button);
        mSubmit_Raised_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
        mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
        mSubmit_Raised_Button.setOnClickListener(this);

    }

    private static String mMobilenumberValues[];

    @Override
    public void onClick(View view) {
        mMobilenumberValues = new String[sMobilenumberFields.size()];
        //  Log.d(TAG, "Member Mobile number size ------>>> : " + mMobilenumberValues.length);
        switch (view.getId()) {
            case R.id.internal_fragment_Submit_button:
                boolean _MasterisValid = true;
                boolean _IsEmpty = true;
                boolean _IsRepeatedMobileNo = false;
                sSendToServer_MemberMobileNumber = "0";
                offline.setIs_transaction_tdy("1.0");
                try {
                    for (int i = 0; i < mMobilenumberValues.length; i++) {
                        boolean isValid = false;
                        mMobilenumberValues[i] = String.valueOf(sMobilenumberFields.get(i).getText());
                        //Log.d(TAG, "Entered Values : " + mMobilenumberValues[i] + " POS : " + i);



                        if (!sMobilenumberFields.get(i).getText().toString().isEmpty()) {
                            _IsEmpty = false;
                        }

                        isValid = MobilenumberUtils.isValidMobile(mMobilenumberValues[i]);

                        if (!isValid) {
                            sMobilenumberFields.get(i).setError(RegionalConversion.getRegionalConversion(AppStrings.mInvalidMobileNo));
                            _MasterisValid = false;
                        }else{
                            memList.get(i).setPhoneNumber(mMobilenumberValues[i]);
                        }
                    }

                    for (int j = 0; j < mMobilenumberValues.length; j++) {
                        String mobileNo = mMobilenumberValues[j];

                        for (int k = 0; k < mMobilenumberValues.length; k++) {
                            if (j != k) {
                                if (!mMobilenumberValues[k].isEmpty()) {
                                    if (mMobilenumberValues[k].equals(mobileNo)) {
                                        _IsRepeatedMobileNo = true;
                                    }
                                }

                            }

                        }
                    }

                    if (_MasterisValid && !_IsEmpty && !_IsRepeatedMobileNo) {


                        callConfirmationDialog();

                    } else {
                        if (_IsRepeatedMobileNo) {
                            TastyToast.makeText(getActivity(), AppStrings.mIsMobileNoRepeat, TastyToast.LENGTH_SHORT,
                                    TastyToast.ERROR);
                        } else {
                            TastyToast.makeText(getActivity(), AppStrings.mCheckValidMobileNo, TastyToast.LENGTH_SHORT,
                                    TastyToast.ERROR);
                        }

                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

                break;

            case R.id.fragment_Edit:

                sSendToServer_MemberMobileNumber = "0";
                sIncome_Total = Integer.valueOf("0");
                mSubmit_Raised_Button.setClickable(true);
                confirmationDialog.dismiss();
                break;

            case R.id.frag_Ok:
                confirmationDialog.dismiss();

                memberMobileNumbersDTOLists.clear();

                for (int i = 0; i < memList.size(); i++) {
                    if (!memList.get(i).getPhoneNumber().isEmpty()){
                        MemberMobileNumbersDTOList memberMobileNumbersDTOList = new MemberMobileNumbersDTOList();
                        memberMobileNumbersDTOList.setMobileNumber(memList.get(i).getPhoneNumber());
                        memberMobileNumbersDTOList.setName(memList.get(i).getMemberName());
                        memberMobileNumbersDTOList.setUserId(memList.get(i).getMemberUserId());
                        memberMobileNumbersDTOList.setShgId(memList.get(i).getShgId());
                        memberMobileNumbersDTOList.setMemberId(memList.get(i).getMemberId());
                        memberMobileNumbersDTOLists.add(memberMobileNumbersDTOList);
                        Log.d("NumbersDTOLists", " " + memberMobileNumbersDTOLists.size());
                    }
                }
                MobileNumberRequestDto mobileNumberRequestDto = new MobileNumberRequestDto();
                mobileNumberRequestDto.setMemberMobileNumbersDTOList(memberMobileNumbersDTOLists);
                String checkData = new Gson().toJson(mobileNumberRequestDto);
                Log.d("NumbersDTOLists", " " + checkData);

                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();
                    RestClient.getRestClient(this).callRestWebServiceForPutMethod(Constants.BASE_URL + Constants.PROFILE_UPDATE_MOBILE_NUMBER, checkData, getActivity(), ServiceType.PROFILE_NUMBER_UPDATE);
//                    RestClient.getRestClient(Profile_MobileNumber_Updation.this).callRestWebServiceForPutMethod(Constants.BASE_URL + Constants.PROFILE_UPDATE_MOBILE_NUMBER, checkData, getActivity(), ServiceType.PROFILE_NUMBER_UPDATE);
                }

                break;

        }
    }

    private void insertTransaction_tdy()
    {

        offline.setIs_transaction_tdy("1.0");
        offline.setShgId(shgDto.getShgId());
            SHGTable.updateIstransaction(offline);

    }
    private void callConfirmationDialog() {
        // TODO Auto-generated method stub

        confirmationDialog = new Dialog(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
        dialogView.setLayoutParams(
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
        confirmationHeader.setTypeface(LoginActivity.sTypeface);

        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

        DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
        Date d = new Date(Long.parseLong(shgDto.getLastTransactionDate()));
        String dateStr = simple.format(d);
        TextView transactdate = (TextView)dialogView.findViewById(R.id.transactdate);
        transactdate.setText(dateStr);

        for (int i = 0; i < mSize; i++) {

            TableRow indv_SavingsRow = new TableRow(getActivity());

            @SuppressWarnings("deprecation")
            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 5, 10, 5);

            TextView memberName_Text = new TextView(getActivity());
            memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                    String.valueOf(memList.get(i).getMemberName())));
            memberName_Text.setTypeface(LoginActivity.sTypeface);
            memberName_Text.setTextColor(R.color.black);
            memberName_Text.setPadding(5, 5, 5, 5);
            memberName_Text.setLayoutParams(contentParams);
            indv_SavingsRow.addView(memberName_Text);

            TextView confirm_values = new TextView(getActivity());
            confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mMobilenumberValues[i])));
            confirm_values.setTextColor(R.color.black);
            confirm_values.setPadding(5, 5, 5, 5);
            confirm_values.setGravity(Gravity.RIGHT);
            confirm_values.setLayoutParams(contentParams);
            indv_SavingsRow.addView(confirm_values);

            confirmationTable.addView(indv_SavingsRow,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }

        mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit);
        mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mEdit_RaisedButton.setOnClickListener(this);

        mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.frag_Ok);
        mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mOk_RaisedButton.setOnClickListener(this);

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialogView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();

        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
        margin.leftMargin = 10;
        margin.rightMargin = 10;
        margin.topMargin = 10;
        margin.bottomMargin = 10;
        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
            }
            switch (serviceType) {
                case PROFILE_NUMBER_UPDATE:
                    try {
                        ResponseDto cdto = new Gson().fromJson(result, ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            Utils.showToast(getActivity(), message);
                            insertTransaction_tdy();
                            if (memberMobileNumbersDTOLists != null && memberMobileNumbersDTOLists.size() > 0) {
                                for (MemberMobileNumbersDTOList mem : memberMobileNumbersDTOLists) {
                                    MemberTable.updateMemberMobileNo(mem);
                                }
                            }

                            FragmentManager fm = getFragmentManager();
                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            MainFragment mainFragment = new MainFragment();
                            Bundle bundles = new Bundle();
                            bundles.putString("Profile",MainFragment.Flag_Profile);
                            mainFragment.setArguments(bundles);
                            NewDrawerScreen.showFragment(mainFragment);
//                            NewDrawerScreen.showFragment(new MainFragment());

                        } else {

                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                    mProgressDilaog.dismiss();
                                    mProgressDilaog = null;
                                }
                            }
                            Utils.showToast(getActivity(), message);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }

        }
    }
}

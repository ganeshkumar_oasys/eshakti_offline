package com.oasys.eshakti.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

//import com.google.zxing.integration.android.IntentIntegrator;
//import com.google.zxing.integration.android.IntentResult;
//import com.kinda.alert.KAlertDialog;
import com.oasys.eshakti.Adapter.MemberreportAdapter;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.OasysUtils.AadharCard;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.DataAttributes;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.QrCodeException;
import com.oasys.eshakti.OasysUtils.RecyclerItemClickListener;
import com.oasys.eshakti.R;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.SHGTable;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UploadAadhaar_PhotoFrag extends Fragment implements View.OnClickListener {

    private RecyclerView recyclerViewMemberReport;
    private LinearLayoutManager linearLayoutManager;
    private static FragmentManager fm;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mHeader, mCashatBank;
    private String getmem_id;
    private Dialog confirmationDialog;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    private TextView mTransactiontext;
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    AadharCard aadharData;




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_member_report, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        recyclerViewMemberReport = (RecyclerView) view.findViewById(R.id.recyclerViewMemberReport);
        mGroupName = (TextView) view.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);

        mCashinHand = (TextView) view.findViewById(R.id.ch);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);

        mCashatBank = (TextView) view.findViewById(R.id.cb);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashatBank.setTypeface(LoginActivity.sTypeface);

        mHeader = (TextView) view.findViewById(R.id.fragmentHeader);
        mHeader.setText(AppStrings.Memberreports);
        mHeader.setTypeface(LoginActivity.sTypeface);


        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewMemberReport.setLayoutManager(linearLayoutManager);
        recyclerViewMemberReport.setHasFixedSize(true);


        Bundle bundle = getArguments();
        getmem_id = bundle.getString("memid");
        Log.d("Mem", getmem_id);

        memList = new ArrayList<>();

        MemberList ml = new MemberList();
        ml.setMemberName(AppStrings.uploadAadhar);
        memList.add(ml);
        MemberList ml1 = new MemberList();
        ml1.setMemberName(AppStrings.uploadPhoto);
        memList.add(ml1);

        MemberreportAdapter memberreportAdapter = new MemberreportAdapter(getActivity(), memList);
        recyclerViewMemberReport.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerViewMemberReport.setAdapter(memberreportAdapter);


        fm = getActivity().getSupportFragmentManager();

        recyclerViewMemberReport.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (memList.get(position).getMemberName().equals(AppStrings.uploadAadhar)) {
                    confirmationDialogg();
                } else {
                    UploadPhotoFragment reportLoanMenu = new UploadPhotoFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("memid", getmem_id);
                    Log.d("Mem1", getmem_id);
                    reportLoanMenu.setArguments(bundle);
                    NewDrawerScreen.showFragment(reportLoanMenu);
                }
            }
        }));


        return view;
    }

    private void confirmationDialogg() {

        confirmationDialog = new Dialog(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
        dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText(AppStrings.confirmation);
        confirmationHeader.setGravity(Gravity.CENTER_HORIZONTAL);
        confirmationHeader.setTypeface(LoginActivity.sTypeface);

        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
        mEdit_RaisedButton.setVisibility(View.GONE);

        mTransactiontext = (TextView)dialogView.findViewById(R.id.transactiontext);
        mTransactiontext.setText(AppStrings.show);


        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
        mOk_RaisedButton.setText(AppStrings.yes);
        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mOk_RaisedButton.setOnClickListener(this);

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialogView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();

    }


    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.frag_Ok:
//                showQRCodeScanner();
        }
    }


    public boolean checkCameraPermission (){
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
            return false;
        }
        return true;
    }

   /* private void showQRCodeScanner() {

        // we need to check if the user has granted the camera permissions
        // otherwise scanner will not work
        try {

            if (!checkCameraPermission()) {
                return;
            }

            IntentIntegrator integrator = new IntentIntegrator(getActivity());

            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
            integrator.setPrompt("Scan a Aadharcard QR Code");
            integrator.setResultDisplayDuration(500);
            integrator.setCameraId(0);  // Use a specific camera of the device
            integrator.initiateScan();


        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //retrieve scan result
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);

        if (scanningResult != null) {
            //we have a result
            String scanContent = scanningResult.getContents();
            String scanFormat = scanningResult.getFormatName();

            // process received data
            if(scanContent != null && !scanContent.isEmpty()){
                processScannedData(scanContent);
            }else{
                showWarningPrompt("Scan Cancelled");
            }

        }else{
            showWarningPrompt("No scan data received!");
        }
    }

    public void showErrorPrompt(String message){
        new KAlertDialog(getActivity(), KAlertDialog.ERROR_TYPE)
                .setTitleText("Error")
                .setContentText(message)
                .show();


    }

    public void showSuccessPrompt(String message){
        new KAlertDialog(getActivity(), KAlertDialog.SUCCESS_TYPE)
                .setTitleText("Success")
                .setContentText(message)
                .show();
    }

    public void showWarningPrompt(String message){
        new KAlertDialog(getActivity(), KAlertDialog.WARNING_TYPE)
                .setContentText(message)
                .show();
    }

    protected void processScannedData(String scanData){
        // check if the scanned string is XML
        // This is to support old QR codes

        if(isXml(scanData)){
            XmlPullParserFactory pullParserFactory;

            try {
                // init the parserfactory
                pullParserFactory = XmlPullParserFactory.newInstance();
                // get the parser
                XmlPullParser parser = pullParserFactory.newPullParser();

                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(new StringReader(scanData));
                aadharData = new AadharCard();

                // parse the XML
                int eventType = parser.getEventType();
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if(eventType == XmlPullParser.START_DOCUMENT) {
                        Log.d("Rajdeol","Start document");
                    } else if(eventType == XmlPullParser.START_TAG && DataAttributes.AADHAAR_DATA_TAG.equals(parser.getName())) {
                        // extract data from tag
                        //uid
                        aadharData.setUuid(parser.getAttributeValue(null,DataAttributes.AADHAR_UID_ATTR));
                        //name
                        aadharData.setName(parser.getAttributeValue(null,DataAttributes.AADHAR_NAME_ATTR));
                        //gender
                        aadharData.setGender(parser.getAttributeValue(null,DataAttributes.AADHAR_GENDER_ATTR));
                        // year of birth
                        aadharData.setDateOfBirth(parser.getAttributeValue(null,DataAttributes.AADHAR_DOB_ATTR));
                        // care of
                        aadharData.setCareOf(parser.getAttributeValue(null,DataAttributes.AADHAR_CO_ATTR));
                        // village Tehsil
                        aadharData.setVtc(parser.getAttributeValue(null,DataAttributes.AADHAR_VTC_ATTR));
                        // Post Office
                        aadharData.setPostOffice(parser.getAttributeValue(null,DataAttributes.AADHAR_PO_ATTR));
                        // district
                        aadharData.setDistrict(parser.getAttributeValue(null,DataAttributes.AADHAR_DIST_ATTR));
                        // state
                        aadharData.setState(parser.getAttributeValue(null,DataAttributes.AADHAR_STATE_ATTR));
                        // Post Code
                        aadharData.setPinCode(parser.getAttributeValue(null,DataAttributes.AADHAR_PC_ATTR));

                    } else if(eventType == XmlPullParser.END_TAG) {
                        Log.d("Rajdeol","End tag "+parser.getName());

                    } else if(eventType == XmlPullParser.TEXT) {
                        Log.d("Rajdeol","Text "+parser.getText());

                    }
                    // update eventType
                    eventType = parser.next();
                }

                // display the data on screen
//                displayScannedData();
                return;
            } catch (XmlPullParserException e) {
                showErrorPrompt("Error in processing QRcode XML");
                e.printStackTrace();
                return;
            } catch (IOException e) {
                showErrorPrompt(e.toString());
                e.printStackTrace();
                return;
            }
        }

        // process secure QR code
//        processEncodedScannedData(scanData);
    }// EO function
*/

//    protected void processEncodedScannedData(String scanData){
//        try {
//            SecureQrCode decodedData = new SecureQrCode(getActivity(),scanData);
//            aadharData = decodedData.getScannedAadharCard();
//            // display the Aadhar Data
//            showSuccessPrompt("Scanned Aadhar Card Successfully");
////            displayScannedData();
//        } catch (QrCodeException e) {
//            showErrorPrompt(e.toString());
//            e.printStackTrace();
//        }
//    }

    protected boolean isXml (String testString){
        Pattern pattern;
        Matcher matcher;
        boolean retBool = false;

        // REGULAR EXPRESSION TO SEE IF IT AT LEAST STARTS AND ENDS
        // WITH THE SAME ELEMENT
        final String XML_PATTERN_STR = "<(\\S+?)(.*?)>(.*?)</\\1>";

        // IF WE HAVE A STRING
        if (testString != null && testString.trim().length() > 0) {

            // IF WE EVEN RESEMBLE XML
            if (testString.trim().startsWith("<")) {

                pattern = Pattern.compile(XML_PATTERN_STR,
                        Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);

                // RETURN TRUE IF IT HAS PASSED BOTH TESTS
                matcher = pattern.matcher(testString);
                retBool = matcher.matches();
            }
            // ELSE WE ARE FALSE
        }

        return retBool;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}

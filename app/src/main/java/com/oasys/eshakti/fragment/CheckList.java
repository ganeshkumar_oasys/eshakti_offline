package com.oasys.eshakti.fragment;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Dto.CheckListDto;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.Dto.ShgBankDetails;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.database.BankTable;
import com.oasys.eshakti.database.SHGTable;

import java.util.ArrayList;
import java.util.List;

public class CheckList extends Fragment implements NewTaskListener {

    private ListOfShg shgDto;
    private ArrayList<ShgBankDetails> bankdetails;
    private NetworkConnection networkConnection;
    private View rootView;
    private TextView mGroupName, mCashInHand, mCashAtBank, mHeader;
    private static List<CheckBox> mCheckBoxFields = new ArrayList<CheckBox>();
    private Dialog mProgressDilaog;
    private int mSize;
    ArrayList<CheckListDto>checkList = new ArrayList<CheckListDto>();
    private LinearLayout mLayout;
    public static CheckBox sCheckBox[];

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_check_list, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());

//        String shgId="3bda4e50-a2fc-4174-9a03-94e2eed5626f";
        if (networkConnection.isNetworkAvailable()) {
            //  onTaskStarted();
          RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GET_CHECKLIST + shgDto.getShgId(), getActivity(), ServiceType.GET_CHECKLIST);// TODO::
          //  RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GET_CHECKLIST + shgId, getActivity(), ServiceType.GET_CHECKLIST);
        }
        mCheckBoxFields.clear();
        //  checkList.clear();

    }

    private void init() {
        try {
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);

            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);

            mHeader = (TextView) rootView.findViewById(R.id.checkList_fragmentHeader);
            mHeader.setText(AppStrings.mCheckList);
            mHeader.setTypeface(LoginActivity.sTypeface);

            mLayout = (LinearLayout) rootView.findViewById(R.id.linearLayout);
            TableLayout checkListTable = (TableLayout) rootView.findViewById(R.id.checkListTable);

            sCheckBox = new CheckBox[mSize];

           /* for (int i = 0; i < mSize ; i++) {

                LinearLayout linearLayout = new LinearLayout(getActivity());

                TextView memberName = new TextView(getActivity());
                memberName.setText(checkList.get(i).getName());
                memberName.setTypeface(LoginActivity.sTypeface);
                memberName.setPadding(20, 0, 0, 0);
                linearLayout.addView(memberName);

                mLayout.addView(linearLayout);

                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                sCheckBox[i] = new CheckBox(getActivity());
                sCheckBox[i].setChecked(checkList.get(i).isStatus());
                sCheckBox[i].setClickable(false);
                sCheckBox[i].setPadding(50, 0, 10, 0);
                sCheckBox[i].setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                sCheckBox[i].setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                linearLayout.addView(sCheckBox[i]);



            }*/



            for (int i = 0; i < mSize; i++) {

                TableRow checkListRow = new TableRow(getActivity());

                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams.setMargins(10, 5, 10, 5);

                TextView check_list_text = new TextView(getActivity());
                check_list_text.setText(checkList.get(i).getName());
                check_list_text.setTypeface(LoginActivity.sTypeface);
//                check_list_text.setTextColor(color.black);
                check_list_text.setPadding(25, 0, 5, 0);
                check_list_text.setLayoutParams(contentParams);
                checkListRow.addView(check_list_text);

                TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams1.setMargins(30, 5, 10, 5);

                CheckBox sCheckBox = new CheckBox(getActivity());
                sCheckBox.setId(i);
                mCheckBoxFields.add(sCheckBox);
                sCheckBox.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                sCheckBox.setClickable(false);
                sCheckBox.setLayoutParams(contentParams1);
                sCheckBox.setChecked(checkList.get(i).isStatus());
                sCheckBox.setGravity(Gravity.LEFT);
                checkListRow.addView(sCheckBox);

                checkListTable.addView(checkListRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            }


//            TableLayout checkListTable = (TableLayout) rootView.findViewById(R.id.checkListTable);

            //  mSize = checkList.size();
           /* if (checkList != null) {


                // TODO:: BANK TRANSACTION
                TableRow checkListRow = new TableRow(getActivity());

                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams.setMargins(10, 5, 10, 5);

                TextView check_list_text = new TextView(getActivity());
                check_list_text.setText(GetSpanText.getSpanString(getActivity(),
                        String.valueOf(AppStrings.savings)));
                check_list_text.setTypeface(LoginActivity.sTypeface);
                check_list_text.setTextColor(R.color.black);
                check_list_text.setPadding(25, 0, 5, 0);
                check_list_text.setLayoutParams(contentParams);
                checkListRow.addView(check_list_text);

                TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams1.setMargins(30, 5, 10, 5);

                CheckBox sCheckBox = new CheckBox(getActivity());
                sCheckBox.setId(0);
                mCheckBoxFields.add(sCheckBox);
                sCheckBox.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                sCheckBox.setClickable(false);
                sCheckBox.setLayoutParams(contentParams1);
                if (checkList.getSavings().equals("false")) {
                    sCheckBox.setChecked(false);
                } else if (checkList.getSavings().equals("true")) {
                    sCheckBox.setChecked(true);
                }  //TODO::
                sCheckBox.setGravity(Gravity.LEFT);
                checkListRow.addView(sCheckBox);

                checkListTable.addView(checkListRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                // TODO:: INCOME
                TableRow checkListRow1 = new TableRow(getActivity());
                TableRow.LayoutParams contentParams2 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams2.setMargins(10, 5, 10, 5);

                TextView check_list_text1 = new TextView(getActivity());
                check_list_text1.setText(GetSpanText.getSpanString(getActivity(),
                        String.valueOf(AppStrings.income)));
                check_list_text1.setTypeface(LoginActivity.sTypeface);
                check_list_text1.setTextColor(R.color.black);
                check_list_text1.setPadding(25, 0, 5, 0);
                check_list_text1.setLayoutParams(contentParams2);
                checkListRow1.addView(check_list_text1);

                TableRow.LayoutParams contentParams3 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams3.setMargins(30, 5, 10, 5);

                CheckBox sCheckBox1 = new CheckBox(getActivity());
                sCheckBox1.setId(0);
                mCheckBoxFields.add(sCheckBox1);
                sCheckBox1.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                sCheckBox1.setClickable(false);
                sCheckBox1.setLayoutParams(contentParams3);
                if (checkList.getIncome().equals("false")) {
                    sCheckBox1.setChecked(false);
                } else if (checkList.getIncome().equals("true")) {
                    sCheckBox1.setChecked(true);
                }  //TODO::
                sCheckBox1.setGravity(Gravity.LEFT);
                checkListRow1.addView(sCheckBox1);

                checkListTable.addView(checkListRow1,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                // TODO:: EXPENSE
                TableRow checkListRow2 = new TableRow(getActivity());
                TableRow.LayoutParams contentParams4 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams4.setMargins(10, 5, 10, 5);

                TextView check_list_text2 = new TextView(getActivity());
                check_list_text2.setText(GetSpanText.getSpanString(getActivity(),
                        String.valueOf(AppStrings.expenses)));
                check_list_text2.setTypeface(LoginActivity.sTypeface);
                check_list_text2.setTextColor(R.color.black);
                check_list_text2.setPadding(25, 0, 5, 0);
                check_list_text2.setLayoutParams(contentParams4);
                checkListRow2.addView(check_list_text2);

                TableRow.LayoutParams contentParams5 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams5.setMargins(30, 5, 10, 5);

                CheckBox sCheckBox2 = new CheckBox(getActivity());
                sCheckBox2.setId(0);
                mCheckBoxFields.add(sCheckBox2);
                sCheckBox2.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                sCheckBox2.setClickable(false);
                sCheckBox2.setLayoutParams(contentParams5);
                if (checkList.getExpense().equals("false")) {
                    sCheckBox2.setChecked(false);
                } else if (checkList.getExpense().equals("true")) {
                    sCheckBox2.setChecked(true);
                }  //TODO::
                sCheckBox2.setGravity(Gravity.LEFT);
                checkListRow2.addView(sCheckBox2);

                checkListTable.addView(checkListRow2,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


// TODO:: INTERNAL LOAN DISBURSEMENT
                TableRow checkListRow3 = new TableRow(getActivity());

                TableRow.LayoutParams contentParams6 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams6.setMargins(10, 5, 10, 5);

                TextView check_list_text3 = new TextView(getActivity());
                check_list_text3.setText(GetSpanText.getSpanString(getActivity(),
                        String.valueOf(AppStrings.InternalLoanDisbursement)));
                check_list_text3.setTypeface(LoginActivity.sTypeface);
                check_list_text3.setTextColor(R.color.black);
                check_list_text3.setPadding(25, 0, 5, 0);
                check_list_text3.setLayoutParams(contentParams6);
                checkListRow3.addView(check_list_text3);

                TableRow.LayoutParams contentParams7 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams7.setMargins(30, 5, 10, 5);

                CheckBox sCheckBox3 = new CheckBox(getActivity());
                sCheckBox3.setId(0);
                mCheckBoxFields.add(sCheckBox3);
                sCheckBox3.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                sCheckBox3.setClickable(false);
                sCheckBox3.setLayoutParams(contentParams7);
                if (checkList.getInternalLoanDisbursement().equals("false")) {
                    sCheckBox3.setChecked(false);
                } else if (checkList.getInternalLoanDisbursement().equals("true")) {
                    sCheckBox3.setChecked(true);
                }  //TODO::
                sCheckBox3.setGravity(Gravity.LEFT);
                checkListRow3.addView(sCheckBox3);

                checkListTable.addView(checkListRow3,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                // TODO:: INTERNAL LOAN REPAYMENT

                TableRow checkListRow4 = new TableRow(getActivity());

                TableRow.LayoutParams contentParams8 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams8.setMargins(10, 5, 10, 5);

                TextView check_list_text4 = new TextView(getActivity());
                check_list_text4.setText(GetSpanText.getSpanString(getActivity(),
                        String.valueOf(AppStrings.mInterloan)));
                check_list_text4.setTypeface(LoginActivity.sTypeface);
                check_list_text4.setTextColor(R.color.black);
                check_list_text4.setPadding(25, 0, 5, 0);
                check_list_text4.setLayoutParams(contentParams8);
                checkListRow4.addView(check_list_text4);

                TableRow.LayoutParams contentParams9 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams9.setMargins(30, 5, 10, 5);

                CheckBox sCheckBox4 = new CheckBox(getActivity());
                sCheckBox4.setId(0);
                mCheckBoxFields.add(sCheckBox4);
                sCheckBox4.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                sCheckBox4.setClickable(false);
                sCheckBox4.setLayoutParams(contentParams9);
                if (checkList.getInternalLoanRepayment().equals("false")) {
                    sCheckBox4.setChecked(false);
                } else if (checkList.getInternalLoanRepayment().equals("true")) {
                    sCheckBox4.setChecked(true);
                }  //TODO::
                sCheckBox4.setGravity(Gravity.LEFT);
                checkListRow4.addView(sCheckBox4);

                checkListTable.addView(checkListRow4,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                // TODO:: BANL LOAN DISBURSEMENT
                TableRow checkListRow5 = new TableRow(getActivity());

                TableRow.LayoutParams contentParams10 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams10.setMargins(10, 5, 10, 5);

                TextView check_list_text5 = new TextView(getActivity());
                check_list_text5.setText(GetSpanText.getSpanString(getActivity(),
                        String.valueOf(AppStrings.mBankLoanDisbursement)));
                check_list_text5.setTypeface(LoginActivity.sTypeface);
                check_list_text5.setTextColor(R.color.black);
                check_list_text5.setPadding(25, 0, 5, 0);
                check_list_text5.setLayoutParams(contentParams10);
                checkListRow5.addView(check_list_text5);

                TableRow.LayoutParams contentParams11 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams11.setMargins(30, 5, 10, 5);

                CheckBox sCheckBox5 = new CheckBox(getActivity());
                sCheckBox5.setId(0);
                mCheckBoxFields.add(sCheckBox5);
                sCheckBox5.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                sCheckBox5.setClickable(false);
                sCheckBox5.setLayoutParams(contentParams11);
                if (checkList.getBankLoanDisbursement().equals("false")) {
                    sCheckBox5.setChecked(false);
                } else if (checkList.getBankLoanDisbursement().equals("true")) {
                    sCheckBox5.setChecked(true);
                }  //TODO::
                sCheckBox5.setGravity(Gravity.LEFT);
                checkListRow5.addView(sCheckBox5);

                checkListTable.addView(checkListRow5,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                // TODO:: BANK TRANSACTION

                TableRow checkListRow6 = new TableRow(getActivity());

                TableRow.LayoutParams contentParams12 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams12.setMargins(10, 5, 10, 5);

                TextView check_list_text6 = new TextView(getActivity());
                check_list_text6.setText(GetSpanText.getSpanString(getActivity(),
                        String.valueOf(AppStrings.bankTransaction)));
                check_list_text6.setTypeface(LoginActivity.sTypeface);
                check_list_text6.setTextColor(R.color.black);
                check_list_text6.setPadding(25, 0, 5, 0);
                check_list_text6.setLayoutParams(contentParams12);
                checkListRow6.addView(check_list_text6);

                TableRow.LayoutParams contentParams13 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams13.setMargins(30, 5, 10, 5);

                CheckBox sCheckBox6 = new CheckBox(getActivity());
                sCheckBox6.setId(0);
                mCheckBoxFields.add(sCheckBox6);
                sCheckBox6.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                sCheckBox6.setClickable(false);
                sCheckBox6.setLayoutParams(contentParams13);
                if (checkList.getBankTransaction().equals("false")) {
                    sCheckBox6.setChecked(false);
                } else if (checkList.getBankTransaction().equals("true")) {
                    sCheckBox6.setChecked(true);
                }  //TODO::
                sCheckBox6.setGravity(Gravity.LEFT);
                checkListRow6.addView(sCheckBox6);

                checkListTable.addView(checkListRow6,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                // TODO:: MEMB. LOAN REPAYMENT
                TableRow checkListRow7 = new TableRow(getActivity());
                TableRow.LayoutParams contentParams14 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams14.setMargins(10, 5, 10, 5);

                TextView check_list_text7 = new TextView(getActivity());
                check_list_text7.setText(GetSpanText.getSpanString(getActivity(),
                        String.valueOf(AppStrings.memberloanrepayment)));
                check_list_text7.setTypeface(LoginActivity.sTypeface);
                check_list_text7.setTextColor(R.color.black);
                check_list_text7.setPadding(25, 0, 5, 0);
                check_list_text7.setLayoutParams(contentParams14);
                checkListRow7.addView(check_list_text7);

                TableRow.LayoutParams contentParams15 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams15.setMargins(30, 5, 10, 5);

                CheckBox sCheckBox7 = new CheckBox(getActivity());
                sCheckBox7.setId(0);
                mCheckBoxFields.add(sCheckBox7);
                sCheckBox7.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                sCheckBox7.setClickable(false);
                sCheckBox7.setLayoutParams(contentParams15);
                if (checkList.getMemberLoanRepayment().equals("false")) {
                    sCheckBox7.setChecked(false);
                } else if (checkList.getMemberLoanRepayment().equals("true")) {
                    sCheckBox7.setChecked(true);
                }  //TODO::
                sCheckBox7.setGravity(Gravity.LEFT);
                checkListRow7.addView(sCheckBox7);

                checkListTable.addView(checkListRow7,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                // TODO:: GRP LOAN REPAYMENT

                TableRow checkListRow8 = new TableRow(getActivity());

                TableRow.LayoutParams contentParams16 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams16.setMargins(10, 5, 10, 5);

                TextView check_list_text8 = new TextView(getActivity());
                check_list_text8.setText(GetSpanText.getSpanString(getActivity(),
                        String.valueOf(AppStrings.grouploanrepayment)));
                check_list_text8.setTypeface(LoginActivity.sTypeface);
                check_list_text8.setTextColor(R.color.black);
                check_list_text8.setPadding(25, 0, 5, 0);
                check_list_text8.setLayoutParams(contentParams16);
                checkListRow8.addView(check_list_text8);

                TableRow.LayoutParams contentParams17 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams17.setMargins(30, 5, 10, 5);

                CheckBox sCheckBox8 = new CheckBox(getActivity());
                sCheckBox8.setId(0);
                mCheckBoxFields.add(sCheckBox8);
                sCheckBox8.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                sCheckBox8.setClickable(false);
                sCheckBox8.setLayoutParams(contentParams17);
                if (checkList.getGroupLoanRepayment().equals("false")) {
                    sCheckBox8.setChecked(false);
                } else if (checkList.getGroupLoanRepayment().equals("true")) {
                    sCheckBox8.setChecked(true);
                }  //TODO::
                sCheckBox8.setGravity(Gravity.LEFT);
                checkListRow8.addView(sCheckBox8);

                checkListTable.addView(checkListRow8,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


            }*/


            //	publicValues.mCheckListWebserviceValues = null;

        } catch (
                Exception e)

        {
            // TODO: handle exception
            e.printStackTrace();
        }

    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        if (mProgressDilaog != null) {
            mProgressDilaog.dismiss();
        }

        switch (serviceType) {
            case GET_CHECKLIST:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        checkList = cdto.getResponseContent().getCheckListDto();
                        mSize = checkList.size();
                        Log.d("size",""+checkList.size());
                        init();

                    } else {

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                        Utils.showToast(getActivity(), message);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }


    }
}

package com.oasys.eshakti.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Dialogue.Dialog_New_TransactionDate;
import com.oasys.eshakti.Dto.CashOfGroup;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.Dto.OfflineDto;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.Dto.SavingRequest;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.GetSpanText;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.MemberTable;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.database.TransactionTable;
import com.oasys.eshakti.views.Get_EdiText_Filter;
import com.oasys.eshakti.views.TextviewUtils;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OI_S_P_Fragment extends Fragment implements View.OnClickListener, NewTaskListener {


    private View rootView;
    public static String TAG = OI_S_P_Fragment.class.getSimpleName();

    List<EditText> sIncomeFields = new ArrayList<EditText>();
    private static String sIncomeAmounts[];
    public static String sSendToServer_Income;

    private TextView mGroupName, mCashinHand, mCashatBank, mHeader, mAutoFill_label, mAutoFill_label1;
    private CheckBox mAutoFill, mAutoFill1;
    private Button mSubmit_Raised_Button;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    private EditText mIncome_values;
    private TableLayout mIncomeTable;

    private Dialog mProgressDilaog;
    Dialog confirmationDialog;

    Boolean isValues;
    String nullVlaue = "0";
    String toBeEditArr[];

    String[] confirmArr;
    int mSize;
    public static int sIncome_Total = 0;
    public static Boolean isIncome = false;

    AlertDialog alertDialog;
    String mLastTrDate = null, mLastTr_ID = null;
    boolean isGetTrid = false;
    private LinearLayout mOthersLayout;
    private TextView mOthersTextView;
    private EditText mOthersEditText;
    private String mOthersAmount_Values;
    boolean isServiceCall = false;
    String mSqliteDBStoredValues_mOtherincomevalues = null, mSqliteDBStoredValues_mSubscriptionIncomevalues = null,
            mSqliteDBStoredValues_mPenaltyIncomevalues = null;

    LinearLayout mMemberNameLayout;
    TextView mMemberName;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<MemberList> arrMem;
    private ArrayList<OfflineDto> offlineData = new ArrayList<>();
    private ArrayList<String> txDateArr;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_savings, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));

        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        arrMem = new ArrayList<>();
        offlineData = new ArrayList<>();
        init();

    }

    private void init() {
        try {
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
            mMemberName = (TextView) rootView.findViewById(R.id.member_name);
            //  mMemberName.setTypeface(LoginActivity.sTypeface);

            /** UI Mapping **/

            mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);
            mHeader.setText(Income.sSelectedIncomeMenu.getName());
            //    mHeader.setTypeface(LoginActivity.sTypeface);

//            if (Income.sSelectedIncomeMenu.getName().toUpperCase().equals(AppStrings.subscriptioncharges) || Income.sSelectedIncomeMenu.getName().toUpperCase().equals(AppStrings.penalty)) {
                mAutoFill_label = (TextView) rootView.findViewById(R.id.autofillLabel);
                mAutoFill_label.setText(AppStrings.autoFill);
                mAutoFill_label.setTypeface(LoginActivity.sTypeface);
                mAutoFill_label.setVisibility(View.VISIBLE);

                mAutoFill = (CheckBox) rootView.findViewById(R.id.autoFill);
                mAutoFill.setVisibility(View.VISIBLE);
                mAutoFill.setOnClickListener(this);
            /*} else {
                mAutoFill_label = (TextView) rootView.findViewById(R.id.autofillLabel);
                mAutoFill_label.setText(AppStrings.autoFill);
                mAutoFill_label.setVisibility(View.GONE);

                mAutoFill = (CheckBox) rootView.findViewById(R.id.autoFill);
                mAutoFill.setVisibility(View.GONE);
                mAutoFill.setOnClickListener(this);

          *//*      mAutoFill_label1 = (TextView) rootView.findViewById(R.id.autofillLabel1);
                mAutoFill_label1.setText(AppStrings.autoFill);
                mAutoFill_label1.setVisibility(View.VISIBLE);

                mAutoFill1 = (CheckBox) rootView.findViewById(R.id.autoFill1);
                mAutoFill1.setVisibility(View.VISIBLE);
                mAutoFill1.setOnClickListener(this);*//*

            }*/

            Log.d(TAG, String.valueOf(mSize));

            mOthersLayout = (LinearLayout) rootView.findViewById(R.id.otherIncomeLayout);
            mOthersTextView = (TextView) rootView.findViewById(R.id.otherLoanTextView);
            mOthersTextView.setTypeface(LoginActivity.sTypeface);
            mOthersEditText = (EditText) rootView.findViewById(R.id.otherLoanEditText);
            mOthersTextView.setText(AppStrings.mOthers);

            mOthersEditText.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
            mOthersEditText.setFilters(Get_EdiText_Filter.editText_filter());
            mOthersEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
            mOthersEditText.setTextColor(R.color.black);
            mOthersEditText.setPadding(5, 5, 5, 5);
            mOthersEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    // TODO Auto-generated method stub
                    if (hasFocus) {
                        ((EditText) v).setGravity(Gravity.LEFT);

                    } else {
                        ((EditText) v).setGravity(Gravity.RIGHT);

                    }
                }
            });

            if (Income.sSelectedIncomeMenu.getName().toUpperCase().equals(AppStrings.subscriptioncharges) || Income.sSelectedIncomeMenu.getName().toUpperCase().equals(AppStrings.penalty)) {
                mOthersLayout.setVisibility(View.GONE);
            } else {
                mOthersLayout.setVisibility(View.VISIBLE);
            }

            TableLayout headerTable = (TableLayout) rootView.findViewById(R.id.savingsTable);
            mIncomeTable = (TableLayout) rootView.findViewById(R.id.fragment_contentTable);
            TableRow savingsHeader = new TableRow(getActivity());
            savingsHeader.setBackgroundResource(R.color.tableHeader);

            ViewGroup.LayoutParams headerParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,
                    1f);

            TextView mMemberName_headerText = new TextView(getActivity());
            mMemberName_headerText
                    .setText(AppStrings.memberName);
            mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
            mMemberName_headerText.setTextColor(Color.WHITE);
            mMemberName_headerText.setPadding(20, 5, 10, 5);
            mMemberName_headerText.setLayoutParams(headerParams);
            savingsHeader.addView(mMemberName_headerText);

            TextView mIncomeAmount_HeaderText = new TextView(getActivity());
            mIncomeAmount_HeaderText
                    .setText(AppStrings.amount);
            mIncomeAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
            mIncomeAmount_HeaderText.setTextColor(Color.WHITE);
            mIncomeAmount_HeaderText.setPadding(10, 5, 80, 5);
            mIncomeAmount_HeaderText.setGravity(Gravity.CENTER);
            mIncomeAmount_HeaderText.setLayoutParams(headerParams);
            mIncomeAmount_HeaderText.setBackgroundResource(R.color.tableHeader);
            savingsHeader.addView(mIncomeAmount_HeaderText);

            headerTable.addView(savingsHeader,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            for (int i = 0; i < mSize; i++) {

                TableRow indv_IncomeRow = new TableRow(getActivity());

                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams.setMargins(10, 5, 10, 5);

                final TextView memberName_Text = new TextView(getActivity());
                memberName_Text.setText(memList.get(i).getMemberName());
                memberName_Text.setTextColor(R.color.black);
                memberName_Text.setPadding(10, 0, 10, 5);
                memberName_Text.setLayoutParams(contentParams);
                memberName_Text.setWidth(200);
                memberName_Text.setSingleLine(true);
                memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
                indv_IncomeRow.addView(memberName_Text);

                TableRow.LayoutParams contentEditParams = new TableRow.LayoutParams(150, ViewGroup.LayoutParams.WRAP_CONTENT);
                contentEditParams.setMargins(30, 5, 100, 5);

                mIncome_values = new EditText(getActivity());

                mIncome_values.setId(i);
                sIncomeFields.add(mIncome_values);
                mIncome_values.setGravity(Gravity.END);
                mIncome_values.setTextColor(Color.BLACK);
                mIncome_values.setPadding(5, 5, 5, 5);
                mIncome_values.setBackgroundResource(R.drawable.edittext_background);
                mIncome_values.setLayoutParams(contentEditParams);// contentParams
                // lParams
                mIncome_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
                mIncome_values.setFilters(Get_EdiText_Filter.editText_filter());
                mIncome_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                mIncome_values.setTextColor(R.color.black);
                mIncome_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);

                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

                        } else {
                            ((EditText) v).setGravity(Gravity.RIGHT);

                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }

                    }
                });
                indv_IncomeRow.addView(mIncome_values);

                mIncomeTable.addView(indv_IncomeRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            }

            mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Submit);
            mSubmit_Raised_Button.setText(AppStrings.submit);
            mSubmit_Raised_Button.setOnClickListener(this);
            mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);

            setVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void setVisibility() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onClick(View view) {

        sIncomeAmounts = new String[sIncomeFields.size()];
        switch (view.getId()) {
            case R.id.fragment_Submit:
                try {
                    sIncome_Total = 0;
                    isIncome = true;
                    confirmArr = new String[mSize];
                    mOthersAmount_Values = "";
                    mOthersAmount_Values = mOthersEditText.getText().toString();
                    if (mOthersAmount_Values.equals("") || mOthersAmount_Values == null) {
                        mOthersAmount_Values = nullVlaue;
                    } else {
                        mOthersAmount_Values = mOthersEditText.getText().toString();
                    }

                    if (mOthersAmount_Values.matches("\\d*\\.?\\d+")) { // match

                        int amount = (int) Math.round(Double.parseDouble(mOthersAmount_Values));
                        mOthersAmount_Values = String.valueOf(amount);
                    }

                    if (arrMem != null && arrMem.size() > 0) {
                        arrMem.clear();
                    }
                    if (offlineData != null && offlineData.size() > 0) {
                        offlineData.clear();
                    }

                    for (int i = 0; i < mSize; i++) {
                        MemberList mem = new MemberList();
                        OfflineDto offline = new OfflineDto();
                        mem.setMemberId(memList.get(i).getMemberId());
                        mem.setAmount(sIncomeFields.get(i).getText().toString());
                        arrMem.add(mem);

                        sIncomeAmounts[i] = sIncomeFields.get(i).getText().toString();

                        sIncome_Total += Integer.parseInt((!sIncomeFields.get(i).getText().toString().equals("") && sIncomeFields.get(i).getText().toString().length() > 0) ? sIncomeFields.get(i).getText().toString() : "0");

                        try {
                            offline.setMemberId(memList.get(i).getMemberId());
                            offline.setMemberName(memList.get(i).getMemberName());
                            offline.setAnimatorId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                            offline.setShgId(memList.get(i).getShgId());
                            offline.setIncomeTypeId(Income.sSelectedIncomeMenu.getId());
//                            offline.setLastTransactionDateTime(shgDto.getLastTransactionDate());
                            offline.setLastTransactionDateTime(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                            offline.setModifiedDateTime(System.currentTimeMillis() + "");
                            offline.setModeOCash("2");
                            offline.setIs_transaction_tdy("1.0");
                            offline.setTxType(NewDrawerScreen.INCOME);
                            if (Income.sSelectedIncomeMenu.getName().toUpperCase().equals(AppStrings.subscriptioncharges)) {
                                offline.setTxSubtype(AppStrings.subscriptioncharges);
                            } else if (Income.sSelectedIncomeMenu.getName().toUpperCase().equals(AppStrings.penalty)) {
                                offline.setTxSubtype(AppStrings.penalty);
                            } else {
                                offline.setTxSubtype(AppStrings.otherincome);
                            }

                            offline.setIc_exp_Amount(sIncomeAmounts[i]);
                            offline.setAmount(mOthersAmount_Values);
                            offlineData.add(offline);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (mOthersAmount_Values != null && mOthersAmount_Values.length() > 0)
                        sIncome_Total = sIncome_Total + Integer.parseInt(mOthersAmount_Values);

                    if (sIncome_Total != 0) {

                        confirmationDialog = new Dialog(getActivity());
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                        dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                                LayoutParams.WRAP_CONTENT));

                        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                        confirmationHeader.setText(AppStrings.confirmation);
                        confirmationHeader.setTypeface(LoginActivity.sTypeface);
                        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                        DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
                        Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
                        String dateStr = simple.format(d);
                        TextView transactdate = (TextView)dialogView.findViewById(R.id.transactdate);
                        transactdate.setText(dateStr);

                        for (int i = 0; i < mSize; i++) {

                            TableRow indv_SavingsRow = new TableRow(getActivity());

                            @SuppressWarnings("deprecation")
                            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                                    LayoutParams.WRAP_CONTENT, 1f);
                            contentParams.setMargins(10, 5, 10, 5);

                            TextView memberName_Text = new TextView(getActivity());
                            memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                                    memList.get(i).getMemberName()));
                            memberName_Text.setPadding(5, 5, 5, 5);
                            memberName_Text.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(memberName_Text);

                            TextView confirm_values = new TextView(getActivity());
                            confirm_values
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf((sIncomeAmounts[i] != null && sIncomeAmounts[i].length() > 0) ? sIncomeAmounts[i] : "0")));
                            confirm_values.setPadding(5, 5, 5, 5);
                            confirm_values.setGravity(Gravity.RIGHT);
                            confirm_values.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(confirm_values);

                            confirmationTable.addView(indv_SavingsRow,
                                    new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

                        }

                        if (Income.sSelectedIncomeMenu.getName().toUpperCase().equals(AppStrings.otherincome)) {

                            TableRow othersRow = new TableRow(getActivity());

                            @SuppressWarnings("deprecation")
                            TableRow.LayoutParams others_contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                                    LayoutParams.WRAP_CONTENT, 1f);
                            others_contentParams.setMargins(10, 5, 10, 5);

                            TextView memberName_Text = new TextView(getActivity());
                            memberName_Text
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mOthers)));
                            memberName_Text.setPadding(5, 5, 5, 5);
                            memberName_Text.setLayoutParams(others_contentParams);
                            othersRow.addView(memberName_Text);

                            TextView confirm_values = new TextView(getActivity());
                            confirm_values.setText(
                                    GetSpanText.getSpanString(getActivity(), String.valueOf(mOthersAmount_Values)));
                            confirm_values.setPadding(5, 5, 5, 5);
                            confirm_values.setGravity(Gravity.RIGHT);
                            confirm_values.setLayoutParams(others_contentParams);
                            othersRow.addView(confirm_values);

                            confirmationTable.addView(othersRow,
                                    new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                        } else {

                        }

                        View rullerView = new View(getActivity());
                        rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                        rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                        // 229,
                        // 242));
                        confirmationTable.addView(rullerView);

                        TableRow totalRow = new TableRow(getActivity());

                        @SuppressWarnings("deprecation")
                        TableRow.LayoutParams totalParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                                LayoutParams.WRAP_CONTENT, 1f);
                        totalParams.setMargins(10, 5, 10, 5);

                        TextView totalText = new TextView(getActivity());
                        totalText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
                        totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
                        totalText.setLayoutParams(totalParams);
                        totalRow.addView(totalText);

                        TextView totalAmount = new TextView(getActivity());
                        totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncome_Total)));// SavingsFragment.sSavings_Total
                        totalAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                        totalAmount.setGravity(Gravity.RIGHT);
                        totalAmount.setLayoutParams(totalParams);
                        totalRow.addView(totalAmount);


                        confirmationTable.addView(totalRow,
                                new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));


                        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                        mEdit_RaisedButton.setText(AppStrings.edit);
                        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                        // 205,
                        // 0));
                        mEdit_RaisedButton.setOnClickListener(this);

                        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                        mOk_RaisedButton.setText(AppStrings.yes);
                        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                        mOk_RaisedButton.setOnClickListener(this);

                        confirmationDialog.getWindow()
                                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        confirmationDialog.setCanceledOnTouchOutside(false);
                        confirmationDialog.setContentView(dialogView);
                        confirmationDialog.setCancelable(true);
                        confirmationDialog.show();

                        MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
                        margin.leftMargin = 10;
                        margin.rightMargin = 10;
                        margin.topMargin = 10;
                        margin.bottomMargin = 10;
                        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
                    } else {
                        //    sSend_To_Server_Donation_FIncome = "0";
                        TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
                break;
            case R.id.fragment_Edit:
                sIncome_Total = 0;
                mOthersAmount_Values = "0";
                mSubmit_Raised_Button.setClickable(true);

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();

                break;
            case R.id.frag_Ok:

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();

                if (networkConnection.isNetworkAvailable()) {
                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 1) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }
                    // NOTHING TO DO::
                } else {

                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 2) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }

                }



                if (Income.sSelectedIncomeMenu.getName().toUpperCase().equals(AppStrings.subscriptioncharges)) {
                    SavingRequest sr = new SavingRequest();
                    sr.setIncomeTypeId(Income.sSelectedIncomeMenu.getId());
                    sr.setModeOfCash("2");
                    sr.setShgId(shgDto.getShgId());
                    sr.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
//                    sr.setTransactionDate(shgDto.getLastTransactionDate());
                    sr.setMobileDate(System.currentTimeMillis() + "");
                    sr.setMemberSaving(arrMem);
                    // sr.setAmount(mOthersAmount_Values);
                    sr.setAmount(mOthersAmount_Values);
                    String sreqString = new Gson().toJson(sr);
                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();
                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.SUBSCRIPTION, sreqString, getActivity(), ServiceType.OI_S_P);
                    } else {
                        if (TransactionTable.getLoginFlag(AppStrings.subscriptioncharges).size() <= 0 || (!TransactionTable.getLoginFlag(AppStrings.subscriptioncharges).get(TransactionTable.getLoginFlag(AppStrings.subscriptioncharges).size()-1).getLoginFlag().equals("1")))
                            insertIncome();
                        else
                            TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);


                    }


                } else if (Income.sSelectedIncomeMenu.getName().toUpperCase().equals(AppStrings.otherincome)) {
                    SavingRequest sr = new SavingRequest();
                    sr.setIncomeTypeId(Income.sSelectedIncomeMenu.getId());
                    sr.setModeOfCash("2");
                    sr.setShgId(shgDto.getShgId());
                    sr.setMobileDate(System.currentTimeMillis() + "");
//                    sr.setTransactionDate(shgDto.getLastTransactionDate());
                    sr.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                    sr.setMemberSaving(arrMem);
                    sr.setAmount(mOthersAmount_Values);
                    String sreqString = new Gson().toJson(sr);
                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();
                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.OTHERINCOME, sreqString, getActivity(), ServiceType.OI_S_P);
                    } else {
                        if ((TransactionTable.getLoginFlag(AppStrings.otherincome).size() <= 0 || (!TransactionTable.getLoginFlag(AppStrings.otherincome).get(TransactionTable.getLoginFlag(AppStrings.otherincome).size()-1).getLoginFlag().equals("1"))))
                            insertIncome();
                        else
                            TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);

                    }

                } else if (Income.sSelectedIncomeMenu.getName().toUpperCase().equals(AppStrings.penalty)) {
                    SavingRequest sr = new SavingRequest();
                    sr.setIncomeTypeId(Income.sSelectedIncomeMenu.getId());
                    sr.setModeOfCash("2");
                    sr.setShgId(shgDto.getShgId());
                    sr.setMobileDate(System.currentTimeMillis() + "");
                    sr.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());
//                    sr.setTransactionDate(shgDto.getLastTransactionDate());
                    sr.setMemberSaving(arrMem);
                    sr.setAmount(mOthersAmount_Values);
                    String sreqString = new Gson().toJson(sr);
                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();
                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.PENALTY, sreqString, getActivity(), ServiceType.OI_S_P);
                    } else {
                        if ((TransactionTable.getLoginFlag(AppStrings.penalty).size() <= 0 || ( !TransactionTable.getLoginFlag(AppStrings.penalty).get(TransactionTable.getLoginFlag(AppStrings.penalty).size() -1).getLoginFlag().equals("1"))))
                            insertIncome();
                        else
                            TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);

                    }
                }
                break;

            case R.id.autoFill:

                String similiar_Income;
                // isValues = true;

                if (mAutoFill.isChecked()) {

                    try {

                        // Makes all edit fields holds the same savings
                        similiar_Income = sIncomeFields.get(0).getText().toString();

                        for (int i = 0; i < sIncomeAmounts.length; i++) {
                            sIncomeFields.get(i).setText(similiar_Income);
                            sIncomeFields.get(i).setGravity(Gravity.RIGHT);
                            sIncomeFields.get(i).clearFocus();
                            sIncomeAmounts[i] = similiar_Income;

                        }

                        /** To clear the values of EditFields in case of uncheck **/

                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();

                        TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }
                } else {

                }

                break;

            case R.id.autoFill1:

                String similiar_Income1;
                // isValues = true;

                if (mAutoFill1.isChecked()) {

                    try {

                        // Makes all edit fields holds the same savings
                        similiar_Income1 = sIncomeFields.get(0).getText().toString();

                        for (int i = 0; i < sIncomeAmounts.length; i++) {
                            sIncomeFields.get(i).setText(similiar_Income1);
                            sIncomeFields.get(i).setGravity(Gravity.RIGHT);
                            sIncomeFields.get(i).clearFocus();
                            sIncomeAmounts[i] = similiar_Income1;

                        }

                        /** To clear the values of EditFields in case of uncheck **/

                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();

                        TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }
                } else {

                }

                break;
        }


    }

    private void insertTransaction_tdy()
    {
        for (OfflineDto ofdto : offlineData) {
            ofdto.setIs_transaction_tdy("1.0");
            ofdto.setShgId(shgDto.getShgId());
            SHGTable.updateIstransaction(ofdto);
        }

    }

    private void insertIncome() {
        try {
            int cih = 0;
            cih = (int) Double.parseDouble(shgDto.getCashInHand()) + sIncome_Total;
            int value = (MySharedPreference.readInteger(getActivity(), MySharedPreference.INCOME_COUNT, 0) + 1);
            if (value > 0)
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.INCOME_COUNT, value);


            for (OfflineDto ofdto : offlineData) {

                ofdto.setTotalIncomeAmount((sIncome_Total) + "");
                ofdto.setCashAtBank(shgDto.getCashAtBank());
                ofdto.setCashInhand(cih + "");
                ofdto.setICount(value + "");
                TransactionTable.insertTransIncomeData(ofdto);
            }

            Log.i("print","getFFlag Value : "+shgDto.getFFlag());
            if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
            }
            String cihstr = "", cabstr = "", lastTranstr = "";
            cihstr = offlineData.get(0).getCashInhand();
            cabstr = offlineData.get(0).getCashAtBank();
            lastTranstr = offlineData.get(0).getLastTransactionDateTime();
            CashOfGroup csg = new CashOfGroup();
            csg.setCashInHand(cihstr);
            csg.setCashAtBank(cabstr);
            csg.setLastTransactionDate(lastTranstr);
            SHGTable.updateSHGDetails(csg, shgDto.getId());

            FragmentManager fm = getFragmentManager();
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            NewDrawerScreen.showFragment(new MainFragment());


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
            }
            switch (serviceType) {
                case OI_S_P:
                    try {
                        if (result != null) {
                            ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                            String message = cdto.getMessage();
                            int statusCode = cdto.getStatusCode();
                            if (statusCode == Utils.Success_Code) {
                                Utils.showToast(getActivity(), message);
                                insertTransaction_tdy();
                                Log.i("print","getFFlag Value : "+shgDto.getFFlag());
                                if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                    SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                                }
                                FragmentManager fm = getFragmentManager();
                                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                MainFragment mainFragment = new MainFragment();
                                Bundle bundles = new Bundle();
                                bundles.putString("Transaction", MainFragment.Flag_Transaction);
                                mainFragment.setArguments(bundles);
                                NewDrawerScreen.showFragment(mainFragment);
//                            NewDrawerScreen.showFragment(new MainFragment());

                            } else {
                                if (statusCode == 401) {

                                    Log.e("Group Logout", "Logout Sucessfully");
                                    AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                    if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                        mProgressDilaog.dismiss();
                                        mProgressDilaog = null;
                                    }
                                }
                                Utils.showToast(getActivity(), message);
                                insertIncome();
                            }
                        } else {
                            insertIncome();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }


        }
    }

}

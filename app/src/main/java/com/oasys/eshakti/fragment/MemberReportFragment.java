package com.oasys.eshakti.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.Adapter.MemberreportAdapter;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.RecyclerItemClickListener;
import com.oasys.eshakti.R;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.MemberTable;
import com.oasys.eshakti.database.SHGTable;

import java.util.List;


public class MemberReportFragment extends Fragment {

    private RecyclerView recyclerViewMemberReport;
    private LinearLayoutManager linearLayoutManager;
    private static FragmentManager fm;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mHeader,mCashatBank;


    public MemberReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_member_report, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        recyclerViewMemberReport = (RecyclerView) view.findViewById(R.id.recyclerViewMemberReport);
        mGroupName = (TextView) view.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);

        mCashinHand = (TextView) view.findViewById(R.id.ch);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);

        mCashatBank = (TextView) view.findViewById(R.id.cb);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashatBank.setTypeface(LoginActivity.sTypeface);

        mHeader = (TextView) view.findViewById(R.id.fragmentHeader);
        mHeader.setText(AppStrings.Memberreports);
        mHeader.setTypeface(LoginActivity.sTypeface);


        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewMemberReport.setLayoutManager(linearLayoutManager);
        recyclerViewMemberReport.setHasFixedSize(true);

        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        MemberreportAdapter memberreportAdapter = new MemberreportAdapter(getActivity(), memList);
        recyclerViewMemberReport.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerViewMemberReport.setAdapter(memberreportAdapter);


        fm = getActivity().getSupportFragmentManager();

        recyclerViewMemberReport.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ReportLoanMenu reportLoanMenu = new ReportLoanMenu();
                String mem_id = memList.get(position).getMemberId();
                String memName = memList.get(position).getMemberName();
                MySharedPreference.writeString(getActivity(),MySharedPreference.MEM_NAME_SUMMARY,memName);
                Bundle bundle = new Bundle();
                bundle.putString("memid", mem_id);
                Log.d("Mem1", mem_id);
                reportLoanMenu.setArguments(bundle);
                NewDrawerScreen.showFragment(reportLoanMenu);

            }
        }));


        return view;
    }

    public static void showFragment(Fragment fragment) {

        FragmentTransaction trans = fm.beginTransaction();
        trans.replace(R.id.static_frame, fragment);
        trans.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out);
        trans.show(fragment).commit();
    }

}

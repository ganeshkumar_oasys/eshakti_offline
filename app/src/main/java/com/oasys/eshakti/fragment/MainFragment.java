package com.oasys.eshakti.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.FileProvider;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.Adapter.DashboardAdapter;
import com.oasys.eshakti.Adapter.DashboardDto;
import com.oasys.eshakti.Dialogue.Dialog_New_TransactionDate;
import com.oasys.eshakti.Dto.CashOfGroup;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.Dto.ShgBankDetails;
import com.oasys.eshakti.Dto.TrainingsList;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.GetTypeface;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.RecyclerItemClickListener;
import com.oasys.eshakti.OasysUtils.RegionalConversion;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.Animator;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.activity.SHGGroupActivity;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.views.ButtonFlat;
import com.oasys.eshakti.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;

public class MainFragment extends Fragment implements View.OnClickListener, NewTaskListener {

    private LinearLayout mTransaction;
    private LinearLayout mProfile;
    private LinearLayout mReports;
    private LinearLayout mMeeting;
    private LinearLayout mSeeting;
    private LinearLayout mHelp;
    private LinearLayout mCheckbacklog;

    private TextView mDashItemname;
    private LinearLayout mDashItemnamelayout;
    private LinearLayout mRecyclerviewlayout;
    private LinearLayout controller;
    private ExpandableListView navList;
    private DrawerLayout drawer_layout;
    private DashboardAdapter dashboardAdapter;

    private RelativeLayout mDisplayPopup;
    private View rootView;
    public RecyclerView recyclerView_dash_board;
    private static ArrayList<DashboardDto> dashboardDtos;
    private LinearLayoutManager linearLayoutManager;
    private NetworkConnection networkConnection;
    private Dialog mProgressDilaog;
    private ListOfShg shgDetails;
    private OnDataPass dataPasser;
    private ArrayList<ShgBankDetails> bankdetails;
    private String amount = "0.0";
    private boolean isPdfDownload = false;
    public static String Flag_Transaction = "TRANSACTION";
    public static String Flag_Profile = "PROFILE";
    public static String Flag_Meeting = "MEETING";
    public static String Flag_Setting = "SETTING";
    private Date  systemDate,lastTransaDate;
    NewDrawerScreen newDrawerScreen;

    private ListOfShg shgDto;
    Dialog confirmationDialog;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dashboard, container, false);
       /* networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        shgDetails = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        dashboardDtos = new ArrayList<>();
        //  bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
       *//* if (networkConnection.isNetworkAvailable()) {            //  onTaskStarted();
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.NAV_DETAILS + shgDetails.getId(), getActivity(), ServiceType.NAV_DETAILS);
            *//**//*for (ShgBankDetails bd : bankdetails) {
                RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.BT_CB_FD_VALUE + bd.getShgSavingsAccountId(), getActivity(), ServiceType.FD_VALUE);
            }*//**//*
        }*//*
        if (networkConnection.isNetworkAvailable()) {
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.NAV_DETAILS + shgDetails.getId(), getActivity(), ServiceType.NAV_DETAILS);
        } else {
            init();
            dashboardDtos.clear();
            transactionsoffSubMenu();
        }*/
/*
        Log.d("flagvalue",EShaktiApplication.flag);

        if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
            NewDrawerScreen.item1.setVisible(false);
            NewDrawerScreen.item2.setVisible(false);
            NewDrawerScreen.item.setVisible(false);
            NewDrawerScreen.logOutItem.setVisible(false);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.INVISIBLE);
        }
        else
        {
            NewDrawerScreen.item1.setVisible(true);
            NewDrawerScreen.item2.setVisible(true);
            NewDrawerScreen.item.setVisible(true);
            NewDrawerScreen.logOutItem.setVisible(true);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.VISIBLE);
        }*/


        return rootView;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
            menu.findItem(R.id.action_home).setVisible(false);
            menu.findItem(R.id.action_logout).setVisible(false);
            menu.findItem(R.id.menu_logout).setVisible(false);
            menu.findItem(R.id.action_grouplist).setVisible(false);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.INVISIBLE);
        }
        else
        {
            menu.findItem(R.id.action_home).setVisible(true);
            menu.findItem(R.id.action_logout).setVisible(true);
            menu.findItem(R.id.menu_logout).setVisible(true);
            menu.findItem(R.id.action_grouplist).setVisible(true);
            NewDrawerScreen.mMenuDashboard.setVisibility(View.VISIBLE);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        shgDetails = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));



        //  bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        if (networkConnection.isNetworkAvailable()) {            //  onTaskStarted();
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.NAV_DETAILS + shgDetails.getId(), getActivity(), ServiceType.NAV_DETAILS);
            /*for (ShgBankDetails bd : bankdetails) {
                RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.BT_CB_FD_VALUE + bd.getShgSavingsAccountId(), getActivity(), ServiceType.FD_VALUE);
            }*/}
        else
        {
            CashOfGroup csg = new CashOfGroup();
            csg.setCashInHand(shgDetails.getCashInHand());
            csg.setCashAtBank(shgDetails.getCashAtBank());
            csg.setLastTransactionDate(shgDetails.getLastTransactionDate());
            SHGTable.updateSHGDetails(csg, shgDto.getId());
            passData(SHGTable.getSHGDetails(shgDetails.getShgId()));

        }


        init();
      /*  dashboardDtos.clear();
        transactionsSubMenu();*/
    }





    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPasser = (OnDataPass) context;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {

            if (MySharedPreference.readInteger(getActivity(), MySharedPreference.MENU_SELECTED, 0) == 1) {
            if (dashboardDtos != null && dashboardDtos.size() > 0)
                dashboardDtos.clear();
                if (networkConnection.isNetworkAvailable()) {
                    transactionsSubMenu();
                } else {
                    transactionsoffSubMenu();
                }
            } else if (MySharedPreference.readInteger(getActivity(), MySharedPreference.MENU_SELECTED, 0) == 2) {
            if (dashboardDtos != null && dashboardDtos.size() > 0)
                dashboardDtos.clear();
                if (networkConnection.isNetworkAvailable()) {
                    profileSubMenu();
                } else {
                    profileOffSubMenu();
                }
            } else if (MySharedPreference.readInteger(getActivity(), MySharedPreference.MENU_SELECTED, 0) == 3) {
            if (dashboardDtos != null && dashboardDtos.size() > 0)
                dashboardDtos.clear();
                if (networkConnection.isNetworkAvailable()) {
                    reportsSubMenu();
                } else {
                    reportsOffSubMenu();
                }
            } else if (MySharedPreference.readInteger(getActivity(), MySharedPreference.MENU_SELECTED, 0) == 4) {
            if (dashboardDtos != null && dashboardDtos.size() > 0)
                dashboardDtos.clear();
                if (networkConnection.isNetworkAvailable()) {
                    meetingsSubMenu();
                } else {
                    meetingsOffSubMenu();
                }
            } else if (MySharedPreference.readInteger(getActivity(), MySharedPreference.MENU_SELECTED, 0) == 5) {
            if (dashboardDtos != null && dashboardDtos.size() > 0)
                dashboardDtos.clear();
                if (networkConnection.isNetworkAvailable()) {
                    settingsSubMenu();
                } else {
                    settingsOffSubMenu();
                }
            } else if (MySharedPreference.readInteger(getActivity(), MySharedPreference.MENU_SELECTED, 0) == 6) {
            if (dashboardDtos != null && dashboardDtos.size() > 0)
                dashboardDtos.clear();
                if (networkConnection.isNetworkAvailable()) {
                    helpSubMenu();
                } else {
                    helpOffSubMenu();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void init() {
        mDisplayPopup = (RelativeLayout) rootView.findViewById(R.id.mDisplayPopup);
        mTransaction = (LinearLayout) rootView.findViewById(R.id.mTransaction);
        mProfile = (LinearLayout) rootView.findViewById(R.id.mProfile);
        mReports = (LinearLayout) rootView.findViewById(R.id.mReports);
        mMeeting = (LinearLayout) rootView.findViewById(R.id.mMeeting);
        mSeeting = (LinearLayout) rootView.findViewById(R.id.mSeeting);
        mHelp = (LinearLayout) rootView.findViewById(R.id.mHelp);
        mCheckbacklog = (LinearLayout) rootView.findViewById(R.id.mCheckbacklog);
        recyclerView_dash_board = (RecyclerView) rootView.findViewById(R.id.recyclerView_dash_board);
        mDashItemname = (TextView) rootView.findViewById(R.id.mDashItemname);


        TextView txtTrans = (TextView) rootView.findViewById(R.id.txtTrans);
        TextView txtSetting = (TextView) rootView.findViewById(R.id.txtSetting);
        TextView txtCheckLog = (TextView) rootView.findViewById(R.id.txtCheckLog);
        TextView txtmeeting = (TextView) rootView.findViewById(R.id.txtmeeting);
        TextView txtHelp = (TextView) rootView.findViewById(R.id.txtHelp);
        TextView txtProfile = (TextView) rootView.findViewById(R.id.txtProfile);
        TextView txtReport = (TextView) rootView.findViewById(R.id.txtReport);


        txtTrans.setText(AppStrings.transaction);
        txtSetting.setText(AppStrings.settings);
        txtProfile.setText(AppStrings.profile);
        txtReport.setText(AppStrings.reports);
        txtHelp.setText(AppStrings.help);
        txtCheckLog.setText(AppStrings.mCheckBackLog);
        txtmeeting.setText(AppStrings.meeting);


        txtTrans.setTypeface(LoginActivity.sTypeface);
        txtCheckLog.setTypeface(LoginActivity.sTypeface);
        txtHelp.setTypeface(LoginActivity.sTypeface);
        txtmeeting.setTypeface(LoginActivity.sTypeface);
        txtProfile.setTypeface(LoginActivity.sTypeface);
        txtReport.setTypeface(LoginActivity.sTypeface);
        txtSetting.setTypeface(LoginActivity.sTypeface);
        mDashItemname.setTypeface(LoginActivity.sTypeface);


        mDashItemnamelayout = (LinearLayout) rootView.findViewById(R.id.mDashItemnamelayout);
        mRecyclerviewlayout = (LinearLayout) rootView.findViewById(R.id.mRecyclerviewlayout);
        controller = (LinearLayout) rootView.findViewById(R.id.controller);
        navList = (ExpandableListView) rootView.findViewById(R.id.navList);
        drawer_layout = (DrawerLayout) rootView.findViewById(R.id.drawer_layout);
        dashboardDtos = new ArrayList<>();

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView_dash_board.setLayoutManager(linearLayoutManager);
        recyclerView_dash_board.setHasFixedSize(true);

        mTransaction.setOnClickListener(this);
        mProfile.setOnClickListener(this);
        mReports.setOnClickListener(this);
        mMeeting.setOnClickListener(this);
        mSeeting.setOnClickListener(this);
        mHelp.setOnClickListener(this);

        recyclerView_dash_board.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if (networkConnection.isNetworkAvailable()) {
                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 1) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }
                    // NOTHING TO DO::
                } else {

                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 2) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }

                }


                if (dashboardDtos != null) {
                    DashboardDto dto = dashboardDtos.get(position);
                    String selectedItem = dto.getName();

                    /*DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
                    DateFormat simple1 = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

                    if (shgDto.getLastTransactionDate() != null && shgDto.getLastTransactionDate().length() > 0) {
                        Date d = new Date(Long.parseLong(shgDto.getLastTransactionDate()));
                        String dateStr = simple.format(d);
                        try {
                            lastTransaDate = df.parse(dateStr);
                            Log.d("lasttransaction", "" + lastTransaDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        try {

                            Calendar calender = Calendar.getInstance();
                            String formattedDate = df.format(calender.getTime());
                            systemDate = df.parse(formattedDate);
                            Log.d("currentdate", "" + systemDate);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }*/


//                    if (systemDate.compareTo(lastTransaDate) > 0) {

                        if (selectedItem.equals(AppStrings.savings)) {
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                //fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.SAVINGS);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);
                        } else if (selectedItem.equals(AppStrings.income)) {
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.INCOME);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);

                        } else if (selectedItem.equals(AppStrings.expenses)) {
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.EXPENCE);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (selectedItem.equals(AppStrings.memberloanrepayment)) {
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.MEMBER_LOAN_REPAYMENT);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (selectedItem.equals(AppStrings.grouploanrepayment)) {
                            Log.d("profileCheck", "working");
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.GROUP_LOAN_REPAYMENT);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //NewDrawerScreen.showFragment(new GroupLoanRepayment());

                        } else if (selectedItem.equals(AppStrings.bankTransaction)) {
                            Log.d("profileCheck", "working");
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.BANK_TRANSACTION);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);

                        } else if (selectedItem.equals(AppStrings.InternalLoanDisbursement)) {
                            Log.d("profileCheck", "working");
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.LOAN_DISBURSEMENT);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else if (selectedItem.equals(AppStrings.mDefault)) {
                            try {

                                CalendarstepwiseDialogShow();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (selectedItem.equals(AppStrings.mCheckList)) {
                            Log.d("CheckList", "working");
                            NewDrawerScreen.showFragment(new CheckList());

                        }
                        else if (selectedItem.equals(AppStrings.social_security)) {
                            Log.d("profileCheck", "working");

//                            TastyToast.makeText(getActivity(),"Inprogress", TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            NewDrawerScreen.showFragment(new SocialSecurityMenu());
//                                NewDrawerScreen.showFragment(new Transaction_Audit_MemberLoan());
//                            } else {
//                                TastyToast.makeText(getActivity(), AppStrings.mAccNoNetworkCheck,
//                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
//                            }
                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);

                        }
                        else if (selectedItem.equals(AppStrings.mCreditLinkageInfo)) {
                            Log.d("profileCheck", "working");
                            if (networkConnection.isNetworkAvailable()) {
                                NewDrawerScreen.showFragment(new CreditLinkage());
                            } else {
                                TastyToast.makeText(getActivity(), AppStrings.mCreditLinkageAlert,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }
                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);

                        }
                        else if (selectedItem.equals(AppStrings.mMobileNoUpdation)) {
                            Log.d("profileCheck", "working");
                            if (networkConnection.isNetworkAvailable()) {
                                NewDrawerScreen.showFragment(new ProfileMobileUpdate());
                            } else {
                                TastyToast.makeText(getActivity(), AppStrings.mMobileNoUpdationNetworkCheck,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }
                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);

                        }
                        else if (selectedItem.equals(AppStrings.mAadhaarNoUpdation)) {
                            if (networkConnection.isNetworkAvailable()) {
                                // MemberDrawerScreen.showFragment(new Animator());
                                TastyToast.makeText(getActivity(), "Page under construction!",
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            } else {
                                TastyToast.makeText(getActivity(), AppStrings.mAadhaarNoNetworkCheck,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }

                        }
                        else if (selectedItem.equals(AppStrings.mAccountNoUpdation)) {
                            Log.d("profileCheck", "working");
                            if (networkConnection.isNetworkAvailable()) {
                                NewDrawerScreen.showFragment(new MemberAcountNumberUpdationFragment());
                            } else {
                                TastyToast.makeText(getActivity(), AppStrings.mAccNoNetworkCheck,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }
                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);

                        }else if (selectedItem.equals(AppStrings.mshgsbacuploadpassbook)) {
                            Log.d("profileCheck", "working");
                            if (networkConnection.isNetworkAvailable()) {
                                NewDrawerScreen.showFragment(new ShgsbAcUploadPassbook());
                            } else {
                                TastyToast.makeText(getActivity(), AppStrings.mShgAccNouploadpassbookNetworkCheck,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }
                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);

                        } else if (selectedItem.equals(AppStrings.mSHGAccountNoUpdation)) {
                            if (networkConnection.isNetworkAvailable()) {
//                                NewDrawerScreen.showFragment(new profile_ShgAccountNumber_updation());
                                NewDrawerScreen.showFragment(new Profile_SHGAccountnumber_updation());
                            } else {
                                TastyToast.makeText(getActivity(), AppStrings.mShgAccNoNetworkCheck,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }

                        } else if (selectedItem.equals(AppStrings.uploadInfo)) {
                            NewDrawerScreen.showFragment(new MemberDetails());

                        } else if (selectedItem.equals(AppStrings.groupProfile)) {
                            NewDrawerScreen.showFragment(new GroupProfile());

                        } else if (selectedItem.equals(AppStrings.agentProfile)) {
                            NewDrawerScreen.showFragment(new Animator());
                            /*if (networkConnection.isNetworkAvailable()) {
                                NewDrawerScreen.showFragment(new Animator());
                            } else {
                                TastyToast.makeText(getActivity(), "Agent profile is empty",
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }*/

                        } else if (selectedItem.equals(AppStrings.presidentRotation)) {
                            NewDrawerScreen.showFragment(new PresidentRotation(getActivity().getApplicationContext()));
                        } else if (selectedItem.equals(AppStrings.Memberreports)) {
                            NewDrawerScreen.showFragment(new MemberReportFragment());
                        } else if (selectedItem.equals(AppStrings.GroupReports)) {
                            Log.d("profileCheck", "working");
                            NewDrawerScreen.showFragment(new GroupReportFragment());
                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);
                        } else if (selectedItem.equals(AppStrings.transactionsummary)) {
                            Log.d("profileCheck", "working");
//                            NewDrawerScreen.showFragment(new Reports_BankTransactionSummary());
                            NewDrawerScreen.showFragment(new BankTransactionReportList());
                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);
                        } else if (selectedItem.equals(AppStrings.bankBalance)) {
                            Log.d("profileCheck", "working");
                            NewDrawerScreen.showFragment(new Reports_BankBalance_Fragment());
                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);
                        } else if (selectedItem.equals(AppStrings.offlineReports)) {
                            Log.d("profileCheck", "working");
                            NewDrawerScreen.showFragment(new OfflineReport_Date());
                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);
                        } else if (selectedItem.equals(AppStrings.Attendance)) {
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.ATTENDANCE);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            // NewDrawerScreen.showFragment(new Attendance());
                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);

                        } else if (selectedItem.equals(AppStrings.MinutesofMeeting)) {
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.MINUTES_OF_MEETINGS);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            // NewDrawerScreen.showFragment(new MinutesOFMeeting());
                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);

                        } else if (selectedItem.equals(AppStrings.auditing)) {
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.AUDITING);

                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (selectedItem.equals(AppStrings.training)) {
                            Log.d("profileCheck", "working");

                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.TRAINING);

                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (selectedItem.equals(AppStrings.upload_schedule)) {
                            NewDrawerScreen.showFragment(new UploadScedule());

                        } else if (selectedItem.equals(AppStrings.upload_passbook)) {
                            NewDrawerScreen.showFragment(new UploadPassbook());
                        }
                        else if (selectedItem.equals(AppStrings.ESHKTHI1)) {
                            Log.d("profileCheck", "working");
                            NewDrawerScreen.showFragment(new EShaktiApp());
                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);
                            ;
                        } else if (selectedItem.equals(AppStrings.contacts)) {
                            NewDrawerScreen.showFragment(new Contacts());
                            ;
                        } else if (selectedItem.equals(AppStrings.mPdfManual)) {
                            CopyReadAssets();
                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);
                            ; }
                        else if (selectedItem.equals(AppStrings.videos)) {
                            NewDrawerScreen.showFragment(new VideoListFragment());
                            ;
                        } else if (selectedItem.equals(AppStrings.passwordchange)) {
                            if (networkConnection.isNetworkAvailable()) {
                                NewDrawerScreen.showFragment(new Settings_Change_Password_Fragment());
                            } else {
                                TastyToast.makeText(getActivity(), AppStrings.offline_ChangePwdAlert,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }

                        } else if (selectedItem.equals(AppStrings.changeLanguage)) {
                            Log.d("ChangeLanguage", "working");
                            if (networkConnection.isNetworkAvailable()) {
                                onTaskStarted();
                                RestClient.getRestClient(MainFragment.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GET_LANGUAGE + MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""), getActivity(), ServiceType.GETLANGUAGE);
                            }
                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);

                        } else if (selectedItem.equals(AppStrings.deactivateAccount)) {
                            NewDrawerScreen.showFragment(new DeactivateAccount());
                        } else if (selectedItem.equals(AppStrings.mSendEmail)) {
                            if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage("Do you want to send an email with database attachment?.");

                                String positiveText = "YES";
                                builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // positive button logic
                                        // sendEmailWithDBAttachment(getActivity());
                                        Utils.sendFile(getActivity());
                                        dialog.dismiss();

                                    }
                                });

                                String negativeText = "NO";
                                builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // negative button logic
                                        dialog.dismiss();
                                    }
                                });

                                AlertDialog dialog = builder.create();
                                // display dialog
                                dialog.show();

                                TextView dialogMessage = (TextView) dialog.findViewById(android.R.id.message);
                                dialogMessage.setTypeface(LoginActivity.sTypeface);

                                Button yesButton = (Button) dialog.findViewById(android.R.id.button1);
                                yesButton.setTypeface(LoginActivity.sTypeface);

                                Button noButton = (Button) dialog.findViewById(android.R.id.button2);
                                noButton.setTypeface(LoginActivity.sTypeface);
                            } else {
                                TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg, TastyToast.LENGTH_SHORT,
                                        TastyToast.WARNING);
                            }
                        }
//                    }
//                        else
//                        {
//                            Toast.makeText(getActivity(),"Transaction can be done tomorrow",Toast.LENGTH_SHORT).show();
//                        }


                  /*  switch (dto.getName()) {
                        case NewDrawerScreen.MOBILE_NUMBER_UPDATION:
                            NewDrawerScreen.showFragment(new ProfileMobileUpdate());


                        case NewDrawerScreen.CREDIT_LINKAGE_INFO:
                            NewDrawerScreen.showFragment(new CreditLinkage());


                        case NewDrawerScreen.ANIMATOR_PROFILE:
                            NewDrawerScreen.showFragment(new Animator());


                        case NewDrawerScreen.GROUP_PROFILE:
                            NewDrawerScreen.showFragment(new GroupProfile());


                        case NewDrawerScreen.SAVINGS:
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.SAVINGS);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        case NewDrawerScreen.GROUP_REPORTS:
                            NewDrawerScreen.showFragment(new GroupReportFragment());


                        case NewDrawerScreen.MEMBER_ACCOUNT_NUMBER_UPDATION:
                            NewDrawerScreen.showFragment(new MemberAcountNumberUpdationFragment());


                        case NewDrawerScreen.MEMBER_REPORTS:
                            NewDrawerScreen.showFragment(new MemberReportFragment());


                        case NewDrawerScreen.BANK_TRANSACTION_SUMMARY:
                            NewDrawerScreen.showFragment(new Reports_BankTransactionSummary());

                        case NewDrawerScreen.SHG_ACCOUNT_NUMBER_UPDATION:
                            NewDrawerScreen.showFragment(new Profile_SHGAccountnumber_updation());

                        case NewDrawerScreen.AUDITING:
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.AUDITING);

                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }



                        case NewDrawerScreen.DE_ACTIVATE_ACCOUNT:
                            NewDrawerScreen.showFragment(new DeactivateAccount());
                            //    NewDrawerScreen.showFragment(new ());
                            //  NewDrawerScreen.showFragment(new Meeting_audit_Fragment());


                        case NewDrawerScreen.CHANGE_PASSWORD:
                            NewDrawerScreen.showFragment(new Settings_Change_Password_Fragment());


                        case NewDrawerScreen.CONTACTS:
                            NewDrawerScreen.showFragment(new Contacts());


                        case NewDrawerScreen.PDF_MANUAL:

                            CopyReadAssets();

                              *//*  isPdfDownload = true;

                                if (networkConnection.isNetworkAvailable()) {
                                    new DownloadPdfFileTask(MainFragment.this, ServiceType.PDF_DOWNLOAD).execute();
                                } else {

                                }*//*
                        //

                        // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);

                        case NewDrawerScreen.CHANGE_LANGUAGE:
                            Log.d("ChangeLanguage", "working");
                            if (networkConnection.isNetworkAvailable()) {
                                onTaskStarted();
                                RestClient.getRestClient(MainFragment.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GET_LANGUAGE + MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""), getActivity(), ServiceType.GETLANGUAGE);
                            }



                        case NewDrawerScreen.INCOME:
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.INCOME);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        case NewDrawerScreen.ESHKTHI:
                            Log.d("profileCheck", "working");
                            NewDrawerScreen.showFragment(new EShaktiApp());
                            // Utils.startActivity(getActivity(), Profile_MobileNumber_Updation.class);



                        case NewDrawerScreen.EXPENCE:
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.EXPENCE);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        case NewDrawerScreen.BANK_TRANSACTION:
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.BANK_TRANSACTION);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        case NewDrawerScreen.LOAN_DISBURSEMENT:
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.LOAN_DISBURSEMENT);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        case NewDrawerScreen.BANK_BALANCE:
                            NewDrawerScreen.showFragment(new Reports_BankBalance_Fragment());


                        case NewDrawerScreen.SELECT_GROUP:
                            startActivity(new Intent(getActivity(), SHGGroupActivity.class));
                            getActivity().finish();


                        case NewDrawerScreen.MEMBER_LOAN_REPAYMENT:
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.MEMBER_LOAN_REPAYMENT);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        case NewDrawerScreen.GROUP_LOAN_REPAYMENT:
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.GROUP_LOAN_REPAYMENT);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        case NewDrawerScreen.MINUTES_OF_MEETINGS:


                        case NewDrawerScreen.TRAINING:
                            Log.d("profileCheck", "working");

                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.TRAINING);

                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        case NewDrawerScreen.ATTENDANCE:
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.ATTENDANCE);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }



                        case NewDrawerScreen.STEP_WISE:
                            try {
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.STEP_WISE);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        case NewDrawerScreen.CHECK_LIST:
                            NewDrawerScreen.showFragment(new CheckList());

                    }*/

                }

            }
        }));
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }


    private void CopyReadAssets() {
        // File file = new File(Environment.getExternalStorageDirectory(), "mobileapplication.pdf");
        try {
            AssetManager assetManager = getActivity().getAssets();
            InputStream in = null;
            OutputStream out = null;
            File fileAssets = new File(getActivity().getFilesDir(), "mobileapplication.pdf");
            try {
                in = assetManager.open("mobileapplication.pdf");
                out = getActivity().openFileOutput(fileAssets.getName(), Context.MODE_PRIVATE);
                copyFile(in, out);
                in.close();
                in = null;
                out.flush();
                out.close();
                out = null;
            } catch (Exception e) {
                Log.e("tag", e.getMessage());
            }


            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri fileUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", fileAssets);
            intent.setDataAndType(fileUri, "application/pdf");
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

            Intent intent1 = Intent.createChooser(intent, "Open File");

            PackageManager pm = getActivity().getPackageManager();
            if (intent.resolveActivity(pm) != null) {
                startActivity(intent1);
            }

        } catch (ActivityNotFoundException e) {
            // TODO: handle exception
            TastyToast.makeText(getActivity(), "No pdf viewer installed, please install any pdf viewer.",
                    TastyToast.LENGTH_SHORT, TastyToast.ERROR);
        }
    }


    @Override
    public void onClick(View view) {

        if (networkConnection.isNetworkAvailable()) {
            if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 1) {
                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                return;
            }
            // NOTHING TO DO::
        } else {

            if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 2) {
                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                return;
            }

        }



        switch (view.getId()) {
            case R.id.mTransaction:
                dashboardDtos.clear();
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.MENU_SELECTED, 1);
                if (networkConnection.isNetworkAvailable()) {
                    transactionsSubMenu();
                } else {
                    transactionsoffSubMenu();
                }
                break;

            case R.id.mProfile:
                dashboardDtos.clear();
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.MENU_SELECTED, 2);
                if (networkConnection.isNetworkAvailable()) {
                    profileSubMenu();
                } else {
                    profileOffSubMenu();
                }
                break;

            case R.id.mReports:
                dashboardDtos.clear();
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.MENU_SELECTED, 3);
                if (networkConnection.isNetworkAvailable()) {
                    reportsSubMenu();
                } else {
                    reportsOffSubMenu();
                }
                break;

            case R.id.mMeeting:
                dashboardDtos.clear();
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.MENU_SELECTED, 4);
                if (networkConnection.isNetworkAvailable())
                    meetingsSubMenu();
                else
                    meetingsOffSubMenu();
                break;

            case R.id.mSeeting:
                dashboardDtos.clear();
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.MENU_SELECTED, 5);
                if (networkConnection.isNetworkAvailable())
                    settingsSubMenu();
                else
                    settingsOffSubMenu();
                break;

            case R.id.mHelp:
                dashboardDtos.clear();
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.MENU_SELECTED, 6);
                if (networkConnection.isNetworkAvailable())
                    helpSubMenu();
                else
                    helpOffSubMenu();
                break;
            case R.id.mCheckbacklog:
                //   dashboardDtos.clear();
                TastyToast.makeText(getActivity(), AppStrings.mCheckBackLogOffline,
                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);

                break;

        }
    }

    private void helpOffSubMenu() {
        DashboardDto eshakthi = new DashboardDto();
        DashboardDto contacts = new DashboardDto();
        DashboardDto pdfmanual = new DashboardDto();
        DashboardDto videos = new DashboardDto();


        eshakthi.setName(AppStrings.ESHKTHI1);
        eshakthi.setImage(R.drawable.aboutusn);
        dashboardDtos.add(eshakthi);

        contacts.setName(AppStrings.contacts);
        contacts.setImage(R.drawable.contactn);
        dashboardDtos.add(contacts);

        pdfmanual.setName(AppStrings.mPdfManual);
        pdfmanual.setImage(R.drawable.aboutusn);
        dashboardDtos.add(pdfmanual);

        videos.setName(AppStrings.mvideos);
        videos.setImage(R.drawable.aboutusn);
        dashboardDtos.add(videos);


        Log.d("dashboardDtos", " P " + dashboardDtos.size());
        dashboardAdapter = new DashboardAdapter(getActivity(), dashboardDtos);
        recyclerView_dash_board.setAdapter(dashboardAdapter);

        mCheckbacklog.setVisibility(View.GONE);
        mRecyclerviewlayout.setVisibility(View.VISIBLE);
        mDashItemname.setText(AppStrings.help);
        mDashItemnamelayout.setBackground(getResources().getDrawable(R.drawable.curve_help));
    }

    private void settingsOffSubMenu() {
        DashboardDto changepassword = new DashboardDto();

        changepassword.setName(AppStrings.passwordchange);
        changepassword.setImage(R.drawable.changepasswordn);
        dashboardDtos.add(changepassword);

        Log.d("dashboardDtos", " P " + dashboardDtos.size());

        dashboardAdapter = new DashboardAdapter(getActivity(), dashboardDtos);
        recyclerView_dash_board.setAdapter(dashboardAdapter);

        mCheckbacklog.setVisibility(View.GONE);
        mRecyclerviewlayout.setVisibility(View.VISIBLE);
        mDashItemname.setText(AppStrings.settings);
        mDashItemnamelayout.setBackground(getResources().getDrawable(R.drawable.curve_setting));
    }

    private void meetingsOffSubMenu() {
        DashboardDto attendance = new DashboardDto();
        DashboardDto minutesofmeeting = new DashboardDto();
        DashboardDto auditing = new DashboardDto();
        DashboardDto training = new DashboardDto();
        DashboardDto uploadschedule = new DashboardDto();
        DashboardDto uploadpassbook = new DashboardDto();


        attendance.setName(AppStrings.Attendance);
        attendance.setImage(R.drawable.attendancen);
        dashboardDtos.add(attendance);

        minutesofmeeting.setName(AppStrings.MinutesofMeeting);
        minutesofmeeting.setImage(R.drawable.minutesn);
        dashboardDtos.add(minutesofmeeting);


        auditing.setName(AppStrings.auditing);
        auditing.setImage(R.drawable.auditn);
        dashboardDtos.add(auditing);

        training.setName(AppStrings.training);
        training.setImage(R.drawable.trainingn);
        dashboardDtos.add(training);

        uploadschedule.setName(AppStrings.upload_schedule);
        uploadschedule.setImage(R.drawable.trainingn);
        dashboardDtos.add(uploadschedule);

        uploadpassbook.setName(AppStrings.upload_passbook);
        uploadpassbook.setImage(R.drawable.trainingn);
        dashboardDtos.add(uploadpassbook);

        Log.d("dashboardDtos", " P " + dashboardDtos.size());

        dashboardAdapter = new DashboardAdapter(getActivity(), dashboardDtos);
        recyclerView_dash_board.setAdapter(dashboardAdapter);


        mCheckbacklog.setVisibility(View.GONE);
        mRecyclerviewlayout.setVisibility(View.VISIBLE);
        mDashItemname.setText(AppStrings.meeting);
        mDashItemnamelayout.setBackground(getResources().getDrawable(R.drawable.curve_meeting));
    }

    private void reportsOffSubMenu() {

        DashboardDto offlinereports = new DashboardDto();


        offlinereports.setName(AppStrings.offlineReports);
        offlinereports.setImage(R.drawable.trialbalancen);
        dashboardDtos.add(offlinereports);

        Log.d("dashboardDtos", " P " + dashboardDtos.size());

        dashboardAdapter = new DashboardAdapter(getActivity(), dashboardDtos);
        recyclerView_dash_board.setAdapter(dashboardAdapter);

        mCheckbacklog.setVisibility(View.GONE);
        mRecyclerviewlayout.setVisibility(View.VISIBLE);
        mDashItemname.setText(AppStrings.reports);
        mDashItemnamelayout.setBackground(getResources().getDrawable(R.drawable.curve_report));
    }

    private void profileOffSubMenu() {
        DashboardDto socialsecurity = new DashboardDto();
        DashboardDto creditlinkage = new DashboardDto();
        DashboardDto mobilenumber = new DashboardDto();
        DashboardDto aadharnumber = new DashboardDto();
        DashboardDto memberaccountnumber = new DashboardDto();
        DashboardDto shgsbacuploadpassbook = new DashboardDto();
        DashboardDto shgaccountnumber = new DashboardDto();
        DashboardDto memberdetails = new DashboardDto();
        DashboardDto groupprofile = new DashboardDto();
        DashboardDto animatorprofile = new DashboardDto();
        Log.d("dashboardDtos", " P " + dashboardDtos.size());

        socialsecurity.setName(AppStrings.mSocialsecurity);
        socialsecurity.setImage(R.drawable.adhaarn);
        dashboardDtos.add(socialsecurity);

        creditlinkage.setName(AppStrings.mCreditLinkageInfo);
        creditlinkage.setImage(R.drawable.adhaarn);
        dashboardDtos.add(creditlinkage);

        mobilenumber.setName(AppStrings.mMobileNoUpdation);
        mobilenumber.setImage(R.drawable.adhaarn);
        dashboardDtos.add(mobilenumber);

        aadharnumber.setName(AppStrings.mAadhaarNoUpdation);
        aadharnumber.setImage(R.drawable.adhaarn);
        dashboardDtos.add(aadharnumber);

        memberaccountnumber.setName(AppStrings.mAccountNoUpdation);
        memberaccountnumber.setImage(R.drawable.adhaarn);
        dashboardDtos.add(memberaccountnumber);

        shgsbacuploadpassbook.setName(AppStrings.mshgsbacuploadpassbook);
        shgsbacuploadpassbook.setImage(R.drawable.adhaarn);
        dashboardDtos.add(shgsbacuploadpassbook);

        shgaccountnumber.setName(AppStrings.mSHGAccountNoUpdation);
        shgaccountnumber.setImage(R.drawable.adhaarn);
        dashboardDtos.add(shgaccountnumber);

        memberdetails.setName(AppStrings.uploadInfo);
        memberdetails.setImage(R.drawable.adhaarn);
        dashboardDtos.add(memberdetails);

        groupprofile.setName(AppStrings.groupProfile);
        groupprofile.setImage(R.drawable.groupprofilen);
        dashboardDtos.add(groupprofile);

        animatorprofile.setName(AppStrings.agentProfile);
        animatorprofile.setImage(R.drawable.agentprofilen);
        dashboardDtos.add(animatorprofile);




        Log.d("dashboardDtos", " P " + dashboardDtos.size());

        dashboardAdapter = new DashboardAdapter(getActivity(), dashboardDtos);
        recyclerView_dash_board.setAdapter(dashboardAdapter);

        mCheckbacklog.setVisibility(View.GONE);
        mRecyclerviewlayout.setVisibility(View.VISIBLE);
        mDashItemname.setText(AppStrings.profile);
        mDashItemnamelayout.setBackground(getResources().getDrawable(R.drawable.curve_profile));
    }

    private void transactionsoffSubMenu() {
        DashboardDto transaction_incomn = new DashboardDto();
        DashboardDto transaction_saveings = new DashboardDto();
        DashboardDto transaction_expenses = new DashboardDto();
        DashboardDto transaction_memberloan = new DashboardDto();
        DashboardDto transaction_grouploan = new DashboardDto();
        DashboardDto transaction_banktransaction = new DashboardDto();
        DashboardDto transaction_loandisbursement = new DashboardDto();
        DashboardDto transaction_stepwise = new DashboardDto();


        transaction_saveings.setName(AppStrings.savings);
        transaction_saveings.setImage(R.drawable.savingsn);
        dashboardDtos.add(transaction_saveings);

        transaction_incomn.setName(AppStrings.income);
        transaction_incomn.setImage(R.drawable.loanrepaymentn);
        dashboardDtos.add(transaction_incomn);

        transaction_expenses.setName(AppStrings.expenses);
        transaction_expenses.setImage(R.drawable.expensesn);
        dashboardDtos.add(transaction_expenses);

        transaction_memberloan.setName(AppStrings.memberloanrepayment);
        transaction_memberloan.setImage(R.drawable.incomen);
        dashboardDtos.add(transaction_memberloan);

        transaction_grouploan.setName(AppStrings.grouploanrepayment);
        transaction_grouploan.setImage(R.drawable.grouploanrepaymentn);
        dashboardDtos.add(transaction_grouploan);

        transaction_banktransaction.setName(AppStrings.bankTransaction);
        transaction_banktransaction.setImage(R.drawable.banksavingsn);
        dashboardDtos.add(transaction_banktransaction);

        transaction_loandisbursement.setName(AppStrings.InternalLoanDisbursement);
        transaction_loandisbursement.setImage(R.drawable.loandisbursementn);
        dashboardDtos.add(transaction_loandisbursement);

        transaction_stepwise.setName(AppStrings.mDefault);
        transaction_stepwise.setImage(R.drawable.savingsn);
        dashboardDtos.add(transaction_stepwise);


        Log.d("dashboardDtos", " " + dashboardDtos.size());
        dashboardAdapter = new DashboardAdapter(getActivity(), dashboardDtos);
        recyclerView_dash_board.setAdapter(dashboardAdapter);

        mCheckbacklog.setVisibility(View.GONE);
        mRecyclerviewlayout.setVisibility(View.VISIBLE);
        mDashItemname.setText(AppStrings.transaction);
        mDashItemnamelayout.setBackground(getResources().getDrawable(R.drawable.curve_lite_blue));
    }

    private void helpSubMenu() {
        DashboardDto eshakthi = new DashboardDto();
        DashboardDto contacts = new DashboardDto();
        DashboardDto pdfmanual = new DashboardDto();
        DashboardDto videos = new DashboardDto();


        eshakthi.setName(AppStrings.ESHKTHI1);
        eshakthi.setImage(R.drawable.aboutusn);
        dashboardDtos.add(eshakthi);

        contacts.setName(AppStrings.contacts);
        contacts.setImage(R.drawable.contactn);
        dashboardDtos.add(contacts);

        pdfmanual.setName(AppStrings.mPdfManual);
        pdfmanual.setImage(R.drawable.aboutusn);
        dashboardDtos.add(pdfmanual);

        videos.setName(AppStrings.mvideos);
        videos.setImage(R.drawable.aboutusn);
        dashboardDtos.add(videos);


        Log.d("dashboardDtos", " P " + dashboardDtos.size());
        dashboardAdapter = new DashboardAdapter(getActivity(), dashboardDtos);
        recyclerView_dash_board.setAdapter(dashboardAdapter);

        mCheckbacklog.setVisibility(View.GONE);
        mRecyclerviewlayout.setVisibility(View.VISIBLE);
        mDashItemname.setText(AppStrings.help);
        mDashItemnamelayout.setBackground(getResources().getDrawable(R.drawable.curve_help));
    }

    private void settingsSubMenu() {
        DashboardDto changepassword = new DashboardDto();
        DashboardDto changelanguage = new DashboardDto();
        DashboardDto deactivateaccount = new DashboardDto();
        DashboardDto email = new DashboardDto();


        changepassword.setName(AppStrings.passwordchange);
        changepassword.setImage(R.drawable.changepasswordn);
        dashboardDtos.add(changepassword);

        changelanguage.setName(AppStrings.changeLanguage);
        changelanguage.setImage(R.drawable.changelanguaguen);
        dashboardDtos.add(changelanguage);

        deactivateaccount.setName(AppStrings.deactivateAccount);
        deactivateaccount.setImage(R.drawable.trainingn);
        dashboardDtos.add(deactivateaccount);

        email.setName(AppStrings.mSendEmail);
        email.setImage(R.drawable.trainingn);
        dashboardDtos.add(email);


        Log.d("dashboardDtos", " P " + dashboardDtos.size());

        dashboardAdapter = new DashboardAdapter(getActivity(), dashboardDtos);
        recyclerView_dash_board.setAdapter(dashboardAdapter);

        mCheckbacklog.setVisibility(View.GONE);
        mRecyclerviewlayout.setVisibility(View.VISIBLE);
        mDashItemname.setText(AppStrings.settings);
        mDashItemnamelayout.setBackground(getResources().getDrawable(R.drawable.curve_setting));
    }

    private void meetingsSubMenu() {
        DashboardDto attendance = new DashboardDto();
        DashboardDto minutesofmeeting = new DashboardDto();
        DashboardDto auditing = new DashboardDto();
        DashboardDto training = new DashboardDto();
        DashboardDto uploadschedule = new DashboardDto();
        DashboardDto uploadpassbook = new DashboardDto();

        attendance.setName(AppStrings.Attendance);
        attendance.setImage(R.drawable.attendancen);
        dashboardDtos.add(attendance);

        minutesofmeeting.setName(AppStrings.MinutesofMeeting);
        minutesofmeeting.setImage(R.drawable.minutesn);
        dashboardDtos.add(minutesofmeeting);


        auditing.setName(AppStrings.auditing);
        auditing.setImage(R.drawable.auditn);
        dashboardDtos.add(auditing);

        training.setName(AppStrings.training);
        training.setImage(R.drawable.trainingn);
        dashboardDtos.add(training);

        uploadschedule.setName(AppStrings.upload_schedule);
        uploadschedule.setImage(R.drawable.trainingn);
        dashboardDtos.add(uploadschedule);

        uploadpassbook.setName(AppStrings.upload_passbook);
        uploadpassbook.setImage(R.drawable.trainingn);
        dashboardDtos.add(uploadpassbook);

        Log.d("dashboardDtos", " P " + dashboardDtos.size());

        dashboardAdapter = new DashboardAdapter(getActivity(), dashboardDtos);
        recyclerView_dash_board.setAdapter(dashboardAdapter);


        mCheckbacklog.setVisibility(View.GONE);
        mRecyclerviewlayout.setVisibility(View.VISIBLE);
        mDashItemname.setText(AppStrings.meeting);
        mDashItemnamelayout.setBackground(getResources().getDrawable(R.drawable.curve_meeting));
    }

    private void reportsSubMenu() {
        DashboardDto memeberreports = new DashboardDto();
        DashboardDto groupreports = new DashboardDto();
        DashboardDto banktransaction = new DashboardDto();
        DashboardDto bankbalance = new DashboardDto();
        DashboardDto offlinereports = new DashboardDto();

        memeberreports.setName(AppStrings.Memberreports);
        memeberreports.setImage(R.drawable.memberreportsn);
        dashboardDtos.add(memeberreports);

        groupreports.setName(AppStrings.GroupReports);
        groupreports.setImage(R.drawable.groupreportsn);
        dashboardDtos.add(groupreports);


        banktransaction.setName(AppStrings.bankTransactionSummary);
        banktransaction.setImage(R.drawable.banktransactionn);
        dashboardDtos.add(banktransaction);

        bankbalance.setName(AppStrings.bankBalance);
        bankbalance.setImage(R.drawable.bankbalancen);
        dashboardDtos.add(bankbalance);

        offlinereports.setName(AppStrings.offlineReports);
        offlinereports.setImage(R.drawable.trialbalancen);
        dashboardDtos.add(offlinereports);

        Log.d("dashboardDtos", " P " + dashboardDtos.size());

        dashboardAdapter = new DashboardAdapter(getActivity(), dashboardDtos);
        recyclerView_dash_board.setAdapter(dashboardAdapter);

        mCheckbacklog.setVisibility(View.GONE);
        mRecyclerviewlayout.setVisibility(View.VISIBLE);
        mDashItemname.setText(AppStrings.reports);
        mDashItemnamelayout.setBackground(getResources().getDrawable(R.drawable.curve_report));
    }

    private void profileSubMenu() {

        DashboardDto socialsecurity = new DashboardDto();
        DashboardDto creditlinkage = new DashboardDto();
        DashboardDto mobilenumber = new DashboardDto();
        DashboardDto aadharnumber = new DashboardDto();
        DashboardDto memberaccountnumber = new DashboardDto();
        DashboardDto shgsbacuploadpassbook = new DashboardDto();
        DashboardDto shgaccountnumber = new DashboardDto();
        DashboardDto memberdetails = new DashboardDto();
        DashboardDto groupprofile = new DashboardDto();
        DashboardDto animatorprofile = new DashboardDto();
        //DashboardDto presidentrotation = new DashboardDto();
        Log.d("dashboardDtos", " P " + dashboardDtos.size());


        socialsecurity.setName(AppStrings.mSocialsecurity);
        socialsecurity.setImage(R.drawable.adhaarn);
        dashboardDtos.add(socialsecurity);

        creditlinkage.setName(AppStrings.mCreditLinkageInfo);
        creditlinkage.setImage(R.drawable.adhaarn);
        dashboardDtos.add(creditlinkage);

        mobilenumber.setName(AppStrings.mMobileNoUpdation);
        mobilenumber.setImage(R.drawable.adhaarn);
        dashboardDtos.add(mobilenumber);

        aadharnumber.setName(AppStrings.mAadhaarNoUpdation);
        aadharnumber.setImage(R.drawable.adhaarn);
        dashboardDtos.add(aadharnumber);

        memberaccountnumber.setName(AppStrings.mAccountNoUpdation);
        memberaccountnumber.setImage(R.drawable.adhaarn);
        dashboardDtos.add(memberaccountnumber);

        shgsbacuploadpassbook.setName(AppStrings.mshgsbacuploadpassbook);
        shgsbacuploadpassbook.setImage(R.drawable.adhaarn);
        dashboardDtos.add(shgsbacuploadpassbook);

        shgaccountnumber.setName(AppStrings.mSHGAccountNoUpdation);
        shgaccountnumber.setImage(R.drawable.adhaarn);
        dashboardDtos.add(shgaccountnumber);

        memberdetails.setName(AppStrings.uploadInfo);
        memberdetails.setImage(R.drawable.adhaarn);
        dashboardDtos.add(memberdetails);

        groupprofile.setName(AppStrings.groupProfile);
        groupprofile.setImage(R.drawable.groupprofilen);
        dashboardDtos.add(groupprofile);

        animatorprofile.setName(AppStrings.agentProfile);
        animatorprofile.setImage(R.drawable.agentprofilen);
        dashboardDtos.add(animatorprofile);

        /*presidentrotation.setName(AppStrings.presidentRotation);
        presidentrotation.setImage(R.drawable.adhaarn);
        dashboardDtos.add(presidentrotation);*/


        Log.d("dashboardDtos", " P " + dashboardDtos.size());

        dashboardAdapter = new DashboardAdapter(getActivity(), dashboardDtos);
        recyclerView_dash_board.setAdapter(dashboardAdapter);

        mCheckbacklog.setVisibility(View.GONE);
        mRecyclerviewlayout.setVisibility(View.VISIBLE);
        mDashItemname.setText(AppStrings.profile);
        mDashItemnamelayout.setBackground(getResources().getDrawable(R.drawable.curve_profile));
    }

    private void transactionsSubMenu() {
        DashboardDto transaction_incomn = new DashboardDto();
        DashboardDto transaction_saveings = new DashboardDto();
        DashboardDto transaction_expenses = new DashboardDto();
        DashboardDto transaction_memberloan = new DashboardDto();
        DashboardDto transaction_grouploan = new DashboardDto();
        DashboardDto transaction_banktransaction = new DashboardDto();
        DashboardDto transaction_loandisbursement = new DashboardDto();
        DashboardDto transaction_stepwise = new DashboardDto();
        DashboardDto transaction_checklist = new DashboardDto();


        transaction_saveings.setName(AppStrings.savings);
        transaction_saveings.setImage(R.drawable.savingsn);
        dashboardDtos.add(transaction_saveings);

        transaction_incomn.setName(AppStrings.income);
        transaction_incomn.setImage(R.drawable.loanrepaymentn);
        dashboardDtos.add(transaction_incomn);

        transaction_expenses.setName(AppStrings.expenses);
        transaction_expenses.setImage(R.drawable.expensesn);
        dashboardDtos.add(transaction_expenses);

        transaction_memberloan.setName(AppStrings.memberloanrepayment);
        transaction_memberloan.setImage(R.drawable.incomen);
        dashboardDtos.add(transaction_memberloan);

        transaction_grouploan.setName(AppStrings.grouploanrepayment);
        transaction_grouploan.setImage(R.drawable.grouploanrepaymentn);
        dashboardDtos.add(transaction_grouploan);

        transaction_banktransaction.setName(AppStrings.bankTransaction);
        transaction_banktransaction.setImage(R.drawable.banksavingsn);
        dashboardDtos.add(transaction_banktransaction);

        transaction_loandisbursement.setName(AppStrings.InternalLoanDisbursement);
        transaction_loandisbursement.setImage(R.drawable.loandisbursementn);
        dashboardDtos.add(transaction_loandisbursement);

        transaction_stepwise.setName(AppStrings.mDefault);
        transaction_stepwise.setImage(R.drawable.savingsn);
        dashboardDtos.add(transaction_stepwise);

        transaction_checklist.setName(AppStrings.mCheckList);
        transaction_checklist.setImage(R.drawable.savingsn);
        dashboardDtos.add(transaction_checklist);


        Log.d("dashboardDtos", " " + dashboardDtos.size());
        dashboardAdapter = new DashboardAdapter(getActivity(), dashboardDtos);
        recyclerView_dash_board.setAdapter(dashboardAdapter);

        mCheckbacklog.setVisibility(View.GONE);
        mRecyclerviewlayout.setVisibility(View.VISIBLE);
        mDashItemname.setText(AppStrings.transaction);
        mDashItemnamelayout.setBackground(getResources().getDrawable(R.drawable.curve_lite_blue));
    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;
        }

        switch (serviceType) {
            case NAV_DETAILS:
                try {
                    if (result != null && result.length() > 0) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        ResponseDto mrDto = gson.fromJson(result, ResponseDto.class);
                        int statusCode = mrDto.getStatusCode();
                        Log.d("Main Frag response ", " " + statusCode);
                        if (statusCode == 400 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                            Utils.showToast(getActivity(),mrDto.getMessage());

                        } else if (statusCode == 401) {
                            Utils.showToast(getActivity(),mrDto.getMessage());
                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        } else if (statusCode == Utils.Success_Code) {
                        //    Utils.showToast(getActivity(),mrDto.getMessage());
                            ArrayList<CashOfGroup> csGrp = mrDto.getResponseContent().getCashOfGroup();
                            SHGTable.updateSHGDetails(csGrp.get(0), shgDetails.getId());
                            passData(SHGTable.getSHGDetails(shgDetails.getShgId()));
//                            init();
//                            dashboardDtos.clear();
//                            transactionsSubMenu();

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case GETLANGUAGE:
                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    final ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                    String message = lrDto.getMessage();
                    int statusCode = lrDto.getStatusCode();


                    Log.d("response status", " " + statusCode);
                    if (statusCode == 400 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                        Utils.showToast(getActivity(), message);
                    } else if (statusCode == 401) {

                        Log.e("Group Logout", "Logout Sucessfully");
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                    } else {
                        Utils.showToast(getActivity(), message);

                        if (lrDto != null && lrDto.getResponseContent().getLanguageList() != null && lrDto.getResponseContent().getLanguageList().size() > 0) {

                            final ArrayList<TrainingsList> langList = lrDto.getResponseContent().getLanguageList();

                            for (TrainingsList ls : langList) {
                                if (!ls.getName().equals("English")) {
                                    EShaktiApplication.setUser_RegLanguage(ls.getName());
                                }
                            }


                            final Dialog ChangeLanguageDialog = new Dialog(getActivity());

                            LayoutInflater li = (LayoutInflater) getActivity()
                                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View dialogView = li.inflate(R.layout.dialog_new_choose_language, null,
                                    false);

                            TextView confirmationHeader = (TextView) dialogView
                                    .findViewById(R.id.chooseLanguageHeader);
                            confirmationHeader.setText(RegionalConversion
                                    .getRegionalConversion(AppStrings.chooseLanguage));
                            confirmationHeader.setTypeface(LoginActivity.sTypeface);
                            final RadioGroup radioGroup = (RadioGroup) dialogView
                                    .findViewById(R.id.radioLanguage);
                            RadioButton radioButton = (RadioButton) dialogView
                                    .findViewById(R.id.radioEnglish);
                            RadioButton radioButton_reg = (RadioButton) dialogView
                                    .findViewById(R.id.radioRegional);
                            radioButton.setText("English");
                            //   radioButton.setTypeface(LoginActivity.sTypeface);
                            if (EShaktiApplication.getUser_RegLanguage() != null) {
                                radioButton_reg.setVisibility(View.VISIBLE);
                                if (EShaktiApplication.getUser_RegLanguage().equals("Hindi")) {
                                    radioButton_reg.setText("हिंदी");
                                    Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/MANGAL.TTF");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Marathi")) {
                                    radioButton_reg.setText("मराठी");
                                    Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/MANGALHindiMarathi.TTF");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Kannada")) {
                                    radioButton_reg.setText("ಕನ್ನಡ");
                                    Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/tungaKannada.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Malayalam")) {
                                    radioButton_reg.setText("മലയാളം");
                                    Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/MLKR0nttMalayalam.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Punjabi")) {
                                    radioButton_reg.setText("ਪੰਜਾਬੀ ");
                                    Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/mangal-1361510185.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Gujarathi")) {
                                    radioButton_reg.setText("ગુજરાતી");
                                    Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/shrutiGujarathi.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Bengali")) {
                                    radioButton_reg.setText("বাঙ্গালী");
                                    Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/kalpurushBengali.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Tamil")) {
                                    radioButton_reg.setText("தமிழ்");
                                    Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/TSCu_SaiIndira.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Assamese")) {
                                    radioButton_reg.setText("Assamese");
                                    Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/KirtanUni_Assamese.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else {
                                    radioButton_reg.setText("ENGLISH");//TODO::
                                    Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/Exo-Medium.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                }
                            } else {
                                radioButton_reg.setVisibility(View.GONE);
                            }
                            // radioButton_reg.setTypeface(LoginActivity.sTypeface);

                            ButtonFlat okButton = (ButtonFlat) dialogView
                                    .findViewById(R.id.dialog_yes_button);
                            okButton.setText(
                                    RegionalConversion.getRegionalConversion(AppStrings.dialogOk));
                            okButton.setTypeface(LoginActivity.sTypeface);
                            okButton.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated
                                    // method
                                    // stub

                                    int selectedId = radioGroup.getCheckedRadioButtonId();
                                    RadioButton radioLanguageButton = (RadioButton) dialogView
                                            .findViewById(selectedId);
                                    String mSelectedLang = radioLanguageButton.getText().toString();                                    // find the radiobutton by

                                    Log.v("On Selected language", mSelectedLang);
                                    try {
                                        /**
                                         * THE LANGUAGE VALUE INSERTS INTO PREFERENCE
                                         **/
                                        if (mSelectedLang.equals("हिंदी")) {
                                            mSelectedLang = "Hindi";
                                        } else if (mSelectedLang.equals("मराठी")) {
                                            mSelectedLang = "Marathi";
                                        } else if (mSelectedLang.equals("ಕನ್ನಡ")) {
                                            mSelectedLang = "Kannada";
                                        } else if (mSelectedLang.equals("മലയാളം")) {
                                            mSelectedLang = "Malayalam";
                                        } else if (mSelectedLang.equals("ਪੰਜਾਬੀ")) {
                                            mSelectedLang = "Punjabi";
                                        } else if (mSelectedLang.equals("ગુજરાતી")) {
                                            mSelectedLang = "Gujarathi";
                                        } else if (mSelectedLang.equals("বাঙ্গালী")) {
                                            mSelectedLang = "Bengali";
                                        } else if (mSelectedLang.equals("தமிழ்")) {
                                            mSelectedLang = "Tamil";
                                        } else if (mSelectedLang.equals("Assamese")) {
                                            mSelectedLang = "Assamese";
                                        }

                                        for (TrainingsList ls : langList) {
                                            if (mSelectedLang.equals("हिंदी")) {
                                                mSelectedLang = "Hindi";
                                            } else if (mSelectedLang.equals("मराठी")) {
                                                mSelectedLang = "Marathi";
                                            } else if (mSelectedLang.equals("ಕನ್ನಡ")) {
                                                mSelectedLang = "Kannada";
                                            } else if (mSelectedLang.equals("മലയാളം")) {
                                                mSelectedLang = "Malayalam";
                                            } else if (mSelectedLang.equals("ਪੰਜਾਬੀ")) {
                                                mSelectedLang = "Punjabi";
                                            } else if (mSelectedLang.equals("ગુજરાતી")) {
                                                mSelectedLang = "Gujarathi";
                                            } else if (mSelectedLang.equals("বাঙ্গালী")) {
                                                mSelectedLang = "Bengali";
                                            } else if (mSelectedLang.equals("தமிழ்")) {
                                                mSelectedLang = "Tamil";
                                            } else if (mSelectedLang.equals("Assamese")) {
                                                mSelectedLang = "Assamese";
                                            }
                                            if (ls.getName().equals(mSelectedLang)) {
                                                MySharedPreference.writeString(getActivity(), MySharedPreference.LANG_ID, ls.getId());
                                            }
                                        }
                                        MySharedPreference.writeString(getActivity(), MySharedPreference.LANG, mSelectedLang);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    LoginActivity.sTypeface = GetTypeface
                                            .getTypeface(getActivity(), mSelectedLang);

                                    Intent intent_login = new Intent(getActivity(),
                                            SHGGroupActivity.class);
                                    intent_login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent_login);
                                    getActivity().overridePendingTransition(R.anim.right_to_left_in,
                                            R.anim.right_to_left_out);
                                    getActivity().finish();

                      /*  RegionalserviceUtil.getRegionalService(LoginActivity.this,
                                mSelectedLang);*/

                                    // isLanguageSelection =
                                    // true;


                                    ChangeLanguageDialog.dismiss();


                                }
                            });

                            ChangeLanguageDialog.getWindow().setBackgroundDrawable(
                                    new ColorDrawable(Color.TRANSPARENT));
                            ChangeLanguageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            ChangeLanguageDialog.setCanceledOnTouchOutside(false);
                            ChangeLanguageDialog.setContentView(dialogView);
                            ChangeLanguageDialog.setCancelable(false);
                            ChangeLanguageDialog.show();
                        }

                    }

                }


                break;

            case FD_VALUE:

                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);

                        amount = cdto.getResponseContent().getSavingsBalance().getCurrentBalance();
                        init();

                    } else {
                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), message);

                    }
                } catch (Exception e) {

                }
                break;
        }
    }


    private void CalendarstepwiseDialogShow()
    {
        confirmationDialog = new Dialog(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialog = inflater.inflate(R.layout.stepwisedialog_layout, null);


        RaisedButton okButton = (RaisedButton) dialog.findViewById(R.id.f_Yes_button);
        okButton.setText(AppStrings.dialogOk);


        RaisedButton noButton = (RaisedButton) dialog.findViewById(R.id.f_No_button);
        noButton.setText(AppStrings.dialogNo);


        TextView alert = (TextView) dialog.findViewById(R.id.confirmationHeader);
        alert.setText(AppStrings.Alert);
        alert.setTypeface(LoginActivity.sTypeface);

        TextView alertmessage = (TextView) dialog.findViewById(R.id.alertmessage);
        alertmessage.setText(AppStrings.stpwisenote);
        alertmessage.setTypeface(LoginActivity.sTypeface);

        TextView continuemessage = (TextView) dialog.findViewById(R.id.continuemessage);
        continuemessage.setText(AppStrings.douwantdate);
        continuemessage.setTypeface(LoginActivity.sTypeface);

//        ImageView image = (ImageView) dialog.findViewById(R.id.dialog_imageView);
//        image.setBackgroundResource(R.drawable.lauchericon);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmationDialog.dismiss();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(getActivity(), NewDrawerScreen.STEP_WISE);
                dialog.show(fm, "");

            }
        });


        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmationDialog.dismiss();
            }
        });


        confirmationDialog.getWindow()
                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialog);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();



    }

    public void passData(ListOfShg data) {
        dataPasser.onDataPass(data);
    }

    public interface OnDataPass {
        public void onDataPass(ListOfShg data);
    }
}

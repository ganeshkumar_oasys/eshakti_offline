package com.oasys.eshakti.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Adapter.CustomItemAdapter;
import com.oasys.eshakti.Dialogue.Dialog_New_TransactionDate;
import com.oasys.eshakti.Dto.AddBTDto;
import com.oasys.eshakti.Dto.CashOfGroup;
import com.oasys.eshakti.Dto.ExistingLoan;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.Dto.OfflineDto;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.Dto.ShgBankDetails;
import com.oasys.eshakti.Dto.TableData;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.GetSpanText;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.BankTable;
import com.oasys.eshakti.database.LoanTable;
import com.oasys.eshakti.database.MemberTable;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.database.TransactionTable;
import com.oasys.eshakti.model.RowItem;
import com.oasys.eshakti.views.MaterialSpinner;
import com.oasys.eshakti.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BT_RD_Entry extends Fragment implements View.OnClickListener, NewTaskListener {

    private View rootView;
    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private ArrayList<ShgBankDetails> bankdetails;
    private String[] menuStr;
    private NetworkConnection networkConnection;
    private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext,mAccountnumbervalue,mRecurringamount1;
    private TextView mLoanOutstandingTextView, mBankName_value_TextView, mBankNameTextView, mLoanAccNo_value_TextView, mAccNo_ValeTextView, mAcctoaccFromBankEdittext, accNotext, mLoanAccNoTextView, mLoanOutstanding_value_TextView, mAccNoTextView;
    private Button mRaised_Submit_Button, mSubmitButton, mEdit_RaisedButton, mOk_RaisedButton;

    private Dialog confirmationDialog;
    MaterialSpinner mSpinner_tobank, mSpinner_loanAcc,mLoanAccTransactionspinner1;
    CustomItemAdapter bankNameAdapter, loanAccAdapter;
    private LinearLayout mSavingsAccLayout, trFlowSelection, mLoanAccLayout,mRdLayout;
    private RadioButton mSavingsAccRadio, mLoanAccRadio;
    private RadioGroup mRadioGroup;
    public static EditText mEditAmount;

    public static String sSend_To_Server_FD;
    public static String[] sFixedDepositItem;
    public static List<EditText> sEditTextFields = new ArrayList<EditText>();
    String mDeposit_Fixed_offline, mInterest_Fixed_offline, mWithdrawl_Fixed_offline, mBankExpenses_Fixed_offline;
    public static int bank_deposit, bank_withdrawl, bank_expenses, bank_interest;
    private int mRDSize;
    private String amount = "0.0";
    String mSelectedCashatBank = "0.0";
    private Dialog mProgressDilaog;
    private List<ExistingLoan> loanDetails;

    private String selectedItemBank, selectedLoanAcc, selectedLoanId;
    public static String mBankNameValue = null, mBankName = "", mTempBankId = "", mLoanAccValue = null, mLoanAccIdValue = null;
    private List<RowItem> bankNameItems, loanAccItems;
    public static int sFixedDepositTotal;
    private  String SelectedType;

    private ArrayList<ShgBankDetails> bankSortedList;

    ArrayList<String> mBanknames_Array = new ArrayList<String>();
    ArrayList<String> mBanknamesId_Array = new ArrayList<String>();

    private String[] mLoanTypeArray, mLoanIdArray;

    ArrayList<String> mEngSendtoServerBank_Array = new ArrayList<String>();
    ArrayList<String> mEngSendtoServerBankId_Array = new ArrayList<String>();

    String selectedRadio = "NONE";
    String selectedspinnertype = "";
    String ToLoanAccNo = "", outstandingAmt = "";
    private ExistingLoan selectedLoan;
    private ExistingLoan selectedLoantype;
    private ShgBankDetails selectedSavinAc;
    private String[] sFixedDepositAmount;
    private int transactionFlowType = 0;
    private TextView acctoaccfrombanktext, acctoaccfrombank;
    OfflineDto offlineDBData = new OfflineDto();
    private LinearLayout ply;
    private  int size,size1;
    String val_edi = "";
    int savposition,accposition,bankposition;
    private int bankcurrent_balance = 0;
    String val="";
    public static int sBank_deposit=0,sBank_Interest=0, sBank_withdrawl=0, sBank_expenses=0;
    private  EditText mInstallmentamount;
    int selectedbank =0;
    private ShgBankDetails bankDetailsaccountnumbr;
    private ArrayList<TableData> tableDataArrayList;
    private ArrayList<TableData> tableDataArrayList1;
    private TableData selectedSavinAcc;
    String shgacc_val="";
    String bal_val="";
    String bank_val="";
    String account_val="";
    int recurringbalance=0;
    String withdrawalamount;


    @Override
    public void onClick(View view) {


        switch (view.getId()) {

            case R.id.fragment_Edit:

                confirmationDialog.dismiss();
                sSend_To_Server_FD = "0";
                sFixedDepositTotal = Integer.valueOf("0");
                break;

            case R.id.frag_Ok:

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();

                if (networkConnection.isNetworkAvailable()) {
                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 1) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }
                    // NOTHING TO DO::
                } else {

                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 2) {
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        return;
                    }

                }

                AddBTDto bt = new AddBTDto();
                bt.setSavingsBankId(shgacc_val);   // RD BANK ID
//                bt.setBankInterest(bank_interest + "");
//                bt.setBankCharges(bank_expenses + "");
                bt.setShgId(shgDto.getShgId());
                if (transactionFlowType != 1) {
                    if (val != null && val!= null)
                        bt.setToBankId(val); // To bank Id
                } else {
                    if (selectedLoan != null && selectedLoan.getLoanId() != null)
                        bt.setToBankId(selectedLoan.getLoanId());
                }

                bt.setTransactionFlowType(transactionFlowType);
                bt.setWithdrawal( mInstallmentamount.getText().toString());
//                bt.setCashDeposit(bank_deposit + "");
                bt.setMobileDate(System.currentTimeMillis() + "");
//                bt.setTransactionDate(shgDto.getLastTransactionDate());
                bt.setFromBankId(BankTransaction.sSelectedBank.getShgSavingsAccountId());
                bt.setTransactionDate(Dialog_New_TransactionDate.cg.getLastTransactionDate());


                try {
                    OfflineDto offline = new OfflineDto();
                    offline.setAnimatorId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                    offline.setShgId(shgDto.getShgId());
                    offline.setSavingsBankId(shgacc_val);

                    /*offline.setBtToSavingAcId("");

                    if (transactionFlowType != 1) {
                        if (val != null && val!= null)
                            offline.setToBankId(val); // To bank Id
                            Log.i("print","val : "+val);
                    } else {
                        if (selectedLoan != null && selectedLoan.getLoanId() != null)
                            offline.setToBankId(selectedLoan.getLoanId());
                            Log.i("print","Selected Loan ID : "+selectedLoan.getLoanId());
                    }*/

                    //Log.i("print","val : "+val+"\t Transaction Flow Type : "+transactionFlowType);

                    //Log.i("print","val : "+val+"\t"+"Selected Loan ID : "+selectedLoan.getLoanId());
//                    offline.setLastTransactionDateTime(shgDto.getLastTransactionDate());
                    offline.setLastTransactionDateTime(Dialog_New_TransactionDate.cg.getLastTransactionDate());
                    offline.setModifiedDateTime(System.currentTimeMillis() + "");
                    offline.setModeOCash("2");
                    offline.setTxType(NewDrawerScreen.BANK_TRANSACTION);
                    offline.setTxSubtype(AppStrings.recurringDeposit);

                    withdrawalamount = mInstallmentamount.getText().toString();
                    recurringbalance = (int) Double.parseDouble(bal_val)-Integer.parseInt(withdrawalamount);
                    offline.setRecurringbankBalanceId(shgacc_val);
                    offline.setCurrent_balance(String.valueOf(recurringbalance));


                    offline.setBtFromSavingAcId(BankTransaction.sSelectedBank.getShgSavingsAccountId());
                    offline.setMFromBk(BankTransaction.sSelectedBank.getBankName());
                    int cih = 0, cab = 0, rd_value = 0, os = 0, cb = 0, cb1 = 0;

                    if (transactionFlowType != 1) {
                        if (val != null && val!= null) {
                            offline.setBtToSavingAcId(val);
                            offline.setMToBk(selectedSavinAc.getBankName());
                            offline.setAccountNumber(val);
                            ShgBankDetails details = BankTable.getBankTransaction(val);





                            cab = ((int) Double.parseDouble(shgDto.getCashAtBank()) + Integer.parseInt(withdrawalamount));
                            offline.setCashAtBank(cab + "");

                            cb1 = ((int) Double.parseDouble(details.getCurrentBalance())) +Integer.parseInt(withdrawalamount);
//                            offline.setCurr_balance1(cb1 + "");
                            offline.setShgSavingsAccountId(val);
                            offline.setCurr_balance(cb1 + "");
//                            offline.setCurr_balance(String.valueOf(cb));
//                            offline.setShgSavingsAccountId(BankTransaction.sSelectedBank.getShgSavingsAccountId());

                        } else {

                        }
                        // offline.setModeOCash("0");
                    } else {
                        if (selectedLoan != null && selectedLoan.getLoanId() != null) {
                            offline.setBtToLoanId(selectedLoan.getLoanId());
                            offline.setBtToSavingAcId(selectedLoan.getLoanId());
                            offline.setMLoanName(selectedLoan.getLoanTypeName());
                            offline.setAccountNumber(selectedLoan.getLoanAccountId());
                            cab = ((int) Double.parseDouble(shgDto.getCashAtBank()));
                            offline.setCashAtBank(cab + "");
                            os = ((int) Double.parseDouble(selectedLoantype.getLoanOutstanding())) - Integer.parseInt(withdrawalamount);


                        } else {

                        }
                        // offline.setModeOCash("");
                    }
                    offline.setOutStanding(os + "");
                    offline.setBtWithdraw(mInstallmentamount.getText().toString());
                    cih = ((int) Double.parseDouble(shgDto.getCashInHand()));
                    offline.setCashInhand(cih + "");
                    offline.setIs_transaction_tdy("1.0");
                    int selcurr_bala=0;
                    selcurr_bala = bankcurrent_balance+bank_withdrawl;
                    offline.setSelectedcurrentbalance(selcurr_bala + "");
                    offlineDBData = offline;
//                    offline.setBtDeposit(bank_deposit + "");
//                    offline.setBtCharge(bank_expenses + "");
//                    offline.setBtExpense(bank_expenses + "");
//                    offline.setBtInterest(bank_interest + "");


//                    rd_value = ((int) Double.parseDouble(amount) + bank_deposit + bank_interest) - (bank_withdrawl + bank_expenses);

                   /* if(transactionFlowType!=1)
                    {
                        cb = ((bankcurrent_balance+bank_withdrawl)-bank_deposit);
                    }
                    else
                    {
                        cb = bankcurrent_balance-bank_deposit;
                    }*/




                    // offline.setAccountNumber(BankTransaction.sSelectedBank.getAccountNumber());

                    ;
//                    int bankcurrent=(int) Double.parseDouble(mSelectedCashatBank);
//                    cb = (bankcurrent-bank_deposit);

//                    offline.setRd_value(rd_value + "");
//                    offline.setCurr_balance(cb + "");


                    //   offline.add(offline);

                } catch (Exception e) {
                    e.printStackTrace();
                }


                String sreqString = new Gson().toJson(bt);
                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();
                    RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.BT_RD, sreqString, getActivity(), ServiceType.BT_RD);
                } else {
                    if (TransactionTable.getLoginFlag(AppStrings.recurringDeposit).size() <= 0 || (!TransactionTable.getLoginFlag(AppStrings.recurringDeposit).get(TransactionTable.getLoginFlag(AppStrings.recurringDeposit).size()-1).getLoginFlag().equals("1")))
                        insertBT();
                    else
                    TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
                            TastyToast.WARNING);

                }

                break;
        }
    }

    private void insertTransaction_tdy() {
        offlineDBData.setIs_transaction_tdy("1.0");
        offlineDBData.setShgId(shgDto.getShgId());
        SHGTable.updateIstransaction(offlineDBData);
    }

    private void updaterecurring_bal() {
        if (offlineDBData.getCurrent_balance() != null || offlineDBData.getCurrent_balance().length() > 0) {
            SHGTable.updateBankrecurringCurrentBalanceDetails(offlineDBData);
        }

    }
    private void insertBT() {
        try {

            int value = (MySharedPreference.readInteger(getActivity(), MySharedPreference.BT_COUNT, 0) + 1);
            if (value > 0)
                MySharedPreference.writeInteger(getActivity(), MySharedPreference.BT_COUNT, value);

            offlineDBData.setBtCount(value + "");
            TransactionTable.insertTransBTData(offlineDBData);


            if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
            }
            insertTransaction_tdy();
            updateBKTXData();
            updateCIH();

            FragmentManager fm = getFragmentManager();
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            NewDrawerScreen.showFragment(new MainFragment());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateBKTXData() {

        try {
            if(offlineDBData.getCurr_balance() == null)
            {
                if (offlineDBData != null && offlineDBData.getOutStanding() != null && offlineDBData.getOutStanding().length() > 0) {
                SHGTable.updateLoanOSDetails(offlineDBData);
            }
            }
            else {
                if (offlineDBData.getCurr_balance() != null || offlineDBData.getCurr_balance().length() > 0) {
                    SHGTable.updateBankCurrentBalanceDetails(offlineDBData);
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    private void updateCIH() {
        String cihstr = "", cabstr = "", lastTranstr = "";
        cihstr = offlineDBData.getCashInhand();
        cabstr = offlineDBData.getCashAtBank();
        lastTranstr = offlineDBData.getLastTransactionDateTime();
        CashOfGroup csg = new CashOfGroup();
        csg.setCashInHand(cihstr);
      /*  if(transactionFlowType==1) {
            csg.setCashAtBank(cabstr);
        }*/
        csg.setCashAtBank(cabstr);
        csg.setLastTransactionDate(lastTranstr);
        SHGTable.updateSHGDetails(csg, shgDto.getId());
    }

//    private void updateBKTXData() {
//        if (offlineDBData != null && offlineDBData.getCurr_balance1() != null && offlineDBData.getCurr_balance1().length() > 0) {
//
//
//            SHGTable.updateToBankCurrentBalanceDetails(offlineDBData);
//        }

//        if (offlineDBData.getCurr_balance() != null || offlineDBData.getCurr_balance().length() > 0) {
//                SHGTable.updateBankCurrentBalanceDetails(offlineDBData);
//            }
//
//        if (offlineDBData != null && offlineDBData.getOutStanding() != null && offlineDBData.getOutStanding().length() > 0) {
//            SHGTable.updateLoanOSDetails(offlineDBData);
//        }

//if(transactionFlowType==0) {
//    if (offlineDBData != null && offlineDBData.getCurr_balance() != null && offlineDBData.getCurr_balance().length() > 0) {
//        String value1 = offlineDBData.getBtFromSavingAcId();
//        offlineDBData.setAccountNumber(value1);
//        SHGTable.updateBankCurrentBalanceDetails(offlineDBData);
//    }
//}

//        if (offlineDBData != null && offlineDBData.getSelectedcurrentbalance() != null && offlineDBData.getSelectedcurrentbalance().length() > 0) {
//            offlineDBData.setAccountNumber(val);
//            SHGTable.updateBankSelectedCurrentBalanceDetails(offlineDBData);
//        }

//        if (offlineDBData != null && offlineDBData.getRd_value() != null && offlineDBData.getRd_value() != "0" && offlineDBData.getRd_value().length() > 0) {
//            String value1 = offlineDBData.getBtFromSavingAcId();
//            offlineDBData.setAccountNumber(value1);
//            SHGTable.updateRDDetails(offlineDBData);
//
//        }
//    }


    @Override
    public void onStart() {
        super.onStart();
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());

      /*  if (networkConnection.isNetworkAvailable()) {            //  onTaskStarted();
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.BT_CB_FD_VALUE + BankTransaction.sSelectedBank.getShgSavingsAccountId(), getActivity(), ServiceType.RD_VALUE);
            //  onTaskStarted();
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.BT_PLOAN + shgDto.getShgId(), getActivity(), ServiceType.LOANTYPE);
        }*/
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        loanDetails = LoanTable.getGrpLoanDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        sFixedDepositItem = new String[]{AppStrings.bankDeposit,
                AppStrings.bankInterest,
                AppStrings.bankExpenses,
                AppStrings.withdrawl};
        sEditTextFields = new ArrayList<EditText>();

        Log.d("amounts",""+BankTransaction.sSelectedBank.getRecurringDepositedBalance());
        amount = BankTransaction.sSelectedBank.getRecurringDepositedBalance();
        mSelectedCashatBank = BankTransaction.sSelectedBank.getCurrentBalance();
        tableDataArrayList = SHGTable.getRecurringData();
        tableDataArrayList1 = SHGTable.getRecurringBank(shgDto.getShgId());
        init();
    }

    private void init() {
        ply.setVisibility(View.VISIBLE);

        mInstallmentamount = (EditText) rootView.findViewById(R.id.installmentamount);

        mGroupName = (TextView) rootView.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);
        mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
        mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashInHand.setTypeface(LoginActivity.sTypeface);
        mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
        mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashAtBank.setTypeface(LoginActivity.sTypeface);
        mHeadertext = (TextView) rootView.findViewById(R.id.fragment_deposit_entry_headertext);
        mHeadertext.setText(AppStrings.recurringWithdrwal);
        mHeadertext.setTypeface(LoginActivity.sTypeface);
        mAccountnumbervalue = (TextView) rootView.findViewById(R.id.accountnumbervalue);
        mRecurringamount1 = (TextView) rootView.findViewById(R.id.recurringamount1);


        mSubmitButton = (RaisedButton) rootView.findViewById(R.id.acctoacc_submit);
        mSubmitButton.setText(AppStrings.submit);
        mSubmitButton.setTypeface(LoginActivity.sTypeface);
//        mSubmitButton.setOnClickListener(this);

        mSpinner_tobank = (MaterialSpinner) rootView.findViewById(R.id.acctoacctobankspinner);
        mSpinner_loanAcc = (MaterialSpinner) rootView.findViewById(R.id.loanAccTransactionspinner);
        mSpinner_loanAcc.setBaseColor(R.color.grey_400);
        mSpinner_loanAcc.setFloatingLabelText(AppStrings.mLoanAccType);
        mSpinner_loanAcc.setPaddingSafe(10, 0, 10, 0);
        mSpinner_tobank.setFloatingLabelText(AppStrings.mTransferSpinnerFloating);
        mSpinner_tobank.setPaddingSafe(10, 0, 10, 0);


        trFlowSelection = (LinearLayout) rootView.findViewById(R.id.trFlowSelection);
        mRdLayout = (LinearLayout) rootView.findViewById(R.id.RdLayout);

        mLoanAccTransactionspinner1 = (MaterialSpinner) rootView.findViewById(R.id.loanAccTransactionspinner1);

        mSavingsAccLayout = (LinearLayout) rootView.findViewById(R.id.acctoacctobankspinnerlayout);
        mLoanAccLayout = (LinearLayout) rootView.findViewById(R.id.loanAccTransactionspinnerLayout);

        mRadioGroup = (RadioGroup) rootView.findViewById(R.id.radio_loanAccTransaction);
        mSavingsAccRadio = (RadioButton) rootView.findViewById(R.id.radioloanaccTransaction_SavingsAcc);
        mLoanAccRadio = (RadioButton) rootView.findViewById(R.id.radioloanaccTransaction_LoanAcc);
        mSavingsAccRadio.setText(AppStrings.mSavingsAccount);
        mSavingsAccRadio.setTypeface(LoginActivity.sTypeface);

        mLoanAccRadio.setText(AppStrings.mLoanaccHeader);
        mLoanAccRadio.setTypeface(LoginActivity.sTypeface);

        mLoanOutstandingTextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_outstandingTextView);
        mLoanOutstandingTextView.setTypeface(LoginActivity.sTypeface);
        mLoanOutstanding_value_TextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_outstanding_value);
        mLoanOutstanding_value_TextView.setTypeface(LoginActivity.sTypeface);

        mBankNameTextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_BankNameTextView);
        mBankNameTextView.setTypeface(LoginActivity.sTypeface);
        acctoaccfrombanktext = (TextView) rootView.findViewById(R.id.acctoaccfrombanktext);
        acctoaccfrombanktext.setText(AppStrings.BankName);
        acctoaccfrombanktext.setTypeface(LoginActivity.sTypeface);
//        acctoaccfrombank = (TextView) rootView.findViewById(R.id.acctoaccfrombank);
        // acctoaccfrombank.setTypeface(LoginActivity.sTypeface);
        mBankName_value_TextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_BankName_value);
        mBankName_value_TextView.setTypeface(LoginActivity.sTypeface);

        mLoanAccNoTextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_accNoTextView);
        mLoanAccNoTextView.setTypeface(LoginActivity.sTypeface);

//        accNotext = (TextView) rootView.findViewById(R.id.accNotext);
//        accNotext.setText(AppStrings.mAccountNumber);
//        accNotext.setTypeface(LoginActivity.sTypeface);
        mLoanAccNo_value_TextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_accNo_value);
        mLoanAccNo_value_TextView.setTypeface(LoginActivity.sTypeface);
        mSubmitButton.setText(AppStrings.submit);
        mSubmitButton.setTypeface(LoginActivity.sTypeface);
        mAccNo_ValeTextView = (TextView) rootView.findViewById(R.id.accNo_value);
        mAccNo_ValeTextView.setText(BankTransaction.sSelectedBank.getAccountNumber());
        mAccNo_ValeTextView.setTypeface(LoginActivity.sTypeface);
        mAcctoaccFromBankEdittext = (TextView) rootView.findViewById(R.id.acctoaccfrombank);
        mAcctoaccFromBankEdittext.setTypeface(LoginActivity.sTypeface);

        if (amount != null) {
            mAcctoaccFromBankEdittext.setText(BankTransaction.sSelectedBank.getBankName());
//            mAcctoaccFromBankEdittext.setText(BankTransaction.sSelectedBank.getBankName() + "  :  " + amount);
        } else {
            mAcctoaccFromBankEdittext.setText(BankTransaction.sSelectedBank.getBankName());
//            mAcctoaccFromBankEdittext.setText(BankTransaction.sSelectedBank.getBankName() + "  :  " + "0");
        }
        mLoanOutstandingTextView.setText(AppStrings.outstanding);
        mBankNameTextView.setText(AppStrings.bankName);
        mLoanAccNoTextView.setText(AppStrings.mAccountNumber);

       /* bankSortedList = new ArrayList<>();
        for (ShgBankDetails details : bankdetails) {
            if (details.getShgSavingsAccountId() != null && !(details.getShgSavingsAccountId().equals(BankTransaction.sSelectedBank.getShgSavingsAccountId())))
                bankSortedList.add(details);
        }*/


        for (int i = 0; i < bankdetails.size(); i++) {
            mBankName = mBankName + bankdetails.get(i).getBankName().toString() + "~";

        }

        for (int i = 0; i < bankdetails.size(); i++) {

            mTempBankId = mTempBankId + bankdetails.get(i).getBankName().toString() + "~";
        }

        Log.e("Temp Bank ID  Values_>", mTempBankId);

        for (int i = 0; i < bankdetails.size(); i++) {
            mBanknames_Array.add(bankdetails.get(i).getBankName());
            mBanknamesId_Array.add(bankdetails.get(i).getBankId());
        }

        for (int i = 0; i < bankdetails.size(); i++) {
            mEngSendtoServerBank_Array.add(bankdetails.get(i).getBankName());
            mEngSendtoServerBankId_Array.add(bankdetails.get(i).getBankId());
        }


        final String[] bankNames = new String[bankdetails.size() + 1];
        bankNames[0] = String.valueOf(AppStrings.S_B_N);
        for (int i = 0; i < bankdetails.size(); i++) {
            //if (!bankdetails.get(i).getShgSavingsAccountId().equals(BankTransaction.sSelectedBank.getShgSavingsAccountId()))
            bankNames[i + 1] = bankdetails.get(i).getBankName().toString();
            System.out.println("-----------------------" + bankNames[i + 1]);
        }

        final String[] bankNames_Id = new String[bankdetails.size() + 1];
        bankNames_Id[0] = String.valueOf(AppStrings.S_B_N);
        for (int i = 0; i < bankdetails.size(); i++) {
            //  if (!bankdetails.get(i).getShgSavingsAccountId().equals(BankTransaction.sSelectedBank.getShgSavingsAccountId()))
//            if(bankdetails.get(i).getBankName().equals(BankTransaction.sSelectedBank.getBankName())) {
                bankNames_Id[i + 1] = bankdetails.get(i).getBankId().toString();
                System.out.println("-----------------------" + bankNames_Id[i + 1]);
//            }
        }

        if (bankdetails.size() < 1) {
            mSavingsAccRadio.setClickable(false);
        } else {
            mSavingsAccRadio.setClickable(true);
        }

         size = bankNames.length;
        bankNameItems = new ArrayList<RowItem>();
        for (int i = 0; i < size; i++) {
            RowItem rowItem = new RowItem(bankNames[i]);
            bankNameItems.add(rowItem);
        }
        bankNameAdapter = new CustomItemAdapter(getActivity(), bankNameItems);
        mSpinner_tobank.setAdapter(bankNameAdapter);

        mSpinner_tobank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                savposition=position;
                if (position == 0) {
                    selectedItemBank = bankNames_Id[0];
                    mBankNameValue = "0";

                } else {

                    selectedItemBank = bankNames_Id[position];
                    selectedSavinAc = bankdetails.get(position - 1);

                    val=bankdetails.get(position - 1).getShgSavingsAccountId();
                    System.out.println("SELECTED BANK NAME : " + selectedItemBank);
                    String mBankname = null;
                    for (int i = 0; i < mBanknames_Array.size(); i++) {
                        if (selectedItemBank.equals(mEngSendtoServerBankId_Array.get(i))) {
                            mBankname = mEngSendtoServerBank_Array.get(i);
                        }
                    }

                    selectedspinnertype =mBankname;

                    mBankNameValue = mBankname;
                    Log.e("Bank N Name--->>>", mBankNameValue);

                    if (val != null) {
                        ShgBankDetails details = BankTable.getBankTransaction(val);
                        bankcurrent_balance = (int) Double.parseDouble(details.getCurrentBalance());
                        Log.d("bankbal",""+bankcurrent_balance+""+val);
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        final String[] bankNames1 = new String[tableDataArrayList1.size() + 1];
        //bankNames1[0] = String.valueOf(AppStrings.S_B_N);
        bankNames1[0] = String.valueOf(AppStrings.selectRDAccount);
        for (int i = 0; i < tableDataArrayList1.size(); i++) {
            bankNames1[i + 1] = tableDataArrayList1.get(i).getBankName()+" - "+
            tableDataArrayList1.get(i).getAccountNo();
            System.out.println("-----------------------" + bankNames1[i + 1]);
        }

        final String[] bankNames_Id1 = new String[tableDataArrayList1.size() + 1];
        //bankNames_Id1[0] = String.valueOf(AppStrings.S_B_N);
        bankNames_Id1[0] = String.valueOf(AppStrings.selectRDAccount);
        for (int i = 0; i < tableDataArrayList1.size(); i++) {
            bankNames_Id1[i + 1] = tableDataArrayList1.get(i).getBankId();
            System.out.println("-----------------------" + bankNames_Id1[i + 1]);
        }

        size1 = bankNames1.length;
        bankNameItems = new ArrayList<RowItem>();
        for (int i = 0; i < size1; i++) {
            RowItem rowItem = new RowItem(bankNames1[i]);
            bankNameItems.add(rowItem);
        }
        bankNameAdapter = new CustomItemAdapter(getActivity(), bankNameItems);
        mLoanAccTransactionspinner1.setAdapter(bankNameAdapter);

        mLoanAccTransactionspinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                bankposition = position;
                if (position == 0) {
                    selectedItemBank = bankNames1[0];
                    mRdLayout.setVisibility(View.INVISIBLE);

                }else
                {
                    selectedItemBank = bankNames1[position];
                    selectedSavinAcc = tableDataArrayList1.get(position - 1);
                    shgacc_val=selectedSavinAcc.getShgSavingsAccountId();
                    bal_val=selectedSavinAcc.getCurrent_balance();
                    bank_val=selectedSavinAcc.getBankName();
                    bank_val=selectedSavinAcc.getBranchName();
                    account_val = selectedSavinAcc.getAccountNo();

                    System.out.println("SELECTED BANK NAME : " + selectedItemBank);


                    mRdLayout.setVisibility(View.VISIBLE);

                    if(bal_val!=null)
                    {
                        mRecurringamount1.setText(bal_val);
                    }
                    else{
                        mRecurringamount1.setText("0.0");
                    }

                    mRecurringamount1.setTypeface(LoginActivity.sTypeface);

                    mAccountnumbervalue.setText(account_val);
                    mAccountnumbervalue.setTypeface(LoginActivity.sTypeface);


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mSubmitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.d("fill",bankposition + accposition + savposition + mInstallmentamount.getText().toString());

                if(bankposition ==0  || mInstallmentamount.getText().toString().isEmpty())
                {
                    TastyToast.makeText(getActivity(),"Provide the all required Details", TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                }
                else if(mLoanAccRadio.isChecked()) {
                    if (accposition == 0) {
                        TastyToast.makeText(getActivity(),"Provide the all required Details", TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                    }
                    else
                    {
                        onShowConfirmationDialog();
                    }
                }
                else if (mSavingsAccRadio.isChecked()) {
                    if (savposition == 0) {
                        TastyToast.makeText(getActivity(),"Provide the all required Details", TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                    }
                    else {
                        onShowConfirmationDialog();
                    }
                }
                else
                {
                    TastyToast.makeText(getActivity(), "Provide the all required Details", TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                }
//                onShowConfirmationDialog();


                /*System.out.println("------  " + selectedRadio.toString());

                int sDepositTotal=0;

                sFixedDepositAmount = new String[sEditTextFields.size()];
                for (int i = 0; i < sFixedDepositAmount.length; i++) {

                    sFixedDepositAmount[i] = String.valueOf(sEditTextFields.get(i).getText());


                    if ((sFixedDepositAmount[i].equals("")) || (sFixedDepositAmount[i] == null)) {
                        sFixedDepositAmount[i] = "0";
                    }

                    if (sFixedDepositAmount[i].matches("\\d*\\.?\\d+")) { // match a
                        // decimal
                        // number

                        int amount = (int) Math.round(Double.parseDouble(sFixedDepositAmount[i]));
                        sFixedDepositAmount[i] = String.valueOf(amount);
                    }

                    sDepositTotal = sDepositTotal + Integer.parseInt(sFixedDepositAmount[i]);
                }
                String bank_deposit1 = sFixedDepositAmount[0];
                String bank_interest2 = sFixedDepositAmount[1];
                String bank_expenses4 = sFixedDepositAmount[2];
                String bank_withdrawl3 = sFixedDepositAmount[3];

                sBank_deposit = Integer.parseInt(sFixedDepositAmount[0]);
                sBank_Interest = Integer.parseInt(sFixedDepositAmount[1]);
                sBank_expenses = Integer.parseInt(sFixedDepositAmount[2]);
                sBank_withdrawl = Integer.parseInt(sFixedDepositAmount[3]);


//                int total_val = sBank_deposit + sBank_Interest + sBank_withdrawl + sBank_expenses;

//                if ((sDepositTotal != 0) || (bank_deposit1.equals("") && bank_interest2.equals("") && bank_withdrawl3.equals("") && bank_expenses4.equals(""))) {
                if ((sDepositTotal != 0) || (bank_deposit1.equals("") && bank_interest2.equals("") && bank_withdrawl3.equals("") && bank_expenses4.equals(""))) {

                    if (bank_deposit1.equals("") && bank_interest2.equals("") && bank_withdrawl3.equals("") && bank_expenses4.equals("")) {

                        TastyToast.makeText(getActivity(), "PLEASE PROVIDE THE CASH DETAILS", TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                    }
                    else {

                        if (!bank_withdrawl3.isEmpty() && sBank_withdrawl!=0) {
                            if (mLoanAccRadio.isChecked()) {
                                if (accposition == 0) {
                                    TastyToast.makeText(getActivity(), "PLEASE SELECT THE TYPE", TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                } else {
//                        mLoanTypeArray[accposition];
                                    onShowConfirmationDialog();
//                                Toast.makeText(getActivity(), mLoanTypeArray[accposition], Toast.LENGTH_SHORT).show();

                                }
                            } else if (mSavingsAccRadio.isChecked()) {
                                if (savposition == 0) {
                                    TastyToast.makeText(getActivity(), "PLEASE SELECT THE BANK", TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                } else {
                                    onShowConfirmationDialog();
//                                Toast.makeText(getActivity(), mBanknames_Array.get(savposition), Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                onShowConfirmationDialog();
                            }

                        } else {
                            onShowConfirmationDialog();
                        }
                    }

                }
                else {
//                    onShowConfirmationDialog();
                    TastyToast.makeText(getActivity(), "PLEASE PROVIDE THE CASH DETAILS", TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                }*/




            }
        });

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub

                try {
                    int selectedId = group.getCheckedRadioButtonId();

                    if (selectedId == R.id.radioloanaccTransaction_SavingsAcc) {
                        System.out.println("-------Savings_acc----");
                        selectedRadio = "SAVINGSACCOUNT";
                        mSpinner_tobank.setSelection(0);
                        if (bankdetails.size() <= 1) {
//                            TastyToast.makeText(getActivity(), AppStrings.mAccToAccTransferToast, TastyToast.LENGTH_SHORT,
//                                    TastyToast.WARNING);

                            transactionFlowType = 0;
                            mSavingsAccLayout.setVisibility(View.VISIBLE);
                            mLoanAccLayout.setVisibility(View.GONE);
                            mSubmitButton.setClickable(false);

                        } else {
                            transactionFlowType = 0;
                            mSavingsAccLayout.setVisibility(View.VISIBLE);
                            mLoanAccLayout.setVisibility(View.GONE);
                            mSubmitButton.setClickable(true);
                        }
                    } else if (selectedId == R.id.radioloanaccTransaction_LoanAcc) {
                        System.out.println("-------Loan_acc----");

                        if (loanDetails.size() >= 1) {
                            selectedRadio = "LOANACCOUNT";
                            transactionFlowType = 1;
                            mLoanAccLayout.setVisibility(View.VISIBLE);
                            mSavingsAccLayout.setVisibility(View.GONE);

                            mLoanTypeArray = new String[loanDetails.size() + 1];
                            mLoanTypeArray[0] = String.valueOf(AppStrings.mLoanAccType);

                            mLoanIdArray = new String[loanDetails.size() + 1];
                            for (int i = 0; i < loanDetails.size(); i++) {
                                if (loanDetails.get(i).getLoanTypeName() != null && loanDetails.get(i).getLoanTypeName().length() > 0) {
                                    mLoanTypeArray[i + 1] = loanDetails.get(i).getLoanTypeName().toString();
                                    mLoanIdArray[i] = loanDetails.get(i).getLoanId().toString();
                                }
                            }

                            loanAccItems = new ArrayList<RowItem>();
                            for (int i = 0; i < loanDetails.size() + 1; i++) {
                                RowItem rowItem = new RowItem(mLoanTypeArray[i]);
                                loanAccItems.add(rowItem);
                            }
                            loanAccAdapter = new CustomItemAdapter(getActivity(), loanAccItems);
                            mSpinner_loanAcc.setAdapter(loanAccAdapter);
                            mSpinner_loanAcc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    // TODO Auto-generated method stub
                                    accposition =position;
                                    if (position == 0) {
                                        selectedLoanAcc = mLoanTypeArray[0];
                                        mLoanAccValue = "0";
                                        mLoanAccIdValue = "0";

                                        mLoanOutstanding_value_TextView.setText("");
                                        mBankName_value_TextView.setText("");
                                        mLoanAccNo_value_TextView.setText("");


                                    } else {
                                        selectedLoan = loanDetails.get(position - 1);

                                        if (loanDetails.get(position - 1).getLoanTypeName() != null) {
                                            selectedLoantype = loanDetails.get(position - 1);
                                            mLoanOutstanding_value_TextView.setText(loanDetails.get(position - 1).getLoanOutstanding());

                                            mBankName_value_TextView.setText(loanDetails.get(position - 1).getBankName());
                                            mLoanAccNo_value_TextView.setText(loanDetails.get(position - 1).getAccountNumber());
                                            ToLoanAccNo = loanDetails.get(position - 1).getAccountNumber();
                                        }
                                        // String dashBoardDate = DatePickerDialog.sDashboardDate;

                                    }
                                    selectedLoanAcc = mLoanTypeArray[position];
                                    mLoanAccValue = selectedLoanAcc;
                                    SelectedType = selectedLoanAcc;
                                    selectedspinnertype = SelectedType;
                                    Log.e("Loan Name", mLoanAccValue + "");
                                    Log.e("Loan Id", mLoanAccIdValue + "");
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // TODO Auto-generated method stub

                                }
                            });
                        } else {

                            TastyToast.makeText(getActivity(), AppStrings.noGroupLoan_Alert, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        size = sFixedDepositItem.length;
        System.out.println("Size of values:>>>" + size);

/*
        try {

            TableLayout tableLayout = (TableLayout) rootView.findViewById(R.id.fragmentDeposit_contentTable);
            TableRow.LayoutParams header_ContentParams = new TableRow.LayoutParams(250, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            header_ContentParams.setMargins(10, 5, 10, 5);
            TableRow.LayoutParams amountParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            amountParams.setMargins(5, 5, 20, 5);
          */
/*  TableRow outerRow = new TableRow(getActivity());

            TableRow.LayoutParams header_ContentParams = new TableRow.LayoutParams(250, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            header_ContentParams.setMargins(10, 5, 10, 5);

            TextView bankName = new TextView(getActivity());
            bankName.setText(GetSpanText.getSpanString(getActivity(),
                    String.valueOf(BankTransaction.sSelected_BankName)));
            bankName.setPadding(15, 5, 5, 5);
            bankName.setTextColor(R.color.black);
            bankName.setLayoutParams(header_ContentParams);
            outerRow.addView(bankName);

            TableRow.LayoutParams amountParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            amountParams.setMargins(5, 5, 20, 5);

            TextView bankDeposit_Amount = new TextView(getActivity());
            bankDeposit_Amount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(amount)));
            bankDeposit_Amount.setTextColor(R.color.black);
            bankDeposit_Amount.setPadding(5, 5, 5, 5);
            bankDeposit_Amount.setGravity(Gravity.RIGHT);
            bankDeposit_Amount.setLayoutParams(amountParams);
            outerRow.addView(bankDeposit_Amount);

            tableLayout.addView(outerRow, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));*//*


            for (int i = 0; i < size; i++) {

                TableRow innerRow = new TableRow(getActivity());
                TextView memberName = new TextView(getActivity());
                memberName.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sFixedDepositItem[i])));
                memberName.setTypeface(LoginActivity.sTypeface);
                memberName.setPadding(15, 0, 5, 5);
                memberName.setLayoutParams(header_ContentParams);
                memberName.setTextColor(R.color.black);
                innerRow.addView(memberName);

                if (i == 3) {
                    mEditAmount = new EditText(getActivity());
                    sEditTextFields.add(mEditAmount);
                    mEditAmount.setId(i);
                    mEditAmount.setInputType(InputType.TYPE_CLASS_NUMBER);
                    mEditAmount.setPadding(5, 5, 5, 5);
                    mEditAmount.setFilters(Get_EdiText_Filter.editText_filter());
                    mEditAmount.setGravity(Gravity.RIGHT);
                    mEditAmount.setLayoutParams(amountParams);
                    mEditAmount.setWidth(150);
                    mEditAmount.setBackgroundResource(R.drawable.edittext_background);

                    */
/*mEditAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            // TODO Auto-generated method stub
                            trFlowSelection.setVisibility(View.VISIBLE);
                            mSavingsAccRadio.setChecked(true);
                            if (hasFocus) {
                                ((EditText) v).setGravity(Gravity.LEFT);
                            } else {
                                ((EditText) v).setGravity(Gravity.RIGHT);
                            }
                        }
                    });*//*



                    innerRow.addView(mEditAmount);
                } else {

                    mEditAmount = new EditText(getActivity());
                    sEditTextFields.add(mEditAmount);
                    mEditAmount.setId(i);
                    mEditAmount.setInputType(InputType.TYPE_CLASS_NUMBER);
                    mEditAmount.setPadding(5, 5, 5, 5);
                    mEditAmount.setFilters(Get_EdiText_Filter.editText_filter());
                    mEditAmount.setGravity(Gravity.RIGHT);
                    mEditAmount.setLayoutParams(amountParams);
                    mEditAmount.setWidth(150);
                    mEditAmount.setBackgroundResource(R.drawable.edittext_background);
                    mEditAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            // TODO Auto-generated method stub
                            trFlowSelection.setVisibility(View.GONE);
                            if (hasFocus) {
                                ((EditText) v).setGravity(Gravity.LEFT);
                            } else {
                                ((EditText) v).setGravity(Gravity.RIGHT);
                            }

                        }
                    });
                    innerRow.addView(mEditAmount);
                }

                tableLayout.addView(innerRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
*/
    }

    private void onShowConfirmationDialog() {

        confirmationDialog = new Dialog(getActivity());
        String[] dialogdiaplayitems = new String[]{AppStrings.Accountnumber, AppStrings.withdrawalamount, AppStrings.type,AppStrings.toaccount};
        String[] dialogdiaplayitemsvalues = new String[]{account_val,mInstallmentamount.getText().toString(),selectedRadio,selectedspinnertype};
        size = dialogdiaplayitems.length;
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
        dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText(AppStrings.confirmation);
        confirmationHeader.setTypeface(LoginActivity.sTypeface);
        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

        DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
        Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
        String dateStr = simple.format(d);
        TextView transactdate = (TextView)dialogView.findViewById(R.id.transactdate);
        transactdate.setText(dateStr);

        for (int i = 0; i < size; i++) {

            TableRow indv_DepositEntryRow = new TableRow(getActivity());

            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 5, 10, 5);

            TextView memberName_Text = new TextView(getActivity());
            memberName_Text.setText(
                    GetSpanText.getSpanString(getActivity(), String.valueOf(dialogdiaplayitems[i])));
            memberName_Text.setTypeface(LoginActivity.sTypeface);
            memberName_Text.setTextColor(R.color.black);
            memberName_Text.setPadding(5, 5, 5, 5);
            memberName_Text.setLayoutParams(contentParams);
            indv_DepositEntryRow.addView(memberName_Text);

            TextView confirm_values = new TextView(getActivity());
            confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(dialogdiaplayitemsvalues[i])));
            confirm_values.setTextColor(R.color.black);
            confirm_values.setPadding(5, 5, 5, 5);
            confirm_values.setGravity(Gravity.RIGHT);
            confirm_values.setLayoutParams(contentParams);
            indv_DepositEntryRow.addView(confirm_values);

            confirmationTable.addView(indv_DepositEntryRow,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }

        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
        mEdit_RaisedButton.setText(AppStrings.edit);
        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mEdit_RaisedButton.setOnClickListener(this);

        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
        mOk_RaisedButton.setText(AppStrings.yes);
        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mOk_RaisedButton.setOnClickListener(this);

        confirmationDialog.getWindow()
                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialogView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();


    }


/*
    private void onShowConfirmationDialog() {
        // TODO Auto-generated method stub

        try {

            sFixedDepositAmount = new String[sEditTextFields.size()];
            mRDSize = sEditTextFields.size();
            sFixedDepositTotal = Integer.valueOf("0");
            sSend_To_Server_FD = "0";

            for (int i = 0; i < sFixedDepositAmount.length; i++) {

                sFixedDepositAmount[i] = String.valueOf(sEditTextFields.get(i).getText());

                if (sFixedDepositAmount[i] == null || sFixedDepositAmount[i].equals("")) {
                    sFixedDepositAmount[i] = "0";
                }

                if (sFixedDepositAmount[i].matches("\\d*\\.?\\d+")) { // match
                    Log.e("Double valuesssssss", sFixedDepositAmount[i]);
                    int amount = (int) Math.round(Double.parseDouble(sFixedDepositAmount[i]));
                    sFixedDepositAmount[i] = String.valueOf(amount);
                }

                Log.e("Integer valuesssssss", sFixedDepositAmount[i]);

                sFixedDepositTotal = sFixedDepositTotal + Integer.parseInt(sFixedDepositAmount[i]);
                sSend_To_Server_FD = sSend_To_Server_FD + sFixedDepositAmount[i] + "~";
            }

            System.out.println(" Total Value:>>>>" + sFixedDepositTotal);

            System.out.println("Send To Confirmation Value:>>>" + sSend_To_Server_FD);

            bank_deposit = Integer.parseInt(sFixedDepositAmount[0]);
            bank_interest = Integer.parseInt(sFixedDepositAmount[1]);
            bank_expenses = Integer.parseInt(sFixedDepositAmount[2]);
            bank_withdrawl = Integer.parseInt(sFixedDepositAmount[3]);

            System.out.println("Bank Deposit:>>>" + bank_deposit);
            System.out.println("Bank Withdrawl:>>>" + bank_withdrawl);
            int fdAvailableWithdrawlAmt = 0;
            if (amount != null) {
                fdAvailableWithdrawlAmt = (int) Double.parseDouble(amount.trim()) + Integer.parseInt(sFixedDepositAmount[0])
                        + Integer.parseInt(sFixedDepositAmount[1]) - Integer.parseInt(sFixedDepositAmount[3]);
            } else {
                fdAvailableWithdrawlAmt = (int) Double.parseDouble("0.0") + Integer.parseInt(sFixedDepositAmount[0])
                        + Integer.parseInt(sFixedDepositAmount[1]) - Integer.parseInt(sFixedDepositAmount[3]);
            }

            System.out.println("Fixed Deposit Available Withdrawl Amount:" + fdAvailableWithdrawlAmt);
            //   if (sFixedDepositTotal != 0 && bank_withdrawl <= fdAvailableWithdrawlAmt) {

            if (bank_withdrawl > 0) {
                if (sFixedDepositTotal != 0 && bank_deposit <= ((int) Double.parseDouble(mSelectedCashatBank.trim()))
                        && bank_withdrawl <= fdAvailableWithdrawlAmt
                        && bank_expenses <= ((int) Double.parseDouble(mSelectedCashatBank.trim()))) {

                    mDeposit_Fixed_offline = sFixedDepositAmount[0];
                    mInterest_Fixed_offline = sFixedDepositAmount[1];
                    mWithdrawl_Fixed_offline = sFixedDepositAmount[2];
                    mBankExpenses_Fixed_offline = sFixedDepositAmount[3];

                    confirmationDialog = new Dialog(getActivity());

                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                    dialogView.setLayoutParams(
                            new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                    confirmationHeader.setText("" + (AppStrings.confirmation));
                    confirmationHeader.setTypeface(LoginActivity.sTypeface);

                    TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                    TableRow typeRow = new TableRow(getActivity());

                    @SuppressWarnings("deprecation")
                    TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    contentParams.setMargins(10, 5, 10, 5);

                    TextView memberName_Text = new TextView(getActivity());
                    memberName_Text.setText(
                            GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferFromBank) + " " + "SB"));
                    memberName_Text.setTypeface(LoginActivity.sTypeface);
                    memberName_Text.setPadding(5, 5, 5, 5);
                    memberName_Text.setLayoutParams(contentParams);
                    typeRow.addView(memberName_Text);

                    TextView confirm_values = new TextView(getActivity());
                    confirm_values.setText(
                            GetSpanText.getSpanString(getActivity(), String.valueOf(BankTransaction.sSelectedBank.getBankName())));
                    confirm_values.setPadding(5, 5, 5, 5);
                    confirm_values.setGravity(Gravity.RIGHT);
                    confirm_values.setLayoutParams(contentParams);
                    typeRow.addView(confirm_values);

                    confirmationTable.addView(typeRow,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    // if (ConnectionUtils.isNetworkAvailable(getActivity())) {
                    TableRow sbAccNoRow = new TableRow(getActivity());

                    @SuppressWarnings("deprecation")
                    TableRow.LayoutParams sbAccParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    sbAccParams.setMargins(10, 5, 10, 5);

                    TextView accNo_Text = new TextView(getActivity());
                    accNo_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mAccountNumber)));
                    accNo_Text.setTypeface(LoginActivity.sTypeface);
                    accNo_Text.setPadding(5, 5, 5, 5);
                    accNo_Text.setLayoutParams(sbAccParams);
                    sbAccNoRow.addView(accNo_Text);

                    TextView accNo_values = new TextView(getActivity());
                    accNo_values
                            .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(BankTransaction.sSelectedBank.getAccountNumber())));
                    accNo_values.setPadding(5, 5, 5, 5);
                    accNo_values.setGravity(Gravity.RIGHT);
                    accNo_values.setLayoutParams(sbAccParams);
                    sbAccNoRow.addView(accNo_values);

                    confirmationTable.addView(sbAccNoRow,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    // }

                    TableRow bankNameRow = new TableRow(getActivity());

                    @SuppressWarnings("deprecation")
                    TableRow.LayoutParams bankNameParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    bankNameParams.setMargins(10, 5, 10, 5);

                    TextView bankName_Text = new TextView(getActivity());
                    if (selectedRadio.equals("SAVINGSACCOUNT")) {
                        bankName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferToBank)));
                        bankName_Text.setTypeface(LoginActivity.sTypeface);
                    } else {
                        bankName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanAccType)));
                        bankName_Text.setTypeface(LoginActivity.sTypeface);
                    }
                    bankName_Text.setPadding(5, 5, 5, 5);
                    bankName_Text.setLayoutParams(bankNameParams);
                    bankNameRow.addView(bankName_Text);

                    TextView bankName_values = new TextView(getActivity());
                    if (selectedRadio.equals("SAVINGSACCOUNT")) {
                        bankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(selectedSavinAc.getBankName())));
                    } else {
                        bankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mLoanAccValue)));
                    }

                    bankName_values.setPadding(5, 5, 5, 5);
                    bankName_values.setGravity(Gravity.RIGHT);
                    bankName_values.setLayoutParams(bankNameParams);
                    bankNameRow.addView(bankName_values);

                    confirmationTable.addView(bankNameRow,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    if (!selectedRadio.equals("SAVINGSACCOUNT")) {
                        TableRow sbAccNoRow1 = new TableRow(getActivity());

                        @SuppressWarnings("deprecation")
                        TableRow.LayoutParams sbAccParams1 = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                        sbAccParams1.setMargins(10, 5, 10, 5);

                        TextView accNo_Text1 = new TextView(getActivity());
                        accNo_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mAccountNumber)));
                        accNo_Text1.setTypeface(LoginActivity.sTypeface);
                        accNo_Text1.setPadding(5, 5, 5, 5);
                        accNo_Text1.setLayoutParams(sbAccParams1);
                        sbAccNoRow1.addView(accNo_Text1);

                        TextView accNo_values1 = new TextView(getActivity());
                        accNo_values1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(ToLoanAccNo)));
                        accNo_values1.setPadding(5, 5, 5, 5);
                        accNo_values1.setGravity(Gravity.RIGHT);
                        accNo_values1.setLayoutParams(sbAccParams1);
                        sbAccNoRow1.addView(accNo_values1);

                        confirmationTable.addView(sbAccNoRow1,
                                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    }


                    for (int i = 0; i < mRDSize; i++) {

                        TableRow indv_DepositEntryRow = new TableRow(getActivity());

                        TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                        contentParams1.setMargins(10, 5, 10, 5);

                        TextView memberName_Text1 = new TextView(getActivity());
                        memberName_Text1.setText(
                                GetSpanText.getSpanString(getActivity(), String.valueOf(sFixedDepositItem[i])));
                        memberName_Text1.setTypeface(LoginActivity.sTypeface);
                        memberName_Text1.setTextColor(R.color.black);
                        memberName_Text1.setPadding(5, 5, 5, 5);
                        memberName_Text1.setLayoutParams(contentParams1);
                        indv_DepositEntryRow.addView(memberName_Text1);

                        TextView confirm_values1 = new TextView(getActivity());
                        confirm_values1.setText(
                                GetSpanText.getSpanString(getActivity(), String.valueOf(sFixedDepositAmount[i])));
                        confirm_values1.setTextColor(R.color.black);
                        confirm_values1.setPadding(5, 5, 5, 5);
                        confirm_values1.setGravity(Gravity.RIGHT);
                        confirm_values1.setLayoutParams(contentParams1);
                        indv_DepositEntryRow.addView(confirm_values1);

                        confirmationTable.addView(indv_DepositEntryRow,
                                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    }


                    mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                    mEdit_RaisedButton.setText("" + (AppStrings.edit));
                    mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                    mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                    // 205,
                    // 0));
                    mEdit_RaisedButton.setOnClickListener(this);

                    mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                    mOk_RaisedButton.setText("" + AppStrings.yes);
                    mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                    mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                    mOk_RaisedButton.setOnClickListener(this);

                    confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    confirmationDialog.setCanceledOnTouchOutside(false);
                    confirmationDialog.setContentView(dialogView);
                    confirmationDialog.setCancelable(true);
                    confirmationDialog.show();

                    ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                    margin.leftMargin = 10;
                    margin.rightMargin = 10;
                    margin.topMargin = 10;
                    margin.bottomMargin = 10;
                    margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
                } else if (bank_withdrawl > fdAvailableWithdrawlAmt) {

                    TastyToast.makeText(getActivity(), AppStrings.cashatBankAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                    sSend_To_Server_FD = "0";
                    sFixedDepositTotal = Integer.valueOf("0");

                } else if (bank_withdrawl > fdAvailableWithdrawlAmt
                        || bank_expenses > ((int) Double.parseDouble(mSelectedCashatBank.trim()))) {

                    if (bank_expenses > ((int) Double.parseDouble(mSelectedCashatBank.trim()))) {

                        TastyToast.makeText(getActivity(), AppStrings.cashatBankAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    } else if (bank_withdrawl > fdAvailableWithdrawlAmt) {

                        TastyToast.makeText(getActivity(), AppStrings.mCheckFixedDepositAmount, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);

                    }

                    sSend_To_Server_FD = "0";
                    sFixedDepositTotal = Integer.valueOf("0");

                } else {
                    TastyToast.makeText(getActivity(), AppStrings.DepositnullAlert, TastyToast.LENGTH_SHORT,
                            TastyToast.WARNING);

                    sSend_To_Server_FD = "0";
                    sFixedDepositTotal = Integer.valueOf("0");

                }

            } else {


                mDeposit_Fixed_offline = sFixedDepositAmount[0];
                mInterest_Fixed_offline = sFixedDepositAmount[1];
                mWithdrawl_Fixed_offline = sFixedDepositAmount[2];
                mBankExpenses_Fixed_offline = sFixedDepositAmount[3];

                confirmationDialog = new Dialog(getActivity());

                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                confirmationHeader.setText("" + (AppStrings.confirmation));
                confirmationHeader.setTypeface(LoginActivity.sTypeface);

                TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
                Date d = new Date(Long.parseLong(Dialog_New_TransactionDate.cg.getLastTransactionDate()));
                String dateStr = simple.format(d);
                TextView transactdate = (TextView)dialogView.findViewById(R.id.transactdate);
                transactdate.setText(dateStr);

                TableRow typeRow = new TableRow(getActivity());

                @SuppressWarnings("deprecation")
                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams.setMargins(10, 5, 10, 5);

                TextView memberName_Text = new TextView(getActivity());
                memberName_Text.setText(
                        GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferFromBank) + " " + "SB"));
                memberName_Text.setTypeface(LoginActivity.sTypeface);
                memberName_Text.setPadding(5, 5, 5, 5);
                memberName_Text.setLayoutParams(contentParams);
                typeRow.addView(memberName_Text);

                TextView confirm_values = new TextView(getActivity());
                confirm_values.setText(
                        GetSpanText.getSpanString(getActivity(), String.valueOf(BankTransaction.sSelectedBank.getBankName())));
                confirm_values.setPadding(5, 5, 5, 5);
                confirm_values.setGravity(Gravity.RIGHT);
                confirm_values.setLayoutParams(contentParams);
                typeRow.addView(confirm_values);

                confirmationTable.addView(typeRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                // if (ConnectionUtils.isNetworkAvailable(getActivity())) {
                TableRow sbAccNoRow = new TableRow(getActivity());

                @SuppressWarnings("deprecation")
                TableRow.LayoutParams sbAccParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                sbAccParams.setMargins(10, 5, 10, 5);

                TextView accNo_Text = new TextView(getActivity());
                accNo_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mAccountNumber)));
                accNo_Text.setTypeface(LoginActivity.sTypeface);
                accNo_Text.setPadding(5, 5, 5, 5);
                accNo_Text.setLayoutParams(sbAccParams);
                sbAccNoRow.addView(accNo_Text);

                TextView accNo_values = new TextView(getActivity());
                if(selectedSavinAc != null) {
                    accNo_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf((selectedSavinAc != null && selectedSavinAc.getAccountNumber() != null && selectedSavinAc.getAccountNumber().length() > 0) ? selectedSavinAc.getAccountNumber() : "NA")));
                }
                else
                {
                    accNo_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(BankTransaction.sSelectedBank.getAccountNumber())));
                }

                accNo_values.setPadding(5, 5, 5, 5);
                accNo_values.setGravity(Gravity.RIGHT);
                accNo_values.setLayoutParams(sbAccParams);
                sbAccNoRow.addView(accNo_values);

                confirmationTable.addView(sbAccNoRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                // }

                TableRow bankNameRow = new TableRow(getActivity());

                @SuppressWarnings("deprecation")
                TableRow.LayoutParams bankNameParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                bankNameParams.setMargins(10, 5, 10, 5);

                TextView bankName_Text = new TextView(getActivity());
                if (!selectedRadio.equals("NONE")) {
                    if (selectedRadio.equals("SAVINGSACCOUNT")) {
                        bankName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferToBank)));
                        bankName_Text.setTypeface(LoginActivity.sTypeface);
                    } else {
                        bankName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanAccType)));
                        bankName_Text.setTypeface(LoginActivity.sTypeface);
                    }
                    bankName_Text.setPadding(5, 5, 5, 5);
                    bankName_Text.setLayoutParams(bankNameParams);
                    bankNameRow.addView(bankName_Text);
                }

                TextView bankName_values = new TextView(getActivity());
                if(!val_edi.isEmpty())
                {
                if (!selectedRadio.equals("NONE")) {
                    if (selectedRadio.equals("SAVINGSACCOUNT")) {
                        bankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(selectedSavinAc.getBankName())));
                    } else {
                        bankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mLoanAccValue)));
                    }
                }
                    bankName_values.setPadding(5, 5, 5, 5);
                    bankName_values.setGravity(Gravity.RIGHT);
                    bankName_values.setLayoutParams(bankNameParams);
                    bankNameRow.addView(bankName_values);
                    confirmationTable.addView(bankNameRow,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                }

*/
/*                if (!selectedRadio.equals("NONE")) {

                    TableRow sbAccNoRow1 = new TableRow(getActivity());

                    @SuppressWarnings("deprecation")
                    TableRow.LayoutParams sbAccParams1 = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    sbAccParams1.setMargins(10, 5, 10, 5);

                    TextView accNo_Text1 = new TextView(getActivity());

                    accNo_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mAccountNumber)));
                    accNo_Text1.setTypeface(LoginActivity.sTypeface);
                    accNo_Text1.setPadding(5, 5, 5, 5);
                    accNo_Text1.setLayoutParams(sbAccParams1);
                    sbAccNoRow1.addView(accNo_Text1);

                    TextView accNo_values1 = new TextView(getActivity());
                    if (!selectedRadio.equals("SAVINGSACCOUNT")) {
                        accNo_values1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(ToLoanAccNo)));
                    } else {
                        accNo_values1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf((selectedSavinAc != null && selectedSavinAc.getAccountNumber() != null && selectedSavinAc.getAccountNumber().length() > 0) ? selectedSavinAc.getAccountNumber() : "NA")));
                    }
                    accNo_values1.setPadding(5, 5, 5, 5);
                    accNo_values1.setGravity(Gravity.RIGHT);
                    accNo_values1.setLayoutParams(sbAccParams1);
                    sbAccNoRow1.addView(accNo_values1);

                    confirmationTable.addView(sbAccNoRow1,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                }*//*


                for (int i = 0; i < mRDSize; i++) {

                    TableRow indv_DepositEntryRow = new TableRow(getActivity());

                    TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    contentParams1.setMargins(10, 5, 10, 5);

                    TextView memberName_Text1 = new TextView(getActivity());
                    memberName_Text1.setText(
                            GetSpanText.getSpanString(getActivity(), String.valueOf(sFixedDepositItem[i])));
                    memberName_Text1.setTypeface(LoginActivity.sTypeface);
                    memberName_Text1.setTextColor(R.color.black);
                    memberName_Text1.setPadding(5, 5, 5, 5);
                    memberName_Text1.setLayoutParams(contentParams1);
                    indv_DepositEntryRow.addView(memberName_Text1);

                    TextView confirm_values1 = new TextView(getActivity());
                    confirm_values1.setText(
                            GetSpanText.getSpanString(getActivity(), String.valueOf(sFixedDepositAmount[i])));
                    confirm_values1.setTextColor(R.color.black);
                    confirm_values1.setPadding(5, 5, 5, 5);
                    confirm_values1.setGravity(Gravity.RIGHT);
                    confirm_values1.setLayoutParams(contentParams1);
                    indv_DepositEntryRow.addView(confirm_values1);

                    confirmationTable.addView(indv_DepositEntryRow,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                }


                mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                mEdit_RaisedButton.setText("" + (AppStrings.edit));
                mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                // 205,
                // 0));
                mEdit_RaisedButton.setOnClickListener(this);

                mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                mOk_RaisedButton.setText("" + AppStrings.yes);
                mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                mOk_RaisedButton.setOnClickListener(this);

                confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                confirmationDialog.setCanceledOnTouchOutside(false);
                confirmationDialog.setContentView(dialogView);
                confirmationDialog.setCancelable(true);
                confirmationDialog.show();

                ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                margin.leftMargin = 10;
                margin.rightMargin = 10;
                margin.topMargin = 10;
                margin.bottomMargin = 10;
                margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);


            }
        } catch (
                Exception e)

        {
            e.printStackTrace();
        }

    }
*/


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_rd_layout, container, false);
        ply = (LinearLayout) rootView.findViewById(R.id.pLy);
        ply.setVisibility(View.GONE);
        return rootView;
    }


    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
            }


            switch (serviceType) {

                case LOANTYPE:
                    try {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503 || statusCode == 409) {                       // showMessage(statusCode);
                            Utils.showToast(getActivity(), message);
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        } else if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        } else if (statusCode == Utils.Success_Code) {

                            if (mProgressDilaog != null) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
           /* if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
            }*/
                            }
                            Utils.showToast(getActivity(), message);
                            for (int i = 0; i < cdto.getResponseContent().getLoansList().size(); i++) {
                                ExistingLoan loanDto = cdto.getResponseContent().getLoansList().get(i);
                                loanDto.setShgId(shgDto.getShgId());
                                LoanTable.insertLoanDetails(loanDto);
                            }

                            loanDetails = LoanTable.getGrpLoanDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));


                        } else {
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                    } catch (Exception e) {

                    }
                    break;

                case BT_RD:
                    try {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {

                            if (mProgressDilaog != null) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }

                            if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                            }

                            updateBKTXData();
                            updateCIH();
                            updaterecurring_bal();
                            insertTransaction_tdy();

                            Utils.showToast(getActivity(), message);
                            FragmentManager fm = getFragmentManager();
                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            MainFragment mainFragment = new MainFragment();
                            Bundle bundles = new Bundle();
                            bundles.putString("Transaction", MainFragment.Flag_Transaction);
                            mainFragment.setArguments(bundles);
                            NewDrawerScreen.showFragment(mainFragment);
//                        NewDrawerScreen.showFragment(new MainFragment());
                        } else {

                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                    mProgressDilaog.dismiss();
                                    mProgressDilaog = null;
                                }
                            }
                            Utils.showToast(getActivity(), message);
                        }
                    } catch (Exception e) {

                    }
                    break;

                case RD_VALUE:
                    try {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            Utils.showToast(getActivity(), message);
                            amount = cdto.getResponseContent().getSavingsBalance().getRecurringDepositedBalance();
                            mSelectedCashatBank = cdto.getResponseContent().getSavingsBalance().getCurrentBalance();
                            //init();
                        } else {

                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                    mProgressDilaog.dismiss();
                                    mProgressDilaog = null;
                                }
                            }
                            Utils.showToast(getActivity(), message);

                        }
                    } catch (Exception e) {

                    }
                    break;

            }

        }
    }
}

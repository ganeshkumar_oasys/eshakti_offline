package com.oasys.eshakti.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Adapter.GroupLoanSummary_Otherloan_Adapter;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.database.SHGTable;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupLoanSummary_Otherloan extends Fragment implements NewTaskListener {

    private GroupLoanSummary_Otherloan_Adapter groupLoanSummary_otherloan_adapter;
    private RecyclerView grouploan_summary_otherloan_expandablelistview;
    private LinearLayoutManager linearLayoutManager;
    ResponseDto responseDto;
    TextView text_loantype, text_outstanding, loanamount, gloan, os;
    private ListOfShg shgDto;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView mHeader, Name,date,amt,inrt;
    private String loan_id = "";
    private String account_number = "";
    private String loantype = "";
    private String bankname = "";

    public GroupLoanSummary_Otherloan() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_group_loan_summary__otherloan, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        try {
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
            Bundle bundle = getArguments();
            loan_id = bundle.getString("loan_id");
            Log.d("loan", loan_id);
            account_number = bundle.getString("account_number");
            Log.d("Mem4", account_number);
            loantype = bundle.getString("loan_type");
            bankname = bundle.getString("bank_name");

            text_loantype = (TextView) view.findViewById(R.id.loanamount);
            text_outstanding = (TextView) view.findViewById(R.id.group_otherLoan_values);
            os = (TextView) view.findViewById(R.id.os);
            gloan = (TextView) view.findViewById(R.id.gloan);
            loanamount = (TextView) view.findViewById(R.id.loanamount);
            mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);
            text_loantype.setTypeface(LoginActivity.sTypeface);
            text_outstanding.setTypeface(LoginActivity.sTypeface);
            gloan.setTypeface(LoginActivity.sTypeface);
            mCashinHand = (TextView) view.findViewById(R.id.ch);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) view.findViewById(R.id.cb);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);
            gloan.setText(AppStrings.groupLoan + " :");
            os.setText(AppStrings.outstanding + " ");

            mHeader = (TextView) view.findViewById(R.id.fragmentHeader);
            mHeader.setText(loantype + " " + AppStrings.summary);
            mHeader.setTypeface(LoginActivity.sTypeface);
            loanamount.setTypeface(LoginActivity.sTypeface);
            os.setTypeface(LoginActivity.sTypeface);

            date = (TextView) view.findViewById(R.id.date);
            date.setText(AppStrings.date);
            date.setTypeface(LoginActivity.sTypeface);
            amt = (TextView) view.findViewById(R.id.amt);
            amt.setText(AppStrings.amount);
            amt.setTypeface(LoginActivity.sTypeface);
            inrt = (TextView) view.findViewById(R.id.inrt);
            inrt.setText(AppStrings.interest);
            inrt.setTypeface(LoginActivity.sTypeface);

            grouploan_summary_otherloan_expandablelistview = (RecyclerView) view.findViewById(R.id.grouploan_summary_otherloan_expandablelistview);
            linearLayoutManager = new LinearLayoutManager(getActivity());
            grouploan_summary_otherloan_expandablelistview.setLayoutManager(linearLayoutManager);
            grouploan_summary_otherloan_expandablelistview.setHasFixedSize(true);
            grouploan_summary_otherloan_expandablelistview.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStart() {
        super.onStart();

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

            RestClient.getRestClient(GroupLoanSummary_Otherloan.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GROUPLOANSUMMARY_OTHERLOAN + loan_id, getActivity(), ServiceType.GROUPLOANSUMMARY_OTHERLOAN_TYPE);
        } else {
            Utils.showToast(getActivity(), "Network Not Available");
        }
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        switch (serviceType) {
            case GROUPLOANSUMMARY_OTHERLOAN_TYPE:
                if (result != null && result.length() > 0) {
                    Log.d("getDetails", " " + result);
                    responseDto = new Gson().fromJson(result, ResponseDto.class);
                    if (responseDto.getStatusCode() == (Utils.Success_Code)) {
                        Utils.showToast(getActivity(), responseDto.getMessage());
                        responseDto.getResponseContent().getGroupLoanSummary().getGroupLoanSummaryDTOList();
                        Log.d("check", "" + responseDto.getResponseContent().getGroupInternalLoansList());

                        groupLoanSummary_otherloan_adapter = new GroupLoanSummary_Otherloan_Adapter(getActivity(), responseDto.getResponseContent().getGroupLoanSummary().getGroupLoanSummaryDTOList());
                        grouploan_summary_otherloan_expandablelistview.setAdapter(groupLoanSummary_otherloan_adapter);
                        text_loantype.setText("₹" + responseDto.getResponseContent().getGroupLoanSummary().getLoanSanctionAmount());
                        text_outstanding.setText("₹" + responseDto.getResponseContent().getGroupLoanSummary().getLoanOutstandingAmount());
                    }
                }
                break;
        }
    }
}

package com.oasys.eshakti.fragment;

import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.R;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.views.CustomHorizontalScrollView;
import com.oasys.eshakti.views.RaisedButton;


public class Transaction_Audit_MemberLoan extends Fragment implements View.OnClickListener {

    private  View view;
    private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
    private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;
    private TextView mTransaction_audit_date,mloanType;
    private RaisedButton mNext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_blank, container, false);
        inIt(view);
        return view;
    }

    private void inIt(View view1) {

        mTransaction_audit_date = (TextView)view1.findViewById(R.id.transaction_audit_date);
        mTransaction_audit_date.setTypeface(LoginActivity.sTypeface);
        mTransaction_audit_date.setText(AppStrings.Transaction_audit_date +""+ "10/09/2020");

        mloanType = (TextView)view1.findViewById(R.id.loanType);
        mloanType.setTypeface(LoginActivity.sTypeface);
        mloanType.setText(AppStrings.Bankloan +"-"+ "Term Loan");

        mNext = (RaisedButton) view1.findViewById(R.id.fragment_Submit);
        mNext.setText(AppStrings.nnext);
        mNext.setOnClickListener(this);


        mLeftHeaderTable = (TableLayout) view1.findViewById(R.id.LeftHeaderTable);
        mRightHeaderTable = (TableLayout) view1.findViewById(R.id.RightHeaderTable);
        mLeftContentTable = (TableLayout) view1.findViewById(R.id.LeftContentTable);
        mRightContentTable = (TableLayout) view1.findViewById(R.id.RightContentTable);

        mHSRightHeader = (CustomHorizontalScrollView) view1.findViewById(R.id.rightHeaderHScrollView);
        mHSRightContent = (CustomHorizontalScrollView) view1.findViewById(R.id.rightContentHScrollView);

        mHSRightHeader.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
            @Override
            public void onScrollChanged(int l, int t, int oldl, int oldt) {

                mHSRightContent.scrollTo(l, 0);
            }
        });


        mHSRightContent.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
            @Override
            public void onScrollChanged(int l, int t, int oldl, int oldt) {
                mHSRightHeader.scrollTo(l, 0);
            }
        });


        TableRow leftHeaderRow = new TableRow(getActivity());

        TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

        TextView mMemberName_Header = new TextView(getActivity());
        mMemberName_Header.setText(AppStrings.memberName);
        mMemberName_Header.setTypeface(LoginActivity.sTypeface);
        mMemberName_Header.setTextColor(Color.WHITE);
        mMemberName_Header.setPadding(10, 5, 10, 5);
        mMemberName_Header.setLayoutParams(lHeaderParams);
        leftHeaderRow.addView(mMemberName_Header);

        mLeftHeaderTable.addView(leftHeaderRow);

        TableRow rightHeaderRow = new TableRow(getActivity());

        TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        contentParams.setMargins(10, 0, 10, 0);

        TextView mOutstanding_Header = new TextView(getActivity());
        mOutstanding_Header.setText(AppStrings.previousamount);
        mOutstanding_Header.setTypeface(LoginActivity.sTypeface);
        mOutstanding_Header.setTextColor(Color.WHITE);
        mOutstanding_Header.setPadding(10, 5, 10, 5);
        mOutstanding_Header.setGravity(Gravity.LEFT);
        mOutstanding_Header.setLayoutParams(contentParams);
        mOutstanding_Header.setBackgroundResource(R.color.tableHeader);
        rightHeaderRow.addView(mOutstanding_Header);


        TableRow.LayoutParams POLParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        POLParams.setMargins(10, 0, 10, 0);

        TextView mPurposeOfLoan_Header = new TextView(getActivity());
        mPurposeOfLoan_Header
                .setText(AppStrings.previousinterest);
        mPurposeOfLoan_Header.setTypeface(LoginActivity.sTypeface);
        mPurposeOfLoan_Header.setTextColor(Color.WHITE);
        mPurposeOfLoan_Header.setGravity(Gravity.RIGHT);
        mPurposeOfLoan_Header.setLayoutParams(POLParams);
        mPurposeOfLoan_Header.setPadding(25, 5, 10, 5);
        mPurposeOfLoan_Header.setBackgroundResource(R.color.tableHeader);
        rightHeaderRow.addView(mPurposeOfLoan_Header);


        TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        rHeaderParams.setMargins(10, 0, 10, 0);

        TextView mPL_Header = new TextView(getActivity());
        mPL_Header.setText(AppStrings.currentamount);
        mPL_Header.setTypeface(LoginActivity.sTypeface);
        mPL_Header.setTextColor(Color.WHITE);
        mPL_Header.setPadding(10, 5, 10, 5);
        mPL_Header.setGravity(Gravity.CENTER);
        mPL_Header.setLayoutParams(rHeaderParams);
        mPL_Header.setBackgroundResource(R.color.tableHeader);
        rightHeaderRow.addView(mPL_Header);



        TableRow.LayoutParams rBefore = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        rBefore.setMargins(10, 0, 10, 0);

        TextView mYear_Header = new TextView(getActivity());
        mYear_Header.setText(AppStrings.currentinterest);
        mYear_Header.setTypeface(LoginActivity.sTypeface);
        mYear_Header.setTextColor(Color.WHITE);
        mYear_Header.setPadding(10, 5, 10, 5);
        mYear_Header.setGravity(Gravity.CENTER);
        mYear_Header.setLayoutParams(rHeaderParams);
        mYear_Header.setBackgroundResource(R.color.tableHeader);
        rightHeaderRow.addView(mYear_Header);
        mRightHeaderTable.addView(rightHeaderRow);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.fragment_Submit:
//                NewDrawerScreen.showFragment(new Transaction_Audit_savings());
        }
    }
}
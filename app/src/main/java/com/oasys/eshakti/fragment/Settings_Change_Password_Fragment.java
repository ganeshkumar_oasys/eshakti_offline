package com.oasys.eshakti.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.RequestDto.ChangepasswordDto;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;

import com.oasys.eshakti.OasysUtils.RegionalConversion;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.database.SHGTable;


/**
 * A simple {@link Fragment} subclass.
 */
public class Settings_Change_Password_Fragment extends Fragment implements View.OnClickListener, NewTaskListener {

    private View view;
    private EditText oldpassword;
    private EditText newpassword;
    private EditText confirmpassword;
    private TextView op_lbl,np_lbl,cp_lbl;
    private String str_oldpwd = "";
    private String str_newpwd = "";
    private String str_confpwd = "";
    private Button submitpassword;
    public static EditText oldPwdEditText, newPwdEditText, confirmPwdEditText;
    private ListOfShg shgDto;
    private TextView groupname,memberName,cashinHand,cashatBank;
    String oldpass,newpass;

    public Settings_Change_Password_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings__change__password_, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        return view;
    }

    public void init() {

        groupname = (TextView) view.findViewById(R.id.groupname);
        groupname.setText(shgDto.getName());
        groupname.setTypeface(LoginActivity.sTypeface);
        memberName = (TextView) view.findViewById(R.id.memberName);
        cashinHand = (TextView) view.findViewById(R.id.cashinHand);
        cashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        cashinHand.setTypeface(LoginActivity.sTypeface);
        cashatBank = (TextView) view.findViewById(R.id.cashatBank);
        cashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        cashatBank.setTypeface(LoginActivity.sTypeface);
        oldpassword = (EditText) view.findViewById(R.id.settings_OldPwdeditText);
       // oldpassword.setText(RegionalConversion.getRegionalConversion(AppStrings.OldPassword));
        oldpassword.setBackgroundResource(R.drawable.edittext_background);
        oldpassword.setHint(AppStrings.OldPassword);
        oldpassword.setTypeface(LoginActivity.sTypeface);
        newpassword = (EditText) view.findViewById(R.id.settings_NewPwdeditText);
      //  newpassword.setText(RegionalConversion.getRegionalConversion(AppStrings.NewPassword));
        newpassword.setBackgroundResource(R.drawable.edittext_background);
        newpassword.setHint(AppStrings.NewPassword);
        newpassword.setTypeface(LoginActivity.sTypeface);
        confirmpassword = (EditText) view.findViewById(R.id.settings_ConfirmPwdeditText);
        //confirmpassword.setText(RegionalConversion.getRegionalConversion(AppStrings.ConfirmPassword));
        confirmpassword.setBackgroundResource(R.drawable.edittext_background);
        confirmpassword.setHint(AppStrings.ConfirmPassword);
        confirmpassword.setTypeface(LoginActivity.sTypeface);
        submitpassword = (Button) view.findViewById(R.id.fragment_settings_changepassword_Submitbutton);
        submitpassword.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
        submitpassword.setTypeface(LoginActivity.sTypeface);


        cp_lbl = (TextView) view.findViewById(R.id.cp_lbl);
        cp_lbl.setText(AppStrings.ConfirmPassword);
        cp_lbl.setTypeface(LoginActivity.sTypeface);
        np_lbl = (TextView) view.findViewById(R.id.np_lbl);
        np_lbl.setText(AppStrings.NewPassword);
        np_lbl.setTypeface(LoginActivity.sTypeface);
        op_lbl = (TextView) view.findViewById(R.id.op_lbl);
        op_lbl.setTypeface(LoginActivity.sTypeface);
        op_lbl.setText(AppStrings.OldPassword);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        submitpassword.setOnClickListener(this);
    }

    public void validate() {
        str_oldpwd = oldpassword.getText().toString();
        str_newpwd = newpassword.getText().toString();
        str_confpwd = confirmpassword.getText().toString();
        if (str_oldpwd.equals("")) {
            oldpassword.setError("Enter old password");
            oldpassword.requestFocus();
            return;
        } else if (str_oldpwd.length() < 6) {
            oldpassword.setError("Enter Correct password ");
            oldpassword.requestFocus();
            return;

        } else if (str_newpwd.equals("")) {
            newpassword.setError("Enter New password");
            newpassword.requestFocus();
            return;
        } else if (str_newpwd.length() < 6) {
            newpassword.setError("Password Should be 6 Characters");
            newpassword.requestFocus();
            return;
        } else if (str_confpwd.equals("")) {
            confirmpassword.setError("Confirm password should not be empty");
            confirmpassword.requestFocus();
            return;
        } else if (!str_confpwd.equals(str_newpwd)) {
            confirmpassword.setError("Newpassword and oldpassword should be same");
            confirmpassword.requestFocus();
            return;
        } else {
            String uname = MySharedPreference.readString(getActivity(), MySharedPreference.USERNAME, "");
            ChangepasswordDto changepasswordDto = new ChangepasswordDto();
            changepasswordDto.setUsername(uname);
           /* try {
                 oldpass=AESCrypt.encrypt(String.valueOf(str_oldpwd));
                 newpass=AESCrypt.encrypt(String.valueOf(str_newpwd));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("hs",AESCrypt.encryptedValue);*/

            changepasswordDto.setOldPassword(str_oldpwd);
            changepasswordDto.setNewPassword(str_newpwd);
            Log.d("Changepwd",changepasswordDto.toString());
            String changepasswordReqJson = new Gson().toJson(changepasswordDto);

            if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

                RestClient.getRestClient(Settings_Change_Password_Fragment.this).callRestWebService(Constants.BASE_URL + Constants.SETTINGS_CHANGEPASSWORD, changepasswordReqJson, getActivity(), ServiceType.CHANGEPASSWORD);

            } else {
                NewDrawerScreen.showFragment(new MainFragment());
                Utils.showToast(getActivity(), "No network Available");
            }
        }
    }

    @Override
    public void onClick(View view) {
        validate();
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        switch (serviceType) {
            case CHANGEPASSWORD:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    if (cdto.getStatusCode() == Utils.Success_Code) {
                        Utils.showToast(getActivity(), cdto.getMessage());
                        FragmentManager fm = getFragmentManager();
                        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        NewDrawerScreen.showFragment(new MainFragment());
                    } else {
                        if (cdto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), cdto.getMessage());
                    }
                } catch (Exception e) {

                }

        }


    }
}

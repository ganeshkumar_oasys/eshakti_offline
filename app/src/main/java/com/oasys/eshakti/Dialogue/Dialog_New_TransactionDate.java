package com.oasys.eshakti.Dialogue;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.Dto.CashOfGroup;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.Dto.ShgBankDetails;
import com.oasys.eshakti.Dto.ShggroupDetailsDTO;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.activity.NewDrawerScreen;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.fragment.Attendance;
import com.oasys.eshakti.fragment.BankTransaction;

import com.oasys.eshakti.fragment.Expense;
import com.oasys.eshakti.fragment.GroupLoanRepayment;
import com.oasys.eshakti.fragment.Income;
import com.oasys.eshakti.fragment.LoanDisbursement;
import com.oasys.eshakti.fragment.Meeting_audit_Fragment;
import com.oasys.eshakti.fragment.Meetings_training;
import com.oasys.eshakti.fragment.MinutesOFMeeting;
import com.oasys.eshakti.fragment.Savings;
import com.oasys.eshakti.fragment.Transaction_memberloan_repayment;
import com.oasys.eshakti.views.ButtonFlat;
import com.tutorialsee.lib.TastyToast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Dialog_New_TransactionDate extends DialogFragment implements DatePickerDialog.OnDateSetListener, NewTaskListener {
    static String sItem = null;
    static Context mContext;
    private View rootView;
    private int mSize;
    private List<MemberList> memList;
    private ArrayList<ShgBankDetails> bankdetails;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    private Date lastTransaDate, systemDate, openingDate, mod_date, newTransa_Date;
    public static Date d2;
    private String shgformationMillis;
    private ListOfShg shgDetails;
    private Dialog mProgressDialog;
    String dateStrs;
    String yesterdayAsString;
    private Date nextMonthFirstDay;
    private String dateStr2;
    private SharedPreferences.Editor myEdit;
    private  String shgGroup_transactiontdy;
    String date="";
    int datepick=0;
    String defaultdatepick="";
    public  static CashOfGroup cg;
    Dialog confirmationDialog;

    private SharedPreferences sharedPreferences;
    public Dialog_New_TransactionDate() {
        // TODO Auto-generated constructor stub
    }

    @SuppressLint("ValidFragment")
    public Dialog_New_TransactionDate(Context context, String item) {
        // TODO Auto-generated constructor stub
        mContext = context;
        sItem = item;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        // setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        rootView = inflater.inflate(R.layout.dialog_new_transactiondate, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDetails = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgGroup_transactiontdy = shgDetails.getIs_transaction_tdy();
        Log.d("transaction",shgGroup_transactiontdy);
        cg = new CashOfGroup();

        init();
        networkConnection = NetworkConnection.getNetworkConnection(getActivity());
        if (networkConnection.isNetworkAvailable()) {
//            onTaskStarted();
            //   RestClient.getRestClient(GroupProfileActivity.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.VERIFICATION + shgDetail, GroupProfileActivity.this, ServiceType.VERIFICATION);
            RestClient.getRestClient((NewTaskListener) getContext()).callWebServiceForGetMethod(Constants.BASE_URL + Constants.VERIFICATION + shgDetails.getShgId(), getActivity(), ServiceType.VERIFICATION);
        }
    }

    private void init() {
        // networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        TextView dialogMsg = (TextView) rootView.findViewById(R.id.dialog_continueDate);
        dialogMsg.setTypeface(LoginActivity.sTypeface);
        TextView dialogTrans = (TextView) rootView.findViewById(R.id.dialog_TransactionDate);
        dialogTrans.setTypeface(LoginActivity.sTypeface);

        DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat simple1 = new SimpleDateFormat("dd/MM/yyyy");
     /*   Date d = new Date(Long.parseLong(SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).getLastTransactionDate()));
        String dateStr = simple.format(d);
        dialogMsg.setText(dateStr + " : " + AppStrings.dialogMsg);*/

        if (shgDto.getLastTransactionDate() != null && shgDto.getLastTransactionDate().length() > 0) {
            Date d = new Date(Long.parseLong(shgDto.getLastTransactionDate()));
            String dateStr = simple.format(d);
            String dateStr1 = simple1.format(d);

//            String sDate = "31012014";
//            SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy", Locale.getDefault());
//            Date date = dateFormat.parse(dateStr);
//            Calendar calendar = Calendar.getInstance();
//            calendar.setTime(d);
//            calendar.add(Calendar.DATE, -1);
//            calendar.add(Calendar.MONTH,1);
//            calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
//            nextMonthFirstDay = calendar.getTime();
//            Log.d("date",""+nextMonthFirstDay.getTime());
//            yesterdayAsString = simple.format(nextMonthFirstDay.getTime());

            try {
                Calendar calender = Calendar.getInstance();
                String formattedDate = df.format(calender.getTime());
                String mBalanceSheetDate = shgDto.getLastTransactionDate(); // dd/MM/yyyy
                String modifiedDate = null;
                if (shgDto.getModifiedDate() != null) {
                    modifiedDate = shgDto.getModifiedDate();
                    Date d1 = new Date(Long.parseLong(mBalanceSheetDate));
                    Date d2 = new Date(Long.parseLong(modifiedDate));
                    String dateStr3 = df.format(d1);
                    String formatted_balancesheetDate = dateStr3;

                    String dateStr4 = df.format(d2);
                    String formatted_modifiedDate = dateStr4;
                    Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");
                    lastTransaDate = df.parse(formatted_balancesheetDate);
                    mod_date = df.parse(formatted_modifiedDate);
                    systemDate = df.parse(formattedDate);
                } else {
                    Date d1 = new Date(Long.parseLong(mBalanceSheetDate));
                    //   Date d2 = new Date(Long.parseLong(modifiedDate));
                    String dateStr3 = df.format(d1);
                    String formatted_balancesheetDate = dateStr3;

                    Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");
                    lastTransaDate = df.parse(formatted_balancesheetDate);
                    mod_date = df.parse(formattedDate);
                    systemDate = df.parse(formattedDate);
                }
                // String mBalanceSheetDate = "1512844200000";


                if (mod_date != null) {
                    if (mod_date.compareTo(systemDate) == 0) {
                        //  if (shgDto.getFFlag() == "1") {  //TODO:: FIRST TRANSADATE UPDATE
                        Calendar cInstance = Calendar.getInstance();
                        Calendar lcal = Calendar.getInstance();
                        lcal.setTimeInMillis(lastTransaDate.getTime());
                        int lyear = lcal.get(Calendar.YEAR); // this is deprecated
                        int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
                        int lday = lcal.get(Calendar.DATE);
                        Calendar ccal1 = Calendar.getInstance();
                        ccal1.setTimeInMillis(systemDate.getTime());
                        int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
                        int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
                        int cday = ccal1.get(Calendar.DATE);
                        if (cday == lday && cmonth == lmonth && cyear == lyear) {
                            cInstance.set(cyear, cmonth, cday);
                        } else {

                            int diffYear = ccal1.get(Calendar.YEAR) - lcal.get(Calendar.YEAR);
                            int diffMonth = diffYear * 12 + ccal1.get(Calendar.MONTH) - lcal.get(Calendar.MONTH);
                            //
                            // if (((cmonth - lmonth) <= 1 && (cyear == lyear || cyear > lyear)) && ((lmonth - cmonth) <= 1 && (cyear == lyear || cyear > lyear))) {
                            if (diffMonth <= 1) {
                                cInstance.set(cyear, cmonth, cday);
                            } else if (diffMonth > 1) {
                                if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                    Calendar c = Calendar.getInstance();
                                    c.setTime(lastTransaDate);
                                    c.add(Calendar.MONTH, 1);
                                    c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
                                    cInstance.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                                } else {
                                    Calendar c = Calendar.getInstance();
                                    c.setTime(lastTransaDate);
                                   /* c.add(Calendar.MONTH, 1);
                                    c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));*/
                                    cInstance.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                                }
                                //    cInstance.set(cyear, cmonth, cday);
                            }

                        }
                        dateStr = df.format(cInstance.getTime());
                        newTransa_Date = df.parse(dateStr);
                        /*} else {  //TODO:: fIRST tRANSAdATE UPDATE
                            Calendar cInstance = Calendar.getInstance();
                            Calendar lcal = Calendar.getInstance();
                            lcal.setTimeInMillis(lastTransaDate.getTime());
                            int lyear = lcal.get(Calendar.YEAR); // this is deprecated
                            int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
                            int lday = lcal.get(Calendar.DATE);

                            Calendar ccal1 = Calendar.getInstance();
                            ccal1.setTimeInMillis(systemDate.getTime());
                            int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
                            int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
                            int cday = ccal1.get(Calendar.DATE);

                        }*/


                    } else if (mod_date.compareTo(systemDate) < 0) {

                        Calendar cInstance = Calendar.getInstance();
                        Calendar lcal = Calendar.getInstance();
                        lcal.setTimeInMillis(lastTransaDate.getTime());
                        int lyear = lcal.get(Calendar.YEAR); // this is deprecated
                        int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
                        int lday = lcal.get(Calendar.DATE);
                        Calendar ccal1 = Calendar.getInstance();
                        ccal1.setTimeInMillis(systemDate.getTime());
                        int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
                        int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
                        int cday = ccal1.get(Calendar.DATE);
                        if ((((cmonth - lmonth) <= 1 && (lmonth - cmonth) <= 1)) && (cyear == lyear || cyear > lyear)) {
                            cInstance.set(cyear, cmonth, cday);
                        } else {
                            Calendar c = Calendar.getInstance();
                            c.setTime(lastTransaDate);
                            c.add(Calendar.MONTH, 1);
                            c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
                            cInstance.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));

                        }
                        dateStr = df.format(cInstance.getTime());
                        newTransa_Date = df.parse(dateStr);
                    }

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
//            dialogMsg.setText(EShaktiApplication.vertficationDto.getShggroupDetailsDTO().getTransactionDate() + " : " + AppStrings.dialogMsg);
            DateFormat simple2 = new SimpleDateFormat("dd/MM/yyyy");
             d2 = new Date(Long.parseLong(shgDto.getLastTransactionDate()));
            dateStr2 = simple.format(d2);
            dialogTrans.setText(AppStrings.lastTransactionDate + " " + dateStr2);

            Log.d("doyouwantdate", Utils.doyouwantdate);
            Calendar ccal12 = Calendar.getInstance();
            ccal12.setTimeInMillis(systemDate.getTime());
            int cyear = ccal12.get(Calendar.YEAR); // this is deprecated
            int cmonth = ccal12.get(Calendar.MONTH);
            DateFormat simple3 = new SimpleDateFormat("dd/MM/yyyy");
            Date d3 = new Date(Long.parseLong(String.valueOf(systemDate.getTime())));
            String C_date = simple3.format(d3);

            Calendar ccal2 = Calendar.getInstance();
            ccal2.setTimeInMillis(systemDate.getTime());
            int douwantyear = ccal2.get(Calendar.YEAR); // this is deprecated
            int douwantmonth = ccal2.get(Calendar.MONTH);

            Calendar cInstance = Calendar.getInstance();
            Calendar lcal = Calendar.getInstance();
            lcal.setTimeInMillis(lastTransaDate.getTime());
            int lyear = lcal.get(Calendar.YEAR); // this is deprecated
            int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
            int lday = lcal.get(Calendar.DATE);

            if (shgGroup_transactiontdy.equals("1.0")) {
                if (Utils.doyouwantdate.equals(dateStr2)) {
                    dialogMsg.setText(dateStr2 + " : " + AppStrings.dialogMsg);
                    Utils.doyouwantdate = dateStr2;
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(d2);
                    nextMonthFirstDay = calendar.getTime();


                } else if (lyear == cyear && lmonth == cmonth) {
                    nextMonthFirstDay = ccal12.getTime();
                    Log.d("date", "" + nextMonthFirstDay.getTime());
                    yesterdayAsString = simple.format(nextMonthFirstDay.getTime());
                    dialogMsg.setText(C_date + " : " + AppStrings.dialogMsg);
                    Utils.doyouwantdate = yesterdayAsString;
                } else {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(d);
//                    calendar.add(Calendar.MONTH, 1);
//                    calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                    nextMonthFirstDay = calendar.getTime();
                    Log.d("date", "" + nextMonthFirstDay.getTime());
                    yesterdayAsString = simple.format(nextMonthFirstDay.getTime());
                    dialogMsg.setText(yesterdayAsString + " : " + AppStrings.dialogMsg);
                    Utils.doyouwantdate = yesterdayAsString;
                }

            } else if(shgGroup_transactiontdy.equals("0.0")) {

                if (Utils.doyouwantdate.equals(dateStr2)) {
                    dialogMsg.setText(dateStr2 + " : " + AppStrings.dialogMsg);
                    Utils.doyouwantdate = dateStr2;
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(d2);
                    nextMonthFirstDay = calendar.getTime();

                } else if (lyear == cyear && lmonth == cmonth) {
                    nextMonthFirstDay = ccal12.getTime();
                    Log.d("date", "" + nextMonthFirstDay.getTime());
                    yesterdayAsString = simple.format(nextMonthFirstDay.getTime());
                    dialogMsg.setText(C_date + " : " + AppStrings.dialogMsg);
                    Utils.doyouwantdate = yesterdayAsString;
                } else {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(d);
                    calendar.add(Calendar.MONTH, 1);
                    calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                    nextMonthFirstDay = calendar.getTime();
                    Log.d("date", "" + nextMonthFirstDay.getTime());
                    yesterdayAsString = simple.format(nextMonthFirstDay.getTime());
                    dialogMsg.setText(yesterdayAsString + " : " + AppStrings.dialogMsg);
                    Utils.doyouwantdate = yesterdayAsString;
                }

            }
            else {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(d);
                calendar.add(Calendar.MONTH, 1);
                calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                nextMonthFirstDay = calendar.getTime();
                Log.d("date", "" + nextMonthFirstDay.getTime());
                yesterdayAsString = simple.format(nextMonthFirstDay.getTime());
                dialogMsg.setText(yesterdayAsString + " : " + AppStrings.dialogMsg);
                Utils.doyouwantdate = yesterdayAsString;

            }
        }
//         else {
//            Date d = new Date();
//            String opdateStr = simple.format(d);
//            String opdateStr1 = simple1.format(d);
//            dialogMsg.setText(opdateStr + " : " + AppStrings.dialogMsg);
//            dialogTrans.setText(AppStrings.lastTransactionDate + opdateStr1);
//        }

        ButtonFlat okButton = (ButtonFlat) rootView.findViewById(R.id.f_Yes_button);
        okButton.setText(AppStrings.dialogOk);
        okButton.setTypeface(LoginActivity.sTypeface);

        ButtonFlat noButton = (ButtonFlat) rootView.findViewById(R.id.f_No_button);
        noButton.setText(AppStrings.dialogNo);
        noButton.setTypeface(LoginActivity.sTypeface);


        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                try {
                    lastTransaDate = newTransa_Date;
                    MySharedPreference.writeString(getActivity(), MySharedPreference.LAST_TRANSACTION, lastTransaDate.getTime() + "");
//                    CashOfGroup cg = new CashOfGroup();
                    cg.setCashAtBank(shgDto.getCashAtBank());
                    cg.setCashInHand(shgDto.getCashInHand());
                    cg.setLastTransactionDate(""+nextMonthFirstDay.getTime());
//                    SHGTable.updateSHGDetails(cg, shgDto.getShgId());
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                CURRENT DATE
               /* Calendar ccal12 = Calendar.getInstance();
                ccal12.setTimeInMillis(systemDate.getTime());
                int cyear = ccal12.get(Calendar.YEAR); // this is deprecated
                int cmonth = ccal12.get(Calendar.MONTH);
                DateFormat simple3 = new SimpleDateFormat("dd/MM/yyyy");
                Date d3 = new Date(Long.parseLong(String.valueOf(systemDate.getTime())));
                String C_date = simple3.format(d3);

                sharedPreferences =getActivity().getSharedPreferences("MySharedPref",MODE_PRIVATE);
                myEdit = sharedPreferences.edit();
                myEdit.putString("Currentdate",C_date);
                myEdit.putString("openingDate",Utils.doyouwantdate);
                myEdit.commit();*/

                switch (sItem) {

                    case NewDrawerScreen.SAVINGS:
                        NewDrawerScreen.showFragment(new Savings());
                        break;
                    case NewDrawerScreen.INCOME:
                        NewDrawerScreen.showFragment(new Income());
                        break;
                    case NewDrawerScreen.EXPENCE:
                        NewDrawerScreen.showFragment(new Expense());
                        break;
                    case NewDrawerScreen.BANK_TRANSACTION:
                        NewDrawerScreen.showFragment(new BankTransaction());
                        break;

                    case NewDrawerScreen.MEMBER_LOAN_REPAYMENT:
                        NewDrawerScreen.showFragment(new Transaction_memberloan_repayment());
                        break;

                    case NewDrawerScreen.LOAN_DISBURSEMENT:
                        NewDrawerScreen.showFragment(new LoanDisbursement());
                        break;

                    case NewDrawerScreen.GROUP_LOAN_REPAYMENT:
                        NewDrawerScreen.showFragment(new GroupLoanRepayment());
                        break;
                    case NewDrawerScreen.MINUTES_OF_MEETINGS:
                        NewDrawerScreen.showFragment(new MinutesOFMeeting());
                        break;
                    case NewDrawerScreen.ATTENDANCE:
                        NewDrawerScreen.showFragment(new Attendance());
                        break;

                    case NewDrawerScreen.AUDITING:
                        NewDrawerScreen.showFragment(new Meeting_audit_Fragment());
                        break;

                    case NewDrawerScreen.TRAINING:
                        NewDrawerScreen.showFragment(new Meetings_training());
                        break;

                    case NewDrawerScreen.STEP_WISE:
                        Attendance attendancefragment = new Attendance();
                        EShaktiApplication.setFlag("1");
//                        CalendarstepwiseDialogShow(view,mContext);
//                        EShaktiApplication.flag="1";
//
////                        Bundle bundle = new Bundle();
////                        bundle.putString("stepwise", "1");
////                        attendancefragment.setArguments(bundle);
                        NewDrawerScreen.showFragment(attendancefragment);
                        break;
                }
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    dismiss();
                    if (shgDto != null)
                        shgformationMillis = shgDto.getOpeningDate();
                    Date opDate = new Date(Long.parseLong(shgformationMillis));
                    String formattedDate1 = df.format(opDate.getTime());
                    openingDate = df.parse(formattedDate1);

                    Calendar calender = Calendar.getInstance();

                    String formattedDate = df.format(calender.getTime());
                    Log.e("Device Date  =  ", formattedDate + "");

                    String mBalanceSheetDate = shgDto.getLastTransactionDate(); // dd/MM/yyyy
                    // String mBalanceSheetDate = "1512844200000";
                    Date d = new Date(Long.parseLong(mBalanceSheetDate));
                    String dateStr = df.format(d);
                    String formatted_balancesheetDate = dateStr;
                    Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

                    lastTransaDate = df.parse(formatted_balancesheetDate);
                    systemDate = df.parse(formattedDate);

//                    if (openingDate.compareTo(lastTransaDate) < 0) {
                        if (lastTransaDate.compareTo(systemDate) < 0 || lastTransaDate.compareTo(systemDate) == 0) {

                            calendarDialogShow(view, mContext);
                        } else {
                            TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY",
                                    TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                        }
                   /* } else {
                        Toast.makeText(getActivity(), "Check Group opening date", Toast.LENGTH_LONG).show();
                    }*/
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }


        });
        ImageView image = (ImageView) rootView.findViewById(R.id.dialog_imageView);
        image.setBackgroundResource(R.drawable.lauchericon);

    }

/*
    private void CalendarstepwiseDialogShow(View view1,final  Context m1Context)
    {
        confirmationDialog = new Dialog(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialog = inflater.inflate(R.layout.stepwisedialog_layout, null);


        RaisedButton okButton = (RaisedButton) dialog.findViewById(R.id.f_Yes_button);
        okButton.setText(AppStrings.dialogOk);


        RaisedButton noButton = (RaisedButton) dialog.findViewById(R.id.f_No_button);
        noButton.setText(AppStrings.dialogNo);


        TextView alert = (TextView) dialog.findViewById(R.id.confirmationHeader);
        alert.setText("ALERT:");
        alert.setTypeface(LoginActivity.sTypeface);

        TextView alertmessage = (TextView) dialog.findViewById(R.id.alertmessage);
        alertmessage.setText("On Entering,The Entire Stepwise process has to be Completed");
        alertmessage.setTypeface(LoginActivity.sTypeface);

        TextView continuemessage = (TextView) dialog.findViewById(R.id.continuemessage);
        continuemessage.setText("Do You Want to Continue?");
        continuemessage.setTypeface(LoginActivity.sTypeface);

        ImageView image = (ImageView) dialog.findViewById(R.id.dialog_imageView);
        image.setBackgroundResource(R.drawable.lauchericon);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmationDialog.dismiss();
                Attendance attendancefragment = new Attendance();
                EShaktiApplication.flag="1";
                NewDrawerScreen.showFragment(attendancefragment);

            }
        });


        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmationDialog.dismiss();
            }
        });


        confirmationDialog.getWindow()
                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialog);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();
       */
/*dialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialogInterface, int i) {
               Attendance attendancefragment = new Attendance();
               EShaktiApplication.flag="1";
               NewDrawerScreen.showFragment(attendancefragment);
               dialogInterface.dismiss();
           }
       });

       dialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialogInterface, int i) {
               dialogInterface.dismiss();
           }
       });*//*



    }
*/

    private void calendarDialogShow(final View view, final Context mContext) {

        Context context = mContext;


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

// ...Irrelevant code for customizing the buttons and title

        LayoutInflater inflater = LayoutInflater.from(context);
        View customView = inflater.inflate(R.layout.datepickerlayout_custom_ly, null);
        dialogBuilder.setView(customView);

        Locale locale = getResources().getConfiguration().locale;
        Locale.setDefault(locale);
         android.widget.DatePicker dpStartDate = (android.widget.DatePicker) customView.findViewById(R.id.dpStartDate);

        final TextView title = (TextView) customView.findViewById(R.id.title);
        final TextView sub_tit = (TextView) customView.findViewById(R.id.sub_tit);

        Calendar now = Calendar.getInstance();
        // final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, now.get(Calendar.YEAR), now.get(Calendar.DATE), now.get(Calendar.DAY_OF_MONTH));
        Calendar min_Cal = Calendar.getInstance();
        Calendar lastDate = Calendar.getInstance();

        Calendar fcal = Calendar.getInstance();
        fcal.setTimeInMillis(openingDate.getTime());
        int fyear = fcal.get(Calendar.YEAR); // this is deprecated
        int fmonth = fcal.get(Calendar.MONTH); // this is deprecated
        int fday = fcal.get(Calendar.DATE);

        Calendar lcal = Calendar.getInstance();
        lcal.setTimeInMillis(lastTransaDate.getTime());
        int lyear = lcal.get(Calendar.YEAR); // this is deprecated
        int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
        int lday = lcal.get(Calendar.DATE);

        Calendar ccal1 = Calendar.getInstance();
        ccal1.setTimeInMillis(systemDate.getTime());
        int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
        int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
        int cday = ccal1.get(Calendar.DATE);

        if (fday == lday && fmonth == lmonth && fyear == lyear) {
            min_Cal.set(lyear, lmonth, lday);
            lastDate.set(cyear, cmonth, cday);
        }
        else if (fyear <= lyear) {

            if (cmonth == lmonth && cyear == lyear) {

                min_Cal.set(lyear, lmonth, lday);
                lastDate.set(cyear, cmonth, cday);

            }
          else if (cmonth <= lmonth && cyear > lyear) {
                min_Cal.set(lyear, lmonth, lday);

                Calendar c = Calendar.getInstance();
                c.setTime(lastTransaDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
               lastDate.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));

            } else if (cmonth > lmonth && cyear == lyear)
            {
                min_Cal.set(lyear, lmonth, lday);
                if ((cmonth - lmonth) == 1) {
//                    lastDate.set(cyear, cmonth, cday);
                } else if ((cmonth - lmonth) > 1) {
                    Calendar c = Calendar.getInstance();
                    c.setTime(lastTransaDate);
                    c.add(Calendar.MONTH, 1);
                    c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                    lastDate.set(cyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
                }

            } else if (cmonth > lmonth && cyear > lyear) {
                min_Cal.set(lyear, lmonth, lday);
                Calendar c = Calendar.getInstance();
                c.setTime(lastTransaDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(lyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
            }

        }
        else
        {
            min_Cal.set(lyear, lmonth, lday);
            Calendar c = Calendar.getInstance();
            c.setTime(lastTransaDate);
            c.add(Calendar.MONTH, 1);
            c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
            lastDate.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
        }

      /*  datePickerDialog.getDatePicker().setMinDate(min_Cal.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(lastDate.getTimeInMillis());*/
        dpStartDate.setCalendarViewShown(false);
        if(lastDate.getTimeInMillis()>=min_Cal.getTimeInMillis()) {
            dpStartDate.setMinDate(min_Cal.getTimeInMillis());
            dpStartDate.setMaxDate(lastDate.getTimeInMillis());
            Log.d("lastdate",""+lastDate.getTimeInMillis());
            defaultdatepick= String.valueOf(lastDate.getTimeInMillis());
        }

        Calendar c = Calendar.getInstance();
        c.setTime(lastTransaDate);
        DateFormat simple1 = new SimpleDateFormat("dd/MM/yyyy");
        String dateStr = simple1.format(c.getTime());

        title.setText(dateStr + " PICK A DATE AFTER THIS DATE");
        sub_tit.setText("Selected Date:");

        dpStartDate.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                Log.e("onDateSet() call", dayOfMonth + "-" + month + "-" + year);
                date = dayOfMonth + "-" + (month + 1) + "-" + year;
                sub_tit.setText("Selected Date:"+date);
                datepick=1;
                try {

                    Calendar c = Calendar.getInstance();
                    c.set(year, month, dayOfMonth);
                    String formattedDate = df.format(c.getTime());
                    lastTransaDate = df.parse(formattedDate);
                    MySharedPreference.writeString(getActivity(), MySharedPreference.LAST_TRANSACTION, lastTransaDate.getTime() + "");


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });




        dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

//                CashOfGroup cg = new CashOfGroup();
                cg.setCashAtBank(shgDto.getCashAtBank());
                cg.setCashInHand(shgDto.getCashInHand());

                if (datepick==0) {
                    DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
                    Date d = new Date(Long.parseLong(defaultdatepick));
                    String dateStr = simple.format(d);
                    lastTransaDate = d;
                    cg.setLastTransactionDate(lastTransaDate.getTime() + "");
                }
                else
                {
                    cg.setLastTransactionDate(lastTransaDate.getTime() + "");
                }


//                    SHGTable.updateSHGDetails(cg, shgDto.getShgId());


                switch (sItem) {

                    case NewDrawerScreen.SAVINGS:
                        NewDrawerScreen.showFragment(new Savings());
                        break;
                    case NewDrawerScreen.INCOME:
                        NewDrawerScreen.showFragment(new Income());
                        break;
                    case NewDrawerScreen.EXPENCE:
                        NewDrawerScreen.showFragment(new Expense());
                        break;
                    case NewDrawerScreen.BANK_TRANSACTION:
                        NewDrawerScreen.showFragment(new BankTransaction());
                        break;
                    case NewDrawerScreen.LOAN_DISBURSEMENT:
                        NewDrawerScreen.showFragment(new LoanDisbursement());
                        break;
                    case NewDrawerScreen.GROUP_LOAN_REPAYMENT:
                        NewDrawerScreen.showFragment(new GroupLoanRepayment());
                        break;
                    case NewDrawerScreen.MEMBER_LOAN_REPAYMENT:
                        NewDrawerScreen.showFragment(new Transaction_memberloan_repayment());
                        break;
                    case NewDrawerScreen.MINUTES_OF_MEETINGS:
                        NewDrawerScreen.showFragment(new MinutesOFMeeting());
                        break;
                    case NewDrawerScreen.ATTENDANCE:
                        NewDrawerScreen.showFragment(new Attendance());
                        break;
                    case NewDrawerScreen.AUDITING:
                        NewDrawerScreen.showFragment(new Meeting_audit_Fragment());
                        break;
                    case NewDrawerScreen.TRAINING:
                        NewDrawerScreen.showFragment(new Meetings_training());
                        break;
                    case NewDrawerScreen.STEP_WISE:
                        Attendance attendancefragment = new Attendance();
                        EShaktiApplication.setFlag("1");
                        NewDrawerScreen.showFragment(attendancefragment);

                        break;
                }
            }
        });
        dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

            }
        });
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();


       /*


        Calendar now = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, now.get(Calendar.YEAR), now.get(Calendar.DATE), now.get(Calendar.DAY_OF_MONTH));
        Calendar min_Cal = Calendar.getInstance();
        Calendar lastDate = Calendar.getInstance();

        Calendar fcal = Calendar.getInstance();
        fcal.setTimeInMillis(openingDate.getTime());
        int fyear = fcal.get(Calendar.YEAR); // this is deprecated
        int fmonth = fcal.get(Calendar.MONTH); // this is deprecated
        int fday = fcal.get(Calendar.DATE);

        Calendar lcal = Calendar.getInstance();
        lcal.setTimeInMillis(lastTransaDate.getTime());
        int lyear = lcal.get(Calendar.YEAR); // this is deprecated
        int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
        int lday = lcal.get(Calendar.DATE);

        Calendar ccal1 = Calendar.getInstance();
        ccal1.setTimeInMillis(systemDate.getTime());
        int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
        int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
        int cday = ccal1.get(Calendar.DATE);

        if (fday == lday && fmonth == lmonth && fyear == lyear) {
            min_Cal.set(lyear, lmonth, lday);
            lastDate.set(cyear, cmonth, cday);
        } else if (fyear <= lyear) {

            if (cmonth == lmonth && cyear == lyear) {
                min_Cal.set(lyear, lmonth, lday);
                lastDate.set(cyear, cmonth, cday);

            } else if (cmonth <= lmonth && cyear > lyear) {
                min_Cal.set(lyear, lmonth, lday);
                Calendar c = Calendar.getInstance();
                c.setTime(lastTransaDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(lyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));

            } else if (cmonth > lmonth && cyear == lyear) {
                min_Cal.set(lyear, lmonth, lday);
                if ((cmonth - lmonth) == 1) {
                    lastDate.set(cyear, cmonth, cday);
                } else if ((cmonth - lmonth) > 1) {
                    Calendar c = Calendar.getInstance();
                    c.setTime(lastTransaDate);
                    c.add(Calendar.MONTH, 1);
                    c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                    lastDate.set(cyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
                }

            } else if (cmonth > lmonth && cyear > lyear) {
                min_Cal.set(lyear, lmonth, lday);
                Calendar c = Calendar.getInstance();
                c.setTime(lastTransaDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(lyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
            }

        }

        datePickerDialog.getDatePicker().setMinDate(min_Cal.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(lastDate.getTimeInMillis());

        datePickerDialog.show();*/
    }


    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
    }

    private static boolean isBrokenSamsungDevice() {
        return (isBetweenAndroidVersions(
                Build.VERSION_CODES.LOLLIPOP,
                Build.VERSION_CODES.O_MR1));
    }

    private static boolean isBetweenAndroidVersions(int min, int max) {
        return Build.VERSION.SDK_INT >= min && Build.VERSION.SDK_INT <= max;
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Log.e("onDateSet() call", dayOfMonth + "-" + month + "-" + year);
        //String date = dayOfMonth + "-" + (month + 1) + "-" + year;
        try {

            Calendar c = Calendar.getInstance();
            c.set(year, month, dayOfMonth);
            String formattedDate = df.format(c.getTime());
            lastTransaDate = df.parse(formattedDate);
            MySharedPreference.writeString(getActivity(), MySharedPreference.LAST_TRANSACTION, lastTransaDate.getTime() + "");

//            CashOfGroup cg = new CashOfGroup();
            cg.setCashAtBank(shgDto.getCashAtBank());
            cg.setCashInHand(shgDto.getCashInHand());
            cg.setLastTransactionDate(lastTransaDate.getTime() + "");
//            SHGTable.updateSHGDetails(cg, shgDto.getShgId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        switch (sItem) {

            case NewDrawerScreen.SAVINGS:
                NewDrawerScreen.showFragment(new Savings());
                break;
            case NewDrawerScreen.INCOME:
                NewDrawerScreen.showFragment(new Income());
                break;
            case NewDrawerScreen.EXPENCE:
                NewDrawerScreen.showFragment(new Expense());
                break;
            case NewDrawerScreen.BANK_TRANSACTION:
                NewDrawerScreen.showFragment(new BankTransaction());
                break;
            case NewDrawerScreen.LOAN_DISBURSEMENT:
                NewDrawerScreen.showFragment(new LoanDisbursement());
                break;
            case NewDrawerScreen.GROUP_LOAN_REPAYMENT:
                NewDrawerScreen.showFragment(new GroupLoanRepayment());
                break;
            case NewDrawerScreen.CREDIT_LINKAGE_INFO:
                break;
            case NewDrawerScreen.ANIMATOR_PROFILE:
                break;
            case NewDrawerScreen.MEMBER_LOAN_REPAYMENT:
                NewDrawerScreen.showFragment(new Transaction_memberloan_repayment());
                break;
            case NewDrawerScreen.MINUTES_OF_MEETINGS:
                NewDrawerScreen.showFragment(new MinutesOFMeeting());
                break;
            case NewDrawerScreen.ATTENDANCE:
                NewDrawerScreen.showFragment(new Attendance());
                break;
            case NewDrawerScreen.AUDITING:
                NewDrawerScreen.showFragment(new Meeting_audit_Fragment());
                break;
            case NewDrawerScreen.TRAINING:
                NewDrawerScreen.showFragment(new Meetings_training());
                break;


        }
    }

    @Override
    public void onTaskStarted() {
        mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDialog.show();
    }



    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        if (mProgressDialog != null) {
            if ((mProgressDialog != null) && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        }
        switch (serviceType) {
            case VERIFICATION:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        ShggroupDetailsDTO shg = cdto.getResponseContent().getShggroupDetailsDTO();
                        dateStrs=cdto.getResponseContent().getShggroupDetailsDTO().getTransactionDate();
                    }

                } catch (Exception e) {
                  e.printStackTrace();
                }
        }
    }
}

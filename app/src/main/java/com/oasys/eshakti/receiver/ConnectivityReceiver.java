package com.oasys.eshakti.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.Service.TransactionService;

public class ConnectivityReceiver extends BroadcastReceiver {

    private ConnectivityReceiverListener mConnectivityReceiverListener;

    public ConnectivityReceiver(ConnectivityReceiverListener listener) {
        mConnectivityReceiverListener = listener;
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        mConnectivityReceiverListener.onNetworkConnectionChanged(isConnected(context));

        if (isConnected(context)) {
            if (MySharedPreference.readBoolean(context, MySharedPreference.LOGOUT, false))
                MySharedPreference.writeBoolean(context, MySharedPreference.LOGOUT, false);

        } else {

            if (MySharedPreference.readBoolean(context, MySharedPreference.LOGOUT, false))
                MySharedPreference.writeBoolean(context, MySharedPreference.LOGOUT, false);
         /*   if (MySharedPreference.readBoolean(context, MySharedPreference.LOGOUT, false))
                MySharedPreference.writeBoolean(context, MySharedPreference.LOGOUT, false);
            else
                MySharedPreference.writeBoolean(context, MySharedPreference.LOGOUT, true);*/
        }


    }

    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public interface ConnectivityReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);
    }
}
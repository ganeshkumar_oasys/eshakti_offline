package com.oasys.eshakti.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.oasys.eshakti.Dto.BankTransaction;
import com.oasys.eshakti.Dto.Donation;
import com.oasys.eshakti.Dto.FDBankTransaction;
import com.oasys.eshakti.Dto.GroupLoanRepayment;
import com.oasys.eshakti.Dto.IncomeDisbursement;
import com.oasys.eshakti.Dto.IncomeType;
import com.oasys.eshakti.Dto.InternalLoanDisbursement;
import com.oasys.eshakti.Dto.MeetingExpense;
import com.oasys.eshakti.Dto.MinutesOfMeeting;
import com.oasys.eshakti.Dto.OtherExpense;
import com.oasys.eshakti.Dto.OtherIncome;
import com.oasys.eshakti.Dto.OtherIncomeGroup;
import com.oasys.eshakti.Dto.Penalty;
import com.oasys.eshakti.Dto.Savings;
import com.oasys.eshakti.Dto.SavingsDisbursement;
import com.oasys.eshakti.Dto.SeedFund;
import com.oasys.eshakti.Dto.Subscription;
import com.oasys.eshakti.Dto.SubscriptionCharges;
import com.oasys.eshakti.Dto.SubscriptiontoFeds;
import com.oasys.eshakti.Dto.TableData;
import com.oasys.eshakti.Dto.VoluntarySavings;
import com.oasys.eshakti.Dto.VoluntarySavingsDisbursement;
import com.oasys.eshakti.Dto.internalLoanRepayment;
import com.oasys.eshakti.Dto.memberLoanRepayment;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.activity.SHGGroupActivity;
import com.oasys.eshakti.fragment.O_MI;

import java.util.ArrayList;


public class DbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "eshakti.db";
    private static final int DATABASE_VERSION = 1;

    public static String getDatabase() {
        return DATABASE_NAME;
    }

    private static DbHelper sDataHelper;
    private static SQLiteDatabase database;
    private static Context contextValue;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    public static DbHelper getInstance(final Context context) {
        if (sDataHelper == null) {
            sDataHelper = new DbHelper(context);
        }
        contextValue = context;
        return sDataHelper;
    }



    public static void openDatabase() {
        sDataHelper = new DbHelper(EShaktiApplication.getInstance());
        sDataHelper.onOpen(database);
        database = sDataHelper.getWritableDatabase();
    }

    public static void closeDatabase() {
        if (database != null && database.isOpen()) {
            database.close();
        }
    }


    public void init() {
        getReadableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(TableName.CREATE_LOGIN_TABLE);
            //     db.execSQL(TableName.CREATE_SHG_GROUP);
            //db.execSQL(TableName.CREATE_MEMBER_DETAILS);
            //  db.execSQL(TableName.CREATE_BANK_DETAILS);
            //  db.execSQL(TableName.CREATE_LOAN_DETAILS);
            //    db.execSQL(TableName.CREATE_EXPENSE_DETAILS);
            db.execSQL(TableName.CREATE_LOAN_BANK_POL);
            db.execSQL(TableName.CREATE_LOAN_BANK_LTYPE);
            db.execSQL(TableName.CREATE_LOAN_INSTAL_TYPE);
            db.execSQL(TableName.CREATE_LOAN_BANK_DETAILS);
            db.execSQL(TableName.CREATE_LOAN_IL_TYPE_SETTING);
            db.execSQL(TableName.CREATE_LOAN_FED_SETTING);
            db.execSQL(TableName.CREATE_LOAN_MFI_SETTING);

            db.execSQL(TableName.C_OFFLINE_ANIMATOR_SHG_LIST);
            db.execSQL(TableName.C_OFFLINE_ANIMATOR_MEMBER_LIST);
            db.execSQL(TableName.C_OFFLINE_ANIMATOR_SHG_INCOME_EXPENSE_TYPE);
            db.execSQL(TableName.C_OFFLINE_ANIMATOR_SHG_TRANSACTION_DETAILS);
            db.execSQL(TableName.C_OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST);
            db.execSQL(TableName.C_OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST);
            db.execSQL(TableName.C_OFFLINE_ANIMATOR_SHG_BANK_LIST);
            db.execSQL(TableName.C_OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST);
            db.execSQL(TableName.C_OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST);
            db.execSQL(TableName.C_OFFLINE_ANIMATOR_INTERNAL_LOAN_PURPOSE_LIST);
            db.execSQL(TableName.C_OFFLINE_ANIMATOR_SHG_MINUTES_OF_MEETING);
            db.execSQL(TableName.C_OFFLINE_ANIMATOR_PROFILE);
            db.execSQL(TableName.C_OFFLINE_ANIMATOR_SHG_PROFILE);
            db.execSQL(TableName.C_OFFLINE_ANIMATOR_SHG_TRAINING);
            db.execSQL(TableName.C_OFFLINE_ANIMATOR_SSN_LIST);
            db.execSQL(TableName.C_OFFLINE_ANIMATOR_SHG_RD_LIST);
            db.execSQL(TableName.C_OFFLINE_ANIMATOR_SSN_MASTER);

            db.execSQL(TableName.CREATE_TABLE_TRANSACTION);// TODO:: Transaction
            db.execSQL(TableName.CREATE_MEMBER_BANKFULL_DETAILS);
            db.execSQL(TableName.CREATE_JANDHAN_BANKNAME_DETAILS);

            db.execSQL(TableName.AUDIT_TRANSACTIONSAVINGS);
            db.execSQL(TableName.AUDIT_VOLUNTARY_SAVINGS);
            db.execSQL(TableName.AUDIT_SAVING_DISBURSEMENT);
            db.execSQL(TableName.AUDIT_INTERNAL_LOAN_DISBURSEMENT);
            db.execSQL(TableName.AUDIT_SEED_FUND);
            db.execSQL(TableName.AUDIT_FDBANK_TRANSACTION);
            db.execSQL(TableName.AUDIT_INCOME_DISBURSEMENT);
            db.execSQL(TableName.AUDIT_OTHER_INCOME);
            db.execSQL(TableName.AUDIT_GROUPLOAN_REPAYMENT);
            db.execSQL(TableName.AUDIT_PENATYTRANSACTION);
            db.execSQL(TableName.AUDIT_BANK_TRANSACTION);
            db.execSQL(TableName.AUDIT_SUBSCRIPTION_FEDS);
            db.execSQL(TableName.AUDIT_OTHERINCOME_GROUP);
            db.execSQL(TableName.AUDIT_MEMBERLOAN_REAPYMENT);
            db.execSQL(TableName.AUDIT_VOLUNTARY_SAVINGSDISBURSEMENT);
            db.execSQL(TableName.AUDIT_OTHER_EXPENSE);
            db.execSQL(TableName.AUDIT_MEETING_EXPENSE);
            db.execSQL(TableName.AUDIT_INTERLOAN_REPAYMENT);
            db.execSQL(TableName.AUDIT_DONATION_TRANSACTION);
            db.execSQL(TableName.AUDIT_SUBSCRIPTION_TRANSACTION);
            db.execSQL(TableName.AUDIT_SUBSCRIPTION_CHARGES);

          /*
            db.execSQL(TableName.CREATE_TABLE_TRANS_INCOME);
            db.execSQL(TableName.CREATE_TABLE_TRANS_EXPENSE);
            db.execSQL(TableName.CREATE_TABLE_TRANS_MEM_LOAN_RP);
            db.execSQL(TableName.CREATE_TABLE_TRANS_LOAN_DIS);
            db.execSQL(TableName.CREATE_TABLE_TRANS_BANKTRANS);*/

        } catch (Exception e) {
            e.printStackTrace();

            Log.e(this.getClass().getName(), e.getMessage());
        }
    }


    @SuppressLint("LongLogTag")
    public boolean insertSHGListData(ArrayList<TableData> data, String syncType) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {

            openDatabase();
            try {
                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.SHG_ID, occupation.getId());
                    values.put(TableConstants.NAME, occupation.getName());
                    values.put(TableConstants.GRP_FORMATION_DATE, occupation.getGroupFormationDate());
                    values.put(TableConstants.SHG_CODE, occupation.getCode());
                    if (occupation.getIs_trans_audit() == null || occupation.getIs_trans_audit().equals("")){
                        values.put(TableConstants.IS_TRANS_AUDIT, 0.0);
                    }else {
                        values.put(TableConstants.IS_TRANS_AUDIT, occupation.getIs_trans_audit());
                    }
                    values.put(TableConstants.OPENINGDATE, occupation.getOpeningDate());
                    values.put(TableConstants.ACTIVEFLAG, occupation.getIsVerified());
                    values.put(TableConstants.BLOCKNAME, occupation.getBlockName());
                    values.put(TableConstants.PANCHAYATNAME, occupation.getPanchayatName());
                    values.put(TableConstants.USERID, MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID, ""));
                    values.put(TableConstants.VILLAGENAME, occupation.getVillageName());
                    values.put(TableConstants.DISTRICT_ID, occupation.getDistrictId());
                    values.put(TableConstants.LASTTRANSACTIONDATE, occupation.getLastTransactionDate());
                    values.put(TableConstants.IS_TRANSACTION_TDY, occupation.getIs_transaction_tdy());
                    String cih = occupation.getCashInHand() + "";
                    String cab = occupation.getCashAtBank() + "";
                    if ((cih.equals("")) || (cih == null)) {
                        cih = "0";
                    }
                    if (cih.matches("\\d*\\.?\\d+")) { // match
                        int outStandingAmount = (int) Math.round(Double.parseDouble(cih));
                        cih = String.valueOf(outStandingAmount);

                    }

                    if ((cab.equals("")) || (cab == null)) {
                        cab = "0";
                    }

                    if (cab.matches("\\d*\\.?\\d+")) { // match
                        int outStandingAmount = (int) Math.round(Double.parseDouble(cab));
                        cab = String.valueOf(outStandingAmount);

                    }

                    values.put(TableConstants.CASH_IN_HAND, cih);
                    values.put(TableConstants.CASH_AT_BANK, cab);

                    values.put(TableConstants.PRESIDENT_NAME, occupation.getPresidentName());
                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_SHG_LIST, TableConstants.SHG_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }
            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_SHG_LIST Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public boolean insertMemberListData(ArrayList<TableData> data, String syncType) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.MEMBER_ID, occupation.getMemberId());
                    values.put(TableConstants.MEMBER_NAME, occupation.getMemberName());
                    values.put(TableConstants.USERID, MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID, ""));
                    values.put(TableConstants.SHG_ID, occupation.getShgId());
                    values.put(TableConstants.PHONE_NO, occupation.getPhoneNumber());
                    values.put(TableConstants.ACCOUNT_NO, occupation.getAccountNumber());
                    values.put(TableConstants.MEMBER_USER_ID, occupation.getUserId());
                    values.put(TableConstants.BANKNAME, occupation.getBankName());
                    values.put(TableConstants.BANK_ID, occupation.getBankId());
                    values.put(TableConstants.BANKNAME_ID, occupation.getBankNameId());
                    values.put(TableConstants.BRANCHNAME, occupation.getBranchName());
                    values.put(TableConstants.BRANCH_ID, occupation.getBranchId());
                    values.put(TableConstants.AADHAR_NUM, occupation.getAadharNumber());
                    values.put(TableConstants.PIP_NUM, occupation.getPipNumber());
                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_MEMBER_LIST, TableConstants.MEMBER_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_MEMBER_LIST Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public boolean insertSHGMinutesOMeetingData(ArrayList<TableData> data, String syncType) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.ID, occupation.getId());
                    values.put(TableConstants.NAME, occupation.getName());
                    values.put(TableConstants.ACTIVEFLAG, occupation.getIsOffline());
                    values.put(TableConstants.USERID, MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID, ""));

                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_SHG_MINUTES_OF_MEETING, TableConstants.ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_SHG_INCOME_EXPENSE_TYPE Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }


    @SuppressLint("LongLogTag")
    public boolean insertIncExpData(ArrayList<TableData> data, String syncType) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.ID, occupation.getId());
                    values.put(TableConstants.NAME, occupation.getName());
                    values.put(TableConstants.TYPE, occupation.getType());
                    values.put(TableConstants.USERID, MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID, ""));

                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_SHG_INCOME_EXPENSE_TYPE, TableConstants.ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_SHG_INCOME_EXPENSE_TYPE Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }



    @SuppressLint("LongLogTag")
    public boolean insertAnimatorData(ArrayList<TableData> data, String syncType) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.ID, occupation.getId());
                    values.put(TableConstants.PHONE_NUMBER, occupation.getPhoneNumber());
                    values.put(TableConstants.DATE_OF_ASSIGNING, occupation.getDateOfAssigning());
                    values.put(TableConstants.TOTAL_GROUP, occupation.getTotalGroup());
                    values.put(TableConstants.AGE, occupation.getAge());
                    values.put(TableConstants.ANIMATOR_NAME, occupation.getAnimatorName());
                    values.put(TableConstants.USERID, MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID, ""));

                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_PROFILE, TableConstants.ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_PROFILE Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }


    @SuppressLint("LongLogTag")
    public boolean insertRecurringData(ArrayList<TableData> data, String syncType) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.ID, occupation.getId());
                    values.put(TableConstants.SHG_ID, occupation.getShgId());
                    Log.i("print","SHG -> "+occupation.getShgId());
                    values.put(TableConstants.USERID, MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID, ""));
                    values.put(TableConstants.SHGSAVINGSACCOUNTID, occupation.getShgSavingsAccountId());
                    values.put(TableConstants.RD_BANKNAME, occupation.getBankName());
                    values.put(TableConstants.RD_BRANCHNAME, occupation.getBranchName());
                    values.put(TableConstants.RD_ACCOUNTNO, occupation.getAccountNo());
                    values.put(TableConstants.RD_BANKID, occupation.getBankId());
                    values.put(TableConstants.RD_IFSC, occupation.getIfsc());
                    values.put(TableConstants.RD_BRANCH_ID, occupation.getBranchId());
                    values.put(TableConstants.RD_ISPRIMARY, occupation.getIsPrimary());
                    values.put(TableConstants.RD_INSTALLAMOUNT, occupation.getInstallment_amount());
                    values.put(TableConstants.RD_RATE_OF_INTEREST, occupation.getRate_of_interest());
                    values.put(TableConstants.RD_TENURE, occupation.getTenure());
                    values.put(TableConstants.RD_CURRENTBALANCE, occupation.getCurrent_balance());
                    values.put(TableConstants.RD_OPENINGBALANCE, occupation.getOpening_balance());
                    values.put(TableConstants.RD_STATUS, occupation.getStatus());
                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_SHG_RD_LIST, TableConstants.ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    //String insertQuery = "insert into "+TableName.OFFLINE_ANIMATOR_SHG_RD_LIST + " where ";
                    String insertQuery = "INSERT INTO "+TableName.OFFLINE_ANIMATOR_SHG_RD_LIST +"(shg_Id, user_id, shgSavingsAccountId, bankName, branchName, accountNo, " +
                            "bankId, ifsc, branchId, isPrimary, installment_amount, rate_of_interest, tenure, current_balance, opening_balance, " +
                            "status) VALUES ('"+occupation.getShgId()+"','"+
                            MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID,"")+"','"+
                    occupation.getShgSavingsAccountId()+"','"+occupation.getBankName()+"',"+occupation.getBranchName()+","+occupation.getAccountNo()+",'"+
                            occupation.getBankId()+"',"+occupation.getIfsc()+",'"+occupation.getBranchId()+"',"+occupation.getIsPrimary()+","+
                            occupation.getInstallment_amount()+","+occupation.getRate_of_interest()+","+occupation.getTenure()+","+
                            occupation.getCurrent_balance()+","+occupation.getOpening_balance()+","+occupation.getStatus()+")";
                    Log.i("print","query : "+insertQuery);
                    //database.execSQL(insertQuery);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_SHG_RD_LIST Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }


    @SuppressLint("LongLogTag")
    public boolean insertSsnTypeData(ArrayList<TableData> data, String syncType) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.ID, occupation.getId());
                    values.put(TableConstants.NAME, occupation.getName());
                    values.put(TableConstants.USERID, MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID, ""));
                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_SSN_MASTER, TableConstants.ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_SSN_MASTER Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }




    @SuppressLint("LongLogTag")
    public boolean insertSSNData(ArrayList<TableData> data, String syncType) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.ID, occupation.getId());
                    values.put(TableConstants.SHG_ID, occupation.getShgId());
                    values.put(TableConstants.SSN_MEMBERNAME, occupation.getMemberName());
                    values.put(TableConstants.SSN_BANKNAME, occupation.getBankName());
                    values.put(TableConstants.SSN_BRANCHNAME, occupation.getBranchName());
                    values.put(TableConstants.SSS_TYPE_ID, occupation.getSssTypeId());
                    values.put(TableConstants.MEMBER_ID, occupation.getMemberId());
                    values.put(TableConstants.BANK_NAME_ID, occupation.getBankNameId());
                    values.put(TableConstants.BRANCH_NAME_ID, occupation.getBranchNameId());
                    values.put(TableConstants.SSS_NUMBER, occupation.getSss_number());
                    values.put(TableConstants.IS_SSS, occupation.getIs_sss());
                    values.put(TableConstants.FIRST_UPDATE, occupation.getFirst_update());
                    values.put(TableConstants.LAST_UPDATE, occupation.getLast_update());
                    values.put(TableConstants.SSS_UPDATE, occupation.getSss_date());
                    values.put(TableConstants.IS_BEFORE_2019, occupation.getIs_before_2019());
                    values.put(TableConstants.CREATED_DATE, occupation.getCreated_date());
                    values.put(TableConstants.MODIFIED_DATE, occupation.getModified_date());
//                    long flag = database.insert(TableName.OFFLINE_ANIMATOR_SSN_LIST,null,values);

                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_SSN_LIST,null, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_SSN_LIST Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }


    @SuppressLint("LongLogTag")
    public boolean insertSSNList_Data(TableData occupation) {
        boolean isSuccessFullyInserted = false;
//        if (!data.isEmpty()) {
            openDatabase();
            try {
//                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.ID, occupation.getId());
                    values.put(TableConstants.SHG_ID, occupation.getShgId());
                    values.put(TableConstants.SSN_MEMBERNAME, occupation.getMemberName());
                    values.put(TableConstants.SSN_BANKNAME, occupation.getBankName());
                    values.put(TableConstants.SSN_BRANCHNAME, occupation.getBranchName());
                    values.put(TableConstants.SSS_TYPE_ID, occupation.getSssTypeId());
                    values.put(TableConstants.MEMBER_ID, occupation.getMemberId());
                    values.put(TableConstants.BANK_NAME_ID, occupation.getBankNameId());
                    values.put(TableConstants.BRANCH_NAME_ID, occupation.getBranchNameId());
                    values.put(TableConstants.SSS_NUMBER, occupation.getSss_number());
                    values.put(TableConstants.IS_SSS, occupation.getIs_sss());
                    values.put(TableConstants.FIRST_UPDATE, occupation.getFirst_update());
                    values.put(TableConstants.LAST_UPDATE, occupation.getLast_update());
                    values.put(TableConstants.SSS_UPDATE, occupation.getSss_date());
                    values.put(TableConstants.IS_BEFORE_2019, occupation.getIs_before_2019());
                    values.put(TableConstants.CREATED_DATE, occupation.getCreated_date());
                    values.put(TableConstants.MODIFIED_DATE, occupation.getModified_date());
//                    long flag = database.insert(TableName.OFFLINE_ANIMATOR_SSN_LIST,null,values);

                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_SSN_LIST, TableConstants.ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
//                        break;
                    }
//                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_SSN_LIST Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
//        }
        return isSuccessFullyInserted;
    }


    @SuppressLint("LongLogTag")
    public boolean insertshgGroupData(ArrayList<TableData> data, String syncType) {
        Log.d("bankaccount",data.toString());
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    Log.d("bankaccount","SHG Name : "+occupation.getSHGName()+"\t\tCreated Date : "+occupation.getShgCreatedDate());
                    values.put(TableConstants.SHG_ID, occupation.getId());
                    values.put(TableConstants.SHG_TYPE,occupation.getShgType());
                    values.put(TableConstants.SHG_CODE,occupation.getCode());
                    values.put(TableConstants.SHG_NAME,occupation.getSHGName());
                    values.put(TableConstants.SHG_CREATION_DATE,occupation.getShgCreatedDate());
                    values.put(TableConstants.SHG_VERIFIED_DATE,occupation.getShgVerifiedDate());
                    values.put(TableConstants.SHG_GROUPMOBILE_NUMBER,occupation.getMobileNumber());
                    values.put(TableConstants.SHG_GROUPTOTAL_MEMBER,occupation.getTotalMembers());
                    values.put(TableConstants.TREASURER_NAME,occupation.getTreasuereName());
                    values.put(TableConstants.SECRETARY_NAME,occupation.getSecretaryName());
                    values.put(TableConstants.VILLAGENAME,occupation.getVillageName());
                    values.put(TableConstants.PRESIDENT_NAME,occupation.getPresidentName());
                    values.put(TableConstants.BLOCK_NAME,occupation.getBlockName());
                    values.put(TableConstants.PANCHAYATNAME,occupation.getGramPachayatName());
                    values.put(TableConstants.USERID, MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID, ""));

                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_SHG_PROFILE, TableConstants.ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_SHG_PROFILE Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }


    @SuppressLint("LongLogTag")
    public boolean insertTrainingData(ArrayList<TableData> data, String syncType) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.TRAINING_NAME,occupation.getName());
                    values.put(TableConstants.TRAINING_UUID,occupation.getUuid());
                    values.put(TableConstants.TRAINING_LANGUAGE_UUID,occupation.getLanguage_uuid());
                    values.put(TableConstants.USERID, MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID, ""));

                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_SHG_TRAINING, TableConstants.ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_SHG_TRAINING Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }


    @SuppressLint("LongLogTag")
    public boolean insertPOSData(ArrayList<TableData> data, String syncType) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.ID, occupation.getId());
                    values.put(TableConstants.NAME, occupation.getName());
                    values.put(TableConstants.USERID, MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID, ""));

                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_INTERNAL_LOAN_PURPOSE_LIST, TableConstants.ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_INTERNAL_LOAN_PURPOSE_LIST Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public boolean insertSHGTransData(ArrayList<TableData> data, String syncType) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.SHG_ID, occupation.getShgId());
                    values.put(TableConstants.CASH_AT_BANK, occupation.getCashAtBank());
                    values.put(TableConstants.CASH_IN_HAND, occupation.getCashInHand());
                    values.put(TableConstants.LASTTRANSACTIONDATE, occupation.getLastTransactionDate());
                    values.put(TableConstants.USERID, MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID, ""));

                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_SHG_TRANSACTION_DETAILS, TableConstants.SHG_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_SHG_TRANSACTION_DETAILS Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public boolean insertSHGbankData(ArrayList<TableData> data, String syncType) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.SHG_ID, occupation.getShgId());
                    values.put(TableConstants.USERID, MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID, ""));
                    values.put(TableConstants.SHG_SB_AC_ID, occupation.getShgSavingsAccountId());
                    values.put(TableConstants.OFF_CURRENT_BALANCE, occupation.getCurrentBalance());
                    values.put(TableConstants.OFF_FD_VALUE, occupation.getCurrentFixedDeposit());
                    values.put(TableConstants.OFF_RD_VALUE, occupation.getRecurringDepositedBalance());

                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST, TableConstants.SHG_SB_AC_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public boolean insertILoanData(ArrayList<TableData> data, String syncType) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.SHG_ID, occupation.getShgId());
                    values.put(TableConstants.USERID, MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID, ""));
                    values.put(TableConstants.MEMBER_ID, occupation.getMemberId());
                    values.put(TableConstants.OFF_LOAN_OS, occupation.getLoanOutstanding());

                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST, TableConstants.MEMBER_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public boolean insertGrpLoanData(ArrayList<TableData> data, String syncType) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.SHG_ID, occupation.getShgId());
                    values.put(TableConstants.USERID, MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID, ""));
                    values.put(TableConstants.LOAN_ID, occupation.getLoanId());
                    values.put(TableConstants.BANK_ID, occupation.getBankId());
                    values.put(TableConstants.LOANTYPENAME, occupation.getLoanType());
                    values.put(TableConstants.OFF_LOAN_OS, occupation.getLoanOutstanding());
                    values.put(TableConstants.LOAN_ACC_ID, occupation.getLoanAccountId());
                    values.put(TableConstants.LOAN_ACC_NO, occupation.getLoanAccountNo());
                    values.put(TableConstants.BRANCH_ID, occupation.getBranchId());
                    values.put(TableConstants.BANKNAME, occupation.getBankName());
                    values.put(TableConstants.OFF_DISBURSEDATETIME, occupation.getDisbursementDate());

                    Log.i("print","Loan Type : "+occupation.getLoanType()+"\t\t"+"Bank Name : "+occupation.getBankName());
                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST, TableConstants.LOAN_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

  /*  @SuppressLint("LongLogTag")
    public boolean insertMemLoanData(ArrayList<TableData> data, String syncType) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {

                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.SHG_ID, occupation.getShgId());
                    values.put(TableConstants.USERID, MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID, ""));
                    values.put(TableConstants.MEMBER_ID, occupation.getMemberId());
                    values.put(TableConstants.LOAN_ID, occupation.getLoanId());
                    values.put(TableConstants.OFF_LOAN_OS, occupation.getMemberLoanOutstanding());

                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST, null, values, SQLiteDatabase.CONFLICT_NONE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }
*/



    @SuppressLint("LongLogTag")
    public boolean insertMemLoanData(ArrayList<TableData> data, String syncType) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {

                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.SHG_ID, occupation.getShgId());
                    values.put(TableConstants.USERID, MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID, ""));
                    values.put(TableConstants.MEMBER_ID, occupation.getMemberId());
                    values.put(TableConstants.LOAN_ID, occupation.getLoanId());
                    values.put(TableConstants.OFF_LOAN_OS, occupation.getMemberLoanOutstanding());

                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST, TableConstants.LOAN_ID + " = " + occupation.getLoanId() + " AND " + TableConstants.MEMBER_ID + " = " + occupation.getMemberId(), values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }


    public void deleteLoantable() {
        openDatabase();
        try {
            database.execSQL("delete from " + TableName.OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST);
            database.execSQL("delete from " + TableName.OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST);
            database.execSQL("delete from " + TableName.OFFLINE_ANIMATOR_SSN_LIST);
            database.execSQL("delete from " + TableName.OFFLINE_ANIMATOR_SHG_RD_LIST);
            database.execSQL("delete from " + TableName.AUDIT_SAVINGS);
            database.execSQL("delete from " + TableName.AUDIT_VOLUNTARYSAVINGS);
            database.execSQL("delete from " + TableName.AUDIT_SAVINGDISBURSEMENT);
            database.execSQL("delete from " + TableName.AUDIT_INTERNALLOANDISBURSEMENT);
            database.execSQL("delete from " + TableName.AUDIT_SEEDFUND);
            database.execSQL("delete from " + TableName.AUDIT_FDBANKTRANSACTION);
            database.execSQL("delete from " + TableName.AUDIT_INCOMEDISBURSEMENT);
            database.execSQL("delete from " + TableName.AUDIT_OTHERINCOME);
            database.execSQL("delete from " + TableName.AUDIT_GROUPLOANREPAYMENT);
            database.execSQL("delete from " + TableName.AUDIT_PENALTY);
            database.execSQL("delete from " + TableName.AUDIT_BANKTRANSACTION);
            database.execSQL("delete from " + TableName.AUDIT_SUBSCRIPTIONFEDS);
            database.execSQL("delete from " + TableName.AUDIT_OTHERINCOMEGROUP);
            database.execSQL("delete from " + TableName.AUDIT_MEMBERLOANREAPYMENT);
            database.execSQL("delete from " + TableName.AUDIT_VOLUNTARYSAVINGSDISBURSEMENT);
            database.execSQL("delete from " + TableName.AUDIT_OTHEREXPENSE);
            database.execSQL("delete from " + TableName.AUDIT_MEETINGEXPENSE);
            database.execSQL("delete from " + TableName.AUDIT_INTERLOANREPAYMENT);
            database.execSQL("delete from " + TableName.AUDIT_DONATION);
            database.execSQL("delete from " + TableName.AUDIT_SUBSCRIPTION);
            database.execSQL("delete from " + TableName.AUDIT_SUBSCRIPTIONCHARGES);
        } catch (Exception e) {
            Log.e("Delete table Exception", e.toString());
        } finally {
            closeDatabase();
        }
    }


    @SuppressLint("LongLogTag")
    public boolean insertSHGBankListData(ArrayList<TableData> data, String syncType) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (TableData occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.SHG_ID, occupation.getShgId());
                    values.put(TableConstants.USERID, MySharedPreference.readString(contextValue, MySharedPreference.ANIMATOR_ID, ""));
                    values.put(TableConstants.SHG_SB_AC_ID, occupation.getShgSavingsAccountId());
                    values.put(TableConstants.BANKNAME, occupation.getBankName());
                    values.put(TableConstants.BRANCHNAME, occupation.getBranchName());
                    values.put(TableConstants.IFSC, occupation.getIfsc());
                    values.put(TableConstants.ACCOUNT_NO, occupation.getAccountNo());
                    values.put(TableConstants.BANK_ID, occupation.getBankId());
                    values.put(TableConstants.BRANCH_ID, occupation.getBranchId());
                    values.put(TableConstants.PRIMARY_FlAG, occupation.getIsPrimary());
                    long flag = database.insertWithOnConflict(TableName.OFFLINE_ANIMATOR_SHG_BANK_LIST, TableConstants.SHG_SB_AC_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("OFFLINE_ANIMATOR_SHG_BANK_LIST Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    public ArrayList<IncomeType> getIncExpData(String type) {
        ArrayList<IncomeType> shgList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_SHG_INCOME_EXPENSE_TYPE + " where " + TableConstants.TYPE + " LIKE '" + type + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    IncomeType shg = new IncomeType();
                    shg.setId(cursor.getString(cursor.getColumnIndex(TableConstants.ID)));
                    shg.setIncomeType(cursor.getString(cursor.getColumnIndex(TableConstants.TYPE)));
                    shg.setName(cursor.getString(cursor.getColumnIndex(TableConstants.NAME)));
                    shgList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return shgList;
    }

    public ArrayList<MinutesOfMeeting> getMoMData(int offline) {
        ArrayList<MinutesOfMeeting> mom = new ArrayList<>();

        try {
            openDatabase();
            String selectQuery = null;
            if (offline == 1) {
                selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_SHG_MINUTES_OF_MEETING + " where " + TableConstants.ACTIVEFLAG + " = " + offline;
            } else {
                selectQuery = "SELECT  * FROM " + TableName.OFFLINE_ANIMATOR_SHG_MINUTES_OF_MEETING;
            }
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    MinutesOfMeeting shg = new MinutesOfMeeting();
                    shg.setId(cursor.getString(cursor.getColumnIndex(TableConstants.ID)));
                    //   shg.set(cursor.getString(cursor.getColumnIndex(TableConstants.TYPE)));
                    shg.setName(cursor.getString(cursor.getColumnIndex(TableConstants.NAME)));
                    mom.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return mom;
    }


    @SuppressLint("LongLogTag")
    public static boolean insertSavingAuditData(ArrayList<Savings> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (Savings occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_SAVINGS, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_SAVINGS Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }


    @SuppressLint("LongLogTag")
    public static boolean insertVoluntarySavingAuditData(ArrayList<VoluntarySavings> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (VoluntarySavings occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_VOLUNTARYSAVINGS, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_VOLUNTARYSAVINGS Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public static boolean insertSavingDisbursementAuditData(ArrayList<SavingsDisbursement> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (SavingsDisbursement occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_SAVINGDISBURSEMENT, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_VOLUNTARYSAVINGS Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public static boolean insertInternalLoanDisbursementAuditData(ArrayList<InternalLoanDisbursement> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (InternalLoanDisbursement occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_INTERNALLOANDISBURSEMENT, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_INTERNALLOANDISBURSEMENT Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public static boolean insertseedFundArrayListAuditData(ArrayList<SeedFund> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (SeedFund occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_SEEDFUND, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_SEEDFUND Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }


    @SuppressLint("LongLogTag")
    public static boolean insertfdBankTransactionArrayListAuditData(ArrayList<FDBankTransaction> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (FDBankTransaction occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_FDBANKTRANSACTION, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_FDBANKTRANSACTION Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public static boolean insertIncomeDisbursementArrayListAuditData(ArrayList<IncomeDisbursement> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (IncomeDisbursement occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_INCOMEDISBURSEMENT, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_INCOMEDISBURSEMENT Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public static boolean insertOtherIncomeArrayListAuditData(ArrayList<OtherIncome> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (OtherIncome occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_OTHERINCOME, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_OTHERINCOME Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }


    @SuppressLint("LongLogTag")
    public static boolean insertGroupLoanRepaymentArrayListAuditData(ArrayList<GroupLoanRepayment> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (GroupLoanRepayment occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_GROUPLOANREPAYMENT, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_GROUPLOANREPAYMENT Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public static boolean insertPenaltyArrayListAuditData(ArrayList<Penalty> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (Penalty occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_PENALTY, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_PENALTY Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public static boolean insertBankTransactionArrayListAuditData(ArrayList<BankTransaction> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (BankTransaction occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_BANKTRANSACTION, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_BANKTRANSACTION Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public static boolean insertSubscriptionFedsArrayListAuditData(ArrayList<SubscriptiontoFeds> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (SubscriptiontoFeds occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_SUBSCRIPTIONFEDS, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_SUBSCRIPTIONFEDS Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public static boolean insertOtherincomeGroupArrayListAuditData(ArrayList<OtherIncomeGroup> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (OtherIncomeGroup occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_OTHERINCOMEGROUP, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_OTHERINCOMEGROUP Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public static boolean insertmemberLoanRepaymentAuditData(ArrayList<memberLoanRepayment> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (memberLoanRepayment occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_MEMBERLOANREAPYMENT, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_MEMBERLOANREAPYMENT Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public static boolean insertvoluntarysavingsdisbursementAuditData(ArrayList<VoluntarySavingsDisbursement> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (VoluntarySavingsDisbursement occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_VOLUNTARYSAVINGSDISBURSEMENT, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_VOLUNTARYSAVINGSDISBURSEMENT Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public static boolean insertOtherExpenseAuditData(ArrayList<OtherExpense> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (OtherExpense occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_OTHEREXPENSE, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_OTHEREXPENSE Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public static boolean insertMeetingExpenseAuditData(ArrayList<MeetingExpense> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (MeetingExpense occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_MEETINGEXPENSE, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_MEETINGEXPENSE Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public static boolean insertinternalLoanRepaymentAuditData(ArrayList<internalLoanRepayment> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (internalLoanRepayment occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_INTERLOANREPAYMENT, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_INTERLOANREPAYMENT Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public static boolean insertDonationAuditData(ArrayList<Donation> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (Donation occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_DONATION, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_DONATION Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public static boolean insertSubscriptionAuditData(ArrayList<Subscription> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (Subscription occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_SUBSCRIPTION, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_SUBSCRIPTION Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }

    @SuppressLint("LongLogTag")
    public static boolean insertSubscriptionChargesAuditData(ArrayList<SubscriptionCharges> data) {
        boolean isSuccessFullyInserted = false;
        if (!data.isEmpty()) {
            openDatabase();
            try {
                for (SubscriptionCharges occupation : data) {
                    ContentValues values = new ContentValues();
                    values.put(TableConstants.AUDIT_ID,occupation.getAuditor_uuid());
                    values.put(TableConstants.SHG_USERID,occupation.getShg_uuid());
                    values.put(TableConstants.AUDIT_MEMBERNAME,occupation.getMember());
                    values.put(TableConstants.AUDIT_MEBERID,occupation.getMember_uuid());
                    values.put(TableConstants.AUDIT_SETTINGS,occupation.getSettings());
                    values.put(TableConstants.AUDIT_TRANSDATE,occupation.getTransaction_date());
                    values.put(TableConstants.OLD_TRANS_AMOUNT,occupation.getOld_transaction_amount());
                    values.put(TableConstants.NEW_TRANS_AMOUNT,occupation.getNew_transaction_amount());
                    long flag = database.insertWithOnConflict(TableName.AUDIT_SUBSCRIPTIONCHARGES, TableConstants.AUDIT_MEBERID, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (flag != -1) {
                        isSuccessFullyInserted = true;
                    } else {
                        isSuccessFullyInserted = false;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e("AUDIT_SUBSCRIPTIONCHARGES Exception", e.toString());
                isSuccessFullyInserted = false;
            } finally {
                closeDatabase();
            }
        }
        return isSuccessFullyInserted;
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}

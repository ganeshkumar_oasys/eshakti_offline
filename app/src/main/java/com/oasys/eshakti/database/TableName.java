package com.oasys.eshakti.database;


public class TableName {

    public static final String TABLE_LOGIN = "LoginValues";
    public static final String TABLE_GROUP_NAME_DETAILS = "A_SHG_Group";
    public static final String TABLE_BANK_DETAILS = "GroupBankDetails";
    public static final String TABLE_MEMBER_DETAILS = "MemberDetails";
    public static final String TABLE_LOAN_DETAILS = "LoanDetails";
    public static final String TABLE_EXPENSE_DETAILS = "ExpenseDetails";

    public static final String TABLE_LOAN_PURPOSE = "LD_POL";
    public static final String TABLE_LOAN_INSTALLMENT = "LD_Installments";
    public static final String TABLE_LOAN_LTYPES = "LD_Loan_Types";
    public static final String TABLE_LOAN_BANK_DETAILS = "LD_Bank_List";
    public static final String TABLE_LOAN_IL_TYPE_SETTING = "LD_IL_LTYPE_SETTING";
    public static final String TABLE_LOAN_FED_SETTING = "LD_IL_FED_SETTING";
    public static final String TABLE_LOAN_MFI_SETTING = "LD_IL_MFI_SETTING";

    public static final String TABLE_MEMBER_BANKFULLDETAILS = "BANKFULLDETAILS";
    public static final String TABLE_JANDHAN_BANKNAMEDETAILS = "BANKNAMEDETAILS";

    //OFFLINE TABLE
    public static final String TABLE_TRANSACTION = "TABLE_TRANSACTION";
    public static final String TABLE_INCOME = "INCOME";
    public static final String TABLE_EXPENSE = "EXPENSE";
    public static final String TABLE_MEM_LOAN_REPAYMENT = "MEMBER_LOAN_REPAYMENT";
    public static final String TABLE_GRP_LOAN_REPAYMENT = "GROUP_LOAN_REPAYMENT";
    public static final String TABLE_LOANDISBURSEMENT = "LOAN_DISBURESEMENT";
    public static final String TABLE_BANKTRANSACTION = "BANK_TRANSACTION";


    //OFFLINE TABLE:
    public static final String OFFLINE_ANIMATOR_SHG_INCOME_EXPENSE_TYPE = "OFFLINE_ANIMATOR_SHG_INCOME_EXPENSE_TYPE";
    public static final String OFFLINE_ANIMATOR_SHG_LIST = "OFFLINE_ANIMATOR_SHG_LIST";
    public static final String OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST = "OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST";
    public static final String OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST = "OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST";
    public static final String OFFLINE_ANIMATOR_SHG_TRANSACTION_DETAILS = "OFFLINE_ANIMATOR_SHG_TRANSACTION_DETAILS";
    public static final String OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST = "OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST";
    public static final String OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST = "OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST";
    public static final String OFFLINE_ANIMATOR_MEMBER_LIST = "OFFLINE_ANIMATOR_MEMBER_LIST";
    public static final String OFFLINE_ANIMATOR_SHG_BANK_LIST = "OFFLINE_ANIMATOR_SHG_BANK_LIST";
    public static final String OFFLINE_ANIMATOR_INTERNAL_LOAN_PURPOSE_LIST = "OFFLINE_ANIMATOR_INTERNAL_LOAN_PURPOSE_LIST";
    public static final String OFFLINE_ANIMATOR_SHG_MINUTES_OF_MEETING = "OFFLINE_ANIMATOR_SHG_MINUTES_OF_MEETING";
    public static final String OFFLINE_ANIMATOR_PROFILE = "OFFLINE_ANIMATOR_PROFILE";
    public static final String OFFLINE_ANIMATOR_SHG_PROFILE = "OFFLINE_ANIMATOR_SHG_PROFILE";
    public static final String OFFLINE_ANIMATOR_SHG_TRAINING = "OFFLINE_ANIMATOR_SHG_TRAINING";
    public static final String OFFLINE_ANIMATOR_SSN_LIST = "OFFLINE_ANIMATOR_SSN_LIST";
    public static final String OFFLINE_ANIMATOR_SHG_RD_LIST = "OFFLINE_ANIMATOR_SHG_RD_LIST";
    public static final String OFFLINE_ANIMATOR_SSN_MASTER = "OFFLINE_ANIMATOR_SSN_MASTER";
    public static final String AUDIT_SAVINGS = "AUDIT_SAVINGS";
    public static final String AUDIT_VOLUNTARYSAVINGS = "AUDIT_VOLUNTARYSAVINGS";
    public static final String AUDIT_SAVINGDISBURSEMENT = "AUDIT_SAVINGDISBURSEMENT";
    public static final String AUDIT_INTERNALLOANDISBURSEMENT = "AUDIT_INTERNALLOANDISBURSEMENT";
    public static final String AUDIT_SEEDFUND = "AUDIT_SEEDFUND";
    public static final String AUDIT_FDBANKTRANSACTION = "AUDIT_FDBANKTRANSACTION";
    public static final String AUDIT_INCOMEDISBURSEMENT = "AUDIT_INCOMEDISBURSEMENT";
    public static final String AUDIT_OTHERINCOME = "AUDIT_OTHERINCOME";
    public static final String AUDIT_GROUPLOANREPAYMENT = "AUDIT_GROUPLOANREPAYMENT";
    public static final String AUDIT_PENALTY = "AUDIT_PENALTY";
    public static final String AUDIT_BANKTRANSACTION = "AUDIT_BANKTRANSACTION";
    public static final String AUDIT_SUBSCRIPTIONFEDS = "AUDIT_SUBSCRIPTIONFEDS";
    public static final String AUDIT_OTHERINCOMEGROUP = "AUDIT_OTHERINCOMEGROUP";
    public static final String AUDIT_MEMBERLOANREAPYMENT = "AUDIT_MEMBERLOANREAPYMENT";
    public static final String AUDIT_VOLUNTARYSAVINGSDISBURSEMENT = "AUDIT_VOLUNTARYSAVINGSDISBURSEMENT";
    public static final String AUDIT_OTHEREXPENSE = "AUDIT_OTHEREXPENSE";
    public static final String AUDIT_MEETINGEXPENSE = "AUDIT_MEETINGEXPENSE";
    public static final String AUDIT_INTERLOANREPAYMENT = "AUDIT_INTERLOANREPAYMENT";
    public static final String AUDIT_DONATION = "AUDIT_DONATION";
    public static final String AUDIT_SUBSCRIPTION = "AUDIT_SUBSCRIPTION";
    public static final String AUDIT_SUBSCRIPTIONCHARGES = "AUDIT_SUBSCRIPTIONCHARGES";


    //OFFLINE CREATE TABLE:
    public static final String C_OFFLINE_ANIMATOR_SHG_INCOME_EXPENSE_TYPE = "create table " + OFFLINE_ANIMATOR_SHG_INCOME_EXPENSE_TYPE + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.ID + " VARCHAR(255) UNIQUE,"  + TableConstants.USERID + " VARCHAR(255)," + TableConstants.NAME + " VARCHAR(255)," + TableConstants.TYPE + " VARCHAR(255)"
            + ")";

    public static final String C_OFFLINE_ANIMATOR_SSN_MASTER = "create table " + OFFLINE_ANIMATOR_SSN_MASTER + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.ID + " VARCHAR(255) UNIQUE,"  + TableConstants.USERID + " VARCHAR(255)," + TableConstants.NAME + " VARCHAR(255)" +
             ")";

    public static final String C_OFFLINE_ANIMATOR_PROFILE = "create table " + OFFLINE_ANIMATOR_PROFILE + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.ID + " VARCHAR(255) UNIQUE," + TableConstants.USERID + " VARCHAR(255)," + TableConstants.PHONE_NUMBER + " VARCHAR(255)," + TableConstants.DATE_OF_ASSIGNING + " VARCHAR(255),"
            + TableConstants.TOTAL_GROUP + " VARCHAR(255)," + TableConstants.AGE + " VARCHAR(255)," + TableConstants.ANIMATOR_NAME + " VARCHAR(255)" + ")";


    public static final String C_OFFLINE_ANIMATOR_SHG_PROFILE = "create table " + OFFLINE_ANIMATOR_SHG_PROFILE + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.SHG_ID + " VARCHAR(255) UNIQUE," + TableConstants.USERID + " VARCHAR(255)," + TableConstants.SHG_TYPE + " VARCHAR(255)," + TableConstants.SHG_CODE + " VARCHAR(255),"
            + TableConstants.SHG_NAME + " VARCHAR(255)," + TableConstants.SHG_CREATION_DATE + " VARCHAR(255)," + TableConstants.SHG_VERIFIED_DATE + " VARCHAR(255),"
            + TableConstants.SHG_GROUPMOBILE_NUMBER + " VARCHAR(255)," + TableConstants.PRESIDENT_NAME + " VARCHAR(255)," + TableConstants.SHG_GROUPTOTAL_MEMBER + " VARCHAR(255)," + TableConstants.SECRETARY_NAME + " VARCHAR(255)," + TableConstants.TREASURER_NAME + " VARCHAR(255)," + TableConstants.BLOCK_NAME + " VARCHAR(255)," + TableConstants.PANCHAYATNAME + " VARCHAR(255)," + TableConstants.VILLAGENAME + " VARCHAR(255)" +")";

    public static final String C_OFFLINE_ANIMATOR_SHG_TRAINING = "create table " + OFFLINE_ANIMATOR_SHG_TRAINING + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.USERID + " VARCHAR(255)," + TableConstants.TRAINING_NAME + " VARCHAR(255)," + TableConstants.TRAINING_UUID + " VARCHAR(255)," + TableConstants.TRAINING_LANGUAGE_UUID + " VARCHAR(255)" +")";


    public static final String C_OFFLINE_ANIMATOR_INTERNAL_LOAN_PURPOSE_LIST= "create table " + OFFLINE_ANIMATOR_INTERNAL_LOAN_PURPOSE_LIST + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.ID + " VARCHAR(255) UNIQUE,"  + TableConstants.USERID + " VARCHAR(255)," + TableConstants.NAME + " VARCHAR(255)"
            + ")";

    public static final String C_OFFLINE_ANIMATOR_SHG_LIST = "create table " + OFFLINE_ANIMATOR_SHG_LIST + "("
            + TableConstants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.SHG_ID + " VARCHAR(255) UNIQUE,"  + TableConstants.USERID + " VARCHAR(255)," + TableConstants.NAME + " VARCHAR(255) ," + TableConstants.IS_TRANS_AUDIT + " VARCHAR(255) ,"
            + TableConstants.GRP_FORMATION_DATE + " VARCHAR(255)," + TableConstants.SHG_CODE + " VARCHAR(255),"+ TableConstants.MODIFIEDDATE + " VARCHAR(255)," + TableConstants.FIRST_SFLAG + " VARCHAR(255),"  + TableConstants.CASH_IN_HAND + " VARCHAR(255)," + TableConstants.CASH_AT_BANK + " VARCHAR(255)," + TableConstants.LASTTRANSACTIONDATE + " VARCHAR(255)," + TableConstants.IS_TRANSACTION_TDY + " VARCHAR(255)," + TableConstants.DISTRICT_ID + " VARCHAR(255)," + TableConstants.VILLAGENAME + " VARCHAR(255)," + TableConstants.PANCHAYATNAME + " VARCHAR(255)," + TableConstants.BLOCKNAME + " VARCHAR(255)," + TableConstants.ACTIVEFLAG + " VARCHAR(255)," + TableConstants.OPENINGDATE + " VARCHAR(255)," + TableConstants.PRESIDENT_NAME + " VARCHAR(255)"
            + ")";

    public static final String C_OFFLINE_ANIMATOR_SHG_RD_LIST = "create table " + OFFLINE_ANIMATOR_SHG_RD_LIST + "("
            + TableConstants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.SHG_ID + " VARCHAR(255) ,"  + TableConstants.USERID + " VARCHAR(255)," + TableConstants.SHGSAVINGSACCOUNTID + " VARCHAR(255) ,"
            + TableConstants.RD_BANKNAME + " VARCHAR(255)," + TableConstants.RD_BRANCHNAME + " VARCHAR(255),"+ TableConstants.RD_ACCOUNTNO + " VARCHAR(255)," + TableConstants.RD_BANKID + " VARCHAR(255),"  + TableConstants.RD_IFSC + " VARCHAR(255)," + TableConstants.RD_BRANCH_ID + " VARCHAR(255)," + TableConstants.RD_ISPRIMARY + " VARCHAR(255)," + TableConstants.RD_INSTALLAMOUNT + " VARCHAR(255)," + TableConstants.RD_RATE_OF_INTEREST + " VARCHAR(255)," + TableConstants.RD_TENURE + " VARCHAR(255)," + TableConstants.RD_CURRENTBALANCE + " VARCHAR(255)," + TableConstants.RD_OPENINGBALANCE + " VARCHAR(255)," + TableConstants.RD_STATUS + " VARCHAR(255)"
            + ")";

    public static final String C_OFFLINE_ANIMATOR_SSN_LIST = "create table " + OFFLINE_ANIMATOR_SSN_LIST + "("
            + TableConstants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.SHG_ID + " VARCHAR(255) ," + TableConstants.SSS_TYPE_ID + " VARCHAR(255)," + TableConstants.MEMBER_ID + " VARCHAR(255)," + TableConstants.SSN_MEMBERNAME + " VARCHAR(255),"
            + TableConstants.BANK_NAME_ID + " VARCHAR(255)," + TableConstants.BRANCH_NAME_ID + " VARCHAR(255),"+ TableConstants.SSS_NUMBER + " VARCHAR(255)," + TableConstants.IS_SSS + " VARCHAR(255),"  + TableConstants.FIRST_UPDATE + " VARCHAR(255)," + TableConstants.SSN_BANKNAME + " VARCHAR(255)," + TableConstants.SSN_BRANCHNAME + " VARCHAR(255),"
            + TableConstants.LAST_UPDATE + " VARCHAR(255)," + TableConstants.SSS_UPDATE + " VARCHAR(255)," + TableConstants.IS_BEFORE_2019 + " VARCHAR(255)," + TableConstants.CREATED_DATE + " VARCHAR(255)," + TableConstants.MODIFIED_DATE + " VARCHAR(255)"
            + ")";

    public static final String C_OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST = "create table " + OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.USERID + " VARCHAR(255)," + TableConstants.SHG_ID + " VARCHAR(255) ,"+ TableConstants.LOAN_ID + " VARCHAR(255),"
            + TableConstants.MEMBER_ID + " VARCHAR(255)," + TableConstants.OFF_LOAN_SFLAG + " VARCHAR(255),"   + TableConstants.OFF_LOAN_OS + " VARCHAR(255)"
            + ")";
    public static final String C_OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST = "create table " + OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.USERID + " VARCHAR(255)," + TableConstants.SHG_ID + " VARCHAR(255)," + TableConstants.OFF_LOAN_SFLAG + " VARCHAR(255),"   + TableConstants.BANK_ID + " VARCHAR(255)," + TableConstants.LOAN_ACC_NO + " VARCHAR(255),"+ TableConstants.LOAN_ACC_ID + " VARCHAR(255)," + TableConstants.BRANCH_ID + " VARCHAR(255)," + TableConstants.BANKNAME + " VARCHAR(255),"
            + TableConstants.LOAN_ID + " VARCHAR(255) UNIQUE," + TableConstants.OFF_LOAN_OS + " VARCHAR(255),"+ TableConstants.OFF_DISBURSEDATETIME + " VARCHAR(255)," + TableConstants.LOANTYPENAME + " VARCHAR(255)"
            + ")";
    public static final String C_OFFLINE_ANIMATOR_SHG_TRANSACTION_DETAILS = "create table " + OFFLINE_ANIMATOR_SHG_TRANSACTION_DETAILS + "("
            + TableConstants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.SHG_ID + " VARCHAR(255) UNIQUE,"  + TableConstants.USERID + " VARCHAR(255)," + TableConstants.LASTTRANSACTIONDATE + " VARCHAR(255)," + TableConstants.CASH_AT_BANK + " VARCHAR(255)," + TableConstants.CASH_IN_HAND + " VARCHAR(255)"
            + ")";
    public static final String C_OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST = "create table " + OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST + "("
            + TableConstants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.SHG_ID + " VARCHAR(255)," + TableConstants.USERID + " VARCHAR(255),"  + TableConstants.SHG_SB_AC_ID + " VARCHAR(255) UNIQUE,"
            + TableConstants.OFF_CURRENT_BALANCE + " VARCHAR(255)," + TableConstants.OFF_FD_VALUE + " VARCHAR(255)," + TableConstants.OFF_RD_VALUE + " VARCHAR(255)"
            + ")";
    public static final String C_OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST = "create table " + OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST + "("
            + TableConstants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.SHG_ID + " VARCHAR(255),"  + TableConstants.USERID + " VARCHAR(255)," + TableConstants.MEMBER_ID + " VARCHAR(255) UNIQUE," + TableConstants.OFF_LOAN_OS + " VARCHAR(255)"
            + ")";

    public static final String C_OFFLINE_ANIMATOR_MEMBER_LIST = "create table " + OFFLINE_ANIMATOR_MEMBER_LIST + "("
            + TableConstants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.MEMBER_ID + " VARCHAR(255) UNIQUE," + TableConstants.MEMBER_NAME + " VARCHAR(255) ,"
            + TableConstants.SHG_ID + " VARCHAR(255),"  + TableConstants.USERID + " VARCHAR(255)," + TableConstants.MEMBER_USER_ID + " VARCHAR(255),"  + TableConstants.BRANCH_ID + " VARCHAR(255)," + TableConstants.BRANCHNAME + " VARCHAR(255)," + TableConstants.BANK_ID + " VARCHAR(255)," + TableConstants.BANKNAME_ID + " VARCHAR(255)," + TableConstants.AADHAR_NUM + " VARCHAR(255)," + TableConstants.PIP_NUM + " VARCHAR(255)," + TableConstants.BANKNAME + " VARCHAR(255)," + TableConstants.ACCOUNT_NO + " VARCHAR(255)," + TableConstants.PHONE_NO + " VARCHAR(255)"
            + ")";

    public static final String C_OFFLINE_ANIMATOR_SHG_BANK_LIST = "create table " + OFFLINE_ANIMATOR_SHG_BANK_LIST + "("
            + TableConstants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.USERID + " VARCHAR(255)," + TableConstants.SHG_ID + " VARCHAR(255)," + TableConstants.IFSC + " VARCHAR(255),"
            + TableConstants.SHG_SB_AC_ID + " VARCHAR(255) UNIQUE,"   + TableConstants.BANK_ID + " VARCHAR(255)," + TableConstants.BRANCH_ID + " VARCHAR(255),"+ TableConstants.BANKNAME + " VARCHAR(255)," + TableConstants.BRANCHNAME + " VARCHAR(255),"+TableConstants.ACCOUNT_NO + " VARCHAR(255)," + TableConstants.PRIMARY_FlAG + " VARCHAR(255)"
            + ")";

    public static final String C_OFFLINE_ANIMATOR_SHG_MINUTES_OF_MEETING = "create table " + OFFLINE_ANIMATOR_SHG_MINUTES_OF_MEETING + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.ID + " VARCHAR(255) UNIQUE," + TableConstants.USERID + " VARCHAR(255),"  + TableConstants.NAME + " VARCHAR(255),"  + TableConstants.ACTIVEFLAG + " INTEGER" + ")";



    public static final String CREATE_LOGIN_TABLE = "create table " + TABLE_LOGIN + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.USERID + " VARCHAR(255) UNIQUE," + TableConstants.ANIMATOR_ID + " VARCHAR(255) UNIQUE,"
            + TableConstants.USERNAME + " VARCHAR(255)," + TableConstants.PASSWORD + " VARCHAR(255)," + TableConstants.LANGUAGE + " VARCHAR(255)"
            + ")";

    public static final String CREATE_SHG_GROUP = "create table " + TABLE_GROUP_NAME_DETAILS + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.USERID + " VARCHAR(255)," + TableConstants.SHG_ID + " VARCHAR(255) UNIQUE," + TableConstants.GRP_FORMATION_DATE + " VARCHAR(255)," + TableConstants.CASH_IN_HAND + " VARCHAR(255)," + TableConstants.CASH_AT_BANK + " VARCHAR(255)," + TableConstants.SHG_CODE + " VARCHAR(255)," + TableConstants.NAME + " VARCHAR(255)," + TableConstants.PRESIDENT_NAME + " VARCHAR(255)," + TableConstants.DISTRICT_ID + " VARCHAR(255)," + TableConstants.FIRST_SFLAG + " VARCHAR(255)," + TableConstants.ACTIVEFLAG + " VARCHAR(255)," + TableConstants.BLOCKNAME + " VARCHAR(255)," + TableConstants.VILLAGENAME + " VARCHAR(255)," + TableConstants.PANCHAYATNAME + " VARCHAR(255)," + TableConstants.OPENINGDATE + " VARCHAR(255)," + TableConstants.MODIFIEDDATE + " VARCHAR(255)," + TableConstants.LASTTRANSACTIONDATE + " VARCHAR(255)" + ")";


    public static final String CREATE_BANK_DETAILS = "create table " + TABLE_BANK_DETAILS + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.SHG_ID + " VARCHAR(255)," + TableConstants.SHG_SB_AC_ID + " VARCHAR(255)," + TableConstants.BANK_ID + " VARCHAR(255)," + TableConstants.BANKNAME + " VARCHAR(255)," + TableConstants.BANKNAME_ID + " VARCHAR(255)," + TableConstants.BRANCHNAME + " VARCHAR(255)," + TableConstants.CURR_FD_BALANCE + " VARCHAR(255)," + TableConstants.CURR_BALANCE + " VARCHAR(255)," + TableConstants.ACCOUNT_NO + " VARCHAR(255) UNIQUE," + TableConstants.BRANCHNAME_ID + " VARCHAR(255) " + ")";

    public static final String CREATE_MEMBER_DETAILS = "create table " + TABLE_MEMBER_DETAILS + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.BANK_ID + " VARCHAR(255)," + TableConstants.ACCOUNT_NO + " VARCHAR(255)," + TableConstants.BRANCHNAME + " VARCHAR(255)," + TableConstants.BANKNAME_ID + " VARCHAR(255)," + TableConstants.BRANCHNAME_ID + " VARCHAR(255)," + TableConstants.BANKNAME + " VARCHAR(255)," + TableConstants.SHG_ID + " VARCHAR(255)," + TableConstants.MEMBER_ID + " VARCHAR(255) UNIQUE," + TableConstants.PHONE_NO + " VARCHAR(255)," + TableConstants.MEMBER_USER_ID + " VARCHAR(255)," + TableConstants.MEMBER_NAME + " VARCHAR(255)" + ")";


    public static final String CREATE_LOAN_DETAILS = "create table " + TABLE_LOAN_DETAILS + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.LOAN_ID + " VARCHAR(255) UNIQUE," + TableConstants.SHG_ID + " VARCHAR(255)," + TableConstants.LOAN_NAME + " VARCHAR(255)," + TableConstants.LOAN_ACC_ID + " VARCHAR(255)," + TableConstants.LOAN_TYPE_ID + " VARCHAR(255)," + TableConstants.BANKNAME + " VARCHAR(255)," + TableConstants.LOAN_ACC_NO + " VARCHAR(255)," + TableConstants.LOAN_AMOUNT + " VARCHAR(255)" + ")";

    public static final String CREATE_EXPENSE_DETAILS = "create table " + TABLE_EXPENSE_DETAILS + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.EXP_TYPE_ID + " VARCHAR(255) UNIQUE," + TableConstants.EXP_TYPE_NAME + " VARCHAR(255)" + ")";


    public static final String CREATE_LOAN_BANK_DETAILS = "create table " + TABLE_LOAN_BANK_DETAILS + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.ID + " VARCHAR(255) UNIQUE," + TableConstants.NAME + " VARCHAR(255)" + ")";
    public static final String CREATE_LOAN_IL_TYPE_SETTING = "create table " + TABLE_LOAN_IL_TYPE_SETTING + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.LOAN_SETTING_ID + " VARCHAR(255) UNIQUE," + TableConstants.S_FLAG + " INTEGER," + TableConstants.LOAN_SETTING_NAME + " VARCHAR(255)" + ")";

    public static final String CREATE_LOAN_FED_SETTING = "create table " + TABLE_LOAN_FED_SETTING + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.LOAN_SETTING_ID + " VARCHAR(255) UNIQUE," + TableConstants.S_FLAG + " INTEGER," + TableConstants.LOAN_SETTING_NAME + " VARCHAR(255)" + ")";

    public static final String CREATE_LOAN_MFI_SETTING = "create table " + TABLE_LOAN_MFI_SETTING + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.LOAN_SETTING_ID + " VARCHAR(255) UNIQUE," + TableConstants.S_FLAG + " INTEGER," + TableConstants.LOAN_SETTING_NAME + " VARCHAR(255)" + ")";

    public static final String CREATE_LOAN_BANK_LTYPE = "create table " + TABLE_LOAN_LTYPES + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.LOAN_TYPE_ID + " VARCHAR(255) UNIQUE," + TableConstants.LOANTYPENAME + " VARCHAR(255)" + ")";

    public static final String CREATE_LOAN_BANK_POL = "create table " + TABLE_LOAN_PURPOSE + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.LOAN_TYPE_ID + " VARCHAR(255) UNIQUE," + TableConstants.LOANTYPENAME + " VARCHAR(255)" + ")";

    public static final String CREATE_LOAN_INSTAL_TYPE = "create table " + TABLE_LOAN_INSTALLMENT + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.InstallmentTypeId + " VARCHAR(255) UNIQUE," + TableConstants.InstallmentTypeName + " VARCHAR(255)" + ")";


    public static final String CREATE_MEMBER_BANKFULL_DETAILS = "create table " + TABLE_MEMBER_BANKFULLDETAILS + " ("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," + TableConstants.MEM_BANK_ID + " VARCHAR(255) ," + TableConstants.MEM_BANK_NAME + " VARCHAR(255) ," + TableConstants.MEM_BRANCHNAME_ID + " VARCHAR(255) UNIQUE," + TableConstants.MEM_BRANCHNAME + " VARCHAR(255)" + ")";

    public static final String CREATE_JANDHAN_BANKNAME_DETAILS = "create table " + TABLE_JANDHAN_BANKNAMEDETAILS + " ("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," + TableConstants.JAN_BANK_ID + " VARCHAR(255) ," + TableConstants.JAN_BANK_NAME + " VARCHAR(255) ," + TableConstants.JAN_BRANCHNAME_ID + " VARCHAR(255) UNIQUE," + TableConstants.JAN_BRANCHNAME + " VARCHAR(255)" + ")";



    //TODO:: OFFLINE TABLE

    public static final String CREATE_TABLE_TRANSACTION = "create table " + TABLE_TRANSACTION + "("
            + TableConstants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + TableConstants.TRANSACTION_TYPE + " VARCHAR(255) ,"  + TableConstants.TRANSACTION_SUB_TYPE + " VARCHAR(255) ,"
            + TableConstants.OFF_LASTTRANSACTIONDATE_TIME + " VARCHAR(255) ,"  + TableConstants.OFF_MOBILEDATE + " VARCHAR(255) ," + TableConstants.ANIMATOR_ID + " VARCHAR(255) ,"
            + TableConstants.CASH_IN_HAND + " VARCHAR(255) ," + TableConstants.OTHER_INCOME + " VARCHAR(255) ,"+ TableConstants.BANK_SB_AC_ID + " VARCHAR(255) ," + TableConstants.MoM_NAME + " VARCHAR(255) ," + TableConstants.MOM_ID + " VARCHAR(255) ,"
            + TableConstants.CASH_AT_BANK + " VARCHAR(255) ,"
            + TableConstants.OFF_DISBURSEDATETIME + " VARCHAR(255) ,"
            + TableConstants.OFF_MODIFIEDDATE + " VARCHAR(255) ,"
            + TableConstants.OFF_SANCTIONDATETIME + " VARCHAR(255) , "
            + TableConstants.OFF_SAVING + " VARCHAR(255) ,"
            + TableConstants.OFF_V_SAVING + " VARCHAR(255) ,"
            + TableConstants.OFF_MEMBER_ID + " VARCHAR(255) ,"
            + TableConstants.OFF_MEMBER_NAME + " VARCHAR(255) ,"
            + TableConstants.OFF_LOAN_OS + " VARCHAR(255) ,"
            + TableConstants.OFF_MEM_OS + " VARCHAR(255) ,"
            + TableConstants.OFF_LOAN_ID + " VARCHAR(255) ,"
            + TableConstants.OFF_LOANTYPENAME + " VARCHAR(255) ,"
            + TableConstants.OFF_LOAN_TYPE_ID + " VARCHAR(255) ,"
            + TableConstants.TLT_SAV_AMOUNT + " VARCHAR(255) ,"
            + TableConstants.GRP_INTEREST + " VARCHAR(255) ,"
            + TableConstants.GRP_REPAYMENT + " VARCHAR(255) ,"
            + TableConstants.GRP_INT_SUB_RX + " VARCHAR(255) ,"
            + TableConstants.GRP_CHARGE + " VARCHAR(255) ,"
            + TableConstants.BT_INT_SUB_RX + " VARCHAR(255) ,"
            + TableConstants.BT_T_Amount + " VARCHAR(255) ,"
            + TableConstants.BT_T_CHARGE + " VARCHAR(255) ,"
            + TableConstants.BT_F_NAME + " VARCHAR(255) ,"
            + TableConstants.BT_T_NAME + " VARCHAR(255) ,"
            + TableConstants.OFF_LOAN_NAME + " VARCHAR(255) ,"
            + TableConstants.OFF_FROMDATE + " VARCHAR(255) ,"
            + TableConstants.OFF_TODATE + " VARCHAR(255) ,"
            + TableConstants.OFF_AUDITDATE + " VARCHAR(255) ,"
            + TableConstants.OFF_AUDITTNAME + " VARCHAR(255) ,"
            + TableConstants.OFF_TRAININGDATE + " VARCHAR(255) ,"
            + TableConstants.OFF_TRAININGLISTID + " VARCHAR(255) ,"
            + TableConstants.BT_CHARGE + " VARCHAR(255) ,"
            + TableConstants.BT_INTEREST + " VARCHAR(255) ,"
            + TableConstants.BT_DEPOSIT + " VARCHAR(255) ,"
            + TableConstants.BT_WITHDRAW + " VARCHAR(255) ,"
            + TableConstants.BT_EXPENSE + " VARCHAR(255) ,"
            + TableConstants.BT_F_BANK_SBAC_ID + " VARCHAR(255) ,"
            + TableConstants.BT_T_BANK_SBAC_ID + " VARCHAR(255) ,"
            + TableConstants.BT_T_LOAN_ACID + " VARCHAR(255) ,"
            + TableConstants.MEM_INT_CDUE + " VARCHAR(255) ,"
            + TableConstants.MEM_AMOUNT + " VARCHAR(255) ,"
            + TableConstants.MEM_INTEREST + " VARCHAR(255) ,"
            + TableConstants.TLT_INCOME_AMOUNT + " VARCHAR(255) ,"
            + TableConstants.TLT_EXP_AMOUNT + " VARCHAR(255) ,"
            + TableConstants.SAVING_COUNT + " VARCHAR(255) ,"
            + TableConstants.INCOME_COUNT + " VARCHAR(255) ,"
            + TableConstants.EXPENSE_COUNT + " VARCHAR(255) ,"
            + TableConstants.BT_COUNT + " VARCHAR(255) ,"
            + TableConstants.MR_COUNT + " VARCHAR(255) ,"
            + TableConstants.GRP_COUNT + " VARCHAR(255) ,"
            + TableConstants.LD_COUNT + " VARCHAR(255) ,"
            + TableConstants.ATT_COUNT + " VARCHAR(255) ,"
            + TableConstants.MoM_COUNT + " VARCHAR(255),"
            + TableConstants.TRAINING_COUNT + " VARCHAR(255),"
            + TableConstants.MR_IL_COUNT + " VARCHAR(255),"
            + TableConstants.AUDIT_COUNT + " VARCHAR(255),"
            + TableConstants.OFF_EXP_TYPE_ID + " VARCHAR(255) ,"
            + TableConstants.OFF_INC_TYPE_ID + " VARCHAR(255) ,"
            + TableConstants.IC_EXP_AMOUNT + " VARCHAR(255) ,"
            + TableConstants.OFF_EXP_TYPE_NAME + " VARCHAR(255) ,"
            + TableConstants.OFF_LOAN_AMOUNT + " VARCHAR(255) ,"
            + TableConstants.UPLOADSCHEDULE_COUNT + " VARCHAR(255) ,"
            + TableConstants.UPLOAD_URL + " VARCHAR(255) ,"
            + TableConstants.OFF_TENURE + " VARCHAR(255) ,"
            + TableConstants.OFF_POL + " VARCHAR(255) ,"
            + TableConstants.OFF_POL_ID + " VARCHAR(255) ,"
            + TableConstants.OFF_BRANCHNAME + " VARCHAR(255) ,"
            + TableConstants.OFF_BRANCHNAME_ID + " VARCHAR(255) ,"
            + TableConstants.OFF_BANKNAME + " VARCHAR(255) ,"
            + TableConstants.OFF_ACCOUNT_NO + " VARCHAR(255) ,"
            + TableConstants.OFF_BANK_ID + " VARCHAR(255) ,"
            + TableConstants.SHG_ID + " VARCHAR(255) ,"
            + TableConstants.LOGIN_SAV + " VARCHAR(10) ,"
            + TableConstants.LOGIN_SUBSCRIPTION + " VARCHAR(10) ,"
            + TableConstants.LOGIN_PENALTY + " VARCHAR(10) ,"
            + TableConstants.LOGIN_OTHERINCOME + " VARCHAR(10) ,"
            + TableConstants.LOGIN_GRP_LOAN + " VARCHAR(10) ,"
            + TableConstants.LOGIN_MR_LOAN + " VARCHAR(10) ,"
            + TableConstants.LOGIN_MR_IL + " VARCHAR(10) ,"
            + TableConstants.LOGIN_MOM + " VARCHAR(10),"
            + TableConstants.LOGIN_AUDIT + " VARCHAR(10),"
            + TableConstants.LOGIN_TRAINING + " VARCHAR(10),"
            + TableConstants.LOGIN_SEEDFUND + " VARCHAR(10),"
            + TableConstants.LOGIN_DONATION + " VARCHAR(10),"
            + TableConstants.LOGIN_BT_ENTRY + " VARCHAR(10),"
            + TableConstants.LOGIN_BT_FD + " VARCHAR(10),"
            + TableConstants.LOGIN_BT_RD + " VARCHAR(10),"
            + TableConstants.LOGIN_BT_ACTOAC + " VARCHAR(10),"
            + TableConstants.LOGIN_EXPENSE + " VARCHAR(10),"
            + TableConstants.LOGIN_aATTD + " VARCHAR(10),"
            + TableConstants.LOGIN_LD_IL + " VARCHAR(10),"
            + TableConstants.LOGIN_UPLOADIMAGE + " VARCHAR(10),"
            + TableConstants.SYNC_STATUS + " VARCHAR(255) ,"
            + TableConstants.OFF_MODE_OF_CASH + " VARCHAR(255)" + ")";

/*
    public static final String CREATE_TABLE_TRANS_INCOME = "create table " + TABLE_INCOME + "("
            + TableConstants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.OFF_LASTTRANSACTIONDATE_TIME + " VARCHAR(255) ,"+ TableConstants.OFF_TLT_AMOUNT+ " VARCHAR(255) ," + TableConstants.OFF_BANK + " VARCHAR(255) ," + TableConstants.OFF_CASH + " VARCHAR(255) ," + TableConstants.OFF_BANK_ID + " VARCHAR(255) ,"+ TableConstants.OFF_BANKNAME + " VARCHAR(255) ,"  + TableConstants.OFF_MEMBER_NAME + " VARCHAR(255) ," + TableConstants.OFF_MEMBER_ID + " VARCHAR(255) ," + TableConstants.SHG_ID + " VARCHAR(255) ,"  + TableConstants.OFF_MODE_OF_CASH + " VARCHAR(255)" + ")";

    public static final String CREATE_TABLE_TRANS_EXPENSE = "create table " + TABLE_EXPENSE+ "("
            + TableConstants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.OFF_LASTTRANSACTIONDATE_TIME + " VARCHAR(255) ,"+ TableConstants.OFF_EXP_TYPE_NAME + " VARCHAR(255) ," + TableConstants.OFF_EXP_TYPE_ID + " VARCHAR(255) ," + TableConstants.OFF_TLT_AMOUNT + " VARCHAR(255) ," + TableConstants.OFF_MEMBER_NAME + " VARCHAR(255) ," + TableConstants.OFF_MEMBER_ID + " VARCHAR(255) ," + TableConstants.SHG_ID + " VARCHAR(255) ,"  + TableConstants.OFF_MODE_OF_CASH + " VARCHAR(255)" + ")";

    public static final String CREATE_TABLE_TRANS_MEM_LOAN_RP = "create table " + TABLE_MEM_LOAN_REPAYMENT+ "("
            + TableConstants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.OFF_LASTTRANSACTIONDATE_TIME + " VARCHAR(255) ,"+ TableConstants.OFF_LOAN_INTEREST + " VARCHAR(255) ," + TableConstants.OFF_LOAN_AMOUNT + " VARCHAR(255) ," + TableConstants.OFF_TLT_AMOUNT + " VARCHAR(255) ," + TableConstants.OFF_LOANTYPENAME + " VARCHAR(255) ," + TableConstants.OFF_LOAN_TYPE_ID + " VARCHAR(255) ," + TableConstants.OFF_LOAN_OS + " VARCHAR(255) ," + TableConstants.OFF_LOAN_NAME + " VARCHAR(255) ," + TableConstants.OFF_LOAN_ID + " VARCHAR(255) ," + TableConstants.OFF_LOAN_ACC_NO + " VARCHAR(255) ," + TableConstants.OFF_LOAN_ACC_ID + " VARCHAR(255) ," + TableConstants.OFF_MEMBER_NAME + " VARCHAR(255) ," + TableConstants.OFF_MEMBER_ID + " VARCHAR(255) ," + TableConstants.SHG_ID + " VARCHAR(255) ,"  + TableConstants.OFF_MODE_OF_CASH + " VARCHAR(255)" + ")";

    public static final String CREATE_TABLE_TRANS_LOAN_DIS= "create table " + TABLE_LOANDISBURSEMENT+ "("
            + TableConstants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.OFF_LOAN_OS + " VARCHAR(255) ,"+ TableConstants.OFF_LOANTYPENAME + " VARCHAR(255) ," + TableConstants.OFF_LOAN_TYPE_ID + " VARCHAR(255) ," + TableConstants.OFF_LASTTRANSACTIONDATE_TIME + " VARCHAR(255) ,"+ TableConstants.OFF_TLT_AMOUNT + " VARCHAR(255) ,"+ TableConstants.OFF_LOAN_AMOUNT + " VARCHAR(255) ,"  + TableConstants.OFF_POL + " VARCHAR(255) ," + TableConstants.OFF_POL_ID + " VARCHAR(255) ," + TableConstants.OFF_MEMBER_NAME + " VARCHAR(255) ," + TableConstants.OFF_MEMBER_ID + " VARCHAR(255) ,"+ TableConstants.OFF_TENURE + " VARCHAR(255) ,"  + TableConstants.SHG_ID + " VARCHAR(255) ,"  + TableConstants.OFF_MODE_OF_CASH + " VARCHAR(255)" + ")";

    public static final String CREATE_TABLE_TRANS_BANKTRANS= "create table " + TABLE_BANKTRANSACTION+ "("
            + TableConstants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.OFF_FD_VALUE + " VARCHAR(255) ,"+ TableConstants.OFF_CURRENT_BALANCE + " VARCHAR(255) ," + TableConstants.OFF_SHG_SB_AC_ID + " VARCHAR(255) ," + TableConstants.OFF_LASTTRANSACTIONDATE_TIME + " VARCHAR(255) ,"+ TableConstants.OFF_BRANCHNAME + " VARCHAR(255) ," + TableConstants.OFF_BRANCHNAME_ID + " VARCHAR(255) ," + TableConstants.OFF_BANKNAME + " VARCHAR(255) ," + TableConstants.OFF_ACCOUNT_NO + " VARCHAR(255) ," + TableConstants.OFF_BANK_ID + " VARCHAR(255) ,"+ TableConstants.SHG_ID + " VARCHAR(255) ,"  + TableConstants.OFF_MODE_OF_CASH + " VARCHAR(255)" + ")";

    public static final String CREATE_TABLE_TRANS_GRP_LOAN_RP = "create table " + TABLE_GRP_LOAN_REPAYMENT+ "("
            + TableConstants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.OFF_LASTTRANSACTIONDATE_TIME + " VARCHAR(255) ,"+ TableConstants.OFF_MEMBER_NAME + " VARCHAR(255) ," + TableConstants.OFF_MEMBER_ID + " VARCHAR(255) ," + TableConstants.SHG_ID + " VARCHAR(255) ,"  + TableConstants.OFF_MODE_OF_CASH + " VARCHAR(255)" + ")";


    public static final String CREATE_TABLE_TRANSACTION= "create table " + TABLE_TRANSACTION + "("
            + TableConstants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"+ TableConstants.TRANSACTION_TYPE + " VARCHAR(255) ," + TableConstants.OFF_LASTTRANSACTIONDATE_TIME + " VARCHAR(255) ," + TableConstants.OFF_SAVING + " VARCHAR(255) ," + TableConstants.OFF_V_SAVING + " VARCHAR(255) ," + TableConstants.OFF_MEMBER_ID + " VARCHAR(255) ,"  + TableConstants.OFF_MEMBER_NAME  + TableConstants.OFF_LOAN_OS + " VARCHAR(255) ,"+ TableConstants.OFF_LOANTYPENAME + " VARCHAR(255) ," + TableConstants.OFF_LOAN_TYPE_ID + " VARCHAR(255) ," + TableConstants.OFF_LOAN_TYPE_ID + " VARCHAR(255) ,"  + TableConstants.OFF_LOAN_TYPE_ID + " VARCHAR(255) ,"  + TableConstants.OFF_LASTTRANSACTIONDATE_TIME + " VARCHAR(255) ,"+ TableConstants.TLT_SAV_AMOUNT + " VARCHAR(255) ,"+ TableConstants.OFF_EXP_TYPE_NAME + " VARCHAR(255) ," + TableConstants.OFF_EXP_TYPE_ID + " VARCHAR(255) ,"+ TableConstants.OFF_LOAN_AMOUNT + " VARCHAR(255) ,"  + TableConstants.OFF_POL + " VARCHAR(255) ," + TableConstants.OFF_POL_ID + " VARCHAR(255) ," + " VARCHAR(255) ," + TableConstants.OFF_BRANCHNAME + " VARCHAR(255) ," + TableConstants.OFF_BRANCHNAME_ID + " VARCHAR(255) ," + TableConstants.OFF_BANKNAME + " VARCHAR(255) ," + TableConstants.OFF_ACCOUNT_NO + " VARCHAR(255) ," + TableConstants.OFF_BANK_ID + " VARCHAR(255) ,"+ TableConstants.SHG_ID + " VARCHAR(255) ,"  + TableConstants.OFF_MODE_OF_CASH + " VARCHAR(255)" + ")";*/





    //OFFLINE TABLE:

  /*  public static final String CREATE_OFFLINE_SAVINGS = "create table " + TABLE_LOAN_INSTALLMENT + "("
            + TableConstants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.InstallmentTypeId + " VARCHAR(255) UNIQUE," + TableConstants.InstallmentTypeName + " VARCHAR(255)" ++ TableConstants.InstallmentTypeName + " VARCHAR(255)" ++ TableConstants.InstallmentTypeName + " VARCHAR(255)" + ")";

*/

    public static final String AUDIT_TRANSACTIONSAVINGS = "create table " + AUDIT_SAVINGS + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
    + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";


    public static final String AUDIT_VOLUNTARY_SAVINGS = "create table " + AUDIT_VOLUNTARYSAVINGS + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";


    public static final String AUDIT_SAVING_DISBURSEMENT = "create table " + AUDIT_SAVINGDISBURSEMENT + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";

    public static final String AUDIT_INTERNAL_LOAN_DISBURSEMENT = "create table " + AUDIT_INTERNALLOANDISBURSEMENT + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";

    public static final String AUDIT_SEED_FUND = "create table " + AUDIT_SEEDFUND + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";

    public static final String AUDIT_FDBANK_TRANSACTION = "create table " + AUDIT_FDBANKTRANSACTION + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";

    public static final String AUDIT_INCOME_DISBURSEMENT = "create table " + AUDIT_INCOMEDISBURSEMENT + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";

    public static final String AUDIT_OTHER_INCOME = "create table " + AUDIT_OTHERINCOME + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";


    public static final String AUDIT_GROUPLOAN_REPAYMENT = "create table " + AUDIT_GROUPLOANREPAYMENT + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";

    public static final String AUDIT_PENATYTRANSACTION = "create table " + AUDIT_PENALTY + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";


    public static final String AUDIT_BANK_TRANSACTION = "create table " + AUDIT_BANKTRANSACTION + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";

    public static final String AUDIT_SUBSCRIPTION_FEDS = "create table " + AUDIT_SUBSCRIPTIONFEDS + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";

    public static final String AUDIT_OTHERINCOME_GROUP = "create table " + AUDIT_OTHERINCOMEGROUP + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";

    public static final String AUDIT_MEMBERLOAN_REAPYMENT = "create table " + AUDIT_MEMBERLOANREAPYMENT + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";

    public static final String AUDIT_VOLUNTARY_SAVINGSDISBURSEMENT = "create table " + AUDIT_VOLUNTARYSAVINGSDISBURSEMENT + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";

    public static final String AUDIT_OTHER_EXPENSE = "create table " + AUDIT_OTHEREXPENSE + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";

    public static final String AUDIT_MEETING_EXPENSE = "create table " + AUDIT_MEETINGEXPENSE + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";

    public static final String AUDIT_INTERLOAN_REPAYMENT = "create table " + AUDIT_INTERLOANREPAYMENT + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";

    public static final String AUDIT_DONATION_TRANSACTION = "create table " + AUDIT_DONATION + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";

    public static final String AUDIT_SUBSCRIPTION_TRANSACTION = "create table " + AUDIT_SUBSCRIPTION + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";

    public static final String AUDIT_SUBSCRIPTION_CHARGES = "create table " + AUDIT_SUBSCRIPTIONCHARGES + "("
            + TableConstants.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TableConstants.AUDIT_ID + " VARCHAR(255)," + TableConstants.SHG_USERID + " VARCHAR(255)," + TableConstants.AUDIT_MEMBERNAME + " VARCHAR(255)," + TableConstants.AUDIT_MEBERID + " VARCHAR(255),"
            + TableConstants.AUDIT_SETTINGS + " VARCHAR(255)," + TableConstants.AUDIT_TRANSDATE + " VARCHAR(255)," + TableConstants.OLD_TRANS_AMOUNT + " VARCHAR(255)," + TableConstants.NEW_TRANS_AMOUNT + " VARCHAR(255)" + ")";

}

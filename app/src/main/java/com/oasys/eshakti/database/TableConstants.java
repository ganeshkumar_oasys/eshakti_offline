package com.oasys.eshakti.database;

public class TableConstants {

    public static final String L_ID = "local_Id";
    public static final String ID = "id";
    public static final String USERID = "user_id";
    public static final String ANIMATOR_ID = "animator_id";
    public static final String USERNAME = "Username";
    public static final String PASSWORD = "Password";
    public static final String LANGUAGE = "Language";

    public static final String TRAINING_NAME = "name";
    public static final String TRAINING_UUID = "uuid";
    public static final String TRAINING_LANGUAGE_UUID = "Language_uuid";

    public static final String SHG_CODE = "shg_code";
    public static final String SHG_NAME = "shg_name";
    public static final String SHG_ID = "shg_Id";
    public static final String SHG_TYPE = "shgType";
    public static final String SHG_CREATION_DATE = "shgCreationdate";
    public static final String SHG_VERIFIED_DATE = "shgVerifieddate";
    public static final String SHG_GROUPMOBILE_NUMBER = "mobilenumber";
    public static final String SHG_GROUPTOTAL_MEMBER = "totalmember";
    public static final String SYNC_STATUS = "sync_status";
    public static final String SHG_SB_AC_ID= "shgSavingsAccountId";
    public static final String CASH_IN_HAND = "CashInHand";
    public static final String CASH_AT_BANK = "CashAtBank";
    public static final String NAME = "name";
    public static final String IS_TRANS_AUDIT = "is_trans_audit";
    public static final String IFSC = "ifsc";
    public static final String PRESIDENT_NAME = "president_name";
    public static final String SECRETARY_NAME = "secretary_name";
    public static final String TREASURER_NAME = "treasurer_name";
    public static final String BLOCK_NAME = "block_name";
    public static final String GRP_FORMATION_DATE = "groupFormationDate";
    public static final String BLOCKNAME = "blockName";
    public static final String PANCHAYATNAME = "panchayatName";
    public static final String VILLAGENAME = "villageName";
    public static final String LASTTRANSACTIONDATE = "lastTransactionDate";
    public static final String IS_TRANSACTION_TDY = "is_transaction_tdy";
    public static final String MODIFIEDDATE = "modifiedDate";
    public static final String OPENINGDATE = "openingDate";
    public static final String CURR_BALANCE = "currentBalance";
    public static final String CURR_FD_BALANCE = "currentFDBalance";
    public static final String ACTIVEFLAG = "status_flag";
    public static final String PRIMARY_FlAG = "Primary_flag";
    public static final String FIRST_SFLAG= "fFlag";
    public static final String DISTRICT_ID = "districId";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String DATE_OF_ASSIGNING = "dateOfAssigning";
    public static final String TOTAL_GROUP = "totalGroup";
    public static final String AGE = "age";
    public static final String ANIMATOR_NAME = "AnimatorName";
    public static final String SSN_TYPENAME = "name";
    public static final String SSN_TYPEID = "id";

    public static final String SAVING_COUNT = "sCount";
    public static final String INCOME_COUNT = "iCount";
    public static final String EXPENSE_COUNT = "eCount";
    public static final String BT_COUNT = "btCount";
    public static final String MR_COUNT = "mrCount";
    public static final String MR_IL_COUNT = "mrILCount";
    public static final String MoM_COUNT = "momCount";
    public static final String ATT_COUNT = "attCount";
    public static final String GRP_COUNT = "gCount";
    public static final String LD_COUNT = "ldCount";
    public static final String UPLOADSCHEDULE_COUNT = "uploadschedulecount";
    public static final String AUDIT_COUNT = "auditcount";
    public static final String TRAINING_COUNT = "traningcount";

    public static final String LOGIN_SAV = "SL";
    public static final String LOGIN_UPLOADIMAGE = "UPIU";
    public static final String LOGIN_SUBSCRIPTION = "SUBL";
    public static final String LOGIN_OTHERINCOME = "OTHIL";
    public static final String LOGIN_PENALTY = "PENL";
    public static final String LOGIN_DONATION = "DONL";
    public static final String LOGIN_SEEDFUND = "SEEDL";
    public static final String LOGIN_BT_ENTRY = "BT_EN_L";
    public static final String LOGIN_BT_FD= "BTFDL";
    public static final String LOGIN_BT_RD = "BTRDL";
    public static final String LOGIN_BT_ACTOAC = "BT_AC_L";
    public static final String LOGIN_MR_LOAN = "MR_L";
    public static final String LOGIN_MR_IL = "MR_IL_L";
    public static final String LOGIN_LD_IL = "LDL";
    public static final String LOGIN_GRP_LOAN = "GRPL";
    public static final String LOGIN_EXPENSE = "EXPL";
    public static final String LOGIN_aATTD = "ATTDL";
    public static final String LOGIN_MOM = "MOML";
    public static final String LOGIN_AUDIT = "AUDITL";
    public static final String LOGIN_TRAINING = "TRANINGL";




    public static final String MEM_BANK_ID = "memberId";
    public static final String MEM_BANK_NAME = "memberName";
    public static final String MEM_BRANCHNAME_ID = "memberBranchName_id";
    public static final String MEM_BRANCHNAME = "memberBranchName";

    public static final String JAN_BANK_ID = "jandhanbankId";
    public static final String JAN_BANK_NAME = "jandhanbankname";
    public static final String JAN_BRANCHNAME_ID = "janBranchName_id";
    public static final String JAN_BRANCHNAME = "janBranchName";




    public static final String MoM_NAME = "momName";
    public static final String MEMBER_ID = "memberId";
    public static final String MEMBER_NAME = "memberName";
    public static final String PHONE_NO = "Phone_No";
    public static final String MEMBER_USER_ID = "member_user_id";


    public static final String BANK_ID = "bankId";
    public static final String BANKNAME = "sSelectedBank";
    public static final String BRANCHNAME = "branchName";
    public static final String ACCOUNT_NO = "accountNumber";
    public static final String BANKNAME_ID = "bankNameId";
    public static final String BRANCHNAME_ID = "branchNameId";
    public static final String BRANCH_ID = "branchId";
    public static final String AADHAR_NUM = "aadharNumber";
    public static final String PIP_NUM = "pipNumber";

    public static final String LOAN_ID = "loanId";
    public static final String LOAN_TYPE_ID = "loanTypeId";
    public static final String LOAN_NAME = "loanName";
    public static final String LOAN_AMOUNT = "OutStanding";
    public static final String LOANTYPENAME = "loanTypeName";
    public static final String LOAN_ACC_NO = "loanAcNo";
    public static final String LOAN_ACC_ID = "loanAcId";
    public static final String OFF_MEM_OS = "memOutStanding";



    public static final String TYPE = "Type";
    public static final String EXP_TYPE_NAME = "ExpenseTypeName";
    public static final String EXP_TYPE_ID = "ExpenseTypeId";

    public static final String LOAN_SETTING_ID = "loanSettingId";
    public static final String S_FLAG = "sflag";
    public static final String LOAN_SETTING_NAME = "loanSettingName";


    public static final String InstallmentTypeId = "installmentTypeId";
    public static final String InstallmentTypeName = "installmentTypeName";
    public static final String TYPENAME = "TypeName";

    //OFFLINE CONSTANTS
    public static final String OFF_LOAN_ID = "loanId";
    public static final String OFF_LOAN_TYPE_ID = "loanTypeId";
    public static final String OFF_LOAN_NAME = "loanName";
    public static final String OFF_LOAN_OS = "OutStanding";
    public static final String OFF_LOANTYPENAME = "loanTypeName";
    public static final String OFF_LOAN_ACC_NO = "loanAcNo";
    public static final String OFF_LOAN_ACC_ID = "loanAcId";
    public static final String OFF_LOAN_AMOUNT= "amount";
    public static final String OFF_LOAN_INTEREST= "ineterest";
    public static final String OFF_LOAN_SFLAG= "sFlag";
    public static final String OFF_LASTTRANSACTIONDATE_TIME = "lastTransactionDateTime";
    public static final String OFF_MOBILEDATE = "mobileDateTime";
    public static final String TRANSACTION_TYPE = "TRANSACTION_TYPE";
    public static final String TRANSACTION_SUB_TYPE = "TRANSACTION_SUB_TYPE";
    public static final String OFF_SANCTIONDATETIME= "sanctionDateTime";
    public static final String OFF_DISBURSEDATETIME= "disbursementDateTime";
    public static final String OFF_MODIFIEDDATE= "modifiedDateTime";

    public static final String OFF_BANK_ID = "bankId";
    public static final String OFF_BANKNAME = "sSelectedBank";
    public static final String OFF_BRANCHNAME = "branchName";
    public static final String OFF_F_ACCOUNT_NO = "fromAccountNumber";
    public static final String OFF_T_ACCOUNT_NO = "toAccountNumber";
    public static final String OFF_ACCOUNT_NO = "accountNumber";
    public static final String OFF_BANKNAME_ID = "bankNameId";
    public static final String OFF_BRANCHNAME_ID = "branchNameId";

    public static final String OFF_MEMBER_ID = "memberId";
    public static final String OFF_MEMBER_NAME = "memberName";
    public static final String OFF_PHONE_NO = "phone_No";
    public static final String OFF_MEMBER_USER_ID = "member_user_id";
    public static final String OFF_EXP_TYPE_NAME = "expenseTypeName";
    public static final String OFF_EXP_TYPE_ID = "expenseTypeId";
    public static final String OFF_INC_TYPE_ID = "incomeTypeId";


    public static final String TLT_INCOME_AMOUNT = "totalIncomeAmount";
    public static final String TLT_EXP_AMOUNT = "totalExpenseAmount";
    public static final String GRP_INTEREST = "grp_interest";
    public static final String MEM_AMOUNT= "mem_amount";
    public static final String MEM_INTEREST = "memInterest";
    public static final String BT_INTEREST = "btInterest";
    public static final String BT_DEPOSIT = "btDeposit";
    public static final String BT_WITHDRAW = "btWithdraw";
    public static final String BT_EXPENSE = "btExpense";
    public static final String BT_F_BANK_SBAC_ID = "btFromSavingAcId";
    public static final String BT_T_BANK_SBAC_ID = "btToSavingAcId";
    public static final String BT_T_LOAN_ACID = "btToLoanId";
    public static final String BT_CHARGE = "btCharge";
    public static final String BT_T_CHARGE = "transferCharge";
    public static final String BT_T_Amount = "transferAmount";
    public static final String MEM_INT_CDUE = "memCurrentDue";
    public static final String GRP_REPAYMENT = "grp_repayment";
    public static final String GRP_INT_SUB_RX= "grp_intSubventionRecieved";
    public static final String BT_INT_SUB_RX= "bt_intSubventionRecieved";
    public static final String GRP_CHARGE = "grp_charge";
    public static final String IC_EXP_AMOUNT = "ic_exp_Amount";
    public static final String OTHER_INCOME= "otherIncome";
    public static final String TLT_SAV_AMOUNT = "totalSavAmount";
    public static final String UPLOAD_URL = "uploadUrl";
    public static final String OFF_MODE_OF_CASH = "modeOCash";
    public static final String OFF_CASH = "cash";
    public static final String OFF_BANK = "bank";
    public static final String OFF_FD_VALUE= "FDValue";
    public static final String OFF_RD_VALUE= "RDValue";
    public static final String OFF_CURRENT_BALANCE= "currentBalance";
    public static final String OFF_SHG_SB_AC_ID= "shgSavingsAccountId";

    public static final String OFF_SAVING = "savings";
    public static final String OFF_V_SAVING = "vSavings";
    public static final String OFF_FD_ID= "fixedDepositId";
    public static final String OFF_FD_NAME = "fixedDepositName";
    public static final String OFF_TENURE= "tenure";


    public static final String OFF_FROMDATE= "fromdate";
    public static final String OFF_AUDITDATE= "auditdate";
    public static final String OFF_TODATE= "todate";
    public static final String OFF_AUDITTNAME= "auditname";


    public static final String OFF_TRAININGDATE= "trainingdate";
    public static final String OFF_TRAININGLISTID= "traininglistid";


    public static final String OFF_POL= "POL";
    public static final String OFF_POL_ID= "POL_ID";

    public static final String BANK_SB_AC_ID = "bankSavingAcId";

//    BankBranchKeyvalues

    public static final String MEMBERBANK_ID = "bank_Id";
    public static final String MEMBERBANK_NAME = "bank_name";

    public static final String MOM_ID = "momId";
    public static final String BT_T_NAME = "mToBank";
    public static final String BT_F_NAME = "mFromBank";


    public static final String SSS_TYPE_ID = "sssTypeId";
    public static final String BANK_NAME_ID = "bankNameId";
    public static final String BRANCH_NAME_ID = "branchNameId";
    public static final String SSS_NUMBER = "sss_number";
    public static final String IS_SSS = "is_sss";
    public static final String FIRST_UPDATE = "first_update";
    public static final String SSN_MEMBERNAME = "memberName";
    public static final String SSN_BANKNAME = "bankName";
    public static final String SSN_BRANCHNAME = "branchName";
    public static final String LAST_UPDATE = "last_update";
    public static final String SSS_UPDATE = "sss_date";
    public static final String IS_BEFORE_2019 = "is_before_2019";
    public static final String CREATED_DATE = "created_date";
    public static final String MODIFIED_DATE = "modified_date";


    public static final String SHGSAVINGSACCOUNTID = "shgSavingsAccountId";
    public static final String RD_BANKNAME = "bankName";
    public static final String RD_BRANCHNAME = "branchName";
    public static final String RD_ACCOUNTNO = "accountNo";
    public static final String RD_BANKID = "bankId";
    public static final String RD_IFSC = "ifsc";
    public static final String RD_BRANCH_ID = "branchId";
    public static final String RD_ISPRIMARY = "isPrimary";
    public static final String RD_INSTALLAMOUNT = "installment_amount";
    public static final String RD_RATE_OF_INTEREST = "rate_of_interest";
    public static final String RD_TENURE = "tenure";
    public static final String RD_CURRENTBALANCE = "current_balance";
    public static final String RD_OPENINGBALANCE = "opening_balance";
    public static final String RD_STATUS = "status";


    //AUDIT COLUMNS

    public static final String AUDIT_ID = "auditor_uuid";
    public static final String SHG_USERID = "shg_uuid";
    public static final String AUDIT_MEMBERNAME = "member";
    public static final String AUDIT_MEBERID = "member_uuid";
    public static final String AUDIT_SETTINGS = "settings";
    public static final String AUDIT_TRANSDATE = "transaction_date";
    public static final String OLD_TRANS_AMOUNT = "old_transaction_amount";
    public static final String NEW_TRANS_AMOUNT = "new_transaction_amount";



}

package com.oasys.eshakti.OasysUtils;

import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.R;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.Service.TransactionService;
import com.oasys.eshakti.activity.ExitActivity;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.database.TransactionTable;
import com.oasys.eshakti.views.ButtonFlat;
import com.oasys.eshakti.views.RaisedButton;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class AppDialogUtils {
    // public static Dialog mProgressDialog;

    public static ProgressDialog mProgressDialog;

    public static Dialog createProgressDialog(Context context) {
        return new ProgressBar(context);
    }

    public static void showConfirmationDialog(final Activity activity, final String string) {
        final Dialog confirmationDialog = new Dialog(activity);

        LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = li.inflate(R.layout.dialog_singin_diff, null, false);
        final ButtonFlat mConYesButton;
        final ButtonFlat mConNoButton;

        TextView mConfirmationHeadertextview;
        TextView mConfirmationTexttextview;

        mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialog_Title);
        mConfirmationTexttextview = (TextView) customView.findViewById(R.id.dialog_Message);

        mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert);
        mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert);

        mConYesButton.setText(AppStrings.dialogOk);

        mConNoButton.setText(AppStrings.dialogNo);

        if (string.equals("SIGNUPDIFF")) {
            mConfirmationHeadertextview.setText("CONFIRMATION");

            mConfirmationTexttextview.setText("Are You want to delete Offline Data?");
            mConYesButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (confirmationDialog.isShowing() && confirmationDialog != null) {
                        confirmationDialog.dismiss();
                    }

                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }


                }
            });

            mConNoButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (confirmationDialog.isShowing() && confirmationDialog != null) {
                        confirmationDialog.dismiss();
                    }
                }
            });
        } else if (string.equals("AADHAARCARD")) {
            mConNoButton.setVisibility(View.INVISIBLE);
            mConfirmationHeadertextview.setText("CONFIRMATION");

            mConfirmationTexttextview.setText("Can You Show you Aadhaar Card");
            mConYesButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (confirmationDialog.isShowing() && confirmationDialog != null) {
                        confirmationDialog.dismiss();
                    }

                }
            });

        } else if (string.equals("SIGNUPDIFF_NOENTRY")) {
            mConfirmationHeadertextview.setText("CONFIRMATION");

            mConfirmationTexttextview.setText(AppStrings.mLoginDetailsDelete);
            mConYesButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (confirmationDialog.isShowing() && confirmationDialog != null) {

                        confirmationDialog.dismiss();

                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }


                    }

                }
            });

            mConNoButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (confirmationDialog.isShowing() && confirmationDialog != null) {
                        confirmationDialog.dismiss();
                    }
                }

            });
        }

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(customView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();

    }

    public static void showGpsWarningDialog(final Activity activity) {
        final Dialog confirmationDialog = new Dialog(activity);

        LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = li.inflate(R.layout.dialog_singin_diff, null, false);
        final ButtonFlat mConYesButton;
        final ButtonFlat mConNoButton;

        TextView mConfirmationHeadertextview;
        TextView mConfirmationTexttextview;

        mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialog_Title);
        mConfirmationTexttextview = (TextView) customView.findViewById(R.id.dialog_Message);

        mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert);
        mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert);
        mConYesButton.setText(AppStrings.dialogOk);

        mConNoButton.setText(AppStrings.dialogNo);

        mConfirmationHeadertextview.setText("GPS settings");

        mConfirmationTexttextview.setText("GPS is not enabled.To Turn on the GPS click th settings.");
        mConYesButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (confirmationDialog.isShowing() && confirmationDialog != null) {

                    confirmationDialog.dismiss();
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    activity.startActivity(intent);
                    activity.finish();
                }

            }
        });

        mConNoButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (confirmationDialog.isShowing() && confirmationDialog != null) {
                    confirmationDialog.dismiss();
                }
            }
        });

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(customView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();

    }

    public static void showReportsDialog(final Activity activity, final int totalcoll, final int totaldisburse) {
        final Dialog confirmationDialog = new Dialog(activity);

        LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = li.inflate(R.layout.default_reports, null, false);

        TextView header, totalAmountLabel, plDisbursementLabel, cashInHandLabel, cashAtBankLabel;
        TextView totalAmount, plDisbursementAmount, cashInHandAmount, cashAtBankAmount;
        Button fragment_Submitbutton;
        fragment_Submitbutton = (Button) customView.findViewById(R.id.fragment_Submitbutton);
        header = (TextView) customView.findViewById(R.id.header);
        header.setText(AppStrings.reports);

        totalAmountLabel = (TextView) customView.findViewById(R.id.lefttotalAmount);
        totalAmountLabel.setText(AppStrings.totalSavings);
        totalAmountLabel.setTextColor(Color.BLACK);

        totalAmount = (TextView) customView.findViewById(R.id.righttotalAmount);
        totalAmount.setText(totalcoll);
        totalAmount.setTextColor(Color.BLACK);

        plDisbursementLabel = (TextView) customView.findViewById(R.id.leftPLAmount);
        plDisbursementLabel.setText(AppStrings.InternalLoanDisbursement + " " + AppStrings.amount);
        plDisbursementLabel.setTextColor(Color.BLACK);

        plDisbursementAmount = (TextView) customView.findViewById(R.id.rightPLAmount);
        plDisbursementAmount.setText(totaldisburse);
        plDisbursementAmount.setTextColor(Color.BLACK);

        cashInHandLabel = (TextView) customView.findViewById(R.id.leftCIHAmount);
        cashInHandLabel.setText(AppStrings.cashinhand);
        cashInHandLabel.setTextColor(Color.BLACK);

        cashInHandAmount = (TextView) customView.findViewById(R.id.rightCIHAmount);
        cashInHandAmount.setText("");
        cashInHandAmount.setTextColor(Color.BLACK);

        cashAtBankLabel = (TextView) customView.findViewById(R.id.leftCABAmount);
        cashAtBankLabel.setText(AppStrings.cashatBank);
        cashAtBankLabel.setTextColor(Color.BLACK);

        cashAtBankAmount = (TextView) customView.findViewById(R.id.rightCABAmount);
        cashAtBankAmount.setText("");
        cashAtBankAmount.setTextColor(Color.BLACK);

        fragment_Submitbutton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

            }
        });

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(customView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();

    }

    public static void showAppVersionUpdateDialog(final Activity activity, String mVersion) {
        final Dialog confirmationDialog = new Dialog(activity);

        LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = li.inflate(R.layout.dialog_appversionupdatealert, null, false);

        TextView header;

        RaisedButton fragment_Submitbutton;
        fragment_Submitbutton = (RaisedButton) customView.findViewById(R.id.fragment_Ok_button_dialog_appVersion);
        header = (TextView) customView.findViewById(R.id.confirmationHeader_dialog_appVersion);
        header.setText(
                "  YOUR APP VERSION CODE IS :  " + String.valueOf(mVersion) + "\n  UPDATED APP VERSION CODE IS :  "
                        + "\n  SO PLEASE UPDATE EShakti APP. ");

        fragment_Submitbutton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                confirmationDialog.dismiss();

                final String appPackageName = activity.getPackageName();
                try {
                    activity.startActivity(
                            new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(customView);
        confirmationDialog.setCancelable(false);
        confirmationDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        confirmationDialog.show();

    }

    public static void LocationAlertDialog(final Activity context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final Dialog alertDialog = builder.create();
        builder.setTitle("Location Services Not Active");
        builder.setMessage("Please enable Location Services and GPS");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                // Show location settings when the user acknowledges the
                // alert dialog
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

    }

    public static void showConfirmationOfflineAvailDialog(final Activity activity) {
        final Dialog confirmationDialog = new Dialog(activity);

        LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = li.inflate(R.layout.dialog_new_singin_diff, null, false);
        final ButtonFlat mConYesButton;
        final ButtonFlat mConNoButton;

        TextView mConfirmationHeadertextview;
        TextView mConfirmationTexttextview;

        mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialog_Title);
        mConfirmationTexttextview = (TextView) customView.findViewById(R.id.dialog_Message);

        mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert);
        mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert);
        mConYesButton.setText(AppStrings.dialogOk);

        mConNoButton.setText(AppStrings.dialogNo);

        mConNoButton.setVisibility(View.GONE);
        mConfirmationHeadertextview.setText("INFORMATION");
        // Your offline data is still available at local db, So you must move
        // data from db to online server!!!
        mConfirmationTexttextview.setText("Click yes to upload offline data.");
        mConYesButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (confirmationDialog.isShowing() && confirmationDialog != null) {

                    confirmationDialog.dismiss();
                    mProgressDialog = new ProgressDialog(activity);
                    mProgressDialog.setTitle("Offline data is uploading.. ");
                    mProgressDialog.setMessage("Please Wait....");
                    mProgressDialog.show();
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.setCanceledOnTouchOutside(false);
                    Intent intent = new Intent(activity, TransactionService.class);
                    activity.startService(intent);

                }


                try {

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            //mProgressDialog.dismiss();
                            // Inclusion of MediaPlayer to start the Welcome note.
						/*MediaPlayer mediaPlayer = MediaPlayer.create(SplashScreenActivity.this, R.raw.welcome);
						mediaPlayer.start();*/

                        }
                    }, 200);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(customView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();

    }

    public static void showConfirmation_CheckBackLogDialog(final Activity activity) {
        final Dialog confirmationDialog = new Dialog(activity);

        LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = li.inflate(R.layout.dialog_check_backlog, null, false);

        final com.oasys.eshakti.views.RaisedButton mConNoButton;

        TextView mConfirmationHeadertextview;

        TextView mTarget_textview, mCompleted_textview, mPending_textview;
        TextView mTarget_values, mCompleted_values, mPending_values;

        mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.confirmationHeader_CheckBackLog);
        mConfirmationHeadertextview.setText(AppStrings.mCheckBackLog);
        mConNoButton = (com.oasys.eshakti.views.RaisedButton) customView.findViewById(R.id.fragment_Ok_button_checkBacklog);
        mConNoButton.setText(AppStrings.yes);

        mTarget_textview = (TextView) customView.findViewById(R.id.target_text);
        mTarget_values = (TextView) customView.findViewById(R.id.target_values);

        mCompleted_textview = (TextView) customView.findViewById(R.id.completed_text);
        mCompleted_values = (TextView) customView.findViewById(R.id.completed_values);

        mPending_textview = (TextView) customView.findViewById(R.id.pending_text);
        mPending_values = (TextView) customView.findViewById(R.id.pending_values);

        mTarget_textview.setText(AppStrings.mTarget);
        mCompleted_textview.setText(AppStrings.mCompleted);
        mPending_textview.setText(AppStrings.mPending);

        mConNoButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                confirmationDialog.dismiss();
            }
        });

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(customView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        confirmationDialog.show();

    }

    public static void showConfirmation_LogoutDialog(final Activity activity) {

        try {
            final Dialog confirmationDialog = new Dialog(activity);

            LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View customView = li.inflate(R.layout.dialog_new_logout, null, false);
            final TextView mHeaderView, mMessageView;

            mHeaderView = (TextView) customView.findViewById(R.id.dialog_Title_logout);
            mMessageView = (TextView) customView.findViewById(R.id.dialog_Message_logout);


            final ButtonFlat mConYesButton, mConNoButton;

            mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert_logout);
            mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert_logout);

            mConYesButton.setText(AppStrings.dialogOk);
            mConYesButton.setTypeface(LoginActivity.sTypeface);
            mConNoButton.setTypeface(LoginActivity.sTypeface);
            mConNoButton.setText(AppStrings.dialogNo);

            mHeaderView.setText(AppStrings.logOut);
            mHeaderView.setTypeface(LoginActivity.sTypeface);
            mHeaderView.setVisibility(View.INVISIBLE);

            if (MySharedPreference.readBoolean(activity, MySharedPreference.UNAUTH, false)) {
                mMessageView.setText(AppStrings.mUnAuth);
            } else if (MySharedPreference.readInteger(activity, MySharedPreference.NETWORK_MODE_FLAG, 0) > 0 && !MySharedPreference.readBoolean(activity, MySharedPreference.LOGOUT, false)) {
                MySharedPreference.writeBoolean(activity, MySharedPreference.LOGOUT, true);
                if (MySharedPreference.readInteger(activity, MySharedPreference.NETWORK_MODE_FLAG, 0) == 2) {
                    mMessageView.setText(AppStrings.mon_mode);
                } else if (MySharedPreference.readInteger(activity, MySharedPreference.NETWORK_MODE_FLAG, 0) == 1) {
                    mMessageView.setText(AppStrings.moff_mode);
                }
            } else if (MySharedPreference.readBoolean(activity, MySharedPreference.LOGOUT, false)) {
                mMessageView.setText(AppStrings.mAskLogout);
            }

            mMessageView.setTypeface(LoginActivity.sTypeface);
            mConYesButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    confirmationDialog.dismiss();

                    try {
                        if (MySharedPreference.readString(activity, MySharedPreference.SHG_ID, "") != null && MySharedPreference.readString(activity, MySharedPreference.SHG_ID, "").length() > 0) {
                            ListOfShg shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(activity, MySharedPreference.SHG_ID, ""));
                            if (shgDto != null && shgDto.getFFlag() != null && (shgDto.getFFlag().equals("1") || shgDto.getFFlag().equals("0"))) {
                                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                            }
                            TransactionTable.updateLoginflag(MySharedPreference.readString(activity, MySharedPreference.SHG_ID, ""));
                        }


                        if (mMessageView.getText().equals(AppStrings.mAskLogout))
                            MySharedPreference.writeBoolean(activity, MySharedPreference.LOGOUT, true);
                        MySharedPreference.writeBoolean(activity, MySharedPreference.SINGIN_DIFF, false);
                        MySharedPreference.writeString(activity, MySharedPreference.ANIMATOR_NAME, "");
                        //   MySharedPreference.writeString(context,MySharedPreference.USERNAME,"");
                        // MySharedPreference.writeString(activity, MySharedPreference.ANIMATOR_ID, "");
                        //MySharedPreference.writeString(activity, MySharedPreference.SHG_ID, "");
                        MySharedPreference.writeString(activity, MySharedPreference.CASHINHAND, "");
                        MySharedPreference.writeString(activity, MySharedPreference.CASHATBANK, "");
                        MySharedPreference.writeString(activity, MySharedPreference.LAST_TRANSACTION, "");

                        activity.startActivity(new Intent(GetExit.getExitIntent(activity)));
                        activity.finish();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });

            mConNoButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    confirmationDialog.dismiss();
                    MySharedPreference.writeBoolean(activity, MySharedPreference.LOGOUT, false);
                }
            });

            confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            confirmationDialog.setCanceledOnTouchOutside(false);
            confirmationDialog.setContentView(customView);
            confirmationDialog.setCancelable(false);
            confirmationDialog.show();

        } catch (
                Exception E)

        {
            E.printStackTrace();
        }

    }

    public static void showNotificationMessageDialog(final Activity activity) {
        final Dialog confirmationDialog = new Dialog(activity);

        LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = li.inflate(R.layout.dialog_notification, null, false);
        final Button mConYesButton;

        TextView mConfirmationHeadertextview;

        mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialogTextView_notification);

        mConYesButton = (Button) customView.findViewById(R.id.dialog_Yes_button_notification);

        mConYesButton.setText(AppStrings.yes);


        mConYesButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (confirmationDialog.isShowing() && confirmationDialog != null) {

                    confirmationDialog.dismiss();

                }

            }
        });

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(customView);
        confirmationDialog.setCancelable(false);
        confirmationDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        confirmationDialog.show();

    }

    public static void showAlertMessageDialog(final Activity activity) {
        final Dialog confirmationDialog = new Dialog(activity);

        LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = li.inflate(R.layout.dialog_alert_message, null, false);
        final Button mConYesButton;

        TextView mConfirmationHeadertextview;

        mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialogTextView_alert);

        mConYesButton = (Button) customView.findViewById(R.id.dialog_Yes_button_alert);

        mConYesButton.setText("EXIT");


        mConfirmationHeadertextview.setTextSize(18);

        mConYesButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (confirmationDialog.isShowing() && confirmationDialog != null) {

                    confirmationDialog.dismiss();
                    activity.finish();

                }

            }
        });

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(customView);
        confirmationDialog.setCancelable(false);
        confirmationDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        confirmationDialog.show();

    }

    public static void showNotificationMessage_AlertDialog(final Activity activity) {
        final Dialog confirmationDialog = new Dialog(activity);

        LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = li.inflate(R.layout.dialog_notification, null, false);
        final Button mConYesButton;

        TextView mConfirmationHeadertextview;
        final Handler handler = new Handler();

        mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialogTextView_notification);

        mConYesButton = (Button) customView.findViewById(R.id.dialog_Yes_button_notification);

        mConYesButton.setText(AppStrings.yes);


        mConYesButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (confirmationDialog.isShowing() && confirmationDialog != null) {

                    confirmationDialog.dismiss();


                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }


                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            handler.post(new Runnable() { // This
                                // thread
                                // runs
                                // in
                                // the
                                // UI
                                @Override
                                public void run() {

                                    // TODO Auto-generated method
                                    // stub


                                }
                            });
                        }
                    };
                    new Thread(runnable).start();

                }

            }
        });

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(customView);
        confirmationDialog.setCancelable(false);
        confirmationDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        confirmationDialog.show();

    }

    public static void showSendEMailAlertDialog(final Activity activity) {
        final Dialog confirmationDialog = new Dialog(activity);

        LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = li.inflate(R.layout.dialog_singin_diff, null, false);
        final ButtonFlat mConYesButton;
        final ButtonFlat mConNoButton;

        TextView mConfirmationHeadertextview;
        TextView mConfirmationTexttextview;

        mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialog_Title);
        mConfirmationTexttextview = (TextView) customView.findViewById(R.id.dialog_Message);

        mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert);
        mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert);
        mConYesButton.setText(AppStrings.dialogOk);

        mConNoButton.setText(AppStrings.dialogNo);

        // mConfirmationHeadertextview.setText("GPS settings");
        mConfirmationHeadertextview.setVisibility(View.INVISIBLE);

        mConfirmationTexttextview.setText("Do you want to send an email with database attachment?.");
        mConfirmationTexttextview.setTextSize(18);
        mConYesButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (confirmationDialog.isShowing() && confirmationDialog != null) {

                    confirmationDialog.dismiss();

                }

            }
        });

        mConNoButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (confirmationDialog.isShowing() && confirmationDialog != null) {
                    confirmationDialog.dismiss();
                }
            }
        });

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(customView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();

    }

    public static void showAlertTransAuditDialog(final Activity activity) {
        final Dialog confirmationDialog = new Dialog(activity);

        LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = li.inflate(R.layout.dialog_singin_diff, null, false);
        final ButtonFlat mConYesButton;
        final ButtonFlat mConNoButton;

        TextView mConfirmationHeadertextview;
        TextView mConfirmationTexttextview;

        mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialog_Title);
        mConfirmationTexttextview = (TextView) customView.findViewById(R.id.dialog_Message);

        mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert);
        mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert);
        mConYesButton.setText("YES");

        mConNoButton.setText("NO");

        mConfirmationHeadertextview.setText("CONFIRMATION");
        // Your offline data is still available at local db, So you must move
        // data from db to online server!!!
        mConfirmationTexttextview
                .setText("Transaction audit has been completed and changes waiting for your approval.");
        mConYesButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (confirmationDialog.isShowing() && confirmationDialog != null) {

                    confirmationDialog.dismiss();


                }

            }
        });

        mConNoButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (confirmationDialog.isShowing() && confirmationDialog != null) {

                    confirmationDialog.dismiss();

                }
            }
        });
        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(customView);
        confirmationDialog.setCancelable(false);
        confirmationDialog.show();

    }

}
package com.oasys.eshakti.OasysUtils;

import android.content.Context;
import android.content.Intent;

import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.activity.ExitActivity;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.database.TransactionTable;


public class GetExit {

    /**
     * Call to Exit
     *
     * @param context context of the view
     * @return intent to exit
     **/

    public static Intent getExitIntent(Context context) {

        Intent intent = null;

        try {

            try {
                if (MySharedPreference.readString(context, MySharedPreference.SHG_ID, "") != null && MySharedPreference.readString(context, MySharedPreference.SHG_ID, "").length() > 0) {
                    ListOfShg shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(context, MySharedPreference.SHG_ID, ""));
                    if (shgDto != null && shgDto.getFFlag() != null && (shgDto.getFFlag().equals("1") || shgDto.getFFlag().equals("0"))) {
                        SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                    }
                    TransactionTable.updateLoginflag(MySharedPreference.readString(context, MySharedPreference.SHG_ID, ""));
                }
                MySharedPreference.writeBoolean(context, MySharedPreference.LOGOUT, true);
                MySharedPreference.writeBoolean(context, MySharedPreference.SINGIN_DIFF, false);
                MySharedPreference.writeString(context, MySharedPreference.ANIMATOR_NAME, "");
                //   MySharedPreference.writeString(context,MySharedPreference.USERNAME,"");
                //      MySharedPreference.writeString(context, MySharedPreference.ANIMATOR_ID, "");
                //   MySharedPreference.writeString(context, MySharedPreference.SHG_ID, "");
                MySharedPreference.writeString(context, MySharedPreference.CASHINHAND, "");
                MySharedPreference.writeString(context, MySharedPreference.CASHATBANK, "");
                MySharedPreference.writeString(context, MySharedPreference.LAST_TRANSACTION, "");


                Intent exitIntent = new Intent(context, ExitActivity.class);
                intent = exitIntent;
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return intent;

    }

}

package com.oasys.eshakti.OasysUtils;

public class AadharnumberUtils {

    public static boolean isValidAadhar(String phone) {
        boolean check = false;
        boolean _SameNumberCheck = false;

        if (phone.length() == 12) {

            if (phone.equals("000000000000")) {
                _SameNumberCheck = false;
            } else if (phone.equals("111111111111")) {
                _SameNumberCheck = false;
            } else if (phone.equals("222222222222")) {
                _SameNumberCheck = false;
            } else if (phone.equals("333333333333")) {
                _SameNumberCheck = false;
            } else if (phone.equals("444444444444")) {
                _SameNumberCheck = false;
            } else if (phone.equals("555555555555")) {
                _SameNumberCheck = false;
            } else if (phone.equals("666666666666")) {
                _SameNumberCheck = false;
            } else if (phone.equals("777777777777")) {
                _SameNumberCheck = false;
            } else if (phone.equals("888888888888")) {
                _SameNumberCheck = false;
            } else if (phone.equals("999999999999")) {
                _SameNumberCheck = false;
            } else {
                _SameNumberCheck = true;
            }

            if (!_SameNumberCheck) {
                check = false;
            }
            else
            {
                check= true;
            }

        } else {
            check = false;
        }
        if (phone.equals("")) {
            check = true;
        }

        return check;
    }
}

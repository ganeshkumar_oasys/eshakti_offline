package com.oasys.eshakti.OasysUtils;

public class RegionalLanguage_gujarathi {
	public RegionalLanguage_gujarathi() {
		// TODO Auto-generated constructor stub
	}

	public static void RegionalStrings() {

		AppStrings.LoginScreen = "પ્રવેશ સ્ક્રીન";
		AppStrings.userName = "મોબાઇલ નંબર";
		AppStrings.passWord = "પાસવર્ડ";
		AppStrings.transaction = "જાતનાં";
		AppStrings.reports = "અહેવાલો";
		AppStrings.profile = "પ્રોફાઇલ";
		AppStrings.meeting = "બેઠક";
		AppStrings.settings = "સેટિંગ્સ";
		AppStrings.help = "મદદ";
		AppStrings.Attendance = "હાજરી";
		AppStrings.MinutesofMeeting = "બેઠક ની મિનિટ";
		AppStrings.savings = "બચત";
		AppStrings.InternalLoanDisbursement = "લોન નુ વિતરન";
		AppStrings.expenses = "ખર્ચ";
		AppStrings.cashinhand = "હાથમાં રોકડ :  ₹ ";
		AppStrings.cashatBank = "બેંક પર રોકડ :  ₹ ";
		AppStrings.memberloanrepayment = "સભ્ય લોન ચુકવણી";
		AppStrings.grouploanrepayment = "ગ્રુપ લોન ચુકવણી";
		AppStrings.income = "આવક";
		AppStrings.bankTransaction = "બેંક જાતનાં";
		AppStrings.fixedDeposit = "ફિક્સ્ડ ડિપોઝિટ";
		AppStrings.otherincome = "અન્ય આવક";
		AppStrings.subscriptioncharges = "ફાળો";
		AppStrings.penalty = "દંડ";
		AppStrings.donation = "દાન";
		AppStrings.federationincome = "ફેડરેશન ની આવક";// "ફેડરેશન આવક";
		AppStrings.date = "તારીખ";
		AppStrings.passwordchange = "પાસવર્ડ બદલો";
		AppStrings.listofexpenses = "ખર્ચ યાદી";
		AppStrings.transport = "પરિવહન";
		AppStrings.snacks = "ચા";
		AppStrings.telephone = "ટેલિફોન";
		AppStrings.stationary = "સ્ટેશનરી";
		AppStrings.otherExpenses = "અન્ય ખર્ચ";
		AppStrings.federationIncomePaid = "ફેડરેશન આવક (ચૂકવણી)";
		AppStrings.bankInterest = "બેન્ક વ્યાજ";
		AppStrings.bankExpenses = "બેંક ખર્ચ";
		AppStrings.bankrepayment = "બેંક ચુકવણી";
		AppStrings.total = "કુલ";
		AppStrings.summary = "સારાંશ";
		AppStrings.savingsSummary = "બચત સારાંશ";
		AppStrings.loanSummary = "લોન સારાંશ";
		AppStrings.lastMonthReport = "માસિક અહેવાલ";
		AppStrings.Memberreports = "સભ્ય અહેવાલો";
		AppStrings.GroupReports = "જૂથ અહેવાલો";
		AppStrings.transactionsummary = "બેંક વ્યવહાર સારાંશ";
		AppStrings.balanceSheet = "સરવૈયા";
		AppStrings.balanceSheetHeader = "બેલેન્સ શીટ ક્યાર થી";
		AppStrings.trialBalance = "ટ્રાયલ બેલેન્સ";
		AppStrings.outstanding = "બાકી";
		AppStrings.totalSavings = "કુલ બચત";
		AppStrings.groupSavingssummary = "જૂથ બચત સારાંશ";
		AppStrings.groupLoansummary = "જૂથ લોન સારાંશ";
		AppStrings.amount = "રકમ";
		AppStrings.principleAmount = "રકમ";
		AppStrings.interest = "વ્યાજ";
		AppStrings.groupLoan = "સમુહ લોન";
		AppStrings.balance = "બાકી";
		AppStrings.Name = "નામ";
		AppStrings.Repaymenybalance = "બાકી";
		AppStrings.details = "વિગતો";
		AppStrings.payment = "ચુકવણી";
		AppStrings.receipt = "આવક";
		AppStrings.FromDate = "તારીખથી";
		AppStrings.ToDate = "આજ સુધી";
		AppStrings.fromDate = "થી";
		AppStrings.toDate = "માટે";
		AppStrings.OldPassword = "જુનો પાસવર્ડ";
		AppStrings.NewPassword = "નવો પાસવર્ડ";
		AppStrings.ConfirmPassword = "પાસવર્ડ ખાતરી કરો";
		AppStrings.agentProfile = "એજન્ટ પ્રોફાઇલ";
		AppStrings.groupProfile = "ગ્રુપ પ્રોફાઇલ";
		AppStrings.bankDeposit = "બેંક ડિપોઝિટ";
		AppStrings.withdrawl = "ઉપાડો લેવો";
		AppStrings.OutstandingAmount = "બાકી";
		AppStrings.GroupLogin = "જૂથ નો લૉગિન";
		AppStrings.AgentLogin = "એજન્ટ નો પ્રવેશ";
		AppStrings.AboutLicense = "લાઇસેંસ";
		AppStrings.contacts = "સંપર્કો";
		AppStrings.contactNo = "સંપર્ક નંબર";
		AppStrings.cashFlowStatement = "કેશ ફ્લો સ્ટેટમેન્ટ";
		AppStrings.IncomeExpenditure = "આવક ખર્ચ";
		AppStrings.transactionCompleted = "ટ્રાન્ઝેક્શન પૂર્ણ";
		AppStrings.loginAlert = "વપરાશકર્તા નામ અને પાસવર્ડ ખોટો છે";
		AppStrings.loginUserAlert = "વપરાશકર્તા વિગતો આપો";
		AppStrings.loginPwdAlert = "પાસવર્ડ વિગતો આપો";
		AppStrings.pwdChangeAlert = "પાસવર્ડ સફળતાપૂર્વક બદલાઈ ગયેલ છે";
		AppStrings.pwdIncorrectAlert = "સાચો પાસવર્ડ દાખલ કરો";
		AppStrings.groupRepaidAlert = "તમારી બાકી રકમ તપાસો";//"તમારી રકમ તપાસો";
		AppStrings.cashinHandAlert = "હાથમાં રોકડ તપાસો";
		AppStrings.cashatBankAlert = "બેંક પર રોકડ તપાસો";
		AppStrings.nullAlert = "રોકડ ની વિગતો આપો";
		AppStrings.DepositnullAlert = "તમામ રોકડ ની વિગતો આપો";
		AppStrings.MinutesAlert = "કૃપા કરીને ઉપર થી કોઈ એક ને પસંદ કરો";
		AppStrings.afterDate = "આ તારીખ પછી તારીખ પસંદ કરો";
		AppStrings.last5Transaction = "છેલ્લા પાંચ જાતનાં";
		AppStrings.internetError = " તમારું ઇન્ટરનેટ કનેક્શન ને તપાસો કરો";
		AppStrings.lastTransactionDate = "છેલ્લા જાતનાં તારીખ";
		AppStrings.yes = "ઠીક છે";
		AppStrings.credit = "ઉધાર";
		AppStrings.debit = "ડેબિટ";
		AppStrings.memberName = "સભ્ય નામ";
		AppStrings.OutsatndingAmt = "ઉત્કૃષ્ટ";
		AppStrings.calFromToDateAlert = "બંને તારીખ સિલેક્ટ કરો";
		AppStrings.selectedDate = "પસંદ કરેલ તારીખ";
		AppStrings.calAlert = "વિવિધ તારીખ પસંદ કરો";
		AppStrings.dialogMsg = "તમે આ તારીખ સાથે ચાલુ રાખવા માંગો છો";
		AppStrings.dialogOk = "હા";
		AppStrings.dialogNo = "ના";

		AppStrings.aboutYesbooks = "EShakti અથવા એસએચજી અંકરૂપણ અમારા માનનીય વડાપ્રધાન નિવેદન સાથે વાક્ય માં નાબાર્ડ સૂક્ષ્મ ક્રેડિટ અને નવીનીકરણ વિભાગ એક પહેલ છે"
				+ " 'અમે ઇલેક્ટ્રોનિક ડિજિટલ ભારત સ્વપ્ન સાથે ખસેડો ...'. ડિજિટલ ભારત ભારત સરકાર રૂ 1.13 લાખ કરોડ પહેલ સરકારી વિભાગો "
				+ "અને ભારતના લોકો સંકલિત અને અસરકારક શાસન તેની ખાતરી કરવા માટે છે. તે ડિજિટલ સત્તા સમાજ અને જ્ઞાન અર્થતંત્ર ભારત પરિવર્તન છે.";
		AppStrings.groupList = "પસંદ કરો જૂથ";// GROUP LIST
		AppStrings.login = "   પ્રવેશ   ";
		AppStrings.submit = "સબમિટ";
		AppStrings.edit = "સંપાદિત કરો";
		AppStrings.bankBalance = "બેંક બેલેન્સ";
		AppStrings.balanceAsOn = " બાકી તારીખ મા : ";
		AppStrings.savingsBook = " બચત રકમ ચોપડી પ્રમાણે : ";
		AppStrings.savingsBank = "  બચત રકમ બેંક પ્રમાણે : ";
		AppStrings.difference = " તફાવત : ";
		AppStrings.loanOutstandingBook = " બાકી રકમ ચોપડી પ્રમાણે : ";
		AppStrings.loanOutstandingBank = " બાકી રકમ બેંક પ્રમાણે : ";
		AppStrings.values = new String[] { "મૂળ (ગામ).",

				"બધા સભ્યો હાજર છે.",

				"બચત રકમ એકત્રિત.",

				"વ્યક્તિગત લોન ચુકવણી અંગે ચર્ચા.",

				"ચુકવણી રકમ અને વ્યાજ સંગ્રહિત.",

				"તાલીમ વિશે ચર્ચા.",

				"બેન્ક લોન મેળવવા નિર્ણય લીધો.",

				"સામાન્ય જોવાઈ અંગે ચર્ચા.",

				"સ્વચ્છતા વિશે ચર્ચા.",

				"શિક્ષણના મહત્વ વિશે ચર્ચા." };

		AppStrings.January = "જાન્યુઆરી";
		AppStrings.February = "ફેબ્રુઆરી";
		AppStrings.March = "કુચ";
		AppStrings.April = "એપ્રિલ";
		AppStrings.May = "મે";
		AppStrings.June = "જૂન";
		AppStrings.July = "જુલાઈ";
		AppStrings.August = "ઓગસ્ટ";
		AppStrings.September = "સપ્ટેમ્બર";
		AppStrings.October = "ઓક્ટોબર";
		AppStrings.November = "નવેમ્બર";
		AppStrings.December = "ડિસેમ્બર";
		AppStrings.uploadPhoto = "અપલોડ કરો ફોટો";
		AppStrings.Il_Disbursed = "આંતિરક લોન આપી";
		AppStrings.IL_Loan = "આંતિરક લોન";
		AppStrings.Il_Repaid = "આંતિરક લોન પાછી";
		AppStrings.offlineTransactionCompleted = "તમારા ઑફલાઇન જાતનાં સફળતાપૂર્વક સાચવવામાં આવી છે";
		AppStrings.offlineTransactionreports = "ઓફલાઇન મોડમાં વ્યવહાર રિપોર્ટ્સ";
		AppStrings.offlineTransaction = "ઓફલાઇન મોડમાં વ્યવહારો";
		AppStrings.disconnectInternet = "ઓફલાઇન મોડમાં સેવા ચાલુ રાખવા કૃપા કરીને તમારા ઇન્ટરનેટ ડિસ્કનેક્ટ કરો";
		AppStrings.Dashboard = "ડેશબોર્ડ";
		AppStrings.bankTransactionSummary = "બેંક જાતનાં સારાંશ";
		AppStrings.BL_Disbursed = "તેમાં વિતરિત";
		AppStrings.BL_Repaid = "પાછી";
		AppStrings.auditing = "ઓડિટીંગ";
		AppStrings.training = "તાલીમ";
		AppStrings.auditor_Name = "ઓડિટર નુ નામ";
		AppStrings.auditing_Date = "ઓડિટીંગ ની તારીખ";
		AppStrings.training_Date = "તાલીમ ની તારીખ";
		AppStrings.auditing_Period = "ઓડિટ સમયગાળો પસંદ કરો";
		AppStrings.training_types = new String[] { "આરોગ્ય માટે જાગૃતિ", "મુનીમ ની જાગૃતિ", "આજીવિકા માટે જાગૃતિ",
				"સામાજિક જાગરૂકતા", "શૈક્ષણિક જાગૃતિ" };
		AppStrings.changeLanguage = "ભાષા બદલો";
		AppStrings.interestRepayAmount = "વર્તમાન કારણે";
		AppStrings.interestRate = "ઈન્ટરેસ્ટ રેટ";
		AppStrings.savingsAmount = "બચત રકમ";
		AppStrings.noGroupLoan_Alert = " ઉપલબ્ધ ગ્રુપ લોન ની સન્ખીયા.";
		AppStrings.confirmation = "પુષ્ટિ";
		AppStrings.logOut = "લૉગ આઉટ";
		AppStrings.fromDateAlert = "તારીખથી આપો";
		AppStrings.toDateAlert = "તારીખ કરવા માટે પ્રદાન કરો";
		AppStrings.auditingDateAlert = "કૃપા કરીને ઓડિટીંગ ની તારીખ આપો";
		AppStrings.auditorAlert = "કૃપા કરીને ઑડિટર નુ નામ આપો";
		AppStrings.nullDetailsAlert = "તમામ વિગતો આપો";
		AppStrings.trainingDateAlert = "તાલીમ તારીખ આપો";
		AppStrings.trainingTypeAlert = "ઓછામાં ઓછા એક તાલીમ પ્રકાર પસંદ કરો";
		AppStrings.offline_ChangePwdAlert = " તમે ઇન્ટરનેટ વિના પાસવર્ડ બદલી શક્શો નહી";
		AppStrings.transactionFailAlert = "તમારા વ્યવહાર નિષ્ફળ છે";
		AppStrings.voluntarySavings = "સ્વૈચ્છિક બચત";
		AppStrings.tenure = "મુદત";
		AppStrings.purposeOfLoan = "લોન નો ઉદ્દેશ્ય";
		AppStrings.chooseLabel = "લોન ને પસંદ કરો";
		AppStrings.Tenure_POL_Alert = "કૃપા કરીને લોન નો ઉદ્દેશ્ય અને મુદત સમયગાળા દાખલ કરો";
		AppStrings.interestSubvention = "વ્યાજ આર્થિક સહાય";
		AppStrings.adminAlert = "તો એડમિન ને સંપર્ક કરો ";
		AppStrings.userNotExist = "લૉગિન ની વિગતો અસ્તિત્વમાં નથી. કૃપા કરીને ઓનલાઇન પ્રવેશ ચાલુ રાખો.";
		AppStrings.tryLater = "થોડીવાર પછી પ્રયાસ કરો";
		AppStrings.offlineDataAvailable = "ઓફલાઇન માહિતી ઉપલબ્ધ. કૃપા કરીને લૉગઆઉટ કરો અને ફરીથી પ્રયાસ કરો.";
		AppStrings.choosePOLAlert = "લોન નો ઉદ્દેશ્ય પસંદકરો";
		AppStrings.InternalLoan = "આંતિરક લોન";
		AppStrings.TrainingTypes = "તાલીમ ના પ્રકાર";
		AppStrings.uploadInfo = "સભ્ય ની વિગતો";
		AppStrings.uploadAadhar = " આધાર ને અપલોડ કરો";
		AppStrings.month = "-- મહિનો પસંદ કરો--";
		AppStrings.year = "-- વર્ષ પસંદ કરો --";
		AppStrings.monthYear_Alert = "કૃપા કરીને મહિનો અને વર્ષ પસંદ કરો.";
		AppStrings.chooseLanguage = "ભાષા પસંદ કરો";
		AppStrings.autoFill = "આપોઆપ ભરશે";
		AppStrings.viewAadhar = " આધાર ને જુઓ";
		AppStrings.viewPhoto = "ફોટો ને જુઓ";
		AppStrings.uploadAlert = "તમે આધાર અને ફોટો વિના ઇન્ટરનેટ કનેક્શન જોઇ નહી શકો";
		AppStrings.upload = "અપલોડ";
		AppStrings.view = "જુઓ";
		AppStrings.showAadharAlert = "કૃપા કરીને તમારો આધાર કાર્ડ બતાવો.";
		AppStrings.deactivateAccount = "ખાતું નિષ્ક્રિય";
		AppStrings.deactivateAccount_SuccessAlert = "તમારું એકાઉન્ટ સફળતાપૂર્વક નિષ્ક્રિય કરવામાં આવ્યું છે";
		AppStrings.deactivateAccount_FailAlert = "તમારું ખાતું નિષ્ક્રિય કરવામાં નિષ્ફળ થયા છે";
		AppStrings.deactivateNetworkAlert = "તમે ઇન્ટરનેટ કનેક્શન વગર તમારું ખાતું નિષ્ક્રિય કરી શક્શો નહી";
		AppStrings.offlineReports = "ઓફલાઇન મોડમાં અહેવાલો";
		AppStrings.registrationSuccess = "તમને સફળતાપૂર્વક રજીસ્ટર કર્યા છે";
		AppStrings.reg_MobileNo = "રજીસ્ટર મોબાઇલ નંબર દાખલ કરો";
		AppStrings.reg_IMEINo = "પરવાનગી અસ્વીકાર.";
		AppStrings.reg_BothNo = "પરવાનગી અસ્વીકાર.";
		AppStrings.reg_IMEIUsed = "પરવાનગી અસ્વીકાર. ઉપકરણ ખોટી જોડણી";
		AppStrings.signInAsDiffUser = "અલગ અલગ વપરાશકર્તાને તરીકે સાઇન ઇન કરો";
		AppStrings.incorrectUserNameAlert = "ખોટું વપરાશકર્તા  નુ નામ";

		AppStrings.noofflinedatas = "કોઈ ઑફલાઇન વ્યવહાર નથી";
		AppStrings.uploadprofilealert = "તમારા પોતાના વિગતો અપલોડ કરો";
		AppStrings.monthYear_Warning = "યોગ્ય મહિનો અને વર્ષ પસંદ કરો";
		AppStrings.of = " કોનુ ";
		AppStrings.previous_DateAlert = "અગાઉની તારીખ ને પસંદ કરો ";
		AppStrings.nobanktransactionreportdatas = "કોઈ બેંક વ્યવહાર અહેવાલો માહિતી";

		AppStrings.mDefault = "ચૂક";
		AppStrings.mTotalDisbursement = "કુલ વિતરન ";
		AppStrings.mInterloan = "વ્યક્તિગત લોન ની ચુકવણી";
		AppStrings.mTotalSavings = "કુલ સંગ્રહો";
		AppStrings.mCommonNetworkErrorMsg = "તમારું નેટવર્ક કનેક્શન ને તપાસો કરો";
		AppStrings.mLoginAlert_ONOFF = "કૃપા કરીને ફરીથી ઇંટરનેટ કનેક્શન નો ઉપયોગ કરી ને લૉગિન કરો ";
		AppStrings.changeLanguageNetworkException = "કૃપા કરીને લૉગઆઉટ કરો , ફરીથી પ્રયાસ કરો ઇન્ટરનેટ કનેક્શનનો ઉપયોગ કરીને લૉગિન કર્વા";
		AppStrings.photouploadmsg = "ફોટો સફળતાપૂર્વક અપલોડ થયો";
		AppStrings.loginAgainAlert = "પૃષ્ઠભૂમિ માહિતી હજી લોડ કરી રહ્યું છે કેટલાક સમય માટે કૃપા કરીને રાહ જુઓ.";//"કૃપા કરીને ફરીથી લૉગિન કરો";

		/* Interloan Disbursement menu */
		AppStrings.mInternalInterloanMenu = "આંતિરક લોન";
		AppStrings.mInternalBankLoanMenu = "બેંક લોન";
		AppStrings.mInternalMFILoanMenu = "MFI લોન";
		AppStrings.mInternalFederationMenu = "ફેડરેશન લોન";

		AppStrings.bankName = "બેંકનું નામ";
		AppStrings.mfiname = "MFI નામ";
		AppStrings.loanType = "લોન ના પ્રકાર";
		AppStrings.bankBranch = "બેંક ની શાખા";
		AppStrings.installment_type = "હપતો ના પ્રકાર";
		AppStrings.NBFC_Name = "એનબીએફસી નુ નામ";
		AppStrings.Loan_Account_No = "લોન એકાઉન્ટ નંબર";
		AppStrings.Loan_Sanction_Amount = "લોન ની મંજૂરી રકમ";
		AppStrings.Loan_Sanction_Date = "લોન મંજૂરી ની તારીખ";
		AppStrings.Loan_Disbursement_Amount = "લોન વિતરન ની રકમ";

		AppStrings.Loan_Disbursement_Date = "લોન વિતરન ની તારીખ";
		AppStrings.Loan_Tenure = "લોન મુદત";
		AppStrings.Subsidy_Amount = "સબસીડીની રાશી";
		AppStrings.Subsidy_Reserve_Fund = "સબસિડી રિઝર્વ રકમ";
		AppStrings.Interest_Rate = "વ્યાજ દર";

		AppStrings.mMonthly = "માસિક";
		AppStrings.mQuarterly = "ત્રિમાસિક";
		AppStrings.mHalf_Yearly = "અર્ધ વાર્ષિક";
		AppStrings.mYearly = "વાર્ષિક";
		AppStrings.mAgri = "કૃષિ";
		AppStrings.mMSME = "એમએસએમઇ";
		AppStrings.mOthers = "અન્ય";

		AppStrings.mMFI_NAME_SPINNER = "MFI નામ";
		AppStrings.mMFINabfins = "નેબફિન્સ";
		AppStrings.mMFIOthers = "અન્ય";
		AppStrings.mInternalTypeLoan = "આંતિરક લોન";
		AppStrings.mPervious = "અગાઉના";
		AppStrings.mNext = "આગામી";
		AppStrings.mPasswordUpdate = "તમારો પાસવર્ડ સફળતાપૂર્વક અપડેટ થયો ";

		AppStrings.mFixedDepositeAmount = "ફિક્સ્ડ ડિપોઝિટ રાશી";
		AppStrings.mFedBankName = "ફેડરેશન નુ નામ";
		AppStrings.mLoanSanc_loanDist = "કૃપા કરીને લોન વિતરન ની રકમ ને તપાસો ";
		AppStrings.mStepWise_LoanDibursement = "આંતિરક લોન નુ વિતરન";

		AppStrings.mExpense_meeting = "બેઠક નો ખર્ચ";
		AppStrings.mSubmitbutton = "ઠીક છે";
		AppStrings.mNewUserSignup = "નોંધણી";
		AppStrings.mMobilenumber = "મોબાઇલ નંબર";
		AppStrings.mIsAadharAvailable = "આધાર ની માહિતી અપડેટ નથી";
		AppStrings.mSavingsTransaction = "બચત વ્યવહાર";
		AppStrings.mBankCharges = "બેંક ખર્ચ";
		AppStrings.mCashDeposit = "રોકડ થાપણ";
		AppStrings.mLimit = "લોન ની મર્યાદા";

		AppStrings.mAccountToAccountTransfer = "એકાઉન્ટ ટ્રાન્સફર એકાઉન્ટ";
		AppStrings.mTransferFromBank = "બેંક પાસેથી";
		AppStrings.mTransferToBank = "બેન્ક";
		AppStrings.mTransferAmount = "ટ્રાન્સફરની રકમ";
		AppStrings.mTransferCharges = "ટ્રાન્સફર ખર્ચ";
		AppStrings.mTransferSpinnerFloating = "બેન્ક";
		AppStrings.mTransferNullToast = "કૃપા કરીને તમામ વિગતો";
		AppStrings.mAccToAccTransferToast = "તમે માત્ર એક બેંક એકાઉન્ટ છે";

		/* Loan Account */
		AppStrings.mLoanaccHeader = "લોન એકાઉન્ટ";
		AppStrings.mLoanaccCash = "રોકડ";
		AppStrings.mLoanaccBank = "બેંક";
		AppStrings.mLoanaccWithdrawal = "વિડ્રોવલઃ";
		AppStrings.mLoanaccExapenses = "ખર્ચ";
		AppStrings.mLoanaccSpinnerFloating = "લોન એકાઉન્ટ બેંક";
		AppStrings.mLoanaccCash_BankToast = "કૃપા કરીને પસંદ કરો રોકડ અથવા બેંક";
		AppStrings.mLoanaccNullToast = "કૃપા કરીને તમામ વિગતો";
		AppStrings.mLoanaccBankNullToast = "કૃપા કરીને પસંદ કરો એક બેંકનું નામ";

		AppStrings.mBankTransSavingsAccount = "બચત ખાતું";
		AppStrings.mBankTransLoanAccount = "લોન એકાઉન્ટ";

		AppStrings.mLoanAccType = "પ્રકાર";
		AppStrings.mLoanAccBankAmountAlert = "તપાસો કરો બેંક બેલેન્સ";

		AppStrings.mCashAtBank = "બેંક પર રોકડ";
		AppStrings.mCashInHand = "હાથમાં રોકડ";
		AppStrings.mShgSeedFund = "SHG સંતાન ફંડ";
		AppStrings.mFixedAssets = "ચોક્કસ સંપતી, નક્કી કરેલી સંપતી";
		AppStrings.mVerified = "ચકાસણી";
		AppStrings.mConfirm = "ખાતરી";
		AppStrings.mProceed = "આગળ વધો";
		AppStrings.mDialogAlertText = "તમારા ઓપનિંગ બેલેન્સ datas સફળતાપૂર્વક અપડેટ કરવામાં આવે છે, હવે તમે આગળ વધી શકે";
		AppStrings.mBalanceSheetDate = "બેલેન્સ શીટ પર અપલોડ કરવામાં આવી છે";
		AppStrings.mCheckMonthlyEntries = "અગાઉના મહિના માટે આ બોલ પર કોઈ પ્રવેશો, જેથી તમે અગાઉના મહિના માટે datas મૂકવા જોઇએ";
		AppStrings.mTermLoanOutstanding = "OUTSTANDING";
		AppStrings.mCashCreditOutstanding = "OUTSTANDING";
		AppStrings.mGroupLoanOutstanding = "ગ્રુપ લોન બાકી";
		AppStrings.mMemberLoanOutstanding = "સભ્ય લોન બાકી";
		AppStrings.mContinueWithDate = "તમે આ તારીખ બદલવા માંગો છો નથી";
		AppStrings.mCheckList = "તપાસ યાદી";
		AppStrings.mMicro_Credit_Plan = "માઇક્રો ક્રેડિટ યોજના";
		AppStrings.mAadharCard_Invalid = "તમારા આધાર કાર્ડ અમાન્ય છે";
		AppStrings.mIsNegativeOpeningBalance = "નકારાત્મક બેલેન્સ સુધારવું";
		AppStrings.mLoginDetailsDelete = "અગાઉના લૉગિન વિગતો કાઢી નાખવામાં આવશે. તમે માહિતી કાઢી નાખવા માંગો છો?";
		AppStrings.mVerifyGroupAlert = "તમે ચકાસવા માટે તમારું ઑનલાઇન હોવું આવશ્યક";
		AppStrings.next = "આગામી";
		AppStrings.mNone = "કંઈ";
		AppStrings.mSelect = "પસંદ";
		AppStrings.mAnimatorName = "એનિમેટર નામ";
		AppStrings.mSavingsAccount = "બચત ખાતું";
		AppStrings.mAccountNumber = "ખાતા નંબર";
		AppStrings.mLoanAccountAlert = "ઑફલાઇન મોડ લોન એકાઉન્ટ સ્થાનાંતરિત કરી શકતાં નથી";
		AppStrings.mSeedFund = "બીજ ફંડ";
		AppStrings.mLoanTypeAlert = "મહેરબાની કરી પસંદ કરો એક લોન એકાઉન્ટ પ્રકાર";
		AppStrings.mOutstandingAlert = "તપાસો કૃપા કરીને તમારી બાકી રકમ";
		AppStrings.mLoanAccTransfer = "લોન એકાઉન્ટ ટ્રાન્સફર";
		AppStrings.mLoanName = "લોન નામ";
		AppStrings.mLoanId = "લોન ID";
		AppStrings.mLoanDisbursementDate = "લોન ચુકવણી તારીખ";
		AppStrings.mAdd = "ઉમેરો";
		AppStrings.mLess = "ઓછી";
		AppStrings.mRepaymentWithInterest = "ચુકવણી (વ્યાજ સાથે)";
		AppStrings.mCharges = "ખર્ચ";
		AppStrings.mExistingLoan = "હાલના લોન";
		AppStrings.mNewLoan = "નવી લોન";
		AppStrings.mIncreaseLimit = "વધારો મર્યાદા";
		AppStrings.mAvailableLimit = "ઉપલબ્ધ મર્યાદા";
		AppStrings.mDisbursementDate = "ચુકવણી તારીખ";
		AppStrings.mDisbursementAmount = "ચુકવણી રકમ";
		AppStrings.mSanctionAmount = "મંજૂરી રકમ";
		AppStrings.mBalanceAmount = "બાકીની રકમ";
		AppStrings.mMemberDisbursementAmount = "સભ્ય ચુકવણી રકમ";
		AppStrings.mLoanDisbursementFromLoanAcc = "લોન એકાઉન્ટ માંથી લોન ચુકવણી";
		AppStrings.mLoanDisbursementFromSbAcc = "એસ.બી. એકાઉન્ટ માંથી લોન ચુકવણી";
		AppStrings.mRepaid = "પાછી";
		AppStrings.mCheckBackLog = "ભરાવો ચકાસી";
		AppStrings.mTarget = "લક્ષ્ય";
		AppStrings.mCompleted = "પૂર્ણ";
		AppStrings.mPending = "બાકી";
		AppStrings.mAskLogout = "તમે લૉગઆઉટ કરવા માંગો છો ?.";
		AppStrings.mCheckDisbursementAlert = "તમારા વહેંચણી રકમ ચેક કરો";
		AppStrings.mCheckbalanceAlert = "તમારું બેલેન્સ રકમ ચેક કરો";
		AppStrings.mVideoManual = "વિડિઓ મેન્યુઅલ";
		AppStrings.mPdfManual = "પીડીએફ માર્ગદર્શિકા";
		AppStrings.mAmountDisbursingAlert = "તમે સભ્યોને ચુકવણી વિના ચાલુ રાખવા માંગો છો ?.";
		AppStrings.mOpeningDate = "ઉદઘાટન તારીખ";
		AppStrings.mCheckLoanSancDate_LoanDisbDate = "તમારી લોન મંજૂરી તારીખ અને લોન વહેંચણી તારીખ ચેક કરો.";
		AppStrings.mLoanDisbursementFromRepaid = "સીસી લોન ઉપાડ";
		AppStrings.mExpense = "ખર્ચ";
		AppStrings.mBeforeLoanPay = "લોનની ચૂકવણી પહેલાં";
		AppStrings.mLoans = "લોન્સ";
		AppStrings.mSurplus = "ફાજલ";
		AppStrings.mPOLAlert = "કૃપા કરીને લોનનો હેતુ દાખલ કરો";
		AppStrings.mCheckLoanAmounts = "કૃપા કરીને તમારી લોનની મંજૂરી રકમ અને લોનની વહેંચણીની રકમ તપાસો";
		AppStrings.mCheckLoanSanctionAmount = "તમારી લોન મંજૂરી રકમ દાખલ કરો";
		AppStrings.mCheckLoanDisbursementAmount = "તમારી લોનની વહેંચણી રકમ દાખલ કરો";
		AppStrings.mIsPasswordSame = "કૃપા કરીને તમારા જૂના પાસવર્ડને તપાસો અને નવો પાસવર્ડ એકસરખા ન હોવો જોઈએ.";
		AppStrings.mSavings_Banktransaction = "બચત - બેંક વ્યવહાર";
		AppStrings.mCheckBackLogOffline = "ઓફલાઇન તપાસ બૅકલિગ જોઈ શકતા નથી";
		AppStrings.mCredit = "ક્રેડિટ";
		AppStrings.mDebit = "ડેબિટ";
		AppStrings.mCheckFixedDepositAmount = "કૃપા કરી તમારી ફિક્સ્ડ ડિપોઝિટની રકમ તપાસો.";
		AppStrings.mCheckDisbursementDate = "કૃપા કરીને તમારી લોનની વહેંચણીની તારીખ તપાસો";
		AppStrings.mSendEmail = "EMAIL";
		AppStrings.mTotalInterest = "કુલ રસ";
		AppStrings.mTotalCharges = "કુલ ખર્ચ";
		AppStrings.mTotalRepayment = "કુલ ચુકવણી";
		AppStrings.mTotalInterestSubventionRecevied = "કુલ વ્યાજની સબવેશન મળ્યા";
		AppStrings.mTotalBankCharges = "કુલ બેંક ખર્ચ";
		AppStrings.mDone = "DONE";
		AppStrings.mPayment = "ચુકવણી";
		AppStrings.mCashWithdrawal = "રોકડ ઉપાડ";
		AppStrings.mFundTransfer = "ફંડ ટ્રાન્સફર";
		AppStrings.mDepositAmount = "ડિપોઝિટ રકમ";
		AppStrings.mWithdrawalAmount = "ઉપાડ રકમ";
		AppStrings.mTransactionMode = "વ્યવહાર મોડ";
		AppStrings.mBankLoanDisbursement = "બેંક લોન વહેંચણી";
		AppStrings.mMemberToMember = "સદસ્યથી બીજા સભ્ય સુધી";
		AppStrings.mDebitAccount = "ડેબિટ એકાઉન્ટ";
		AppStrings.mSHG_SavingsAccount = "shg બચત ખાતું";
		AppStrings.mDestinationMemberName = "ગંતવ્ય સભ્યનું નામ";
		AppStrings.mSourceMemberName = "સ્રોત સભ્યનું નામ";
		AppStrings.mMobileNoUpdation = "મોબાઇલ નંબર અપડેટ";
		AppStrings.mMobileNo = "મોબાઇલ નંબર";
		AppStrings.mMobileNoUpdateSuccessAlert = "મોબાઇલ નંબર સફળતાપૂર્વક અપડેટ થયો છે";
		AppStrings.mAadhaarNoUpdation = "આધાર નંબર અપિડીન";
		AppStrings.mSHGAccountNoUpdation = "શો એકાઉન્ટ નંબર નંબર";
		AppStrings.mAccountNoUpdation = "સભ્ય એકાઉન્ટ નંબર અપડેટ";
		AppStrings.mAadhaarNo = "આધાર નંબર વન";
		AppStrings.mBranchName = "શાખાનું નામ";
		AppStrings.mAadhaarNoUpdateSuccessAlert = "આધાર નંબર સફળતાપૂર્વક અપડેટ થયો છે";
		AppStrings.mMemberAccNoUpdateSuccessAlert = "સભ્ય એકાઉન્ટ નંબર સફળતાપૂર્વક અપડેટ થયો છે";
		AppStrings.mSHGAccNoUpdateSuccessAlert = "એસએચજી એકાઉન્ટ નંબર સફળતાપૂર્વક અપડેટ થયો છે";
		AppStrings.mCheckValidMobileNo = "કૃપા કરી માન્ય મોબાઇલ નંબર દાખલ કરો";
		AppStrings.mInvalidAadhaarNo = "અમાન્ય આધાર નંબર";
		AppStrings.mCheckValidAadhaarNo = "કૃપા કરી માન્ય આધાર નંબર દાખલ કરો";
		AppStrings.mInvalidAccountNo = "અમાન્ય એકાઉન્ટ નંબર";
		AppStrings.mCheckAccountNumber = "કૃપા કરીને તમારું એકાઉન્ટ નંબર તપાસો";
		AppStrings.mInvalidMobileNo = "અમાન્ય મોબાઇલ નંબર";
		AppStrings.mUploadScheduleAlert = "CAN'T UPLOAD SCHEDULE VI IN OFFLINE MODE";
		AppStrings.mIsMobileNoRepeat = "મોબાઇલ નંબરો પુનરાવર્તન";
		AppStrings.mMobileNoUpdationNetworkCheck = "ઓફલાઇન મોડમાં મોબાઇલ નંબર અપડેટ કરી શકતા નથી";
		AppStrings.mAadhaarNoNetworkCheck = "ઑફઅર મોડમાં આધાર નંબર અપડેટ કરી શકતા નથી";
		AppStrings.mShgAccNoNetworkCheck = "ઑફલાઇન મોડમાં એસએચજી એકાઉન્ટ નંબરને અપડેટ કરી શકતો નથી";
		AppStrings.mAccNoNetworkCheck = "ઑફલાઇન મોડમાં સભ્ય એકાઉન્ટ નંબર અપડેટ કરી શકતા નથી";
		AppStrings.mAccountNoRepeat = "એકાઉન્ટ નંબર્સ પુનરાવર્તન";
		AppStrings.mCheckBankAndBranchNameSelected = "અનુરૂપ એકાઉન્ટ નંબર માટે પસંદ કરેલ બેંક અને શાખા નામ તપાસો";
		AppStrings.mCheckNewLoanSancDate = "લોન મંજૂરી તારીખ પસંદ કરો";
		AppStrings.mCreditLinkageInfo = "ક્રેડિટ જોડાણ માહિતી";
		AppStrings.mCreditLinkage = "ક્રેડિટ જોડાણ";
		AppStrings.mCreditLinkage_content = "Number of times bank loans were taken by the SHG and repaid completely.";
		AppStrings.mCreditLinkageAlert = "ઑફલાઇન મોડમાં ક્રેડિટ લિંક્સ માહિતીને અપડેટ કરી શકતા નથી";
		AppStrings.mShgCode = "शग कोड";
		AppStrings.mDonation="દાન";
		AppStrings.mloanaccountnumber="લોન એકાઉન્ટ નંબર";
		AppStrings.mcash="રોકડ";




	}
}

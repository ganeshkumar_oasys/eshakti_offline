package com.oasys.eshakti.OasysUtils;

import android.view.View;
import android.view.ViewGroup;

public interface ExpandListItemClickListener {

	void onItemClick(ViewGroup parent, View view, int position);
	void onItemClickVerification(ViewGroup parent, View view, int position);
	void onItemClickAudit(ViewGroup parent, View view, int position);
}

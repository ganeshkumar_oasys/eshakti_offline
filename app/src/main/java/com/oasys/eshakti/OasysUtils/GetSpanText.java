package com.oasys.eshakti.OasysUtils;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;

import com.oasys.eshakti.R;



public class GetSpanText {

    public GetSpanText() {
        // TODO Auto-generated constructor stub
    }

    public static SpannableStringBuilder getSpanString(Context context, String str) {
        SpannableStringBuilder SS = new SpannableStringBuilder(str);

        SS.setSpan(
                null,
                0,
                str.length(),
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        SS.setSpan(new TextAppearanceSpan(context, R.style.text), 0, str.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        return SS;
    }
}

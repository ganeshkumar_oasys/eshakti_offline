package com.oasys.eshakti.OasysUtils;


public class RegionalLanguage_marathi {
	public RegionalLanguage_marathi() {
		// TODO Auto-generated constructor stub
	}

	public static void RegionalStrings() {

		AppStrings.LoginScreen = "लॉगीन स्क्रीन";
		AppStrings.userName = "मोबाइल क्रमांक";
		AppStrings.passWord = "पासवर्ड";
		AppStrings.transaction = "व्यवहार";
		AppStrings.reports = "अहवाल";
		AppStrings.profile = "रूपरेशा";
		AppStrings.meeting = "बैठक";
		AppStrings.settings = "मांडणी";
		AppStrings.help = "मदत";
		AppStrings.Attendance = "उपस्थिती";
		AppStrings.MinutesofMeeting = "सभेचे इतिवृत्त";
		AppStrings.savings = "बचत ";
		AppStrings.InternalLoanDisbursement = "कर्ज़ वाटप";
		AppStrings.expenses = "खर्च";
		AppStrings.cashinhand = "रोकड :  ₹ ";
		AppStrings.cashatBank = "बँकेतील रोकड :  ₹ ";
		AppStrings.memberloanrepayment = "सदस्य कर्जाची परतफेड";
		AppStrings.grouploanrepayment = "गट कर्ज परतफेड";
		AppStrings.income = "उत्पन्न";
		AppStrings.bankTransaction = "बँक व्यवहार";
		AppStrings.fixedDeposit = "मुदत ठेव";
		AppStrings.otherincome = "इतर उत्पन्न";
		AppStrings.subscriptioncharges = "वर्गणी";
		AppStrings.penalty = "दंड";
		AppStrings.donation = "दान";
		AppStrings.federationincome = "महासंघाचा उत्पन्न";//"महासंघाचा उत्पन्न";
		AppStrings.date = "दिनांक";
		AppStrings.passwordchange = "संकेतशब्द बदला";
		AppStrings.listofexpenses = "खर्च यादी";
		AppStrings.transport = "वाहतूक";
		AppStrings.snacks = "चहा";
		AppStrings.telephone = "दूरध्वनी";
		AppStrings.stationary = "लेखन साहित्य";
		AppStrings.otherExpenses = "इतर खर्च";
		AppStrings.federationIncomePaid = "महासंघाचा उत्पन्न (दिले)";
		AppStrings.bankInterest = "बँक व्याज";
		AppStrings.bankExpenses = "बँक खर्च";
		AppStrings.bankrepayment = "बैंके ची परतफेड ";
		AppStrings.total = "एकूण";
		AppStrings.summary = "सारांश";
		AppStrings.savingsSummary = "बचत - सारांश";
		AppStrings.loanSummary = "कर्ज सारांश";
		AppStrings.lastMonthReport = "मासिक अहवाल";
		AppStrings.Memberreports = "सदस्य अहवाल";
		AppStrings.GroupReports = "समूह अहवाल";
		AppStrings.transactionsummary = "बँक व्यवहार सारांश";
		AppStrings.balanceSheet = "ताळेबंद";
		AppStrings.balanceSheetHeader = "____चा ताळेबंद";
		AppStrings.trialBalance = "चाचणी शिल्लक";
		AppStrings.outstanding = "थकबाकी";
		AppStrings.totalSavings = "एकूण बचत";
		AppStrings.groupSavingssummary = "समूह बचत सारांश";
		AppStrings.groupLoansummary = "गट कर्ज सारांश";
		AppStrings.amount = "रक्कम";
		AppStrings.principleAmount = "रक्कम";
		AppStrings.interest = "व्याज";
		AppStrings.groupLoan = "गट कर्ज";
		AppStrings.balance = "शिल्लक";
		AppStrings.Name = "नाव";
		AppStrings.Repaymenybalance = "परतफेड शिल्लक";
		AppStrings.details = "तपशील";
		AppStrings.payment = "रक्कम";
		AppStrings.receipt = "पावती";
		AppStrings.FromDate = "तारखे पासून";
		AppStrings.ToDate = "तारखे पर्यंत ";
		AppStrings.fromDate = "पासून ";
		AppStrings.toDate = "पर्यंत ";
		AppStrings.OldPassword = "जुना संकेतशब्द";
		AppStrings.NewPassword = "नवीन संकेतशब्द";
		AppStrings.ConfirmPassword = "संकेतशब्द पुष्टि करा";
		AppStrings.agentProfile = "एजंटची माहिती";
		AppStrings.groupProfile = "गट माहिती ";
		AppStrings.bankDeposit = "बँक ठेव";
		AppStrings.withdrawl = "काढून घेणे ";
		AppStrings.OutstandingAmount = "थकबाकी";
		AppStrings.GroupLogin = "गट लॉग इन";
		AppStrings.AgentLogin = "एजंट लॉग इन";
		AppStrings.AboutLicense = "परवाना";
		AppStrings.contacts = "संपर्क";
		AppStrings.contactNo = "संपर्क क्रमांक";
		AppStrings.cashFlowStatement = "रोख प्रवाह अहवाल";
		AppStrings.IncomeExpenditure = "उत्पन्न / खर्च";
		AppStrings.transactionCompleted = "व्यवहार पूर्ण";
		AppStrings.loginAlert = "वापरकर्ता नाव आणि संकेतशब्द चुकीचे आहेत";
		AppStrings.loginUserAlert = "वापरकर्ता तपशील द्या";
		AppStrings.loginPwdAlert = "संकेतशब्द तपशील द्या";
		AppStrings.pwdChangeAlert = "संकेतशब्द यशस्वीरीत्या बदलले आहे";
		AppStrings.pwdIncorrectAlert = "योग्य संकेतशब्द प्रविष्ट करा";
		AppStrings.groupRepaidAlert = "आपली थकबाकी रक्कम तपासा";//"आपली  रक्कम पहा";
		AppStrings.cashinHandAlert = "रोकड पहा";
		AppStrings.cashatBankAlert = "बँकेतील रोकड पहा";
		AppStrings.nullAlert = "रोख रक्कम तपशील द्या";
		AppStrings.DepositnullAlert = "सर्व रोख तपशील द्या";
		AppStrings.MinutesAlert = "कृपया वरील कोणालाही निवडा";
		AppStrings.afterDate = "या तारखेनंतर एक तारीख निवडा";
		AppStrings.last5Transaction = "गेल्या पाच व्यवहाराची माहिती ";
		AppStrings.internetError = "कृपया आपले इंटरनेट कनेक्शन तपासा";
		AppStrings.lastTransactionDate = "गेल्या व्यवहाराची तारीख ";
		AppStrings.yes = "ठीक आहे";
		AppStrings.credit = "क्रेडिट";
		AppStrings.debit = "डेबिट";
		AppStrings.memberName = "सदस्य नाव";
		AppStrings.OutsatndingAmt = "थकबाकी";
		AppStrings.calFromToDateAlert = "दोन्ही तारखा निवडा";
		AppStrings.selectedDate = "निवडलेली तारीख";
		AppStrings.calAlert = " वेगली  तारीख निवडा";
		AppStrings.dialogMsg = "आपण या तारीखेनुसार नक्की पुढे जायचे आहे का ? ";
		AppStrings.dialogOk = "होय";
		AppStrings.dialogNo = "नाही";

		AppStrings.aboutYesbooks = "ईशक्ती किंवा स्वयं सहायता समूहांचे डिजिटायजेशन हा आपल्या माननीय पंतप्रधानां चे विधान "
				+ " ईलेक्ट्रोनिक डिजिटल भारताचे स्वप्न साकार करुया "
				+"च्या दिशा निर्देशा नुसार, नाबार्ड च्या सूक्ष्म ऋण नवप्रवर्तन विभाग ने पुढाकार घेतलेला उपक्रम आहे॰ डिजिटल भारत हा 1.13 लाख कोटी रुपयांचा,"
						+ " सरकारी विभाग आणि भारतीय जनता एकत्रित करण्याचा आणि परिणामकारक शासन याची तजवीज करण्यासाठीचा, भारत सरकार चा एक उपक्रम आहे. "
						+ "या उपक्रमा द्वारे भारताचे 'डिजिटल सक्षम समाज व ज्ञानी अर्थव्यवस्था' मध्ये परिवर्तित करण्या साठी आहे.";

		AppStrings.groupList = "गट निवडा ";// GROUP LIST
		AppStrings.login = "लॉग इन करा";
		AppStrings.submit = "सादर";
		AppStrings.edit = "संपादन";
		AppStrings.bankBalance = "बँक बॅलन्स";
		AppStrings.balanceAsOn = " ---पर्यंत शिल्लक : ";
		AppStrings.savingsBook = " वही प्रमाणे बचत खाते : ";
		AppStrings.savingsBank = " बँके प्रमाणे बचत खाते : ";
		AppStrings.difference = " फरक : ";
		AppStrings.loanOutstandingBook = " वही  प्रमाणे थकबाकी : ";
		AppStrings.loanOutstandingBank = " बँके प्रमाणे  थकबाकी : ";
		AppStrings.values = new String[] { "मुळ (ग्रामीण)",

				"सर्व सदस्य उपस्थित आहेत",

				"बचत रक्कम गोळा केली",

				"वैयक्तिक कर्ज वितरण बाबत चर्चा केली",

				"परतफेड रक्कम आणि व्याज गोळा केले",

				"प्रशिक्षण बाबत चर्चा केली ",

				"बँकेचे कर्ज घेण्याचा निर्णय घेतला",

				"सर्वसामान्य मतां बद्दल चर्चा केली",

				"स्वच्छते बाबत चर्चा केली ",

				"शिक्षणाच्या महत्वा बद्दल चर्चा केली " };

		AppStrings.January = "जानेवारी";
		AppStrings.February = "फेब्रुवारी";
		AppStrings.March = "मार्च";
		AppStrings.April = "एप्रिल";
		AppStrings.May = "मे";
		AppStrings.June = "जून";
		AppStrings.July = "जुलै";
		AppStrings.August = "ऑगस्ट";
		AppStrings.September = "सप्टेंबर";
		AppStrings.October = "ऑक्टोबर";
		AppStrings.November = "नोव्हेंबर";
		AppStrings.December = "डिसेंबर";

		AppStrings.uploadPhoto = "फोटो अपलोड करा";
		AppStrings.Il_Disbursed = "अंतर्गत कर्ज वाटप";
		AppStrings.IL_Loan = "अंतर्गत कर्ज";
		AppStrings.Il_Repaid = "अंतर्गत कर्जाची परतफेड";
		AppStrings.offlineTransactionCompleted = "आपल्या ऑफलाइन व्यवहाराच्या यशस्वीपणे जतन केले गेले आहे";
		AppStrings.offlineTransactionreports = "ऑफलाइन व्यवहाराच्या अहवाल";
		AppStrings.offlineTransaction = "ऑफलाइन व्यवहार";
		AppStrings.disconnectInternet = "कृपया ऑफलाइन सेवा सुरू करण्यासाठी आपल्या इंटरनेट डिस्कनेक्ट करा";
		AppStrings.Dashboard = "डॅशबोर्ड";
		AppStrings.bankTransactionSummary = "बँक व्यवहार सारांश";
		AppStrings.BL_Disbursed = "वितरित करण्यात आलेली ";
		AppStrings.BL_Repaid = "परतफेड";
		AppStrings.auditing = "हिशेब तपासणी";
		AppStrings.training = "प्रशिक्षण";
		AppStrings.auditor_Name = " लेखापरीक्षका चे नाव";
		AppStrings.auditing_Date = "हिशेब तपासणी तारीख";
		AppStrings.training_Date = "प्रशिक्षण तारीख";
		AppStrings.auditing_Period = "लेखापरीक्षा कालावधी निवडा";
		AppStrings.training_types = new String[] { "आरोग्य जागरूकता", "पुस्तक राखणदार जागरूकता", "उपजीविका जागरूकता",
				"सामाजिक जागरूकता", "शैक्षणिक जागरूकता" };
		AppStrings.changeLanguage = "भाषा बदला";
		AppStrings.interestRepayAmount = "चालू देय";
		AppStrings.interestRate = "व्याज दर";
		AppStrings.savingsAmount = "बचत रक्कम ";
		AppStrings.noGroupLoan_Alert = "गट कर्ज उपलब्ध नाही";
		AppStrings.confirmation = "पुष्टि करणे";
		AppStrings.logOut = "बाहेर पडणे";
		AppStrings.fromDateAlert = "तारखेपासून पुरवा";
		AppStrings.toDateAlert = "कृपया आत्तापर्यंतच्या प्रदान";
		AppStrings.auditingDateAlert = "कृपया हिशेब तपासणी तारीख प्रदान करा";
		AppStrings.auditorAlert = "कृपया लेखापरीक्षकाचे, नाव द्या";
		AppStrings.nullDetailsAlert = "सर्व तपशील प्रदान करा";
		AppStrings.trainingDateAlert = "प्रशिक्षण तारीख प्रदान करा";
		AppStrings.trainingTypeAlert = "किमान एक प्रशिक्षण प्रकार निवडा";
		AppStrings.offline_ChangePwdAlert = "आपण इंटरनेट शिवाय संकेतशब्द बदलू शकत नाही";
		AppStrings.transactionFailAlert = "तुमचा व्यवहार यशस्वी झाले नाही";
		AppStrings.voluntarySavings = "स्वैच्छिक बचत ";
		AppStrings.tenure = "कालावधी";
		AppStrings.purposeOfLoan = "कर्जाचा हेतू";
		AppStrings.chooseLabel = "कर्ज निवडा";
		AppStrings.Tenure_POL_Alert = "कृपया कर्ज कालावधी आणि उद्देश प्रविष्ट करा";
		AppStrings.interestSubvention = "व्याज सवलत";
		AppStrings.adminAlert = "कृपया आपल्या प्रशासकाशी संपर्क साधा ";
		AppStrings.userNotExist = "लॉगिन तपशील अस्तित्वात नाही. कृपया ऑनलाइन लॉग इन चालू ठेवा";
		AppStrings.tryLater = "कृपया नंतर पुन्हा प्रयत्न करा";
		AppStrings.offlineDataAvailable = "ऑफलाइन डेटा उपलब्ध आहे. कृपया लॉग आउट करा आणि पुन्हा प्रयत्न करा.";
		AppStrings.choosePOLAlert = "कर्जाचा हेतू निवडा";
		AppStrings.InternalLoan = "अंतर्गत कर्ज";
		AppStrings.TrainingTypes = "प्रशिक्षण प्रकार";
		AppStrings.uploadInfo = "सदस्य तपशील";
		AppStrings.uploadAadhar = "आधार विवरण अपलोड करा";
		AppStrings.month = "-- महिना निवडा --";
		AppStrings.year = "-- वर्ष निवडा --";
		AppStrings.monthYear_Alert = "कृपया महिना आणि वर्ष निवडा.";
		AppStrings.chooseLanguage = "भाषा निवडा";
		AppStrings.autoFill = "स्वयं-भरण";
		AppStrings.viewAadhar = "आधार पहा ";
		AppStrings.viewPhoto = "फोटो पहा";
		AppStrings.uploadAlert = "आपण इंटरनेट कनेक्शन शिवाय आधार आणि फोटो अपलोड करू शकत नाही ";//"आपण इंटरनेट कनेक्शन शिवाय आधार आणि फोटो अपलोड करू शकत";
		AppStrings.upload = "अपलोड";
		AppStrings.view = "पहा";
		AppStrings.showAadharAlert = "कृपया आपले आधार कार्ड दाखवा";
		AppStrings.deactivateAccount = "खाते निष्क्रिय करा";
		AppStrings.deactivateAccount_SuccessAlert = "आपले खाते यशस्वीपणे निष्क्रिय करण्यात आले आहे";
		AppStrings.deactivateAccount_FailAlert = "आपले खाते निष्क्रिय करण्यात अयशस्वी झालात";
		AppStrings.deactivateNetworkAlert = "आपण इंटरनेट जोडणी शिवाय आपले खाते निष्क्रिय करू शकत नाही";
		AppStrings.offlineReports = "ऑफलाइन अहवाल";
		AppStrings.registrationSuccess = "आपण यशस्वीरित्या नोंदणीकृत झालात";
		AppStrings.reg_MobileNo = "नोंदणीकृत मोबाईल नंबर प्रविष्ट करा ";
		AppStrings.reg_IMEINo = "प्रवेश नाकारला.";
		AppStrings.reg_BothNo = "प्रवेश नाकारला.";
		AppStrings.reg_IMEIUsed = "प्रवेश नाकारला. डिव्हाइस जुळत नाही";
		AppStrings.signInAsDiffUser = "भिन्न वापरकर्ता म्हणून साइन इन करा ";
		AppStrings.incorrectUserNameAlert = "अयोग्य वापरकर्तानाव";

		AppStrings.noofflinedatas = "ऑफलाइन व्यवहाराच्या नोंदी नाहीत";
		AppStrings.uploadprofilealert = "स्वत: चे तपशील अपलोड करा";
		AppStrings.monthYear_Warning = "कृपया योग्य महिना आणि वर्ष निवडा";
		AppStrings.of = " ऑफ ";
		AppStrings.previous_DateAlert = "मागील तारीख निवडा ";
		AppStrings.nobanktransactionreportdatas = "नाही व्यवहारासाठी डेटा अहवाल";

		AppStrings.mDefault = "डीफॉल्ट";
		AppStrings.mTotalDisbursement = "एकूण खर्च";
		AppStrings.mInterloan = "वैयक्तिक कर्जाची परतफेड";
		AppStrings.mTotalSavings = "एकूण संकलन";
		AppStrings.mCommonNetworkErrorMsg = "कृपया आपले नेटवर्क कनेक्शन तपासा";
		AppStrings.mLoginAlert_ONOFF = "कृपया इंटरनेट कनेक्शन वापरून पुन्हा लॉगिन करा";
		AppStrings.changeLanguageNetworkException = "कृपया लॉगआउट करा, इंटरनेट कनेक्शन वापरत पुन्हा लॉग इन करा";
		AppStrings.photouploadmsg = "फोटो यशस्वीरित्या अपलोड झाले";
		AppStrings.loginAgainAlert = "पार्श्वभूमी डेटा अद्याप कृपया थोडा वेळ प्रतीक्षा लोड करत आहे.";//"कृपया पुन्हा लॉगिन करा";

		/* Interloan Disbursement menu */
		AppStrings.mInternalInterloanMenu = "अंतर्गत कर्ज";
		AppStrings.mInternalBankLoanMenu = "बँकेकडून कर्ज";
		AppStrings.mInternalMFILoanMenu = "एम.एफ़.आई. कडून कर्ज";
		AppStrings.mInternalFederationMenu = "फेडरेशन कर्ज";

		AppStrings.bankName = "बँकेचे नाव";
		AppStrings.mfiname = "एम.एफ़.आई. चे नाव";
		AppStrings.loanType = "कर्ज प्रकार";
		AppStrings.bankBranch = "बँक शाखा";
		AppStrings.installment_type = "हप्ता प्रकार";
		AppStrings.NBFC_Name = "एन.बी.एफ़.सी. / बिगर बॅंकिंग वित्तीय कंपनी चे नाव";
		AppStrings.Loan_Account_No = "कर्ज खाते क्रमांक";
		AppStrings.Loan_Sanction_Amount = "कर्ज मंजूरी रकम ";
		AppStrings.Loan_Sanction_Date = "कर्ज मंजूर तारीख";
		AppStrings.Loan_Disbursement_Amount = "कर्ज वाटप रक्कम";

		AppStrings.Loan_Disbursement_Date = "कर्ज वाटप तारीख";
		AppStrings.Loan_Tenure = "कर्ज कालावधी";
		AppStrings.Subsidy_Amount = "अनुदानाचे रक्कम ";
		AppStrings.Subsidy_Reserve_Fund = "अनुदान राखीव रक्कम ";
		AppStrings.Interest_Rate = "व्याज दर";

		AppStrings.mMonthly = "मासिक";
		AppStrings.mQuarterly = "तिमाही";
		AppStrings.mHalf_Yearly = "सहामाही";
		AppStrings.mYearly = "वार्षिक";
		AppStrings.mAgri = "शेती";
		AppStrings.mMSME = "MSME";
		AppStrings.mOthers = "इतर";

		AppStrings.mMFI_NAME_SPINNER = "एम.एफ़.आई. चे नाव";
		AppStrings.mMFINabfins = "NABFINS";
		AppStrings.mMFIOthers = "इतर";

		AppStrings.mInternalTypeLoan = "अंतर्गत कर्ज";

		AppStrings.mPervious = "मागील";
		AppStrings.mNext = "पुढे";
		AppStrings.mPasswordUpdate = "आपले संकेतशब्द यशस्वीरित्या अद्यतनित अपडेट करण्यात आले आहे  ";

		AppStrings.mFixedDepositeAmount = "मुदत ठेव रक्कम";
		AppStrings.mFedBankName = "फेडेरेशन / महासंघाचे नाव";//"BANK/NBFC NAME";
		AppStrings.mLoanSanc_loanDist = "कृपया कर्ज वाटप रक्कम तपासणी करा";
		AppStrings.mStepWise_LoanDibursement = "अंतर्गत कर्ज वाटप";

		AppStrings.mExpense_meeting="सभा खर्च ";//भेट खर्च";
		AppStrings.mSubmitbutton = "ठीक आहे";
		AppStrings.mNewUserSignup="नोंदणी";
		AppStrings.mMobilenumber="मोबाईल नंबर";
		AppStrings.mIsAadharAvailable = "आधार माहिती अद्ययावत नाही";
		AppStrings.mSavingsTransaction = "बचत व्यवहार";
		AppStrings.mBankCharges = "बँक शुल्क";
		AppStrings.mCashDeposit = "रोख ठेव";
		AppStrings.mLimit = "कर्ज मर्यादा ";

		AppStrings.mAccountToAccountTransfer = "खाते हस्तांतरण खाते";
		AppStrings.mTransferFromBank = "बँकेकडून";
		AppStrings.mTransferToBank = "बँक";
		AppStrings.mTransferAmount = "हस्तांतरण रक्कम";
		AppStrings.mTransferCharges = "हस्तांतरण शुल्क";
		AppStrings.mTransferSpinnerFloating = "बँक";
		AppStrings.mTransferNullToast = "कृपया सर्व तपशील प्रदान";
		AppStrings.mAccToAccTransferToast = "आपण फक्त एक बँक खाते";

		/* Loan Account */
		AppStrings.mLoanaccHeader = "कर्ज खाते";
		AppStrings.mLoanaccCash = "रोख";
		AppStrings.mLoanaccBank = "बँक";
		AppStrings.mLoanaccWithdrawal = "काढणे";
		AppStrings.mLoanaccExapenses = "खर्च";
		AppStrings.mLoanaccSpinnerFloating = "कर्ज खाते बँक";
		AppStrings.mLoanaccCash_BankToast = "कृपया रोख किंवा बँक निवडा";
		AppStrings.mLoanaccNullToast = "कृपया सर्व तपशील प्रदान";
		AppStrings.mLoanaccBankNullToast = "कृपया एक बँक नाव निवडा";

		AppStrings.mBankTransSavingsAccount = "बचत खाते";
		AppStrings.mBankTransLoanAccount = "कर्ज खाते";

		AppStrings.mLoanAccType = "TYPE";
		AppStrings.mLoanAccBankAmountAlert = "कृपया बँक बॅलन्स चेक";

		AppStrings.mCashAtBank = "बँकेतील रोकड";
		AppStrings.mCashInHand = "रोकड";
		AppStrings.mShgSeedFund = "बचत गट संतती फंड";
		AppStrings.mFixedAssets = "स्थिर मालमत्ता";
		AppStrings.mVerified = "सत्यापित";
		AppStrings.mConfirm = "पुष्टी";
		AppStrings.mProceed = "पुढे";
		AppStrings.mDialogAlertText = "आपल्या प्रारंभिक शिल्लक datas आता आपण पुढे जाऊ, यशस्वीरित्या अद्यतनित केले जातात";
		AppStrings.mBalanceSheetDate = "ताळेबंद अपलोड करण्यात आले आहे";
		AppStrings.mCheckMonthlyEntries = "मागील महिन्यात नोंदी, त्यामुळे आपण मागील महिन्यात datas ठेवले पाहिजे";
		AppStrings.mTermLoanOutstanding = "OUTSTANDING";
		AppStrings.mCashCreditOutstanding = "OUTSTANDING";
		AppStrings.mGroupLoanOutstanding = "गट कर्ज थकबाकी";
		AppStrings.mMemberLoanOutstanding = "सदस्य कर्ज थकबाकी";
		AppStrings.mContinueWithDate = "आपण या तारीख बदलू इच्छित नाही";
		AppStrings.mCheckList = "पडताळणी सूची";
		AppStrings.mMicro_Credit_Plan = "सूक्ष्म क्रेडिट प्लॅन";
		AppStrings.mAadharCard_Invalid = "आपल्या आधार कार्ड अवैध आहे";
		AppStrings.mIsNegativeOpeningBalance = "नकारात्मक शिल्लक सरळ";
		AppStrings.mLoginDetailsDelete = "मागील लॉगिन तपशील हटवली जातील. आपण डेटा हटवू इच्छिता?";
		AppStrings.mVerifyGroupAlert = "सत्यापित करण्यासाठी आपण ऑनलाइन असणे आवश्यक आहे";
		AppStrings.next = "पुढे";
		AppStrings.mNone = "काहीही नाही";
		AppStrings.mSelect = "निवडा";
		AppStrings.mAnimatorName = "अॅनिमेटर नाव";
		AppStrings.mSavingsAccount = "बचत खाते";
		AppStrings.mAccountNumber = "खाते क्रमांक";
		AppStrings.mLoanAccountAlert = "ऑफलाइन मोड कर्ज खाते हस्तांतरित करू शकत नाही";
		AppStrings.mSeedFund = "बी निधी";
		AppStrings.mLoanTypeAlert = "कृपया कर्ज खाते प्रकार निवडा";
		AppStrings.mOutstandingAlert = "कृपया आपले उर्वरित रकमेपर्यंत पहा";
		AppStrings.mLoanAccTransfer = "कर्ज खाते हस्तांतरण";
		AppStrings.mLoanName = "कर्ज नाव";
		AppStrings.mLoanId = "कर्ज आयडी";
		AppStrings.mLoanDisbursementDate = "कर्ज वाटप तारीख";
		AppStrings.mAdd = "जोडा";
		AppStrings.mLess = "कमी";
		AppStrings.mRepaymentWithInterest = "परतफेड (व्याजासह एकूण)";
		AppStrings.mCharges = "शुल्क";
		AppStrings.mExistingLoan = "विद्यमान कर्ज";
		AppStrings.mNewLoan = "नवीन कर्ज";
		AppStrings.mIncreaseLimit = "मर्यादा वाढ";
		AppStrings.mAvailableLimit = "उपलब्ध मर्यादा";
		AppStrings.mDisbursementDate = "खर्च तारीख";
		AppStrings.mDisbursementAmount = "वाटप रक्कम";
		AppStrings.mSanctionAmount = "मंजूर रक्कम";
		AppStrings.mBalanceAmount = "रक्कम";
		AppStrings.mMemberDisbursementAmount = "सदस्य वाटप रक्कम";
		AppStrings.mLoanDisbursementFromLoanAcc = "कर्ज खाते कर्ज वाटप";
		AppStrings.mLoanDisbursementFromSbAcc = "बचत खाते कर्ज वाटप";
		AppStrings.mRepaid = "परतफेड";
		AppStrings.mCheckBackLog = "अनुशेष तपासा";
		AppStrings.mTarget = "लक्ष्य";
		AppStrings.mCompleted = "पूर्ण";
		AppStrings.mPending = "प्रलंबित";
		AppStrings.mAskLogout = "आपण लॉग आऊट करू इच्छिता ?.";
		AppStrings.mCheckDisbursementAlert = "आपल्या वाटप रक्कम तपासा";
		AppStrings.mCheckbalanceAlert = "आपल्या रक्कम तपासा";
		AppStrings.mVideoManual = "व्हिडिओ मॅन्युअल";
		AppStrings.mPdfManual = "पीडीएफ मॅन्युअल";
		AppStrings.mAmountDisbursingAlert = "आपण सदस्यांना वितरण न करता पुढे करू इच्छिता ?.";
		AppStrings.mOpeningDate = "उघडण्याच्या तारीख";
		AppStrings.mCheckLoanSancDate_LoanDisbDate = "आपल्या कर्ज मंजूर तारीख आणि कर्ज वितरण तारीख तपासा.";
		AppStrings.mLoanDisbursementFromRepaid = "सीसी कर्ज काढणे";
		AppStrings.mExpense = "खर्च";
		AppStrings.mBeforeLoanPay = "कर्जाची परतफेड करण्यापूर्वी";
		AppStrings.mLoans = "कर्ज";
		AppStrings.mSurplus = "अतिरिक्त";
		AppStrings.mPOLAlert = "कृपया कर्जाचा उद्देश प्रविष्ट करा";
		AppStrings.mCheckLoanAmounts = "कृपया आपल्या कर्ज मंजुरीची रक्कम आणि कर्ज वितरणाच्या रकमेची तपासणी करा";
		AppStrings.mCheckLoanSanctionAmount = "तुमचे कर्ज मंजुरी रक्कम भरा";
		AppStrings.mCheckLoanDisbursementAmount = "तुमच्या कर्जाची रक्कम वाटप करा";
		AppStrings.mIsPasswordSame = "कृपया आपला जुना संकेतशब्द तपासा आणि नवीन संकेतशब्द समान नसावा.";
		AppStrings.mSavings_Banktransaction = "बचत - बँक व्यवहार";
		AppStrings.mCheckBackLogOffline = "ऑफलाइन बघायला पाठपुरावा पाहू शकत नाही";
		AppStrings.mCredit = "क्रेडिट";
		AppStrings.mDebit = "डेबिट";
		AppStrings.mCheckFixedDepositAmount = "कृपया तुमची मुदत ठेव रक्कम तपासा.";
		AppStrings.mCheckDisbursementDate = "तुमची कर्जेची वाटचाल पहा";
		AppStrings.mSendEmail = "EMAIL";
		AppStrings.mTotalInterest = "एकूण व्याज";
		AppStrings.mTotalCharges = "एकूण शुल्क";
		AppStrings.mTotalRepayment = "एकूण परतफेड";
		AppStrings.mTotalInterestSubventionRecevied = "एकूण व्याज सूट प्राप्त";
		AppStrings.mTotalBankCharges = "एकूण बँक शुल्क";
		AppStrings.mDone = "DONE";
		AppStrings.mPayment = "देयक";
		AppStrings.mCashWithdrawal = "पैसे काढणे";
		AppStrings.mFundTransfer = "निधी हस्तांतरण";
		AppStrings.mDepositAmount = "ठेव रक्कम";
		AppStrings.mWithdrawalAmount = "पैसे काढण्याची रक्कम";
		AppStrings.mTransactionMode = "व्यवहार मोड";
		AppStrings.mBankLoanDisbursement = "बँक कर्ज वितरण";
		AppStrings.mMemberToMember = "सदस्य पासून दुसर्या सदस्याकडे";
		AppStrings.mDebitAccount = "डेबिट खाते";
		AppStrings.mSHG_SavingsAccount = "shg बचत खाते";
		AppStrings.mDestinationMemberName = "गंतव्य सदस्य नाव";
		AppStrings.mSourceMemberName = "स्रोत सदस्य नाव";
		AppStrings.mMobileNoUpdation = "मोबाइल नंबर अद्ययावत";
		AppStrings.mMobileNo = "मोबाइल नंबर";
		AppStrings.mMobileNoUpdateSuccessAlert = "मोबाइल नंबर यशस्वीरित्या अद्यतनित केला";
		AppStrings.mAadhaarNoUpdation = "आधार क्रमांक अपदीन";
		AppStrings.mSHGAccountNoUpdation = "शो खाते क्रमांक क्रमांक";
		AppStrings.mAccountNoUpdation = "सदस्य खाते क्रमांक सुधारणा";
		AppStrings.mAadhaarNo = "आधार क्रमांक 1";
		AppStrings.mBranchName = "शाखेचे नाव";
		AppStrings.mAadhaarNoUpdateSuccessAlert = "आधार क्रमांक यशस्वीरित्या अद्यतनित केला";
		AppStrings.mMemberAccNoUpdateSuccessAlert = "सदस्य खाते क्रमांक यशस्वीरित्या अद्यतनित केला";
		AppStrings.mSHGAccNoUpdateSuccessAlert = "SHG खाते क्रमांक यशस्वीरित्या अद्यतनित केला";
		AppStrings.mCheckValidMobileNo = "कृपया वैध मोबाइल नंबर प्रविष्ट करा";
		AppStrings.mInvalidAadhaarNo = "अवैध आधार क्रमांक";
		AppStrings.mCheckValidAadhaarNo = "कृपया एक वैध आधार क्रमांक प्रविष्ट करा";
		AppStrings.mInvalidAccountNo = "अवैध खाते क्रमांक";
		AppStrings.mCheckAccountNumber = "कृपया आपले खाते क्रमांक तपासा";
		AppStrings.mInvalidMobileNo = "अवैध मोबाईल नंबर";
		AppStrings.mUploadScheduleAlert = "CAN'T UPLOAD SCHEDULE VI IN OFFLINE MODE";
		AppStrings.mIsMobileNoRepeat = "मोबाइल नंबर पुनरावृत्ती";
		AppStrings.mMobileNoUpdationNetworkCheck = "ऑफलाइन मोडमध्ये मोबाइल नंबर अद्ययावत करू शकत नाही";
		AppStrings.mAadhaarNoNetworkCheck = "आधार क्रमांक ऑफलाइन मोडमध्ये बदलू शकत नाही";
		AppStrings.mShgAccNoNetworkCheck = "एसएचजी अकाउंट नंबर ऑफलाइन मोडमध्ये अपडेट करू शकत नाही";
		AppStrings.mAccNoNetworkCheck = "ऑफलाइन मोड मध्ये सदस्य खाते क्रमांक अद्यतनित करू शकत नाही";
		AppStrings.mAccountNoRepeat = "खाते क्रमांक पुनरावृत्ती";
		AppStrings.mCheckBankAndBranchNameSelected = "कृपया संबंधित खाते क्रमांकासाठी निवडलेले बँक आणि शाखेचे नाव तपासा";
		AppStrings.mCheckNewLoanSancDate = "कृपया कर्ज मंजुरीची तारीख निवडा";
		AppStrings.mCreditLinkageInfo = "क्रेडिट दुवा माहिती";
		AppStrings.mCreditLinkage = "क्रेडिट जोडणी";
		AppStrings.mCreditLinkage_content = "Number of times bank loans were taken by the SHG and repaid completely.";
		AppStrings.mCreditLinkageAlert = "ऑफलाइन मोडमध्ये क्रेडिट लिंकेज माहिती अद्यतनित करू शकत नाही";
		AppStrings.mDonation="देणगी";
		AppStrings.mloanaccountnumber="कर्ज खाते क्रमांक";
		AppStrings.mcash="रोख";

	}

}

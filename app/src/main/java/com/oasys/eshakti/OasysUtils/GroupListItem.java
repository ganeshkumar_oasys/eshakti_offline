package com.oasys.eshakti.OasysUtils;

public class GroupListItem {

	private int leftImageId;
	private String title;
	private int imageId;
	
	public GroupListItem(int leftImageId, String title , int imageId) {
		this.leftImageId = leftImageId;
		this.title = title;
		this.imageId = imageId;  
	}
	
	public int getLeftImageId() {
		return leftImageId;
	}

	public void setLeftImageId(int leftImageId) {
		this.leftImageId = leftImageId;
	}
	public int getImageId() {
		return imageId;
	}
	public void setImageId(int imageId) {
		this.imageId = imageId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

}

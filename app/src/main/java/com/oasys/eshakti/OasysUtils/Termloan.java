package com.oasys.eshakti.OasysUtils;

import com.oasys.eshakti.Dto.ExistingLoan;
import com.oasys.eshakti.Dto.OfflineDto;
import com.oasys.eshakti.Dto.VLoanTypes;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.database.LoanTable;
import com.oasys.eshakti.database.SHGTable;

import java.util.ArrayList;

public class Termloan {

    private static ArrayList<OfflineDto> offlineDBTermLoanData1 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData2 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData3 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData4 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData5 = new ArrayList<>();

//    GROUPLOAN UPDATION
    public static ArrayList<OfflineDto> offlineDBTermLoanData6 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBTermLoanData7 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBTermLoanData8 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBTermLoanData9 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBTermLoanData10 = new ArrayList<>();


    public static void termLoan()
    {
        ArrayList<VLoanTypes> termLoan =  EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan();

        if(termLoan!=null && termLoan.size()>0) {
            for (int j = 0; j < termLoan.size(); j++) {
                OfflineDto offline1 = new OfflineDto();
                offline1.setLoanId(termLoan.get(j).getLoanId());
                offline1.setMemberId(termLoan.get(j).getMemeberId());
                int amount = (int) Double.parseDouble(termLoan.get(j).getAmount());
                offline1.setOutStanding(amount + "");
                offline1.setMem_os(amount + "");
                offlineDBTermLoanData1.add(offline1);
            }
        }
    }

    public static void termLoan1()
    {
        ArrayList<VLoanTypes> termLoan1 =  EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan1();

        if(termLoan1!=null && termLoan1.size()>0) {
            for (int k = 0; k < termLoan1.size(); k++) {
                OfflineDto offline2 = new OfflineDto();
                offline2.setLoanId(termLoan1.get(k).getLoanId());
                offline2.setMemberId(termLoan1.get(k).getMemeberId());
                int amount1 = (int) Double.parseDouble(termLoan1.get(k).getAmount());
                offline2.setOutStanding(amount1 + "");
                offline2.setMem_os(amount1 + "");
                offlineDBTermLoanData2.add(offline2);
            }
        }
    }

    public  static void termLoan2()
    {

        ArrayList<VLoanTypes> termLoan2 =  EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan2();
        if(termLoan2!=null && termLoan2.size()>0) {
            for (int l = 0; l < termLoan2.size(); l++) {
                OfflineDto offline3 = new OfflineDto();
                offline3.setLoanId(termLoan2.get(l).getLoanId());
                offline3.setMemberId(termLoan2.get(l).getMemeberId());
                int amount2 = (int) Double.parseDouble(termLoan2.get(l).getAmount());
                offline3.setOutStanding(amount2 + "");
                offline3.setMem_os(amount2 + "");
                offlineDBTermLoanData3.add(offline3);
            }
        }
    }

    public  static void termLoan3()
    {

        ArrayList<VLoanTypes> termLoan3 =  EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan3();

        if(termLoan3!=null && termLoan3.size()>0) {
            for (int m = 0; m < termLoan3.size(); m++) {
                OfflineDto offline4 = new OfflineDto();
                offline4.setLoanId(termLoan3.get(m).getLoanId());
                offline4.setMemberId(termLoan3.get(m).getMemeberId());
                int amount3 = (int) Double.parseDouble(termLoan3.get(m).getAmount());
                offline4.setOutStanding(amount3 + "");
                offline4.setMem_os(amount3 + "");
                offlineDBTermLoanData4.add(offline4);
            }
        }
    }

    public  static void termLoan4()
    {

        ArrayList<VLoanTypes> termLoan4 =  EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan4();

        if(termLoan4!=null && termLoan4.size()>0) {

            for (int n = 0; n < termLoan4.size(); n++) {
                OfflineDto offline5 = new OfflineDto();
                offline5.setLoanId(termLoan4.get(n).getLoanId());
                offline5.setMemberId(termLoan4.get(n).getMemeberId());
                int amount4 = (int) Double.parseDouble(termLoan4.get(n).getAmount());
                offline5.setOutStanding(amount4 + "");
                offline5.setMem_os(amount4 + "");
                offlineDBTermLoanData5.add(offline5);
            }
        }
    }



    // GroupLoan Outstanding

    public static  void groupTermLoan() {

        ArrayList<VLoanTypes> groupTermLoan = EShaktiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan();
        if (groupTermLoan != null && groupTermLoan.size() > 0) {
            OfflineDto offline1 = new OfflineDto();
            offline1.setLoanId(groupTermLoan.get(0).getLoanId());
            offline1.setOutStanding(groupTermLoan.get(0).getOutStandingAmount());
            offlineDBTermLoanData6.add(offline1);
        }
    }

    public static  void groupTermLoan1() {

        ArrayList<VLoanTypes> groupTermLoan1 = EShaktiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan1();
        if (groupTermLoan1 != null && groupTermLoan1.size() > 0) {
            OfflineDto offline2 = new OfflineDto();
            offline2.setLoanId(groupTermLoan1.get(0).getLoanId());
            offline2.setOutStanding(groupTermLoan1.get(0).getOutStandingAmount());
            offlineDBTermLoanData7.add(offline2);
        }
    }



    public static  void groupTermLoan2() {

        ArrayList<VLoanTypes> groupTermLoan2 = EShaktiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan2();
        if (groupTermLoan2 != null && groupTermLoan2.size() > 0) {
            OfflineDto offline3 = new OfflineDto();
            offline3.setLoanId(groupTermLoan2.get(0).getLoanId());
            offline3.setOutStanding(groupTermLoan2.get(0).getOutStandingAmount());
            offlineDBTermLoanData8.add(offline3);
        }
    }

    public static  void groupTermLoan3() {

        ArrayList<VLoanTypes> groupTermLoan3 = EShaktiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan3();
        if (groupTermLoan3 != null && groupTermLoan3.size() > 0) {
            OfflineDto offline4 = new OfflineDto();
            offline4.setLoanId(groupTermLoan3.get(0).getLoanId());
            offline4.setOutStanding(groupTermLoan3.get(0).getOutStandingAmount());
            offlineDBTermLoanData9.add(offline4);
        }
    }

    public static  void groupTermLoan4() {

        ArrayList<VLoanTypes> groupTermLoan4 = EShaktiApplication.vertficationDto.getGroupfinancialDetails().get(0).getTermLoan4();
        if (groupTermLoan4 != null && groupTermLoan4.size() > 0) {
            OfflineDto offline5 = new OfflineDto();
            offline5.setLoanId(groupTermLoan4.get(0).getLoanId());
            offline5.setOutStanding(groupTermLoan4.get(0).getOutStandingAmount());
            offlineDBTermLoanData10.add(offline5);
        }
    }

    public static void updateMROSTERMLoan()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData1) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static void updateMROSTERMLoan1()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData2) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static  void updateMROSTERMLoan2()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData3) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }
    public static  void updateMROSTERMLoan3()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData4) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static  void updateMROSTERMLoan4()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData5) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }


    public static void updateTERMLOANEDIT() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBTermLoanData6.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBTermLoanData6.get(0).getLoanId());

    }

    public static void updateTERMLOANEDIT1() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBTermLoanData7.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBTermLoanData7.get(0).getLoanId());

    }


    public static void updateTERMLOANEDIT2() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBTermLoanData8.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBTermLoanData8.get(0).getLoanId());

    }

    public static void updateTERMLOANEDIT3() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBTermLoanData9.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBTermLoanData9.get(0).getLoanId());

    }

    public static void updateTERMLOANEDIT4() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBTermLoanData10.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBTermLoanData10.get(0).getLoanId());

    }

}

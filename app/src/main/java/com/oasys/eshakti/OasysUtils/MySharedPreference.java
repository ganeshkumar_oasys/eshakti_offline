package com.oasys.eshakti.OasysUtils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

/**
 * Created by ftuser on 5/8/15.
 */
public class MySharedPreference {


    public static final String SAVING_COUNT = "sCount";
    public static final String INCOME_COUNT = "iCount";
    public static final String EXPENSE_COUNT = "eCount";
    public static final String BT_COUNT = "btCount";
    public static final String MR_COUNT = "mrCount";
    public static final String MR_IL_COUNT = "mrILCount";
    public static final String ATTND_COUNT = "attCount";
    public static final String MoM_COUNT = "momCount";
    public static final String GRP_COUNT = "gCount";
    public static final String LD_COUNT = "ldCount";
    public static final String UPLOADSCHEDULE_COUNT = "uploadschedulecount";
    public static final String AUDIT_COUNT = "auditingcount";
    public static final String TRAINING_COUNT = "Trainingcount";


    public static final String PREF_NAME = "ESHAKTI";
    public static final String USERNAME = "username";
    public static final String MENU_SELECTED = "MENU_SELECTED";
    public static final String ForceSyncSuccess = "ForceSyncSuccess";
    public static final String UNAUTH = "Unauthorized";
    public static final String ON_OFF_FLAG = "OnOffFlag";
    public static final String NETWORK_MODE_FLAG = "modeFlag";

 //   public static final String PASSWORD = "password";
    public static final String ANIMATOR_ID = "animatorID";
    public static final String DB_AMT = "DA";
    public static final String MEM_NAME_SUMMARY = "memName";
    public static final String ANIMATOR_NAME = "animatorName";
    public static final String SHG_ID = "shg_id";
    public static final String CASHINHAND = "CashInHand";
    public static final String CASHATBANK = "CashAtBank";
    public static final String LAST_TRANSACTION = "LastTransaction";
    public static final String LANG_CODE = "Lang_Code";
    public static final String BALANCESHEET_DATE_TRANSACTION = "BalanceSheetDate";

    public static final String SINGIN_DIFF = "SignInDiff";
    public static final String LOGOUT = "LogOut";


    public static final int MODE = Context.MODE_PRIVATE;
    public static final String ACCESS_TOKEN = "Cokkies";
    public static final String REFRESH_TOKEN = "NewCokkies";


    public static final String COKKIES = "Cokkies";
    public static final String TermLoan = "TermLoanCount";
    public static final String MFICount = "MFILoanCount";
    public static final String CCcount = "CCr.Count";
    public static final String RFACount = "RFACount";
    public static final String BulkCount = "BulkLoanCount";
    public static final String CifCount = "CIFCount";
    public static final String ChangeUrl = "ChangeUrl";
    public static final String LANG = "language";
    public static final String LANG_ID = "language_id";


    private static final String TAG = MySharedPreference.class.getCanonicalName();


    public static void writeBoolean(Context context, String key, boolean value) {
        try {
            getEditor(context).putBoolean(key, value).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getUserlangcode(Context context) {
        return readString(context, MySharedPreference.LANG, null);
    }
/*

    public static void writeLong(Context context, String key, String langcode) {
        try {
            getEditor(context).putString(key, langcode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*///TODO::


    public static boolean readBoolean(Context context, String key,
                                      boolean defValue) {
        try {
            return getPreferences(context).getBoolean(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void writeInteger(Context context, String key, int value) {
        try {
            getEditor(context).putInt(key, value).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static int readInteger(Context context, String key, int defValue) {
        try {
            return getPreferences(context).getInt(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void writeString(Context context, String key, String value) {
        try {
            getEditor(context).putString(key, value).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*  public static String readString(Context context, String key, String defValue) {
          try {
              return getPreferences(context).getString(key, defValue);
          } catch (Exception e) {
              e.printStackTrace();
          }
          return "";
      }*/
    public static String readString(Context context, String key, String defValue) {
        try {
            return getPreferences(context).getString(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

  /*  public static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).commit();
    }

    public static float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }  */

    public static void writeLong(Context context, String key, long value) {
        try {
            getEditor(context).putLong(key, value).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static long readLong(Context context, String key, long defValue) {
        try {
            return getPreferences(context).getLong(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static SharedPreferences getPreferences(Context context) {
        try {
            if (context == null) {
                Log.e(TAG, "My shared preference context is null : ");
            }
            Log.d(TAG, "My shared preference context is not null : ");
            return context.getSharedPreferences(PREF_NAME, MODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Editor getEditor(Context context) {
        try {
            return getPreferences(context).edit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void clearSharedPreference(Context context, String prefName) {
        SharedPreferences settings;
        Editor editor;

        settings = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.clear();
        editor.commit();
//        MySharedPreference.getEditor(context).clear().commit();
    }
    //mContext.getSharedPreferences(TAG, Context.MODE_PRIVATE).edit().remove(yourKey).commit();

    public static void clearUserProfileSharedPreference(Context context, String prefName) {
        SharedPreferences settings;
        Editor editor;

        settings = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.clear();
        editor.commit();
        //MySharedPreference.getEditor(context).clear().commit();
    }
}

package com.oasys.eshakti.OasysUtils;

import com.oasys.eshakti.Dto.ExistingLoan;
import com.oasys.eshakti.Dto.OfflineDto;
import com.oasys.eshakti.Dto.VLoanTypes;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.database.LoanTable;
import com.oasys.eshakti.database.SHGTable;

import java.util.ArrayList;

public class BulkLoan {

    private static ArrayList<OfflineDto> offlineDBTermLoanData1 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData2 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData3 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData4 = new ArrayList<>();
    private static ArrayList<OfflineDto> offlineDBTermLoanData5 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBBulkLoanData6 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBBulkLoanData7 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBBulkLoanData8 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBBulkLoanData9 = new ArrayList<>();
    public static ArrayList<OfflineDto> offlineDBBulkLoanData10 = new ArrayList<>();

    public static void bulkLoan()
    {
        ArrayList<VLoanTypes> bulkLoan =  EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan();

        if(bulkLoan!=null && bulkLoan.size()>0) {
            for (int j = 0; j < bulkLoan.size(); j++) {
                OfflineDto offline1 = new OfflineDto();
                offline1.setLoanId(bulkLoan.get(j).getLoanId());
                offline1.setMemberId(bulkLoan.get(j).getMemeberId());
                int amount = (int) Double.parseDouble(bulkLoan.get(j).getAmount());
                offline1.setOutStanding(amount + "");
                offline1.setMem_os(amount + "");
                offlineDBTermLoanData1.add(offline1);
            }
        }
    }

    public static void bulkLoan1()
    {
        ArrayList<VLoanTypes> bulkLoan1 =  EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan1();

        if(bulkLoan1!=null && bulkLoan1.size()>0) {
            for (int k = 0; k < bulkLoan1.size(); k++) {
                OfflineDto offline2 = new OfflineDto();
                offline2.setLoanId(bulkLoan1.get(k).getLoanId());
                offline2.setMemberId(bulkLoan1.get(k).getMemeberId());
                int amount1 = (int) Double.parseDouble(bulkLoan1.get(k).getAmount());
                offline2.setOutStanding(amount1 + "");
                offline2.setMem_os(amount1 + "");
                offlineDBTermLoanData2.add(offline2);
            }
        }
    }

    public  static void bulkLoan2()
    {

        ArrayList<VLoanTypes> bulkLoan2 =  EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan2();
        if(bulkLoan2!=null && bulkLoan2.size()>0) {
            for (int l = 0; l < bulkLoan2.size(); l++) {
                OfflineDto offline3 = new OfflineDto();
                offline3.setLoanId(bulkLoan2.get(l).getLoanId());
                offline3.setMemberId(bulkLoan2.get(l).getMemeberId());
                int amount2 = (int) Double.parseDouble(bulkLoan2.get(l).getAmount());
                offline3.setOutStanding(amount2 + "");
                offline3.setMem_os(amount2 + "");
                offlineDBTermLoanData3.add(offline3);
            }
        }
    }

    public  static void bulkLoan3()
    {

        ArrayList<VLoanTypes> bulkLoan3 =  EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan3();

        if(bulkLoan3!=null && bulkLoan3.size()>0) {
            for (int m = 0; m < bulkLoan3.size(); m++) {
                OfflineDto offline4 = new OfflineDto();
                offline4.setLoanId(bulkLoan3.get(m).getLoanId());
                offline4.setMemberId(bulkLoan3.get(m).getMemeberId());
                int amount3 = (int) Double.parseDouble(bulkLoan3.get(m).getAmount());
                offline4.setOutStanding(amount3 + "");
                offline4.setMem_os(amount3 + "");
                offlineDBTermLoanData4.add(offline4);
            }
        }
    }

    public  static void bulkLoan4()
    {

        ArrayList<VLoanTypes> bulkLoan4 =  EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan4();

        if(bulkLoan4!=null && bulkLoan4.size()>0) {

            for (int n = 0; n < bulkLoan4.size(); n++) {
                OfflineDto offline5 = new OfflineDto();
                offline5.setLoanId(bulkLoan4.get(n).getLoanId());
                offline5.setMemberId(bulkLoan4.get(n).getMemeberId());
                int amount4 = (int) Double.parseDouble(bulkLoan4.get(n).getAmount());
                offline5.setOutStanding(amount4 + "");
                offline5.setMem_os(amount4 + "");
                offlineDBTermLoanData5.add(offline5);
            }
        }
    }

    // GroupLoan Outstanding

    public static  void groupBULKLoan() {

        ArrayList<VLoanTypes> groupBULKLoan = EShaktiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan();
        if (groupBULKLoan != null && groupBULKLoan.size() > 0) {
            OfflineDto offline1 = new OfflineDto();
            offline1.setLoanId(groupBULKLoan.get(0).getLoanId());
            offline1.setOutStanding(groupBULKLoan.get(0).getOutStandingAmount());
            offlineDBBulkLoanData6.add(offline1);
        }
    }
    public static  void groupBULKLoan1() {

        ArrayList<VLoanTypes> groupBULKLoan1 = EShaktiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan1();
        if (groupBULKLoan1 != null && groupBULKLoan1.size() > 0) {
            OfflineDto offline2 = new OfflineDto();
            offline2.setLoanId(groupBULKLoan1.get(0).getLoanId());
            offline2.setOutStanding(groupBULKLoan1.get(0).getOutStandingAmount());
            offlineDBBulkLoanData7.add(offline2);
        }
    }
    public static  void groupBULKLoan2() {

        ArrayList<VLoanTypes> groupBULKLoan2 = EShaktiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan2();
        if (groupBULKLoan2 != null && groupBULKLoan2.size() > 0) {
            OfflineDto offline3 = new OfflineDto();
            offline3.setLoanId(groupBULKLoan2.get(0).getLoanId());
            offline3.setOutStanding(groupBULKLoan2.get(0).getOutStandingAmount());
            offlineDBBulkLoanData8.add(offline3);
        }
    }

    public static  void groupBULKLoan3() {

        ArrayList<VLoanTypes> groupBULKLoan3 = EShaktiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan3();
        if (groupBULKLoan3 != null && groupBULKLoan3.size() > 0) {
            OfflineDto offline4 = new OfflineDto();
            offline4.setLoanId(groupBULKLoan3.get(0).getLoanId());
            offline4.setOutStanding(groupBULKLoan3.get(0).getOutStandingAmount());
            offlineDBBulkLoanData9.add(offline4);
        }
    }
    public static  void groupBULKLoan4() {

        ArrayList<VLoanTypes> groupBULKLoan4 = EShaktiApplication.vertficationDto.getGroupfinancialDetails().get(0).getBulkLoan4();
        if (groupBULKLoan4 != null && groupBULKLoan4.size() > 0) {
            OfflineDto offline5 = new OfflineDto();
            offline5.setLoanId(groupBULKLoan4.get(0).getLoanId());
            offline5.setOutStanding(groupBULKLoan4.get(0).getOutStandingAmount());
            offlineDBBulkLoanData10.add(offline5);
        }
    }

    public static void updateMROSBulkLoan()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData1) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static void updateMROSBulkLoan1()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData2) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static  void updateMROSBulkLoan2()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData3) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }
    public static  void updateMROSBulkLoan3()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData4) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static  void updateMROSBulkLoan4()
    {
        for (OfflineDto ofdto : offlineDBTermLoanData5) {
            LoanTable.updateMROSDetails(ofdto);
        }
    }

    public static void updateBULKLOANEDIT() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBBulkLoanData6.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBBulkLoanData6.get(0).getLoanId());

    }

    public static void updateBULKLOANEDIT1() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBBulkLoanData7.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBBulkLoanData7.get(0).getLoanId());

    }
    public static void updateBULKLOANEDIT2() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBBulkLoanData8.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBBulkLoanData8.get(0).getLoanId());

    }
    public static void updateBULKLOANEDIT3() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBBulkLoanData9.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBBulkLoanData9.get(0).getLoanId());

    }

    public static void updateBULKLOANEDIT4() {
        String grouploan_outstanding = "";
        grouploan_outstanding = offlineDBBulkLoanData10.get(0).getOutStanding();

        ExistingLoan existingLoan1 = new ExistingLoan();
        existingLoan1.setLoanOutstanding(grouploan_outstanding);
        SHGTable.updateSHGGrouploanDetails(existingLoan1, offlineDBBulkLoanData10.get(0).getLoanId());

    }

}

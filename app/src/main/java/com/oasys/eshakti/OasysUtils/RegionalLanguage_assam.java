package com.oasys.eshakti.OasysUtils;


public class RegionalLanguage_assam {

	public RegionalLanguage_assam(){
		
	}
	
	public static void RegionalStrings() {

		AppStrings.LoginScreen = "লগ-ইন স্ক্ৰীন";
		AppStrings.userName = "ব্যৱহাৰকাৰীৰ নাম";
		AppStrings.passWord = "পাছৱৰ্ড ";
		AppStrings.transaction = "লেনদেন";
		AppStrings.reports = "ৰিপৰ্ট";
		AppStrings.profile = "প্ৰফাইল";
		AppStrings.meeting = "মিটিং";
		AppStrings.settings = "ছেটিংচ";
		AppStrings.help = "হেল্প / সহায়তা";
		AppStrings.Attendance = "উপস্থিতি";
		AppStrings.MinutesofMeeting = "মিটিঙৰ কাৰ্য্যবিৱৰণ";
		AppStrings.savings = "সঞ্চয়";

		AppStrings.InternalLoanDisbursement = "অভ্যন্তৰীণ ঋণ বিতৰণ ";// "LOAN
																			// DISBURSEMENT";

		AppStrings.InternalLoanDisbursement = "ঋণ বিতৰণ";

		AppStrings.expenses = "ব্যয়";
		AppStrings.cashinhand = "হাতত নগদ জমাৰাশি :  ₹ ";
		AppStrings.cashatBank = "বেংকত জমাৰাশি :  ₹ ";
		AppStrings.memberloanrepayment = "সদস্যৰ ঋণ পৰিশোধ";
		AppStrings.grouploanrepayment = "গোটৰ ঋণ পৰিশোধ";
		AppStrings.income = "আয়";
		AppStrings.bankTransaction = "বেংক লেনদেন";
		AppStrings.fixedDeposit = "ফিক্সড ডিপ'জিত ";
		AppStrings.otherincome = "অন্য আয়";
		AppStrings.subscriptioncharges = "বৰঙণি";
		AppStrings.penalty = "পেনাল্টি";
		AppStrings.donation = "দান /বৰঙণি";
		AppStrings.federationincome = "ফেদাৰেচনৰ আয়";//
		AppStrings.date = " তাৰিখ";
		AppStrings.passwordchange = "পাছৱৰ্ড সলনি কৰক";
		AppStrings.listofexpenses = "খৰছৰ তালিকা";
		AppStrings.transport = "পৰিবহন";
		AppStrings.snacks = "চাহ";
		AppStrings.telephone = "টেলিফোন";
		AppStrings.stationary = "লেখন-সামগ্ৰী";//
		AppStrings.otherExpenses = "অন্য খৰচ";
		AppStrings.federationIncomePaid = "ফেদাৰেচনৰ আয় (আদায়কৃত)";
		AppStrings.bankInterest = "বেংকৰ সুদ";
		AppStrings.bankExpenses = "বেংকৰ খৰচ";
		AppStrings.bankrepayment = "বেংকত পৰিশোধ";
		AppStrings.total = "মুঠ";
		AppStrings.summary = "সাৰাংশ";
		AppStrings.savingsSummary = "জমাৰ বিৱৰণ";
		AppStrings.loanSummary = "ঋণৰ বিৱৰণ";
		AppStrings.lastMonthReport = "মাহেকীয়া ৰিপ'ৰ্ট";
		AppStrings.Memberreports = "সদস্যৰ ৰিপ'ৰ্ট";
		AppStrings.GroupReports = "গোটৰ ৰিপ'ৰ্ট";
		AppStrings.transactionsummary = "বেংক লেনদেনৰ সাৰাংশ ";
		AppStrings.balanceSheet = "বেলেঞ্ছ-ছীট";
		AppStrings.balanceSheetHeader = "…..... তাৰিখলৈ বেলেঞ্ছ-ছীট";
		AppStrings.trialBalance = "ট্ৰায়েল বেলেঞ্ছ";
		AppStrings.outstanding = "অসমাপ্ত / বাকী থকা";
		AppStrings.totalSavings = "মুঠ জমা";
		AppStrings.groupSavingssummary = "গোটৰ সঞ্ছয়-সাৰাংশ";
		AppStrings.groupLoansummary = "গোটৰ ঋণ-সাৰাংশ";
		AppStrings.amount = "ৰাশি";
		AppStrings.principleAmount = "AMOUNT";
		AppStrings.interest = "সুদ";
		AppStrings.groupLoan = "গোটৰ ঋণ";
		AppStrings.balance = "অৱশিষ্ট";
		AppStrings.Name = "নাম";
		AppStrings.Repaymenybalance = "অৱশিষ্ট";
		AppStrings.details = "বিস্তৃত";
		AppStrings.payment = "পৰিশোধ";
		AppStrings.receipt = "প্ৰাপ্তি";
		AppStrings.FromDate = "তাৰিখৰ পৰা";
		AppStrings.ToDate = "তাৰিখলৈ";
		AppStrings.fromDate = "পৰা";
		AppStrings.toDate = "লৈ";
		AppStrings.OldPassword = "পুৰণা  পাছৱৰ্ড ";
		AppStrings.NewPassword = "নতুন পাছৱৰ্ড ";
		AppStrings.ConfirmPassword = "পাছৱৰ্ড নিশ্চিত কৰক";
		AppStrings.agentProfile = "এনিমেটৰৰ প্ৰফাইল";// "AGENT PROFILE";
		AppStrings.groupProfile = "গোটৰ প্ৰফাইল";
		AppStrings.bankDeposit = "বেংকত জমা";
		AppStrings.withdrawl = "উলিওৱা";
		AppStrings.OutstandingAmount = "অসমাপ্ত / বাকী থকা";
		AppStrings.GroupLogin = "গোটৰ  লগ-ইন";
		AppStrings.AgentLogin = "এনিমেটৰৰ লগ-ইন";// "AGENT LOGIN";
		AppStrings.AboutLicense = "লাইচেঞ্চ";
		AppStrings.contacts = "যোগাযোগ";
		AppStrings.contactNo = "যোগাযোগ নং";
		AppStrings.cashFlowStatement = "নগদ প্ৰবাহ বিবৰণী";
		AppStrings.IncomeExpenditure = "আয় ব্যয়";
		AppStrings.transactionCompleted = "লেনদেন সম্পন্ন";
		AppStrings.loginAlert = "ব্যৱহাৰকাৰীৰ  নাম আৰু পাছৱৰ্ড অশুদ্ধ";
		AppStrings.loginUserAlert = "ব্যৱহাৰকাৰীৰ বিৱৰণ দিয়ক";
		AppStrings.loginPwdAlert = "পাছৱৰ্ডৰ বিৱৰণ দিয়ক ";
		AppStrings.pwdChangeAlert = "পাছৱৰ্ড সফলতাৰে সলনি হ'ল";
		AppStrings.pwdIncorrectAlert = "শুদ্ধ পাছৱৰ্ড দিয়ক";
		AppStrings.groupRepaidAlert = "CHECK YOUR OUTSTANDING AMOUNT";//"আপোনাৰ ধন-ৰাশি পৰীক্ষা কৰক";
		AppStrings.cashinHandAlert = "আপোনাৰ  হাতত থকা ধন-ৰাশি পৰীক্ষা কৰক";
		AppStrings.cashatBankAlert = "বেংকত থকা ধন-ৰাশি পৰীক্ষা কৰক";
		AppStrings.nullAlert = "নগদ ধনৰ বিৱৰণ দিয়ক ";
		AppStrings.DepositnullAlert = "সকলো নগদ ধনৰ বিৱৰণ দিয়ক ";
		AppStrings.MinutesAlert = "ওপৰৰ যিকোনো এটা বাচি লওক";
		AppStrings.afterDate = "বৰ্তমান তাৰিখৰ পিছৰ তাৰিখ এটা লওক";
		AppStrings.last5Transaction = "শেষৰ পাঁচটা লেনদেন";
		AppStrings.internetError = "অনুগ্ৰহ কৰি আপোনাৰ ইন্টাৰনেট সংযোগ পৰীক্ষা কৰক";
		AppStrings.lastTransactionDate = "শেষৰটো লেনদেনৰ তাৰিখ";
		AppStrings.yes = "অ'কে / সঠিক";
		AppStrings.credit = "জমা ";
		AppStrings.debit = "ধাৰ";
		AppStrings.memberName = "সদস্যৰ নাম";
		AppStrings.OutsatndingAmt = "অসমাপ্ত / বাকী থকা";
		AppStrings.calFromToDateAlert = "দুয়োটা তাৰিখ বাছি লওক";
		AppStrings.selectedDate = "বাচি লোৱা তাৰিখ";
		AppStrings.calAlert = "অন্য তাৰিখ লওক";
		AppStrings.dialogMsg = "এই তাৰিখটোৰে আগবাঢ়িবনে?";
		AppStrings.dialogOk = "হয়";
		AppStrings.dialogNo = "নহয়";

		AppStrings.aboutYesbooks = "ই-শক্তি' বা 'চেল্ফ হেল্প গ্ৰুপ ডিজিটাইজেচন' হৈছে নাবাৰ্ডৰ সুক্ষ্ম ঋণ নৱপ্ৰৱৰ্তন বিভাগৰ (MCID) এটি উদ্যোগ, যিটো  মাননীয় প্ৰধানমন্ত্ৰীৰ বক্তব্য"
				+"' আমি ইলেকট্ৰনিক ডিজিটেল ভাৰতৰ সপোনেৰে আগবঢ়িছোঁ ......'" +"ৰে এক অংশ । ভাৰতৰ জনগণ আৰু চৰকাৰী বিভাগসমূহৰ একত্ৰীকৰণ তথা কাৰ্য্যক্ষম পৰিচালন নিশ্চিতকৰণৰ"
						+ " বাবে 'ডিজিটেল ভাৰত' হৈছে ভাৰত চৰকাৰৰ ১.১৩ লাখ কোটি টকাৰ এক উদ্যোগ । ইয়ে ভাৰতক ডিজিটেল শক্তিক্ষম ছছাইটি আৰু জ্ঞানক্ষম অৰ্থনীতিলৈ পৰিবৰ্তিত কৰিব ।";

		AppStrings.groupList = "গোট বাচক";// GROUP LIST
		AppStrings.login = "   লগ-ইন    ";
		AppStrings.submit = "চাবমিট কৰক";
		AppStrings.edit = "সম্পাদন কৰক";
		AppStrings.bankBalance = "বেংকত থকা জমা-ৰাশি";
		AppStrings.balanceAsOn = " …….. তাৰিখলৈ জমা-ৰাশি : ";
		AppStrings.savingsBook = " বুক অনুসৰি  সঞ্চয় ৰাশি : ";
		AppStrings.savingsBank = " বেংক অনুসৰি সঞ্চয় ৰাশি : ";
		AppStrings.difference = "ব্যৱধান : ";
		AppStrings.loanOutstandingBook = " বুক অনুসৰি দেয় : ";
		AppStrings.loanOutstandingBank = " বেংক অনুসৰি দেয় : ";
		AppStrings.values = new String[] { "স্থান (গাঁও)",

				"সকলো সদস্য উপস্থিত আছে",

				"সঞ্চয় ৰাশি সংগ্ৰহ কৰা হ'ল",

				"ব্যক্তিগত ঋণ বিতৰণ সম্বন্ধে আলোচনা  কৰা হ'ল",

				"মূল আৰু সুদৰ কিস্তি আদায় কৰা হ'ল",

				"প্ৰশিক্ষণ সম্বন্ধে আলোচনা  কৰা হ'ল",

				"বেংকৰ ঋণৰ বাবে সিদ্ধান্ত লোৱা হ'ল",

				"সমূহীয়া সিদ্ধান্ত সম্বন্ধে আলোচনা  কৰা হ'ল",

				"চাফ-চিকুনতা সম্বন্ধে আলোচনা  কৰা হ'ল",

				"শিক্ষাৰ গুৰুত্ব সম্বন্ধে আলোচনা  কৰা হ'ল" };

		AppStrings.January = "জানুৱাৰী";
		AppStrings.February = "ফেব্ৰুৱাৰী";
		AppStrings.March = "মাৰ্চ ";
		AppStrings.April = "এপ্ৰিল ";
		AppStrings.May = "মে ";
		AppStrings.June = "জুন";
		AppStrings.July = "জুলাই ";
		AppStrings.August = "আগষ্ট";
		AppStrings.September = "ছেপ্তেম্বৰ ";
		AppStrings.October = "অক্টোবৰ";
		AppStrings.November = "নৱেম্বৰ ";
		AppStrings.December = "ডিচেম্বৰ";
		AppStrings.uploadPhoto = "ফ'টো আপলোড কৰক";
		AppStrings.Il_Disbursed = "বিতৰণ কৰা অভ্যন্তৰীণ ঋণ";
		AppStrings.IL_Loan = "অভ্যন্তৰীণ ঋণ";
		AppStrings.Il_Repaid = "অভ্যন্তৰীণ ঋণ পৰিশোধ কৰা হ'ল";
		AppStrings.offlineTransactionCompleted = "আপোনাৰ অফলাইন ট্ৰেনজেকচন সফলতাৰে চেভ কৰা হ'ল";
		AppStrings.offlineTransactionreports = "অফলাইন ট্ৰেনজেকচন ৰিপৰ্ট";
		AppStrings.offlineTransaction = "অফলাইন ট্ৰেনজেকচন ";
		AppStrings.disconnectInternet = "অফলাইন চাৰ্ভিছৰ কাৰণে  ইন্টাৰনেট সংযোগ বিচ্ছিন্ন কৰক";
		AppStrings.Dashboard = "ডেচ্-বোৰ্ড";
		AppStrings.bankTransactionSummary = "বেংক লেনদেনৰ সাৰাংশ ";
		AppStrings.BL_Disbursed = "বিতৰণ কৰা হ'ল";
		AppStrings.BL_Repaid = "পৰিশোধ কৰা হ'ল";
		AppStrings.auditing = "হিচাপ-পৰীক্ষণ";
		AppStrings.training = "প্ৰশিক্ষণ";
		AppStrings.auditor_Name = "হিচাপ-পৰীক্ষকৰ নাম ";
		AppStrings.auditing_Date = "হিচাপ-পৰীক্ষণৰ তাৰিখ";
		AppStrings.training_Date = "প্ৰশিক্ষণৰ তাৰিখ";
		AppStrings.auditing_Period = "হিচাপ-পৰীক্ষণৰ কাল বাচি লওক";
		AppStrings.training_types = new String[] { "স্বাস্থ্যৰ প্ৰতি সজাগতা", "হিচাপ-ৰক্ষণৰ প্ৰতি সজাগতা", "জীৱন ধাৰণৰ প্ৰতি সজাগতা",
				"সামাজিক সজাগতা", "শৈক্ষিক সজাগতা" };
		AppStrings.changeLanguage = "ভাষা সলনি কৰক";
		AppStrings.interestRepayAmount = "বৰ্তমানৰ  দেয়";
		AppStrings.interestRate = "সুদৰ হাৰ";
		AppStrings.savingsAmount = "সঞ্চয় ৰাশি";
		AppStrings.noGroupLoan_Alert = "গোটৰ ঋণ উপলব্ধ  নহয়";
		AppStrings.confirmation = " নিশ্চিতকৰণ / নিশ্চিতি";
		AppStrings.logOut = "লগ আউট";
		AppStrings.fromDateAlert = "কোন তাৰিখৰ পৰা";
		AppStrings.toDateAlert = " কোন তাৰিখলৈ";
		AppStrings.auditingDateAlert = "হিচাপ পৰীক্ষাৰ তাৰিখ";
		AppStrings.auditorAlert = "হিচাপ পৰীক্ষকৰ নাম দিয়ক";
		AppStrings.nullDetailsAlert = "সকলো তথ্য দিয়ক";
		AppStrings.trainingDateAlert = "প্ৰশিক্ষণৰ তাৰিখ দিয়ক";
		AppStrings.trainingTypeAlert = "কমেও এটা প্ৰশিক্ষণৰ প্ৰকাৰ বাছনি কৰক";
		AppStrings.offline_ChangePwdAlert = "ইন্টাৰনেট অবিহনে পাছৱৰ্ড  সলনি কৰিব নোৱাৰে";
		AppStrings.transactionFailAlert = "আপোনাৰ লেন-দেন সফল  নহ'ল";
		AppStrings.voluntarySavings = "ঐচ্ছিক সঞ্চয়";
		AppStrings.tenure = "কাৰ্যকাল";
		AppStrings.purposeOfLoan = "ঋণ লোৱাৰ উদ্দেশ্য";
		AppStrings.chooseLabel = "ঋণ বাছনি কৰক";
		AppStrings.Tenure_POL_Alert = "ঋণ লোৱাৰ উদ্দেশ্য আৰু কাৰ্যকাল নিশ্চিত কৰক";
		AppStrings.interestSubvention = "সুদৰ অনুদান পোৱা গ'ল";//
		AppStrings.adminAlert = "আপোনাৰ এডমিনৰ লগত যোগাযোগ কৰক";
		AppStrings.userNotExist = "লগ-ইন তথ্য উপলব্ধ  নহয় । অন-লাইন  লগ-ইন কৰি আগবাঢ়ক";
		AppStrings.tryLater = "পিছত চেষ্টা কৰক";
		AppStrings.offlineDataAvailable = "অফ-লাইন ডাটা উপলব্ধ । লগ-আউট  কৰি আকৌ চেষ্টা কৰক";
		AppStrings.choosePOLAlert = "ঋণ লোৱাৰ উদ্দেশ্য ";
		AppStrings.InternalLoan = "আভ্যন্তৰীণ ঋণ";
		AppStrings.TrainingTypes = "প্ৰশিক্ষণৰ প্ৰকাৰ";
		AppStrings.uploadInfo = "সদস্যৰ বিতং তথ্য";
		AppStrings.uploadAadhar = "আধাৰ' আপলোড কৰক";
		AppStrings.month = "-- মাহ বাছনি কৰক --";
		AppStrings.year = "-- বছৰ বাছনি কৰক --";
		AppStrings.monthYear_Alert = "মাহ আৰু বছৰ বাছনি কৰক";
		AppStrings.chooseLanguage = "ভাষা বাছনি কৰক";
		AppStrings.autoFill = "অট'-ফিল";
		AppStrings.viewAadhar = "আধাৰ দেখুৱাওক";
		AppStrings.viewPhoto = "ফটো দেখুৱাওক";
		AppStrings.uploadAlert = "ইন্টাৰনেট সংযোগ অবিহনে আধাৰ আৰু ফটো  আপলোড কৰিব  নোৱাৰি";// "YOU
																									// CAN'T
																									// UPLOAD
																									// THE
																									// AADHAR
																									// AND
																									// PHOTO
																									// WITHOUT
																									// INTERNET
																									// CONNECTION";
		AppStrings.upload = "আপলোড কৰক";
		AppStrings.view = "ভিউ";
		AppStrings.showAadharAlert = "আপোনাৰ আধাৰ কাৰ্ড দেখুৱাওক";
		AppStrings.deactivateAccount = " একাউণ্ট অচল কৰক ";
		AppStrings.deactivateAccount_SuccessAlert = "আপোনাৰ একাউণ্ট সফলতাৰে অচল কৰা হ'ল";
		AppStrings.deactivateAccount_FailAlert = " একাউণ্ট অচল কৰা প্ৰক্ৰিয়া ব্যৰ্থ হ'ল";
		AppStrings.deactivateNetworkAlert = "ইন্টাৰনেট অবিহনে একাউণ্ট অচল কৰিব  নোৱাৰি";
		AppStrings.offlineReports = "অফলাইন ৰিপৰ্ট";
		AppStrings.registrationSuccess = "আপোনাক সফলতাৰে ৰেজিষ্টাৰ কৰা হ'ল";
		AppStrings.reg_MobileNo = "ৰেজিষ্টাৰ থকা মোবাইল নম্বৰটো দিয়ক";
		AppStrings.reg_IMEINo = "অনুমতি বাৰণ কৰা হ'ল";
		AppStrings.reg_BothNo = "অনুমতি বাৰণ কৰা হ'ল";
		AppStrings.reg_IMEIUsed = "অনুমতি বাৰণ কৰা হ'ল।  ডিভইচত বিসংগতি আছে";
		AppStrings.signInAsDiffUser = "অন্য ব্যৱহাৰকাৰী হিচাপে  ছাইন-ইন কৰক";
		AppStrings.incorrectUserNameAlert = "ব্যৱহাৰকাৰীৰ নাম অশুদ্ধ";

		AppStrings.noofflinedatas = "অফলাইন ট্ৰেনজেকচন নহব";
		AppStrings.uploadprofilealert = "আপোনাৰ বিতং তথ্য আপলোড কৰক";
		AppStrings.monthYear_Warning = "সঠিক মাহ আৰু বছৰ বাছনি কৰক";
		AppStrings.of = " OF ";//"-"
		AppStrings.previous_DateAlert = "পূৰ্ববৰ্তী তাৰিখ বাছনি কৰক";
		AppStrings.nobanktransactionreportdatas = "বেংক লেনদেন ৰিপোৰ্টৰ ডাটা উপলব্ধ নহয়";

		AppStrings.mDefault = "ডিফল্ট";
		AppStrings.mTotalDisbursement = "মুঠ বিতৰণ";
		AppStrings.mInterloan = "ব্যক্তিগত ঋণ পৰিশোধ";// "PERSONAL LOAN
															// REPAYMENT";
		AppStrings.mTotalSavings = "মুঠ সংগ্ৰহ";
		AppStrings.mCommonNetworkErrorMsg = "অনুগ্ৰহ কৰি আপোনাৰ নেটৱৰ্ক সংযোগ পৰীক্ষা কৰক";
		AppStrings.mLoginAlert_ONOFF = "ইন্টাৰনেট সংযোগ কৰি পুনৰ লগ-ইন কৰক";
		AppStrings.changeLanguageNetworkException = "লগ-আউট কৰক, ইন্টাৰনেট সংযোগ কৰি পুনৰ লগ-ইন কৰাৰ চেষ্টা কৰক";
		AppStrings.photouploadmsg = "ফ'টো সফলতাৰে আপলোড কৰা হ'ল";
		AppStrings.loginAgainAlert = "BACKGROUND DATA IS STILL LOADING PLEASE WAIT FOR SOME TIME.";//"আকৌ লগ-ইন কৰক";

		/* Interloan Disbursement menu */
		AppStrings.mInternalInterloanMenu = "অভ্যন্তৰীণ ঋণ";
		AppStrings.mInternalBankLoanMenu = "বেংকৰ ঋণ";
		AppStrings.mInternalMFILoanMenu = "এম এফ আই ঋণ";
		AppStrings.mInternalFederationMenu = "ফেডাৰেচনৰ ঋণ";

		AppStrings.bankName = "বেংকৰ নাম ";
		AppStrings.mfiname = "এম এফ আই-ৰ নাম";
		AppStrings.loanType = "ঋণৰ প্ৰকাৰ";
		AppStrings.bankBranch = "বেংকৰ শাখা";
		AppStrings.installment_type = "কিস্তিৰ প্ৰকাৰ";
		AppStrings.NBFC_Name = "এন বি এফ চি-ৰ নাম";
		AppStrings.Loan_Account_No = "ল'ন একাউন্ট নম্বৰ";
		AppStrings.Loan_Sanction_Amount = "অনুমোদিত ঋণৰ ৰাশি";
		AppStrings.Loan_Sanction_Date = "ঋণ অনুমোদন কৰা তাৰিখ";
		AppStrings.Loan_Disbursement_Amount = "বিতৰণ কৰা ঋণৰ ৰাশি";

		AppStrings.Loan_Disbursement_Date = "ঋণ বিতৰণ কৰা তাৰিখ";
		AppStrings.Loan_Tenure = "ঋণৰ কাৰ্যকাল";
		AppStrings.Subsidy_Amount = "চাবচিডি ৰাশি";
		AppStrings.Subsidy_Reserve_Fund = "জমা ৰখা চাবচিডি ৰাশি";
		AppStrings.Interest_Rate = "সুদৰ হাৰ";

		AppStrings.mMonthly = "মাহিলি";
		AppStrings.mQuarterly = "পষেকীয়া";
		AppStrings.mHalf_Yearly = "ছমহীয়া ";
		AppStrings.mYearly = "বছৰেকীয়া";
		AppStrings.mAgri = "কৃষি";
		AppStrings.mMSME = "এম এচ এম ই ";
		AppStrings.mOthers = "অন্যান্য";

		AppStrings.mMFI_NAME_SPINNER = "এম এফ আই-ৰ নাম";
		AppStrings.mMFINabfins = "নেবফিনচ্";
		AppStrings.mMFIOthers = "অন্যান্য";

		AppStrings.mInternalTypeLoan = "অভ্যন্তৰীণ ঋণ";

		AppStrings.mPervious = "পূৰ্ববৰ্তী";
		AppStrings.mNext = "পৰবৰ্তী";
		AppStrings.mPasswordUpdate = "আপোনাৰ পাছৱৰ্ড সফলতাৰে আপডেট কৰা হ'ল";

		AppStrings.mExpense_meeting = "মিটিঙৰ খৰচ";

		AppStrings.mFixedDepositeAmount = "ফিক্সড ডিপ'জিত ৰাশি";
		AppStrings.mFedBankName = "ফেডাৰেচনৰ নাম ";// "BANK/NBFC NAME";
		AppStrings.mLoanSanc_loanDist = "ঋণ বিতৰণৰ ৰাশি পৰীক্ষা কৰক";

		AppStrings.mSubmitbutton = "অ'কে / সঠিক";

		AppStrings.mStepWise_LoanDibursement = "অভ্যন্তৰীণ ঋণ বিতৰণ ";
		AppStrings.mNewUserSignup = "পঞ্জীয়ন";
		AppStrings.mMobilenumber = "মোবাইল নম্বৰ ";
		AppStrings.mIsAadharAvailable = "আধাৰৰ তথ্য আপডেট কৰা হোৱা নাই";
		AppStrings.mSavingsTransaction = "সঞ্ছয়ৰ লেনদেন";
		AppStrings.mBankCharges = "বেংকৰ মাচুল";
		AppStrings.mCashDeposit = "নগদ জমা";
		AppStrings.mLimit = "ঋণৰ সীমা";

		AppStrings.mAccountToAccountTransfer = "একাউন্টৰ পৰা একাউন্টলৈ স্থানান্তৰ";
		AppStrings.mTransferFromBank = "পৰা বেংকত";
		AppStrings.mTransferToBank = "লৈ বেংকত";
		AppStrings.mTransferAmount = "ধন-ৰাশি স্থানান্তৰ কৰক";
		AppStrings.mTransferCharges = "স্থানান্তৰণ-মাচুল";
		AppStrings.mTransferSpinnerFloating = "লৈ বেংকত";
		AppStrings.mTransferNullToast = "সকলো তথ্য দিয়ক";
		AppStrings.mAccToAccTransferToast = "YOU HAVE ONLY ONE BANK ACCOUNT";

		/* Loan Account */
		AppStrings.mLoanaccHeader = "ল'ন একাউন্ট";
		AppStrings.mLoanaccCash = "হাতত";
		AppStrings.mLoanaccBank = "বেংকত";
		AppStrings.mLoanaccWithdrawal = "উলিওৱা";
		AppStrings.mLoanaccExapenses = "ব্যয়";
		AppStrings.mLoanaccSpinnerFloating = "ল'ন একাউন্ট বেংকত ";
		AppStrings.mLoanaccCash_BankToast = "PLEASE SELECT A CASH OR BANK";
		AppStrings.mLoanaccNullToast = "সকলো তথ্য দিয়ক";
		AppStrings.mLoanaccBankNullToast = "PLEASE SELECT A BANK NAME";

		AppStrings.mBankTransSavingsAccount = "বুক অনুসৰি";
		AppStrings.mBankTransLoanAccount = "ল'ন একাউন্ট";

		AppStrings.mLoanAccType = "TYPE";
		AppStrings.mLoanAccBankAmountAlert = "বেংকত থকা জমা-ৰাশি পৰীক্ষা কৰক";

		AppStrings.mCashAtBank = "বেংকত জমাৰাশি";
		AppStrings.mCashInHand = "হাতত নগদ জমাৰাশি";
		AppStrings.mShgSeedFund = "SHG SEED FUND";
		AppStrings.mFixedAssets = "FIXED ASSETS";
		AppStrings.mVerified = "VERIFIED";
		AppStrings.mConfirm = "CONFIRM";
		AppStrings.mProceed = "PROCEED";
		AppStrings.mDialogAlertText = "YOUR OPENING BALANCE DATAS ARE UPDATED SUCCESSFULLY, NOW YOU CAN PROCEED";
		AppStrings.mBalanceSheetDate = "BALANCE SHEET DATE";
		AppStrings.mCheckMonthlyEntries = "NO ENTRIES FOR PREVIOUS MONTH, SO YOU SHOULD PUT DATAS FOR PREVIOUS MONTH";
		AppStrings.mTermLoanOutstanding = "OUTSTANDING";
		AppStrings.mCashCreditOutstanding = "OUTSTANDING";
		AppStrings.mGroupLoanOutstanding = "GROUP LOAN OUTSTANDING";
		AppStrings.mMemberLoanOutstanding = "MEMBER LOAN OUTSTANDING";
		AppStrings.mContinueWithDate = "DO YOU WANT TO CHANGE THIS DATE";
		AppStrings.mCheckList = "CHECK LIST";
		AppStrings.mMicro_Credit_Plan = "MICRO CREDIT PLAN";
		AppStrings.mAadharCard_Invalid = "YOUR AADHAR CARD IS INVALID";
		AppStrings.mIsNegativeOpeningBalance = "RECTIFY NEGATIVE BALANCE";
		AppStrings.mLoginDetailsDelete = "PREVIOUS LOGIN DETAILS WILL BE DELETED. ARE YOU WANT TO DELETE DATA?";
		AppStrings.mVerifyGroupAlert = "YOU MUST BE ONLINE TO VERIFY";
		AppStrings.next = "NEXT";
		AppStrings.mNone = "NONE";
		AppStrings.mSelect = "SELECT";
		AppStrings.mAnimatorName = "ANIMATOR NAME";
		AppStrings.mSavingsAccount = "SAVINGS ACCOUNT";
		AppStrings.mAccountNumber = "ACCOUNT NUMBER";
		AppStrings.mLoanAccountAlert = "CAN'T TRANSFER LOAN ACCOUNT IN OFFLINE MODE";
		AppStrings.mSeedFund = "SEED FUND";
		AppStrings.mLoanTypeAlert = "PLEASE CHOOSE A LOAN ACCOUNT TYPE";
		AppStrings.mOutstandingAlert = "PLEASE CHECK YOUR OUTSTANDING AMOUNT";
		AppStrings.mLoanAccTransfer = "ল'ন একাউন্ট";
		AppStrings.mLoanName = "LOAN NAME";
		AppStrings.mLoanId = "LOAN ID";
		AppStrings.mLoanDisbursementDate = "LOAN DISBURSEMENT DATE";
		AppStrings.mAdd = "ADD";
		AppStrings.mLess = "LESS";
		AppStrings.mRepaymentWithInterest = "REPAYMENT ( WITH INTEREST )";
		AppStrings.mCharges = "CHARGES";
		AppStrings.mExistingLoan = "EXISTING LOAN";
		AppStrings.mNewLoan = "NEW LOAN";
		AppStrings.mIncreaseLimit = "INCREASE LIMIT";
		AppStrings.mAvailableLimit = "AVAILABLE LIMIT";
		AppStrings.mDisbursementDate = "DISBURSEMENT DATE";
		AppStrings.mDisbursementAmount = "DISBURSEMENT AMOUNT";
		AppStrings.mSanctionAmount = "SANCTION AMOUNT";
		AppStrings.mBalanceAmount = "BALANCE AMOUNT";
		AppStrings.mMemberDisbursementAmount = "MEMBER DISBURSEMENT AMOUNT";
		AppStrings.mLoanDisbursementFromLoanAcc = "LOAN DISBURSEMENT FROM LOAN ACCOUNT";
		AppStrings.mLoanDisbursementFromSbAcc = "LOAN DISBURSEMENT FROM SB ACCOUNT";
		AppStrings.mRepaid = "REPAID";
		AppStrings.mCheckBackLog = "CHECK BACKLOG";
		AppStrings.mTarget = "TARGET";
		AppStrings.mCompleted = "COMPLETED";
		AppStrings.mPending = "PENDING";
		AppStrings.mAskLogout = "DO YOU WANT TO LOGOUT?.";
		AppStrings.mCheckDisbursementAlert = "PLEASE CHECK YOUR DISBURSEMENT AMOUNT";
		AppStrings.mCheckbalanceAlert = "PLEASE CHECK YOUR BALANCE AMOUNT";
		AppStrings.mVideoManual = "VIDEO MANUAL";
		AppStrings.mPdfManual = "PDF MANUAL";
		AppStrings.mAmountDisbursingAlert = "DO YOU WANT TO CONTINUE WITHOUT DISBURSING TO MEMBERS?.";
		AppStrings.mOpeningDate = "OPENING DATE";
		AppStrings.mCheckLoanSancDate_LoanDisbDate = "PLEASE CHECK YOUR LOAN SANCTION DATE AND LOAN DISBURSEMENT DATE.";
		AppStrings.mLoanDisbursementFromRepaid = "CC LOAN WITHDRAWAL";
		AppStrings.mExpense = "EXPENSE";
		AppStrings.mBeforeLoanPay = "BEFORE LOAN PAY";
		AppStrings.mLoans = "LOANS";
		AppStrings.mSurplus = "SURPLUS";
		AppStrings.mPOLAlert = "PLEASE ENTER THE PURPOSE OF LOAN";
		AppStrings.mCheckLoanAmounts = "PLEASE CHECK YOUR LOAN SANCTION AMOUNT AND LOAN DISBURSEMENT AMOUNT";
		AppStrings.mCheckLoanSanctionAmount = "ENTER YOUR LOAN SANCTION AMOUNT";
		AppStrings.mCheckLoanDisbursementAmount = "ENTER YOUR LOAN DISBURSEMENT AMOUNT";
		AppStrings.mIsPasswordSame = "PLEASE CHECK YOUR OLD PASSWORD AND NEW PASSWORD SHOULD NOT BE SAME.";
		AppStrings.mSavings_Banktransaction = "SAVINGS - BANK TRANSACTION";
		AppStrings.mCheckBackLogOffline = "CAN'T VIEW CHECK BACKLOG IN OFFLINE";
		AppStrings.mCredit = "CREDIT";
		AppStrings.mDebit = "DEBIT";
		AppStrings.mCheckFixedDepositAmount = "PLEASE CHECK YOUR FIXED DEPOSIT AMOUNT";
		AppStrings.mCheckDisbursementDate = "PLEASE CHECK YOUR LOAN DISBURSEMENT DATE";
		AppStrings.mSendEmail = "EMAIL";
		AppStrings.mTotalInterest = "TOTAL INTEREST";
		AppStrings.mTotalCharges = "TOTAL CHARGES";
		AppStrings.mTotalRepayment = "TOTAL REPAYMENT";
		AppStrings.mTotalInterestSubventionRecevied = "TOTAL INTEREST SUBVENTION RECEVIED";
		AppStrings.mTotalBankCharges = "TOTAL BANK CHARGES";
		AppStrings.mDone = "DONE";
		AppStrings.mPayment = "PAYMENT";
		AppStrings.mCashWithdrawal = "CASH WITHDRAWAL";
		AppStrings.mFundTransfer = "FUND TRANSFER";
		AppStrings.mDepositAmount = "DEPOSIT AMOUNT";
		AppStrings.mWithdrawalAmount = "WITHDRAWAL AMOUNT";
		AppStrings.mTransactionMode = "TRANSACTION MODE";
		AppStrings.mBankLoanDisbursement = "BANK LOAN DISBURSEMENT";
		AppStrings.mMemberToMember = "MEMBER TO MEMBER";
		AppStrings.mDebitAccount = "DEBIT ACCOUNT";
		AppStrings.mSHG_SavingsAccount = "SHG SAVINGS ACCOUNT";
		AppStrings.mDestinationMemberName = "DESTINATION MEMBER NAME";
		AppStrings.mSourceMemberName = "SOURCE MEMBER NAME";
		AppStrings.mMobileNoUpdation = "MOBILE NUMBER UPDATION";
		AppStrings.mMobileNo = "MOBILE NUMBER";
		AppStrings.mMobileNoUpdateSuccessAlert = "MOBILE NUMBER UPDATED SUCCESSFULLY.";
		AppStrings.mAadhaarNoUpdation = "AADHAAR NUMBER UPDATION";
		AppStrings.mSHGAccountNoUpdation = "SHG ACCOUNT NUMBER UPDATION";
		AppStrings.mAccountNoUpdation = "MEMBER ACCOUNT NUMBER UPDATION";
		AppStrings.mAadhaarNo = "AADHAAR NUMBER";
		AppStrings.mBranchName = "BRANCH NAME";
		AppStrings.mAadhaarNoUpdateSuccessAlert = "AADHAAR NUMBER UPDATED SUCCESSFULLY.";
		AppStrings.mMemberAccNoUpdateSuccessAlert = "MEMBER ACCOUNT NUMBER UPDATED SUCCESSFULLY.";
		AppStrings.mSHGAccNoUpdateSuccessAlert = "SHG ACCOUNT NUMBER UPDATED SUCCESSFULLY.";
		AppStrings.mCheckValidMobileNo = "PLEASE ENTER A VAILD MOBILE NUMBER";
		AppStrings.mInvalidAadhaarNo = "INVALID AADHAAR NUMBER";
		AppStrings.mCheckValidAadhaarNo = "PLEASE ENTER A VAILD AADHAAR NUMBER";
		AppStrings.mInvalidAccountNo = "INVALID ACCOUNT NUMBER";
		AppStrings.mCheckAccountNumber = "PLEASE CHECK YOUR ACCOUNT NUMBER";
		AppStrings.mInvalidMobileNo = "INVALID MOBILE NUMBER";
		AppStrings.mUploadScheduleAlert = "CAN'T UPLOAD SCHEDULE VI IN OFFLINE MODE";
		AppStrings.mIsMobileNoRepeat = "MOBILE NUMBERS REPEATED";
		AppStrings.mMobileNoUpdationNetworkCheck = "CAN'T UPDATE MOBILE NUMBER IN OFFLINE MODE";
		AppStrings.mAadhaarNoNetworkCheck = "CAN'T UPDATE AADHAAR NUMBER IN OFFLINE MODE";
		AppStrings.mShgAccNoNetworkCheck = "CAN'T UPDATE SHG ACCOUNT NUMBER IN OFFLINE MODE";
		AppStrings.mAccNoNetworkCheck = "CAN'T UPDATE MEMBER ACCOUNT NUMBER IN OFFLINE MODE";
		AppStrings.mAccountNoRepeat = "ACCOUNT NUMBERS REPEATED";
		AppStrings.mCheckBankAndBranchNameSelected = "PLEASE CHECK BANK AND BRANCH NAME SELECTED FOR A CORRESPONDING ACCOUNT NUMBER";
		AppStrings.mCheckNewLoanSancDate = "PLEASE SELECT A LOAN SANCTION DATE";
		AppStrings.mCreditLinkageInfo = "CREDIT LINKAGE INFO";
		AppStrings.mCreditLinkage = "CREDIT LINKAGE";
		AppStrings.mCreditLinkage_content = "Number of times bank loans were taken by the SHG and repaid completely.";
		AppStrings.mCreditLinkageAlert = "CAN'T UPDATE CREDIT LINKAGE INFO IN OFFLINE MODE";
	}
}

package com.oasys.eshakti.OasysUtils;

public interface NavigationManager {
    void showFragment(String title);
}

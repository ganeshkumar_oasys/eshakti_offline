package com.oasys.eshakti.OasysUtils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.oasys.eshakti.EShaktiApplication;


public class PrefUtils {

	private static final String APP_PREF_FILE_NAME = "yesteam_yesbooks_app";

	private static final String KEY_USER_LANGUAGE_CODE = "yesbooks_user_language_code";

	private static final String AGENT_USERNAME_KEY = "Agent_Username_Key";

	private static final String GROUP_USERNAME_KEY = "Group_Username_Key";

	private static final String S_I_P_SC_VALUES_KEY = "S_I_P_SC_Values_Key";

	private static final String ML_REPAID_PA_VALUES_KEY = "ML_REPAID_PA_VALUES_KEY";

	private static final String ML_REPAID_INTR_VALUES_KEY = "ML_REPAID_INTR_VALUES_KEY";

	private static final String EXPENSES_VALUES_KEY = "EXPENSES_VALUES_KEY";

	private static final String BANK_DEPOSIT_VALUES_KEY = "BANK_DEPOSIT_VALUES_KEY";

	private static final String FIXED_DEPOSIT_VALUES_KEY = "FIXED_DEPOSIT_VALUES_KEY";

	private final static String GROUP_LOAN_REPAID_KEY = "Group_Loan_Repaid_Key";

	private final static String USERNAME_KEY = "UserName_Key";

	private final static String OFFLINE_MEMBERLOANREPAY = "OFF_ML_Repayment";

	private final static String OFFLINE_UNIQUEID = "Offline_Uniqueid";

	private final static String OFFLINE_MEMLOANID = "Off_memloanid";

	private final static String OFFLINE_GROUPLOSREPAY = "Off_Grouplosrepay";

	private final static String OFFLINE_GROUPLOSID = "Off_GrouplosId";

	private final static String OFFLINE_BANKDEPOSIT = "Off_bankdeposit";

	private final static String OFFLINE_BANKDEPOSITNAME = "Off_bankdepositname";

	private final static String OFFLINE_BANK_FIXED_DEPOSIT = "Off_bankfixeddeposit";

	private final static String OFFLINE_BANK_FIXED_DEPOSITNAME = "Off_bankfixeddepositname";

	private final static String OFFLINE_TRANSACTION_ID = "Off_transaction_id";

	private final static String APP_INSTALLED_KEY = "App_installed_key";

	private final static String OFFLINE_SERVICE_NAMESPACE = "Offline_service_namespace";

	private final static String APP_DEVICE_ADMIN = "App_Device_Admin";

	private final static String APP_USERTYPE = "App_Usertype";

	private final static String CONTACTS = "Contacts";

	private final static String GROUP_OFFLINEDATA_VALUE = "Group_Offlinedata_value";

	private final static String LOGIN_GROUP_SERVICE = "Login_Group_Service";

	private final static String ANIMATOR_NAME = "Animator_Name";

	private final static String ANIMATOR_TARGET_VALUES = "Animator_Target_Values";

	private final static String ANIMATOR_OFFLINE_DATA_VALUES = "Animator_offline_Data_Values";

	private final static String PREF_LOGIN_VALUES = "Pref_Login_Values";

	private final static String PREF_GROUPLIST_VALUES = "Pref_GroupList_Values";

	private final static String PREF_GROUP_MASTER_RESPONSE_VALUES = "Pref_GroupMasterResponse_Values";

	private final static String PREF_ANIMATOR_PENDING_GROUP = "Pref_Animator_pending_group";

	private final static String PREF_FIRST_LOGIN_DASHBOARD = "Pref_First_Login_Dashboard";

	private final static String PREF_STEP_WISE_SCREEN_COUNT = "Pref_Step_Wise_Screen_Count";

	private final static String PREF_STEP_WISE_MEMBER_LOAN_REPAY_SCREEN_COUNT = "Pref_Step_Wise_Member_Loan_Repay_Screen_Count";

	private final static String PREF_STEP_WISE_GROUP_ID = "Step_Wise_Group_Id";

	private final static String PREF_STATUS_GROUP_ID = "Status_Group_Id";

	private final static String PREF_STATUS_TRANS_TYPE = "Status_Trans_Type";

	private final static String PREF_STATUS_UNIQUEID = "Status_UniqueId";

	private final static String PREF_STATUS_TRANSACTION_SUCCESS = "Status_Transaction_Success";

	private final static String PREF_STATUS_DATA_INSERT_SUCCESS = "Status_Data_Insert_Success";

	private final static String PREF_STATUS_CALCULATION_SUCCESS = "Status_Calculation_Success";

	private final static String PREF_CURRENT_DATE = "Current_Date";

	private final static String PREF_CURRENT_FORMAT_DATE = "Current_Format_Date";

	private final static String PREF_ADD_GROUP_FLAG = "Add_Group_Flag";

	private static final String APP_LOCATION_PERMISSION = "App_Location";
	private static final String APP_STORAGE_PERMISSION = "App_Storage";
	private static final String APP_PHONE_PERMISSION = "App_Phone";
	private static final String APP_CAMERA_PERMISSION = "App_Camera";

	private static final SharedPreferences mApplicationSharedPref;

	public static Editor mEditor;

	static {
		mApplicationSharedPref = EShaktiApplication.getInstance().getSharedPreferences(APP_PREF_FILE_NAME,
				Context.MODE_PRIVATE);
	}

	public static String getUserlangcode() {
		return mApplicationSharedPref.getString(KEY_USER_LANGUAGE_CODE, null);
	}

	public static void setUserlangcode(final String langcode) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(KEY_USER_LANGUAGE_CODE, langcode);
		mEditor.commit();
	}

	public static String getAgentUserName() {
		return mApplicationSharedPref.getString(AGENT_USERNAME_KEY, null);
	}

	public static void setAgentUserName(String userName) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(AGENT_USERNAME_KEY, userName);
		mEditor.commit();
	}

	public static String getGroupUserName() {
		return mApplicationSharedPref.getString(GROUP_USERNAME_KEY, null);
	}

	public static void setGroupUserName(String userName) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(GROUP_USERNAME_KEY, userName);
		mEditor.commit();
	}

	public static String getSIPSCValuesKey() {
		return mApplicationSharedPref.getString(S_I_P_SC_VALUES_KEY, null);
	}

	public static void setSIPSCValuesKey(String values) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(S_I_P_SC_VALUES_KEY, values);
		mEditor.commit();
	}

	public static String getMLRepaidPA_ValuesKey() {
		return mApplicationSharedPref.getString(ML_REPAID_PA_VALUES_KEY, null);
	}

	public static void setMLRepaidPA_ValuesKey(String values) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(ML_REPAID_PA_VALUES_KEY, values);
		mEditor.commit();
	}

	public static String getMLRepaidIntr_ValuesKey() {
		return mApplicationSharedPref.getString(ML_REPAID_INTR_VALUES_KEY, null);
	}

	public static void setMLRepaidIntr_VlauesKey(String values) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(ML_REPAID_INTR_VALUES_KEY, values);
		mEditor.commit();
	}

	public static String getExpensesValuesKey() {
		return mApplicationSharedPref.getString(EXPENSES_VALUES_KEY, null);
	}

	public static void setExpensesValuesKey(String values) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(EXPENSES_VALUES_KEY, values);
		mEditor.commit();
	}

	public static String getBankDepositValuesKey() {
		return mApplicationSharedPref.getString(BANK_DEPOSIT_VALUES_KEY, null);
	}

	public static void setBankDepositValuesKey(String values) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(BANK_DEPOSIT_VALUES_KEY, values);
		mEditor.commit();
	}

	public static String getFixedDepositValuesKey() {
		return mApplicationSharedPref.getString(FIXED_DEPOSIT_VALUES_KEY, null);
	}

	public static void setFixedDepositValuesKey(String values) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(FIXED_DEPOSIT_VALUES_KEY, values);
		mEditor.commit();
	}

	public static String getGroupLoanRepaidKey() {
		return mApplicationSharedPref.getString(GROUP_LOAN_REPAID_KEY, null);
	}

	public static void setGropLoanRepaidKey(String values) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(GROUP_LOAN_REPAID_KEY, values);
		mEditor.commit();
	}

	public static String getUsernameKey() {
		return mApplicationSharedPref.getString(USERNAME_KEY, null);
	}

	public static void setUserNameKey(String values) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(USERNAME_KEY, values);
		mEditor.commit();
	}

	public static String getOfflineMLrepay() {
		return mApplicationSharedPref.getString(OFFLINE_MEMBERLOANREPAY, null);
	}

	public static void setOfflineMLrepay(String offMLrepay) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(OFFLINE_MEMBERLOANREPAY, offMLrepay);
		mEditor.commit();
	}

	public static String getOfflineUniqueID() {
		return mApplicationSharedPref.getString(OFFLINE_UNIQUEID, null);
	}

	public static void setOfflineUniqueID(String offUniqueId) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(OFFLINE_UNIQUEID, offUniqueId);
		mEditor.commit();
	}

	public static String getOfflineMemberloanID() {
		return mApplicationSharedPref.getString(OFFLINE_MEMLOANID, null);
	}

	public static void setOfflineMemberLoanID(String offMemberLoanId) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(OFFLINE_MEMLOANID, offMemberLoanId);
		mEditor.commit();
	}

	public static String getOfflineGroupOSLID() {
		return mApplicationSharedPref.getString(OFFLINE_GROUPLOSID, null);
	}

	public static void setOfflineGroupOsLoanID(String offGroupLoanId) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(OFFLINE_GROUPLOSID, offGroupLoanId);
		mEditor.commit();
	}

	public static String getOfflineGroupLosrepay() {
		return mApplicationSharedPref.getString(OFFLINE_GROUPLOSREPAY, null);
	}

	public static void setOfflineGroupLosrepay(String offGroupLosrepay) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(OFFLINE_GROUPLOSREPAY, offGroupLosrepay);
		mEditor.commit();
	}

	public static String getOfflineBankDeposit() {
		return mApplicationSharedPref.getString(OFFLINE_BANKDEPOSIT, null);
	}

	public static void setOfflineBankDeposit(String offBankDeposit) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(OFFLINE_BANKDEPOSIT, offBankDeposit);
		mEditor.commit();
	}

	public static String getOfflineBankDepositName() {
		return mApplicationSharedPref.getString(OFFLINE_BANKDEPOSITNAME, null);
	}

	public static void setOfflineBankDepositName(String offBankDepositName) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(OFFLINE_BANKDEPOSITNAME, offBankDepositName);
		mEditor.commit();
	}

	public static String getOfflineBankFixedDeposit() {
		return mApplicationSharedPref.getString(OFFLINE_BANK_FIXED_DEPOSIT, null);
	}

	public static void setOfflineBankFixedDeposit(String offBankFixedDeposit) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(OFFLINE_BANK_FIXED_DEPOSIT, offBankFixedDeposit);
		mEditor.commit();
	}

	public static String getOfflineBankFixedDepositName() {
		return mApplicationSharedPref.getString(OFFLINE_BANK_FIXED_DEPOSITNAME, null);
	}

	public static void setOfflineBankFixedDepositName(String offBankFixedDepositName) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(OFFLINE_BANK_FIXED_DEPOSITNAME, offBankFixedDepositName);
		mEditor.commit();
	}

	public static String getOfflineTransactionId() {
		return mApplicationSharedPref.getString(OFFLINE_TRANSACTION_ID, null);
	}

	public static void setOfflineTransactionId(String offTransactionId) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(OFFLINE_TRANSACTION_ID, offTransactionId);
		mEditor.commit();
	}

	public static String getAppinstalledkey() {
		return mApplicationSharedPref.getString(APP_INSTALLED_KEY, null);
	}

	public static void setAppinstalledkey(String appinstalledkey) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(APP_INSTALLED_KEY, appinstalledkey);
		mEditor.commit();
	}

	public static String getServicenamespace() {
		return mApplicationSharedPref.getString(OFFLINE_SERVICE_NAMESPACE, null);
	}

	public static void setServicenamespace(String servicenamespace) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(OFFLINE_SERVICE_NAMESPACE, servicenamespace);
		mEditor.commit();
	}

	public static String getAppDeviceAdmin() {
		return mApplicationSharedPref.getString(APP_DEVICE_ADMIN, null);
	}

	public static void setAppDeviceAdmin(String appdeviceadmin) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(APP_DEVICE_ADMIN, appdeviceadmin);
		mEditor.commit();
	}

	public static String getAppUsertype() {
		return mApplicationSharedPref.getString(APP_USERTYPE, null);
	}

	public static void setAppUsertype(String appusertype) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(APP_USERTYPE, appusertype);
		mEditor.commit();
	}

	public static String getOfflineContacts() {
		return mApplicationSharedPref.getString(CONTACTS, null);
	}

	public static void setOfflineContacts(String contacts) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(CONTACTS, contacts);
		mEditor.commit();
	}

	public static String getGroup_Offlinedata() {
		return mApplicationSharedPref.getString(GROUP_OFFLINEDATA_VALUE, null);
	}

	public static void setGroup_Offlinedata(String groupofflinedata) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(GROUP_OFFLINEDATA_VALUE, groupofflinedata);
		mEditor.commit();
	}

	public static String getLoginGroupService() {
		return mApplicationSharedPref.getString(LOGIN_GROUP_SERVICE, null);
	}

	public static void setLoginGroupService(String logingroupservice) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(LOGIN_GROUP_SERVICE, logingroupservice);
		mEditor.commit();
	}

	public static String getAnimatorName() {
		return mApplicationSharedPref.getString(ANIMATOR_NAME, null);
	}

	public static void setAnimatorName(String animatorname) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(ANIMATOR_NAME, animatorname);
		mEditor.commit();
	}

	public static String getAnimatorTargetValues() {
		return mApplicationSharedPref.getString(ANIMATOR_TARGET_VALUES, null);
	}

	public static void setAnimatorTargetValues(String animatorTargetValues) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(ANIMATOR_TARGET_VALUES, animatorTargetValues);
		mEditor.commit();
	}

	public static String getAnimatorOfflineDataValues() {
		return mApplicationSharedPref.getString(ANIMATOR_OFFLINE_DATA_VALUES, null);
	}

	public static void setAnimatorOfflineDataValues(String animatorOfflineDataValues) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(ANIMATOR_OFFLINE_DATA_VALUES, animatorOfflineDataValues);
		mEditor.commit();
	}

	public static String getLoginValues() {
		return mApplicationSharedPref.getString(PREF_LOGIN_VALUES, null);
	}

	public static void setLoginValues(String loginvalues) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(PREF_LOGIN_VALUES, loginvalues);
		mEditor.commit();
	}

	public static String getGrouplistValues() {
		return mApplicationSharedPref.getString(PREF_GROUPLIST_VALUES, null);
	}

	public static void setGrouplistValues(String Grouplist) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(PREF_GROUPLIST_VALUES, Grouplist);
		mEditor.commit();
	}

	public static String getGroupMasterResValues() {
		return mApplicationSharedPref.getString(PREF_GROUP_MASTER_RESPONSE_VALUES, null);
	}

	public static void setGroupMasterResValues(String GroupMasterRes) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(PREF_GROUP_MASTER_RESPONSE_VALUES, GroupMasterRes);
		mEditor.commit();
	}

	public static String getAnimatorPendingGroupValues() {
		return mApplicationSharedPref.getString(PREF_ANIMATOR_PENDING_GROUP, null);
	}

	public static void setAnimatorPendingGroupValues(String animatorPendingGroup) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(PREF_ANIMATOR_PENDING_GROUP, animatorPendingGroup);
		mEditor.commit();
	}

	public static String getFirstLoginDashboard() {
		return mApplicationSharedPref.getString(PREF_FIRST_LOGIN_DASHBOARD, null);
	}

	public static void setFirstLoginDashboard(String firstlogindashboard) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(PREF_FIRST_LOGIN_DASHBOARD, firstlogindashboard);
		mEditor.commit();
	}

	public static String getStepWiseScreenCount() {
		return mApplicationSharedPref.getString(PREF_STEP_WISE_SCREEN_COUNT, null);
	}

	public static void setStepWiseScreenCount(String StepWiseScreenCount) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(PREF_STEP_WISE_SCREEN_COUNT, StepWiseScreenCount);
		mEditor.commit();
	}

	public static String getStepWise_MemberloanrepayScreenCount() {
		return mApplicationSharedPref.getString(PREF_STEP_WISE_MEMBER_LOAN_REPAY_SCREEN_COUNT, null);
	}

	public static void setStepWise_MemberloanrepayScreenCount(String MemberloanrepayScreenCount) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(PREF_STEP_WISE_MEMBER_LOAN_REPAY_SCREEN_COUNT, MemberloanrepayScreenCount);
		mEditor.commit();
	}

	public static String getStepWise_GroupId() {
		return mApplicationSharedPref.getString(PREF_STEP_WISE_GROUP_ID, null);
	}

	public static void setStepWise_GroupId(String StepWise_GroupId) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(PREF_STEP_WISE_GROUP_ID, StepWise_GroupId);
		mEditor.commit();
	}

	public static String getStatus_GroupId() {
		return mApplicationSharedPref.getString(PREF_STATUS_GROUP_ID, null);
	}

	public static void setStatus_GroupId(String StatusGroupId) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(PREF_STATUS_GROUP_ID, StatusGroupId);
		mEditor.commit();
	}

	public static String getStatus_Trans_Type() {
		return mApplicationSharedPref.getString(PREF_STATUS_TRANS_TYPE, null);
	}

	public static void setStatus_Trans_Type(String Status_TransType) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(PREF_STATUS_TRANS_TYPE, Status_TransType);
		mEditor.commit();
	}

	public static String getStatus_UniqueID() {
		return mApplicationSharedPref.getString(PREF_STATUS_UNIQUEID, null);
	}

	public static void setStatus_UniqueID(String StatusUniqueID) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(PREF_STATUS_UNIQUEID, StatusUniqueID);
		mEditor.commit();
	}

	public static String getStatus_Transacton_Success() {
		return mApplicationSharedPref.getString(PREF_STATUS_TRANSACTION_SUCCESS, null);
	}

	public static void setStatus_Transacton_Success(String Status_TransactionSuccess) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(PREF_STATUS_TRANSACTION_SUCCESS, Status_TransactionSuccess);
		mEditor.commit();
	}

	public static String getStatus_Data_Insert_Success() {
		return mApplicationSharedPref.getString(PREF_STATUS_DATA_INSERT_SUCCESS, null);
	}

	public static void setStatus_Data_Insert_Success(String Status_Data_InsertSuccess) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(PREF_STATUS_DATA_INSERT_SUCCESS, Status_Data_InsertSuccess);
		mEditor.commit();
	}

	public static String getStatus_Calculation_Success() {
		return mApplicationSharedPref.getString(PREF_STATUS_CALCULATION_SUCCESS, null);
	}

	public static void setStatus_Calculation_Success(String Status_CalculationSuccess) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(PREF_STATUS_CALCULATION_SUCCESS, Status_CalculationSuccess);
		mEditor.commit();
	}

	public static String getCurrentDate() {
		return mApplicationSharedPref.getString(PREF_CURRENT_DATE, null);
	}

	public static void setCurrentDate(String CurrentDate) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(PREF_CURRENT_DATE, CurrentDate);
		mEditor.commit();
	}

	public static void clearCurrentDate() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(PREF_CURRENT_DATE);
		editor.commit();
	}

	public static String getCurrentFormatDate() {
		return mApplicationSharedPref.getString(PREF_CURRENT_FORMAT_DATE, null);
	}

	public static void setCurrentFormatDate(String CurrentFormatDate) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(PREF_CURRENT_FORMAT_DATE, CurrentFormatDate);
		mEditor.commit();
	}

	public static String getAddGroupFlag() {
		return mApplicationSharedPref.getString(PREF_ADD_GROUP_FLAG, null);
	}

	public static void setAddGroupFlag(String AddGroupFlag) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(PREF_ADD_GROUP_FLAG, AddGroupFlag);
		mEditor.commit();
	}

	public static void clearAddGroupFlag() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(PREF_ADD_GROUP_FLAG);
		editor.commit();
	}

	public static void clearCurrentFormatDate() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(PREF_CURRENT_FORMAT_DATE);
		editor.commit();
	}

	public static void clearStatus_Calculation_Success() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(PREF_STATUS_CALCULATION_SUCCESS);
		editor.commit();
	}

	public static void clearStatus_Data_Insert_Success() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(PREF_STATUS_DATA_INSERT_SUCCESS);
		editor.commit();
	}

	public static void clearStatus_Transacton_Success() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(PREF_STATUS_TRANSACTION_SUCCESS);
		editor.commit();
	}

	public static void clearStatus_UniqueID() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(PREF_STATUS_UNIQUEID);
		editor.commit();
	}

	public static void clearStatus_Trans_Type() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(PREF_STATUS_TRANS_TYPE);
		editor.commit();
	}

	public static void clearStatus_GroupId() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(PREF_STATUS_GROUP_ID);
		editor.commit();
	}

	public static void clearStepWise_GroupId() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(PREF_STEP_WISE_GROUP_ID);
		editor.commit();
	}

	public static void clearStepWise_MemberloanrepayScreenCount() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(PREF_STEP_WISE_MEMBER_LOAN_REPAY_SCREEN_COUNT);
		editor.commit();
	}

	public static void clearStepWiseScreenCount() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(PREF_STEP_WISE_SCREEN_COUNT);
		editor.commit();
	}

	public static void clearFirstLoginDashboard() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(PREF_FIRST_LOGIN_DASHBOARD);
		editor.commit();
	}

	public static void clearAnimatorPendingGroupValues() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(PREF_ANIMATOR_PENDING_GROUP);
		editor.commit();
	}

	public static void clearGroupMasterResValues() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(PREF_GROUP_MASTER_RESPONSE_VALUES);
		editor.commit();
	}

	public static void clearGrouplistValues() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(PREF_GROUPLIST_VALUES);
		editor.commit();
	}

	public static void clearLoginValues() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(PREF_LOGIN_VALUES);
		editor.commit();
	}

	public static void clearAnimatorOfflineDataValues() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(ANIMATOR_OFFLINE_DATA_VALUES);
		editor.commit();
	}

	public static void clearAnimatorTargetValues() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(ANIMATOR_TARGET_VALUES);
		editor.commit();
	}

	public static void clearAnimatorName() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(ANIMATOR_NAME);
		editor.commit();
	}

	public static void clearLoginGroupService() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(LOGIN_GROUP_SERVICE);
		editor.commit();
	}

	public static void clearGroup_Offlinedata() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(GROUP_OFFLINEDATA_VALUE);
		editor.commit();
	}

	public static void clearAppUsertype() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(APP_DEVICE_ADMIN);
		editor.commit();
	}

	public static void clearAppDeviceAdmin() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(APP_DEVICE_ADMIN);
		editor.commit();
	}

	public static void clearMemberLoanrepay() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(OFFLINE_MEMBERLOANREPAY);
		editor.commit();
	}

	public static void clearMemberLoanId() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(OFFLINE_MEMLOANID);
		editor.commit();
	}

	public static void clearGroupLoanOSId() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(OFFLINE_GROUPLOSID);
		editor.commit();
	}

	public static void clearGroupLoanOsrepay() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(OFFLINE_GROUPLOSREPAY);
		editor.commit();
	}

	public static void clearBankDeposit() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(OFFLINE_BANKDEPOSIT);
		editor.commit();
	}

	public static void clearBankDepositName() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(OFFLINE_BANKDEPOSITNAME);
		editor.commit();
	}

	public static void clearBankFixedDeposit() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(OFFLINE_BANK_FIXED_DEPOSIT);
		editor.commit();
	}

	public static void clearBankFixedDepositName() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(OFFLINE_BANK_FIXED_DEPOSITNAME);
		editor.commit();
	}

	public static void clearTransactionId() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(OFFLINE_TRANSACTION_ID);
		editor.commit();
	}

	public static void clearUserId() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(USERNAME_KEY);
		editor.commit();
	}

	public static void clearUserLanguageCode() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(KEY_USER_LANGUAGE_CODE);
		editor.commit();
	}
	/*
	 * Permission Pref.Utils
	 */

	public static String getLocationPermission() {
		return mApplicationSharedPref.getString(APP_LOCATION_PERMISSION, null);
	}

	public static void setLocationPermission(String Language) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(APP_LOCATION_PERMISSION, Language);
		mEditor.commit();
	}

	public static void clearLocationPermission() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(APP_LOCATION_PERMISSION);
		editor.commit();
	}

	public static String getStoragePermission() {
		return mApplicationSharedPref.getString(APP_STORAGE_PERMISSION, null);
	}

	public static void setStoragePermission(String storage) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(APP_STORAGE_PERMISSION, storage);
		mEditor.commit();
	}

	public static void clearStoragePermission() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(APP_STORAGE_PERMISSION);
		editor.commit();
	}

	public static String getPhonePermission() {
		return mApplicationSharedPref.getString(APP_PHONE_PERMISSION, null);
	}

	public static void setPhonePermission(String phone) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(APP_PHONE_PERMISSION, phone);
		mEditor.commit();
	}

	public static void clearPhonePermission() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(APP_PHONE_PERMISSION);
		editor.commit();
	}

	public static String getCameraPermission() {
		return mApplicationSharedPref.getString(APP_CAMERA_PERMISSION, null);
	}

	public static void setCameraPermission(String Camera) {
		mEditor = mApplicationSharedPref.edit();
		mEditor.putString(APP_CAMERA_PERMISSION, Camera);
		mEditor.commit();
	}

	public static void clearCameraPermission() {
		Editor editor = mApplicationSharedPref.edit();
		editor.remove(APP_CAMERA_PERMISSION);
		editor.commit();
	}

}

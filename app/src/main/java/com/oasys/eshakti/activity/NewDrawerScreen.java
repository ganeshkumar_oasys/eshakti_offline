package com.oasys.eshakti.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.Adapter.DashboardAdapter;
import com.oasys.eshakti.Adapter.DashboardDto;
import com.oasys.eshakti.Dialogue.Dialog_New_TransactionDate;
import com.oasys.eshakti.Dto.ExpensesTypeDtoList;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.Dto.TrainingsList;
import com.oasys.eshakti.Dto.offlineDto.SavingsDetails;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.AppialogUtilslogout;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.CustomExpandableListAdapter;
import com.oasys.eshakti.OasysUtils.FragmentNavigationManager;
import com.oasys.eshakti.OasysUtils.GetExit;
import com.oasys.eshakti.OasysUtils.GetTypeface;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NavigationManager;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.RegionalConversion;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.database.ExpenseTable;
import com.oasys.eshakti.database.MemberTable;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.database.TransactionTable;
import com.oasys.eshakti.fragment.BankTransactionReportList;
import com.oasys.eshakti.fragment.CheckList;
import com.oasys.eshakti.fragment.Contacts;
import com.oasys.eshakti.fragment.CreditLinkage;
import com.oasys.eshakti.fragment.DeactivateAccount;
import com.oasys.eshakti.fragment.EShaktiApp;
import com.oasys.eshakti.fragment.GroupProfile;
import com.oasys.eshakti.fragment.GroupReportFragment;
import com.oasys.eshakti.fragment.MainFragment;
import com.oasys.eshakti.fragment.MemberAcountNumberUpdationFragment;
import com.oasys.eshakti.fragment.MemberDetails;
import com.oasys.eshakti.fragment.MemberReportFragment;
import com.oasys.eshakti.fragment.OfflineReport_Date;
import com.oasys.eshakti.fragment.ProfileMobileUpdate;
import com.oasys.eshakti.fragment.Profile_SHGAccountnumber_updation;
import com.oasys.eshakti.fragment.Reports_BankBalance_Fragment;
import com.oasys.eshakti.fragment.Settings_Change_Password_Fragment;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.fragment.ShgsbAcUploadPassbook;
import com.oasys.eshakti.fragment.SocialSecurityMenu;
import com.oasys.eshakti.fragment.UploadPassbook;
import com.oasys.eshakti.fragment.UploadScedule;
import com.oasys.eshakti.fragment.VideoListFragment;
//import com.oasys.eshakti.fragment.profile_ShgAccountNumber_updation;
import com.oasys.eshakti.views.ButtonFlat;
import com.tutorialsee.lib.TastyToast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class NewDrawerScreen extends AppCompatActivity implements View.OnClickListener, NewTaskListener, MainFragment.OnDataPass {

    public static final String MOBILE_NUMBER_UPDATION = "MOBILE NUMBER UPDATION";
    public static final String SOCIAL_SECURITY = "SOCIAL SECURITY";
    public static final String CREDIT_LINKAGE_INFO = "CREDIT LINKAGE INFO";
    public static final String AADHAR_NUMBER_UPDATION = "AADHAR NUMBER UPDATION";
    public static final String MEMBER_ACCOUNT_NUMBER_UPDATION = "MEMBER ACCOUNT NUMBER UPDATION";
    public static final String SHG_ACCOUNT_NUMBER_UPDATION = "SHG ACCOUNT NUMBER UPDATION";
    public static final String MEMBER_DETAILS = "MEMBER DETAILS";
    public static final String GROUP_PROFILE = "GROUP PROFILE";
    public static final String ANIMATOR_PROFILE = "ANIMATOR PROFILE";
    public static final String SAVINGS = "SAVINGS";
    public static final String INCOME = "INCOME";
    public static final String EXPENCE = "EXPENSES";
    public static final String MEMBER_LOAN_REPAYMENT = "MEMBER LOAN REPAYMENT";
    public static final String GROUP_LOAN_REPAYMENT = "GROUP LOAN REPAYMENT";
    public static final String BANK_TRANSACTION = "BANK TRANSACTION";
    public static final String LOAN_DISBURSEMENT = "LOAN DISBURSEMENT";
    public static final String STEP_WISE = "STEP WISE";
    public static final String TRANSACTION = "TRANSACTION";
    public static final String PROFILE = "PROFILE";
    public static final String REPORTS = "REPORTS";
    public static final String MEETINGS = "MEETING";
    public static final String SETTINGS = "SETTINGS";
    public static final String HELP = "HELP";
    public static final String SELECT_GROUP = "SELECT GROUP";
    public static final String LOG_OUT = "LOG OUT";
    public static final String OFFLINE_REPORTS = "OFFLINE REPORTS";
    public static final String ATTENDANCE = "ATTENDANCE";
    public static final String MINUTES_OF_MEETINGS = "MINUTES OF MEETING";
    public static final String AUDITING = "AUDITING";
    public static final String TRAINING = "TRAINING";
    public static final String CHANGE_PASSWORD = "CHANGE PASSWORD";
    public static final String ESHKTHI = "ESHKTHI";
    public static final String CONTACTS = "CONTACTS";
    public static final String PDF_MANUAL = "PDF MANUAL";
    public static final String MEMBER_ACCOUNT_UPDATION = "MEMBER ACCOUNT UPDATION";
    public static final String CHECK_LIST = "CHECK LIST";
    public static final String MEMBER_REPORTS = "MEMBER REPORTS";
    public static final String GROUP_REPORTS = "GROUP REPORTS";
    public static final String BANK_TRANSACTION_SUMMARY = "BANK TRANSACTION SUMMARY";
    public static final String BANK_BALANCE = "BANK BALANCE";
    public static final String UPLOAD_SCHEDULE_VI = "UPLOAD SCHEDULE VI";
    public static final String CHANGE_LANGUAGE = "CHANGE LANGUAGE";
    public static final String DE_ACTIVATE_ACCOUNT = "DEACTIVATE ACCOUNT";
    public static final String E_MAIL = "E-MAIL";
    public static final String ESHKTHI1 = "ESHKTHI";
    public static final String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";

    public static DrawerLayout mDrawerLayout;
    public static ActionBarDrawerToggle mActionBarDrawerToggle;
    public static Toolbar mToolbarDashboard;
    private ExpandableListView expandableListView;
    private ExpandableListAdapter expandableListAdapter;
    private NavigationManager navigationManager;
    private List<String> lstTitle;
    private Map<String, List<String>> childList;
    public static ImageView mMenuDashboard, mMenuLogout;
    private int lastExpandedPosition = -1;
    private RelativeLayout mDisplayPopup;
    private LinearLayout mTransaction;
    private LinearLayout mProfile;
    private LinearLayout mReports;
    private LinearLayout mMeeting;
    private LinearLayout mSeeting;
    private LinearLayout mHelp;
    private LinearLayout mCheckbacklog;
    private RecyclerView recyclerView_dash_board;
    private LinearLayout mDashItemnamelayout;
    private LinearLayout mRecyclerviewlayout;
    private LinearLayout controller;
    private ExpandableListView navList;
    private DrawerLayout drawer_layout;
    private DashboardAdapter dashboardAdapter;
    private LinearLayoutManager linearLayoutManager;

    private ArrayList<DashboardDto> dashboardDtos;
    private TextView mDashItemname, name;
    private int backStackEntryCount;
    private static FragmentManager fm;
    private TextView shg_name;
    private TextView last_trans_date;
    private TextView op_date_txt;
    private TextView cih_txt;
    private TextView cah_txt, cih_h, cih_b, op_date, lt_date;
    private TextView no;
    public static ListOfShg shgDetails;
    private TextView a_name;
    private Dialog mProgressDilaog;

    /*TestCommit*/

    private NetworkConnection networkConnection;
    private TextView ver;
    public NewDrawerScreen context;
    private boolean completionFlag = false;
    private boolean isPdfDownload = false;
    String nullVlaue = "0";
    private String  openflag="0";
    public static MenuItem item;
    public static MenuItem  item1;
    public static MenuItem  item2;
    public static MenuItem logOutItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_drawer_screen);        //  initView();

//        EShaktiApplication.flag = "0";


        fm = getSupportFragmentManager();

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null)
        {
             openflag = bundle.getString("Openingdate");
        }

        mToolbarDashboard = (Toolbar) findViewById(R.id.mToolbarDashboard);
        setSupportActionBar(mToolbarDashboard);


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        expandableListView = (ExpandableListView) findViewById(R.id.navList);
        mMenuDashboard = (ImageView) findViewById(R.id.mMenuDashboard);
        name = (TextView) findViewById(R.id.name);
        ver = (TextView) findViewById(R.id.version);

        shg_name = (TextView) findViewById(R.id.shg_name);
        a_name = (TextView) findViewById(R.id.a_name);
        last_trans_date = (TextView) findViewById(R.id.last_trans_date);
        op_date_txt = (TextView) findViewById(R.id.op_date_txt);
        cih_txt = (TextView) findViewById(R.id.cih_txt);
        cah_txt = (TextView) findViewById(R.id.cah_txt);
        no = (TextView) findViewById(R.id.no);

        cih_h = (TextView) findViewById(R.id.cih_h);
        cih_b = (TextView) findViewById(R.id.cih_b);
        op_date = (TextView) findViewById(R.id.op_date);
        lt_date = (TextView) findViewById(R.id.lt_date);

        cih_h.setTypeface(LoginActivity.sTypeface);
        cih_b.setTypeface(LoginActivity.sTypeface);
        op_date.setTypeface(LoginActivity.sTypeface);
        lt_date.setTypeface(LoginActivity.sTypeface);
        shg_name.setTypeface(LoginActivity.sTypeface);

        cih_h.setText(AppStrings.cashinhand);
        cih_b.setText(AppStrings.cashatBank);
        op_date.setText(AppStrings.mOpeningDate + " : ");
        lt_date.setText(AppStrings.lastTransactionDate + " : ");

       /* shg_name.setTypeface(LoginActivity.sTypeface);
        a_name.setTypeface(LoginActivity.sTypeface);
        last_trans_date.setTypeface(LoginActivity.sTypeface);
        op_date_txt.setTypeface(LoginActivity.sTypeface);
        cih_txt.setTypeface(LoginActivity.sTypeface);
        cah_txt.setTypeface(LoginActivity.sTypeface);
        no.setTypeface(LoginActivity.sTypeface);*/


        networkConnection = NetworkConnection.getNetworkConnection(getApplicationContext());
        navigationManager = FragmentNavigationManager.getInstance(this);
        name.setText("ESHAKTI");
        name.setTypeface(LoginActivity.sTypeface);

        shgDetails = SHGTable.getSHGDetails(MySharedPreference.readString(this, MySharedPreference.SHG_ID, ""));
        setNavigationHeaderValues();

       /* runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();
                    completionFlag = false;

                    RestClient.getRestClient(NewDrawerScreen.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GET_EXPENSE, NewDrawerScreen.this, ServiceType.EXPENSE);
                    RestClient.getRestClient(NewDrawerScreen.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.Get_Member + shgDetails.getId(), NewDrawerScreen.this, ServiceType.MEMBER_LIST);
                }
            }
        });
*/
        if (networkConnection.isNetworkAvailable()) {
            genData();
            mToolbarDashboard.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        } else {
            genOfflineData();
            mToolbarDashboard.setBackgroundColor(getResources().getColor(R.color.help_bordercolor));
        }

        addDrawersItem();
        setUpDrawer();
        if (savedInstanceState == null) {
            selectFirstItemAsDefault();
        }


        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {

            @Override
            public void onBackStackChanged() {
                // TODO Auto-generated method stub
                backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
                Log.i(NewDrawerScreen.this.getClass().getName(), "Count value is --> " + backStackEntryCount);
                //  fragmentName = getSupportFragmentManager().getFragments();

                if (backStackEntryCount != 0) {
                    return;
                } else {
                    Fragment fragment = new MainFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.static_frame, fragment)
                            .setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out)
                            .show(fragment).commit();
                }
            }
        });

        String versionName = "0.0";
        int versionCode = -1;
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
            versionCode = packageInfo.versionCode;
            ver.setText(AppStrings.version + versionName);
            ver.setTypeface(LoginActivity.sTypeface);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        showFragment(new MainFragment());

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        item = menu.getItem(0);
        item1 =  menu.findItem(R.id.action_home).setVisible(true);
        item.setVisible(true);
        logOutItem = menu.getItem(1);
        item2 = menu.findItem(R.id.action_logout).setVisible(true);
        logOutItem.setVisible(true);


        SpannableStringBuilder SS = new SpannableStringBuilder(AppStrings.groupList);
        SpannableStringBuilder logOutBuilder = new SpannableStringBuilder(AppStrings.logOut);

        if (item.getItemId() == R.id.action_grouplist) {
            item.setTitle(SS);
        }

        if (logOutItem.getItemId() == R.id.menu_logout) {
            logOutItem.setTitle(logOutBuilder);
        }

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_grouplist) {

            try {

              /*  if (EShaktiApplication.getFlag()!=null && EShaktiApplication.getFlag().trim().equals("1") ) {
                    selectGroup(NewDrawerScreen.this);
//                    Toast.makeText(getApplicationContext(), "your way is Stepwise", Toast.LENGTH_SHORT).show();
                }
                else
                {*/
                    startActivity(new Intent(NewDrawerScreen.this, SHGGroupActivity.class));
                    finish();
//                }

            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
/*                if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
                    PrefUtils.setLoginGroupService("2");
                    new Login_webserviceTask(MainActivity.this).execute();
                } else {
                    startActivity(new Intent(this, SHGGroupActivity.class));
                    overridePendingTransition(R.anim.right_to_left_in, R.anim.right_to_left_out);
                    finish();
                }*/
            return true;

        } else if (id == R.id.action_home) {

            Intent intent = new Intent(NewDrawerScreen.this, NewDrawerScreen.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

            finish();
            return true;

        } else if (id == R.id.menu_logout) {
            Log.e(" Logout", "Logout Sucessfully");
//            MySharedPreference.writeBoolean(this, MySharedPreference.LOGOUT, true);
//            startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
//            this.finish();
            try {

                if (networkConnection.isNetworkAvailable()) {

                    RestClient.getRestClient(NewDrawerScreen.this).callRestWebServiceForDelete(Constants.BASE_URL + Constants.LOGOUT_TOKENDELETION, NewDrawerScreen.this, ServiceType.LOG_OUT, "");

                } else {
                    startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
                    this.finish();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return true;

        } else if (id == R.id.action_logout) {
            MySharedPreference.writeBoolean(this, MySharedPreference.LOGOUT, true);

            AppialogUtilslogout.showConfirmation_LogoutDialog(NewDrawerScreen.this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showConfirmation_LogoutDialog1(final Activity activity) {

        try {
            final Dialog confirmationDialog = new Dialog(activity);

            LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View customView = li.inflate(R.layout.dialog_new_logout, null, false);
            final TextView mHeaderView, mMessageView;

            mHeaderView = (TextView) customView.findViewById(R.id.dialog_Title_logout);
            mMessageView = (TextView) customView.findViewById(R.id.dialog_Message_logout);


            final ButtonFlat mConYesButton, mConNoButton;
            mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert_logout);
            mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert_logout);

            mConYesButton.setText(AppStrings.dialogOk);
            mConYesButton.setTypeface(LoginActivity.sTypeface);
            mConNoButton.setTypeface(LoginActivity.sTypeface);
            mConNoButton.setText(AppStrings.dialogNo);

            mHeaderView.setText(AppStrings.logOut);
            mHeaderView.setTypeface(LoginActivity.sTypeface);
            mHeaderView.setVisibility(View.INVISIBLE);

            if (MySharedPreference.readBoolean(activity, MySharedPreference.UNAUTH, false)) {
                mMessageView.setText(AppStrings.mUnAuth);
            } else if (MySharedPreference.readInteger(activity, MySharedPreference.NETWORK_MODE_FLAG, 0) > 0 && !MySharedPreference.readBoolean(activity, MySharedPreference.LOGOUT, false)) {
                MySharedPreference.writeBoolean(activity, MySharedPreference.LOGOUT, true);
                if (MySharedPreference.readInteger(activity, MySharedPreference.NETWORK_MODE_FLAG, 0) == 2) {
                    mMessageView.setText(AppStrings.mon_mode);
                } else if (MySharedPreference.readInteger(activity, MySharedPreference.NETWORK_MODE_FLAG, 0) == 1) {
                    mMessageView.setText(AppStrings.moff_mode);
                }
            } else if (MySharedPreference.readBoolean(activity, MySharedPreference.LOGOUT, false)) {
                mMessageView.setText(AppStrings.mAskLogout);
            }

            mMessageView.setTypeface(LoginActivity.sTypeface);
            mConYesButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    confirmationDialog.dismiss();

                    try {
                        if (MySharedPreference.readString(activity, MySharedPreference.SHG_ID, "") != null && MySharedPreference.readString(activity, MySharedPreference.SHG_ID, "").length() > 0) {
                            ListOfShg shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(activity, MySharedPreference.SHG_ID, ""));
                            if (shgDto != null && shgDto.getFFlag() != null && (shgDto.getFFlag().equals("1") || shgDto.getFFlag().equals("0"))) {
                                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                            }
                            TransactionTable.updateLoginflag(MySharedPreference.readString(activity, MySharedPreference.SHG_ID, ""));
                        }


                        if (mMessageView.getText().equals(AppStrings.mAskLogout))
                            MySharedPreference.writeBoolean(activity, MySharedPreference.LOGOUT, true);
                        MySharedPreference.writeBoolean(activity, MySharedPreference.SINGIN_DIFF, false);
                        MySharedPreference.writeString(activity, MySharedPreference.ANIMATOR_NAME, "");
                        //   MySharedPreference.writeString(context,MySharedPreference.USERNAME,"");
                        // MySharedPreference.writeString(activity, MySharedPreference.ANIMATOR_ID, "");
                        //MySharedPreference.writeString(activity, MySharedPreference.SHG_ID, "");
                        MySharedPreference.writeString(activity, MySharedPreference.CASHINHAND, "");
                        MySharedPreference.writeString(activity, MySharedPreference.CASHATBANK, "");
                        MySharedPreference.writeString(activity, MySharedPreference.LAST_TRANSACTION, "");

//                            activity.startActivity(new Intent(GetExit.getExitIntent(activity)));
//                            activity.finish();

                        try {

                            if (networkConnection.isNetworkAvailable()) {

                                RestClient.getRestClient(NewDrawerScreen.this).callRestWebServiceForDelete(Constants.BASE_URL + Constants.LOGOUT_TOKENDELETION, NewDrawerScreen.this, ServiceType.LOG_OUT, "");

                            } else {
                                activity.startActivity(new Intent(GetExit.getExitIntent(activity)));
                                activity.finish();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });

            mConNoButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    confirmationDialog.dismiss();
                    MySharedPreference.writeBoolean(activity, MySharedPreference.LOGOUT, false);
                }
            });

            confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            confirmationDialog.setCanceledOnTouchOutside(false);
            confirmationDialog.setContentView(customView);
            confirmationDialog.setCancelable(false);
            confirmationDialog.show();

        } catch (
                Exception E)

        {
            E.printStackTrace();
        }

    }


   /* @Override
    protected void onResume() {
        super.onResume();
        Log.d("resumevalue1","prabhu");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("resumevalue2","oasys");
    }*/

    private void setNavigationHeaderValues() {
        Log.e("Nav Menu update resume", "reached");
        shgDetails = SHGTable.getSHGDetails(MySharedPreference.readString(this, MySharedPreference.SHG_ID, ""));

        if (shgDetails.getLastTransactionDate() != null && !shgDetails.getLastTransactionDate().equals("NA") && shgDetails.getLastTransactionDate().length() > 0) {
            DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
            Date d = new Date(Long.parseLong(shgDetails.getLastTransactionDate()));
            String dateStr = simple.format(d);
            last_trans_date.setText(dateStr);
            name.setText("   "+RegionalConversion.getRegionalConversion(AppStrings.mAppName)+ "    " + dateStr);

//            Calendar calendar = Calendar.getInstance();
//            calendar.setTime(d);
//            calendar.add(Calendar.DATE, -1);
//            String yesterdayAsString = simple.format(calendar.getTime());
//            op_date_txt.setText(yesterdayAsString);

//            Log.d("flag",openflag);
//            if(openflag.equals("1"))
//            {
//                DateFormat simple1 = new SimpleDateFormat("dd/MM/yyyy");
//                String datesr=shgDetails.getOpeningDate();
//                Date d1 = new Date(Long.parseLong(datesr)*1000L);
//                Log.d("pruint",""+d1);
//                String dateStr1 = simple.format(d1);
//                op_date_txt.setText(dateStr1);
//            }
//            else
//            {
                DateFormat simple1 = new SimpleDateFormat("dd/MM/yyyy");
                Date d1 = new Date(Long.parseLong(shgDetails.getOpeningDate()));
                String dateStr1 = simple1.format(d1);
                op_date_txt.setText(dateStr1);
//            }

            /*DateFormat simple1 = new SimpleDateFormat("dd/MM/yyyy");
            String datesr=shgDetails.getOpeningDate();
            Date d1 = new Date(Long.parseLong(datesr)*1000);
            Log.d("pruint",""+d1);
            String dateStr1 = simple.format(d1);
            op_date_txt.setText(dateStr1);*/


        } else {
            last_trans_date.setText("NA");
            name.setText("ESHAKTI" + " ");
            op_date_txt.setText("NA");
        }
     /*   DateFormat simple = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
        Date dq = new Date(Long.parseLong(shgDetails.ge()));
        String dateStr_1 = simple.format(dq);*/
//        if (shgDetails.getGroupFormationDate() != null && shgDetails.getGroupFormationDate().length() > 0) {
//            DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
//            Date d = new Date(Long.parseLong(shgDetails.getOpeningDate()));
//            String dateStr = simple.format(d);
//            op_date_txt.setText(dateStr);
//        } else {
//            op_date_txt.setText("NA");
//        }
        if (shgDetails.getName() != null && shgDetails.getName().length() > 0)
            a_name.setText(shgDetails.getName() + " / " + shgDetails.getPresidentName());
        if (shgDetails.getName() != null && shgDetails.getName().length() > 0)
            shg_name.setText(MySharedPreference.readString(this, MySharedPreference.ANIMATOR_NAME, "") + " / ");
        if (shgDetails.getCashInHand() != null && shgDetails.getCashInHand().length() > 0) {

            //  cih_txt.setText(cih);
            String cih = shgDetails.getCashInHand() + "";
            if ((cih.equals("")) || (cih == null)) {
                cih = "0";
            }
            if (cih.matches("\\d*\\.?\\d+")) { // match
                int outStandingAmount = (int) Math.round(Double.parseDouble(cih));
                cih = String.valueOf(outStandingAmount);
                cih_txt.setText(cih);
            }

        }
        if (shgDetails.getCashAtBank() != null && shgDetails.getCashAtBank().length() > 0) {
            String cab = shgDetails.getCashAtBank();

            if ((cab.equals("")) || (cab == null)) {
                cab = nullVlaue;
            }

            if (cab.matches("\\d*\\.?\\d+")) { // match
                // a
                // decimal
                // number

                int outStandingAmount = (int) Math.round(Double.parseDouble(cab));
                cab = String.valueOf(outStandingAmount);
                cah_txt.setText(cab);
            }
            // cah_txt.setText(shgDetails.getCashAtBank());
        }
        if (MySharedPreference.readString(this, MySharedPreference.USERNAME, "") != null && MySharedPreference.readString(this, MySharedPreference.USERNAME, "").length() > 0)
            no.setText(MySharedPreference.readString(this, MySharedPreference.USERNAME, ""));
    }

    public void UIUpdate(ListOfShg shgDto) {
        try {
            shgDetails = shgDto;
            setNavigationHeaderValues();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void showFragment(Fragment fragment) {

        FragmentTransaction trans = fm.beginTransaction();
        trans.replace(R.id.static_frame, fragment);
        trans.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out);
        trans.addToBackStack(fragment.getClass().getName()).commit();


    }

   /* @Override
    protected void onResume() {
        super.onResume();
        Log.d("flagvalue",EShaktiApplication.flag);

        if (EShaktiApplication.flag!=null && EShaktiApplication.flag.trim().equals("1") ) {
            item1.setVisible(false);
            item2.setVisible(false);
            item.setVisible(false);
            logOutItem.setVisible(false);
            mMenuDashboard.setVisibility(View.INVISIBLE);
        }
        else
        {
            item1.setVisible(true);
            item2.setVisible(true);
            item.setVisible(true);
            logOutItem.setVisible(true);
            mMenuDashboard.setVisibility(View.VISIBLE);
        }
    }*/

    @Override
    public void onBackPressed() {
        Log.e("NewDrawerScreen", "onBackPressed.....");

        if (backStackEntryCount != 0) {
            Log.e("NewDrawerScreen", "onBackPressed 1.....");


                if (EShaktiApplication.getFlag() != null && EShaktiApplication.getFlag().trim().equals("1")) {

                }
                else {

                    super.onBackPressed();

                }
            } else {
                Log.e("NewDrawerScreen", "onBackPressed 2.....");
                Fragment fragment = new MainFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.static_frame, fragment)
                        .setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
                        .commit();
            }
        }

        /* if (backStackEntryCount == 1) {

            super.onBackPressed();
        } else*/

    


    private void genData() {

     /*   List<String> title = Arrays.asList(TRANSACTION, PROFILE, REPORTS, MEETINGS, SETTINGS, HELP, SELECT_GROUP, LOG_OUT);
        List<String> child_Item_One = Arrays.asList(SAVINGS, INCOME, EXPENCE, MEMBER_LOAN_REPAYMENT, GROUP_LOAN_REPAYMENT, BANK_TRANSACTION, LOAN_DISBURSEMENT, STEP_WISE);
        List<String> child_Item_Two = Arrays.asList(CREDIT_LINKAGE_INFO, MOBILE_NUMBER_UPDATION, AADHAR_NUMBER_UPDATION, MEMBER_ACCOUNT_UPDATION, MEMBER_DETAILS, GROUP_PROFILE, ANIMATOR_PROFILE);
        List<String> child_Item_Three = Arrays.asList(OFFLINE_REPORTS);
        List<String> child_Item_Four = Arrays.asList(ATTENDANCE, MINUTES_OF_MEETINGS, AUDITING, TRAINING);
        List<String> child_Item_Five = Arrays.asList(CHANGE_PASSWORD);
        List<String> child_Item_sIX = Arrays.asList(ESHKTHI, CONTACTS, PDF_MANUAL);
        List<String> child_Item_Seven = Arrays.asList();*/

        List<String> title = Arrays.asList(AppStrings.transaction, AppStrings.profile, AppStrings.reports, AppStrings.meeting, AppStrings.settings, AppStrings.help, AppStrings.groupList, AppStrings.logOut);
        List<String> child_Item_One = Arrays.asList(AppStrings.savings, AppStrings.income, AppStrings.expenses, AppStrings.memberloanrepayment, AppStrings.grouploanrepayment, AppStrings.bankTransaction, AppStrings.InternalLoanDisbursement, AppStrings.mDefault, AppStrings.mCheckList);
        List<String> child_Item_Two = Arrays.asList(AppStrings.social_security, AppStrings.mCreditLinkageInfo, AppStrings.mMobileNoUpdation, AppStrings.mAadhaarNoUpdation, AppStrings.mAccountNoUpdation,AppStrings.mshgsbacuploadpassbook,AppStrings.mSHGAccountNoUpdation, AppStrings.uploadInfo, AppStrings.groupProfile, AppStrings.agentProfile);
        List<String> child_Item_Three = Arrays.asList(AppStrings.Memberreports, AppStrings.GroupReports, AppStrings.transactionsummary, AppStrings.bankBalance, AppStrings.offlineReports);
        List<String> child_Item_Four = Arrays.asList(AppStrings.Attendance, AppStrings.MinutesofMeeting, AppStrings.auditing, AppStrings.training, AppStrings.upload_schedule,AppStrings.upload_passbook);
        List<String> child_Item_Five = Arrays.asList(AppStrings.passwordchange, AppStrings.changeLanguage, AppStrings.deactivateAccount, AppStrings.mSendEmail);
        List<String> child_Item_sIX = Arrays.asList(AppStrings.ESHKTHI1, AppStrings.contacts, AppStrings.mPdfManual,AppStrings.mvideos);
        List<String> child_Item_Seven = Arrays.asList();

        childList = new LinkedHashMap<>();
        childList.put(title.get(0), child_Item_One);
        childList.put(title.get(1), child_Item_Two);
        childList.put(title.get(2), child_Item_Three);
        childList.put(title.get(3), child_Item_Four);
        childList.put(title.get(4), child_Item_Five);
        childList.put(title.get(5), child_Item_sIX);
        childList.put(title.get(6), child_Item_Seven);
        childList.put(title.get(7), child_Item_Seven);
        lstTitle = new ArrayList<String>(childList.keySet());
    }

    private void genOfflineData() {

     /*   List<String> title = Arrays.asList(TRANSACTION, PROFILE, REPORTS, MEETINGS, SETTINGS, HELP, SELECT_GROUP, LOG_OUT);
        List<String> child_Item_One = Arrays.asList(SAVINGS, INCOME, EXPENCE, MEMBER_LOAN_REPAYMENT, GROUP_LOAN_REPAYMENT, BANK_TRANSACTION, LOAN_DISBURSEMENT, STEP_WISE);
        List<String> child_Item_Two = Arrays.asList(CREDIT_LINKAGE_INFO, MOBILE_NUMBER_UPDATION, AADHAR_NUMBER_UPDATION, MEMBER_ACCOUNT_UPDATION, MEMBER_DETAILS, GROUP_PROFILE, ANIMATOR_PROFILE);
        List<String> child_Item_Three = Arrays.asList(OFFLINE_REPORTS);
        List<String> child_Item_Four = Arrays.asList(ATTENDANCE, MINUTES_OF_MEETINGS, AUDITING, TRAINING);
        List<String> child_Item_Five = Arrays.asList(CHANGE_PASSWORD);
        List<String> child_Item_sIX = Arrays.asList(ESHKTHI, CONTACTS, PDF_MANUAL);
        List<String> child_Item_Seven = Arrays.asList();*/

        List<String> title = Arrays.asList(AppStrings.transaction, AppStrings.profile, AppStrings.reports, AppStrings.meeting, AppStrings.settings, AppStrings.help, AppStrings.groupList, AppStrings.logOut);
        List<String> child_Item_One = Arrays.asList(AppStrings.savings, AppStrings.income, AppStrings.expenses, AppStrings.memberloanrepayment, AppStrings.grouploanrepayment, AppStrings.bankTransaction, AppStrings.InternalLoanDisbursement, AppStrings.mDefault);
        List<String> child_Item_Two = Arrays.asList(AppStrings.social_security, AppStrings.mCreditLinkageInfo, AppStrings.mMobileNoUpdation, AppStrings.mAadhaarNoUpdation, AppStrings.mAccountNoUpdation,AppStrings.mshgsbacuploadpassbook,AppStrings.mSHGAccountNoUpdation, AppStrings.uploadInfo, AppStrings.groupProfile, AppStrings.agentProfile);
        List<String> child_Item_Three = Arrays.asList(AppStrings.offlineReports);
        List<String> child_Item_Four = Arrays.asList(AppStrings.Attendance, AppStrings.MinutesofMeeting, AppStrings.auditing, AppStrings.training);
        List<String> child_Item_Five = Arrays.asList(AppStrings.passwordchange);
        List<String> child_Item_sIX = Arrays.asList(AppStrings.ESHKTHI1, AppStrings.contacts, AppStrings.mPdfManual,AppStrings.mvideos);
        List<String> child_Item_Seven = Arrays.asList();

        childList = new LinkedHashMap<>();
        childList.put(title.get(0), child_Item_One);
        childList.put(title.get(1), child_Item_Two);
        childList.put(title.get(2), child_Item_Three);
        childList.put(title.get(3), child_Item_Four);
        childList.put(title.get(4), child_Item_Five);
        childList.put(title.get(5), child_Item_sIX);
        childList.put(title.get(6), child_Item_Seven);
        childList.put(title.get(7), child_Item_Seven);
        lstTitle = new ArrayList<String>(childList.keySet());
    }

    private void addDrawersItem() {
        mMenuDashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });

        expandableListAdapter = new CustomExpandableListAdapter(this, lstTitle, childList);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                Log.d("groupPosition", " " + groupPosition);
                switch (groupPosition) {
                    case 6:
                        startActivity(new Intent(NewDrawerScreen.this, SHGGroupActivity.class));
                        finish();
                        break;
                }
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
            }
        });

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                Log.d("groupPosition", " " + i);
                switch (i) {
                    case 6:
                        startActivity(new Intent(NewDrawerScreen.this, SHGGroupActivity.class));
                        finish();
                        break;
                    case 7:
                        MySharedPreference.writeBoolean(NewDrawerScreen.this, MySharedPreference.LOGOUT, true);
                        startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
                        finish();
                        break;
                }
                return false;
            }
        });


        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (networkConnection.isNetworkAvailable()) {
                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 1) {
                        AppDialogUtils.showConfirmation_LogoutDialog(NewDrawerScreen.this);
                        return false;
                    }
                    // NOTHING TO DO::
                } else {

                    if (MySharedPreference.readInteger(EShaktiApplication.getInstance(), MySharedPreference.NETWORK_MODE_FLAG, 0) != 2) {
                        AppDialogUtils.showConfirmation_LogoutDialog(NewDrawerScreen.this);
                        return false;
                    }

                }


                String selectedItem = ((List) (childList.get(lstTitle.get(groupPosition)))).get(childPosition).toString();
                mDrawerLayout.closeDrawer(GravityCompat.START);
                Log.d("childPosition", " " + selectedItem + " GP" + groupPosition + " CP" + childPosition + " id" + id);

                switch (groupPosition) {
                    case 0:

                        if (selectedItem.equals(AppStrings.savings)) {
                            try {
                                Log.i("print","Value : "+AppStrings.savings);
                                FragmentManager fm = getSupportFragmentManager();
                                //fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(NewDrawerScreen.this, NewDrawerScreen.SAVINGS);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        } else if (selectedItem.equals(AppStrings.income)) {
                            try {
                                FragmentManager fm = getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(NewDrawerScreen.this, NewDrawerScreen.INCOME);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        } else if (selectedItem.equals(AppStrings.expenses)) {
                            try {
                                FragmentManager fm = getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(NewDrawerScreen.this, NewDrawerScreen.EXPENCE);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;
                        } else if (selectedItem.equals(AppStrings.memberloanrepayment)) {
                            try {
                                FragmentManager fm = getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(NewDrawerScreen.this, NewDrawerScreen.MEMBER_LOAN_REPAYMENT);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;
                        } else if (selectedItem.equals(AppStrings.grouploanrepayment)) {
                            Log.d("profileCheck", "working");
                            try {
                                FragmentManager fm = getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(NewDrawerScreen.this, NewDrawerScreen.GROUP_LOAN_REPAYMENT);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //NewDrawerScreen.showFragment(new GroupLoanRepayment());
                            break;
                        } else if (selectedItem.equals(AppStrings.bankTransaction)) {
                            Log.d("profileCheck", "working");
                            try {
                                FragmentManager fm = getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(NewDrawerScreen.this, NewDrawerScreen.BANK_TRANSACTION);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        } else if (selectedItem.equals(AppStrings.InternalLoanDisbursement)) {
                            Log.d("profileCheck", "working");
                            try {
                                FragmentManager fm = getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(NewDrawerScreen.this, NewDrawerScreen.LOAN_DISBURSEMENT);

                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            break;
                        } else if (selectedItem.equals(AppStrings.mDefault)) {
                            try {
                                Log.i("print","String Value : "+AppStrings.mDefault);
                                FragmentManager fm = getSupportFragmentManager();
                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(NewDrawerScreen.this, NewDrawerScreen.STEP_WISE);
                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            break;
                        } else if (selectedItem.equals(AppStrings.mCheckList)) {
                            Log.d("CheckList", "working");
                            NewDrawerScreen.showFragment(new CheckList());
                            break;
                        }


                        break;

                    case 1:

                        //   AppStrings.social_security, AppStrings.mCreditLinkageInfo, AppStrings.mMobileNoUpdation, AppStrings.mAadhaarNoUpdation, AppStrings.mAccountNoUpdation, AppStrings.mSHGAccountNoUpdation, AppStrings.uploadInfo, AppStrings.groupProfile, AppStrings.agentProfile);

                        if (selectedItem.equals(AppStrings.social_security)) {
                            Log.d("profileCheck", "working");
                            if (networkConnection.isNetworkAvailable()) {
                                NewDrawerScreen.showFragment(new SocialSecurityMenu());
                            } else {
                                TastyToast.makeText(NewDrawerScreen.this, AppStrings.mSocialSecurityAlert,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        }
                        if (selectedItem.equals(AppStrings.mCreditLinkageInfo)) {
                            Log.d("profileCheck", "working");
                            if (networkConnection.isNetworkAvailable()) {
                                NewDrawerScreen.showFragment(new CreditLinkage());
                            } else {
                                TastyToast.makeText(NewDrawerScreen.this, AppStrings.mCreditLinkageAlert,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        } else if (selectedItem.equals(AppStrings.mMobileNoUpdation)) {
                            Log.d("profileCheck", "working");
                            if (networkConnection.isNetworkAvailable()) {
                                NewDrawerScreen.showFragment(new ProfileMobileUpdate());
                            } else {
                                TastyToast.makeText(NewDrawerScreen.this, AppStrings.mMobileNoUpdationNetworkCheck,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        } else if (selectedItem.equals(AppStrings.social_security)) {
                            Log.d("profileCheck", "working");
                            NewDrawerScreen.showFragment(new ProfileMobileUpdate());
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;

                        } else if (selectedItem.equals(AppStrings.mAadhaarNoUpdation)) {
                            if (networkConnection.isNetworkAvailable()) {
                                //   NewDrawerScreen.showFragment(new Animator());
//                                NewDrawerScreen.showFragment(new profile_ShgAccountNumber_updation());
                                TastyToast.makeText(NewDrawerScreen.this, "Page under construction!",
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            } else {
                                TastyToast.makeText(NewDrawerScreen.this, AppStrings.mAadhaarNoNetworkCheck,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }

                        } else if (selectedItem.equals(AppStrings.mAccountNoUpdation)) {
                            Log.d("profileCheck", "working");
                            if (networkConnection.isNetworkAvailable()) {
                                NewDrawerScreen.showFragment(new MemberAcountNumberUpdationFragment());
                            } else {
                                TastyToast.makeText(NewDrawerScreen.this, AppStrings.mAccNoNetworkCheck,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        }else if (selectedItem.equals(AppStrings.mshgsbacuploadpassbook)) {
                            Log.d("profileCheck", "working");
                            if (networkConnection.isNetworkAvailable()) {
                                NewDrawerScreen.showFragment(new ShgsbAcUploadPassbook());
                            } else {
                                TastyToast.makeText(NewDrawerScreen.this, AppStrings.mAccNoNetworkCheck,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        } else if (selectedItem.equals(AppStrings.mSHGAccountNoUpdation)) {
                            if (networkConnection.isNetworkAvailable()) {
//                                NewDrawerScreen.showFragment(new profile_ShgAccountNumber_updation());
                                NewDrawerScreen.showFragment(new Profile_SHGAccountnumber_updation());
                                Log.i("print","clicked");
                            } else {
                                TastyToast.makeText(NewDrawerScreen.this, AppStrings.mShgAccNoNetworkCheck,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }
                            break;
                        } else if (selectedItem.equals(AppStrings.uploadInfo)) {
                            NewDrawerScreen.showFragment(new MemberDetails());
                            break;
                        } else if (selectedItem.equals(AppStrings.groupProfile)) {
                            NewDrawerScreen.showFragment(new GroupProfile());
                            break;
                        } else if (selectedItem.equals(AppStrings.agentProfile)) {
                            if (networkConnection.isNetworkAvailable()) {
                                NewDrawerScreen.showFragment(new Animator());
                            } else {
                                TastyToast.makeText(NewDrawerScreen.this, "Agent profile is empty",
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }
                            break;
                        }

                        break;

                    case 2:

                        if (selectedItem.equals(AppStrings.Memberreports)) {
                            NewDrawerScreen.showFragment(new MemberReportFragment());
                            break;
                        } else if (selectedItem.equals(AppStrings.GroupReports)) {
                            Log.d("profileCheck", "working");
                            NewDrawerScreen.showFragment(new GroupReportFragment());
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        } else if (selectedItem.equals(AppStrings.transactionsummary)) {
                            Log.d("profileCheck", "working");
//                            NewDrawerScreen.showFragment(new Reports_BankTransactionSummary());
                            NewDrawerScreen.showFragment(new BankTransactionReportList());
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        } else if (selectedItem.equals(AppStrings.bankBalance)) {
                            Log.d("profileCheck", "working");
                            NewDrawerScreen.showFragment(new Reports_BankBalance_Fragment());
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        } else if (selectedItem.equals(AppStrings.offlineReports)) {
                            Log.d("profileCheck", "working");
                            NewDrawerScreen.showFragment(new OfflineReport_Date());
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        }


                        break;

                    case 3:

                        if (selectedItem.equals(AppStrings.Attendance)) {
                            Log.d("profileCheck", "working");
                            try {
                                FragmentManager fm = getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(NewDrawerScreen.this, NewDrawerScreen.ATTENDANCE);

                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // NewDrawerScreen.showFragment(new Attendance());
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        } else if (selectedItem.equals(AppStrings.MinutesofMeeting)) {
                            Log.d("profileCheck", "working");
                            Log.d("profileCheck", "working");
                            try {
                                FragmentManager fm = getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(NewDrawerScreen.this, NewDrawerScreen.MINUTES_OF_MEETINGS);

                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            // NewDrawerScreen.showFragment(new MinutesOFMeeting());
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        } else if (selectedItem.equals(AppStrings.auditing)) {
                            try {
                                FragmentManager fm = getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(NewDrawerScreen.this, NewDrawerScreen.AUDITING);

                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            break;
                        } else if (selectedItem.equals(AppStrings.training)) {
                            Log.d("profileCheck", "working");

                            try {
                                FragmentManager fm = getSupportFragmentManager();

                                Dialog_New_TransactionDate dialog = new Dialog_New_TransactionDate(NewDrawerScreen.this, NewDrawerScreen.TRAINING);

                                dialog.show(fm, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            break;
                        } else if (selectedItem.equals(AppStrings.upload_schedule)) {
                            NewDrawerScreen.showFragment(new UploadScedule());
                            break;
                        }
                        else if (selectedItem.equals(AppStrings.upload_passbook)) {
                            NewDrawerScreen.showFragment(new UploadPassbook());
                            break;

                        }
                        break;

                    case 4:

                        if (selectedItem.equals(AppStrings.passwordchange)) {

                            if (networkConnection.isNetworkAvailable()) {
                                NewDrawerScreen.showFragment(new Settings_Change_Password_Fragment());
                            } else {
                                TastyToast.makeText(NewDrawerScreen.this, AppStrings.offline_ChangePwdAlert,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }
                            break;
                        } else if (selectedItem.equals(AppStrings.changeLanguage)) {
                            Log.d("ChangeLanguage", "working");
                            if (networkConnection.isNetworkAvailable()) {
                                onTaskStarted();
                                RestClient.getRestClient(NewDrawerScreen.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GET_LANGUAGE + MySharedPreference.readString(NewDrawerScreen.this, MySharedPreference.ANIMATOR_ID, ""), NewDrawerScreen.this, ServiceType.GETLANGUAGE);
                            }
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        } else if (selectedItem.equals(AppStrings.deactivateAccount)) {
                            NewDrawerScreen.showFragment(new DeactivateAccount());
                            break;
                        } else if (selectedItem.equals(AppStrings.mSendEmail)) {
                            if (NetworkConnection.getNetworkConnection(NewDrawerScreen.this).isNetworkAvailable()) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(NewDrawerScreen.this);
                                builder.setMessage("Do you want to send an email with database attachment?.");

                                String positiveText = "YES";
                                builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // positive button logic
                                        // sendEmailWithDBAttachment(getActivity());
                                        Utils.sendFile(NewDrawerScreen.this);
                                    }
                                });

                                String negativeText = "NO";
                                builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // negative button logic
                                        dialog.dismiss();
                                    }
                                });

                                AlertDialog dialog = builder.create();
                                // display dialog
                                dialog.show();

                                TextView dialogMessage = (TextView) dialog.findViewById(android.R.id.message);
                                dialogMessage.setTypeface(LoginActivity.sTypeface);

                                Button yesButton = (Button) dialog.findViewById(android.R.id.button1);
                                yesButton.setTypeface(LoginActivity.sTypeface);

                                Button noButton = (Button) dialog.findViewById(android.R.id.button2);
                                noButton.setTypeface(LoginActivity.sTypeface);
                            } else {
                                TastyToast.makeText(NewDrawerScreen.this, AppStrings.mCommonNetworkErrorMsg, TastyToast.LENGTH_SHORT,
                                        TastyToast.WARNING);
                            }
                        }

                        break;

                    case 5:


                        if (selectedItem.equals(ESHKTHI1)) {
                            Log.d("profileCheck", "working");
                            NewDrawerScreen.showFragment(new EShaktiApp());
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        } else if (selectedItem.equals(AppStrings.contacts)) {
                            NewDrawerScreen.showFragment(new Contacts());
                            break;
                        } else if (selectedItem.equals(AppStrings.mPdfManual)) {
                            CopyReadAssets();
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        }
                        else if (selectedItem.equals(AppStrings.mvideos)) {
                            NewDrawerScreen.showFragment(new VideoListFragment());
                            // Utils.startActivity(NewDrawerScreen.this, Profile_MobileNumber_Updation.class);
                            break;
                        }


                        break;


                }

                return false;
            }
        });
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    expandableListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

    }


    private void selectGroup(final Activity activity)
    {
        final Dialog confirmationDialog = new Dialog(activity);

        LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = li.inflate(R.layout.dialog_new_logout, null, false);
        final TextView mHeaderView, mMessageView;

        mHeaderView = (TextView) customView.findViewById(R.id.dialog_Title_logout);
        mMessageView = (TextView) customView.findViewById(R.id.dialog_Message_logout);


        final ButtonFlat mConYesButton, mConNoButton;

        mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert_logout);
        mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert_logout);

        mConYesButton.setText(AppStrings.dialogOk);
        mConNoButton.setText(AppStrings.dialogNo);
        mConNoButton.setVisibility(View.VISIBLE);
        mConYesButton.setTypeface(LoginActivity.sTypeface);
        mConNoButton.setTypeface(LoginActivity.sTypeface);

        mHeaderView.setText("STEPWISE CONFIRMATION");
        mMessageView.setText("THE TRANSACTION ENTERED WILL NOT BE STORED.DO YOU WANT TO CONTINUE?");
        mMessageView.setTypeface(LoginActivity.sTypeface);

        mConYesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SavingsDetails base=new SavingsDetails();
                Log.d("datas",""+base);
                EShaktiApplication.setFlag("0");
                startActivity(new Intent(NewDrawerScreen.this, SHGGroupActivity.class));
                finish();
                confirmationDialog.dismiss();
            }
        });

        mConNoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                confirmationDialog.dismiss();
            }
        });

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(customView);
        confirmationDialog.setCancelable(false);
        confirmationDialog.show();

    }
    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }


    private void CopyReadAssets() {
        // File file = new File(Environment.getExternalStorageDirectory(), "mobileapplication.pdf");
        try {
            AssetManager assetManager = getAssets();
            InputStream in = null;
            OutputStream out = null;
            File fileAssets = new File(getFilesDir(), "mobileapplication.pdf");
            try {
                in = assetManager.open("mobileapplication.pdf");
                out = openFileOutput(fileAssets.getName(), Context.MODE_PRIVATE);
                copyFile(in, out);
                in.close();
                in = null;
                out.flush();
                out.close();
                out = null;
            } catch (Exception e) {
                Log.e("tag", e.getMessage());
            }

            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri fileUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", fileAssets);
            intent.setDataAndType(fileUri, "application/pdf");
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

            Intent intent1 = Intent.createChooser(intent, "Open File");

            PackageManager pm = getPackageManager();
            if (intent.resolveActivity(pm) != null) {
                startActivity(intent1);
            }


        } catch (ActivityNotFoundException e) {
            // TODO: handle exception
            TastyToast.makeText(this, "No pdf viewer installed, please install any pdf viewer.",
                    TastyToast.LENGTH_SHORT, TastyToast.ERROR);
        }
    }

    private void setUpDrawer() {
        mActionBarDrawerToggle = new ActionBarDrawerToggle(NewDrawerScreen.this, mDrawerLayout, R.string.open, R.string.exit) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                setNavigationHeaderValues();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };

        mActionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);

    }

    private void selectFirstItemAsDefault() {
        if (navigationManager != null) {
            String firstItem = lstTitle.get(0);
            navigationManager.showFragment(firstItem);
        }
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(this);
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {


        switch (serviceType) {
            case MEMBER_LIST:

                try {
                    if (result != null && result.length() > 0) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        ResponseDto mrDto = gson.fromJson(result, ResponseDto.class);
                        int statusCode = mrDto.getStatusCode();
                        String message = mrDto.getMessage();
                        //   String message=mrDto.getMessage();
                        Log.d("response status", " " + statusCode);
                        if (statusCode == 400 || statusCode == 403 || statusCode == 500 || statusCode == 503 || statusCode == 409) {
                            // showMessage(statusCode);
                            Utils.showToast(this, message);
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }

                        } else if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(NewDrawerScreen.this);
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        } else if (statusCode == Utils.Success_Code) {
                            Utils.showToast(this, message);
                            for (int i = 0; i < mrDto.getResponseContent().getMembers().size(); i++) {
                                MemberList memberDetails = mrDto.getResponseContent().getMembers().get(i);
                                MemberTable.insertMemberData(memberDetails);
                            }
                            completionFlag = true;


                        } else {
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                        mProgressDilaog.dismiss();
                        mProgressDilaog = null;
                    }
                }
                break;

            case EXPENSE:
                try {
                    if (result != null && result.length() > 0) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        ResponseDto mrDto = gson.fromJson(result, ResponseDto.class);
                        int statusCode = mrDto.getStatusCode();
                        String message = mrDto.getMessage();
                        Log.d("response status", " " + statusCode);
                        if (statusCode == 400 || statusCode == 403 || statusCode == 500 || statusCode == 503 || statusCode == 409) {
                            // showMessage(statusCode);
                            Utils.showToast(this, message);
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }

                        } else if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(NewDrawerScreen.this);
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        } else if (statusCode == Utils.Success_Code) {
                            Utils.showToast(this, message);
                            for (int i = 0; i < mrDto.getResponseContent().getExpensesTypeDTOList().size(); i++) {
                                ExpensesTypeDtoList expense = mrDto.getResponseContent().getExpensesTypeDTOList().get(i);
                                ExpenseTable.insertExpenseData(expense);
                            }
                            completionFlag = false;

                        } else {
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;


            case GETLANGUAGE:
                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    final ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                    String message = lrDto.getMessage();
                    int statusCode = lrDto.getStatusCode();


                    Log.d("response status", " " + statusCode);
                    if (statusCode == 400 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                        Utils.showToast(this, message);
                    } else if (statusCode == 401) {

                        Log.e("Group Logout", "Logout Sucessfully");
                        AppDialogUtils.showConfirmation_LogoutDialog(NewDrawerScreen.this);

                    } else {
                        Utils.showToast(this, message);

                        if (lrDto != null && lrDto.getResponseContent().getLanguageList() != null && lrDto.getResponseContent().getLanguageList().size() > 0) {

                            final ArrayList<TrainingsList> langList = lrDto.getResponseContent().getLanguageList();

                            for (TrainingsList ls : langList) {
                                if (!ls.getName().equals("English")) {
                                    EShaktiApplication.setUser_RegLanguage(ls.getName());
                                }
                            }


                            final Dialog ChangeLanguageDialog = new Dialog(this);

                            LayoutInflater li = (LayoutInflater) this
                                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View dialogView = li.inflate(R.layout.dialog_new_choose_language, null,
                                    false);

                            TextView confirmationHeader = (TextView) dialogView
                                    .findViewById(R.id.chooseLanguageHeader);
                            confirmationHeader.setText(RegionalConversion
                                    .getRegionalConversion(AppStrings.chooseLanguage));
                            confirmationHeader.setTypeface(LoginActivity.sTypeface);
                            final RadioGroup radioGroup = (RadioGroup) dialogView
                                    .findViewById(R.id.radioLanguage);
                            RadioButton radioButton = (RadioButton) dialogView
                                    .findViewById(R.id.radioEnglish);
                            RadioButton radioButton_reg = (RadioButton) dialogView
                                    .findViewById(R.id.radioRegional);
                            radioButton.setText("English");
                            //   radioButton.setTypeface(LoginActivity.sTypeface);
                            if (EShaktiApplication.getUser_RegLanguage() != null) {
                                radioButton_reg.setVisibility(View.VISIBLE);
                                if (EShaktiApplication.getUser_RegLanguage().equals("Hindi")) {
                                    radioButton_reg.setText("हिंदी");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/MANGAL.TTF");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Marathi")) {
                                    radioButton_reg.setText("मराठी");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/MANGALHindiMarathi.TTF");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Kannada")) {
                                    radioButton_reg.setText("ಕನ್ನಡ");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/tungaKannada.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Malayalam")) {
                                    radioButton_reg.setText("മലയാളം");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/MLKR0nttMalayalam.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Punjabi")) {
                                    radioButton_reg.setText("ਪੰਜਾਬੀ ");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/mangal-1361510185.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Gujarathi")) {
                                    radioButton_reg.setText("ગુજરાતી");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/shrutiGujarathi.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Bengali")) {
                                    radioButton_reg.setText("বাঙ্গালী");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/kalpurushBengali.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Tamil")) {
                                    radioButton_reg.setText("தமிழ்");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/TSCu_SaiIndira.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Assamese")) {
                                    radioButton_reg.setText("Assamese");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/KirtanUni_Assamese.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else {
                                    radioButton_reg.setText("ENGLISH");//TODO::
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/Exo-Medium.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                }
                            } else {
                                radioButton_reg.setVisibility(View.GONE);
                            }
                            // radioButton_reg.setTypeface(LoginActivity.sTypeface);

                            ButtonFlat okButton = (ButtonFlat) dialogView
                                    .findViewById(R.id.dialog_yes_button);
                            okButton.setText(
                                    RegionalConversion.getRegionalConversion(AppStrings.dialogOk));
                            okButton.setTypeface(LoginActivity.sTypeface);
                            okButton.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated
                                    // method
                                    // stub

                                    int selectedId = radioGroup.getCheckedRadioButtonId();
                                    RadioButton radioLanguageButton = (RadioButton) dialogView
                                            .findViewById(selectedId);
                                    String mSelectedLang = radioLanguageButton.getText().toString();                                    // find the radiobutton by

                                    Log.v("On Selected language", mSelectedLang);
                                    try {
                                        /**
                                         * THE LANGUAGE VALUE INSERTS INTO PREFERENCE
                                         **/
                                        if (mSelectedLang.equals("हिंदी")) {
                                            mSelectedLang = "Hindi";
                                        } else if (mSelectedLang.equals("मराठी")) {
                                            mSelectedLang = "Marathi";
                                        } else if (mSelectedLang.equals("ಕನ್ನಡ")) {
                                            mSelectedLang = "Kannada";
                                        } else if (mSelectedLang.equals("മലയാളം")) {
                                            mSelectedLang = "Malayalam";
                                        } else if (mSelectedLang.equals("ਪੰਜਾਬੀ")) {
                                            mSelectedLang = "Punjabi";
                                        } else if (mSelectedLang.equals("ગુજરાતી")) {
                                            mSelectedLang = "Gujarathi";
                                        } else if (mSelectedLang.equals("বাঙ্গালী")) {
                                            mSelectedLang = "Bengali";
                                        } else if (mSelectedLang.equals("தமிழ்")) {
                                            mSelectedLang = "Tamil";
                                        } else if (mSelectedLang.equals("Assamese")) {
                                            mSelectedLang = "Assamese";
                                        }

                                        for (TrainingsList ls : langList) {
                                            if (mSelectedLang.equals("हिंदी")) {
                                                mSelectedLang = "Hindi";
                                            } else if (mSelectedLang.equals("मराठी")) {
                                                mSelectedLang = "Marathi";
                                            } else if (mSelectedLang.equals("ಕನ್ನಡ")) {
                                                mSelectedLang = "Kannada";
                                            } else if (mSelectedLang.equals("മലയാളം")) {
                                                mSelectedLang = "Malayalam";
                                            } else if (mSelectedLang.equals("ਪੰਜਾਬੀ")) {
                                                mSelectedLang = "Punjabi";
                                            } else if (mSelectedLang.equals("ગુજરાતી")) {
                                                mSelectedLang = "Gujarathi";
                                            } else if (mSelectedLang.equals("বাঙ্গালী")) {
                                                mSelectedLang = "Bengali";
                                            } else if (mSelectedLang.equals("தமிழ்")) {
                                                mSelectedLang = "Tamil";
                                            } else if (mSelectedLang.equals("Assamese")) {
                                                mSelectedLang = "Assamese";
                                            }
                                            if (ls.getName().equals(mSelectedLang)) {
                                                MySharedPreference.writeString(NewDrawerScreen.this, MySharedPreference.LANG_ID, ls.getId());
                                            }
                                        }
                                        MySharedPreference.writeString(NewDrawerScreen.this, MySharedPreference.LANG, mSelectedLang);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    LoginActivity.sTypeface = GetTypeface
                                            .getTypeface(NewDrawerScreen.this, mSelectedLang);

                                    Intent intent_login = new Intent(NewDrawerScreen.this,
                                            SHGGroupActivity.class);
                                    intent_login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent_login);
                                    overridePendingTransition(R.anim.right_to_left_in,
                                            R.anim.right_to_left_out);
                                    finish();

                      /*  RegionalserviceUtil.getRegionalService(LoginActivity.this,
                                mSelectedLang);*/

                                    // isLanguageSelection =
                                    // true;


                                    ChangeLanguageDialog.dismiss();


                                }
                            });

                            ChangeLanguageDialog.getWindow().setBackgroundDrawable(
                                    new ColorDrawable(Color.TRANSPARENT));
                            ChangeLanguageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            ChangeLanguageDialog.setCanceledOnTouchOutside(false);
                            ChangeLanguageDialog.setContentView(dialogView);
                            ChangeLanguageDialog.setCancelable(false);
                            ChangeLanguageDialog.show();
                        }

                    }

                }

                break;

            case LOG_OUT:

                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();
                    Log.d("response status", " " + statusCode);

                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(this, message);

//                       startActivity(new Intent(NewDrawerScreen.this,SplashScreenActivity.class));

                        startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
                        this.finish();
                    }

                }

                break;

        }
        if (completionFlag) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
            }

        }

    }

    @Override
    public void onDataPass(ListOfShg data) {
        UIUpdate(data);
    }


// Dashboard


}

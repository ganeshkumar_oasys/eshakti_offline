package com.oasys.eshakti.activity;

import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;

import com.oasys.eshakti.EShaktiApplication;

public abstract class BaseActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
	}

	@Override
	protected void onStart() {
		super.onStart();

	}

	protected EShaktiApplication getApplciation() {
		return (EShaktiApplication) getApplication();
	}

}

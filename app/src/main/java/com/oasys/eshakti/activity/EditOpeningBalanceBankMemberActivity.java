package com.oasys.eshakti.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.VLoanTypes;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.GetExit;
import com.oasys.eshakti.OasysUtils.GetSpanText;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.R;
import com.oasys.eshakti.R.color;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.views.Get_EdiText_Filter;
import com.oasys.eshakti.views.RaisedButton;
import com.oasys.eshakti.views.TextviewUtils;
import com.tutorialsee.lib.TastyToast;


import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class EditOpeningBalanceBankMemberActivity extends AppCompatActivity implements OnClickListener {

    public static final String TAG = EditOpeningBalanceBankMemberActivity.class.getSimpleName();
    private TextView mGroupName, mHeaderTextView, mHeaderText;
    private TableLayout mHeaderTable, mContentTable;
    private RaisedButton mSubmit_RaisedButton;

    List<EditText> sSavingsFields;
    private EditText mSavings_values;
    int mSize;
    String[] mEditMasterValues;
    public static Vector<String> mInternalOSVectorMemberId, mInternalOSVectorLoanId;
    public static ArrayList<String> mInternalOSVector;
    String mLoanId = null, mLoanName = null;

    Dialog confirmationDialog;
    private static String sOutstandingAmounts[];
    public static String sSendToServer_Outstanding;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    String[] confirmArr;
    private String mAmount_Values;
    public static int sOutstandingAmount_Total;
    String nullVlaue = "0";
    String mLanguageLocalae;
    boolean mIsNegativeValues = false;
    LinearLayout mMemberNameLayout;
    TextView mMemberName;
//    private ArrayList<VLoanTypes> mEditInternalOS_indi;
    public static ArrayList<VLoanTypes> mEditInternalOS_indi;
    private ListOfShg shgDetails;
    private int termCount;
    private int ccCount;
    private int CifCount;
    private int rfaCount;
    private int bulkCount;
    private int mfiCount;
    private Toolbar mToolbar;
    private TextView mTitle;

    public EditOpeningBalanceBankMemberActivity() {
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        mInternalOSVectorMemberId.clear();
        mInternalOSVectorLoanId.clear();
        mInternalOSVector.clear();
        sOutstandingAmounts = null;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_new_ob_internalloan);
        mInternalOSVectorMemberId = new Vector<String>();
        mInternalOSVectorLoanId = new Vector<String>();
        mInternalOSVector = new ArrayList<>();
        sOutstandingAmount_Total = Integer.valueOf(nullVlaue);

        sSavingsFields = new ArrayList<EditText>();

        try {
            shgDetails = SHGTable.getSHGDetails(MySharedPreference.readString(this, MySharedPreference.SHG_ID, ""));

            mToolbar = (Toolbar) findViewById(R.id.toolbar_grouplist);
            mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
            mTitle.setText("ESHAKTI");
            mTitle.setGravity(Gravity.CENTER);

            mGroupName = (TextView) findViewById(R.id.groupname_edit_Internal);
            mGroupName.setText(String.valueOf(""));
            mGroupName.setText(shgDetails.getName() + " / " + shgDetails.getPresidentName());

            mHeaderTextView = (TextView) findViewById(R.id.fragmentHeader_edit_Internal);
            mHeaderTextView.setText(" " + (AppStrings.mMemberLoanOutstanding));
            mHeaderText = (TextView) findViewById(R.id.header_edit_Internal);

            mHeaderTable = (TableLayout) findViewById(R.id.headerTable_edit_Internal);
            mContentTable = (TableLayout) findViewById(R.id.contentTableLayout_edit_Internal);

            mMemberNameLayout = (LinearLayout) findViewById(R.id.member_name_layout);
            mMemberName = (TextView) findViewById(R.id.member_name);

            mSubmit_RaisedButton = (RaisedButton) findViewById(R.id.fragment_edit_Internal);
            mSubmit_RaisedButton.setText((AppStrings.mConfirm));
            mSubmit_RaisedButton.setOnClickListener(this);

            termCount = EShaktiApplication.vertficationDto.getListCountLoan().getTermLoan();
            mfiCount = EShaktiApplication.vertficationDto.getListCountLoan().getMFILoan();
            ccCount = EShaktiApplication.vertficationDto.getListCountLoan().getCashCredit();
            rfaCount = EShaktiApplication.vertficationDto.getListCountLoan().getRFA();
            bulkCount = EShaktiApplication.vertficationDto.getListCountLoan().getBulkLoan();
            CifCount = EShaktiApplication.vertficationDto.getListCountLoan().getCIF();  // TODO:; Dynamic


            if (termCount > 0) {
                if (termCount == 1) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan();
                    mHeaderText.setText("TermLoan " + (AppStrings.outstanding));
                } else if (termCount == 2) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan1();
                    mHeaderText.setText("TermLoan1 " + (AppStrings.outstanding));
                } else if (termCount == 3) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan2();
                    mHeaderText.setText("TermLoan2 " + (AppStrings.outstanding));
                } else if (termCount == 4) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan3();
                    mHeaderText.setText("TermLoan3 " + (AppStrings.outstanding));
                } else if (termCount == 5) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan4();
                    mHeaderText.setText("TermLoan4 " + (AppStrings.outstanding));
                } else if (termCount == 6) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan5();
                    mHeaderText.setText("TermLoan5 " + (AppStrings.outstanding));
                } else if (termCount == 7) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan6();
                    mHeaderTextView.setText("TermLoan6 " + (AppStrings.outstanding));
                } else if (termCount == 8) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan7();
                    mHeaderTextView.setText("TermLoan7 " + (AppStrings.outstanding));
                } else if (termCount == 9) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan8();
                    mHeaderTextView.setText("TermLoan8 " + (AppStrings.outstanding));
                } else if (termCount == 10) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan9();
                    mHeaderTextView.setText("TermLoan9 " + (AppStrings.outstanding));
                } else if (termCount == 11) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan10();
                    mHeaderTextView.setText("TermLoan10 " + (AppStrings.outstanding));
                } else if (termCount == 12) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan11();
                    mHeaderTextView.setText("TermLoan11 " + (AppStrings.outstanding));
                } else if (termCount == 13) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan12();
                    mHeaderTextView.setText("TermLoan12 " + (AppStrings.outstanding));
                } else if (termCount == 14) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan13();
                    mHeaderTextView.setText("TermLoan13 " + (AppStrings.outstanding));
                } else if (termCount == 15) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan14();
                    mHeaderTextView.setText("TermLoan14 " + (AppStrings.outstanding));
                }
                String[] mTempValues = new String[mEditInternalOS_indi.size()];
                for (int i = 0; i < mEditInternalOS_indi.size(); i++) {
                    mTempValues[i] = mEditInternalOS_indi.get(i).getAmount();
                    mInternalOSVector.add((mTempValues[i] != null && mTempValues[i].length() > 0) ? mTempValues[i] : "0.0");
                }

            } else if (mfiCount > 0) {

                if (mfiCount == 1) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan();
                    mHeaderText.setText("MFILoan " + (AppStrings.outstanding));
                } else if (mfiCount == 2) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan1();
                    mHeaderText.setText("MFILoan1 " + (AppStrings.outstanding));
                } else if (mfiCount == 3) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan2();
                    mHeaderText.setText("MFILoan2 " + (AppStrings.outstanding));
                } else if (mfiCount == 4) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan3();
                    mHeaderText.setText("MFILoan3 " + (AppStrings.outstanding));
                } else if (mfiCount == 5) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan4();
                    mHeaderText.setText("MFILoan4 " + (AppStrings.outstanding));
                } else if (mfiCount == 6) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan5();
                    mHeaderText.setText("MFILoan5 " + (AppStrings.outstanding));
                } else if (mfiCount == 7) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan6();
                    mHeaderText.setText("MFILoan6 " + (AppStrings.outstanding));
                } else if (mfiCount == 8) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan7();
                    mHeaderText.setText("MFILoan7 " + (AppStrings.outstanding));
                } else if (mfiCount == 9) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan8();
                    mHeaderText.setText("MFILoan8 " + (AppStrings.outstanding));
                } else if (mfiCount == 10) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan9();
                    mHeaderText.setText("MFILoan9 " + (AppStrings.outstanding));
                } else if (mfiCount == 11) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan10();
                    mHeaderText.setText("MFILoan10 " + (AppStrings.outstanding));
                } else if (mfiCount == 12) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan11();
                    mHeaderText.setText("MFILoan11 " + (AppStrings.outstanding));
                } else if (mfiCount == 13) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan12();
                    mHeaderText.setText("MFILoan12 " + (AppStrings.outstanding));
                } else if (mfiCount == 14) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan13();
                    mHeaderText.setText("MFILoan13 " + (AppStrings.outstanding));
                } else if (mfiCount == 15) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan14();
                    mHeaderText.setText("MFILoan14 " + (AppStrings.outstanding));
                }


                String[] mTempValues = new String[mEditInternalOS_indi.size()];
                for (int i = 0; i < mEditInternalOS_indi.size(); i++) {
                    mTempValues[i] = mEditInternalOS_indi.get(i).getAmount();
                    mInternalOSVector.add((mTempValues[i] != null && mTempValues[i].length() > 0) ? mTempValues[i] : "0.0");
                }
            } else if (ccCount > 0) {

                if (ccCount == 1) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit();
                    mHeaderText.setText("CashCreditLoan " + (AppStrings.outstanding));
                } else if (ccCount == 2) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit1();
                    mHeaderText.setText("CashCreditLoan1 " + (AppStrings.outstanding));
                } else if (ccCount == 3) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit2();
                    mHeaderText.setText("CashCreditLoan2 " + (AppStrings.outstanding));
                } else if (ccCount == 4) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit3();
                    mHeaderText.setText("CashCreditLoan3 " + (AppStrings.outstanding));
                } else if (ccCount == 5) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit4();
                    mHeaderText.setText("CashCreditLoan4 " + (AppStrings.outstanding));
                } else if (ccCount == 6) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit5();
                    mHeaderText.setText("CashCreditLoan5 " + (AppStrings.outstanding));
                } else if (ccCount == 7) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit6();
                    mHeaderText.setText("CashCreditLoan6 " + (AppStrings.outstanding));
                } else if (ccCount == 8) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit7();
                    mHeaderText.setText("CashCreditLoan7 " + (AppStrings.outstanding));
                } else if (ccCount == 9) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit8();
                    mHeaderText.setText("CashCreditLoan8 " + (AppStrings.outstanding));
                } else if (ccCount == 10) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit9();
                    mHeaderText.setText("CashCreditLoan9 " + (AppStrings.outstanding));
                } else if (ccCount == 11) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit10();
                    mHeaderText.setText("CashCreditLoan10 " + (AppStrings.outstanding));
                } else if (ccCount == 12) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit11();
                    mHeaderText.setText("CashCreditLoan11 " + (AppStrings.outstanding));
                } else if (ccCount == 13) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit12();
                    mHeaderText.setText("CashCreditLoan12 " + (AppStrings.outstanding));
                } else if (ccCount == 14) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit13();
                    mHeaderText.setText("CashCreditLoan13 " + (AppStrings.outstanding));
                } else if (ccCount == 15) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit14();
                    mHeaderText.setText("CashCreditLoan14 " + (AppStrings.outstanding));
                }

                String[] mTempValues = new String[mEditInternalOS_indi.size()];
                for (int i = 0; i < mEditInternalOS_indi.size(); i++) {
                    mTempValues[i] = mEditInternalOS_indi.get(i).getAmount();
                    mInternalOSVector.add((mTempValues[i] != null && mTempValues[i].length() > 0) ? mTempValues[i] : "0.0");
                }

            } else if (CifCount > 0) {

                if (CifCount == 1) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF();
                    mHeaderText.setText("CIFLoan " + (AppStrings.outstanding));
                } else if (CifCount == 2) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF1();
                    mHeaderText.setText("CIFLoan1 " + (AppStrings.outstanding));
                } else if (CifCount == 3) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF2();
                    mHeaderText.setText("CIFLoan2 " + (AppStrings.outstanding));
                } else if (CifCount == 4) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF3();
                    mHeaderText.setText("CIFLoan3 " + (AppStrings.outstanding));
                } else if (CifCount == 5) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF4();
                    mHeaderText.setText("CIFLoan4 " + (AppStrings.outstanding));
                } else if (CifCount == 6) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF5();
                    mHeaderText.setText("CIFLoan5 " + (AppStrings.outstanding));
                } else if (CifCount == 7) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF6();
                    mHeaderText.setText("CIFLoan6 " + (AppStrings.outstanding));
                } else if (CifCount == 8) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF7();
                    mHeaderText.setText("CIFLoan7 " + (AppStrings.outstanding));
                } else if (CifCount == 9) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF8();
                    mHeaderText.setText("CIFLoan8 " + (AppStrings.outstanding));
                } else if (CifCount == 10) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF9();
                    mHeaderText.setText("CIFLoan9 " + (AppStrings.outstanding));
                } else if (CifCount == 11) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF10();
                    mHeaderText.setText("CIFLoan10 " + (AppStrings.outstanding));
                } else if (CifCount == 12) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF11();
                    mHeaderText.setText("CIFLoan11 " + (AppStrings.outstanding));
                } else if (CifCount == 13) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF12();
                    mHeaderText.setText("CIFLoan12 " + (AppStrings.outstanding));
                } else if (CifCount == 14) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF13();
                    mHeaderText.setText("CIFLoan13 " + (AppStrings.outstanding));
                } else if (CifCount == 15) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF14();
                    mHeaderText.setText("CIFLoan14 " + (AppStrings.outstanding));
                }

                String[] mTempValues = new String[mEditInternalOS_indi.size()];
                for (int i = 0; i < mEditInternalOS_indi.size(); i++) {
                    mTempValues[i] = mEditInternalOS_indi.get(i).getAmount();
                    mInternalOSVector.add((mTempValues[i] != null && mTempValues[i].length() > 0) ? mTempValues[i] : "0.0");
                }

            } else if (rfaCount > 0) {

                if (rfaCount == 1) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA();
                    mHeaderText.setText("RFALoan " + (AppStrings.outstanding));
                } else if (rfaCount == 2) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA1();
                    mHeaderText.setText("RFALoan1 " + (AppStrings.outstanding));
                } else if (rfaCount == 3) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA2();
                    mHeaderText.setText("RFALoan2 " + (AppStrings.outstanding));
                } else if (rfaCount == 4) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA3();
                    mHeaderText.setText("RFALoan3 " + (AppStrings.outstanding));
                } else if (rfaCount == 5) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA4();
                    mHeaderText.setText("RFALoan4 " + (AppStrings.outstanding));
                } else if (rfaCount == 6) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA5();
                    mHeaderText.setText("RFALoan5 " + (AppStrings.outstanding));
                } else if (rfaCount == 7) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA6();
                    mHeaderText.setText("RFALoan6 " + (AppStrings.outstanding));
                } else if (rfaCount == 8) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA7();
                    mHeaderText.setText("RFALoan7 " + (AppStrings.outstanding));
                } else if (rfaCount == 9) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA8();
                    mHeaderText.setText("RFALoan8 " + (AppStrings.outstanding));
                } else if (rfaCount == 10) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA9();
                    mHeaderText.setText("RFALoan9 " + (AppStrings.outstanding));
                } else if (rfaCount == 11) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA10();
                    mHeaderText.setText("RFALoan10 " + (AppStrings.outstanding));
                } else if (rfaCount == 12) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA11();
                    mHeaderText.setText("RFALoan11 " + (AppStrings.outstanding));
                } else if (rfaCount == 13) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA12();
                    mHeaderText.setText("RFALoan12 " + (AppStrings.outstanding));
                } else if (rfaCount == 14) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA13();
                    mHeaderText.setText("RFALoan13 " + (AppStrings.outstanding));
                } else if (rfaCount == 15) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA14();
                    mHeaderText.setText("RFALoan14 " + (AppStrings.outstanding));
                }

                String[] mTempValues = new String[mEditInternalOS_indi.size()];
                for (int i = 0; i < mEditInternalOS_indi.size(); i++) {
                    mTempValues[i] = mEditInternalOS_indi.get(i).getAmount();
                    mInternalOSVector.add((mTempValues[i] != null && mTempValues[i].length() > 0) ? mTempValues[i] : "0.0");
                }

            } else if (bulkCount > 0) {

                if (bulkCount == 1) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan();
                    mHeaderText.setText("BulkLoan " + (AppStrings.outstanding));
                } else if (bulkCount == 2) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan1();
                    mHeaderText.setText("BulkLoan1 " + (AppStrings.outstanding));
                } else if (bulkCount == 3) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan2();
                    mHeaderText.setText("BulkLoan2 " + (AppStrings.outstanding));
                } else if (bulkCount == 4) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan3();
                    mHeaderText.setText("BulkLoan3 " + (AppStrings.outstanding));
                } else if (bulkCount == 5) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan4();
                    mHeaderText.setText("BulkLoan4 " + (AppStrings.outstanding));
                } else if (bulkCount == 6) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan5();
                    mHeaderText.setText("BulkLoan5 " + (AppStrings.outstanding));
                } else if (bulkCount == 7) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan6();
                    mHeaderText.setText("BulkLoan6 " + (AppStrings.outstanding));
                } else if (bulkCount == 8) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan7();
                    mHeaderText.setText("BulkLoan7 " + (AppStrings.outstanding));
                } else if (bulkCount == 9) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan8();
                    mHeaderText.setText("BulkLoan8 " + (AppStrings.outstanding));
                } else if (bulkCount == 10) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan9();
                    mHeaderText.setText("BulkLoan9 " + (AppStrings.outstanding));
                } else if (bulkCount == 11) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan10();
                    mHeaderText.setText("BulkLoan10 " + (AppStrings.outstanding));
                } else if (bulkCount == 12) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan11();
                    mHeaderText.setText("BulkLoan11 " + (AppStrings.outstanding));
                } else if (bulkCount == 13) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan12();
                    mHeaderText.setText("BulkLoan12 " + (AppStrings.outstanding));
                } else if (bulkCount == 14) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan13();
                    mHeaderText.setText("BulkLoan13 " + (AppStrings.outstanding));
                } else if (bulkCount == 15) {
                    mEditInternalOS_indi = EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan14();
                    mHeaderText.setText("BulkLoan14 " + (AppStrings.outstanding));
                }


                String[] mTempValues = new String[mEditInternalOS_indi.size()];
                for (int i = 0; i < mEditInternalOS_indi.size(); i++) {
                    mTempValues[i] = mEditInternalOS_indi.get(i).getAmount();
                    mInternalOSVector.add((mTempValues[i] != null && mTempValues[i].length() > 0) ? mTempValues[i] : "0.0");
                }

            } else if (EShaktiApplication.vertficationDto.getGroupfinancialDetails() != null && EShaktiApplication.vertficationDto.getGroupfinancialDetails().size() > 0) {


                EShaktiApplication.vertficationDto.getListCountLoan().setTermLoan(MySharedPreference.readInteger(this, MySharedPreference.TermLoan, 0));
                EShaktiApplication.vertficationDto.getListCountLoan().setMFILoan(MySharedPreference.readInteger(this, MySharedPreference.MFICount, 0));
                EShaktiApplication.vertficationDto.getListCountLoan().setCIF(MySharedPreference.readInteger(this, MySharedPreference.CifCount, 0));
                EShaktiApplication.vertficationDto.getListCountLoan().setRFA(MySharedPreference.readInteger(this, MySharedPreference.RFACount, 0));
                EShaktiApplication.vertficationDto.getListCountLoan().setCashCredit(MySharedPreference.readInteger(this, MySharedPreference.CCcount, 0));
                EShaktiApplication.vertficationDto.getListCountLoan().setBulkLoan(MySharedPreference.readInteger(this, MySharedPreference.BulkCount, 0));


                Log.i("print","GroupLoanActivity");
                Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
                        EditOpeningBalanceGroupLoanActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                finish();


            } else if (EShaktiApplication.vertficationDto.getShgBalanceDetailsDTO() != null) {

                Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
                        EditOpeningBalanceBankDetailsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                finish();


            }


            buildTableHeaderLayout();

            buildTableContentLayout();

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }

    private void buildTableHeaderLayout() {
        // TODO Auto-generated method stub

        TableRow leftHeaderRow = new TableRow(this);

        TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT, 1f);

        TextView mMemberName_headerText = new TextView(this);
        mMemberName_headerText.setText((String.valueOf(AppStrings.memberName)));
        mMemberName_headerText.setTextColor(Color.WHITE);
        mMemberName_headerText.setPadding(20, 5, 10, 5);
        mMemberName_headerText.setLayoutParams(lHeaderParams);
        leftHeaderRow.addView(mMemberName_headerText);

        TextView mSavingsAmount_HeaderText = new TextView(this);
        mSavingsAmount_HeaderText
                .setText((String.valueOf(AppStrings.OutstandingAmount)));
        mSavingsAmount_HeaderText.setTextColor(Color.WHITE);
        mSavingsAmount_HeaderText.setPadding(10, 5, 50, 5);
        mSavingsAmount_HeaderText.setLayoutParams(lHeaderParams);
        mSavingsAmount_HeaderText.setGravity(Gravity.RIGHT);
        mSavingsAmount_HeaderText.setSingleLine(true);
        leftHeaderRow.addView(mSavingsAmount_HeaderText);

        mHeaderTable.addView(leftHeaderRow);

    }

    @SuppressWarnings("deprecation")
    private void buildTableContentLayout() {
        // TODO Auto-generated method stub
        mSize = mInternalOSVector.size();
        Log.d(TAG, String.valueOf(mSize));

        for (int i = 0; i < mSize; i++) {
            TableRow leftContentRow = new TableRow(this);

            TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
            leftContentParams.setMargins(5, 5, 5, 5);

            final TextView memberName_Text = new TextView(this);
            memberName_Text.setText(GetSpanText.getSpanString(this, mEditInternalOS_indi.get(i).getMemberName()));
            memberName_Text.setTextColor(color.black);
            memberName_Text.setPadding(15, 5, 5, 5);
            memberName_Text.setLayoutParams(leftContentParams);
            memberName_Text.setWidth(200);
            memberName_Text.setSingleLine(true);
            memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
            leftContentRow.addView(memberName_Text);

            TableRow.LayoutParams leftContentParams1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
            leftContentParams1.setMargins(5, 5, 80, 5);

            mSavings_values = new EditText(this);
            mSavings_values.setId(i);
            sSavingsFields.add(mSavings_values);
            mSavings_values.setPadding(5, 5, 5, 5);
            mSavings_values.setBackgroundResource(R.drawable.edittext_background);
            mSavings_values.setLayoutParams(leftContentParams1);
            mSavings_values.setTextAppearance(this, R.style.MyMaterialTheme);
            mSavings_values.setFilters(Get_EdiText_Filter.editText_filter());
            mSavings_values.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
            mSavings_values.setTextColor(color.black);
            mSavings_values.setWidth(150);
            mSavings_values.setText(mInternalOSVector.get(i));
            mSavings_values.setGravity(Gravity.LEFT);
            final int finalI = i;
            mSavings_values.setOnFocusChangeListener(new OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    // TODO Auto-generated method stub
                    if (hasFocus) {

                        if(v.getId()==finalI) {

                            if (sSavingsFields.get(finalI).getText().toString().equals("0.0") || sSavingsFields.get(finalI).getText().toString().equals("0")) {
                                ((EditText)v).setText("");
                            }
                        }

                        mMemberNameLayout.setVisibility(View.VISIBLE);
                        mMemberName.setText(memberName_Text.getText().toString().trim());
                        TextviewUtils.manageBlinkEffect(mMemberName, EditOpeningBalanceBankMemberActivity.this);
                    } else {
                        mMemberNameLayout.setVisibility(View.GONE);
                        mMemberName.setText("");
                    }
                }
            });
            leftContentRow.addView(mSavings_values);

            mContentTable.addView(leftContentRow);

        }

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        switch (v.getId()) {
            case R.id.fragment_edit_Internal:

                try {

                    mMemberNameLayout.setVisibility(View.GONE);
                    mMemberName.setText("");

                    sOutstandingAmounts = new String[mSize];
                    // sIncome_Total = 0;

                    confirmArr = new String[mSize];

                    sSendToServer_Outstanding = "";
                    sOutstandingAmount_Total = Integer.valueOf(nullVlaue);

                    StringBuilder builder = new StringBuilder();

                    for (int i = 0; i < mSize; i++) {

                        sOutstandingAmounts[i] = sSavingsFields.get(i).getText().toString();

                        if ((sOutstandingAmounts[i].equals("")) || (sOutstandingAmounts[i] == null)) {
                            sOutstandingAmounts[i] = nullVlaue;
                        }

                        if (sOutstandingAmounts[i].matches("\\d*\\.?\\d+")) { // match
                            // a
                            // decimal
                            // number

                            int outStandingAmount = (int) Math.round(Double.parseDouble(sOutstandingAmounts[i]));
                            sOutstandingAmounts[i] = String.valueOf(outStandingAmount);
                        }

                        if (Integer.parseInt(sOutstandingAmounts[i]) < 0) {
                            mIsNegativeValues = true;
                        }


                        if (termCount > 0) {
                            if (termCount == 1) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (termCount == 2) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan1().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            } else if (termCount == 3) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan2().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            } else if (termCount == 4) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan3().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            } else if (termCount == 5) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan4().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            } else if (termCount == 6) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan5().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            } else if (termCount == 7) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan6().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            }
                        } else if (mfiCount > 0) {
                            if (mfiCount == 1) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            } else if (mfiCount == 2) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan1().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            } else if (mfiCount == 3) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan2().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            } else if (mfiCount == 4) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan3().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            } else if (mfiCount == 5) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan4().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            } else if (mfiCount == 6) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan5().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            } else if (mfiCount == 7) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan6().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            }
                        } else if (ccCount > 0) {
                            if (ccCount == 1) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            } else if (ccCount == 2) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit1().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            } else if (ccCount == 3) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit2().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            } else if (ccCount == 4) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit3().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            } else if (ccCount == 5) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit4().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            } else if (ccCount == 6) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit5().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            } else if (ccCount == 7) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit6().get(i).setAmount(sOutstandingAmounts[i]);
                                ;
                            }
                        } else if (CifCount > 0) {
                            if (CifCount == 1) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (CifCount == 2) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF1().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (CifCount == 3) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF2().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (CifCount == 4) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF3().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (CifCount == 5) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF4().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (CifCount == 6) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF5().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (CifCount == 7) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF6().get(i).setAmount(sOutstandingAmounts[i]);
                            }
                        } else if (rfaCount > 0) {
                            if (rfaCount == 1) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (rfaCount == 2) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA1().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (rfaCount == 3) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA2().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (rfaCount == 4) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA3().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (rfaCount == 5) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA4().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (rfaCount == 6) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA5().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (rfaCount == 7) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getRFA6().get(i).setAmount(sOutstandingAmounts[i]);
                            }
                        } else if (bulkCount > 0) {
                            if (bulkCount == 1) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (bulkCount == 2) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan1().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (bulkCount == 3) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan2().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (bulkCount == 4) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan3().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (bulkCount == 5) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan4().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (bulkCount == 6) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan5().get(i).setAmount(sOutstandingAmounts[i]);
                            } else if (bulkCount == 7) {
                                EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan6().get(i).setAmount(sOutstandingAmounts[i]);
                            }
                        }

                        sOutstandingAmount_Total = sOutstandingAmount_Total + Integer.parseInt(sOutstandingAmounts[i]);

                        builder.append(sOutstandingAmounts[i]).append(",");
                    }

                    Log.d(TAG, sSendToServer_Outstanding);

                    Log.d(TAG, "TOTAL " + String.valueOf(sOutstandingAmount_Total));

                    // Do the SP insertion

                    if (!mIsNegativeValues) {
                        confirmationDialog = new Dialog(this);

                        LayoutInflater inflater = this.getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                        dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                                LayoutParams.WRAP_CONTENT));

                        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                        confirmationHeader.setText((AppStrings.confirmation));

                        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                        for (int i = 0; i < mSize; i++) {

                            TableRow indv_SavingsRow = new TableRow(this);

                            @SuppressWarnings("deprecation")
                            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                                    LayoutParams.WRAP_CONTENT, 1f);
                            contentParams.setMargins(10, 5, 10, 5);

                            TextView memberName_Text = new TextView(this);
                            memberName_Text.setText(GetSpanText.getSpanString(this,
                                    mEditInternalOS_indi.get(i).getMemberName()));
                            memberName_Text.setTextColor(color.black);
                            memberName_Text.setPadding(5, 5, 5, 5);
                            memberName_Text.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(memberName_Text);

                            TextView confirm_values = new TextView(this);
                            confirm_values.setText(GetSpanText.getSpanString(this, String.valueOf(sOutstandingAmounts[i])));
                            confirm_values.setTextColor(color.black);
                            confirm_values.setPadding(5, 5, 5, 5);
                            confirm_values.setGravity(Gravity.RIGHT);
                            confirm_values.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(confirm_values);

                            confirmationTable.addView(indv_SavingsRow,
                                    new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

                        }

                        View rullerView = new View(this);
                        rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                        rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                        // 229,
                        // 242));
                        confirmationTable.addView(rullerView);

                        TableRow totalRow = new TableRow(this);

                        @SuppressWarnings("deprecation")
                        TableRow.LayoutParams totalParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                                LayoutParams.WRAP_CONTENT, 1f);
                        totalParams.setMargins(10, 5, 10, 5);

                        TextView totalText = new TextView(this);
                        totalText.setText(GetSpanText.getSpanString(this, String.valueOf(AppStrings.total)));
                        totalText.setTextColor(color.black);
                        totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
                        totalText.setLayoutParams(totalParams);
                        totalRow.addView(totalText);

                        TextView totalAmount = new TextView(this);
                        totalAmount.setText(GetSpanText.getSpanString(this, String.valueOf(sOutstandingAmount_Total)));// SavingsFragment.sSavings_Total
                        totalAmount.setTextColor(color.black);
                        totalAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                        totalAmount.setGravity(Gravity.RIGHT);
                        totalAmount.setLayoutParams(totalParams);
                        totalRow.addView(totalAmount);

                        confirmationTable.addView(totalRow,
                                new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

                        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                        mEdit_RaisedButton.setText((AppStrings.edit));
                        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                        // 205,
                        // 0));
                        mEdit_RaisedButton.setOnClickListener(this);

                        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                        mOk_RaisedButton.setText((AppStrings.mVerified));
                        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                        mOk_RaisedButton.setOnClickListener(this);

                        confirmationDialog.getWindow()
                                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        confirmationDialog.setCanceledOnTouchOutside(false);
                        confirmationDialog.setContentView(dialogView);
                        confirmationDialog.setCancelable(true);
                        confirmationDialog.show();

                        MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
                        margin.leftMargin = 10;
                        margin.rightMargin = 10;
                        margin.topMargin = 10;
                        margin.bottomMargin = 10;
                        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
                    } else {

                        TastyToast.makeText(getApplicationContext(), AppStrings.mIsNegativeOpeningBalance,
                                TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                        sSendToServer_Outstanding = "0";
                        sOutstandingAmount_Total = Integer.valueOf(nullVlaue);

                        mIsNegativeValues = false;

                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

                break;

            case R.id.fragment_Edit:

                mSubmit_RaisedButton.setClickable(true);

                sSendToServer_Outstanding = "0";
                sOutstandingAmount_Total = Integer.valueOf(nullVlaue);

                mAmount_Values = "0";

                confirmationDialog.dismiss();
                break;

            case R.id.frag_Ok:

                confirmationDialog.dismiss();
                mIsNegativeValues = false;

                if (termCount > 0) {
                    Log.i("print","if");
                    int count = EShaktiApplication.vertficationDto.getListCountLoan().getTermLoan() - 1;
                    EShaktiApplication.vertficationDto.getListCountLoan().setTermLoan(count);


                    Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
                            EditOpeningBalanceBankMemberActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
                    finish();


                } else if (mfiCount > 0) {
                    Log.i("print","else-if(1)");
                    int count = EShaktiApplication.vertficationDto.getListCountLoan().getMFILoan() - 1;
                    EShaktiApplication.vertficationDto.getListCountLoan().setMFILoan(count);
                    Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
                            EditOpeningBalanceBankMemberActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();
                } else if (ccCount > 0) {
                    Log.i("print","else-if(2)");
                    int count = EShaktiApplication.vertficationDto.getListCountLoan().getCashCredit() - 1;
                    EShaktiApplication.vertficationDto.getListCountLoan().setCashCredit(count);

                    Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
                            EditOpeningBalanceBankMemberActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();

                } else if (CifCount > 0) {
                    Log.i("print","else-if(3)");
                    int count = EShaktiApplication.vertficationDto.getListCountLoan().getCIF() - 1;
                    EShaktiApplication.vertficationDto.getListCountLoan().setCIF(count);

                    Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
                            EditOpeningBalanceBankMemberActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();

                } else if (rfaCount > 0) {
                    Log.i("print","else-if(4)");
                    int count = EShaktiApplication.vertficationDto.getListCountLoan().getRFA() - 1;
                    EShaktiApplication.vertficationDto.getListCountLoan().setRFA(count);

                    Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
                            EditOpeningBalanceBankMemberActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();

                } else if (bulkCount > 1) {
                    Log.i("print","else-if(5)");
                    int count = EShaktiApplication.vertficationDto.getListCountLoan().getBulkLoan() - 1;
                    EShaktiApplication.vertficationDto.getListCountLoan().setBulkLoan(count);
                    Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
                            EditOpeningBalanceBankMemberActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();

                } else if (EShaktiApplication.vertficationDto.getGroupfinancialDetails() != null && EShaktiApplication.vertficationDto.getGroupfinancialDetails().size() > 0) {


                    Log.i("print","else-if(6)");
                    EShaktiApplication.vertficationDto.getListCountLoan().setTermLoan(MySharedPreference.readInteger(this, MySharedPreference.TermLoan, 0));
                    EShaktiApplication.vertficationDto.getListCountLoan().setMFILoan(MySharedPreference.readInteger(this, MySharedPreference.MFICount, 0));
                    EShaktiApplication.vertficationDto.getListCountLoan().setCIF(MySharedPreference.readInteger(this, MySharedPreference.CifCount, 0));
                    EShaktiApplication.vertficationDto.getListCountLoan().setRFA(MySharedPreference.readInteger(this, MySharedPreference.RFACount, 0));
                    EShaktiApplication.vertficationDto.getListCountLoan().setCashCredit(MySharedPreference.readInteger(this, MySharedPreference.CCcount, 0));
                    EShaktiApplication.vertficationDto.getListCountLoan().setBulkLoan(MySharedPreference.readInteger(this, MySharedPreference.BulkCount, 0));


                    Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
                            EditOpeningBalanceGroupLoanActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();


                } else if (EShaktiApplication.vertficationDto.getShgBalanceDetailsDTO() != null) {

                    Log.i("print","else-if(7)");
                    Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
                            EditOpeningBalanceBankDetailsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();


                }

                break;

        }

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_edit_ob, menu);
        MenuItem item = menu.getItem(0);
        item.setVisible(true);
        MenuItem logOutItem = menu.getItem(1);
        logOutItem.setVisible(true);

        SpannableStringBuilder SS = new SpannableStringBuilder(AppStrings.groupList);
        SpannableStringBuilder logOutBuilder = new SpannableStringBuilder(AppStrings.logOut);

        if (item.getItemId() == R.id.action_grouplist_edit) {

            item.setTitle(SS);

        }

        if (logOutItem.getItemId() == R.id.menu_logout_edit) {

            logOutItem.setTitle(logOutBuilder);
        }


        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_grouplist_edit) {

            try {
                startActivity(new Intent(EditOpeningBalanceBankMemberActivity.this, SHGGroupActivity.class));
                finish();
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
/*                if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
                    PrefUtils.setLoginGroupService("2");
                    new Login_webserviceTask(MainActivity.this).execute();
                } else {
                    startActivity(new Intent(this, SHGGroupActivity.class));
                    overridePendingTransition(R.anim.right_to_left_in, R.anim.right_to_left_out);
                    finish();
                }*/
            return true;

        } else if (id == R.id.menu_logout_edit) {
            Log.e(" Logout", "Logout Sucessfully");
            startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    /*	if (!"".equals("")) {
				if (publicValues.mBankLoanSize_BankLoan == publicValues.mCurrentBankLoanSize_BankLoan) {

					Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
							EditOpeningBalanceGroupLoanActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
					finish();
				} else if (publicValues.mBankLoanSize_BankLoan == 1) {

					Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
							EditOpeningBalanceGroupLoanActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
					finish();
				} else {

					publicValues.mCurrentBankLoanSize_BankLoan = publicValues.mCurrentBankLoanSize_BankLoan + 1;
					if (publicValues.mBankLoanSize_BankLoan == publicValues.mCurrentBankLoanSize_BankLoan) {


						Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
								EditOpeningBalanceGroupLoanActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
						finish();

					} else {

						Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
								EditOpeningBalanceBankMemberActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

						finish();
					}
				}
			} else if (!Get_Edit_OpeningbalanceWebservice.mEdit_OpeningBalance_Values_Check[4].equals("")) {

				if (publicValues.mBankLoanSize_BankLoan == publicValues.mCurrentBankLoanSize_BankLoan) {
					publicValues.mCurrentBankLoanSize_BankLoan = 0;
					publicValues.mBankLoanSize_BankLoan = 0;
					if (publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan == null) {
						publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = "";
					}
					publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan
							+ sSendToServer_Outstanding + "%";
					Log.e("Current Bank Member Loan Values---->>>111",
							publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan);
					Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
							EditOpeningBalanceBankDetailsActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
					finish();
				} else if (publicValues.mBankLoanSize_BankLoan == 1) {
					publicValues.mCurrentBankLoanSize_BankLoan = 0;
					publicValues.mBankLoanSize_BankLoan = 0;
					if (publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan == null) {
						publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = "";
					}
					publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan
							+ sSendToServer_Outstanding + "%";
					Log.e("Current Bank Member Loan Values---->>>222",
							publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan);
					Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
							EditOpeningBalanceBankDetailsActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
					finish();
				} else {

					if (publicValues.mBankLoanSize_BankLoan == publicValues.mCurrentBankLoanSize_BankLoan) {

						Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
								EditOpeningBalanceBankDetailsActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
						finish();

					} else {

						Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
								EditOpeningBalanceBankMemberActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

						finish();
					}
				}

			}*/

}
package com.oasys.eshakti.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.oasys.eshakti.R;

public class MainActivityTransaction extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_transaction);
    }
}
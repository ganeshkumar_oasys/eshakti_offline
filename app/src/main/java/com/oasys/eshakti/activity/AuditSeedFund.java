package com.oasys.eshakti.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.Dto.InternalLoanDisbursement;
import com.oasys.eshakti.Dto.OfflineDto;
import com.oasys.eshakti.Dto.RequestDto.AuditRequest;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.Dto.SeedFund;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.GetExit;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.views.ButtonFlat;
import com.oasys.eshakti.views.CustomHorizontalScrollView;
import com.oasys.eshakti.views.RaisedButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.oasys.eshakti.activity.SHGGroupActivity.auditor_id;
import static com.oasys.eshakti.activity.SHGGroupActivity.bankTransactionsize;
import static com.oasys.eshakti.activity.SHGGroupActivity.donationsize;
import static com.oasys.eshakti.activity.SHGGroupActivity.fDBankTransactionsize;
import static com.oasys.eshakti.activity.SHGGroupActivity.groupLoanRepaymentsize;
import static com.oasys.eshakti.activity.SHGGroupActivity.incomeDisbursementsize;
import static com.oasys.eshakti.activity.SHGGroupActivity.internalLoanRepaymentsize;
import static com.oasys.eshakti.activity.SHGGroupActivity.meetingExpensesize;
import static com.oasys.eshakti.activity.SHGGroupActivity.memberLoanRepaymentsize;
import static com.oasys.eshakti.activity.SHGGroupActivity.otherExpensesize;
import static com.oasys.eshakti.activity.SHGGroupActivity.otherIncomeGroupsize;
import static com.oasys.eshakti.activity.SHGGroupActivity.otherIncomesize;
import static com.oasys.eshakti.activity.SHGGroupActivity.penaltysize;
import static com.oasys.eshakti.activity.SHGGroupActivity.savingdisbursementsize;
import static com.oasys.eshakti.activity.SHGGroupActivity.savingsize;
import static com.oasys.eshakti.activity.SHGGroupActivity.seedFundsize;
import static com.oasys.eshakti.activity.SHGGroupActivity.shg_userId;
import static com.oasys.eshakti.activity.SHGGroupActivity.subscriptionChargessize;
import static com.oasys.eshakti.activity.SHGGroupActivity.subscriptionsize;
import static com.oasys.eshakti.activity.SHGGroupActivity.subscriptiontoFedssize;
import static com.oasys.eshakti.activity.SHGGroupActivity.voluntarySavingsDisbursementsize;
import static com.oasys.eshakti.activity.SHGGroupActivity.voluntarysavingsize;
import static com.oasys.eshakti.database.SHGTable.getInternalLoanDisbursementAuditDetails;
import static com.oasys.eshakti.database.SHGTable.getSeedfundAuditDetails;
import static com.oasys.eshakti.database.SHGTable.updateAuditIstransaction;


public class AuditSeedFund extends AppCompatActivity implements View.OnClickListener, NewTaskListener {
    private Toolbar mToolbar;
    private TextView mTitle,mGroupName,mTransaction_audit_date,mloanType;
    private String nameStr, mobStr;
    ArrayList<SeedFund> datavalues;
    String dateStr,remarks_value=null,dateStr1;
    private RaisedButton mNext;
    private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
    private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;
    String width[] = {AppStrings.memberName, AppStrings.savingsAmount, AppStrings.voluntarySavings};
    int[] rightHeaderWidth = new int[width.length];
    private TextView mSavings_values,mVSavings_values;
    private List<TextView> sSavingsFields,sVSavingsFields;
    Dialog confirmationDialog,confirmationDialog_approve,confirmationDialog_deny;
    private NetworkConnection networkConnection;
    public static MenuItem item,item1,item2,logOutItem;
    EditText remarks;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_transaction__audit_savings);
        inIt();
    }

    private void inIt() {

        networkConnection = NetworkConnection.getNetworkConnection(getApplicationContext());
        nameStr = MySharedPreference.readString(AuditSeedFund.this, MySharedPreference.ANIMATOR_NAME, "");
        mobStr = MySharedPreference.readString(AuditSeedFund.this, MySharedPreference.USERNAME, "");
        datavalues = getSeedfundAuditDetails();
        sSavingsFields = new ArrayList<TextView>();
        sVSavingsFields = new ArrayList<TextView>();

        mToolbar = (Toolbar) findViewById(R.id.toolbar_grouplist);
        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(nameStr + "    " + mobStr);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");

        mGroupName = (TextView) findViewById(R.id.groupname);
        mGroupName.setText(SHGGroupActivity.shgName+"/"+SHGGroupActivity.presidentname);
        mGroupName.setTypeface(LoginActivity.sTypeface);

        DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
        Date d = new Date(Long.parseLong(datavalues.get(0).getTransaction_date()));
        dateStr = simple.format(d);

        mTransaction_audit_date = (TextView)findViewById(R.id.transaction_audit_date);
        mTransaction_audit_date.setTypeface(LoginActivity.sTypeface);
        mTransaction_audit_date.setText(AppStrings.Transaction_audit_date +""+ dateStr);

        mloanType = (TextView)findViewById(R.id.loanType);
        mloanType.setTypeface(LoginActivity.sTypeface);
        mloanType.setText(AppStrings.Seedfund);

        mNext = (RaisedButton) findViewById(R.id.fragment_Submit);
        mNext.setText(AppStrings.nnext);
        mNext.setOnClickListener(this);


        mLeftHeaderTable = (TableLayout) findViewById(R.id.LeftHeaderTable);
        mRightHeaderTable = (TableLayout) findViewById(R.id.RightHeaderTable);
        mLeftContentTable = (TableLayout) findViewById(R.id.LeftContentTable);
        mRightContentTable = (TableLayout) findViewById(R.id.RightContentTable);

        mHSRightHeader = (CustomHorizontalScrollView) findViewById(R.id.rightHeaderHScrollView);
        mHSRightContent = (CustomHorizontalScrollView) findViewById(R.id.rightContentHScrollView);

        mHSRightHeader.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
            @Override
            public void onScrollChanged(int l, int t, int oldl, int oldt) {

                mHSRightContent.scrollTo(l, 0);
            }
        });


        mHSRightContent.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
            @Override
            public void onScrollChanged(int l, int t, int oldl, int oldt) {
                mHSRightHeader.scrollTo(l, 0);
            }
        });

        TableRow leftHeaderRow = new TableRow(getApplicationContext());

        TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

        TextView mMemberName_Header = new TextView(getApplicationContext());
        mMemberName_Header.setText(AppStrings.memberName);
        mMemberName_Header.setTypeface(LoginActivity.sTypeface);
        mMemberName_Header.setTextColor(Color.WHITE);
        mMemberName_Header.setPadding(10, 5, 10, 5);
        mMemberName_Header.setLayoutParams(lHeaderParams);
        leftHeaderRow.addView(mMemberName_Header);

        mLeftHeaderTable.addView(leftHeaderRow);

        TableRow rightHeaderRow = new TableRow(getApplicationContext());

        TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        contentParams.setMargins(10, 0, 10, 0);

        TextView mOutstanding_Header = new TextView(getApplicationContext());
        mOutstanding_Header.setText(AppStrings.previousamount);
        mOutstanding_Header.setTypeface(LoginActivity.sTypeface);
        mOutstanding_Header.setTextColor(Color.WHITE);
        mOutstanding_Header.setPadding(10, 5, 10, 5);
        mOutstanding_Header.setGravity(Gravity.LEFT);
        mOutstanding_Header.setLayoutParams(contentParams);
        mOutstanding_Header.setBackgroundResource(R.color.tableHeader);
        rightHeaderRow.addView(mOutstanding_Header);


        TableRow.LayoutParams POLParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        POLParams.setMargins(10, 0, 10, 0);

        TextView mPurposeOfLoan_Header = new TextView(getApplicationContext());
        mPurposeOfLoan_Header
                .setText(AppStrings.currentamount);
        mPurposeOfLoan_Header.setTypeface(LoginActivity.sTypeface);
        mPurposeOfLoan_Header.setTextColor(Color.WHITE);
        mPurposeOfLoan_Header.setGravity(Gravity.RIGHT);
        mPurposeOfLoan_Header.setLayoutParams(POLParams);
        mPurposeOfLoan_Header.setPadding(25, 5, 10, 5);
        mPurposeOfLoan_Header.setBackgroundResource(R.color.tableHeader);
        rightHeaderRow.addView(mPurposeOfLoan_Header);
        mRightHeaderTable.addView(rightHeaderRow);

        getTableRowHeaderCellWidth();


        for (int i = 0; i < datavalues.size(); i++) {

            TableRow leftContentRow = new TableRow(getApplicationContext());

            TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 60, 1f);
            leftContentParams.setMargins(5, 5, 5, 5);

            final TextView memberName_Text = new TextView(getApplicationContext());
            memberName_Text.setText(datavalues.get(i).getMember());
            memberName_Text.setTextColor(R.color.black);
            memberName_Text.setPadding(5, 5, 5, 5);
            memberName_Text.setLayoutParams(leftContentParams);
            memberName_Text.setWidth(200);
            memberName_Text.setSingleLine(true);
            memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
            leftContentRow.addView(memberName_Text);

            mLeftContentTable.addView(leftContentRow);

            TableRow rightContentRow = new TableRow(getApplicationContext());

            TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(rightHeaderWidth[1],
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rightContentParams.setMargins(100, 5, 10, 5);

            mSavings_values = new TextView(getApplicationContext());
            mSavings_values.setId(i);
            sSavingsFields.add(mSavings_values);
            mSavings_values.setPadding(5, 5, 5, 5);
            mSavings_values.setLayoutParams(rightContentParams);
            mSavings_values.setText(datavalues.get(i).getOld_transaction_amount());
            mSavings_values.setTextAppearance(getApplicationContext(), R.style.MyMaterialTheme);
            mSavings_values.setInputType(InputType.TYPE_CLASS_NUMBER);
            mSavings_values.setTextColor(R.color.black);
            mSavings_values.setWidth(50);
            rightContentRow.addView(mSavings_values);

            mVSavings_values = new TextView(getApplicationContext());
            mVSavings_values.setId(i);
            sVSavingsFields.add(mVSavings_values);
            mVSavings_values.setPadding(5, 5, 5, 5);
            mVSavings_values.setText(datavalues.get(i).getNew_transaction_amount());
            mVSavings_values.setLayoutParams(rightContentParams);
            mVSavings_values.setTextAppearance(getApplicationContext(), R.style.MyMaterialTheme);
            mVSavings_values.setInputType(InputType.TYPE_CLASS_NUMBER);
            mVSavings_values.setTextColor(R.color.black);
            mVSavings_values.setWidth(100);
            rightContentRow.addView(mVSavings_values);

            mRightContentTable.addView(rightContentRow);
        }
    }




    private void getTableRowHeaderCellWidth() {
        int lefHeaderChildCount = ((TableRow) mLeftHeaderTable.getChildAt(0)).getChildCount();
        int rightHeaderChildCount = ((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount();

        for (int x = 0; x < (lefHeaderChildCount + rightHeaderChildCount); x++) {
            if (x == 0) {
                rightHeaderWidth[x] = viewWidth(((TableRow) mLeftHeaderTable.getChildAt(0)).getChildAt(x));
            } else {
                rightHeaderWidth[x] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(x - 1));
            }
        }
    }

    private int viewWidth(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredWidth();
    }
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.fragment_Submit:

                if (fDBankTransactionsize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditFdBankTransaction.class));
                } else if (incomeDisbursementsize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditIncomeDisbursement.class));
                } else if (otherIncomesize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditOtherIncome.class));
                } else if (groupLoanRepaymentsize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditGroupLoanRepayment.class));
                } else if (penaltysize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditPenalty.class));
                } else if (bankTransactionsize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditBankTransaction.class));
                } else if (subscriptiontoFedssize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditSubscriptiontoFeds.class));
                } else if (otherIncomeGroupsize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditOtherincomeGroup.class));
                } else if (memberLoanRepaymentsize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditMemberLoanReapyment.class));
                } else if (voluntarySavingsDisbursementsize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditVoluntarySavingsDisbursement.class));
                } else if (otherExpensesize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditOtherExpense.class));
                } else if (meetingExpensesize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditMeetingExpense.class));
                } else if (internalLoanRepaymentsize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditInternalLoanRepayment.class));
                } else if (donationsize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditDonation.class));
                } else if (voluntarysavingsize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditVoluntarysavings.class));
                } else if (savingdisbursementsize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditSavingdisbursement.class));
                } else if (savingsize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, Audit_savings.class));
                } else if (subscriptionsize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditSubscription.class));
                } else if (subscriptionChargessize != 0) {
                    startActivity(new Intent(AuditSeedFund.this, AuditSubscriptiontoFeds.class));
                } else {
                    showCustomDialog();
                }
                break;
            case R.id.button_approve:
                confirmationDialog.dismiss();
                approveDialog();
                break;

            case R.id.button_deny:
                confirmationDialog.dismiss();
                denyDialog();
                break;
            case R.id.button_approve2:
                confirmationDialog_approve.dismiss();
                auditApiCall();
                break;
            case R.id.button_proceed:
                confirmationDialog_deny.dismiss();
                remarks_value = remarks.getText().toString();
                auditApiCall();
                break;
            case R.id.button_deny2:
                confirmationDialog_approve.dismiss();
                break;
            case R.id.button_deny1:
                confirmationDialog_deny.dismiss();
                break;
        }
    }

    private void showCustomDialog() {

        confirmationDialog = new Dialog(AuditSeedFund.this);

        LayoutInflater inflater = AuditSeedFund.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.transaction_audit_dialog, null);


        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText("" + AppStrings.confirmation);
        confirmationHeader.setTypeface(LoginActivity.sTypeface);

        ButtonFlat button_approve = (ButtonFlat) dialogView.findViewById(R.id.button_approve);
        button_approve.setText("" + AppStrings.approve);
        button_approve.setTypeface(LoginActivity.sTypeface);
        button_approve.setOnClickListener(this);


        ButtonFlat button_deny = (ButtonFlat) dialogView.findViewById(R.id.button_deny);
        button_deny.setText("" + AppStrings.deny);
        button_deny.setTypeface(LoginActivity.sTypeface);
        button_deny.setOnClickListener(this);

        TextView confirmationdescribe = (TextView) dialogView.findViewById(R.id.describe);
        confirmationdescribe.setText("" + AppStrings.transaction_auditdialog_describe);
        confirmationdescribe.setTypeface(LoginActivity.sTypeface);

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialogView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();
    }

    private void approveDialog() {

        confirmationDialog_approve = new Dialog(AuditSeedFund.this);

        LayoutInflater inflater = AuditSeedFund.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.transaction_approve_audit_dialog, null);

        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText("" + AppStrings.confirmation);
        confirmationHeader.setTypeface(LoginActivity.sTypeface);

        ButtonFlat button_approve = (ButtonFlat) dialogView.findViewById(R.id.button_approve2);
        button_approve.setText("" + AppStrings.proceed);
        button_approve.setTypeface(LoginActivity.sTypeface);
        button_approve.setOnClickListener(this);


        ButtonFlat button_deny = (ButtonFlat) dialogView.findViewById(R.id.button_deny2);
        button_deny.setText("" + AppStrings.cancel);
        button_deny.setTypeface(LoginActivity.sTypeface);
        button_deny.setOnClickListener(this);


        TextView confirmationdescribe = (TextView) dialogView.findViewById(R.id.describe);
        confirmationdescribe.setText("" + AppStrings.transaction_approve_describe);
        confirmationdescribe.setTypeface(LoginActivity.sTypeface);

        confirmationDialog_approve.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog_approve.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog_approve.setCanceledOnTouchOutside(false);
        confirmationDialog_approve.setContentView(dialogView);
        confirmationDialog_approve.setCancelable(true);
        confirmationDialog_approve.show();

    }

    private void denyDialog() {

        confirmationDialog_deny = new Dialog(AuditSeedFund.this);

        LayoutInflater inflater = AuditSeedFund.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.transaction_audit_proceed, null);

        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText("" + AppStrings.confirmation);
        confirmationHeader.setTypeface(LoginActivity.sTypeface);

        remarks = (EditText) dialogView.findViewById(R.id.remarks);
        remarks.setVisibility(View.VISIBLE);

        ButtonFlat button_proceed = (ButtonFlat) dialogView.findViewById(R.id.button_proceed);
        button_proceed.setText("" + AppStrings.proceed);
        button_proceed.setTypeface(LoginActivity.sTypeface);
        button_proceed.setOnClickListener(this);


        ButtonFlat button_deny = (ButtonFlat) dialogView.findViewById(R.id.button_deny1);
        button_deny.setText("" + AppStrings.cancel);
        button_deny.setTypeface(LoginActivity.sTypeface);
        button_deny.setOnClickListener(this);


        TextView confirmationdescribe = (TextView) dialogView.findViewById(R.id.describe);
        confirmationdescribe.setText("" + AppStrings.transaction_approve_describe_IA);
        confirmationdescribe.setTypeface(LoginActivity.sTypeface);

        confirmationDialog_deny.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog_deny.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog_deny.setCanceledOnTouchOutside(false);
        confirmationDialog_deny.setContentView(dialogView);
        confirmationDialog_deny.setCancelable(true);
        confirmationDialog_deny.show();
    }

    private void auditApiCall(){

        AuditRequest auditRequest =new AuditRequest();
        auditRequest.setAuditorId(auditor_id);
        if(remarks_value !=null){
            auditRequest.setRemarks(remarks_value);
        }else {
            auditRequest.setRemarks("");
        }
        auditRequest.setShgId(shg_userId);
        auditRequest.setValidation("true");
        DateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
        Date d = new Date(Long.parseLong(datavalues.get(0).getTransaction_date()));
        dateStr1 = simple.format(d);
        auditRequest.setTransDate(dateStr1);
        String sreqString = new Gson().toJson(auditRequest);
        if (networkConnection.isNetworkAvailable()) {

            RestClient.getRestClient(AuditSeedFund.this).callRestWebService(Constants.BASE_URL + Constants.APPROVE_AUDIT,sreqString, AuditSeedFund.this, ServiceType.APPROVEAUDIT);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        item = menu.getItem(0);
        item.setVisible(true);
        logOutItem = menu.getItem(1);
        logOutItem.setVisible(true);

        item1 =  menu.findItem(R.id.action_home).setVisible(false);
        item2 = menu.findItem(R.id.action_logout).setVisible(false);

        SpannableStringBuilder SS = new SpannableStringBuilder(AppStrings.groupList);
        SpannableStringBuilder logOutBuilder = new SpannableStringBuilder(AppStrings.logOut);

        if (item.getItemId() == R.id.action_grouplist) {
            item.setTitle(SS);
        }
        if (logOutItem.getItemId() == R.id.menu_logout) {
            logOutItem.setTitle(logOutBuilder);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_grouplist) {
            try {
                startActivity(new Intent(AuditSeedFund.this, SHGGroupActivity.class));
                finish();
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
            return true;
        }else if (id == R.id.menu_logout) {
            Log.e(" Logout", "Logout Sucessfully");

            try {

                if (networkConnection.isNetworkAvailable()) {

                    RestClient.getRestClient(AuditSeedFund.this).callRestWebServiceForDelete(Constants.BASE_URL + Constants.LOGOUT_TOKENDELETION, AuditSeedFund.this, ServiceType.LOG_OUT, "");

                } else {
                    startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
                    this.finish();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public void isTransaudit(){
        OfflineDto offline = new OfflineDto();
        offline.setShgId(shg_userId);
        offline.setIs_trans_Audit("0.0");
        updateAuditIstransaction(offline);
    }
    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        switch (serviceType) {

            case APPROVEAUDIT:

                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();
                    Log.d("response status", " " + statusCode);
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(this, message);
                        isTransaudit();
                        startActivity(new Intent(AuditSeedFund.this,SHGGroupActivity.class));

                    }
                    else {
                        Utils.showToast(this, message);
                    }
                }
                break;

            case LOG_OUT:

                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();
                    Log.d("response status", " " + statusCode);

                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(this, message);

                        startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
                        this.finish();
                    }

                }

                break;


        }

    }
}

package com.oasys.eshakti.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.GetExit;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.views.ButtonFlat;
import com.tutorialsee.lib.TastyToast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class EditOpenBalanceDialogActivity extends AppCompatActivity {

    private String mLanguagelocale = "";
    Locale locale;
    String balsheetDate = "", mTrnsDate;
    public static String mCheck_EditOpening_BalncesheetChange = "";
    private Toolbar mToolbar;
    private TextView mTitle;
    private ListOfShg shgDto;
    public static String dateStr1;
    private  Date balanceDate;
    public static String dateStr;



    public EditOpenBalanceDialogActivity() {
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_edit_openingbalance_dialog);
        try {
            mToolbar = (Toolbar) findViewById(R.id.toolbar_grouplist);
            mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
            mTitle.setText("ESHAKTI");
            mTitle.setGravity(Gravity.CENTER);

            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(this, MySharedPreference.SHG_ID, ""));

            balsheetDate = EShaktiApplication.vertficationDto.getShggroupDetailsDTO().getTransactionDate();
            mTrnsDate = shgDto.getLastTransactionDate();
            TextView lastTr = (TextView) findViewById(R.id.edit_dialog_TransactionDate);

            lastTr.setText(AppStrings.mBalanceSheetDate + " : " + balsheetDate);

            TextView dialogMsg = (TextView) findViewById(R.id.edit_dialog_continueDate);
            dialogMsg.setText(// CalendarFragment.sDashboardDate
                    AppStrings.mContinueWithDate);

            ButtonFlat okButton = (ButtonFlat) findViewById(R.id.edit_fragment_Yes_button);
            ButtonFlat noButton = (ButtonFlat) findViewById(R.id.edit_fragment_No_button);


            if (GroupProfileActivity.isOpendate!=null && GroupProfileActivity.isOpendate.equals("0"))
            {
                okButton.setVisibility(View.VISIBLE);
                okButton.setText(AppStrings.dialogOk);
                noButton.setVisibility(View.VISIBLE);
                noButton.setText(AppStrings.dialogNo);

            }
            else
            {
                noButton.setVisibility(View.VISIBLE);
                noButton.setText("PROCEED");

            }
            okButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    onSetCalendarValues();
                    try {
                        Calendar calender = Calendar.getInstance();

                        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                        String formattedDate = df.format(calender.getTime());
                        Log.e("Device Date  =  ", formattedDate + "");

                        String mBalanceSheetDate = balsheetDate; //dd/MM/yyyy
                        String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
                        Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

                        balanceDate = df.parse(formatted_balancesheetDate);
                        Date systemDate = df.parse(formattedDate);


                        if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {

                            calendarDialogShow(EditOpenBalanceDialogActivity.this);
                        } else {
                            TastyToast.makeText(EditOpenBalanceDialogActivity.this, "PLEASE SET YOUR DEVICE DATE CORRECTLY", TastyToast.LENGTH_SHORT, TastyToast.ERROR);

                        }
                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }

                }
            });


            noButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    try {

                        if (EShaktiApplication.vertficationDto.getListCountLoan() != null) {
                            if (EShaktiApplication.vertficationDto.getMemberfinancialDetails() != null && EShaktiApplication.vertficationDto.getMemberfinancialDetails().size() > 0) {

                                if (EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getSavings() != null && EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getSavings().size() > 0) {

                                    Intent intent = new Intent(EditOpenBalanceDialogActivity.this,
                                            EditOpeningBalanceSavingsActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                                    finish();
                                } else if (EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan() != null || EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit() != null || EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan() != null || EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF() != null || EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan() != null) {
                                    Intent intent = new Intent(EditOpenBalanceDialogActivity.this,
                                            EditOpeningBalanceBankMemberActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                                    finish();
                                }
                            } else if (EShaktiApplication.vertficationDto.getGroupfinancialDetails() != null && EShaktiApplication.vertficationDto.getGroupfinancialDetails().size() > 0) {

                                Intent intent = new Intent(EditOpenBalanceDialogActivity.this,
                                        EditOpeningBalanceGroupLoanActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                                finish();

                            }
                        } else if (EShaktiApplication.vertficationDto.getShgBalanceDetailsDTO() != null) {

                            Intent intent = new Intent(EditOpenBalanceDialogActivity.this,
                                    EditOpeningBalanceBankDetailsActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                            finish();


                        }
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            });

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    private void calendarDialogShow(Context mContext) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);

// ...Irrelevant code for customizing the buttons and title

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View customView = inflater.inflate(R.layout.datepickerlayout_custom_ly, null);
        dialogBuilder.setView(customView);
        final DatePicker dpStartDate = (DatePicker) customView.findViewById(R.id.dpStartDate);
        final TextView title = (TextView) customView.findViewById(R.id.title);
        final TextView sub_tit = (TextView) customView.findViewById(R.id.sub_tit);


        String balancesheet_date = "01/01/2017";
        Calendar now = Calendar.getInstance();
        String dateArr[] = balancesheet_date.split("/");

        Calendar min_Cal = Calendar.getInstance();

        int day = Integer.parseInt(dateArr[0]);
        int month = Integer.parseInt(dateArr[1]);
        int year = Integer.parseInt(dateArr[2]);

        min_Cal.set(year, (month - 1), day);

        Calendar fcal = Calendar.getInstance();
        fcal.setTimeInMillis(balanceDate.getTime());

        dpStartDate.setMinDate(fcal.getTimeInMillis());
        dpStartDate.setMaxDate(now.getTimeInMillis());


        dpStartDate.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                Log.e("onDateSet() call", dayOfMonth + "-" + month + "-" + year);
                //String date = dayOfMonth + "-" + (month + 1) + "-" + year;

                try {
                    Calendar c = Calendar.getInstance();
//                    Date currentdate = c.getTime();
                    c.set(year, month, dayOfMonth);
                    DateFormat simple1 = new SimpleDateFormat("dd/MM/yyyy");
                    dateStr = simple1.format(c.getTime());
//                    c.add(Calendar.DAY_OF_YEAR, 1);
                    dateStr1 = simple1.format(c.getTime());
//                    EShaktiApplication.vertficationDto.getShggroupDetailsDTO().setTransactionDate(dateStr);
                    title.setText(dateStr1 + " PICK A DATE AFTER THIS DATE");
                    sub_tit.setText("Selected Date:"+dateStr1);

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }
        });


    /*    Calendar c = Calendar.getInstance();
        c.setTime(lastTransaDate);
        DateFormat simple1 = new SimpleDateFormat("dd/MM/yyyy");
        String dateStr = simple1.format(c.getTime());*/

        title.setText(balsheetDate + " PICK A DATE AFTER THIS DATE");
        sub_tit.setText("Selected Date:");

        dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                /*SimpleDateFormat dateFormat = new SimpleDateFormat( "dd/MM/yyyy");
                Calendar cal = Calendar.getInstance();
                try {
                    cal.setTime( dateFormat.parse(dateStr1));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                cal.add( Calendar.DATE, 1 );
                String convertedDate=dateFormat.format(cal.getTime());
                EShaktiApplication.vertficationDto.getShggroupDetailsDTO().setTransactionDate(convertedDate);*/
                EShaktiApplication.vertficationDto.getShggroupDetailsDTO().setTransactionDate(dateStr1);

                if (EShaktiApplication.vertficationDto.getListCountLoan() != null) {
                    Log.i("print","if");
                    if (EShaktiApplication.vertficationDto.getMemberfinancialDetails() != null && EShaktiApplication.vertficationDto.getMemberfinancialDetails().size() > 0) {
                        Log.i("print","if");
                        if (EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getSavings() != null && EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getSavings().size() > 0) {
                            Log.i("print","if(SavingsActivity)");
                            Intent intent = new Intent(EditOpenBalanceDialogActivity.this,
                                    EditOpeningBalanceSavingsActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                            finish();
                        } else if (EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getTermLoan() != null || EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCashCredit() != null || EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getMFILoan() != null || EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getCIF() != null || EShaktiApplication.vertficationDto.getMemberfinancialDetails().get(0).getBulkLoan() != null) {
                            Log.i("print","else-if(MemberActivity)");
                            Intent intent = new Intent(EditOpenBalanceDialogActivity.this,
                                    EditOpeningBalanceBankMemberActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                            finish();
                        }
                    } else if (EShaktiApplication.vertficationDto.getGroupfinancialDetails() != null && EShaktiApplication.vertficationDto.getGroupfinancialDetails().size() > 0) {
                        Log.i("print","else-if");
                        Intent intent = new Intent(EditOpenBalanceDialogActivity.this,
                                EditOpeningBalanceGroupLoanActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                        finish();

                    }
                } else if (EShaktiApplication.vertficationDto.getShgBalanceDetailsDTO() != null) {
                    Log.i("print","else-if");
                    Intent intent = new Intent(EditOpenBalanceDialogActivity.this,
                            EditOpeningBalanceBankDetailsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                    finish();


                }


            }
        });
        dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

            }
        });
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();


       /*


        Calendar now = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, now.get(Calendar.YEAR), now.get(Calendar.DATE), now.get(Calendar.DAY_OF_MONTH));
        Calendar min_Cal = Calendar.getInstance();
        Calendar lastDate = Calendar.getInstance();

        Calendar fcal = Calendar.getInstance();
        fcal.setTimeInMillis(openingDate.getTime());
        int fyear = fcal.get(Calendar.YEAR); // this is deprecated
        int fmonth = fcal.get(Calendar.MONTH); // this is deprecated
        int fday = fcal.get(Calendar.DATE);

        Calendar lcal = Calendar.getInstance();
        lcal.setTimeInMillis(lastTransaDate.getTime());
        int lyear = lcal.get(Calendar.YEAR); // this is deprecated
        int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
        int lday = lcal.get(Calendar.DATE);

        Calendar ccal1 = Calendar.getInstance();
        ccal1.setTimeInMillis(systemDate.getTime());
        int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
        int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
        int cday = ccal1.get(Calendar.DATE);

        if (fday == lday && fmonth == lmonth && fyear == lyear) {
            min_Cal.set(lyear, lmonth, lday);
            lastDate.set(cyear, cmonth, cday);
        } else if (fyear <= lyear) {

            if (cmonth == lmonth && cyear == lyear) {
                min_Cal.set(lyear, lmonth, lday);
                lastDate.set(cyear, cmonth, cday);

            } else if (cmonth <= lmonth && cyear > lyear) {
                min_Cal.set(lyear, lmonth, lday);
                Calendar c = Calendar.getInstance();
                c.setTime(lastTransaDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(lyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));

            } else if (cmonth > lmonth && cyear == lyear) {
                min_Cal.set(lyear, lmonth, lday);
                if ((cmonth - lmonth) == 1) {
                    lastDate.set(cyear, cmonth, cday);
                } else if ((cmonth - lmonth) > 1) {
                    Calendar c = Calendar.getInstance();
                    c.setTime(lastTransaDate);
                    c.add(Calendar.MONTH, 1);
                    c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                    lastDate.set(cyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
                }

            } else if (cmonth > lmonth && cyear > lyear) {
                min_Cal.set(lyear, lmonth, lday);
                Calendar c = Calendar.getInstance();
                c.setTime(lastTransaDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(lyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
            }

        }

        datePickerDialog.getDatePicker().setMinDate(min_Cal.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(lastDate.getTimeInMillis());

        datePickerDialog.show();*/
    }

    private void onSetCalendarValues() {
        // TODO Auto-generated method stub

        if (EShaktiApplication.getNextMonthLastDate() == null) {
            try {
                if (EShaktiApplication.isCalendarDateVisibleFlag()) {
                    EShaktiApplication.setCalendarDateVisibleFlag(false);
                }

                Calendar calender = Calendar.getInstance();

                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                String formattedDate = df.format(calender.getTime());
                Log.e("Device Date  =  ", formattedDate + "");

                String mBalanceSheetDate = balsheetDate; //dd/MM/yyyy
                String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
                Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

                Date balanceDate = df.parse(formatted_balancesheetDate);
                Date systemDate = df.parse(formattedDate);


                if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {
                    System.err.println(" balancesheet date is less than sys date");

                    String mLastTransactionDate = shgDto.getLastTransactionDate();

                    String formattedDate_LastTransaction = df.format(new Date(Long.parseLong(mLastTransactionDate)));

                    String days = Utils.get_count_of_days(formattedDate_LastTransaction, formattedDate);

                    int mDaycount = Integer.parseInt(days);
                    Log.e("Total Days Count__----", mDaycount + "");


                    if (mDaycount > 30) {

                        SimpleDateFormat df_after = new SimpleDateFormat("dd-MM-yyyy");

                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(df_after.parse(formattedDate_LastTransaction));
                        calendar.add(Calendar.MONTH, 1);
                        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                        Date nextMonthFirstDay = calendar.getTime();
                        System.out.println("------ Next month first date   =  " + df_after.format(nextMonthFirstDay) + "");

                        Calendar cal_last_date = Calendar.getInstance();
                        cal_last_date.setTime(df_after.parse(formattedDate_LastTransaction));
                        cal_last_date.add(Calendar.MONTH, 1);
                        cal_last_date.set(Calendar.DATE, cal_last_date.getActualMaximum(Calendar.DAY_OF_MONTH));
                        Date nextMonthLastDay = cal_last_date.getTime();
                        System.out.println("------ Next month Last date   =  " + df_after.format(nextMonthLastDay) + "");

                        //
                        String lastDateOfNextMonth = df_after.format(nextMonthLastDay);
                        Calendar currentDate = Calendar.getInstance();
                        int current_Day = currentDate.get(Calendar.DAY_OF_MONTH);
                        int current_month = currentDate.get(Calendar.MONTH) + 1;
                        int current_year = currentDate.get(Calendar.YEAR);
                        String current_date = current_Day + "-" + current_month + "-" + current_year;

                        Date lastdate = null, currentdate = null;
                        String min_date_str = null;

                        try {

                            lastdate = df_after.parse(lastDateOfNextMonth);
                            currentdate = df_after.parse(current_date);

                        } catch (ParseException e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }

                        if (lastdate.compareTo(currentdate) <= 0) {
                            min_date_str = lastDateOfNextMonth;
                        } else {
                            min_date_str = current_date;
                        }

                        Log.e("Minimum Date !!!!!	", min_date_str + "");

                        //       EShaktiApplication.setNextMonthLastDate(min_date_str);

                    } else {
                        Calendar currentDate = Calendar.getInstance();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                    }
                }

            } catch (Exception e) {
                // TODO: handle exception
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_edit_ob, menu);
        MenuItem item = menu.getItem(0);
        item.setVisible(true);
        MenuItem logOutItem = menu.getItem(1);
        logOutItem.setVisible(true);

        SpannableStringBuilder SS = new SpannableStringBuilder(AppStrings.groupList);
        SpannableStringBuilder logOutBuilder = new SpannableStringBuilder(AppStrings.logOut);

        if (item.getItemId() == R.id.action_grouplist_edit) {

            item.setTitle(SS);

        }

        if (logOutItem.getItemId() == R.id.menu_logout_edit) {

            logOutItem.setTitle(logOutBuilder);
        }


        return false;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_grouplist_edit) {

            try {
                startActivity(new Intent(EditOpenBalanceDialogActivity.this, SHGGroupActivity.class));
                finish();
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
/*                if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
                    PrefUtils.setLoginGroupService("2");
                    new Login_webserviceTask(MainActivity.this).execute();
                } else {
                    startActivity(new Intent(this, SHGGroupActivity.class));
                    overridePendingTransition(R.anim.right_to_left_in, R.anim.right_to_left_out);
                    finish();
                }*/
            return true;

        } else if (id == R.id.menu_logout_edit) {
            Log.e(" Logout", "Logout Sucessfully");
            startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

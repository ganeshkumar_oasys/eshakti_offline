package com.oasys.eshakti.activity;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.Dto.TableData;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.R;
import com.oasys.eshakti.database.SHGTable;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Animator extends Fragment  {

    private TextView cashinHand;
    private TextView cashinhandvalue;
    private TextView cashatbank;
    private TextView cashatbankvalue;
    private TextView animatorprofile;
    private TextView animatorname;
    private TextView animatornamevalue;
    private TextView totalgroup;
    private TextView totalgroupvalue;
    private TextView telephonenumber;
    private TextView telephonenumbervalue;
    private TextView age;
    private TextView agevalue;
    private TextView date;
    private TextView animator_name;
    private TextView datevalue;
    ResponseDto animatorProfileDto;
    private View rootView;
    private ListOfShg shgDto;
    private String userId;
    private String langId;
    private String animatorid;
    private TableData tableData;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //rootView = inflater.inflate(R.layout.activity_animator, container, false);
        rootView = inflater.inflate(R.layout.animator_layout, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));


        userId = MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, "");
        langId = MySharedPreference.readString(getActivity(), MySharedPreference.LANG_ID, "");
        tableData = SHGTable.getAnimatorDetails(userId);
        Log.d("animatordetails",""+tableData);
       /* if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

            String url = Constants.BASE_URL + Constants.SHG_URL + userId + "&languageId=" + langId;
            RestClient.getRestClient(Animator.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.SHG_LIST);

        }*/

       /* String url = Constants.BASE_URL + Constants.PROFILE_ANIMATOR + userId;
        Log.d("url", url);
        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
            RestClient.getRestClient(Animator.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.ANIMATOR_PROFILE);
        } else {
            Utils.showToast(getActivity(), "Network Not Available");
        }*/

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
       /* String url = Constants.BASE_URL + Constants.PROFILE_ANIMATOR + "fc9a1917-2931-475f-a24c-2e7f224e83ac";

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
            RestClient.getRestClient(Animator.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.ANIMATOR_PROFILE);
        } else {
            Utils.showToast(getActivity(), "Network Not Available");
        }*/
    }

    private void initView() {
        /*cashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
        cashinHand.setText(AppStrings.cashinhand);
        cashinHand.setTypeface(LoginActivity.sTypeface);*/

        animator_name = (TextView) rootView.findViewById(R.id.groupname);
        animator_name.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
        animator_name.setTypeface(LoginActivity.sTypeface);

        cashinHand = (TextView) rootView.findViewById(R.id.cashinHand);

        cashinHand.setText("CASH IN HAND: ₹" + shgDto.getCashInHand());
        cashinHand.setTypeface(LoginActivity.sTypeface);

        /*cashatbank = (TextView) rootView.findViewById(R.id.cashatbank);
        cashatbank.setText(AppStrings.cashatBank);
        cashatbank.setTypeface(LoginActivity.sTypeface);*/

        cashatbank = (TextView) rootView.findViewById(R.id.cashatBank);
        cashatbank.setText("CASH AT BANK: ₹" + shgDto.getCashAtBank());
        cashatbank.setTypeface(LoginActivity.sTypeface);

        animatorprofile = (TextView) rootView.findViewById(R.id.animatorprofile);
        animatorprofile.setText(AppStrings.animator_profile);
        animatorprofile.setTypeface(LoginActivity.sTypeface);

        animatorname = (TextView) rootView.findViewById(R.id.animatorname);
        animatorname.setText(AppStrings.animator_name);
        animatorname.setTypeface(LoginActivity.sTypeface);

        animatornamevalue = (TextView) rootView.findViewById(R.id.animatornamevalue);
        animatornamevalue.setText(tableData.getAnimatorName());
        animatornamevalue.setTypeface(LoginActivity.sTypeface);

        totalgroup = (TextView) rootView.findViewById(R.id.totalgroup);
        totalgroup.setText(AppStrings.Total_group);
        totalgroup.setTypeface(LoginActivity.sTypeface);

        totalgroupvalue = (TextView) rootView.findViewById(R.id.totalgroupvalue);
        int totalgrp=(int) Double.parseDouble(tableData.getTotalGroup());
        totalgroupvalue.setText(""+totalgrp);
        totalgroupvalue.setTypeface(LoginActivity.sTypeface);

        telephonenumber = (TextView) rootView.findViewById(R.id.telephonenumber);
        telephonenumber.setText(AppStrings.telephonenumber);
        telephonenumber.setTypeface(LoginActivity.sTypeface);

        telephonenumbervalue = (TextView) rootView.findViewById(R.id.telephonenumbervalue);
        String phoneNum= String.valueOf(tableData.getPhoneNumber());
        telephonenumbervalue.setText(phoneNum);
        telephonenumbervalue.setTypeface(LoginActivity.sTypeface);

        age = (TextView) rootView.findViewById(R.id.age);
        age.setText(AppStrings.age);
        age.setTypeface(LoginActivity.sTypeface);

        agevalue = (TextView) rootView.findViewById(R.id.agevalue);

        if (tableData.getAge()!=null)
        {
            int age=(int) Double.parseDouble(tableData.getAge());
            agevalue.setText(""+age);
            agevalue.setTypeface(LoginActivity.sTypeface);
        }
        else
        {
            agevalue.setText("-");
            agevalue.setTypeface(LoginActivity.sTypeface);
        }



        date = (TextView) rootView.findViewById(R.id.date);
        date.setText(AppStrings.dateofassigning);
        date.setTypeface(LoginActivity.sTypeface);

        datevalue = (TextView) rootView.findViewById(R.id.datevalue);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date d = new Date(tableData.getDateOfAssigning());
        String dateStr = df.format(d);
        datevalue.setText(dateStr);
        datevalue.setTypeface(LoginActivity.sTypeface);


    }

/*
    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        switch (serviceType) {

            case SHG_LIST:
                try {
                    if (result != null && result.length() > 0) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                        int statusCode = lrDto.getStatusCode();
                        String message = lrDto.getMessage();
                        Log.d("response status", " " + statusCode);
                        if (statusCode == 400 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                            // showMessage(statusCode);
                            Utils.showToast(getActivity(), message);

                        } else if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        } else if (statusCode == Utils.Success_Code) {

                            animatorid = lrDto.getResponseContent().getAnimatorId();

//                            String url = Constants.BASE_URL + Constants.PROFILE_ANIMATOR + animatorid;
//                            Log.d("url", url);

//                            if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
//                                RestClient.getRestClient(Animator.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.ANIMATOR_PROFILE);
//                            } else {
//                                Utils.showToast(getActivity(), "Network Not Available");
//                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;*/
            /*case ANIMATOR_PROFILE:
//                Log.d("getDetails", " " + result.toString());
                try {
                    if (result != null && result.length() > 0) {

                        animatorProfileDto = new Gson().fromJson(result, ResponseDto.class);
                        if (animatorProfileDto.getStatusCode() == Utils.Success_Code) {
                            Utils.showToast(getActivity(), animatorProfileDto.getMessage());

                            if ((animatorProfileDto.getResponseContent() != null)) {

                                animatornamevalue.setText(animatorProfileDto.getResponseContent().getAnimatorProfile().getAnimatorName());
                                totalgroupvalue.setText(animatorProfileDto.getResponseContent().getAnimatorProfile().getTotalGroup());
                                telephonenumbervalue.setText(animatorProfileDto.getResponseContent().getAnimatorProfile().getPhoneNumber());
                                agevalue.setText(animatorProfileDto.getResponseContent().getAnimatorProfile().getAge());

                                if (animatorProfileDto.getResponseContent().getAnimatorProfile().getDateOfAssigning() != null && !animatorProfileDto.getResponseContent().getAnimatorProfile().getDateOfAssigning().equals("-")) {
                                    DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
                                    Date d = new Date(Long.parseLong(animatorProfileDto.getResponseContent().getAnimatorProfile().getDateOfAssigning()));
                                    String dateStr = simple.format(d);
                                    datevalue.setText(dateStr);
                                } else {
                                    datevalue.setText("-");
                                }
//                       datevalue.setText(animatorProfileDto.getResponseContent().getAnimatorProfile().getDateOfAssigning());
                            }

                        }

                       else if (animatorProfileDto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            Utils.showToast(getActivity(), "Network error");
                        }
                       else {

                            Utils.showToast(getActivity(), animatorProfileDto.getMessage());

                        }
                    }


                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
*/



}




package com.oasys.eshakti.activity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.oasys.eshakti.Dialogue.TableExceptionErrorDialog;
import com.oasys.eshakti.Dto.BaseDTO;
import com.oasys.eshakti.Dto.FirstSynchReqDto;
import com.oasys.eshakti.Dto.FirstSynchResDto;
import com.oasys.eshakti.Dto.FistSyncInputDto;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.OnSuccessCallback;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.R;
import com.oasys.eshakti.Service.NewTaskListener;
import com.oasys.eshakti.Service.RestClient;
import com.oasys.eshakti.database.DbHelper;
import com.oasys.eshakti.enums.TableNames;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SyncActivity extends BaseActivity implements NewTaskListener {

    private RestClient restClient;
    private NetworkConnection networkConnection;
    List<FistSyncInputDto> firstSync;  //FistSync items
    TableExceptionErrorDialog tableExceptionErrorDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);

        restClient = RestClient.getRestClient(this);
        networkConnection = NetworkConnection.getNetworkConnection(getApplicationContext());

        firstTimeSyncDetails();
    }

    public void firstTimeSyncDetails() {
        try {
            FirstSynchReqDto fpsRequest = new FirstSynchReqDto();
            String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID).toUpperCase();
            fpsRequest.setDeviceNum(deviceId);
//            serverUrl = DbHelper.getInstance(this).getMasterData("serverUrl");
            String updateData = new Gson().toJson(fpsRequest);
            if (networkConnection.isNetworkAvailable()) {
                restClient.callRestWebService(Constants.GET_TABLE_NAME_URL, updateData, SyncActivity.this, ServiceType.GET_TABLE_NAME_API);
            } else {
                Toast.makeText(SyncActivity.this, R.string.check_internet, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.e("SyncPageActivity", e.toString(), e);

        }

    }


    private void syncTableDetails(Map<String, Integer> tableDetails) {
        Log.e("SyncPageActivity ", "syncTableDetails() called tableDetails ->" + tableDetails);

        firstSync = new ArrayList<>();
        List<String> masterDataEmpty = new ArrayList<>();
       /* int count = tableDetails.get("TABLE_WATER_SOURCE");

       //TODO::  Need to add New Table Name

        if (count > 0)
            firstSync.add(getInputDTO("TABLE_WATER_SOURCE", count, "TABLE_WATER_SOURCE downloading", "TABLE_WATER_SOURCE downloaded with", TableNames.TABLE_WATER_SOURCE));*/

        int count = tableDetails.get("OFFLINE_ANIMATOR_SHG_INCOME_EXPENSE_TYPE");
        if (count > 0)
            firstSync.add(getInputDTO("OFFLINE_ANIMATOR_SHG_INCOME_EXPENSE_TYPE", count, "OFFLINE_ANIMATOR_SHG_INCOME_EXPENSE_TYPE downloading", "OFFLINE_ANIMATOR_SHG_INCOME_EXPENSE_TYPE downloaded with", TableNames.TABLE_STATE));

        count = tableDetails.get("OFFLINE_ANIMATOR_SHG_LIST");
        if (count > 0)
            firstSync.add(getInputDTO("OFFLINE_ANIMATOR_SHG_LIST", count, "OFFLINE_ANIMATOR_SHG_LIST downloading", "OFFLINE_ANIMATOR_SHG_LIST downloaded with", TableNames.TABLE_VILLAGE));

        count = tableDetails.get("OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST");
        if (count > 0)
            firstSync.add(getInputDTO("OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST", count, "OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST downloading", "OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST downloaded with", TableNames.TABLE_BLOCK));

        count = tableDetails.get("OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST");
        if (count > 0)
            firstSync.add(getInputDTO("OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST", count, "OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST downloading", "OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST downloaded with", TableNames.TABLE_GRAMPANCHAYAT));

        count = tableDetails.get("OFFLINE_ANIMATOR_SHG_TRANSACTION_DETAILS");
        if (count > 0)
            firstSync.add(getInputDTO("OFFLINE_ANIMATOR_SHG_TRANSACTION_DETAILS", count, "OFFLINE_ANIMATOR_SHG_TRANSACTION_DETAILS downloading", "OFFLINE_ANIMATOR_SHG_TRANSACTION_DETAILS downloaded with", TableNames.TABLE_DISTRICT));

        count = tableDetails.get("OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST");
        if (count > 0)
            firstSync.add(getInputDTO("OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST", count, "OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST downloading", "OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST downloaded with", TableNames.TABLE_DRAINAGE_TYPE));


        count = tableDetails.get("OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST");
        if (count > 0)
            firstSync.add(getInputDTO("OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST", count, "OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST downloading", "OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST downloaded with", TableNames.TABLE_HOUSE_TYPE));

        count = tableDetails.get("OFFLINE_ANIMATOR_MEMBER_LIST");
        if (count > 0)
            firstSync.add(getInputDTO("OFFLINE_ANIMATOR_MEMBER_LIST", count, "OFFLINE_ANIMATOR_MEMBER_LIST downloading", "OFFLINE_ANIMATOR_MEMBER_LIST downloaded with", TableNames.TABLE_TOILET_TYPE));
        count = tableDetails.get("OFFLINE_ANIMATOR_SHG_BANK_LIST");
        if (count > 0)
            firstSync.add(getInputDTO("OFFLINE_ANIMATOR_SHG_BANK_LIST", count, "OFFLINE_ANIMATOR_SHG_BANK_LIST  downloading", " OFFLINE_ANIMATOR_SHG_BANK_LIST downloaded with", TableNames.TABLE_RELATIONSHIP));


        FirstSynchReqDto fpsRequest = new FirstSynchReqDto();
        String deviceId = Settings.Secure.getString(
                getContentResolver(), Settings.Secure.ANDROID_ID).toUpperCase();
        fpsRequest.setDeviceNum(deviceId);
        fpsRequest.setTableName(firstSync.get(0).getTableName());
        setTableSyncCall(fpsRequest);
//        setTextStrings(firstSync.get(0).getTextToDisplay() + "....");

    }

    private void setTableSyncCall(FirstSynchReqDto fpsRequest) {
        try {
            Log.e("SyncPageActivity ", "setTableSyncCall() called FirstSynchReqDto ->" + fpsRequest);
            String updateData = new Gson().toJson(fpsRequest);
            if (networkConnection.isNetworkAvailable()) {
                restClient.callRestWebService(Constants.GET_TABLE_DETAILS_URL, updateData, SyncActivity.this,
                        ServiceType.GET_TABLE_DETAILS_API);
            } else {
                Toast.makeText(SyncActivity.this, R.string.check_internet, Toast.LENGTH_SHORT).show();
            }


        } catch (Exception e) {
            //Log.e("SyncPageActivity", e.toString(), e);
            Log.e("SyncPageActivity ", "setTableSyncCall() called Exception ->" + e);

        }
    }

    private FistSyncInputDto getInputDTO(String tableName, int count, String textToDisplay, String endText, TableNames names) {
        Log.e("SyncPageActivity ", "getInputDTO() called tableName ->" + tableName);

        FistSyncInputDto inputDto = new FistSyncInputDto();
        inputDto.setTableName(tableName);
        inputDto.setCount(count);
        inputDto.setTableNames(names);
        inputDto.setTextToDisplay(textToDisplay);
        inputDto.setEndTextToDisplay(endText);
        inputDto.setDynamic(true);
        return inputDto;
    }

    private void setTableResponse(FirstSynchResDto fpsDataDto) {
        try {
//            Log.e("SyncPageActivity ", "setTableResponse() called  response ->" + response);

//            GsonBuilder gsonBuilder = new GsonBuilder();
//            Gson gson = gsonBuilder.create();
//            FirstSynchResDto fpsDataDto = gson.fromJson(response, FirstSynchResDto.class);
            if (fpsDataDto.getStatusCode() == 0) {
                insertIntoDatabase(fpsDataDto);
            } else {
//                errorInSync();
                return;
            }
        } catch (Exception e) {
            //    Util.LoggingQueue(this, "Sync Page", "setTableResponse Exception..."+e);
            Log.e("SyncPageActivity ", "setTableResponse() called Exception " + e);
//            errorInSync();
            return;
        }
    }

    private void insertIntoDatabase(FirstSynchResDto firstSynchResDto) {
        try {
            Log.e("SyncPageActivity ", "insertIntoDatabase() called  ");
            FistSyncInputDto fistSyncInputDto = firstSync.get(0);
//            setTextStrings(firstSync.get(0).getEndTextToDisplay() + " items " + firstSynchResDto.getTotalSentCount() + "....");
//            Util.LoggingQueue(SyncPageActivity.this, "SyncPageActivity ", "insertIntoDatabase() called  getTableNames ->" + fistSyncInputDto.getTableNames());
            boolean isExceptionThrown;
            switch (fistSyncInputDto.getTableNames()) {
                case OFFLINE_ANIMATOR_MEMBER_LIST:
                 /*   isExceptionThrown = DbHelper.getInstance(this).insertProductGroup(firstSynchResDto.getProductGroupDtos(), "FirstSync");
                    if (!isExceptionThrown) {

                        exceptionInSync();
                        return;
                    }*/
                    break;
                case OFFLINE_ANIMATOR_SHG_LIST:
                   /* isExceptionThrown = DbHelper.getInstance(this).insertCardTypeData(firstSynchResDto.getCardtypeDto(), "FirstSync");
                    if (!isExceptionThrown) {

                        exceptionInSync();
                        return;
                    }*/
                    break;
            /*   case OFFLINE_ANIMATOR_SHG_INCOME_EXPENSE_TYPE:
                    isExceptionThrown = DbHelper.getInstance(this).insertCardTypeData(firstSynchResDto.getCardtypeDto(), "FirstSync");
                    if (!isExceptionThrown) {

                        exceptionInSync();
                        return;
                    }
                    break;

                     case OFFLINE_ANIMATOR_SHG_MEMBER_LOAN_REPAYMENT_LIST:
                    isExceptionThrown = DbHelper.getInstance(this).insertCardTypeData(firstSynchResDto.getCardtypeDto(), "FirstSync");
                    if (!isExceptionThrown) {
                      
                        exceptionInSync();
                        return;
                    }
                    break;
                case OFFLINE_ANIMATOR_SHG_GROUP_LOAN_REPAYMENT_LIST:
                    isExceptionThrown = DbHelper.getInstance(this).insertCardTypeData(firstSynchResDto.getCardtypeDto(), "FirstSync");
                    if (!isExceptionThrown) {
                      
                        exceptionInSync();
                        return;
                    }
                    break;
                case OFFLINE_ANIMATOR_SHG_TRANSACTION_DETAILS:
                    isExceptionThrown = DbHelper.getInstance(this).insertCardTypeData(firstSynchResDto.getCardtypeDto(), "FirstSync");
                    if (!isExceptionThrown) {
                      
                        exceptionInSync();
                        return;
                    }
                    break;


                case OFFLINE_ANIMATOR_SHG_BANK_TRANSACTION_LIST:
                    isExceptionThrown = DbHelper.getInstance(this).insertProductGroup(firstSynchResDto.getProductGroupDtos(), "FirstSync");
                    if (!isExceptionThrown) {
                      
                        exceptionInSync();
                        return;
                    }
                    break;

                case OFFLINE_ANIMATOR_SHG_INTERNAL_LOAN_LIST:
                    isExceptionThrown = DbHelper.getInstance(this).insertProductGroup(firstSynchResDto.getProductGroupDtos(), "FirstSync");
                    if (!isExceptionThrown) {
                      
                        exceptionInSync();
                        return;
                    }
                    break;

                case OFFLINE_ANIMATOR_SHG_BANK_LIST:
                    isExceptionThrown = DbHelper.getInstance(this).insertProductGroup(firstSynchResDto.getProductGroupDtos(), "FirstSync");
                    if (!isExceptionThrown) {
                      
                        exceptionInSync();
                        return;
                    }
                    break;
*/

                default:
                    break;
            }
            afterDatabaseInsertion(firstSynchResDto);
        } catch (Exception e) {
            Log.e("SyncPageActivity ", "insertIntoDatabase() called  Exception ->" + e);
            exceptionInSync();

        }
    }

    private void afterDatabaseInsertion(FirstSynchResDto firstSynchResDto) {

        Log.e("SyncPageActivity ", "afterDatabaseInsertion() called  FirstSynchResDto ->" + firstSynchResDto);

        FirstSynchReqDto fpsRequest = new FirstSynchReqDto();
        String deviceId = Settings.Secure.getString(
                getContentResolver(), Settings.Secure.ANDROID_ID).toUpperCase();
        fpsRequest.setDeviceNum(deviceId);
        if (firstSynchResDto.isHasMore()) {
            fpsRequest.setTotalCount(firstSynchResDto.getTotalCount());
            fpsRequest.setTotalSentCount(firstSynchResDto.getTotalSentCount());
            fpsRequest.setCurrentCount(firstSynchResDto.getCurrentCount());
            fpsRequest.setTableName(firstSync.get(0).getTableName());
            setTableSyncCall(fpsRequest);
        } else {
            firstSync.remove(0);
//            setDownloadedProgress();
            if (firstSync.size() > 0) {
                fpsRequest.setTableName(firstSync.get(0).getTableName());
//                setTextStrings(firstSync.get(0).getTextToDisplay() + "....");
                setTableSyncCall(fpsRequest);
            } else {
//                getOpeningStock();
//                firstSyncSuccess();
//                getOpeningStockInLocal();

                MySharedPreference.writeBoolean(getApplicationContext(), MySharedPreference.ForceSyncSuccess, true);
                Toast.makeText(SyncActivity.this, "First Sync Success", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(SyncActivity.this, SHGGroupActivity.class));
                finish();
//                Log.e("First Sync Success", "------------------XXXXXXXXXXXXX__________________");
            }
        }
    }


    public void exceptionInSync() {
        Log.e("SyncPageActivity ", "exceptionInSync() called   ->");
        tableExceptionErrorDialog = new TableExceptionErrorDialog(this);
        tableExceptionErrorDialog.show();
    }


    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String response, ServiceType serviceType) {
        switch (serviceType) {
            case GET_TABLE_NAME_API:
                if (response != null) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    BaseDTO baseDTO = gson.fromJson(response, BaseDTO.class);
                    if (baseDTO.getStatusCode() == 0) {
                        FirstSynchResDto synchResDto = null;
                        ObjectMapper mapper = new ObjectMapper();
                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
                        try {
                            String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
                            synchResDto = gson.fromJson(jsonValue, new TypeToken<FirstSynchResDto>() {
                            }.getType());
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }
                        if (synchResDto.getStatusCode() == 0) {
//                            Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
                            if ((synchResDto.getTableDetails() == null || synchResDto.getTableDetails().isEmpty())) {
//                            errorInSync();
                                return;
                            }
                            syncTableDetails(synchResDto.getTableDetails());

                        } else {
                            Toast.makeText(this, "Fail", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, getString(R.string.error_network_timeout), Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case GET_TABLE_DETAILS_API:
                if (response != null) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    Log.e("Log in RESPONSE ", " ::::::::: " + response);
                    BaseDTO baseDTO = gson.fromJson(response, BaseDTO.class);
                    if (baseDTO.getStatusCode() == 0) {
                        FirstSynchResDto synchResDto = null;
                        ObjectMapper mapper = new ObjectMapper();
                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
                        try {
                            String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
                            synchResDto = gson.fromJson(jsonValue, new TypeToken<FirstSynchResDto>() {
                            }.getType());
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }

                        if (synchResDto.getStatusCode() == 0) {
//                            Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
                            setTableResponse(synchResDto);

                        } else {
                            Toast.makeText(this, "Fail", Toast.LENGTH_SHORT).show();
                        }


                    } else {
                        Toast.makeText(this, "Fail", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(this, getString(R.string.error_network_timeout), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}

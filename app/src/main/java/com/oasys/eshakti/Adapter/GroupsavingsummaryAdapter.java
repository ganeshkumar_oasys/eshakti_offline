package com.oasys.eshakti.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.Dto.GroupSavingsSummary;
import com.oasys.eshakti.R;

import java.util.ArrayList;

public class GroupsavingsummaryAdapter extends RecyclerView.Adapter<GroupsavingsummaryAdapter.MyViewholder> {

    private Context context;
    private ArrayList<GroupSavingsSummary> groupSavingsSummaries ;

    public GroupsavingsummaryAdapter(Context context, ArrayList<GroupSavingsSummary> groupSavingsSummaries) {
        this.context = context;
        this.groupSavingsSummaries = groupSavingsSummaries;
    }

    @NonNull
    @Override
    public MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.groupsavingviews, parent, false);
        return new MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder holder, int position) {
    holder.groupsavings_name.setText(groupSavingsSummaries.get(position).getName());
    holder.groupsavings_amount.setText(groupSavingsSummaries.get(position).getSavingsAmount());
    holder.groupsavings_voluntarysaving.setText(groupSavingsSummaries.get(position).getVoluntrySavingsAmount());
    }

    @Override
    public int getItemCount() {
        return groupSavingsSummaries.size();
    }

    class MyViewholder extends RecyclerView.ViewHolder {
        TextView groupsavings_name,groupsavings_amount,groupsavings_voluntarysaving;



        public MyViewholder(View itemView) {
            super(itemView);
            groupsavings_name = (TextView) itemView.findViewById(R.id.mGroupreportName);
            groupsavings_amount = (TextView) itemView.findViewById(R.id.mGroupreportsavings);
            groupsavings_voluntarysaving = (TextView) itemView.findViewById(R.id.mGroupreporVoluntary);

        }
    }
}

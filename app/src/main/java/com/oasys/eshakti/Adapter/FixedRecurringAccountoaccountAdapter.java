package com.oasys.eshakti.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.Dto.GroupBankTransactionSummaryList;
import com.oasys.eshakti.R;
import com.oasys.eshakti.activity.LoginActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class FixedRecurringAccountoaccountAdapter extends RecyclerView.Adapter<FixedRecurringAccountoaccountAdapter.ViewHolder> {

    private Context context;
    private ArrayList<GroupBankTransactionSummaryList> groupBankTransactionSummaryDTOLists;

    public FixedRecurringAccountoaccountAdapter(Context context, ArrayList<GroupBankTransactionSummaryList> groupBankTransactionSummaryDTOLists) {
        this.context = context;
        this.groupBankTransactionSummaryDTOLists = groupBankTransactionSummaryDTOLists;
    }


    @NonNull
    @Override
    public FixedRecurringAccountoaccountAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bank_transaction_view, parent, false);
        return new FixedRecurringAccountoaccountAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FixedRecurringAccountoaccountAdapter.ViewHolder holder, int position) {
        try {
            String amount = "";
            amount = groupBankTransactionSummaryDTOLists.get(position).getAmount();
            int oam = (int) Double.parseDouble(amount);
            holder.mamount.setText(oam + "");


            holder.mdetails.setText(groupBankTransactionSummaryDTOLists.get(position).getDetails());
            DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
            Date d = new Date(Long.parseLong(groupBankTransactionSummaryDTOLists.get(position).getTransactiondate()));
            String dateStr = simple.format(d);
            holder.mTransactionDate.setText(dateStr);
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {
        return groupBankTransactionSummaryDTOLists.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mamount;
        TextView mdetails;
        TextView mTransactionDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mamount = (TextView) itemView.findViewById(R.id.bts_amount);
            mamount.setTypeface(LoginActivity.sTypeface);
            mdetails = (TextView) itemView.findViewById(R.id.bts_details);
            mdetails.setTypeface(LoginActivity.sTypeface);
            mTransactionDate = (TextView) itemView.findViewById(R.id.bts_date);
            mTransactionDate.setTypeface(LoginActivity.sTypeface);
        }
    }
}

package com.oasys.eshakti.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.R;

import java.util.List;

public class MemberreportAdapter extends RecyclerView.Adapter<MemberreportAdapter.MyViewholder> {

    private Context context;
    private List<MemberList> personnames ;

    public MemberreportAdapter(Context context, List<MemberList> personnames) {
        this.context = context;
        this.personnames = personnames;
    }

    @NonNull
    @Override
    public MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.memberreportitem, parent, false);
        return new MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder holder,final int position) {
        holder.mMemberreportName.setText(personnames.get(position).getMemberName());


    }


    @Override
    public int getItemCount() {
        return personnames.size();
    }

    class MyViewholder extends RecyclerView.ViewHolder {
        TextView mMemberreportName;


        public MyViewholder(View itemView) {
            super(itemView);
            mMemberreportName = (TextView) itemView.findViewById(R.id.mMemberreportName);

        }
    }
}

package com.oasys.eshakti.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.oasys.eshakti.Dto.BranchList;
import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.R;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.database.BankBranchTable;
import com.oasys.eshakti.database.SHGTable;

import java.util.ArrayList;
import java.util.List;

import static com.oasys.eshakti.fragment.JDFragment.janadhanselected_bank_branches;

@SuppressLint("ValidFragment")
public class Dialog_jandhanAccountNumberUpdation extends DialogFragment {

    Context mContext;
    List<String> listArr = new ArrayList<String>();
    private EditText mSearchEditText;
    private ListView mListView;
    private ArrayAdapter<String> adapter = null;
    int mRowPos, mColPos;
    TableLayout mTableLayout;
    private TextView mDialogHeader;
    public static  String s;
    public  static  List<BranchList> branchLists;
    public static  List<String> branchname;
    private ListOfShg shgDto;


    @SuppressLint("ValidFragment")
    public Dialog_jandhanAccountNumberUpdation(Context context, List<String> mBankNameWithoutDupList, int rowPos, int colPos, TableLayout tableLayout) {
        mContext = context;
        listArr = mBankNameWithoutDupList;
        mRowPos = rowPos;
        mColPos = colPos;
        mTableLayout = tableLayout;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.dialog_search, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MaterialDialog);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));

        try {
            mDialogHeader = (TextView) rootView.findViewById(R.id.dialog_header);
            mDialogHeader.setText("CHOOSE AN BANK");
            mDialogHeader.setTypeface(LoginActivity.sTypeface);

            mSearchEditText = (EditText) rootView.findViewById(R.id.searchEditText);
            mSearchEditText.setTypeface(LoginActivity.sTypeface);

            mSearchEditText.addTextChangedListener(filterTextWatcher);
            mListView = (ListView) rootView.findViewById(R.id.dialogListView);

            adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, listArr) {
                @SuppressWarnings("deprecation")
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view = super.getView(position, convertView, parent);

                    TextView textview = (TextView) view.findViewById(android.R.id.text1);

                    textview.setTextSize(14);
                    textview.setTypeface(LoginActivity.sTypeface);
                    textview.setTextColor(getResources().getColor(R.color.black));

                    return view;

                }
            };
            mListView.setAdapter(adapter);

            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                    Log.d("Dialog_ListviewFragment", "Selected Item is = " + mListView.getItemAtPosition(position));
                    dismiss();
                    try {

                        View view = mTableLayout.getChildAt(mRowPos);
                        TableRow r = (TableRow) view;
                        TextView getTextview = (TextView) r.getChildAt(mColPos);
                        s = BankBranchTable.jandhanbankname.get(position);
                        getTextview.setText(s);
                        branchLists = BankBranchTable.getJandhanBranchList(BankBranchTable.jandhanbankList.get(position).getId());
                        janadhanselected_bank_branches.put(mRowPos, BankBranchTable.getJandhanBranchList(BankBranchTable.jandhanbankList.get(position).getId()));

                        getTextview.setTextSize(14);
                        getTextview.setTypeface(LoginActivity.sTypeface);
                        getTextview.setTextColor(getResources().getColor(R.color.black));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return rootView;
    }

    private TextWatcher filterTextWatcher = new TextWatcher() {

        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            adapter.getFilter().filter(s);
        }
    };
}

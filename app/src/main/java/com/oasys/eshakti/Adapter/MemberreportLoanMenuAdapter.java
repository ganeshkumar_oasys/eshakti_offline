package com.oasys.eshakti.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.R;
import com.oasys.eshakti.activity.LoginActivity;

import java.util.ArrayList;

public class MemberreportLoanMenuAdapter extends RecyclerView.Adapter<MemberreportLoanMenuAdapter.MyViewholder> {

    private Context context;
    private ArrayList<String> loanmodes;

    public MemberreportLoanMenuAdapter(Context context, ArrayList<String> loanmodes) {
        this.context = context;
        this.loanmodes = loanmodes;
    }

    @NonNull
    @Override
    public MemberreportLoanMenuAdapter.MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.memberreportitem, parent, false);
        return new MemberreportLoanMenuAdapter.MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder holder, final int position) {
        holder.mMemberreportNameloan.setText(loanmodes.get(position));
        holder.mMemberreportNameloan.setTypeface(LoginActivity.sTypeface);

    }

    @Override
    public int getItemCount() {
        return loanmodes.size();
    }

    class MyViewholder extends RecyclerView.ViewHolder {
        TextView mMemberreportNameloan;


        public MyViewholder(View itemView) {
            super(itemView);
            mMemberreportNameloan = (TextView) itemView.findViewById(R.id.mMemberreportName);

        }
    }
}

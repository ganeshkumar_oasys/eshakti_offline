package com.oasys.eshakti.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.Dto.GroupInternalLoansList;
import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.R;
import com.oasys.eshakti.database.MemberTable;

import java.util.ArrayList;
import java.util.List;

public class GroupReport_Grouploansummary_Internalloan_Adapter extends RecyclerView.Adapter<GroupReport_Grouploansummary_Internalloan_Adapter.MyViewholder> {

    private Context context;
    private ArrayList<GroupInternalLoansList> groupInternalLoansLists;
    private List<MemberList> memList;

    public GroupReport_Grouploansummary_Internalloan_Adapter(Context context, ArrayList<GroupInternalLoansList> groupInternalLoansLists) {
        this.context = context;

        this.groupInternalLoansLists = groupInternalLoansLists;
    }

    @NonNull
    @Override
    public MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.groupsavingviews, parent, false);
        return new MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder holder, int position) {

        memList = MemberTable.getMemberList(MySharedPreference.readString(context, MySharedPreference.SHG_ID, ""));

         holder.groupsavings_name.setText(groupInternalLoansLists.get(position).getGroupMemberName());
//        holder.groupsavings_name.setText(memList.get(position).getMemberName());
        holder.groupsavings_amount.setText(groupInternalLoansLists.get(position).getAmount());
        holder.groupsavings_voluntarysaving.setText(groupInternalLoansLists.get(position).getLoanAmountRepaid());

        Log.i("print",groupInternalLoansLists.get(position).getGroupMemberName());
    }

    @Override
    public int getItemCount() {
        return groupInternalLoansLists.size();
    }


    class MyViewholder extends RecyclerView.ViewHolder {
        TextView groupsavings_name, groupsavings_amount, groupsavings_voluntarysaving;


        public MyViewholder(View itemView) {
            super(itemView);


            groupsavings_name = (TextView) itemView.findViewById(R.id.mGroupreportName);
            groupsavings_amount = (TextView) itemView.findViewById(R.id.mGroupreportsavings);
            groupsavings_voluntarysaving = (TextView) itemView.findViewById(R.id.mGroupreporVoluntary);

        }
    }
}

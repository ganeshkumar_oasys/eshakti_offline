package com.oasys.eshakti.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.Dto.MembermonthlyReport;
import com.oasys.eshakti.R;

import java.util.ArrayList;

public class MonthlyReportAdapter extends RecyclerView.Adapter<MonthlyReportAdapter.ViewHolder> {

    private Context context;
    private ArrayList<MembermonthlyReport> membermonthlyReport;

    public MonthlyReportAdapter(Context context, ArrayList<MembermonthlyReport> membermonthlyReport) {
        this.context = context;
        this.membermonthlyReport = membermonthlyReport;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.monthly_report_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mloanoutstanding.setText(membermonthlyReport.get(position).getAmount());
        holder.mloantype.setText(membermonthlyReport.get(position).getName());
    }

    @Override
    public int getItemCount() {
         return membermonthlyReport.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mloantype;
        TextView mloanoutstanding;

        public ViewHolder(View itemView) {
            super(itemView);
            mloanoutstanding = (TextView) itemView.findViewById(R.id.amount);
            mloantype = (TextView) itemView.findViewById(R.id.typename);
        }
    }
}

package com.oasys.eshakti.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.oasys.eshakti.Dto.MemberList;
import com.oasys.eshakti.R;

import java.util.List;

public class ProfileNumberAdapter extends RecyclerView.Adapter<ProfileNumberAdapter.ViewHolder> {
    private Context context;
    private List<MemberList> profileNumberDtos;
  //  private PhoneNoUpdate dataPasser;

    public ProfileNumberAdapter(Context context, List<MemberList> profileNumberDtos) {
        this.context = context;
        this.profileNumberDtos = profileNumberDtos;
    //    dataPasser=(PhoneNoUpdate) context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_number_updation_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mProfileName.setText(profileNumberDtos.get(position).getMemberName());
        holder.mProfileNumber.setText(profileNumberDtos.get(position).getPhoneNumber());
        profileNumberDtos.get(position).setPhoneNumber(holder.mProfileNumber.getText().toString());
        //dataPasser.updateContactNo(profileNumberDtos);
    }

   /* public interface PhoneNoUpdate{
        public void updateContactNo(List<MemberList> memList);
    }*/

    @Override
    public int getItemCount() {
        return profileNumberDtos.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mProfileName;
        EditText mProfileNumber;

        public ViewHolder(View itemView) {
            super(itemView);
            mProfileName = (TextView) itemView.findViewById(R.id.mProfileName);
            mProfileNumber = (EditText) itemView.findViewById(R.id.mProfileNumber);
        }
    }
}

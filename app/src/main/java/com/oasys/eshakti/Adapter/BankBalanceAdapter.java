package com.oasys.eshakti.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.Dto.BankBalanceDTOList;
import com.oasys.eshakti.R;

import java.util.ArrayList;

/**
 * Created by MuthukumarPandi on 12/8/2018.
 */

public class BankBalanceAdapter extends RecyclerView.Adapter<BankBalanceAdapter.ViewHolder> {
    private Context context;
    private ArrayList<BankBalanceDTOList> bankBalanceDTOLists;

    public BankBalanceAdapter(Context context, ArrayList<BankBalanceDTOList> bankBalanceDTOLists) {
        this.context = context;
        this.bankBalanceDTOLists = bankBalanceDTOLists;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bankbalance_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mloanoutstanding.setText(bankBalanceDTOLists.get(position).getLoanOutstanding());
        holder.mloantype.setText(bankBalanceDTOLists.get(position).getLoanType() + " OUTSTANDING AS PER BOOK :");
        //holder.mloantype.setText(bankBalanceDTOLists.get(position).getLoanType());
    }

    @Override
    public int getItemCount() {
        return bankBalanceDTOLists.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mloanoutstanding;
        TextView mloantype;

        public ViewHolder(View itemView) {
            super(itemView);
            mloanoutstanding = (TextView) itemView.findViewById(R.id.bb_loanoutstanding);
            mloantype = (TextView) itemView.findViewById(R.id.bb_loantype);
        }
    }

}

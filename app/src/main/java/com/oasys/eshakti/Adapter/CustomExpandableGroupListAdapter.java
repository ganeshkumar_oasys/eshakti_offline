package com.oasys.eshakti.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.oasys.eshakti.Dto.ListOfShg;
import com.oasys.eshakti.OasysUtils.AppStrings;
import com.oasys.eshakti.OasysUtils.ExpandListItemClickListener;
import com.oasys.eshakti.OasysUtils.GroupListItem;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.NetworkConnection;
import com.oasys.eshakti.OasysUtils.RegionalConversion;
import com.oasys.eshakti.OasysUtils.Utils;
import com.oasys.eshakti.R;
import com.oasys.eshakti.activity.LoginActivity;
import com.oasys.eshakti.database.SHGTable;
import com.oasys.eshakti.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Vector;


public class CustomExpandableGroupListAdapter extends BaseExpandableListAdapter implements Filterable {

    public ArrayList<ListOfShg> shgList;
    Context context;
    List<GroupListItem> _listDataHeader;
    private ArrayList<HashMap<String, String>> listDataChild;
    private ExpandListItemClickListener mCallBack;
    private NetworkConnection networkConnection;
    boolean flag = false;
    Vector<String> mGroupId;
    List<GroupListItem> filterdata;
    Vector<String> mGroupValues;
    private boolean flag_verificaton = false;
    private  Date lastTransaDate,systemDate;



    public CustomExpandableGroupListAdapter(Context context, ArrayList<ListOfShg> shgDto, List<GroupListItem> listDataHeader,ArrayList<HashMap<String, String>> listChildData, ExpandListItemClickListener callback) {
        // TODO Auto-generated constructor stub
        networkConnection = NetworkConnection.getNetworkConnection(context);
        this.context = context;
        this._listDataHeader = listDataHeader;
        this.listDataChild = listChildData;
        filterdata = new ArrayList<>(_listDataHeader);
        this.shgList = shgDto;
        this.mCallBack = callback;
        lastTransaDate=null;

    }

    @Override
    public int getGroupCount() {
        // TODO Auto-generated method stub
        return this._listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        // TODO Auto-generated method stub
        return 1;
    }


    @Override
    public Object getGroup(int groupPosition) {
        // TODO Auto-generated method stub
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public HashMap<String, String> getChild(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return this.listDataChild.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        // TODO Auto-generated method stub
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        try {

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.view_new_row, null);
            }

            ImageView listLeftImage = (ImageView) convertView.findViewById(R.id.dynamicImage_left);
            TextView listTitle = (TextView) convertView.findViewById(R.id.dynamicText);
            ImageView listImage = (ImageView) convertView.findViewById(R.id.dynamicImage);

            listLeftImage.setVisibility(View.VISIBLE);

            GroupListItem group_listItem = (GroupListItem) getGroup(groupPosition);
            ListOfShg dto = this.shgList.get(groupPosition);

            listTitle.setText(RegionalConversion.getRegionalConversion(String.valueOf(group_listItem.getTitle())));
            listTitle.setTypeface(LoginActivity.sTypeface);
            listTitle.setSelected(true);

            if (networkConnection.isNetworkAvailable()) {

                if (dto.isVerified()) {
                    flag_verificaton = false;
                    listTitle.setTextColor(Color.WHITE);
                } else if (!dto.isVerified()) {
                    flag_verificaton = true;
                    listTitle.setTextColor(Color.YELLOW);
                }


                if (dto.getLastTransactionDate().length() > 0) {
                    listLeftImage.setImageResource(R.drawable.stars);
                    Calendar cal = Calendar.getInstance();
                    // DateFormat simple = new SimpleDateFormat("dd MMM yyyy HH:mm:ss:SSS Z");
                    Date result = new Date(Long.parseLong(dto.getLastTransactionDate()));
                    cal.setTime(result);
                    int month = cal.get(Calendar.MONTH);
                    long yr = cal.get(Calendar.YEAR);
                    //  String dateStr = simple.format(result);
                    Date d = new Date();
                    cal.setTime(d);
                    int month1 = cal.get(Calendar.MONTH);
                    int yr1 = cal.get(Calendar.YEAR);
                    if (month == month1 && yr == yr1) {
                        listLeftImage.setVisibility(View.INVISIBLE);
                    } else {
                        listLeftImage.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                if (dto.isVerified()) {
                    flag_verificaton = false;
                    listTitle.setTextColor(Color.WHITE);
                } else if (!dto.isVerified()) {
                    flag_verificaton = true;
                    listTitle.setTextColor(Color.YELLOW);
                }
                listLeftImage.setVisibility(View.INVISIBLE);
            }
            //  listLeftImage.setVisibility(View.INVISIBLE);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView,
                             final ViewGroup parent) {
        // TODO Auto-generated method stub
        HashMap<String, String> childMenu = getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.view_new_content, null);
        }

        RaisedButton button = (RaisedButton) convertView.findViewById(R.id.activity_next_button);
        TextView mShgName = (TextView) convertView.findViewById(R.id.shgcode);
        TextView mShgName_Values = (TextView) convertView.findViewById(R.id.shgcode_values);
        TextView mBlockname = (TextView) convertView.findViewById(R.id.blockname);
        TextView mBlockNameValues = (TextView) convertView.findViewById(R.id.blockname_values);
        TextView mPanchayatName = (TextView) convertView.findViewById(R.id.panchayat);
        TextView mPanchayatNameValues = (TextView) convertView.findViewById(R.id.panchayat_values);
        TextView mVillagename = (TextView) convertView.findViewById(R.id.villagename);
        TextView mVillagenameValues = (TextView) convertView.findViewById(R.id.villagename_vlaues);
        TextView mTransactionDate = (TextView) convertView.findViewById(R.id.transDate);
        TextView mTransactionDateValues = (TextView) convertView.findViewById(R.id.transDate_vlaues);

        /*mShgName.setTypeface(LoginActivity.sTypeface);
      //  mShgName_Values.setTypeface(LoginActivity.sTypeface);
        mBlockname.setTypeface(LoginActivity.sTypeface);
        mBlockNameValues.setTypeface(LoginActivity.sTypeface);
        button.setTypeface(LoginActivity.sTypeface);

        mPanchayatName.setTypeface(LoginActivity.sTypeface);
        mPanchayatNameValues.setTypeface(LoginActivity.sTypeface);
        mVillagename.setTypeface(LoginActivity.sTypeface);
        mVillagenameValues.setTypeface(LoginActivity.sTypeface);

        mTransactionDate.setTypeface(LoginActivity.sTypeface);
       // mTransactionDateValues.setTypeface(LoginActivity.sTypeface);*/

        mShgName.setTextColor(Color.BLACK);
        mShgName_Values.setTextColor(Color.BLACK);
        mBlockname.setTextColor(Color.BLACK);
        mBlockNameValues.setTextColor(Color.BLACK);

        mPanchayatName.setTextColor(Color.BLACK);
        mPanchayatNameValues.setTextColor(Color.BLACK);
        mVillagename.setTextColor(Color.BLACK);
        mVillagenameValues.setTextColor(Color.BLACK);

        mTransactionDate.setTextColor(Color.BLACK);
        mTransactionDateValues.setTextColor(Color.BLACK);

        button.setText(AppStrings.mSelect);

        mShgName.setText(childMenu.get("SHGCode_Label") + "  :  ");
        mShgName_Values.setText(childMenu.get("SHGCode"));


        mBlockname.setText(childMenu.get("BlockName_Label") + "  :  ");
        mBlockNameValues.setText(childMenu.get("BlockName"));

        if (childMenu.get("BlockName_Label").equals("Municipality Name")) {
            mVillagename.setVisibility(View.GONE);
            mVillagenameValues.setVisibility(View.GONE);
            mPanchayatName.setVisibility(View.VISIBLE);
            mPanchayatNameValues.setVisibility(View.VISIBLE);
            mPanchayatName.setText(childMenu.get("PanchayatName_Label") + "  :  ");
            mPanchayatNameValues.setText(childMenu.get("PanchayatName"));

        } else if (childMenu.get("BlockName_Label").equals("Panchayat Name")) {
            mVillagename.setVisibility(View.GONE);
            mVillagenameValues.setVisibility(View.GONE);
            mPanchayatName.setVisibility(View.GONE);
            mPanchayatNameValues.setVisibility(View.GONE);

        } else {
            mVillagename.setVisibility(View.VISIBLE);
            mVillagenameValues.setVisibility(View.VISIBLE);
            mPanchayatName.setVisibility(View.VISIBLE);
            mPanchayatNameValues.setVisibility(View.VISIBLE);
            mVillagename.setText(childMenu.get("VillageName_Label") + "  :  ");
            mVillagenameValues.setText(childMenu.get("VillageName"));
            mPanchayatName.setText(childMenu.get("PanchayatName_Label") + "  :  ");
            mPanchayatNameValues.setText(childMenu.get("PanchayatName"));
        }

        mTransactionDate.setText(childMenu.get("LastTransactionDate_Label") + "  :  ");

        DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
        if (!childMenu.get("LastTransactionDate").equals("NA")) {
            Date d = new Date(Long.parseLong(childMenu.get("LastTransactionDate")));
            String dateStr = simple.format(d);
            mTransactionDateValues.setText(dateStr);
        }



        button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                ListOfShg dto = shgList.get(groupPosition);

                if (dto.isVerified()) {
                    flag_verificaton = false;
                    Log.i("print","false");
                } else if (!dto.isVerified()) {
                    flag_verificaton = true;
                    Log.i("print","true");
                }



                DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
                DateFormat simple1 = new SimpleDateFormat("dd-MM-yyyy");
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

                if (dto.getLastTransactionDate() != null && dto.getLastTransactionDate().length() > 0) {
                    Date d = new Date(Long.parseLong(dto.getLastTransactionDate()));
                    String dateStr = simple.format(d);
                    try {
                          lastTransaDate = df.parse(dateStr);
                        Log.d("lasttransaction", "" + lastTransaDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    try {

                        Calendar calender = Calendar.getInstance();
                        String formattedDate = df.format(calender.getTime());
                        systemDate = df.parse(formattedDate);
                        Log.d("currentdate", "" + systemDate);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                    if (!flag_verificaton) {
                        if (systemDate.compareTo(lastTransaDate) >= 0) {

                            if (shgList.get(groupPosition).getIsTransAudit()!=null){
                                if(shgList.get(groupPosition).getIsTransAudit().equals("1.0")){
                                    mCallBack.onItemClickAudit(parent, v, groupPosition);
                                    Log.i("print","if");
                                }else {
                                    mCallBack.onItemClick(parent, v, groupPosition);
                                    Log.i("print","else");
                                }
                            }
                            /*if(shgList.get(groupPosition).getIsTransAudit().equals("1.0")){
                                mCallBack.onItemClickAudit(parent, v, groupPosition);
                                Log.i("print","if");
                            }else {
                                mCallBack.onItemClick(parent, v, groupPosition);
                                Log.i("print","else");
                            }*/
                        }
                        else
                        {
                            Toast.makeText(context,"Transaction can be done tomorrow",Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {

                        if (networkConnection.isNetworkAvailable())
                                mCallBack.onItemClickVerification(parent, v, groupPosition);
                            else
                                TastyToast.makeText(context, AppStrings.mVerifyGroupAlert,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                    }
                }


        });
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Filter getFilter() {
        return examplefilter;
    }


    /*public void filterData(String query) {
        query = query.toLowerCase();
        Log.v("MyListAdapter", String.valueOf(_listDataHeader.size()));
        _listDataHeader.clear();

        if(query.isEmpty()){
//            _listDataHeader.addAll(originalList);
        }
        else {

        }
    }*/

    private Filter examplefilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<GroupListItem> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(filterdata);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (GroupListItem item : filterdata) {
                    if (item.getTitle().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }


        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            _listDataHeader.clear();
            _listDataHeader.addAll((List) filterResults.values);
            notifyDataSetChanged();
        }
    };

}


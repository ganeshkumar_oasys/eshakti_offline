package com.oasys.eshakti.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.oasys.eshakti.Dto.SavingsAccounts;
import com.oasys.eshakti.R;

import java.util.ArrayList;

public class Grouploan_Adapter extends BaseAdapter {
    Context c;
    ArrayList<SavingsAccounts> objects;

    public Grouploan_Adapter(Context c, ArrayList<SavingsAccounts> objects) {
        this.c = c;
        this.objects = objects;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {


        SavingsAccounts cur_obj = objects.get(position);
        LayoutInflater inflater = ((Activity) c).getLayoutInflater();
        View row = inflater.inflate(R.layout.grouploan_row, viewGroup, false);
        TextView label = (TextView) row.findViewById(R.id.row_spn_grouploan);
        label.setText(cur_obj.getBankName() );
        cur_obj.getId();
        return row;
    }
}

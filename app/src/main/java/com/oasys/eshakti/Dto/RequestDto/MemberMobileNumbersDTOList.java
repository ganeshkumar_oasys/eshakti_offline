package com.oasys.eshakti.Dto.RequestDto;

import java.io.Serializable;

import lombok.Data;

@Data
public class MemberMobileNumbersDTOList implements Serializable
{
     String name;

     String userId;

     String mobileNumber;
}

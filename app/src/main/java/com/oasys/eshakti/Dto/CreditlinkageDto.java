package com.oasys.eshakti.Dto;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 12/6/2018.
 */
@Data
public class CreditlinkageDto implements Serializable {
    private String shgId;
    private String creditLinkageCount;
}

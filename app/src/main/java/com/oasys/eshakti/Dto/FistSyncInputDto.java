package com.oasys.eshakti.Dto;

import com.oasys.eshakti.enums.TableNames;

import lombok.Data;

@Data
public class FistSyncInputDto {
    String tableName;
    int count;
    String textToDisplay;
    String endTextToDisplay;
    boolean dynamic;
    TableNames tableNames;
}

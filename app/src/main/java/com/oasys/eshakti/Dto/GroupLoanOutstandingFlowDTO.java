package com.oasys.eshakti.Dto;

import lombok.Data;

@Data
public  class GroupLoanOutstandingFlowDTO {


    private LoanType loanType;

    private String loanDisbursementAmount;

    private String isDigital;

    private ShgId shgId;

    private String existingLoanOutstanding;

    private String transactionDate;

    private String type;

    private double loanOutstandingAmount;

    private long disburseMentdate;

    private String loanRepayAmount;

    private String id;

    private LoanId loanId;
}

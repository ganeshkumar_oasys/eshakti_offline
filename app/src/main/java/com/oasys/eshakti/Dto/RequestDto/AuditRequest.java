package com.oasys.eshakti.Dto.RequestDto;

import lombok.Data;

@Data
public class AuditRequest {

    private String auditorId;

    private String shgId;

    private String transDate;

    private String validation;

    private String remarks;
}

package com.oasys.eshakti.Dto;

import lombok.Data;

@Data
public class ExistingLoan {

    private String loanId,loanAccountId,loanType,loanAccountNo,memberId,memberLoanOutstanding;
    private String shgId,bankId,branchId,bankName;
    private String loanTypeName;
    private String accountNumber;
    private String disbursmentDate;
    private String loanOutstanding;


    private String id;
    private String name;

}

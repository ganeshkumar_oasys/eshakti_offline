package com.oasys.eshakti.Dto.RequestDto;

import java.io.Serializable;

import lombok.Data;

@Data
public class ShgSavingsAccountDTOList implements Serializable {

    private String bankId;

    private String shgId;

    private String bankNameId;

    private String branchName;

    private String bankName;

    private String accountNumber;

    private String branchNameId;

    private String memberId;
}

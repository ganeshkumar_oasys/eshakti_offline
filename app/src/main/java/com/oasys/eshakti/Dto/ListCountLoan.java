package com.oasys.eshakti.Dto;

import lombok.Data;

@Data
public class ListCountLoan {

    private int CIF = 0;
    private int BulkLoan = 0;
    private int MFILoan = 0;
    private int TermLoan = 0;
    private int CashCredit = 0;
    private int RFA = 0;
}

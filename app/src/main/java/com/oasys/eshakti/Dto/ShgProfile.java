package com.oasys.eshakti.Dto;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 12/7/2018.
 */
@Data
public class ShgProfile implements Serializable {

    private String shgName;

    private String shgType;

    private String panchayatName;

    private String shgCode;

    private String presidentName;

    private String blockName;

    private String secretaryName;

    private String shgCreatedDate;

    private String totalMembers;

    private String villageName;

    private String treasuereName;

    private String mobileNumber;

}

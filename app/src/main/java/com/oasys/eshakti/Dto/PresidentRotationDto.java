package com.oasys.eshakti.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class PresidentRotationDto implements Serializable {
    String presidentRotationDate;
    String presidentName;
}

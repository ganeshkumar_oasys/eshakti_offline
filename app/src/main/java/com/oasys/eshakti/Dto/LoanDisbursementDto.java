package com.oasys.eshakti.Dto;


import lombok.Data;

@Data
public class LoanDisbursementDto {

    private String loanTypeId,id,name;
    private String loanTypeName;

    private String installmentTypeId;
    private String installmentTypeName;

    private String loanSettingId;
    private String loanSettingName;
    private int sflag ;


}

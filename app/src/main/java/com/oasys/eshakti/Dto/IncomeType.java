package com.oasys.eshakti.Dto;

import lombok.Data;

@Data
public class IncomeType {

    String id;
    String incomeType;
    String name;
}

package com.oasys.eshakti.Dto;

import lombok.Data;

@Data
public class MemberLoanList {

    private String memberLoanOutstanding;

    private String shgId;

    private String loanId;

    private String memberId;

}

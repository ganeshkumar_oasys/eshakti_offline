package com.oasys.eshakti.Dto;


import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;



@Data
public class ResponseContent implements Serializable {

    private String meetingId;

    private String name,accessToken,expiresInMilliseconds,refreshToken,ImageByteCode;

    private String userId;

    private ArrayList<ListOfShg> listOfShg;
    private ArrayList<TableData> tableData;

    private ArrayList<CashOfGroup> cashOfGroup;
    private CashOfGroup savingsBalance;

    private ArrayList<MemberList> members;

    private ArrayList<ShgInternalLoanListDTO> shgInternalLoanListDTO;
    private ListCountLoan listCountLoan;

    private ArrayList<GroupfinancialDetails> groupfinancialDetails;

    private ShggroupDetailsDTO shggroupDetailsDTO;

    private ArrayList<ShgBankList> shgBankList;

    private ShgBalanceDetailsDTO shgBalanceDetailsDTO;

    private ArrayList<MemberfinancialDetails> memberfinancialDetails;


    private String animatorId;
    private String animatorName, fixedDepositBalance;


    private String disbursementAmount, outStanding, memberDisbursementAmount;
    private String balanceAmount, balance;
    private String sansactionAmount, sanctionAmount, bankCharges;

    private AnimatorProfile animatorProfile;

    private ShgProfile shgProfile;

    private ArrayList<GroupSavingsSummary> groupSavingsSummary;
    private ArrayList<ExistingLoan> settingsList;

    // private String totalSavings;
    private ArrayList<ExpensesTypeDtoList> expensesTypeDTOList;
    private ArrayList<LoanDisbursementDto> loanSettingList,financeBanksList;
    private ArrayList<LoanDisbursementDto> InstallmentTypeList;

    private ArrayList<LoanBankDto> bankNamesList, branchNameList;

    /*  private String totalSavings;*/

    private String totalVoluntarySaving;

    private String[] yearList;

    private ArrayList<MonthlyReport> monthlyReport;

    private ArrayList<ExistingLoan> loansList;

    private ArrayList<LoanDto> purposeOfloanTypeList, newLoanTypes;

    private ArrayList<LoanDto> purposeOfInternalLoanTypes;

//    private CheckListDto CheckList;
       private ArrayList<CheckListDto> CheckListDto;


    private ArrayList<LoanDto> loanTypes;

    private ArrayList<TotalSavings> totalSavings;

    private String[] loansummary;

    private ArrayList<MemberList> memberloanRepaymentList;

    private ArrayList<ShgBankDetails> shgBankDetails;

    private ArrayList<IncomeType> incomeType;

    private ArrayList<BankBalanceDTOList> bankBalanceDTOList;

//    private ArrayList<GroupBankTransactionSummaryList> groupBankTransactionSummaryList;
    private ArrayList<GroupBankTransactionSummaryList> GroupBankTransactionSummaryList;


    private ArrayList<ShgBankDTOList> shgBankDTOList;

    // private ArrayList<LoansList> loansList;

    //private ArrayList<MemberloanRepaymentList> memberloanRepaymentList;
    private ArrayList<SavingsAccounts> savingsAccounts;

    private ArrayList<MinutesOfMeeting> minutesOfMeeting;
    private ArrayList<PurposeOfLoans> purposeOfLoans;


    private ArrayList<MemberInternalLoan> memberInternalLoan;


    private ArrayList<GroupInternalLoansList> groupInternalLoansList;


    private String loanOutstanding;

//    private ArrayList<TotalSavingsAmount> totalSavingsAmount;
//    private ArrayList<InternalLoan> memberinternallaon;
//    private ArrayList<MonthlyReport> membermonthlyReport;
//    private ArrayList<LoanSummary> memberreportloansummary;


    private String otherloanOutstanding;


    private ArrayList<MemberLoanSummary> memberLoanSummary;


    private String totalGrouploan,TotalAmount,Total,totalMemberSavings,totalMemberSavingsSummary,totalMemberSavingsDisbursment;


    private ArrayList<MemberSavingsSummary> memberSavingsSummary,memberSavingsDisbursment;


    private String grouptotalSavings;


    private GroupLoanSummary groupLoanSummary;
//    @SerializedName("TrainingsList")
    private ArrayList<TrainingsList> TrainingsList;
//    @SerializedName("languageList")
    private ArrayList<TrainingsList>  languageList;

    private String auditDate;

//  OFFLINE LOAN CREATION

    private String cashInHand;

    private String cashAtBank;

    private String lastTransactionDate;

    private GroupLoanOutstandingFlowDTO groupLoanOutstandingFlowDTO;



//    private MemberLoanOutstandingFlowDTO[] memberLoanOutstandingFlowDTO;


    private String totalSavingsAmount;

    private ArrayList<MembermonthlyReport> membermonthlyReport;

    private ArrayList<Memberinternallaon> memberinternallaon;

    private ArrayList<Memberreportloansummary> memberreportloansummary;

    private ArrayList<InternalLoanTotalOutstanding> internalLoanTotalOutstanding =null;

    private ArrayList<InternalLoanRepayHistory> internalLoanRepayHistory;

    private ArrayList<GroupBankTransactionSummaryDTOList> groupBankTransactionSummaryDTOList;




    private ArrayList<MemberLoanList> memberLoanList;

    private ArrayList<BankTransactionList> bankTransactionList;

    private ArrayList<GroupLoanList> groupLoanList;

    private ArrayList<DisbursementList> disbursementList;

    private ArrayList<BankDetails> bankDetails;

//    private ArrayList<MemberMobileNumbersDTOList> memberMobileNumbersDTOList;


    private RdFlagDetails rdFlagDetails;


//    RECURRING DEPOSIT

    private String createdDate;

    private String createdBy;

    private String interest;

    private String installmentAmount;

    private String currentBalance;

    private String modifiedDate;

    private ShgId shgId;

    private SavingsbankId savingsbankId;

    private String modifiedBy;

    private String id;

    private String tenure;

    private String status;

    private ArrayList<AuditorDetails> auditorDetails;



}

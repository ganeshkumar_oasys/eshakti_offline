package com.oasys.eshakti.Dto;

import java.io.Serializable;

import lombok.Data;


@Data
public class ListOfShg implements Serializable {
    private String is_transaction_tdy;
    private String panchayatName;
    private String name;
    private String id;
    private String groupFormationDate,openingDate;
    boolean isVerified;
    private String cashInHand;
    private String cashAtBank;
    private String blockName;
    private String code;
    private String villageName;
    private String lastTransactionDate,presidentName,modifiedDate,fFlag;
    private String districtId;
    private String isTransAudit;


    //Get ShgBank value
    private String shgId;
    private String bankId;
    private String bankName;
    private String branchName;
    private String accountNumber;
    private String shgSavingsAccountId;
    private String bankNameId;
    private String branchNameId;



}

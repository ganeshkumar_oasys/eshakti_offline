package com.oasys.eshakti.Dto.RequestDto;

import java.io.Serializable;

import lombok.Data;

@Data
public class SsnNumbersDTOList implements Serializable
{

    private String branchId;

    private String isSss;

    private String sssDte;

    private String lastUpdate;

    private String shgId;

    private String bankNameId;

    private String userId;

    private String sss_number;

    private String isBefore;

    private String firstUpdate;

    private String memberId;

    private String sssTypeUuid;

}

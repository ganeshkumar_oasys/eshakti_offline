package com.oasys.eshakti.Dto;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 12/17/2018.
 */
@Data
public class LoansList implements Serializable {

    private String accountNumber;

    private String bankName;

    private String loanTypeName;

    private String loanId;

    private String disbursmentDate;
}

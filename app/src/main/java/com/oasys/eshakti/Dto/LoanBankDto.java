package com.oasys.eshakti.Dto;


import java.util.ArrayList;
import java.util.IdentityHashMap;

import lombok.Data;

@Data
public class LoanBankDto {

    private ArrayList<BranchList> branchList;
    private String id;
    private String name;
    private String none;
    private String ifscCode;

}

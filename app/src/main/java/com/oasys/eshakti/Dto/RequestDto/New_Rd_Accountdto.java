package com.oasys.eshakti.Dto.RequestDto;

import lombok.Data;

@Data
public class New_Rd_Accountdto {

    private String interestRate;

    private String installmentAmount;

//    private String accountNo;

    private String loanAccountNumber;

    private String shgId;

    private String branchName;

    private String bankName;

    private String ifsc;

    private String transactionDate;

    private String tenure;
}

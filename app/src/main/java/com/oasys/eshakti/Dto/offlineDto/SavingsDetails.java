package com.oasys.eshakti.Dto.offlineDto;


import com.oasys.eshakti.Dto.AddBTDto;
import com.oasys.eshakti.Dto.OfflineDto;
import com.oasys.eshakti.Dto.RequestDto.AttendanceRequestDto;
import com.oasys.eshakti.Dto.RequestDto.GroupLoanRepaymentDto;
import com.oasys.eshakti.Dto.RequestDto.ImageDto;
import com.oasys.eshakti.Dto.RequestDto.MeetingAuditingDetailsDto;
import com.oasys.eshakti.Dto.RequestDto.MemberloanRepayRequestDto;
import com.oasys.eshakti.Dto.RequestDto.MinutesofmeetingsRequestDto;
import com.oasys.eshakti.Dto.RequestDto.RepaymentDetails;
import com.oasys.eshakti.Dto.RequestDto.TrainingRequestDto;
import com.oasys.eshakti.Dto.SavingRequest;

import java.util.ArrayList;
import java.util.HashMap;

import lombok.Data;

@Data
public class SavingsDetails {

    ArrayList<SavingRequest> savingsDetails;
    ArrayList<SavingRequest> incomeDetails;
    ArrayList<SavingRequest> expenseDetails;
    ArrayList<SavingRequest> internalLoanDetails;
    ArrayList<AddBTDto> bankTransactionDetails;
    ArrayList<AddBTDto> accountToAccountDetails;
    ArrayList<AddBTDto> savingsAccountToLoanAccountDetails;
    ArrayList<AddBTDto> fixedDepositDetails;
    ArrayList<AddBTDto> recurringDepositDetails;
    ArrayList<MemberloanRepayRequestDto> memberLoanRepaymentDetails, internalLoanRepaymentDetails;
    ArrayList<GroupLoanRepaymentDto> groupLoanRepaymentDetails;
    ArrayList<AttendanceRequestDto> shgMeetingAttendanceDetails;
    ArrayList<MinutesofmeetingsRequestDto> minutesOfMeetingDetails;
    ArrayList<ImageDto> imageUpload;
    ArrayList<MeetingAuditingDetailsDto> meetingAuditingDetailsDto;
    ArrayList<TrainingRequestDto> trainingRequestDto;
    private String userId;
}

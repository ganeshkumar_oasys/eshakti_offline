package com.oasys.eshakti.Dto;


import lombok.Data;

@Data
public class AddBTDto {

    public String cashDeposit;
    public String bankInterest;
    public String interestSubventionReceived;
    public String withdrawal;
    public String bankCharges;
    public String transactionDate, toBankId;
    public String mobileDate;
    public String shgId;
    public String bankId;
    public String savingsBankId;
    public int transactionFlowType;
    public String savingsBankDetailsId;
    public String loanBankDetailsId;
    public String fromSavingsBankDetailsId;
    public String toSavingsBankDetailsId;
    public String transferCharges;
    public String transferAmount;
    public String fromBankId;
}

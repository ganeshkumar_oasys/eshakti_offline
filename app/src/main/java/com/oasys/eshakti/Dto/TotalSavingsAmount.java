package com.oasys.eshakti.Dto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

@Data
public class TotalSavingsAmount implements Serializable {

    private String savings;
    private ArrayList<TotalSavingAmount> totalSavingsAmount;
    public String total_Savings;


}

package com.oasys.eshakti.Dto.RequestDto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

@Data
public class ImageDto implements Serializable {

    private ArrayList<ShgAccountNumbersUpdateDTOList> shgAccountNumbersUpdateDTOList;

    private String url;
    private String id;

}

package com.oasys.eshakti.Dto;

import lombok.Data;

@Data
public class BankDetails {

    private String currentBalance;

    private String shgId;

    private String recurringDepositedBalance;

    private String currentFixedDeposit;

    private String shgSavingsAccountId;


}

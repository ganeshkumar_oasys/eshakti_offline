package com.oasys.eshakti.Dto;

import lombok.Data;

@Data
public class GroupLoanRepayment {

    private String transaction_date;

    private String auditor_uuid;

    private String settings;

    private String old_transaction_amount;

    private String member;

    private String new_transaction_amount;

    private String transaction_uuid;

    private String shg_uuid;

    private String member_uuid;
}

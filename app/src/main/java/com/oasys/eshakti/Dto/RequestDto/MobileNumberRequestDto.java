package com.oasys.eshakti.Dto.RequestDto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

@Data
public class MobileNumberRequestDto implements Serializable
{
     ArrayList<MemberMobileNumbersDTOList> memberMobileNumbersDTOList;

}

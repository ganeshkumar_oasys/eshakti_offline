package com.oasys.eshakti.Dto.RequestDto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 1/4/2019.
 */
@Data
public class TrainingRequestDto implements Serializable {
    private String shgId;
    private String trainingDate;
    private ArrayList<TrainingListId> trainingListId;
//    private String id;
}

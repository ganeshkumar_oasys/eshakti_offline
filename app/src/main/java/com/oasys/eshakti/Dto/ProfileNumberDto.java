package com.oasys.eshakti.Dto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

@Data
public class ProfileNumberDto implements Serializable
{
     ArrayList<ProfileResponseContentsDto> responseContents;

    String message;

     String statusCode;
}

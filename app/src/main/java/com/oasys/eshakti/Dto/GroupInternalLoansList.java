package com.oasys.eshakti.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public  class GroupInternalLoansList implements Serializable {

    private String amount;

    private String groupMemberName;

    private String loanAmountRepaid;
}

package com.oasys.eshakti.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class AnimatorProfile implements Serializable {

    private String totalGroup;

    private String dateOfAssigning;

    private String phoneNumber;

    private String age;

    private String AnimatorName;

}

package com.oasys.eshakti.Dto;

import lombok.Data;

@Data
public class SavingsbankId {

    private String currentRecurringDepositedBalance;

    private String currentBalance;

    private ShgId shgId;

    private String isRd;

    private BankId bankId;

    private String createdDate;

    private String createdBy;

    private String isPrimary;

    private String currentFixedDepositedBalance;

    private String modifiedDate;

    private String modifiedBy;

    private String id;

    private String openingBalance;

    private String openingDate;
}

package com.oasys.eshakti.Dto;


import java.io.Serializable;

import lombok.Data;

@Data
public class ShgBalanceDetailsDTO implements Serializable {

    private String shgCashInHand;

    private String shgSeedFund;

    private AssetsAndLiabilitiesDTO assetsAndLiabilitiesDTO;

    private String shgCashId;

}

package com.oasys.eshakti.Dto;

import lombok.Data;

@Data
public class GroupBankTransactionSummaryDTOList {

    private String amount;

    private String transactiondate;

    private String details;
}

package com.oasys.eshakti.Dto;


import lombok.Data;

@Data
class SavingAccountId {

    String id;
}

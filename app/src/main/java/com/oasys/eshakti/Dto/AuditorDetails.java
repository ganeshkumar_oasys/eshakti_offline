package com.oasys.eshakti.Dto;

import java.util.ArrayList;

import lombok.Data;

@Data
public class AuditorDetails {

    private String shgUserId;

    private String auditorUserId;


    private ArrayList<InternalLoanDisbursement> InternalLoanDisbursement;

    private ArrayList<SeedFund> SeedFund;

    private ArrayList<FDBankTransaction> FDBankTransaction ;


    private ArrayList<IncomeDisbursement> IncomeDisbursement;

    private ArrayList<OtherIncome> OtherIncome;

    private ArrayList<GroupLoanRepayment> GroupLoanRepayment ;

    private ArrayList<Penalty> Penalty;

    private ArrayList<BankTransaction> BankTransaction ;

    private ArrayList<SubscriptiontoFeds> SubscriptiontoFeds;

    private  ArrayList<OtherIncomeGroup> OtherIncomeGroup;

    private ArrayList<memberLoanRepayment> memberLoanRepayment;


    private ArrayList<VoluntarySavingsDisbursement> VoluntarySavingsDisbursement;

    private ArrayList <OtherExpense> OtherExpense;

    private ArrayList<MeetingExpense>  MeetingExpense;

    private ArrayList<internalLoanRepayment> internalLoanRepayment;

    private ArrayList<Donation> Donation;

    private ArrayList<VoluntarySavings> VoluntarySavings;

    private ArrayList<SavingsDisbursement> SavingsDisbursement;

    private ArrayList<Savings> savings;

    private ArrayList<Subscription> Subscription;

    private ArrayList<SubscriptionCharges> SubscriptionCharges;

}

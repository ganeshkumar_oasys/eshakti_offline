package com.oasys.eshakti.Dto;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 12/10/2018.
 */
@Data
public class GroupBankTransactionSummaryList implements Serializable {
    private String amount;

    private String transactiondate;

    private String details;
}

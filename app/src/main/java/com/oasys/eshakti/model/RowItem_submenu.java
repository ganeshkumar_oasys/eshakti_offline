package com.oasys.eshakti.model;

public class RowItem_submenu {
	private String submenu_name;
	private int submenu_image_id;

	public RowItem_submenu(String submenu_name, int submenu_image_id) {

		this.submenu_name = submenu_name;
		this.submenu_image_id = submenu_image_id;
	}

	public String getSubmenu_name() {
		return submenu_name;
	}

	public void setSubmenu_name(String submenu_name) {
		this.submenu_name = submenu_name;
	}

	public int getSubmenu_img_id() {
		return submenu_image_id;
	}

	public void setSubmenu_img_id(int submenu_image_id) {
		this.submenu_image_id = submenu_image_id;
	}

}

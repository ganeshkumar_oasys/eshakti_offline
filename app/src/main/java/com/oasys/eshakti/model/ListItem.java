package com.oasys.eshakti.model;

import lombok.Data;

@Data
public class ListItem {

	private String id;
	private String title;
	private String walletValue;
	private int imageId;
	private boolean isSelected;
	private String accountno;

}

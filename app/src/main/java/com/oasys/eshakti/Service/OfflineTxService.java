package com.oasys.eshakti.Service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.oasys.eshakti.process.TransactionProcess;
import com.oasys.eshakti.process.TransactionProcess_Test;

public class OfflineTxService extends IntentService {

    private static final int JOB_ID = 1001;

    public OfflineTxService() {
        super("OfflineTxService");
    }
   /* public static void enqueueWork(Context ctx, Intent intent) {
        enqueueWork(ctx, OfflineTxService.class, JOB_ID, intent);
    }
    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        Log.e(this.getClass().getName(), "TransactionService started...");
        TransactionProcess billProcess = new TransactionProcess();
        BaseSchedulerService bsAllocation = (BaseSchedulerService) billProcess;
        bsAllocation.process(OfflineTxService.this);
    }*/

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.e(this.getClass().getName(), "TransactionService started...");
        //TransactionProcess billProcess = new TransactionProcess();
        TransactionProcess_Test billProcess = new TransactionProcess_Test();
        BaseSchedulerService bsAllocation = (BaseSchedulerService) billProcess;
        bsAllocation.process(OfflineTxService.this);
    }
}

package com.oasys.eshakti.Service;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.oasys.eshakti.Dto.ResponseDto;
import com.oasys.eshakti.EShaktiApplication;
import com.oasys.eshakti.OasysUtils.Constants;
import com.oasys.eshakti.OasysUtils.MySharedPreference;
import com.oasys.eshakti.OasysUtils.ServiceType;
import com.oasys.eshakti.R;
import com.oasys.eshakti.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.activity.LoginActivity;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class RestClient {
//    private static final int MY_SOCKET_TIMEOUT_MS = 20000;
private static final int MY_SOCKET_TIMEOUT_MS = 180000;
    public static int mStatusCode;

    private static NewTaskListener onCallSuccessCallback;
    private String TAG = "RestClient";
    private Dialog dialog;

    public static RestClient restClient;

    public static RestClient getRestClient(NewTaskListener NewTaskListener) {
        if (restClient == null) {
            restClient = new RestClient();
        }
        onCallSuccessCallback = NewTaskListener;
        return restClient;
    }

    public void callRestWebService(String _url, String reqJson, final Context context, final ServiceType serviceType) {
        JsonObjectRequest jsonObjReq = null;
        try {
            final String url = _url;
            Log.e("request", reqJson);
            Log.i("print","URL : "+url);
            Log.i("print","Request Body : "+reqJson);
            if (!url.contains(Constants.BASE_URL + Constants.OFFLINE_API))
                showProgressDialog(context);
            jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(reqJson),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("URL", url);
                            Log.e(TAG, "volley response......" + response.toString());
                            Log.i("print","Response : "+response.toString());
                            onCallSuccessCallback.onTaskFinished(response.toString(), serviceType);
                            MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, false);
                            dismisProgressDialog();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.statusCode == HttpStatus.SC_UNAUTHORIZED) {
                        // HTTP Status Code: 401 Unauthorized
                        MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, true);
                        final ResponseDto lrDto = new ResponseDto();
                        lrDto.setStatusCode(401);
                        lrDto.setMessage("Unauthorized! Invalid credentials.");
                        String e_response = new Gson().toJson(lrDto);
                        Log.e(TAG, "volley response......" + e_response);
                        onCallSuccessCallback.onTaskFinished(e_response, serviceType);
                    } else {
                        MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, false);
                        if (error instanceof NoConnectionError) {
                            Log.e(TAG, "volley error response......NoConnectionError" + error.getMessage());
                            onCallSuccessCallback.onTaskFinished(null, serviceType);
                        } else if (error instanceof ServerError) {
                            Log.e(TAG, "volley error response......ServerError" + error.getMessage());
                            onCallSuccessCallback.onTaskFinished(null, serviceType);
                        } else if (error instanceof TimeoutError) {
                            Log.e(TAG, "volley error response......TimeoutError" + error.getMessage());
                            onCallSuccessCallback.onTaskFinished(null, serviceType);
                        } else {
                            Log.e(TAG, "volley error response......" + error.getMessage());
                        }
                    }
                    dismisProgressDialog();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    if (MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, "") != null && MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, "").length() > 0)
                        headers.put("Authorization", "Bearer " + MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, ""));
                    return headers;
                }
            };
        } catch (Exception e) {
        }
        RetryPolicy retryPolicy = new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(retryPolicy);
        EShaktiApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    public void callRestWebServiceForPutMethod(String _url, String reqJson, final Context context, final ServiceType serviceType) {
        JsonObjectRequest jsonObjReq = null;
        try {
            final String url = _url;
            Log.e("request", reqJson);
            showProgressDialog(context);
            jsonObjReq = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(reqJson),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("URL", url);
                            Log.e(TAG, "volley response......" + response.toString());
                            onCallSuccessCallback.onTaskFinished(response.toString(), serviceType);
                            MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, false);
                            dismisProgressDialog();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.statusCode == HttpStatus.SC_UNAUTHORIZED) {
                        // HTTP Status Code: 401 Unauthorized
                        MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, true);
                        final ResponseDto lrDto = new ResponseDto();
                        lrDto.setStatusCode(401);
                        lrDto.setMessage("Unauthorized! Invalid credentials.");
                        String e_response = new Gson().toJson(lrDto);
                        Log.e(TAG, "volley response......" + e_response);
                        onCallSuccessCallback.onTaskFinished(e_response, serviceType);
                    } else {
                        MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, false);
                        if (error instanceof NoConnectionError) {
                            Log.e(TAG, "volley error response......" + error.getMessage());
                            onCallSuccessCallback.onTaskFinished(null, serviceType);
                        } else if (error instanceof ServerError) {
                            Log.e(TAG, "volley error response......" + error.getMessage());
                            onCallSuccessCallback.onTaskFinished(null, serviceType);
                        } else if (error instanceof TimeoutError) {
                            Log.e(TAG, "volley error response......" + error.getMessage());
                            onCallSuccessCallback.onTaskFinished(null, serviceType);
                        } else {
                            Log.e(TAG, "volley error response......" + error.getMessage());
                        }
                    }
                    dismisProgressDialog();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    if (MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, "") != null && MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, "").length() > 0)
                        headers.put("Authorization", "Bearer " + MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, ""));
                    return headers;
                }
            };
        } catch (Exception e) {
        }
        RetryPolicy retryPolicy = new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(retryPolicy);
        EShaktiApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    public void callWebServiceForGetMethod(final String url, final Context context, final ServiceType serviceType) {
        String tag_string_req = "string_req";
        showProgressDialog(context);


        StringRequest strReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("URL", url);
                Log.e(TAG, "volley response......" + response);
                onCallSuccessCallback.onTaskFinished(response, serviceType);
                MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, false);
                dismisProgressDialog();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.statusCode == HttpStatus.SC_UNAUTHORIZED) {
                    // HTTP Status Code: 401 Unauthorized
                    MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, true);
                    final ResponseDto lrDto = new ResponseDto();
                    lrDto.setStatusCode(401);
                    lrDto.setMessage("Unauthorized! Invalid credentials.");
                    String e_response = new Gson().toJson(lrDto);
                    Log.e(TAG, "volley response......" + e_response);
                    onCallSuccessCallback.onTaskFinished(e_response, serviceType);
                } else {
                    MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, false);
                    if (error instanceof NoConnectionError) {
                        Log.e(TAG, "volley error response......" + error.getMessage());
                        onCallSuccessCallback.onTaskFinished(null, serviceType);
                    } else if (error instanceof ServerError) {
                        Log.e(TAG, "volley error response......" + error.getMessage());
                        onCallSuccessCallback.onTaskFinished(null, serviceType);
                    } else if (error instanceof TimeoutError) {
                        Log.e(TAG, "volley error response......" + error.getMessage());
                        onCallSuccessCallback.onTaskFinished(null, serviceType);
                    } else {
                        Log.e(TAG, "volley error response......" + error.getMessage());
                    }
                }
                dismisProgressDialog();

            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                if (MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, "") != null && MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, "").length() > 0)
                    headers.put("Authorization", "Bearer " + MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, ""));
                return headers;
            }
        };


        RetryPolicy retryPolicy = new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(retryPolicy);
        EShaktiApplication.getInstance().addToRequestQueue(strReq);
    }

    public void callRestWebServiceForDelete(final String url, final Context context, final ServiceType serviceType, final String sessionId) {
        String tag_string_req = "string_req";
        final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("URL", url);
                Log.e(TAG, "volley response......" + response);
                onCallSuccessCallback.onTaskFinished(response, serviceType);
                pDialog.dismiss();

            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(context,
                            context.getString(R.string.error_network_timeout),
                            Toast.LENGTH_LONG).show();
                }
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.dismiss();
            }

        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                if (MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, "") != null && MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, "").length() > 0)
                    headers.put("Authorization", "Bearer " + MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, ""));
                return headers;
            }
        };

        EShaktiApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


//    FOR ONE REASON
public void callWebServiceForGetMethod1(final String url, final Context context, final ServiceType serviceType) {
    String tag_string_req = "string_req";
    showProgressDialog(context);


    StringRequest strReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            Log.e("URL", url);
            Log.e(TAG, "volley response......" + response);
            onCallSuccessCallback.onTaskFinished(response, serviceType);
            MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, false);
            dismisProgressDialog();

        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {

            NetworkResponse networkResponse = error.networkResponse;
            if (networkResponse != null && networkResponse.statusCode == HttpStatus.SC_UNAUTHORIZED) {
                // HTTP Status Code: 401 Unauthorized
                MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, true);
                final ResponseDto lrDto = new ResponseDto();
                lrDto.setStatusCode(401);
                lrDto.setMessage("Unauthorized! Invalid credentials.");
                String e_response = new Gson().toJson(lrDto);
                Log.e(TAG, "volley response......" + e_response);
                onCallSuccessCallback.onTaskFinished(e_response, serviceType);
            } else {
                MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, false);
                if (error instanceof NoConnectionError) {
                    Log.e(TAG, "volley error response......" + error.getMessage());
                    onCallSuccessCallback.onTaskFinished(null, serviceType);
                } else if (error instanceof ServerError) {
                    Log.e(TAG, "volley error response......" + error.getMessage());
                    onCallSuccessCallback.onTaskFinished(null, serviceType);
                } else if (error instanceof TimeoutError) {
                    Log.e(TAG, "volley error response......" + error.getMessage());
                    onCallSuccessCallback.onTaskFinished(null, serviceType);
                } else {
                    Log.e(TAG, "volley error response......" + error.getMessage());
                }
            }
            dismisProgressDialog();

        }

    }) {

        @Override
        protected Response<String> parseNetworkResponse(NetworkResponse response) {
            mStatusCode = response.statusCode;
            return super.parseNetworkResponse(response);
        }
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String> headers = new HashMap<String, String>();
            headers.put("Content-Type", "application/json; charset=utf-8");
            if (MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, "") != null && MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, "").length() > 0)
                headers.put("Authorization", "Bearer " + MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, ""));
            return headers;
        }
    };


    RetryPolicy retryPolicy = new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    strReq.setRetryPolicy(retryPolicy);
    EShaktiApplication.getInstance().addToRequestQueue(strReq);
}


    public void showProgressDialog(Context context) {
        if (dialog == null || (!dialog.isShowing())) {
            dialog = AppDialogUtils.createProgressDialog(context);
            dialog.show();

        }
    }

    public void dismisProgressDialog() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
                dialog = null;
            }
        }
    }
}

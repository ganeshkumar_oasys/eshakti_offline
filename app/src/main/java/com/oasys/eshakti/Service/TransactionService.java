package com.oasys.eshakti.Service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import androidx.annotation.Nullable;

import android.util.Log;
import android.widget.Toast;

import com.oasys.eshakti.OasysUtils.NetworkConnection;


import java.util.ArrayList;

/**
 * Created by user1 on 9/12/16.
 */
public class TransactionService extends Service {

    private boolean isRunning;
    private Context context;
    private Thread backgroundThread;
    private NetworkConnection network;
    private static int count = 0;

   /* @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e(this.getClass().getName(), "Kill Service called");
        super.onTaskRemoved(rootIntent);
    }*/

   /*  @Override
   public boolean onStartJob(JobParameters jobParameters) {


      mJobHandler.sendMessage( Message.obtain( mJobHandler, 1, jobParameters ) );

     // reschedule the job
      return true;
   }

   private Handler mJobHandler = new Handler( new Handler.Callback() {

      @Override
      public boolean handleMessage( Message msg ) {
       *//*  Toast.makeText( getApplicationContext(),
                 "JobService task running", Toast.LENGTH_SHORT )
                 .show();*//*
         Log.e("TransactionService", msg+"jobService task running");
         ArrayList<Object> obj = new ArrayList<>();
         obj.add(TransactionService.class);
    *//*  obj.add(AllocationService.class);
      obj.add(RegularSyncService.class);
      obj.add(InwardService.class);
      obj.add(AdjustmentService.class);
      obj.add(BillService.class);
      obj.add(AdvanceStockService.class);
      obj.add(CloseSaleService.class);
      obj.add(LoginService.class);
      obj.add(RemoteLogService.class);
      obj.add(SyncExceptionService.class);
      obj.add(MigrationService.class);
      obj.add(BiometricService.class);
      obj.add(InspectionReportService.class);
      obj.add(InspectionReportAckService.class);
      obj.add(InspectionCriteriaService.class);
      obj.add(Smart_Card_status_service.class);*//*

         network = NetworkConnection.getNetworkConnection(getApplicationContext());

         if (network.isNetworkAvailable() ) {
            Log.e(this.getClass().getName(),"Sess.Service started");
            start_All_services(obj);
         }


         jobFinished( (JobParameters) msg.obj, false );
         return true;
      }

   } );

   @Override
   public boolean onStopJob(JobParameters jobParameters) {
      Log.e(this.getClass().getName(),"Sess.Service onDestroy");
      mJobHandler.removeMessages( 1 );
      return false;
   }*/


     @Override
     public void onCreate() {
        this.context = this;
        this.isRunning = false;
        this.backgroundThread = new Thread(myTask);
     }

     private Runnable myTask = new Runnable() {
        public void run() {
           // Do something here

           Log.e("TransactionService", "started");
           ArrayList<Object> obj = new ArrayList<>();
           obj.add(OfflineTxService.class);


         network = NetworkConnection.getNetworkConnection(context);

         if (network.isNetworkAvailable() ) {
            Log.e(this.getClass().getName(),"Service started");
            start_All_services(obj,null);
         }

      }
   };

    private void start_All_services(ArrayList<Object> obj, Intent intent) {

        for (int i = 0; i < obj.size(); i++) {
            Log.e(this.getClass().getName(), "Service called");


         /*   if (((Class) obj.get(i)).getName().equals("com.oasys.eshakti.Service.OfflineTxService")) {
               OfflineTxService.enqueueWork(getApplicationContext(),intent);
            }*/

           getApplicationContext().startService(new Intent(this, (Class<Object>) obj.get(i)));

        }


    }

  /*  @Override
    protected void onHandleWork(@NonNull Intent intent) {
        *//* your code here *//*
        Log.e("TransactionService", "jobService task running");

        try {

            ArrayList<Object> obj = new ArrayList<>();
            obj.add(OfflineTxService.class);

            network = NetworkConnection.getNetworkConnection(getApplicationContext());

            if (network.isNetworkAvailable()) {
                Log.e(this.getClass().getName(), "Sess.Service started");
                start_All_services(obj,intent);
            }


        } catch (Exception e) {
            e.printStackTrace();
            Log.e(this.getClass().getName(), e.getMessage());
        }



        *//* reset the alarm *//*
    //    OfflineTxReceiver.setAlarm(getApplicationContext(), false);
        stopSelf();
    }

    private static final int JOB_ID = 1000;

    public static void enqueueWork(Context ctx, Intent intent) {
        enqueueWork(ctx, TransactionService.class, JOB_ID, intent);
    }*/


  /*  @Override
    public void onDestroy() {
        this.isRunning = false;
        Log.e(this.getClass().getName(), "Kill Service called in OnDestroy");
        Intent i = new Intent(OfflineTxReceiver.CUSTOM_INTENT1);
        sendBroadcast(i);
        super.onDestroy();

    }
*/
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    final Handler mHandler = new Handler();

    // Helper for showing tests
    void showToast(final CharSequence text) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(TransactionService.this, text, Toast.LENGTH_SHORT).show();
            }
        });
    }
   @Override
   public void onDestroy() {
      this.isRunning = false;

   }

   @Override
   public int onStartCommand(Intent intent, int flags, int startId) {
      Log.e(this.getClass().getName(),"onStartCommand called");
      if(!this.isRunning) {
         Log.e(this.getClass().getName(),"onStartCommand recalled");
         this.isRunning = true;
         this.backgroundThread.start();
      }
      stopSelf(startId);
      return START_NOT_STICKY;
   }

}
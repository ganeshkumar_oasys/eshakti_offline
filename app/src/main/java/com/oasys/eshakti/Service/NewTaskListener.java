package com.oasys.eshakti.Service;


import com.oasys.eshakti.OasysUtils.ServiceType;

public interface NewTaskListener {
	
	void onTaskStarted();
	
	void onTaskFinished(String result, ServiceType serviceType);

}
